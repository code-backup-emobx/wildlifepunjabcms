<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\HarikaDetail;
use App\Models\HarikaImages;
use App\Models\NangalImages;
use App\Models\NangalDetail;

use App\Models\KesopurDetail;
use App\Models\KeshopurImages;

class ProtectedWetland extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

  	public function harikeWildlifeList(){

  		$harika_detail_listing = HarikaDetail::with('get_harika_images')->where('deleted_status','0')->paginate(10);
  		return view('protectedWetland.harikeWetland.listing',compact('harika_detail_listing'));
  	}

  	public function addharikeWildlife(){

  		return view('protectedWetland.harikeWetland.add');
  	}

  	public function saveharikeWildlife(Request $request){

  		// return $request->all();

      $validated = $request->validate([
       	 'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'title_wildlife' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
      	'access' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'altitude' => 'required',
        'flora' => 'required',
        'important_flora' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'fauna' => 'required',
        'historical_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'historical_description' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'title' => 'required',
        'image' => 'required',
      ]);


  		$banner_image = $request->file('banner_image');
  		$banner_heading = $request->input('banner_heading');
  		$title_wildlife = $request->input('title_wildlife');
  		$description = $request->input('description');
  		$location = $request->input('location');
  		$access = $request->input('access');
  		$latitude = $request->input('latitude');
  		$longitude = $request->input('longitude');
  		$altitude = $request->input('altitude');
  		$flora = $request->input('flora');
  		$fauna = $request->input('fauna');
  		$historical_heading = $request->input('historical_heading');
  		$historical_description = $request->input('historical_description');

  		$title = $request->input('title');
  		$image = $request->input('image');

  		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/harike_wildlife_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/harike_wildlife_image/' . $filename;
  
		}

  		$save_harika_detail = new HarikaDetail();
  		$save_harika_detail->banner_image = $banner_image;
  		$save_harika_detail->banner_heading = $banner_heading;
  		$save_harika_detail->title_wildlife = $title_wildlife;
  		$save_harika_detail->description = $description;
  		$save_harika_detail->location = $location;
  		$save_harika_detail->access = $access;
  		$save_harika_detail->latitude = $latitude;
  		$save_harika_detail->longitude = $longitude;
  		$save_harika_detail->altitude = $altitude;
  		$save_harika_detail->flora = $flora;
  		$save_harika_detail->fauna = $fauna;
  		$save_harika_detail->historical_heading = $historical_heading;
  		$save_harika_detail->historical_description = $historical_description;
  		$save_harika_detail->save();


		$harika_detail_id = $save_harika_detail->id;

          $image_data=array();
		if($request->hasFile('image') != "")
    {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				        $file->move(public_path() . '/harike_wildlife_image/', $image);
				        $image_data[] = '/harike_wildlife_image/'.$image;
            }
		}

		for($i=0;$i<count($title);$i++)
        {
        	$save_harika_data = new HarikaImages();
        	$save_harika_data->harika_detail_id = $harika_detail_id;
        	$save_harika_data->title=$title[$i];
        	$save_harika_data->image=$image_data[$i];
        	
        	$save_harika_data->save();
        }

        return redirect('/harike_wildlife')->with('success','Harika Detail Added Successfully');

  	}

  	public function editharikeWildlife($id){

  		$edit_harika_detail = HarikaDetail::with('get_harika_images')->first();
  		return view('protectedWetland.harikeWetland.edit',compact('edit_harika_detail'));
  	}

  	public function updateharikeWildlife(Request $request , $id){

  		// return $request->all();
		
      $validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'title_wildlife' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'access' => 'required',
        'fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'historical_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'historical_description' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
      ]);   
    
  		$banner_image = $request->file('banner_image');
  		$banner_heading = $request->input('banner_heading');
  		$title_wildlife = $request->input('title_wildlife');
  		$description = $request->input('description');
  		$location = $request->input('location');
  		$access = $request->input('access');
  		$latitude = $request->input('latitude');
  		$longitude = $request->input('longitude');
  		$altitude = $request->input('altitude');
  		$flora = $request->input('flora');
  		$fauna = $request->input('fauna');
  		$historical_heading = $request->input('historical_heading');
  		$historical_description = $request->input('historical_description');

  		$title = $request->input('title');
  		$image = $request->input('image');

  		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/harike_wildlife_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/harike_wildlife_image/' . $filename;
            $update_harika_detail = HarikaDetail::find($id);
      			$update_harika_detail->banner_image = $banner_image;
      			$update_harika_detail->save();
		}
    else{

          $update_harika_detail = HarikaDetail::find($id);
      $update_harika_detail->banner_heading = $banner_heading;
      $update_harika_detail->title_wildlife = $title_wildlife;
      $update_harika_detail->description = $description;
      $update_harika_detail->location = $location;
      $update_harika_detail->access = $access;
      $update_harika_detail->latitude = $latitude;
      $update_harika_detail->longitude = $longitude;
      $update_harika_detail->altitude = $altitude;
      $update_harika_detail->flora = $flora;
      $update_harika_detail->fauna = $fauna;
      $update_harika_detail->historical_heading = $historical_heading;
      $update_harika_detail->historical_description = $historical_description;
      $update_harika_detail->save();


    // $harika_detail_id = $update_harika_detail->id;

    //      $image_data=array();
    // if($request->hasFile('image') != "")
    //     {
    //         foreach($request->file('image') as $key=>$file)
    //         {
    //             // $image = $file->getClientOriginalName();
    //             $image = time() . '.' . $file->getClientOriginalName();
    //     $file->move(public_path() . '/harike_wildlife_image/', $image);
    //     $image_data[] = '/harike_wildlife_image/'.$image;
    //         }
    // }

    //   for($i=0;$i<count($title);$i++)
    //     {
    //       $update_harika_data = new HarikaImages();
    //       $update_harika_data->harika_detail_id = $harika_detail_id;
    //       $update_harika_data->title=$title[$i];
    //       $update_harika_data->image=$image_data[$i];
          
    //       $update_harika_data->save();
    //     }
    }
  	

        return redirect('/harike_wildlife')->with('success','Harika Detail Added Successfully');
  	}

  	public function deleteharikeWildlife($id){

  		  $delete_harika_data = HarikaDetail::find($id);
       	$delete_harika_data->deleted_status = '1';
        $delete_harika_data->save();
       
        return redirect('/harike_wildlife')->with('success','Harika Detail Deleted Successfully');
      
  	}

    public function nangalwildlifesantuariesList(){

      $nangallisting = NangalDetail::with('get_nangal_images')->where('deleted_status','0')->paginate(10);
      return view('protectedWetland.nangal.listing',compact('nangallisting'));

    }

    public function addnangalwildlife(){

       return view('protectedWetland.nangal.add');
    }
    
    public function savenangalwildlife(Request $request){

        // return $request->all();

       $validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'title_wildlife' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
      	'access' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'altitude' => 'required',
        'flora' => 'required',
        'important_flora' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'fauna' => 'required',
        'historical_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'historical_description' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'title' => 'required',
        'image' => 'required',
      ]);

      $banner_image = $request->file('banner_image');
      $banner_heading = $request->input('banner_heading');
      $title_wildlife = $request->input('title_wildlife');
      $description = $request->input('description');
      $location = $request->input('location');
      $access = $request->input('access');
      $latitude = $request->input('latitude');
      $longitude = $request->input('longitude');
      $altitude = $request->input('altitude');
      $flora = $request->input('flora');
      $fauna = $request->input('fauna');
      $historical_heading = $request->input('historical_heading');
      $historical_description = $request->input('historical_description');

      $title = $request->input('title');
      $image = $request->input('image');

      if($request->hasFile('banner_image') != ""){
          $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/harike_wildlife_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/harike_wildlife_image/' . $filename;
  
    }

      $save_nangal_detail = new NangalDetail();
      $save_nangal_detail->banner_image = $banner_image;
      $save_nangal_detail->banner_heading = $banner_heading;
      $save_nangal_detail->title_wildlife = $title_wildlife;
      $save_nangal_detail->description = $description;
      $save_nangal_detail->location = $location;
      $save_nangal_detail->access = $access;
      $save_nangal_detail->latitude = $latitude;
      $save_nangal_detail->longitude = $longitude;
      $save_nangal_detail->altitude = $altitude;
      $save_nangal_detail->flora = $flora;
      $save_nangal_detail->fauna = $fauna;
      $save_nangal_detail->historical_heading = $historical_heading;
      $save_nangal_detail->historical_description = $historical_description;
      $save_nangal_detail->save();


    $nangal_detail_id = $save_nangal_detail->id;

          $image_data=array();
    if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
        $file->move(public_path() . '/harike_wildlife_image/', $image);
        $image_data[] = '/harike_wildlife_image/'.$image;
            }
    }

    for($i=0;$i<count($title);$i++)
        {
          $save_nangal_data = new NangalImages();
          $save_nangal_data->harika_detail_id = $nangal_detail_id;
          $save_nangal_data->title=$title[$i];
          $save_nangal_data->image=$image_data[$i];
          
          $save_nangal_data->save();
        }

        return redirect('/nangal_wildlife_santuaries')->with('success','Nangal Wildlife Santuaries Detail Added Successfully');

    }

    public function editnangalwildlife($id){

        $editnangal = NangalDetail::with('get_nangal_images')->where('id',$id)->first();
         return view('protectedWetland.nangal.edit',compact('editnangal'));

    }

    public function updatenangalwildlife(Request $request,$id){

          // return $request->all();
    
       $validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'title_wildlife' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'access' => 'required',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'historical_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'historical_description' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
      ]);   

      $banner_image = $request->file('banner_image');
      $banner_heading = $request->input('banner_heading');
      $title_wildlife = $request->input('title_wildlife');
      $description = $request->input('description');
      $location = $request->input('location');
      $access = $request->input('access');
      $latitude = $request->input('latitude');
      $longitude = $request->input('longitude');
      $altitude = $request->input('altitude');
      $flora = $request->input('flora');
      $fauna = $request->input('fauna');
      $historical_heading = $request->input('historical_heading');
      $historical_description = $request->input('historical_description');

      $title = $request->input('title');
      $image = $request->input('image');

      if($request->hasFile('banner_image') != ""){
          $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/harike_wildlife_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/harike_wildlife_image/' . $filename;
            $save_harika_detail = NangalDetail::find($id);
        $update_nangal_detail->banner_image = $banner_image;
        $update_nangal_detail->save();
    }

      $update_nangal_detail = NangalDetail::find($id);
      $update_nangal_detail->banner_heading = $banner_heading;
      $update_nangal_detail->title_wildlife = $title_wildlife;
      $update_nangal_detail->description = $description;
      $update_nangal_detail->location = $location;
      $update_nangal_detail->access = $access;
      $update_nangal_detail->latitude = $latitude;
      $update_nangal_detail->longitude = $longitude;
      $update_nangal_detail->altitude = $altitude;
      $update_nangal_detail->flora = $flora;
      $update_nangal_detail->fauna = $fauna;
      $update_nangal_detail->historical_heading = $historical_heading;
      $update_nangal_detail->historical_description = $historical_description;
      $update_nangal_detail->save();


    // $harika_detail_id = $update_nangal_detail->id;

    //      $image_data=array();
    // if($request->hasFile('image') != "")
    //     {
    //         foreach($request->file('image') as $key=>$file)
    //         {
    //             // $image = $file->getClientOriginalName();
    //             $image = time() . '.' . $file->getClientOriginalName();
    //     $file->move(public_path() . '/harike_wildlife_image/', $image);
    //     $image_data[] = '/harike_wildlife_image/'.$image;
    //         }
    // }

    // for($i=0;$i<count($title);$i++)
    //     {
    //       $update_harika_data = NangalImages::find($id);
    //       $update_harika_data->harika_detail_id = $harika_detail_id;
    //       $update_harika_data->title=$title[$i];
    //       $update_harika_data->image=$image_data[$i];
          
    //       $update_harika_data->save();
    //     }

        return redirect('/nangal_wildlife_santuaries')->with('success','Nangal Wildlife Santuaries Detail Updated Successfully');
    }

    public function deletenangalwildlife($id){

       $delete_nangal =  NangalDetail::find($id);
        $delete_nangal->deleted_status = '1';
        $delete_nangal->save();

         return redirect('/nangal_wildlife_santuaries')->with('success','Nangal Wildlife Santuaries Detail Delete Successfully');
    }

    public function keshopursantuariesList(){

      $keshopur_detail_listing = KesopurDetail::with('get_keshopur_images')->where('deleted_status','0')->paginate(10);
      return view('protectedWetland.keshopur.listing',compact('keshopur_detail_listing'));
    }


    public function addkeshopur(){

      return view('protectedWetland.keshopur.add');
    }

    public function savekeshopur( Request $request){
	
           // return $request->all();
           $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'title_wildlife' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
      	'access' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'altitude' => 'required',
        'flora' => 'required',
        'important_flora' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'fauna' => 'required',
        'historical_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'historical_description' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'title' => 'required',
        'image' => 'required',
                ]);
      $banner_image = $request->file('banner_image');
      $banner_heading = $request->input('banner_heading');
      $title_wildlife = $request->input('title_wildlife');
      $description = $request->input('description');
      $location = $request->input('location');
      $access = $request->input('access');
      $latitude = $request->input('latitude');
      $longitude = $request->input('longitude');
      $altitude = $request->input('altitude');
      $flora = $request->input('flora');
      $fauna = $request->input('fauna');
      $historical_heading = $request->input('historical_heading');
      $historical_description = $request->input('historical_description');

      $title = $request->input('title');
      $image = $request->input('image');

      if($request->hasFile('banner_image') != ""){
          $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/harike_wildlife_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/harike_wildlife_image/' . $filename;
  
    }

      $save_keshopur_detail = new KesopurDetail();
      $save_keshopur_detail->banner_image = $banner_image;
      $save_keshopur_detail->banner_heading = $banner_heading;
      $save_keshopur_detail->title_wildlife = $title_wildlife;
      $save_keshopur_detail->description = $description;
      $save_keshopur_detail->location = $location;
      $save_keshopur_detail->access = $access;
      $save_keshopur_detail->latitude = $latitude;
      $save_keshopur_detail->longitude = $longitude;
      $save_keshopur_detail->altitude = $altitude;
      $save_keshopur_detail->flora = $flora;
      $save_keshopur_detail->fauna = $fauna;
      $save_keshopur_detail->historical_heading = $historical_heading;
      $save_keshopur_detail->historical_description = $historical_description;
      $save_keshopur_detail->save();


    $nangal_detail_id = $save_keshopur_detail->id;

          $image_data=array();
    if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
        $file->move(public_path() . '/harike_wildlife_image/', $image);
        $image_data[] = '/harike_wildlife_image/'.$image;
            }
    }

    for($i=0;$i<count($title);$i++)
        {
          $save_keshopur_detail = new KeshopurImages();
          $save_keshopur_detail->harika_detail_id = $nangal_detail_id;
          $save_keshopur_detail->title=$title[$i];
          $save_keshopur_detail->image=$image_data[$i];
          
          $save_keshopur_detail->save();
        }

        return redirect('/keshopur_santuaries')->with('success','Keshopur Wildlife Santuaries Detail Added Successfully');

    }

    public function editkeshopurdetail($id){

      $keshopur_update = KesopurDetail::with('get_keshopur_images')->where('id',$id)->first();
       return view('protectedWetland.keshopur.edit',compact('keshopur_update'));
    }

    public function updatekeshopurdetail(Request $request,$id){

            // return $request->all();
	
       $validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'title_wildlife' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'historical_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'historical_description' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
      ]);   
    
      $banner_image = $request->file('banner_image');
      $banner_heading = $request->input('banner_heading');
      $title_wildlife = $request->input('title_wildlife');
      $description = $request->input('description');
      $location = $request->input('location');
      $access = $request->input('access');
      $latitude = $request->input('latitude');
      $longitude = $request->input('longitude');
      $altitude = $request->input('altitude');
      $flora = $request->input('flora');
      $fauna = $request->input('fauna');
      $historical_heading = $request->input('historical_heading');
      $historical_description = $request->input('historical_description');

      $title = $request->input('title');
      $image = $request->input('image');

      if($request->hasFile('banner_image') != ""){
          $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/harike_wildlife_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/harike_wildlife_image/' . $filename;
            $update_kshopur_detail = NangalDetail::find($id);
        $update_kshopur_detail->banner_image = $banner_image;
        $update_kshopur_detail->save();
    }

      $update_kesopur_detail = KesopurDetail::find($id);
      $update_kesopur_detail->banner_heading = $banner_heading;
      $update_kesopur_detail->title_wildlife = $title_wildlife;
      $update_kesopur_detail->description = $description;
      $update_kesopur_detail->location = $location;
      $update_kesopur_detail->access = $access;
      $update_kesopur_detail->latitude = $latitude;
      $update_kesopur_detail->longitude = $longitude;
      $update_kesopur_detail->altitude = $altitude;
      $update_kesopur_detail->flora = $flora;
      $update_kesopur_detail->fauna = $fauna;
      $update_kesopur_detail->historical_heading = $historical_heading;
      $update_kesopur_detail->historical_description = $historical_description;
      $update_kesopur_detail->save();


    // $harika_detail_id = $update_kshopur_detail->id;

    //      $image_data=array();
    // if($request->hasFile('image') != "")
    //     {
    //         foreach($request->file('image') as $key=>$file)
    //         {
    //             // $image = $file->getClientOriginalName();
    //             $image = time() . '.' . $file->getClientOriginalName();
    //     $file->move(public_path() . '/harike_wildlife_image/', $image);
    //     $image_data[] = '/harike_wildlife_image/'.$image;
    //         }
    // }

    // for($i=0;$i<count($title);$i++)
    //     {
    //       $update_keshopur_detail = KesopurImages::find($id);
    //       $update_keshopur_detail->harika_detail_id = $harika_detail_id;
    //       $update_keshopur_detail->title=$title[$i];
    //       $update_keshopur_detail->image=$image_data[$i];
          
    //       $update_keshopur_detail->save();
    //     }

        return redirect('/keshopur_santuaries')->with('success','Keshopur Wildlife Santuaries Detail Updated Successfully');
    }

    public function deletekeshopurdetail($id){

        $delete_harika_detail = KesopurDetail::find($id);
        $delete_harika_detail->deleted_status = '1';
        $delete_harika_detail->save();
         return redirect('/keshopur_santuaries')->with('success','Keshopur Wildlife Santuaries Detail Deleted Successfully');
    }
// end class 
}