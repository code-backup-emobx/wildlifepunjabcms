<?php error_reporting('0'); ?>
@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Bir Bhunerheri Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/bir_bhunerheri_pa' ) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Bir Bhunerheri List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('/update_birbhunerheri/'.$editbirBhunerheri->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Bir Moti Bagh Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="">

                                            <div>
                                            	<img src="{{ $editbirBhunerheri->banner_image }}" style="width:50%;height:50%">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Bir Moti Bagh Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{ $editbirBhunerheri->banner_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea class="form-control" name="description" rows="9" style=" height: 118px;">{{ $editbirBhunerheri->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>WildLife Heading:</label>
                                        <input type="text" class="form-control"  placeholder="Enter WildLife Heading" name="wildlife_heading" value="{{ $editbirBhunerheri->wildlife_heading }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>WildLife Title:</label>
                                        <input type="text" class="form-control" placeholder="Enter Wild Life Title" name="wildlife_title" value="{{ $editbirBhunerheri->wildlife_title }}">
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>District:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full District" name="district" value="{{ $editbirBhunerheri->district }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ $editbirBhunerheri->location }}"/>
                                    </div>
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Area:</label>
                                        <input type="text" class="form-control" placeholder="Enter the Area" name="area" value="{{ $editbirBhunerheri->area }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Status Of Land:</label>
                                        <input type="text" class="form-control" placeholder="Enter Status Of Land" name="status_of_land" value="{{ $editbirBhunerheri->status_of_land }}"/>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Notification Detail: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="notification_detail" style=" height: 112px;" rows="5">{{ $editbirBhunerheri->notification_detail }}</textarea>
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Important Fauna: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="important_fauna" style=" height: 112px;" rows="5">{{ $editbirBhunerheri->important_fauna }}</textarea>
                                        </div>
                                    </div>
                                </div>   
                                    
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Important Fauna: </label>
                                        <div class="col-lg-6">
                                             <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="important_flora" style=" height: 112px;" rows="5">{{ $editbirBhunerheri->important_flora }}</textarea>
                                        </div>
                                    </div>
                                </div>    
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
