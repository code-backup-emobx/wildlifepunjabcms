@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Beas River Conservation Reserve</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/cr_beasriver') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Beas River List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('update_beasriver/'.$editbeas_river->id ) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Beas River Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="">

                                            <div>
                                            	<img src="{{ $editbeas_river->banner_image }}" style="width:50%;height:50%;margin-top:2%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Beas River Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{ $editbeas_river->banner_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Beas River Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea class="form-control" name="description" style="height:100px;" rows="9">{{ $editbeas_river->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Beas River Heading:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Beas River Heading" name="rakhsarai_heading" value="{{ $editbeas_river->rakhsarai_heading }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Beas River Title:</label>
                                        <input type="text" class="form-control" placeholder="Enter Beas River Title" name="rakhsarai_title" value="{{ $editbeas_river->rakhsarai_title }}">
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>District:</label>
                                        <input type="text" class="form-control" name="district" value="{{ $editbeas_river->district }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control"  name="location" value="{{ $editbeas_river->location }}"/>
                                    </div>
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Area:</label>
                                        <input type="text" class="form-control" value="{{ $editbeas_river->area }}" name="area"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Status Of Land:</label>
                                        <input type="text" class="form-control"value="{{ $editbeas_river->status_of_land }}" name="status_of_land"/>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Notification Detail: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="notification_detail" style="height:100px;" rows="5">{{ $editbeas_river->notification_detail }}</textarea>
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Important Fauna: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="important_fauna" style="height:100px;" rows="5">{{ $editbeas_river->important_fauna }}</textarea>
                                        </div>
                                    </div>
                                </div>   
                                    
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Important Fauna: </label>
                                        <div class="col-lg-6">
                                             <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="important_flora"  style="height:100px;" rows="5">{{ $editbeas_river->important_flora }}</textarea>
                                        </div>
                                    </div>
                                </div>    
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
