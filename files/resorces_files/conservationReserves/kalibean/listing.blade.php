@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Kali Bean List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_kalibean') }}">
                                    <button type="button" class="btn btn-sm">Add Kali Bean
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Background Image </th>
                                                        <th> Banner Title </th>
                                                        <th> Description </th>
                                                        <th> Panniwala Gujwala</th>
                                                        <th> Table Heading </th>
                                                        <th> District </th>
                                                        <th> Location </th>
                                                        <th> Area </th>
                                                       <th> Notification Title </th>
                                                        <th> Notification Pdf </th>
                                                        <th> Important Fauna </th>
                                                        <th> Important Flora </th>
                                                        <th> Reach Us</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php 
                                                    $count = 1;
                                                    @endphp
                                                 	@foreach($kali_list as $listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $listing->banner_heading }} </td>
                                                        <td> {{ $listing->description }} </td>
                                                        <td> {{ $listing->rakhsarai_heading }} </td>
                                                        <td> {{ $listing->rakhsarai_title }} </td>
                                                        <td> {{ $listing->district }} </td>
                                                        <td> {{ $listing->location }} </td>
                                                        <td> {{ $listing->area }} </td>
                                                        
                                                         <td>
                                                            <a href="{{ url('/kalibean_notification_list/'.$listing->id) }}"><span class="btn btn-light btn-primary">{{ $listing->text_count }}</span></a>
                                                        </td>
                                                        <td>
                                                           <a href="{{ url('/kalibean_notification_pdf/'.$listing->id) }}"><span class="btn btn-light btn-primary">{{ $listing->pdf_count }}</span></a>
                                                        </td>
                                                        <td> {{ $listing->important_fauna }} </td>
                                                        <td>  {{ $listing->important_flora }} </td>
                                                        <td>
                                                            <a href="{{ url('update_kalibean/'.$listing->id ) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_kalibean/'.$listing->id ) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                      {{ $kali_list->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

