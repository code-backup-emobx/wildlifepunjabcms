@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Notification Text</span>
                            </div>
                            <!-- <div class="actions">
                              	<a href="{{ url('notification_list/') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Bir Moti Bagh List</button>
                    			
                				</a>
                            </div> -->
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('ropar_edit_notification_text/'.$edit_notification->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                             
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Notification Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="notification_title" value="{{ $edit_notification->notification_title}}">
                                        </div>
                                    </div>
                                </div>

                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>
@endpush