@extends('layouts.master')
@section('content')

	<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Tourism Gallery</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/tourism_detail') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Tourism List</button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('add_gallery/'.$t_id->tourism_type_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Type: </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="tourism_type_id">
                                           	<option>Select</option>
                                         	@foreach($tourism_type as $list)
                                         		<option value="{{ $list->id }}">{{ $list->type_name }}</option>
                                         	@endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" 
                                            name="banner_image" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" 
                                            name="banner_heading" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Description Heading:</label>
                											<input type="text" class="form-control" 
                                        name="description_heading" value="" required>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Description:</label>
                											<textarea name="description" id="editor1"></textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>How To Reach:</label>
                											<input type="text" class="form-control" 
                                        name="heading_how_to_reach" value="" required>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>How To Reach Detail:</label>
                											<textarea name="detail_how_to_reach" id="editor2"></textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Permission Required Heading:</label>
                  											<input type="text" class="form-control" 
                                          name="heading_permission_required" value="" required>
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Permission Required Description:</label>
                											<textarea name="detail_required_permmision" id="editor3"></textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Hire A Guide Heading:</label>
                  											<input type="text" class="form-control" 
                                          name="heading_hire_a_guide" value="" required>
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Hire A Guide Heading Description:</label>
                											<textarea name="descripton_hire_guide" id="editor4"></textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                  									<div class="col-lg-12">
                  										<label>Accommodation Options Heading:</label>
                  										<input type="text" class="form-control" 
                                        name="heading_accommodation_options" value="" required>
                  									</div>
                  								</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Accommodation Options Detail:</label>
                											<textarea name="detail_accommodation_options" id="editor5"></textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Do's Heading:</label>
                  											<input type="text" class="form-control" 
                                           name="heading_do" value="" required>
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Do's Detail:</label>
                											<textarea name="detail_do" id="editor7"></textarea>
                										</div>
                									</div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Don'ts Heading:</label>
                  											<input type="text" class="form-control" 
                                           name="heading_dont" value="" required>
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                    <div class="col-lg-12">
                                      <label>Don'ts Detail:</label>
                                      <textarea name="detail_dont" id="editor8"></textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                      <div class="col-lg-12">
                                        <label>Heading Best Time Visit:</label>
                                        <input type="text" class="form-control" 
                                           name="heading_best_time_visit" value="" required>
                                      </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                    <div class="col-lg-12">
                                      <label>Heading Best Time Visit Detail:</label>
                                      <textarea name="detail_best_time_visit" id="editor9"></textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                      <div class="col-lg-12">
                                        <label>Heading Images:</label>
                                        <input type="text" class="form-control" 
                                           name=" images_heading" value="" required>
                                      </div>
                                    </div>
                                </div>


                                <div class="form-body geographic_zone" id="add_images" style="padding-left: 5%">
                                   	<div class="card-body">
                  										<div class="form-group row">
                  											<div class="col-lg-6">
                  												<label>Add Image:</label>
                  												<input type="file" class="form-control" 
                  												name="image[]"/>
                  											</div>
                  											<div class="col-lg-6">
                  												<label>Add Image:</label>
                  												<input type="file" class="form-control" 
                  												name="image[]"/>
                  											</div>
                  										</div>
                  									</div>
                                </div>

                                 <div id="add_more_geographic_zone" style="padding-left: 5%;margin-bottom:5%;">
                                    </div>

                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" 
                                    value="Add More" style="float:right;">Add More</button>
                                </div>   

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
    CKEDITOR.replace('editor1');
</script>
<script>
    CKEDITOR.replace('editor2');
</script>
<script>
    CKEDITOR.replace('editor3');
</script>
<script>
    CKEDITOR.replace('editor4');
</script>
<script>
    CKEDITOR.replace('editor5');
</script>
<script>
    CKEDITOR.replace('editor6');
</script>
<script>
	 CKEDITOR.replace('editor7');
</script>
<script>
   CKEDITOR.replace('editor8');
</script>
<script>
   CKEDITOR.replace('editor9');
</script>
<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
    	// alert('hello');
      var html = $( ".geographic_zone" ).html();
       $("#add_more_geographic_zone").append(html);
  });
});
</script>

@endpush
