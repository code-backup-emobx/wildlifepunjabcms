@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Tourism List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_tourism') }}">
                                    <button type="button" class="btn btn-sm">Add Tourism
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Background Image </th>
                                                        <th> Heading </th>
                                                        <th> Description Heading </th>
                                                        <th> Description </th>
                                                        <th> Heading How To Reach </th>
                                                        <th> Detail How To Reach </th>
                                                        <th> Heading Permission Required </th>
                                                        <th> Detail Permission Required </th>
                                                        <th> Heading Hire A Guide </th>
                                                        <th> Detail Hire A Guide </th>
                                                        <th> Heading Accommodation options </th>
                                                        <th> Detail Accommodation options </th>
                                                        <th> Heading Do </th>
                                                        <th> Detail Do </th>
                                                        <th> Heading Don't </th>
                                                        <th> Detail Don't </th>
                                                        <th> Heading Best Time Visit </th>
                                                        <th> Detail Best Time Visit </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php 
                                                    $count = 1;
                                                    @endphp
                                                 	@if(!empty($tourismDetaillist))
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $tourismDetaillist->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $tourismDetaillist->banner_heading }} </td>
                                                        <td> {{ $tourismDetaillist->description_heading }} </td>
                                                        <td> {!! $tourismDetaillist->description !!} </td>
                                                         <td> {{ $tourismDetaillist->heading_how_to_reach }} </td>
                                                         <td> {!! $tourismDetaillist->detail_how_to_reach !!} </td>
                                                         <td> {{ $tourismDetaillist->heading_permission_required }} </td>

                                                         <td> {!! $tourismDetaillist->	detail_required_permmision !!} </td>

                                                         <td> {{ $tourismDetaillist->	heading_hire_a_guide }} </td>

                                                         <td> {!! $tourismDetaillist->	descripton_hire_guide !!} </td>

                                                         <td> {{ $tourismDetaillist->		heading_accommodation_options }} </td>

                                                         <td> {!! $tourismDetaillist->	detail_accommodation_options !!} </td>

                                                         <td> {{ $tourismDetaillist->heading_do }} </td>
                                                         <td> {!! $tourismDetaillist->detail_do !!} </td>
                                                         <td> {{ $tourismDetaillist->heading_dont }} </td>
                                                         <td> {!! $tourismDetaillist->detail_dont !!} </td>

                                                         <td> {{ $tourismDetaillist->heading_best_time_visit }} </td>

                                                         <td> {!! $tourismDetaillist->detail_best_time_visit !!} </td>
                                                      
                                                        <td>
                                                           
                                                             <a href="{{url('edit_detail/'.$tourismDetaillist->tourism_type_id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white; margin-top:3%;"><i class="fa fa-edit"></i> Edit</button></a>

                                                            <a href="{{url('delete_detail/'.$tourismDetaillist->tourism_type_id )}}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>

                                                        </td>
                                                    </tr>
                                                    @else
                                                    <tr>
                                                    	<td><h5 >No Data </h5></td>
                                                    </tr>
                                                   @endif
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

