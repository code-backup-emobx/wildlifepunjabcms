@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Tourism Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/tourism_detail') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Tourism List</button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('update_tourism/'.$edit_tourism->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" 
                                            name="banner_image" value="">
                                            <div>
                                            	<img src="{{ $edit_tourism->banner_image }}" style="width:50%;height:50%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" 
                                            name="banner_heading" value="{{ $edit_tourism->banner_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Type : </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="tourism_type_id">

                                           	<option value="">Select</option>
                                           	@foreach($tourism_type as $list)
                                           		<option value="{{ $list->id }}" @if($list->id == $edit_tourism->get_bannerdata->tourism_type_id) selected @endif>{{ $list->type_name }}</option>
                                           	@endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Image : </label>
                                        <div class="col-lg-6">
                                           <input type="file" name="image" class="form-control" value="">
                                      

                                        <div>
                                            <img src="{{ $edit_tourism->get_bannerdata->image }}" style="width:26%;height:50%;">
                                        </div>
                                          </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Time : </label>
                                        <div class="col-lg-6">
                                           <input type="text" name="time" class="form-control" value="{{ $edit_tourism->get_bannerdata->time }}">
                                        </div>
                                    </div>
                                </div>

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
