@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Harike Wildlife Sanctuary</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/harike_wildlife') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Harike Wildlife Sanctuary List</button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('/add_harike_wildlife ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Harike Wildlife Sanctuary Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Harike Wildlife Sanctuary Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Harike Wildlife Title : </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="title_wildlife" value="{{ old('title_wildlife') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Harike Wildlife Sanctuary Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea class="form-control" name="description" rows="5">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">

                                	<div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ old('location') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full District" name="access" value="{{ old('access') }}"/>
                                    </div>
                                 
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Latitute:</label>
                                        <input type="text" class="form-control" placeholder="Enter the Area" name="latitude" value="{{ old('latitude') }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Status Of Land" name="longitude" value="{{ old('longitude') }}"/>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Altitude: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="altitude" rows="5">{{ old('altitude') }}</textarea>
                                        </div>
                                    </div>
                                </div>  

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Flora: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="flora" rows="5">{{ old('flora') }}</textarea>
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Fauna: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="fauna" rows="5">{{ old('fauna') }}</textarea>
                                        </div>
                                    </div>
                                </div>   

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Historical Heading: </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" 
                                            name="historical_heading" value="{{ old('historical_heading') }}">
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Historical Description: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="historical_description" rows="5">{{ old('historical_description') }}</textarea>
                                        </div>
                                    </div>
                                </div>     
                                <!-- -------------------- -->
                                <div style="background-color: #E5EFF1;padding-bottom: 2%;">
                                        <div><p style="padding:1%;color:black;">Add Image And Title For Historical Importance</p></div>
                               		<div class="form-group row" style="padding-left:5%;">
	                                    <div class="col-lg-5">
	                                        <label>Title 1:</label>
	                                        <input type="text" class="form-control" placeholder="Enter the Area" name="title[]"/>
	                                    </div>
	                                    <div class="col-lg-5">
	                                        <label>Image 1:</label>
	                                        <input type="file" class="form-control" placeholder="Enter Status Of Land" name="image[]"/>
	                                    </div>
                                	</div>

                                	<div class="form-group row" style="padding-left:5%;">
	                                    <div class="col-lg-5">
	                                        <label>Title 2:</label>
	                                        <input type="text" class="form-control" placeholder="Enter the Area" name="title[]"/>
	                                    </div>
	                                    <div class="col-lg-5">
	                                        <label>Image 2:</label>
	                                        <input type="file" class="form-control" placeholder="Enter Status Of Land" name="image[]"/>
	                                    </div>
                                	</div>

                                	<div class="form-group row" style="padding-left:5%;">
	                                    <div class="col-lg-5">
	                                        <label>Title 3:</label>
	                                        <input type="text" class="form-control" placeholder="Enter the Area" name="title[]"/>
	                                    </div>
	                                    <div class="col-lg-5">
	                                        <label>Image 3:</label>
	                                        <input type="file" class="form-control" placeholder="Enter Status Of Land" name="image[]"/>
	                                    </div>
                                	</div>

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
