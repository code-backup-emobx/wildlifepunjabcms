@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Nangal Wildlife Sanctuary List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_nangal_wildlife') }}">
                                    <button type="button" class="btn btn-sm">Add Nangal Wildlife Sanctuary
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Background Image </th>
                                                        <th> Background Heading </th>
                                                        <th> Banner Title </th>
                                                        <th> Description </th>
                                                        <th> Nangal</th>
                                                        <th> Table Heading </th>
                                                        <th> District </th>
                                                        <th> Location </th>
                                                        <th> Area </th>
                                                        <th> Notification Detail </th>
                                                        <th> Important Fauna </th>
                                                        <th> Important Flora </th>
                                                        <th> Reach Us</th>
                                                        <th> Title </th>
                                                        <th> Image </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php 
                                                    $count = 1;
                                                    @endphp
                                                    @foreach($nangallisting as $listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $listing->banner_heading }} </td>
                                                        <td> {{ $listing->title_wildlife }} </td>
                                                        <td> {{ $listing->description }} </td>
                                                        <td> {{ $listing->location }} </td>
                                                        <td> {{ $listing->access }} </td>
                                                        <td> {{ $listing->latitude }} </td>
                                                        <td> {{ $listing->longitude }} </td>
                                                        <td> {{ $listing->altitude }} </td>
                                                        <td> {{ $listing->flora }} </td>
                                                        <td> {{ $listing->fauna }} </td>
                                                        <td> {{ $listing->historical_heading }} </td>
                                                        <td> {{ $listing->historical_description }} </td>
                                                        <td>
                                                        	@php
                                                        	$count = 1;
                                                        	@endphp
	                                                        @foreach($listing['get_nangal_images'] as $images_listing)
	                                                        	{{ $count++ }}.{{ $images_listing->title }}<hr>
	                                                        @endforeach
                                                        </td>
                                                        <td>
                                                        	@php
                                                        	$count = 1;
                                                        	@endphp
	                                                        @foreach($listing['get_nangal_images'] as $images_listing)
	                                                        	{{ $count++ }}.<img src="{{ $images_listing->image }}" style="width:89px;height:65px;margin-top:2%;"><hr>
	                                                        @endforeach
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('update_nangal_detail/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_nangal_detail/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $nangallisting->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

