@extends('layouts.master')
@section('content')


<div class="page-content-wrapper ">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        @if(Session::has('success'))

            <div class="alert alert-success">

                {{ Session::get('success') }}

                @php

                Session::forget('success');

                @endphp

            </div>

        @endif
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            
            <ul class="page-breadcrumb">
                <li>
                <a href="#">Home</a>
                <i class="fa fa-circle"></i>
                </li>
                <li>
                <span>Dashboard</span>
                </li>
            </ul>

        </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Admin Dashboard</h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="{{ url('/harike_wildlife')}}" class="dashboard-stat dashboard-stat-v2 blue">
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                       
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup">4</span>
                            </div>
                            <div class="desc"> Total Protected Wetland </div>
                        </div>
                       
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="{{ url('/chattbir_zoo_list') }}" class="dashboard-stat dashboard-stat-v2 red">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup">5</span> </div>
                            <div class="desc">Total Zoo </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="{{ url('/bir_motibagh_pa') }}" class="dashboard-stat dashboard-stat-v2 green">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup">13</span>
                            </div>
                            <div class="desc"> Total Wildlife Santuaries</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="url('/panniwalalist')" class="dashboard-stat dashboard-stat-v2 purple">
                        <div class="visual">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"></span>4</div>
                            <div class="desc"> Community Reserve </div>
                        </div>
                    </a>
                </div>
            </div>
        <div class="clearfix"></div>
    <!-- END DASHBOARD STATS 1-->
      <!--   <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                BEGIN PORTLET-->
               <!--  <div class="portlet light bordered">
                    <div> <h4>Lorem ipsum dolor sit amet</h4></div>
                    <figure class="highcharts-figure">
                        <div id="tendays"></div>
                    </figure>

                </div> -->
                <!-- END PORTLET-->
          <!--   </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                BEGIN PORTLET-->
                <!-- <div class="portlet light bordered">
                    <div><h4>Lorem ipsum dolor sit amet</h4></div>
                    <figure class="highcharts-figure">
                        <div id="closedcomplaints"></div>

                    </figure> --> 
                    <!-- END PORTLET-->
               <!--  </div>
            </div>
        </div> -->
    </div>
</div>
@endsection

