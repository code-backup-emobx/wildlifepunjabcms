<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Visitor;

class CountVisitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		// $ip = hash('sha512', $request->ip());
		$ip = $request->ip();
		
     
    // Visitor::where('date', today())->where('ip', $ip)->count() < 1
        if($check = Visitor::where('ip', $ip)->latest()->first())
        {
        // $date = Visitor::where('ip', $ip)->latest()->first();
		$updated_at_date = $check->updated_at;
		$to =  $updated_at_date;
		$from = date('Y-m-d H:i:s');
		$difference = $to->diff($from);
		$date_difference = $difference->i;
           if($date_difference > 10){
           		Visitor::create([
                'date' => today(),
                'ip' => $ip,
            ]);
           }
        }
   		else{
           Visitor::create([
                'date' => today(),
                'ip' => $ip,
            ]);
        }
       
      
        return $next($request);
    }
}