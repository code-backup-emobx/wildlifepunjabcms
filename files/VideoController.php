<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Video;
use App\Models\ProtectedAreaSubcategory;
use App\Models\ZooType;
use App\Models\ProtectWetland;


class VideoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function videosList(){

	   $listing = Video::get();

		return view('video.listing',compact('listing'));
	}

	public function addvideos(){

		$zoo_list = ZooType::get();
		$protected_area = ProtectedAreaSubcategory::get();
		$protect_wetland = ProtectWetland::get();
		return view('video.add',compact('zoo_list','protected_area','protect_wetland'));
	}

	public function savevideos(Request $request){


		$rules = [
        'video_title' => 'required',
        'type' => 'required',
        'video_link' => ['required', 'regex:/^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/'],
    ];

    $customMessages = [
        'regex' => 'Please enter the valid you tube video link.'
    ];

    $this->validate($request, $rules, $customMessages);
    	


		$video_title = $request->input('video_title');
		$video_link = $request->input('video_link');
		$type = $request->input('type');

		 $new_link = substr($video_link, 32); 

		$site_url = ProtectedAreaSubcategory::where('id',$type)->first();
		$site_url_	= $site_url->site_url;
		$save_video = new Video();
		$save_video->video_title = $video_title;
		$save_video->video_link = $new_link;
		$save_video->type = $type;
		$save_video->site_link = $site_url_;
		$save_video->save();

		return redirect('/videos_list')->with('success','Video Detail Save Succesfully');
	}

	public function deleteVideo($id){

		$delet_video = Video::find($id);
		$delet_video->delete();
		return redirect('/videos_list')->with('success','Video Detail Delete Succesfully');
	}

	public function editVideo($id){
		$zoo_list = ZooType::get();
		$edit_video = Video::where('id',$id)->first();
		$protected_area = ProtectedAreaSubcategory::get();
		return view('video.edit',compact('edit_video','zoo_list','protected_area'));
	}

	public function updateVideo(Request $request , $id){

		$rules = [
        'video_link' => ['regex:/^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/'],
    ];

    $customMessages = [
        'regex' => 'Please enter the valid you tube video link.'
    ];

    $this->validate($request, $rules, $customMessages);
    	


		$video_title = $request->input('video_title');
		$video_link = $request->input('video_link');
		$type = $request->input('type');

		 $new_link = substr($video_link, 32); 

		$update_video = Video::find($id);
		$update_video->video_title = $video_title;
		$update_video->video_link = $new_link;
		$update_video->type = $type;
		$update_video->save();

		return redirect('/videos_list')->with('success','Video Detail Update Succesfully');
	}

// end class 
}