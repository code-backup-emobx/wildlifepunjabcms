<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\FooterScheme;
use App\Models\FooterSchemeName;
use App\Models\FooterAbout;
use App\Models\FooterAboutDetail;
use App\Models\FooterPolicyGuideline;

use App\Models\NotificationBannerDetail;
use App\Models\NotificationDetail;

use App\Models\TenderBannerDetail;
use App\Models\TenderTypes;
use App\Models\TenderDetails;
 
use App\Models\ContactUsBannerDetail;
use App\Models\ContactUs;
use App\Models\ContactUsDetail;

use App\Models\ImpLinkDetail;

use App\Models\ServicesDetail;


class FooterController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function scheme_detailList(){

    	$footer_schemelist = FooterScheme::with('get_scheme_name')->where('deleted_status','0')->paginate(10);
    	return view('footer.usefullLink.scheme.listing',compact('footer_schemelist'));
    }

    public function addschemedetail(){

    	return view('footer.usefullLink.scheme.add');
    }

    public function saveschemedetail(Request $request){
	
    	
    
        $validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'name_of_scheme' => 'required',
        ]);

    	$banner_image = $request->file('banner_image'); 
    	$banner_heading = $request->input('banner_heading');
    	$name_of_scheme = $request->input('name_of_scheme');


		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_scheme_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_scheme_images/' . $filename;
		}

    	$add_scheme = new FooterScheme();
    	$add_scheme->banner_image = $banner_image ;
    	$add_scheme->banner_heading = $banner_heading ;
    	$add_scheme->save();

    	$banner_id = $add_scheme->id;
    	foreach($name_of_scheme as $scheme_name){

    		$save_name = new FooterSchemeName();
    		$save_name->banner_id = $banner_id;
    		$save_name->name_of_scheme = $scheme_name;
    		$save_name->save();
    	}

    	return redirect('/scheme_detail')->with('success','Footer Scheme Detail Added Successfully');
    }

    public function editchemedetail($id){

    	$edit_schemelist = FooterScheme::with('get_scheme_name')->where('id',$id)->first();
    	return view('footer.usefullLink.scheme.edit',compact('edit_schemelist'));
    }

    public function updatechemedetail(Request $request ,$id){
    
    	 $validated = $request->validate([
         'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        ]);


    	$banner_image = $request->file('banner_image'); 
    	$banner_heading = $request->input('banner_heading');
    	$name_of_scheme = $request->input('name_of_scheme');


		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_scheme_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_scheme_images/' . $filename;
            $add_scheme = FooterScheme::find($id);
    		$add_scheme->banner_image = $banner_image;
    		$add_scheme->save();
		}

    	$add_scheme = FooterScheme::find($id);
    	$add_scheme->banner_heading = $banner_heading ;
    	$add_scheme->save();

    	// $banner_id = $add_scheme->id;
    	// if(!empty($banner_id)){

    	// 	foreach($name_of_scheme as $scheme_name){

	    // 		$save_name = FooterSchemeName::find($banner_id);
	    // 		$save_name->name_of_scheme = $scheme_name;
	    // 		$save_name->save();
    	// 	}

    	// }
    
    	return redirect('/scheme_detail')->with('success','Footer Scheme Detail Added Successfully');
    }

    public function deletechemedetail($id){

    	$delete_scheme = FooterScheme::find($id);
    	$delete_scheme->deleted_status = '1' ;
    	$delete_scheme->save();
    	return redirect('/scheme_detail')->with('success','Footer Scheme Detail Delete Successfully');

    }

    public function about_usList(){

    	$about_list = FooterAbout::with('get_footer_about_detail')->where('deleted_status','0')->paginate(10);
    	return view('footer.about.listing',compact('about_list'));
    }

    public function addabout_us(){

    	return view('footer.about.add');
    }

    public function saveabout_us(Request $request){
    	
    
        $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
            'title' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
            'description' => 'required',
            'protected_area_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'protected_area_description' => 'required',
        ]);


    	$banner_image = $request->file('banner_image');
    	$banner_heading = $request->input('banner_heading');
    	$title = $request->input('title');
    	$description = $request->input('description');
    	$protected_area_heading = $request->input('protected_area_heading');
    	$protected_area_description = $request->input('protected_area_description');

    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_about_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_about_images/' . $filename;
		}

		$save_about_banner_detail = new FooterAbout();
		$save_about_banner_detail->banner_image = $banner_image;
		$save_about_banner_detail->banner_heading = $banner_heading;
		$save_about_banner_detail->save();

		$about_id = $save_about_banner_detail->id;

		if(!empty($about_id)){

	    		$save_name = new FooterAboutDetail();
	    		$save_name->banner_id = $about_id ;
	    		$save_name->title = $title;
	    		$save_name->description = $description;
	    		$save_name->protected_area_heading = $protected_area_heading;
	    		$save_name->protected_area_description = $protected_area_description;
	    		$save_name->save();
    	}

		return redirect('/about_us_detail')->with('success','Footer About Detail Added Successfully');
    }

    public function edit_about_us($id){

       $edit_about = FooterAbout::with('get_footer_about_detail')->where('id',$id)->first();
        return view('footer.about.edit',compact('edit_about'));
    }

    public function update_about_us(Request $request , $id){
		
    	 $validated = $request->validate([
         'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
            'title' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
            'protected_area_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);
    
        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');
        $title = $request->input('title');
        $description = $request->input('description');
        $protected_area_heading = $request->input('protected_area_heading');
        $protected_area_description = $request->input('protected_area_description');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_about_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_about_images/' . $filename;

            $update_about_banner_detail = FooterAbout::find($id);
            $update_about_banner_detail->banner_image = $banner_image;
            $update_about_banner_detail->banner_heading = $banner_heading;
            $update_about_banner_detail->save();
        }
        else{

            $update_about_banner_detail = FooterAbout::find($id);
            $update_about_banner_detail->banner_heading = $banner_heading;
            $update_about_banner_detail->save();
        }
      
        $about_id = $update_about_banner_detail->id;

        if(!empty($about_id)){

                $udate_name = FooterAboutDetail::find($id);
                $udate_name->banner_id = $about_id ;
                $udate_name->title = $title;
                $udate_name->description = $description;
                $udate_name->protected_area_heading = $protected_area_heading;
                $udate_name->protected_area_description = $protected_area_description;
                $udate_name->save();
        }

        return redirect('/about_us_detail')->with('success','Footer About Detail Updated Successfully');
    }

    public function delete_about_us($id){

        $delete_about_banner_detail = FooterAbout::find($id);
        $delete_about_banner_detail->deleted_status = '1';
        $delete_about_banner_detail->save();
         return redirect('/about_us_detail')->with('success','Footer About Detail Deleted Successfully');
    }
    // Policy guideline
    public function policyguidelineList(){

        $policy_list = FooterPolicyGuideline::where('deleted_status','0')->paginate(10);
        return view('footer.policy.listing',compact('policy_list'));
    }

    public function addpolicyguideline(){

        return view('footer.policy.add');        
    }

    public function savepolicyguideline(Request $request){

        $validated = $request->validate([
            'policy_guideline_pdf' => 'required|mimes:pdf'
        ]);

        $policy_guideline_pdf = $request->file('policy_guideline_pdf');

        if($request->hasFile('policy_guideline_pdf') != ""){
            $filename =$policy_guideline_pdf->getClientOriginalName();
            $destinationPath = public_path('/footer_about_policypdf');
            $policy_guideline_pdf->move($destinationPath, $filename);
            $policy_guideline_pdf = '/footer_about_policypdf/' . $filename;
        }

        $save_file = new FooterPolicyGuideline();
        $save_file->policy_guideline = $policy_guideline_pdf;
        $save_file->save();

        return redirect('/policy_guideline')->with('success','File Uploaded Successfully');

    }

    public function editpolicyguideline($id){

        $edit_policy = FooterPolicyGuideline::where('id',$id)->first();
        return view('footer.policy.edit',compact('edit_policy'));
    }

    public function updatepolicyguideline(Request $request , $id){
    	
    	 $validated = $request->validate([
            'policy_guideline_pdf' => 'mimes:pdf'
        ]);

          $policy_guideline_pdf = $request->file('policy_guideline_pdf');

        if($request->hasFile('policy_guideline_pdf') != ""){
            $filename =$policy_guideline_pdf->getClientOriginalName();
            $destinationPath = public_path('/footer_about_policypdf');
            $policy_guideline_pdf->move($destinationPath, $filename);
            $policy_guideline_pdf = '/footer_about_policypdf/' . $filename;
            $update_file = FooterPolicyGuideline::find($id);
            $update_file->policy_guideline = $policy_guideline_pdf;
            $update_file->save();
        }

        

        return redirect('/policy_guideline')->with('success','File Updated Successfully');        
    }

    public function deletepolicyguideline($id){

        $save_file = FooterPolicyGuideline::find($id);
        $save_file->deleted_status = '1';
        $save_file->save();

        return redirect('/policy_guideline')->with('success','File Delete Successfully'); 
    }


    public function notificationDetail(){
        $notification_detail = NotificationBannerDetail::with('get_details')->where('deleted_status','0')->paginate(10);
        return view('footer.notification.listing',compact('notification_detail'));
    }

    public function addNotification(){

        return view('footer.notification.add');
    }

    public function saveNotification(Request $request){
	
        $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'name' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'pdf_url' => 'required|mimes:pdf',
        ]);

        $banner_heading = $request->input('banner_heading');
        $banner_image = $request->file('banner_image');
        $name = $request->input('name');
        $pdf_url = $request->file('pdf_url');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/notification');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/notification/' . $filename;
        }

        $save_detail = new NotificationBannerDetail();
        $save_detail->banner_heading = $banner_heading;
        $save_detail->banner_image = $banner_image;
        $save_detail->save();
        $id = $save_detail->id;

        $image_data=array();
        if($request->hasFile('pdf_url') != "")
        {
            foreach($request->file('pdf_url') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
                $file->move(public_path() . '/notification/', $image);
                $image_data[] = '/notification/'.$image;
            }
        }

        for($i=0;$i<count($name);$i++)
        {
            $save_notification_detail = new NotificationDetail();
            $save_notification_detail->banner_id = $id;
            $save_notification_detail->name=$name[$i];
            $save_notification_detail->pdf_url=$image_data[$i];
            $save_notification_detail->save();
        }
        return redirect('/notification_detail')->with('success','Added Successfully');
    }

    public function editNotification($id){

        $edit_notification = NotificationBannerDetail::where('id',$id)->first();
        return view('footer.notification.edit',compact('edit_notification'));
    }

    public function updateNotification(Request $request ,$id){
    
    	  $validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
          	'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);


        $banner_heading = $request->input('banner_heading');
        $banner_image = $request->file('banner_image');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/notification');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/notification/' . $filename;

            $update_detail = NotificationBannerDetail::find($id);
            $update_detail->banner_image = $banner_image;
            $update_detail->save();
        }
        else{
            $update_detail = NotificationBannerDetail::find($id);
            $update_detail->banner_heading = $banner_heading;
            $update_detail->save();
        }
        return redirect('/notification_detail')->with('success','Detail Updated Successfully');
    }

    public function deleteNotification($id){
         
        $delete_detail = NotificationBannerDetail::find($id);
        $delete_detail->deleted_status = '1';
        $delete_detail->save();
         return redirect('/notification_detail')->with('success','Detail Deleted Successfully');

    }

    // tender detail add 
    public function tenderDetail(){

       $tender_list = TenderBannerDetail::with('get_tender_detail')->where('deleted_status','0')->paginate(10);
        // $tender_list = TenderBannerDetail::where('deleted_status','0')->get();
        return view('footer.tender.listing',compact('tender_list'));
    }

    public function addtenderDetail(){

        return view('footer.tender.add');
    }

    public function savetenderDetail(Request $request){
    	
    
         $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tender/' . $filename;
        }

        $save_tenderDetail = new TenderBannerDetail();
        $save_tenderDetail->banner_image = $banner_image;
        $save_tenderDetail->banner_heading = $banner_heading;
        $save_tenderDetail->save();

        return redirect('/tender_detail')->with('success','Tender Banner Detail Added Successfully');
    }

    public function addTender($id){

        $tender_types = TenderTypes::all();
        $banner_detail = TenderBannerDetail::where('id',$id)->first();
        return view('footer.tender.add_tender_detail',compact('tender_types','banner_detail'));
    }

    public function saveTender(Request $request ,$id){

        // return $request->all();

         $validated = $request->validate([
            'tender_type_id' => 'required',
            'text' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'tender_pdf' => 'required|mimes:pdf',
        ]);

        $tender_type_id = $request->input('tender_type_id');
        $text = $request->input('text');
        $tender_pdf = $request->file('tender_pdf');

        

        if($request->hasFile('tender_pdf') != ""){
            $filename =$tender_pdf->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $tender_pdf->move($destinationPath, $filename);
            $tender_pdf = '/tender/' . $filename;
        }

        $type_name = TenderTypes::where('id',$tender_type_id)->first();
       $tender_type_name =  $type_name->type_name;
        $add_tenderDetail = new TenderDetails();
        $add_tenderDetail->tender_type_id = $tender_type_id;
        $add_tenderDetail->tender_type_name =  $tender_type_name;
        $add_tenderDetail->banner_id = $id;
        $add_tenderDetail->text = $text;
        $add_tenderDetail->tender_pdf = $tender_pdf;
        $add_tenderDetail->save();

        return redirect('/tender_detail')->with('success','Tender Detail Added Successfully');
    }

    public function editTender($id){

        $edit_tender = TenderBannerDetail::where('id',$id)->first();

        return view('footer.tender.edit',compact('edit_tender'));
    }

    public function updateTender(Request $request , $id){
	
		$validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|size:2048',
        	'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tender/' . $filename;
            $update_tenderDetail = TenderBannerDetail::find($id);
            $update_tenderDetail->banner_image = $banner_image;
            $update_tenderDetail->save();
        }
        else{

            $update_tenderDetail = TenderBannerDetail::find($id);
            $update_tenderDetail->banner_heading = $banner_heading;
            $update_tenderDetail->save();
        }
    

        return redirect('/tender_detail')->with('success','Tender Banner Detail Updated Successfully');
    }

    public function deleteTender($id){

        $delete_tenderDetail = TenderBannerDetail::find($id);
        $delete_tenderDetail->deleted_status = '1';
        $delete_tenderDetail->save();
        return redirect('/tender_detail')->with('success','Tender Banner Detail Deleted Successfully');
    }

    public function contactUslist(){

        $contact_us_banner_detail =  ContactUsBannerDetail::all();
        return view('footer.contactUs.list',compact('contact_us_banner_detail'));
    }

    public function addcontactBanner(){

        return view('footer.contactUs.add_banner');
    }

    public function savecontactBanner(Request $request){
    
    	  $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|size:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');


        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/contactUs');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/contactUs/' . $filename;
        }

        $save_banner_detail = new ContactUsBannerDetail();
        $save_banner_detail->banner_image = $banner_image;
        $save_banner_detail->banner_heading = $banner_heading;
        $save_banner_detail->save();

        return redirect('/contact_us')->with('success','Contact Us Banner Detail Successfully');
    }

    public function addContactDetail(){

        $contactus_detail = ContactUs::all();
        return view('footer.contactUs.add_contactdetail',compact('contactus_detail'));
    }
    public function saveContactDetail(Request $request){
    
    	
    	  $validated = $request->validate([
            'image_icon' => 'required|mimes:jpg,png,jpeg|max:2048',
            'content_type_id' => 'required',
            'description' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $content_type_id = $request->content_type_id;
        $image_icon = $request->image_icon;
        $description = $request->description;

        if($request->hasFile('image_icon') != ""){
            $filename =$image_icon->getClientOriginalName();
            $destinationPath = public_path('/contactUs');
            $image_icon->move($destinationPath, $filename);
            $image_icon = '/contactUs/' . $filename;
        }
         $name_content = ContactUs::where('id',$content_type_id)->first();
          $name = $name_content->category_name;
        $save_new_detail = new ContactUsDetail();
        $save_new_detail->content_type_id = $content_type_id;
        $save_new_detail->content_type_name = $name;
        $save_new_detail->image_icon = $image_icon;
        $save_new_detail->description = $description;
        $save_new_detail->save();

        return redirect('/contact_us')->with('success','Contact Us Detail Successfully');
    }

    public function impList(){
        $impList =  ImpLinkDetail::where('deleted_status','0')->paginate(10);
        return view('impLink.list',compact('impList'));
    }

    public function addImpLink(){

        return view('impLink.add');
    }

    public function saveImpLink(Request $request){

        $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/impLink');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/impLink/' . $filename;
        }

        $saveimpLink = new ImpLinkDetail();
        $saveimpLink->banner_image = $banner_image; 
        $saveimpLink->banner_heading = $banner_heading; 
        $saveimpLink->save(); 

        return redirect('/imp_list')->with('success','Implink Detail Added Successfully');
    }

    public function editImpLink($id){

        $edit_imp = ImpLinkDetail::where('id',$id)->first();
        return view('impLink.edit',compact('edit_imp'));
    }

    public function updateImpLink(Request $request ,$id){
		
    	$validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        	'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);
    
        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/impLink');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/impLink/' . $filename;
            $saveimpLink = ImpLinkDetail::find($id);
            $saveimpLink->banner_image = $banner_image; 
            $saveimpLink->save(); 

        }
        else{

            $saveimpLink = ImpLinkDetail::find($id);
            $saveimpLink->banner_heading = $banner_heading; 
            $saveimpLink->save(); 
        }
       

        return redirect('/imp_list')->with('success','Implink Detail Updated Successfully');
    }

    public function deleteImpLink($id){

        $saveimpLink = ImpLinkDetail::find($id);
        $saveimpLink->deleted_status = '1'; 
        $saveimpLink->save(); 

        return redirect('/imp_list')->with('success','Implink Detail Deleted Successfully');
    }

    public function servicesList(){

        $services_list  = ServicesDetail::where('deleted_status','0')->paginate(10);
        return view('services.list',compact('services_list'));        
    }

    public function addservices(){

        return view('services.add');
    }

    public function saveservices(Request $request){

        $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'title' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'description' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);


        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');
        $title = $request->input('title');
        $description = $request->input('description');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/services');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/services/' . $filename;
        }

        $save_services = new ServicesDetail();
        $save_services->banner_image = $banner_image;
        $save_services->banner_heading = $banner_heading;
        $save_services->title = $title;
        $save_services->description = $description;
        $save_services->save();

        return redirect('/services_list')->with('success','Services Added Successfully');
    }

    public function editservices($id){

        $edit_services = ServicesDetail::where('id',$id)->first();

        return view('services.edit',compact('edit_services'));
    }

    public function updateservices(Request $request, $id){

		$validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            'title' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            // 'description' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);
        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');
        $title = $request->input('title');
        $description = $request->input('description');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/services');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/services/' . $filename;
            
            $update_services = ServicesDetail::find($id);
            $update_services->banner_image = $banner_image;
            $update_services->save();
        }
        else{

            $update_services = ServicesDetail::find($id);
            $update_services->banner_heading = $banner_heading;
            $update_services->title = $title;
            $update_services->description = $description;
            $update_services->save();

        }
      

        return redirect('/services_list')->with('success','Services Updated Successfully');
    }

    public function deleteservices($id){

        $delete_services = ServicesDetail::find($id);
        $delete_services->deleted_status = '1';
        $delete_services->save();

        return redirect('/services_list')->with('success','Services Deleted Successfully');
    }
// end class 
}