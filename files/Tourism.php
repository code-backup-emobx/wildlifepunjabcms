<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\TourismBannerDetail;
use App\Models\TourismData;
use App\Models\TourismTypes;
use App\Models\TourismLinkDetail;
use App\Models\TourismLinkImages;
use DB;

class Tourism extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function tourismList(){

    	 $tourism_detail = TourismBannerDetail::with('get_bannerdata','get_bannerdata.get_tourism_type')->where('deleted_status','0')->get();
    	return view('tourism.listing',compact('tourism_detail'));
    }

    public function addtourism(){
    	$tourism_type = TourismTypes::all();
    	return view('tourism.add',compact('tourism_type'));
    }

    public function savetourism(Request $request){
    
    	$validated = $request->validate([
        	'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        	'image' => 'required|mimes:jpg,png,jpeg|max:2048',
        	'tourism_type_id' => 'required',
        	'time' => 'required',
        	'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
    	]);	

    	$banner_image = $request->file('banner_image');
    	$banner_heading = $request->input('banner_heading');
    	$tourism_type_id = $request->tourism_type_id;
    	$image = $request->file('image');
    	$time = $request->input('time');


    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
		}

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $image->move($destinationPath, $filename);
            $image = '/tourism/' . $filename;
		}

		$save_banner = new TourismBannerDetail();
		$save_banner->banner_image = $banner_image;
		$save_banner->banner_heading = $banner_heading ;
		$save_banner->save();
		$id = $save_banner->id;

		$save_tourism_data = new TourismData();
		$save_tourism_data->banner_id = $id;
		$save_tourism_data->tourism_type_id = $tourism_type_id;
		$save_tourism_data->image = $image;
		$save_tourism_data->time = $time;
		$save_tourism_data->save();

		return redirect('/tourism_detail')->with('success','Tourism Detail Added Successfully');
    }

    public function edittourism($id){
    	$tourism_type = TourismTypes::all();
      $edit_tourism = TourismBannerDetail::with('get_bannerdata','get_bannerdata.get_tourism_type')->where('id',$id)->first();
    	return view('tourism.edit',compact('edit_tourism','tourism_type'));
    }
	
	public function updateTourism(Request $request , $id){
    // return $request->all();die;

    	$validated = $request->validate([
        	'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        	'image' => 'mimes:jpg,png,jpeg|max:2048',
           // 'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
    	]);	

    	$banner_image = $request->file('banner_image');
    	$banner_heading = $request->banner_heading;
    	$tourism_type_id = $request->tourism_type_id;
    	$image = $request->file('image');
    	$time = $request->input('time');

     
    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
        
        	$update_banner = TourismBannerDetail::find($id);
			    $update_banner->banner_image = $banner_image;
        	$update_banner->save();
		}

		$update_banner = TourismBannerDetail::find($id);
		$update_banner->banner_heading = $banner_heading ;
		$update_banner->save();
		$b_id = $update_banner->id;
    
    		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $image->move($destinationPath, $filename);
            $image = '/tourism/' . $filename;
            // $update_tourism_data = TourismData::find($b_id);
            // $update_tourism_data->image = $image;
            // $update_tourism_data->save();
            $update_tourism_data = TourismData::where('banner_id',$b_id)->update(["image"=>$image]);
		}
		else{
        	// $update_tourism_data = TourismData::find($b_id);
        	// // $update_tourism_data->banner_id = $b_id;
        	// $update_tourism_data->tourism_type_id = $tourism_type_id;
        	// $update_tourism_data->image = $image;
        	// $update_tourism_data->time = $time;
        	// $update_tourism_data->save();
       $update_tourism_data = TourismData::where('banner_id',$b_id)->update(["tourism_type_id"=>$tourism_type_id,"time"=>$time]);
        }

     
		return redirect('/tourism_detail')->with('success','Tourism Detail Added Successfully');
    }

    public function addtourismgallery($id){
        $t_id = TourismData::with('get_bdata','get_tourism_type')->where('id',$id)->first();
        $tourism_type = TourismTypes::all();
        return view('tourism.addgallery',compact('tourism_type','t_id'));
    }

    public function savetourismgallery(Request $request,$id){
    
      // return $request->all();
    
    	$validated = $request->validate([
        	'tourism_type_id' => 'required',
        	'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        	'banner_heading' => 'required',
        	'description_heading' => 'required',
        	'description' => 'required',
        	'heading_how_to_reach' => 'required',
        	'detail_how_to_reach' => 'required',
        	'heading_permission_required' => 'required',
        	'detail_required_permmision' => 'required',
        	'heading_hire_a_guide' => 'required',
        	'descripton_hire_guide' => 'required',
        	'heading_accommodation_options' => 'required',
        	'detail_accommodation_options' => 'required',
        	'heading_do' => 'required',
        	'detail_do' => 'required',
        	'heading_dont' => 'required',
        	'detail_dont' => 'required',
        	'heading_best_time_visit' => 'required',
        	'detail_best_time_visit' => 'required',
        	'images_heading' => 'required',
        	'image' => 'required',
    	]);	
    
       $tourism_type_id = $request->input('tourism_type_id');
       $banner_image = $request->file('banner_image');
       $banner_heading = $request->input('banner_heading');
       $description_heading = $request->input('description_heading');
       $description = $request->input('description');

       $heading_how_to_reach = $request->input('heading_how_to_reach');
       $detail_how_to_reach = $request->input('detail_how_to_reach');

       $heading_permission_required = $request->input('heading_permission_required');
       $detail_required_permmision = $request->input('detail_required_permmision');

       $heading_hire_a_guide = $request->input('heading_hire_a_guide');
       $descripton_hire_guide = $request->input('descripton_hire_guide');

       $heading_accommodation_options = $request->input('heading_accommodation_options');
       $detail_accommodation_options = $request->input('detail_accommodation_options');

       $heading_do = $request->input('heading_do');
       $detail_do = $request->input('detail_do');

       $heading_dont = $request->input('heading_dont');
       $detail_dont = $request->input('detail_dont');

       $heading_best_time_visit = $request->input('heading_best_time_visit');
       $detail_best_time_visit = $request->input('detail_best_time_visit');

       $images_heading = $request->input('images_heading');
       $image = $request->file('image');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
        }

       $save_detail = new TourismLinkDetail();
       $save_detail->tourism_type_id = $tourism_type_id;
       $save_detail->banner_image = $banner_image;
       $save_detail->banner_heading = $banner_heading;
       $save_detail->description_heading = $description_heading;
       $save_detail->description = $description;

       $save_detail->heading_how_to_reach = $heading_how_to_reach;
       $save_detail->detail_how_to_reach = $detail_how_to_reach;

       $save_detail->heading_permission_required = $heading_permission_required;
       $save_detail->detail_required_permmision = $detail_required_permmision;

       $save_detail->heading_hire_a_guide = $heading_hire_a_guide;
       $save_detail->descripton_hire_guide = $descripton_hire_guide;

       $save_detail->heading_accommodation_options = $heading_accommodation_options;
       $save_detail->detail_accommodation_options = $detail_accommodation_options;

       $save_detail->heading_do = $heading_do;
       $save_detail->detail_do = $detail_do;

       $save_detail->heading_dont = $heading_dont;
       $save_detail->detail_dont = $detail_dont;

        $save_detail->heading_best_time_visit = $heading_best_time_visit;
       $save_detail->detail_best_time_visit = $detail_best_time_visit;

       $save_detail->images_heading = $images_heading;
       $save_detail->save();
       $id = $save_detail->id;

        $image_data=array();
        if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = $file->getClientOriginalName();
                $file->move(public_path() . '/tourism/', $image);
                $image_data[] = '/tourism/'.$image;
            }
        }

        for($i=0;$i<count($image_data);$i++)
        {
            
            $save_images = new TourismLinkImages();
            $save_images->tourism_link_id = $id; 
            $save_images->tourism_type_id = $tourism_type_id ;
            $save_images->image = $image_data[$i];
            $save_images->save();
        }

       return redirect('/tourism_detail')->with('success','Gallery Images Added Successfully');
      
   }

   public function deletetourismgallery($id){

          $delete_tourism_detail = TourismBannerDetail::find($id);
          $delete_tourism_detail->deleted_status = '1';
          $delete_tourism_detail->save();
        return redirect('/tourism_detail')->with('success','Tourism Detail Deleted Successfully');
   }

   public function editTourismDetail($id){

      $tourismDetail = TourismLinkDetail::where('tourism_type_id',$id)->where('deleted_status','0')->first();
    
     return view('tourism.editgalleryDetail',compact('tourismDetail'));
   }

   public function updateTourismDetail(Request $request , $id){

       // return $request->all();
   
   		$validated = $request->validate([
        	'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);		
   
       $tourism_type_id = $request->input('tourism_type_id');
       $banner_image = $request->file('banner_image');
       $banner_heading = $request->input('banner_heading');
       $description_heading = $request->input('description_heading');
       $description = $request->input('description');

       $heading_how_to_reach = $request->input('heading_how_to_reach');
       $detail_how_to_reach = $request->input('detail_how_to_reach');

       $heading_permission_required = $request->input('heading_permission_required');
       $detail_required_permmision = $request->input('detail_required_permmision');

       $heading_hire_a_guide = $request->input('heading_hire_a_guide');
       $descripton_hire_guide = $request->input('descripton_hire_guide');

       $heading_accommodation_options = $request->input('heading_accommodation_options');
       $detail_accommodation_options = $request->input('detail_accommodation_options');

       $heading_do = $request->input('heading_do');
       $detail_do = $request->input('detail_do');

       $heading_dont = $request->input('heading_dont');
       $detail_dont = $request->input('detail_dont');

       $heading_best_time_visit = $request->input('heading_best_time_visit');
       $detail_best_time_visit = $request->input('detail_best_time_visit');

      

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
            $edit_detail = TourismLinkDetail::find($id);
            $edit_detail->banner_image = $banner_image;
            $edit_detail->save();
        }
        else{

          $edit_detail = TourismLinkDetail::find($id);
       $edit_detail->banner_heading = $banner_heading;
       $edit_detail->description_heading = $description_heading;
       $edit_detail->description = $description;

       $edit_detail->heading_how_to_reach = $heading_how_to_reach;
       $edit_detail->detail_how_to_reach = $detail_how_to_reach;

       $edit_detail->heading_permission_required = $heading_permission_required;
       $edit_detail->detail_required_permmision = $detail_required_permmision;

       $edit_detail->heading_hire_a_guide = $heading_hire_a_guide;
       $edit_detail->descripton_hire_guide = $descripton_hire_guide;

       $edit_detail->heading_accommodation_options = $heading_accommodation_options;
       $edit_detail->detail_accommodation_options = $detail_accommodation_options;

       $edit_detail->heading_do = $heading_do;
       $edit_detail->detail_do = $detail_do;

       $edit_detail->heading_dont = $heading_dont;
       $edit_detail->detail_dont = $detail_dont;

        $edit_detail->heading_best_time_visit = $heading_best_time_visit;
       $edit_detail->detail_best_time_visit = $detail_best_time_visit;

       
       $edit_detail->save();

        }
       
         return redirect('view_detail/'.$id)->with('success','Tourism Detail Updated Successfully');
   }

   public function tourismDetail($id){
     $tourismDetaillist = TourismLinkDetail::where('tourism_type_id',$id)->where('deleted_status','0')->first();
     return view('tourism.detail_listing',compact('tourismDetaillist'));
   }

   public function deletetourismDetail($id){

     $delete_detail = TourismLinkDetail::find($id);
     $delete_detail->deleted_status = '1';
     $delete_detail->save();
     return redirect()->back()->with('success','Delete Successfully');
   }
// end class 
}