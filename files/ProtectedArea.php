<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\BirMotiBagh;
use App\Models\BirGurdialpur;
use App\Models\BirBhunerheri;

use App\Models\BirMihas;
use App\Models\BirDosanjh;
use App\Models\BirBhadson;
use App\Models\BirAishwan;
use App\Models\BirAbohar;
use App\Models\BirHarike;

use App\Models\BirTakhniRehmpur;
use App\Models\BirJhajjarbachauli;
use App\Models\BirKathlaurKushlian;
use App\Models\BirNangalWildlife;

use App\Models\BirMotiBaghNotificationDetail;
use App\Models\BirGurdialpurNotificationDetail;
use App\Models\BirBhunerheriNotificationDetail;
use App\Models\BirMehasNotificationDetail;
use App\Models\BirDosanjhNotificationDetail;
use App\Models\BirBhadsonNotificationDetail;
use App\Models\BirAishwanNotificationDetail;
use App\Models\BirAboharNotificationDetail;
use App\Models\BirHarikeNotificationDetail;
use App\Models\BirTakhnipurNotificationDetail;
use App\Models\BirJhajjarBachauliNotificationDetail;
use App\Models\BirKathlaurKushlianNotificationDetail;
use App\Models\BirNangalNotificationDetail;



class ProtectedArea extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function birMotiBghList(){

		$birmotibagh_list = BirMotiBagh::with('get_notification_detail')->where('deleted_status','0')->paginate(10);

		foreach($birmotibagh_list as $list){

			$list->text_count = BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$list->id)->count();

			$list->pdf_count = BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$list->id)->count();
		}
		// return $birmotibagh_list;
	    return view('protectedArea.wildlifeSantuaries.birmotiBagh.listing',compact('birmotibagh_list'));
	}
	
	public function addBirmotibagh(){

		return view('protectedArea.wildlifeSantuaries.birmotiBagh.add');
	}
	
	public function saveBirmotibagh(Request $request){

		// return $request->all();

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required|regex:/^[\.a-zA-Z0-9,!?– ]*$/',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/birmotiBagh');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/birmotiBagh/' . $filename;
		}

		$save_birmotibagh = new BirMotiBagh();
		$save_birmotibagh->banner_image = $banner_image;
		$save_birmotibagh->banner_heading = $banner_heading;
		$save_birmotibagh->description = $description;
		$save_birmotibagh->wildlife_heading = $wildlife_heading;
		$save_birmotibagh->wildlife_title = $wildlife_title;
		$save_birmotibagh->district = $district;
		$save_birmotibagh->location = $location;
		$save_birmotibagh->area = $area;
		$save_birmotibagh->status_of_land = $status_of_land;
		// $save_birmotibagh->notification_detail = $notification_detail;
		$save_birmotibagh->important_fauna = $important_fauna;
		$save_birmotibagh->important_flora = $important_flora;
		$save_birmotibagh->save();

		$id = $save_birmotibagh->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/notification/', $image);
				$image_data[] = '/notification/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirMotiBaghNotificationDetail();
			$save_subheading->bir_motibagh_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_motibagh_pa')->with('success','Bir Moti Bagh Detail Added Successfully');
	}
	
	public function editBirmotibagh($id){

		$edit_bir_motibagh = BirMotiBagh::with('get_notification_detail')->where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.birmotiBagh.edit',compact('edit_bir_motibagh'));
	}

	public function updateBirmotibagh(Request $request , $id){
    
		// return $request->all();
    	
		$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
        // 'description' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        'wildlife_heading' => 'regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
        // 'location' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        'wildlife_title' => 'regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
        'district' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        // 'area' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        'status_of_land' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        // 'notification_detail' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        // 'important_fauna' => 'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        // 'important_flora' =>'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
    	]);	
    

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->notificationtitle;
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');
		$pdf_file = $request->file('pdf_file');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/birmotiBagh');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/birmotiBagh/' . $filename;
            $update_birmotibagh = BirMotiBagh::find($id);
			$update_birmotibagh->banner_image = $banner_image;
			$update_birmotibagh->banner_heading = $banner_heading;
			$update_birmotibagh->description = $description;
			$update_birmotibagh->wildlife_heading = $wildlife_heading;
			$update_birmotibagh->wildlife_title = $wildlife_title;
			$update_birmotibagh->district = $district;
			$update_birmotibagh->location = $location;
			$update_birmotibagh->area = $area;
			$update_birmotibagh->status_of_land = $status_of_land;
			// $update_birmotibagh->notification_detail = $notification_detail;
			$update_birmotibagh->important_fauna = $important_fauna;
			$update_birmotibagh->important_flora = $important_flora;
			$update_birmotibagh->save();

			$id = $update_birmotibagh->id;

			$pdf_data=array();

			if($request->hasFile('pdf_file') != "")
	        {
	            foreach($request->file('pdf_file') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $image = time() . '.' . $file->getClientOriginalName();
					$file->move(public_path() . '/notification/', $image);
					$pdf_data[] = '/notification/'.$image;
	            }
			}
			for($i=0;$i<count($notificationtitle);$i++){

				BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$id)->update(["bir_moti_bagh_id"=>$id,"notification_title"=> $notificationtitle[$i]]);

				for($i=0;$i<count($pdf_data);$i++)
	        {
				
				BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$id)->update(["bir_moti_bagh_id"=>$id,"notification_title"=> $notificationtitle[$i],"pdf_url"=>$pdf_data[$i]]);
		    }

			}
			

            
		}
		else{
			$update_birmotibagh = BirMotiBagh::find($id);
			$update_birmotibagh->banner_heading = $banner_heading;
			$update_birmotibagh->description = $description;
			$update_birmotibagh->wildlife_heading = $wildlife_heading;
			$update_birmotibagh->wildlife_title = $wildlife_title;
			$update_birmotibagh->district = $district;
			$update_birmotibagh->location = $location;
			$update_birmotibagh->area = $area;
			$update_birmotibagh->status_of_land = $status_of_land;
			// $update_birmotibagh->notification_detail = $notification_detail;
			$update_birmotibagh->important_fauna = $important_fauna;
			$update_birmotibagh->important_flora = $important_flora;
			$update_birmotibagh->save();

			$id = $update_birmotibagh->id;

			$pdf_data=array();
			if($request->hasFile('pdf_file') != "")
	        {
	            foreach($request->file('pdf_file') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $image = time() . '.' . $file->getClientOriginalName();
					$file->move(public_path() . '/notification/', $image);
					$pdf_data[] = '/notification/'.$image;
	            }
			}
			for($i=0;$i<count($notificationtitle);$i++){

				BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$id)->update(["bir_moti_bagh_id"=>$id,"notification_title"=> $notificationtitle[$i]]);

				for($i=0;$i<count($pdf_data);$i++)
	        	{
					BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$id)->update(["bir_moti_bagh_id"=>$id,"notification_title"=> $notificationtitle[$i],"pdf_url"=>$pdf_data[$i]]);
		    	}
			}
			
		   
		}
	

		return redirect('/bir_motibagh_pa')->with('success','Bir Moti Bagh Detail Updated Successfully');

	}

	public function deleteBirmotibagh($id){

		$delete_birmotibagh = BirMotiBagh::find($id);
		$delete_birmotibagh->deleted_status = '1';
		$delete_birmotibagh->save();
		return redirect('/bir_motibagh_pa')->with('success','Bir Moti Bagh Detail Deleted Successfully');;
	}

	// notification text
	public function notificationtextListing($id){

		$notification_list = BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.birmotiBagh.notification_list',compact('notification_list','id'));
	}

	public function deleteListing($id){

		$delete_listing = BirMotiBaghNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function editNotificationText($id){

		$edit_notification = BirMotiBaghNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.birmotiBagh.edit_notification',compact('edit_notification'));
	}

	public function updateNotificationText(Request $request , $id){

		$update_notification = BirMotiBaghNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/notification_list/'.$update_notification->bir_moti_bagh_id)->with('success','Notification Text updated successfully');
	}

	public function addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.birmotiBagh.add_new_notification',compact('id'));		 
	}

	public function saveNewNotificationText(Request $request , $id){

		$save_notification = new BirMotiBaghNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_moti_bagh_id = $id;
		$save_notification->save();

		return redirect('/notification_list/'.$save_notification->bir_moti_bagh_id)->with('success','Notification Text Save successfully');
	}

	public function notification_pdflist($id){

		$notification_pdf_list = BirMotiBaghNotificationDetail::where('bir_moti_bagh_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.birmotiBagh.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function edit_notification_pdf($id){

		$edit = BirMotiBaghNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.birmotiBagh.edit_notification_pdf',compact('edit','id'));
	}

	public function update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirMotiBaghNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/dosanjh/', $image);
				$pdf_url = '/dosanjh/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

	           return redirect('notification_pdf/'.$update_pdf->bir_moti_bagh_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('notification_pdf/'.$update_pdf->bir_moti_bagh_id)->with('success','Notification pdf updated successfully');
			}

			// return redirect('/notification_pdf/'.$update_pdf->bir_moti_bagh_id)->with('success','Notification Text updated successfully');
	}

	public function delete_notification_pdf($id){

		$delete_pdf = BirMotiBaghNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function bir_gurdiaplurpaList(){

		$gurdialpur_list = BirGurdialpur::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($gurdialpur_list as $list){

			$list->text_count = BirGurdialpurNotificationDetail::where('bir_gurdialpur_id',$list->id)->count();

			$list->pdf_count = BirGurdialpurNotificationDetail::where('bir_gurdialpur_id',$list->id)->count();
		}
		// return $gurdialpur_list;
		return view('protectedArea.wildlifeSantuaries.birgurdialpur.listing',compact('gurdialpur_list'));
	}

	public function addbir_gurdiaplurpa(){

		return view('protectedArea.wildlifeSantuaries.birgurdialpur.add');
	}

	public function savebir_gurdiaplurpa(Request $request){

		// return $request->all();

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/gurdialpur');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/gurdialpur/' . $filename;
		}

		$save_gurdiaplur = new BirGurdialpur();
		$save_gurdiaplur->banner_image = $banner_image;
		$save_gurdiaplur->banner_heading = $banner_heading;
		$save_gurdiaplur->description = $description;
		$save_gurdiaplur->wildlife_heading = $wildlife_heading;
		$save_gurdiaplur->wildlife_title = $wildlife_title;
		$save_gurdiaplur->district = $district;
		$save_gurdiaplur->location = $location;
		$save_gurdiaplur->area = $area;
		$save_gurdiaplur->status_of_land = $status_of_land;
		// $save_gurdiaplur->notification_detail = $notification_detail;
		$save_gurdiaplur->important_fauna = $important_fauna;
		$save_gurdiaplur->important_flora = $important_flora;
		$save_gurdiaplur->save();

		$id = $save_gurdiaplur->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/gurdialpur/', $image);
				$image_data[] = '/gurdialpur/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirGurdialpurNotificationDetail();
			$save_subheading->bir_gurdialpur_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_gurdiaplur_pa')->with('success','Bir Gurdialpur Detail Added Successfully');
	}

	public function editbir_gurdiaplurpa($id){

		$edit_gurdialpur = BirGurdialpur::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.birgurdialpur.edit',compact('edit_gurdialpur'));
	}
	public function updatebir_gurdiaplurpa(Request $request , $id){
		
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		// return $request->all();
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/gurdialpur');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/gurdialpur/' . $filename;
            $update_gurdiaplur = BirGurdialpur::find($id);
            $update_gurdiaplur->banner_heading = $banner_heading;
			$update_gurdiaplur->banner_image = $banner_image;
			$update_gurdiaplur->description = $description;
			$update_gurdiaplur->wildlife_heading = $wildlife_heading;
			$update_gurdiaplur->wildlife_title = $wildlife_title;
			$update_gurdiaplur->district = $district;
			$update_gurdiaplur->location = $location;
			$update_gurdiaplur->area = $area;
			$update_gurdiaplur->status_of_land = $status_of_land;
			$update_gurdiaplur->notification_detail = $notification_detail;
			$update_gurdiaplur->important_fauna = $important_fauna;
			$update_gurdiaplur->important_flora = $important_flora;
			$update_gurdiaplur->save();
          
		}
		else{
			$update_gurdiaplur = BirGurdialpur::find($id);
			$update_gurdiaplur->banner_heading = $banner_heading;
			$update_gurdiaplur->description = $description;
			$update_gurdiaplur->wildlife_heading = $wildlife_heading;
			$update_gurdiaplur->wildlife_title = $wildlife_title;
			$update_gurdiaplur->district = $district;
			$update_gurdiaplur->location = $location;
			$update_gurdiaplur->area = $area;
			$update_gurdiaplur->status_of_land = $status_of_land;
			$update_gurdiaplur->notification_detail = $notification_detail;
			$update_gurdiaplur->important_fauna = $important_fauna;
			$update_gurdiaplur->important_flora = $important_flora;
			$update_gurdiaplur->save();
		}
			
			
		return redirect('/bir_gurdiaplur_pa')->with('success','Bir Gurdialpur Detail Updated Successfully');
	}

	public function deletebir_gurdiaplurpa($id){

		$delete_gurdiaplur = BirGurdialpur::find($id);
		$delete_gurdiaplur->deleted_status = '1';
		$delete_gurdiaplur->save();
		return redirect('/bir_gurdiaplur_pa')->with('success','Bir Gurdialpur Detail Deleted Successfully');
	}

	// notification text
	public function gur_notificationtextListing($id){

		$notification_list = BirGurdialpurNotificationDetail::where('bir_gurdialpur_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.birgurdialpur.notification_list',compact('notification_list','id'));
	}

	public function gur_deleteListing($id){

		$delete_listing = BirGurdialpurNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function gur_editNotificationText($id){

		$edit_notification = BirGurdialpurNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.birgurdialpur.edit_notification',compact('edit_notification'));
	}

	public function gur_updateNotificationText(Request $request , $id){

		$update_notification = BirGurdialpurNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/gur_notification_list/'.$update_notification->bir_gurdialpur_id)->with('success','Notification Text updated successfully');
	}

	public function gur_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.birgurdialpur.add_new_notification',compact('id'));		 
	}

	public function gur_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirGurdialpurNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_gurdialpur_id = $id;
		$save_notification->save();

		return redirect('/gur_notification_list/'.$save_notification->bir_gurdialpur_id)->with('success','Notification Text Save successfully');
	}

	public function gur_notification_pdflist($id){

		$notification_pdf_list = BirGurdialpurNotificationDetail::where('bir_gurdialpur_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.birgurdialpur.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function gur_edit_notification_pdf($id){

		$edit = BirGurdialpurNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.birgurdialpur.edit_notification_pdf',compact('edit','id'));
	}

	public function gur_update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/gurdialpur/', $image);
				$pdf_url = '/gurdialpur/'.$image;
	           
			}
			$update_pdf = BirGurdialpurNotificationDetail::find($id);
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

			return redirect('/gur_notification_pdf/'.$update_pdf->bir_gurdialpur_id)->with('success','Notification pdf updated successfully');
	}

	public function gur_delete_notification_pdf($id){

		$delete_pdf = BirGurdialpurNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function birBhunerheriList(){

		$birbhunerheri = BirBhunerheri::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($birbhunerheri as $list){

			$list->text_count = BirBhunerheriNotificationDetail::where('bir_bhunerheri_id',$list->id)->count();
			$list->pdf_count = BirBhunerheriNotificationDetail::where('bir_bhunerheri_id',$list->id)->count();
		}
		// return $birbhunerheri;
		return view('protectedArea.wildlifeSantuaries.bhunerheri.listing',compact('birbhunerheri'));
	}

	public function addbirBhunerheri(){

		return view('protectedArea.wildlifeSantuaries.bhunerheri.add');
	}
	public function savebirBhunerheri(Request $request){

		// return $request->all();

		$validated = $request->validate([
	      
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);


		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bhunerheri');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bhunerheri/' . $filename;
		}

		$save_bhunerheri = new BirBhunerheri();
		$save_bhunerheri->banner_image = $banner_image;
		$save_bhunerheri->banner_heading = $banner_heading;
		$save_bhunerheri->description = $description;
		$save_bhunerheri->wildlife_heading = $wildlife_heading;
		$save_bhunerheri->wildlife_title = $wildlife_title;
		$save_bhunerheri->district = $district;
		$save_bhunerheri->location = $location;
		$save_bhunerheri->area = $area;
		$save_bhunerheri->status_of_land = $status_of_land;
		// $save_bhunerheri->notification_detail = $notification_detail;
		$save_bhunerheri->important_fauna = $important_fauna;
		$save_bhunerheri->important_flora = $important_flora;
		$save_bhunerheri->save();

		$id = $save_bhunerheri->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/gurdialpur/', $image);
				$image_data[] = '/gurdialpur/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirBhunerheriNotificationDetail();
			$save_subheading->bir_bhunerheri_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_bhunerheri_pa')->with('success','Bir Bhunerheri Detail Added Successfully');

	}

	public function editbirBhunerheri($id){

		$editbirBhunerheri = BirBhunerheri::with('')->where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.bhunerheri.edit',compact('editbirBhunerheri'));
	}

	public function updatebirBhunerheri(Request $request ,$id){

		// return $request->all();
		
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);		
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bhunerheri');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bhunerheri/' . $filename;

            $update_bhunerheri = BirBhunerheri::find($id);
			$update_bhunerheri->banner_image = $banner_image;
			$update_bhunerheri->banner_heading = $banner_heading;
			$update_bhunerheri->description = $description;
			$update_bhunerheri->wildlife_heading = $wildlife_heading;
			$update_bhunerheri->wildlife_title = $wildlife_title;
			$update_bhunerheri->district = $district;
			$update_bhunerheri->location = $location;
			$update_bhunerheri->area = $area;
			$update_bhunerheri->status_of_land = $status_of_land;
			$update_bhunerheri->notification_detail = $notification_detail;
			$update_bhunerheri->important_fauna = $important_fauna;
			$update_bhunerheri->important_flora = $important_flora;
			$update_bhunerheri->save();
		}
		else{
			$update_bhunerheri = BirBhunerheri::find($id);
			$update_bhunerheri->banner_heading = $banner_heading;
			$update_bhunerheri->description = $description;
			$update_bhunerheri->wildlife_heading = $wildlife_heading;
			$update_bhunerheri->wildlife_title = $wildlife_title;
			$update_bhunerheri->district = $district;
			$update_bhunerheri->location = $location;
			$update_bhunerheri->area = $area;
			$update_bhunerheri->status_of_land = $status_of_land;
			$update_bhunerheri->notification_detail = $notification_detail;
			$update_bhunerheri->important_fauna = $important_fauna;
			$update_bhunerheri->important_flora = $important_flora;
			$update_bhunerheri->save();
		}
		
		return redirect('/bir_bhunerheri_pa')->with('success','Bir Bhunerheri Detail Added Successfully');
	}

	public function deletebirBhunerheri($id){

		$delete_bir_bhunerheri = BirBhunerheri::find($id);
		$delete_bir_bhunerheri->deleted_status = '1';
		$delete_bir_bhunerheri->save();
		return redirect('/bir_bhunerheri_pa')->with('success','Bir Bhunerheri Detail Added Successfully');
	}


	// notification text
	public function bhunerheri_notificationtextListing($id){

		$notification_list = BirBhunerheriNotificationDetail::where('bir_bhunerheri_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.bhunerheri.notification_list',compact('notification_list','id'));
	}

	public function bhunerheri_deleteListing($id){

		$delete_listing = BirBhunerheriNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function bhunerheri_editNotificationText($id){

		$edit_notification = BirBhunerheriNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.bhunerheri.edit_notification',compact('edit_notification'));
	}

	public function bhunerheri_updateNotificationText(Request $request , $id){

		$update_notification = BirBhunerheriNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/bhunerheri_notification_list/'.$update_notification->bir_bhunerheri_id)->with('success','Notification Text updated successfully');
	}

	public function bhunerheri_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.bhunerheri.add_new_notification',compact('id'));		 
	}

	public function bhunerheri_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirBhunerheriNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_bhunerheri_id = $id;
		$save_notification->save();

		return redirect('/bhunerheri_notification_list/'.$save_notification->bir_bhunerheri_id)->with('success','Notification Text Save successfully');
	}

	public function bhunerheri_notification_pdflist($id){

		$notification_pdf_list = BirBhunerheriNotificationDetail::where('bir_bhunerheri_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.bhunerheri.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function bhunerheri_edit_notification_pdf($id){

		$edit = BirBhunerheriNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.bhunerheri.edit_notification_pdf',compact('edit','id'));
	}

	public function bhunerheri_update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/bhunerheri/', $image);
				$pdf_url = '/bhunerheri/'.$image;
	           
			}
			$update_pdf = BirBhunerheriNotificationDetail::find($id);
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

			return redirect('/bhunerheri_notification_pdf/'.$update_pdf->bir_bhunerheri_id)->with('success','Notification pdf updated successfully');
	}

	public function bhunerheri_delete_notification_pdf($id){

		$delete_pdf = BirBhunerheriNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}


	public function bir_mehasList(){

		$bir_mehas_list = BirMihas::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($bir_mehas_list as $list){
			$list->text_count = BirMehasNotificationDetail::where('bir_mehas_id',$list->id)->count();
			$list->pdf_count = BirMehasNotificationDetail::where('bir_mehas_id',$list->id)->count();

		}
		// return $bir_mehas_list;
		return view('protectedArea.wildlifeSantuaries.mehas.listing',compact('bir_mehas_list'));
	}

	public function addbir_mehas(){

		return view('protectedArea.wildlifeSantuaries.mehas.add');
	}
	public function savebir_mehas(Request $request){

    	
		// return $request->all();

		$validated = $request->validate([
	    'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/birmihas');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/birmihas/' . $filename;
		}

		$save_birmihas = new BirMihas();
		$save_birmihas->banner_image = $banner_image;
		$save_birmihas->banner_heading = $banner_heading;
		$save_birmihas->description = $description;
		$save_birmihas->wildlife_heading = $wildlife_heading;
		$save_birmihas->wildlife_title = $wildlife_title;
		$save_birmihas->district = $district;
		$save_birmihas->location = $location;
		$save_birmihas->area = $area;
		$save_birmihas->status_of_land = $status_of_land;
		$save_birmihas->important_fauna = $important_fauna;
		$save_birmihas->important_flora = $important_flora;
		$save_birmihas->save();

		$id = $save_birmihas->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/bhunerheri/', $image);
				$image_data[] = '/bhunerheri/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirMehasNotificationDetail();
			$save_subheading->bir_mehas_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_mehas_pa')->with('success','Bir Mehas Detail Added Successfully');


	}

	public function editbir_mehas($id){

		$edit_birmehas = BirMihas::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.mehas.edit',compact('edit_birmehas'));
	}

	public function updatebir_mehas(Request $request ,$id){

		$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/birmihas');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/birmihas/' . $filename;
            $update_bir_mihas = BirMihas::find($id);
			$update_bir_mihas->banner_image = $banner_image;
			$update_bir_mihas->banner_heading = $banner_heading;
			$update_bir_mihas->description = $description;
			$update_bir_mihas->wildlife_heading = $wildlife_heading;
			$update_bir_mihas->wildlife_title = $wildlife_title;
			$update_bir_mihas->district = $district;
			$update_bir_mihas->location = $location;
			$update_bir_mihas->area = $area;
			$update_bir_mihas->status_of_land = $status_of_land;
			$update_bir_mihas->notification_detail = $notification_detail;
			$update_bir_mihas->important_fauna = $important_fauna;
			$update_bir_mihas->important_flora = $important_flora;
			$update_bir_mihas->save();
		}
		else{
			$update_bir_mihas = BirMihas::find($id);
			$update_bir_mihas->banner_heading = $banner_heading;
			$update_bir_mihas->description = $description;
			$update_bir_mihas->wildlife_heading = $wildlife_heading;
			$update_bir_mihas->wildlife_title = $wildlife_title;
			$update_bir_mihas->district = $district;
			$update_bir_mihas->location = $location;
			$update_bir_mihas->area = $area;
			$update_bir_mihas->status_of_land = $status_of_land;
			$update_bir_mihas->notification_detail = $notification_detail;
			$update_bir_mihas->important_fauna = $important_fauna;
			$update_bir_mihas->important_flora = $important_flora;
			$update_bir_mihas->save();
		}

		return redirect('/bir_mehas_pa')->with('success','Bir Mehas Detail Updated Successfully');
	}

	public function deletebir_mehas($id){

		$save_bhunerheri = BirMihas::find($id);
		$save_bhunerheri->deleted_status = '1';
		$save_bhunerheri->save();
		return redirect('/bir_mehas_pa')->with('success','Bir Mehas Detail Deleted Successfully');
	}

	// notification text
	public function mehas_notificationtextListing($id){

		$notification_list = BirMehasNotificationDetail::where('bir_mehas_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.mehas.notification_list',compact('notification_list','id'));
	}

	public function mehas_deleteListing($id){

		$delete_listing = BirMehasNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function mehas_editNotificationText($id){

		$edit_notification = BirMehasNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.mehas.edit_notification',compact('edit_notification'));
	}

	public function mehas_updateNotificationText(Request $request , $id){

		$update_notification = BirMehasNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/mehas_notification_list/'.$update_notification->bir_mehas_id)->with('success','Notification Text updated successfully');
	}

	public function mehas_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.mehas.add_new_notification',compact('id'));		 
	}

	public function mehas_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirMehasNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_mehas_id = $id;
		$save_notification->save();

		return redirect('/mehas_notification_list/'.$save_notification->bir_mehas_id)->with('success','Notification Text Save successfully');
	}

	public function mehas_notification_pdflist($id){

		$notification_pdf_list = BirMehasNotificationDetail::where('bir_mehas_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.mehas.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function mehas_edit_notification_pdf($id){

		$edit = BirMehasNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.mehas.edit_notification_pdf',compact('edit','id'));
	}

	public function mehas_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirMehasNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/mehas/', $image);
				$pdf_url = '/mehas/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('mehas_notification_pdf/'.$update_pdf->bir_mehas_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('mehas_notification_pdf/'.$update_pdf->bir_mehas_id)->with('success','Notification pdf updated successfully');
			}

			
	}

	public function mehas_delete_notification_pdf($id){

		$delete_pdf = BirMehasNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function dosanjhList(){

		$dosanjhlist = BirDosanjh::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($dosanjhlist as $list){

			$list->text_count = BirDosanjhNotificationDetail::where('bir_dosanjh_id',$list->id)->count();
			$list->pdf_count = BirDosanjhNotificationDetail::where('bir_dosanjh_id',$list->id)->count();
		}
		// return $dosanjhlist;
		return view('protectedArea.wildlifeSantuaries.dosanjh.listing',compact('dosanjhlist'));
	}
	public function adddosanjh(){

		return view('protectedArea.wildlifeSantuaries.dosanjh.add');
	}
	public function savedosanjh(Request $request){

		// return $request->all();

		$validated = $request->validate([
	    'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/dosanjh');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/dosanjh/' . $filename;
		}

		$save_birdosanjh = new BirDosanjh();
		$save_birdosanjh->banner_image = $banner_image;
		$save_birdosanjh->banner_heading = $banner_heading;
		$save_birdosanjh->description = $description;
		$save_birdosanjh->wildlife_heading = $wildlife_heading;
		$save_birdosanjh->wildlife_title = $wildlife_title;
		$save_birdosanjh->district = $district;
		$save_birdosanjh->location = $location;
		$save_birdosanjh->area = $area;
		$save_birdosanjh->status_of_land = $status_of_land;
		$save_birdosanjh->important_fauna = $important_fauna;
		$save_birdosanjh->important_flora = $important_flora;
		$save_birdosanjh->save();

		$id = $save_birdosanjh->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/gurdialpur/', $image);
				$image_data[] = '/gurdialpur/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirDosanjhNotificationDetail();
			$save_subheading->bir_dosanjh_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_dosanjh_pa')->with('success','Bir Dosanjh Detail Added Successfully');
	}

	public function editdosanjh($id){

		$editdosanjh = BirDosanjh::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.dosanjh.edit',compact('editdosanjh'));
	}

	public function updatedosanjh(Request $request, $id){

		// return $request->all();
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/dosanjh');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/dosanjh/' . $filename;

            $update_birdosanjh = BirDosanjh::find($id);
			$update_birdosanjh->banner_image = $banner_image;
			$update_birdosanjh->banner_heading = $banner_heading;
			$update_birdosanjh->description = $description;
			$update_birdosanjh->wildlife_heading = $wildlife_heading;
			$update_birdosanjh->wildlife_title = $wildlife_title;
			$update_birdosanjh->district = $district;
			$update_birdosanjh->location = $location;
			$update_birdosanjh->area = $area;
			$update_birdosanjh->status_of_land = $status_of_land;
			$update_birdosanjh->notification_detail = $notification_detail;
			$update_birdosanjh->important_fauna = $important_fauna;
			$update_birdosanjh->important_flora = $important_flora;
			$update_birdosanjh->update();
		}
		else{
			$update_birdosanjh = BirDosanjh::find($id);
			$update_birdosanjh->banner_heading = $banner_heading;
			$update_birdosanjh->description = $description;
			$update_birdosanjh->wildlife_heading = $wildlife_heading;
			$update_birdosanjh->wildlife_title = $wildlife_title;
			$update_birdosanjh->district = $district;
			$update_birdosanjh->location = $location;
			$update_birdosanjh->area = $area;
			$update_birdosanjh->status_of_land = $status_of_land;
			$update_birdosanjh->notification_detail = $notification_detail;
			$update_birdosanjh->important_fauna = $important_fauna;
			$update_birdosanjh->important_flora = $important_flora;
			$update_birdosanjh->save();

		}

		return redirect('/bir_dosanjh_pa')->with('success','Bir Dosanjh Detail Updated Successfully');
	}

	public function deletedosanjh($id){

		$delete_dosanjh = BirDosanjh::find($id);
		$delete_dosanjh->deleted_status = '1';
		$delete_dosanjh->save();
		return redirect('/bir_dosanjh_pa')->with('success','Bir Dosanjh Detail Deleted Successfully');

	}

	// notification text
	public function dosanjh_notificationtextListing($id){

		$notification_list = BirDosanjhNotificationDetail::where('bir_dosanjh_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.dosanjh.notification_list',compact('notification_list','id'));
	}

	public function dosanjh_deleteListing($id){

		$delete_listing = BirDosanjhNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function dosanjh_editNotificationText($id){

		$edit_notification = BirDosanjhNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.dosanjh.edit_notification',compact('edit_notification'));
	}

	public function dosanjh_updateNotificationText(Request $request , $id){

		$update_notification = BirDosanjhNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/dosanjh_notification_list/'.$update_notification->bir_dosanjh_id)->with('success','Notification Text updated successfully');
	}

	public function dosanjh_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.dosanjh.add_new_notification',compact('id'));		 
	}

	public function dosanjh_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirDosanjhNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_dosanjh_id = $id;
		$save_notification->save();

		return redirect('/dosanjh_notification_list/'.$save_notification->bir_dosanjh_id)->with('success','Notification Text Save successfully');
	}

	public function dosanjh_notification_pdflist($id){

		$notification_pdf_list = BirDosanjhNotificationDetail::where('bir_dosanjh_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.dosanjh.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function dosanjh_edit_notification_pdf($id){

		$edit = BirDosanjhNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.dosanjh.edit_notification_pdf',compact('edit','id'));
	}

	public function dosanjh_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirDosanjhNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/dosanjh/', $image);
				$pdf_url = '/dosanjh/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('dosanjh_notification_pdf/'.$update_pdf->bir_dosanjh_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('dosanjh_notification_pdf/'.$update_pdf->bir_dosanjh_id)->with('success','Notification pdf updated successfully');
			}

			
	}

	public function dosanjh_delete_notification_pdf($id){

		$delete_pdf = BirDosanjhNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function birBhadsonList(){

		$birbhadson = BirBhadson::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($birbhadson as $list){

			$list->text_count = BirBhadsonNotificationDetail::where('bir_bhadson_id',$list->id)->count();

			$list->pdf_count = BirBhadsonNotificationDetail::where('bir_bhadson_id',$list->id)->count();
		}
		return view('protectedArea.wildlifeSantuaries.bhadson.listing',compact('birbhadson'));
	}
	public function addbhadson(){

		return view('protectedArea.wildlifeSantuaries.bhadson.add');
	}
	public function savebhadson(Request $request){

		// return $request->all();

		$validated = $request->validate([
	    'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_bhadson');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_bhadson/' . $filename;
		}

		$save_bhadson = new BirBhadson();
		$save_bhadson->banner_image = $banner_image;
		$save_bhadson->banner_heading = $banner_heading;
		$save_bhadson->description = $description;
		$save_bhadson->wildlife_heading = $wildlife_heading;
		$save_bhadson->wildlife_title = $wildlife_title;
		$save_bhadson->district = $district;
		$save_bhadson->location = $location;
		$save_bhadson->area = $area;
		$save_bhadson->status_of_land = $status_of_land;
		$save_bhadson->important_fauna = $important_fauna;
		$save_bhadson->important_flora = $important_flora;
		$save_bhadson->save();

		$id = $save_bhadson->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/gurdialpur/', $image);
				$image_data[] = '/gurdialpur/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirBhadsonNotificationDetail();
			$save_subheading->bir_bhadson_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_bhadson_pa')->with('success','Bir Bhadson Detail Added Successfully');
	}

	public function editbhadson($id){

		$editbhadson = BirBhadson::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.bhadson.edit',compact('editbhadson'));
	}

	public function updatebhadson(Request $request ,$id){

		// return $request->all();
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_bhadson');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_bhadson/' . $filename;

            $update_bhadson = BirBhadson::find($id);
			$update_bhadson->banner_image = $banner_image;
			$update_bhadson->banner_heading = $banner_heading;
			$update_bhadson->description = $description;
			$update_bhadson->wildlife_heading = $wildlife_heading;
			$update_bhadson->wildlife_title = $wildlife_title;
			$update_bhadson->district = $district;
			$update_bhadson->location = $location;
			$update_bhadson->area = $area;
			$update_bhadson->status_of_land = $status_of_land;
			$update_bhadson->notification_detail = $notification_detail;
			$update_bhadson->important_fauna = $important_fauna;
			$update_bhadson->important_flora = $important_flora;
			$update_bhadson->save();
		}
		else{
			$update_bhadson = BirBhadson::find($id);
			$update_bhadson->banner_heading = $banner_heading;
			$update_bhadson->description = $description;
			$update_bhadson->wildlife_heading = $wildlife_heading;
			$update_bhadson->wildlife_title = $wildlife_title;
			$update_bhadson->district = $district;
			$update_bhadson->location = $location;
			$update_bhadson->area = $area;
			$update_bhadson->status_of_land = $status_of_land;
			$update_bhadson->notification_detail = $notification_detail;
			$update_bhadson->important_fauna = $important_fauna;
			$update_bhadson->important_flora = $important_flora;
			$update_bhadson->save();

		}

		return redirect('/bir_bhadson_pa')->with('success','Bir Bhadson Detail Updated Successfully');
	}

	public function deletebhadson($id){

		$delete_bhadson = BirBhadson::find($id);
		$delete_bhadson->deleted_status = '1';
		$delete_bhadson->save();
		return redirect('/bir_bhadson_pa')->with('success','Bir Bhadson Detail Deleted Successfully');
	}

	// notification text
	public function bhadson_notificationtextListing($id){

		$notification_list = BirBhadsonNotificationDetail::where('bir_bhadson_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.bhadson.notification_list',compact('notification_list','id'));
	}

	public function bhadson_deleteListing($id){

		$delete_listing = BirBhadsonNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function bhadson_editNotificationText($id){

		$edit_notification = BirBhadsonNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.bhadson.edit_notification',compact('edit_notification'));
	}

	public function bhadson_updateNotificationText(Request $request , $id){

		$update_notification = BirBhadsonNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/bhadson_notification_list/'.$update_notification->bir_bhadson_id)->with('success','Notification Text updated successfully');
	}

	public function bhadson_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.bhadson.add_new_notification',compact('id'));		 
	}

	public function bhadson_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirBhadsonNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_bhadson_id = $id;
		$save_notification->save();

		return redirect('/bhadson_notification_list/'.$save_notification->bir_dosanjh_id)->with('success','Notification Text Save successfully');
	}

	public function bhadson_notification_pdflist($id){

		$notification_pdf_list = BirBhadsonNotificationDetail::where('bir_bhadson_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.bhadson.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function bhadson_edit_notification_pdf($id){

		$edit = BirBhadsonNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.bhadson.edit_notification_pdf',compact('edit','id'));
	}

	public function bhadson_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirBhadsonNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/dosanjh/', $image);
				$pdf_url = '/dosanjh/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('bhadson_notification_pdf/'.$update_pdf->bir_bhadson_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('bhadson_notification_pdf/'.$update_pdf->bir_bhadson_id)->with('success','Notification pdf updated successfully');
			}

			
	}

	public function bhadson_delete_notification_pdf($id){

		$delete_pdf = BirBhadsonNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}



	public function bir_aishwanDetail(){

		$bir_aishwan_list = BirAishwan::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($bir_aishwan_list as $list){

			$list->text_count = BirAishwanNotificationDetail::where('bir_aishwan_id',$list->id)->count();
			$list->pdf_count = BirAishwanNotificationDetail::where('bir_aishwan_id',$list->id)->count();
		}
        return view('protectedArea.wildlifeSantuaries.aishwan.listing',compact('bir_aishwan_list'));
    }

    public function addbir_aishwan(){

    	return view('protectedArea.wildlifeSantuaries.aishwan.add');

    }
    public function savebir_aishwan(Request $request){

		// return $request->all();
    	$validated = $request->validate([
	    'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_aishwan');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_aishwan/' . $filename;
		}

		$save_aishwan = new BirAishwan();
		$save_aishwan->banner_image = $banner_image;
		$save_aishwan->banner_heading = $banner_heading;
		$save_aishwan->description = $description;
		$save_aishwan->wildlife_heading = $wildlife_heading;
		$save_aishwan->wildlife_title = $wildlife_title;
		$save_aishwan->district = $district;
		$save_aishwan->location = $location;
		$save_aishwan->area = $area;
		$save_aishwan->status_of_land = $status_of_land;
		$save_aishwan->important_fauna = $important_fauna;
		$save_aishwan->important_flora = $important_flora;
		$save_aishwan->save();

		$id = $save_aishwan->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/bir_aishwan/', $image);
				$image_data[] = '/bir_aishwan/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirAishwanNotificationDetail();
			$save_subheading->bir_aishwan_id  = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/bir_aishwan_pa')->with('success','Bir Aishwan Detail Added Successfully');

    }

    public function editbir_aishwan($id){

    	$editbir_aishwan = BirAishwan::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.aishwan.edit',compact('editbir_aishwan'));
    }

    public function updatebir_aishwan(Request $request ,$id){
	
    	// return $request->all();
    	
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_aishwan');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_aishwan/' . $filename;

            $update_aishwan = BirAishwan::find($id);
			$update_aishwan->banner_image = $banner_image;
			$update_aishwan->banner_heading = $banner_heading;
			$update_aishwan->description = $description;
			$update_aishwan->wildlife_heading = $wildlife_heading;
			$update_aishwan->wildlife_title = $wildlife_title;
			$update_aishwan->district = $district;
			$update_aishwan->location = $location;
			$update_aishwan->area = $area;
			$update_aishwan->status_of_land = $status_of_land;
			$update_aishwan->notification_detail = $notification_detail;
			$update_aishwan->important_fauna = $important_fauna;
			$update_aishwan->important_flora = $important_flora;
			$update_aishwan->save();
		}
		else{
			$update_aishwan = BirAishwan::find($id);
			$update_aishwan->banner_heading = $banner_heading;
			$update_aishwan->description = $description;
			$update_aishwan->wildlife_heading = $wildlife_heading;
			$update_aishwan->wildlife_title = $wildlife_title;
			$update_aishwan->district = $district;
			$update_aishwan->location = $location;
			$update_aishwan->area = $area;
			$update_aishwan->status_of_land = $status_of_land;
			$update_aishwan->notification_detail = $notification_detail;
			$update_aishwan->important_fauna = $important_fauna;
			$update_aishwan->important_flora = $important_flora;
			$update_aishwan->save();
		}
		
		return redirect('/bir_aishwan_pa')->with('success','Bir Aishwan Detail Updated Successfully');

    }

    public function deletebir_aishwan($id){
    	$deletebir_aishwan = BirAishwan::find($id);
		$deletebir_aishwan->deleted_status = '1';
		$deletebir_aishwan->save();
		return redirect('/bir_aishwan_pa')->with('success','Bir Aishwan Detail Deleted Successfully');

    }

    // notification text
	public function aishwan_notificationtextListing($id){

		$notification_list = BirAishwanNotificationDetail::where('bir_aishwan_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.aishwan.notification_list',compact('notification_list','id'));
	}

	public function aishwan_deleteListing($id){

		$delete_listing = BirAishwanNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function aishwan_editNotificationText($id){

		$edit_notification = BirAishwanNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.aishwan.edit_notification',compact('edit_notification'));
	} 

	public function aishwan_updateNotificationText(Request $request , $id){

		$update_notification = BirAishwanNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/aishwan_notification_list/'.$update_notification->bir_aishwan_id)->with('success','Notification Text updated successfully');
	}

	public function aishwan_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.aishwan.add_new_notification',compact('id'));		 
	}

	public function aishwan_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirAishwanNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_bhadson_id = $id;
		$save_notification->save();

		return redirect('/aishwan_notification_list/'.$save_notification->bir_aishwan_id)->with('success','Notification Text Save successfully');
	}

	public function aishwan_notification_pdflist($id){

		$notification_pdf_list = BirAishwanNotificationDetail::where('bir_aishwan_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.aishwan.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function aishwan_edit_notification_pdf($id){

		$edit = BirAishwanNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.aishwan.edit_notification_pdf',compact('edit','id'));
	}

	public function aishwan_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirAishwanNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/aishwan/', $image);
				$pdf_url = '/aishwan/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('aishwan_notification_pdf/'.$update_pdf->bir_aishwan_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('aishwan_notification_pdf/'.$update_pdf->bir_aishwan_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function aishwan_delete_notification_pdf($id){

		$delete_pdf = BirBhadsonNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}


    public function bir_aboharDetail(){

    	$bir_abohar_detail = BirAbohar::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach($bir_abohar_detail as $list){

    		$list->text_count = BirAboharNotificationDetail::where('bir_abohar_id',$list->id)->count();
    		$list->pdf_count = BirAboharNotificationDetail::where('bir_abohar_id',$list->id)->count();

    	}
    	return view('protectedArea.wildlifeSantuaries.abohar.listing',compact('bir_abohar_detail'));
    } 

    public function addbir_abohar(){

    	return view('protectedArea.wildlifeSantuaries.abohar.add');
    }
    public function savebir_abohar(Request $request){

    	// return $request->all();

    	$validated = $request->validate([
	    'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_abohar');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_abohar/' . $filename;

            }
            $save_abohar = new BirAbohar();
			$save_abohar->banner_image = $banner_image;
			$save_abohar->banner_heading = $banner_heading;
			$save_abohar->description = $description;
			$save_abohar->wildlife_heading = $wildlife_heading;
			$save_abohar->wildlife_title = $wildlife_title;
			$save_abohar->district = $district;
			$save_abohar->location = $location;
			$save_abohar->area = $area;
			$save_abohar->status_of_land = $status_of_land;
			$save_abohar->important_fauna = $important_fauna;
			$save_abohar->important_flora = $important_flora;
			$save_abohar->save();

			$id = $save_abohar->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/bir_aishwan/', $image);
				$image_data[] = '/bir_aishwan/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirAboharNotificationDetail();
			$save_subheading->bir_abohar_id  = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		
		
		return redirect('/bir_abohar_pa')->with('success','Bir Abohar Detail Added Successfully');
    }

    public function editbir_abohar($id){

    	$edit_bir_abohar = BirAbohar::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.abohar.edit',compact('edit_bir_abohar'));
    }

    public function updatebir_abohar(Request $request ,$id){

		// return $request->all();
    
    		
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	


		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_abohar');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_abohar/' . $filename;

            $update_abohar = BirAbohar::find($id);
			$update_abohar->banner_image = $banner_image;
			$update_abohar->banner_heading = $banner_heading;
			$update_abohar->description = $description;
			$update_abohar->wildlife_heading = $wildlife_heading;
			$update_abohar->wildlife_title = $wildlife_title;
			$update_abohar->district = $district;
			$update_abohar->location = $location;
			$update_abohar->area = $area;
			$update_abohar->status_of_land = $status_of_land;
			$update_abohar->notification_detail = $notification_detail;
			$update_abohar->important_fauna = $important_fauna;
			$update_abohar->important_flora = $important_flora;
			$update_abohar->save();
        }
        else{

        	$update_abohar = BirAbohar::find($id);
			$update_abohar->banner_heading = $banner_heading;
			$update_abohar->description = $description;
			$update_abohar->wildlife_heading = $wildlife_heading;
			$update_abohar->wildlife_title = $wildlife_title;
			$update_abohar->district = $district;
			$update_abohar->location = $location;
			$update_abohar->area = $area;
			$update_abohar->status_of_land = $status_of_land;
			$update_abohar->notification_detail = $notification_detail;
			$update_abohar->important_fauna = $important_fauna;
			$update_abohar->important_flora = $important_flora;
			$update_abohar->save();
        }
         
		return redirect('/bir_abohar_pa')->with('success','Bir Abohar Detail Added Successfully');
        
    }

    public function deletebir_abohar($id){

    	$deletebir_abohar = BirAbohar::find($id);
		$deletebir_abohar->deleted_status = '1';
		$deletebir_abohar->save();
		return redirect('/bir_abohar_pa')->with('success','Bir Abohar Detail Deleted Successfully');
    }

       // notification text
	public function abohar_notificationtextListing($id){

		$notification_list = BirAboharNotificationDetail::where('bir_abohar_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.abohar.notification_list',compact('notification_list','id'));
	}

	public function abohar_deleteListing($id){

		$delete_listing = BirAboharNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function abohar_editNotificationText($id){

		$edit_notification = BirAboharNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.abohar.edit_notification',compact('edit_notification'));
	} 

	public function abohar_updateNotificationText(Request $request , $id){

		$update_notification = BirAboharNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/abohar_notification_list/'.$update_notification->bir_abohar_id)->with('success','Notification Text updated successfully');
	}

	public function abohar_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.abohar.add_new_notification',compact('id'));		 
	}

	public function abohar_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirAboharNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_abohar_id = $id;
		$save_notification->save();

		return redirect('/abohar_notification_list/'.$save_notification->bir_abohar_id)->with('success','Notification Text Save successfully');
	}

	public function abohar_notification_pdflist($id){

	$notification_pdf_list = BirAboharNotificationDetail::where('bir_abohar_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.abohar.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function abohar_edit_notification_pdf($id){

		$edit = BirAboharNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.abohar.edit_notification_pdf',compact('edit','id'));
	}

	public function abohar_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirAboharNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/abohar/', $image);
				$pdf_url = '/abohar/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('abohar_notification_pdf/'.$update_pdf->bir_abohar_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('abohar_notification_pdf/'.$update_pdf->bir_abohar_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function abohar_delete_notification_pdf($id){

		$delete_pdf = BirAboharNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

    public function bir_harikeDetail(){

    	$harike_listing = BirHarike::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach($harike_listing as $list){

    		$list->text_count = BirHarikeNotificationDetail::where('bir_harike_id',$list->id)->count();
    		$list->pdf_count = BirHarikeNotificationDetail::where('bir_harike_id',$list->id)->count();
    	}
    	return view('protectedArea.wildlifeSantuaries.harike.listing',compact('harike_listing'));
    }
    public function addbir_harike(){

    	return view('protectedArea.wildlifeSantuaries.harike.add');
    }

    public function savebir_harike(Request $request){

    	// return $request->all();
    	$validated = $request->validate([
	           'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_harke');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_harke/' . $filename;

            }
            $save_birharike = new BirHarike();
			$save_birharike->banner_image = $banner_image;
			$save_birharike->banner_heading = $banner_heading;
			$save_birharike->description = $description;
			$save_birharike->wildlife_heading = $wildlife_heading;
			$save_birharike->wildlife_title = $wildlife_title;
			$save_birharike->district = $district;
			$save_birharike->location = $location;
			$save_birharike->area = $area;
			$save_birharike->status_of_land = $status_of_land;
			$save_birharike->important_fauna = $important_fauna;
			$save_birharike->important_flora = $important_flora;
			$save_birharike->save();

			$id = $save_birharike->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/bir_aishwan/', $image);
				$image_data[] = '/bir_aishwan/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirHarikeNotificationDetail();
			$save_subheading->bir_harike_id   = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }
		
		return redirect('/bir_harike_pa')->with('success','Bir Harike Detail Added Successfully');
    }

    public function editbir_harike($id){

    	$edit_harike = BirHarike::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.harike.edit',compact('edit_harike'));
    }

    public function updatebir_harike(Request $request , $id){

    	// return $request->all();
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_harke');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_harke/' . $filename;

            $update_bhunerheri = BirHarike::find($id);
			$update_bhunerheri->banner_image = $banner_image;
			$update_bhunerheri->banner_heading = $banner_heading;
			$update_bhunerheri->description = $description;
			$update_bhunerheri->wildlife_heading = $wildlife_heading;
			$update_bhunerheri->wildlife_title = $wildlife_title;
			$update_bhunerheri->district = $district;
			$update_bhunerheri->location = $location;
			$update_bhunerheri->area = $area;
			$update_bhunerheri->status_of_land = $status_of_land;
			$update_bhunerheri->notification_detail = $notification_detail;
			$update_bhunerheri->important_fauna = $important_fauna;
			$update_bhunerheri->important_flora = $important_flora;
			$update_bhunerheri->save();
        }
        else{

        	$update_bhunerheri = BirHarike::find($id);
			$update_bhunerheri->banner_heading = $banner_heading;
			$update_bhunerheri->description = $description;
			$update_bhunerheri->wildlife_heading = $wildlife_heading;
			$update_bhunerheri->wildlife_title = $wildlife_title;
			$update_bhunerheri->district = $district;
			$update_bhunerheri->location = $location;
			$update_bhunerheri->area = $area;
			$update_bhunerheri->status_of_land = $status_of_land;
			$update_bhunerheri->notification_detail = $notification_detail;
			$update_bhunerheri->important_fauna = $important_fauna;
			$update_bhunerheri->important_flora = $important_flora;
			$update_bhunerheri->save();
        }
           
		return redirect('/bir_harike_pa')->with('success','Bir Harike Detail Updated Successfully');
    }

    public function deletebir_harike($id){

    	$delete_bhunerheri = BirHarike::find($id);
		$delete_bhunerheri->deleted_status = '1';
		$delete_bhunerheri->save();
		return redirect('/bir_harike_pa')->with('success','Bir Harike Detail Deleted Successfully');
    }

     // notification text
	public function harike_notificationtextListing($id){

		$notification_list = BirHarikeNotificationDetail::where('bir_harike_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.harike.notification_list',compact('notification_list','id'));
	}

	public function harike_deleteListing($id){

		$delete_listing = BirHarikeNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function harike_editNotificationText($id){

		$edit_notification = BirHarikeNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.harike.edit_notification',compact('edit_notification'));
	} 

	public function harike_updateNotificationText(Request $request , $id){

		$update_notification = BirHarikeNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/harike_notification_list/'.$update_notification->bir_harike_id)->with('success','Notification Text updated successfully');
	}

	public function harike_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.harike.add_new_notification',compact('id'));		 
	}

	public function harike_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirHarikeNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_harike_id = $id;
		$save_notification->save();

		return redirect('/harike_notification_list/'.$save_notification->bir_abohar_id)->with('success','Notification Text Save successfully');
	}

	public function harike_notification_pdflist($id){

	$notification_pdf_list = BirHarikeNotificationDetail::where('bir_harike_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.harike.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function harike_edit_notification_pdf($id){

		$edit = BirHarikeNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.harike.edit_notification_pdf',compact('edit','id'));
	}

	public function harike_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirHarikeNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/harike/', $image);
				$pdf_url = '/harike/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('harike_notification_pdf/'.$update_pdf->bir_harike_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('harike_notification_pdf/'.$update_pdf->bir_harike_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function harike_delete_notification_pdf($id){

		$delete_pdf = BirHarikeNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}


    public function bir_takhni_rehmpur(){
    	$takhnirehmpur_detail = BirTakhniRehmpur::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach( $takhnirehmpur_detail as $list){

    		$list->text_count = BirTakhnipurNotificationDetail::where('bir_takhnipur_id',$list->id)->count();
    		$list->pdf_count = BirTakhnipurNotificationDetail::where('bir_takhnipur_id',$list->id)->count();
    	}
    	return view('protectedArea.wildlifeSantuaries.takhniRehmpur.listing',compact('takhnirehmpur_detail'));
    }

    public function addbir_takhni_rehmpur(){

		return view('protectedArea.wildlifeSantuaries.takhniRehmpur.add');    	
    }

    public function savebir_takhni_rehmpur(Request $request){

    		// return $request->all();
    	$validated = $request->validate([
	    'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_takhnipur');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_takhnipur/' . $filename;
        }
     	
     	    $save_takhni_rehmpur = new BirTakhniRehmpur();
			$save_takhni_rehmpur->banner_image = $banner_image;
			$save_takhni_rehmpur->banner_heading = $banner_heading;
			$save_takhni_rehmpur->description = $description;
			$save_takhni_rehmpur->wildlife_heading = $wildlife_heading;
			$save_takhni_rehmpur->wildlife_title = $wildlife_title;
			$save_takhni_rehmpur->district = $district;
			$save_takhni_rehmpur->location = $location;
			$save_takhni_rehmpur->area = $area;
			$save_takhni_rehmpur->status_of_land = $status_of_land;
			$save_takhni_rehmpur->important_fauna = $important_fauna;
			$save_takhni_rehmpur->important_flora = $important_flora;
			$save_takhni_rehmpur->save();

			$id = $save_takhni_rehmpur->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/bir_aishwan/', $image);
				$image_data[] = '/bir_aishwan/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BirTakhnipurNotificationDetail();
			$save_subheading->bir_takhnipur_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }
           
		return redirect('/takhni_rehmpur_pa')->with('success','Bir Takhni Rehmpur Detail Added Successfully');
    }

    public function editbir_takhni_rehmpur($id){

    	$editbir_takhni_rehmpur = BirTakhniRehmpur::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.takhniRehmpur.edit',compact('editbir_takhni_rehmpur'));    
    }

    public function updatebir_takhni_rehmpur(Request $request, $id){

    		// return $request->all();
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/bir_takhnipur');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/bir_takhnipur/' . $filename;

         	$update_takhni_rehmpur = BirTakhniRehmpur::find($id);
			$update_takhni_rehmpur->banner_image = $banner_image;
			$update_takhni_rehmpur->banner_heading = $banner_heading;
			$update_takhni_rehmpur->description = $description;
			$update_takhni_rehmpur->wildlife_heading = $wildlife_heading;
			$update_takhni_rehmpur->wildlife_title = $wildlife_title;
			$update_takhni_rehmpur->district = $district;
			$update_takhni_rehmpur->location = $location;
			$update_takhni_rehmpur->area = $area;
			$update_takhni_rehmpur->status_of_land = $status_of_land;
			$update_takhni_rehmpur->notification_detail = $notification_detail;
			$update_takhni_rehmpur->important_fauna = $important_fauna;
			$update_takhni_rehmpur->important_flora = $important_flora;
			$update_takhni_rehmpur->save();
        }
     	else{

     		$update_takhni_rehmpur = BirTakhniRehmpur::find($id);
			$update_takhni_rehmpur->banner_heading = $banner_heading;
			$update_takhni_rehmpur->description = $description;
			$update_takhni_rehmpur->wildlife_heading = $wildlife_heading;
			$update_takhni_rehmpur->wildlife_title = $wildlife_title;
			$update_takhni_rehmpur->district = $district;
			$update_takhni_rehmpur->location = $location;
			$update_takhni_rehmpur->area = $area;
			$update_takhni_rehmpur->status_of_land = $status_of_land;
			$update_takhni_rehmpur->notification_detail = $notification_detail;
			$update_takhni_rehmpur->important_fauna = $important_fauna;
			$update_takhni_rehmpur->important_flora = $important_flora;
			$update_takhni_rehmpur->save();
     	}
     	   
           
		return redirect('/takhni_rehmpur_pa')->with('success','Bir Takhni Rehmpur Detail Updated Successfully');
    }

    public function deletebir_takhni_rehmpur($id){

    	$delete_takhni_rehmpur = BirTakhniRehmpur::find($id);
		$delete_takhni_rehmpur->deleted_status = '1';
		$delete_takhni_rehmpur->save();
		return redirect('/takhni_rehmpur_pa')->with('success','Bir Takhni Rehmpur Detail Deleted Successfully');
    }


     // notification text
	public function takhni_notificationtextListing($id){

		$notification_list = BirTakhnipurNotificationDetail::where('bir_takhnipur_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.takhniRehmpur.notification_list',compact('notification_list','id'));
	}

	public function takhni_deleteListing($id){

		$delete_listing = BirTakhnipurNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function takhni_editNotificationText($id){

		$edit_notification = BirTakhnipurNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.takhniRehmpur.edit_notification',compact('edit_notification'));
	} 

	public function takhni_updateNotificationText(Request $request , $id){

		$update_notification = BirTakhnipurNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/takhni_notification_list/'.$update_notification->bir_takhnipur_id)->with('success','Notification Text updated successfully');
	}

	public function takhni_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.takhniRehmpur.add_new_notification',compact('id'));		 
	}

	public function takhni_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirTakhnipurNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_takhnipur_id = $id;
		$save_notification->save();

		return redirect('/takhni_notification_list/'.$save_notification->bir_takhnipur_id)->with('success','Notification Text Save successfully');
	}

	public function takhni_notification_pdflist($id){

	$notification_pdf_list = BirTakhnipurNotificationDetail::where('bir_takhnipur_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.takhniRehmpur.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function takhni_edit_notification_pdf($id){

		$edit = BirTakhnipurNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.takhniRehmpur.edit_notification_pdf',compact('edit','id'));
	}

	public function takhni_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirTakhnipurNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/bir_takhnipur/', $image);
				$pdf_url = '/bir_takhnipur/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('takhni_notification_pdf/'.$update_pdf->bir_takhnipur_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('takhni_notification_pdf/'.$update_pdf->bir_takhnipur_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function takhni_delete_notification_pdf($id){

		$delete_pdf = BirTakhnipurNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

    public function jhajjarbachauliList(){

    	$jhajjarbachauliList = BirJhajjarbachauli::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach($jhajjarbachauliList as $list){
    		$list->text_count = BirJhajjarBachauliNotificationDetail::where('bir_jhajjarbachauli_id',$list->id)->count();
    		$list->pdf_count = BirJhajjarBachauliNotificationDetail::where('bir_jhajjarbachauli_id',$list->id)->count();

    	}
    	return view('protectedArea.wildlifeSantuaries.jhajjarbachual.listing',compact('jhajjarbachauliList'));
    }

    public function addjhajjarbachauli(){

    	return view('protectedArea.wildlifeSantuaries.jhajjarbachual.add');
    }

    public function savejhajjarbachauli(Request $request){

    		// return $request->all();
    	$validated = $request->validate([
	        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/jhajjarbachauli');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/jhajjarbachauli/' . $filename;

         
        }
   			$save_jhajjarbachauli = new BirJhajjarbachauli();
			$save_jhajjarbachauli->banner_image = $banner_image;
			$save_jhajjarbachauli->banner_heading = $banner_heading;
			$save_jhajjarbachauli->description = $description;
			$save_jhajjarbachauli->wildlife_heading = $wildlife_heading;
			$save_jhajjarbachauli->wildlife_title = $wildlife_title;
			$save_jhajjarbachauli->district = $district;
			$save_jhajjarbachauli->location = $location;
			$save_jhajjarbachauli->area = $area;
			$save_jhajjarbachauli->status_of_land = $status_of_land;
			$save_jhajjarbachauli->important_fauna = $important_fauna;
			$save_jhajjarbachauli->important_flora = $important_flora;
			$save_jhajjarbachauli->save();

			$id = $save_jhajjarbachauli->id;

			$image_data=array();
			if($request->hasFile('pdf_file') != "")
	        {
	            foreach($request->file('pdf_file') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $image = time() . '.' . $file->getClientOriginalName();
					$file->move(public_path() . '/bir_aishwan/', $image);
					$image_data[] = '/bir_aishwan/'.$image;
	            }
			}
			for($i=0;$i<count($image_data);$i++)
	        {
				$save_subheading = new BirJhajjarBachauliNotificationDetail();
				$save_subheading->bir_jhajjarbachauli_id = $id;
				$save_subheading->notification_title = $notificationtitle[$i] ;
				$save_subheading->pdf_url = $image_data[$i] ;
				$save_subheading->save();
		    }

		return redirect('/jhajjarbachauli_pa')->with('success','Bir Jhajjar Bachauli Detail Added Successfully');
    }

    public function editjhajjarbachauli($id){

    	$editjhajjarbachauli = BirJhajjarbachauli::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.jhajjarbachual.edit',compact('editjhajjarbachauli'));
    }

    public function updatejhajjarbachauli(Request $request , $id){

    			// return $request->all();
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/jhajjarbachauli');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/jhajjarbachauli/' . $filename;

            $update_jhajjarbachauli = BirJhajjarbachauli::find($id);
			$update_jhajjarbachauli->banner_image = $banner_image;
			$update_jhajjarbachauli->banner_heading = $banner_heading;
			$update_jhajjarbachauli->description = $description;
			$update_jhajjarbachauli->wildlife_heading = $wildlife_heading;
			$update_jhajjarbachauli->wildlife_title = $wildlife_title;
			$update_jhajjarbachauli->district = $district;
			$update_jhajjarbachauli->location = $location;
			$update_jhajjarbachauli->area = $area;
			$update_jhajjarbachauli->status_of_land = $status_of_land;
			$update_jhajjarbachauli->notification_detail = $notification_detail;
			$update_jhajjarbachauli->important_fauna = $important_fauna;
			$update_jhajjarbachauli->important_flora = $important_flora;
			$update_jhajjarbachauli->save();
         
        }
        else{

        	$update_jhajjarbachauli = BirJhajjarbachauli::find($id);
			$update_jhajjarbachauli->banner_heading = $banner_heading;
			$update_jhajjarbachauli->description = $description;
			$update_jhajjarbachauli->wildlife_heading = $wildlife_heading;
			$update_jhajjarbachauli->wildlife_title = $wildlife_title;
			$update_jhajjarbachauli->district = $district;
			$update_jhajjarbachauli->location = $location;
			$update_jhajjarbachauli->area = $area;
			$update_jhajjarbachauli->status_of_land = $status_of_land;
			$update_jhajjarbachauli->notification_detail = $notification_detail;
			$update_jhajjarbachauli->important_fauna = $important_fauna;
			$update_jhajjarbachauli->important_flora = $important_flora;
			$update_jhajjarbachauli->save();
        }
   		

		return redirect('/jhajjarbachauli_pa')->with('success','Bir Jhajjar Bachauli Detail Update Successfully');
    }

    public function deletejhajjarbachauli($id){

    	
        $update_jhajjarbachauli = BirJhajjarbachauli::find($id);
	    $update_jhajjarbachauli->deleted_status = '1';
        $update_jhajjarbachauli->save();
		return redirect('/jhajjarbachauli_pa')->with('success','Bir Jhajjar Bachauli Detail Deleted Successfully');
    }

     // notification text
	public function jhajjarbachauli_notificationtextListing($id){

		$notification_list = BirJhajjarBachauliNotificationDetail::where('bir_jhajjarbachauli_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.jhajjarbachual.notification_list',compact('notification_list','id'));
	}

	public function jhajjarbachauli_deleteListing($id){

		$delete_listing = BirJhajjarBachauliNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function jhajjarbachauli_editNotificationText($id){

		$edit_notification = BirJhajjarBachauliNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.jhajjarbachual.edit_notification',compact('edit_notification'));
	} 

	public function jhajjarbachauli_updateNotificationText(Request $request , $id){

		$update_notification = BirJhajjarBachauliNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/jhajjarbachauli_notification_list/'.$update_notification->bir_jhajjarbachauli_id)->with('success','Notification Text updated successfully');
	}

	public function jhajjarbachauli_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.jhajjarbachual.add_new_notification',compact('id'));		 
	}

	public function jhajjarbachauli_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirJhajjarBachauliNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_jhajjarbachauli_id = $id;
		$save_notification->save();

		return redirect('/jhajjarbachauli_notification_list/'.$save_notification->bir_jhajjarbachauli_id)->with('success','Notification Text Save successfully');
	}

	public function jhajjarbachauli_notification_pdflist($id){

	$notification_pdf_list = BirJhajjarBachauliNotificationDetail::where('bir_jhajjarbachauli_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.jhajjarbachual.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function jhajjarbachauli_edit_notification_pdf($id){

		$edit = BirJhajjarBachauliNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.jhajjarbachual.edit_notification_pdf',compact('edit','id'));
	}

	public function jhajjarbachauli_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirJhajjarBachauliNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/jhajjarbachauli/', $image);
				$pdf_url = '/jhajjarbachauli/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('jhajjarbachauli_notification_pdf/'.$update_pdf->bir_jhajjarbachauli_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('jhajjarbachauli_notification_pdf/'.$update_pdf->bir_jhajjarbachauli_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function jhajjarbachauli_delete_notification_pdf($id){

		$delete_pdf = BirJhajjarBachauliNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

    public function kathlaur_kushlianList(){

    	$kushlianlist = BirKathlaurKushlian::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach($kushlianlist as $list){

    		$list->text_count = BirKathlaurKushlianNotificationDetail::where('bir_kathlaur_kushlian_id',$list->id)->count();
    		$list->pdf_count = BirKathlaurKushlianNotificationDetail::where('bir_kathlaur_kushlian_id',$list->id)->count();
    	}
    	return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.listing',compact('kushlianlist'));
    }

    public function addkathlaur_kushlian(){

    	return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.add');
    }
    public function savekathlaur_kushlian(Request $request){

    	// return $request->all();
    	$validated = $request->validate([
	              'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/kathlaur_kushlian');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/kathlaur_kushlian/' . $filename;

         
        }
   		    $save_kathlaur_kushlian = new BirKathlaurKushlian();
			$save_kathlaur_kushlian->banner_image = $banner_image;
			$save_kathlaur_kushlian->banner_heading = $banner_heading;
			$save_kathlaur_kushlian->description = $description;
			$save_kathlaur_kushlian->wildlife_heading = $wildlife_heading;
			$save_kathlaur_kushlian->wildlife_title = $wildlife_title;
			$save_kathlaur_kushlian->district = $district;
			$save_kathlaur_kushlian->location = $location;
			$save_kathlaur_kushlian->area = $area;
			$save_kathlaur_kushlian->status_of_land = $status_of_land;
			$save_kathlaur_kushlian->important_fauna = $important_fauna;
			$save_kathlaur_kushlian->important_flora = $important_flora;
			$save_kathlaur_kushlian->save();

			$id = $save_kathlaur_kushlian->id;

			$image_data=array();
			if($request->hasFile('pdf_file') != "")
	        {
	            foreach($request->file('pdf_file') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $image = time() . '.' . $file->getClientOriginalName();
					$file->move(public_path() . '/kathlaur_kushlian/', $image);
					$image_data[] = '/kathlaur_kushlian/'.$image;
	            }
			}
			for($i=0;$i<count($image_data);$i++)
	        {
				$save_subheading = new BirKathlaurKushlianNotificationDetail();
				$save_subheading->bir_kathlaur_kushlian_id = $id;
				$save_subheading->notification_title = $notificationtitle[$i] ;
				$save_subheading->pdf_url = $image_data[$i] ;
				$save_subheading->save();
		    }

		return redirect('/kathlaur_kushlian_pa')->with('success','Bir Kathlaur Kushlian Detail Update Successfully');
    }

    public function editkathlaur_kushlian($id){

    	$editkathlaur_kushlian = BirKathlaurKushlian::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.edit',compact('editkathlaur_kushlian'));
    }

    public function updatekathlaur_kushlian(Request $request ,$id){

		$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	

    	// return $request->all();
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/kathlaur_kushlian');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/kathlaur_kushlian/' . $filename;

            $update_kathlaur_kushlian = BirKathlaurKushlian::find($id);
			$update_kathlaur_kushlian->banner_image = $banner_image;
			$update_kathlaur_kushlian->banner_heading = $banner_heading;
			$update_kathlaur_kushlian->description = $description;
			$update_kathlaur_kushlian->wildlife_heading = $wildlife_heading;
			$update_kathlaur_kushlian->wildlife_title = $wildlife_title;
			$update_kathlaur_kushlian->district = $district;
			$update_kathlaur_kushlian->location = $location;
			$update_kathlaur_kushlian->area = $area;
			$update_kathlaur_kushlian->status_of_land = $status_of_land;
			$update_kathlaur_kushlian->notification_detail = $notification_detail;
			$update_kathlaur_kushlian->important_fauna = $important_fauna;
			$update_kathlaur_kushlian->important_flora = $important_flora;
			$update_kathlaur_kushlian->save();

         
        }
        else{
        	$update_kathlaur_kushlian = BirKathlaurKushlian::find($id);
			$update_kathlaur_kushlian->banner_heading = $banner_heading;
			$update_kathlaur_kushlian->description = $description;
			$update_kathlaur_kushlian->wildlife_heading = $wildlife_heading;
			$update_kathlaur_kushlian->wildlife_title = $wildlife_title;
			$update_kathlaur_kushlian->district = $district;
			$update_kathlaur_kushlian->location = $location;
			$update_kathlaur_kushlian->area = $area;
			$update_kathlaur_kushlian->status_of_land = $status_of_land;
			$update_kathlaur_kushlian->notification_detail = $notification_detail;
			$update_kathlaur_kushlian->important_fauna = $important_fauna;
			$update_kathlaur_kushlian->important_flora = $important_flora;
			$update_kathlaur_kushlian->save();
        }
   		    

		return redirect('/kathlaur_kushlian_pa')->with('success','Bir Kathlaur Kushlian Detail Update Successfully');
    }

    public function deletekathlaur_kushlian($id){

    	$delete_kathlaur_kushlian = BirKathlaurKushlian::find($id);
		$delete_kathlaur_kushlian->deleted_status = '1';
		$delete_kathlaur_kushlian->save();
		return redirect('/kathlaur_kushlian_pa')->with('success','Bir Kathlaur Kushlian Detail Delete Successfully');
    }

       // notification text
	public function kathlaur_notificationtextListing($id){

		$notification_list = BirKathlaurKushlianNotificationDetail::where('bir_kathlaur_kushlian_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.notification_list',compact('notification_list','id'));
	}

	public function kathlaur_deleteListing($id){

		$delete_listing = BirKathlaurKushlianNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function kathlaur_editNotificationText($id){

		$edit_notification = BirKathlaurKushlianNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.edit_notification',compact('edit_notification'));
	} 

	public function kathlaur_updateNotificationText(Request $request , $id){

		$update_notification = BirKathlaurKushlianNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/kathlaur_notification_list/'.$update_notification->bir_kathlaur_kushlian_id)->with('success','Notification Text updated successfully');
	}

	public function kathlaur_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.add_new_notification',compact('id'));		 
	}

	public function kathlaur_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirKathlaurKushlianNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_kathlaur_kushlian_id = $id;
		$save_notification->save();

		return redirect('/kathlaur_notification_list/'.$save_notification->bir_kathlaur_kushlian_id)->with('success','Notification Text Save successfully');
	}

	public function kathlaur_notification_pdflist($id){

	$notification_pdf_list = BirKathlaurKushlianNotificationDetail::where('bir_kathlaur_kushlian_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function kathlaur_edit_notification_pdf($id){

		$edit = BirKathlaurKushlianNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.kathlaurkushlian.edit_notification_pdf',compact('edit','id'));
	}

	public function kathlaur_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirKathlaurKushlianNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/kathlaur_kushlian/', $image);
				$pdf_url = '/kathlaur_kushlian/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('kathlaur_notification_pdf/'.$update_pdf->bir_kathlaur_kushlian_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('kathlaur_notification_pdf/'.$update_pdf->bir_kathlaur_kushlian_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function kathlaur_delete_notification_pdf($id){

		$delete_pdf = BirKathlaurKushlianNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

    public function nangal_wildlifeList(){

    	$nangal_wildlifeList = BirNangalWildlife::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach($nangal_wildlifeList as $list){

    		$list->text_count = BirNangalNotificationDetail::where('bir_nangal_id',$list->id)->count();
    		$list->pdf_count = BirNangalNotificationDetail::where('bir_nangal_id',$list->id)->count();
    	}
    	return view('protectedArea.wildlifeSantuaries.nangalWildlife.listing',compact('nangal_wildlifeList'));
    }

    public function addnangal_wildlife(){

    	return view('protectedArea.wildlifeSantuaries.nangalWildlife.add');
    }
    public function savenangal_wildlife(Request $request){

    	// return $request->all();
    	$validated = $request->validate([
	             'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'location' => 'required',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
    	]);
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/nangal_wildlife');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/nangal_wildlife/' . $filename;

         
        }
   		    $save_nangalwildlife = new BirNangalWildlife();
			$save_nangalwildlife->banner_image = $banner_image;
			$save_nangalwildlife->banner_heading = $banner_heading;
			$save_nangalwildlife->description = $description;
			$save_nangalwildlife->wildlife_heading = $wildlife_heading;
			$save_nangalwildlife->wildlife_title = $wildlife_title;
			$save_nangalwildlife->district = $district;
			$save_nangalwildlife->location = $location;
			$save_nangalwildlife->area = $area;
			$save_nangalwildlife->status_of_land = $status_of_land;
			$save_nangalwildlife->important_fauna = $important_fauna;
			$save_nangalwildlife->important_flora = $important_flora;
			$save_nangalwildlife->save();

			$id = $save_nangalwildlife->id;

			$image_data=array();
			if($request->hasFile('pdf_file') != "")
	        {
	            foreach($request->file('pdf_file') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $image = time() . '.' . $file->getClientOriginalName();
					$file->move(public_path() . '/kathlaur_kushlian/', $image);
					$image_data[] = '/kathlaur_kushlian/'.$image;
	            }
			}
			for($i=0;$i<count($image_data);$i++)
	        {
				$save_subheading = new BirNangalNotificationDetail();
				$save_subheading->bir_nangal_id = $id;
				$save_subheading->notification_title = $notificationtitle[$i] ;
				$save_subheading->pdf_url = $image_data[$i] ;
				$save_subheading->save();
		    }

		return redirect('/nangal_wildlife_pa')->with('success','Bir Nangal Wildlife Detail Save Successfully');
    }

    public function editnangal_wildlife($id){

    	$editnangal_wildlife = BirNangalWildlife::where('id',$id)->first();
    	return view('protectedArea.wildlifeSantuaries.nangalWildlife.edit',compact('editnangal_wildlife'));
    }

    public function updatenangal_wildlife(Request $request , $id){


    	// return $request->all();
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/nangal_wildlife');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/nangal_wildlife/' . $filename;

            $update_nangal_wildlife = BirNangalWildlife::find($id);
			$update_nangal_wildlife->banner_image = $banner_image;
			$update_nangal_wildlife->banner_heading = $banner_heading;
			$update_nangal_wildlife->description = $description;
			$update_nangal_wildlife->wildlife_heading = $wildlife_heading;
			$update_nangal_wildlife->wildlife_title = $wildlife_title;
			$update_nangal_wildlife->district = $district;
			$update_nangal_wildlife->location = $location;
			$update_nangal_wildlife->area = $area;
			$update_nangal_wildlife->status_of_land = $status_of_land;
			$update_nangal_wildlife->notification_detail = $notification_detail;
			$update_nangal_wildlife->important_fauna = $important_fauna;
			$update_nangal_wildlife->important_flora = $important_flora;
         	$update_nangal_wildlife->save();
        }
        else{
        	$update_nangal_wildlife = BirNangalWildlife::find($id);
			$update_nangal_wildlife->banner_heading = $banner_heading;
			$update_nangal_wildlife->description = $description;
			$update_nangal_wildlife->wildlife_heading = $wildlife_heading;
			$update_nangal_wildlife->wildlife_title = $wildlife_title;
			$update_nangal_wildlife->district = $district;
			$update_nangal_wildlife->location = $location;
			$update_nangal_wildlife->area = $area;
			$update_nangal_wildlife->status_of_land = $status_of_land;
			$update_nangal_wildlife->notification_detail = $notification_detail;
			$update_nangal_wildlife->important_fauna = $important_fauna;
			$update_nangal_wildlife->important_flora = $important_flora;
			$update_nangal_wildlife->save();
        }
   		    

		return redirect('/nangal_wildlife_pa')->with('success','Bir Nangal Wildlife Detail Update Successfully');
    }

    public function deletenangal_wildlife($id){
    	
		$delete_nangal_wildlife = BirNangalWildlife::find($id);
		$delete_nangal_wildlife->deleted_status = '1';
		$delete_nangal_wildlife->save();
		return redirect('/nangal_wildlife_pa')->with('success','Bir Nangal Wildlife Detail Delete Successfully');

    } 

      // notification text
	public function nangal_notificationtextListing($id){

		$notification_list = BirNangalNotificationDetail::where('bir_nangal_id',$id)->get();

	return view('protectedArea.wildlifeSantuaries.nangalWildlife.notification_list',compact('notification_list','id'));
	}

	public function nangal_deleteListing($id){

		$delete_listing = BirNangalNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function nangal_editNotificationText($id){

		$edit_notification = BirNangalNotificationDetail::where('id',$id)->first();

	return view('protectedArea.wildlifeSantuaries.nangalWildlife.edit_notification',compact('edit_notification'));
	} 

	public function nangal_updateNotificationText(Request $request , $id){

		$update_notification = BirNangalNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/nangal_notification_list/'.$update_notification->bir_nangal_id)->with('success','Notification Text updated successfully');
	}

	public function nangal_addNewNotificationText($id){

		return view('protectedArea.wildlifeSantuaries.nangalWildlife.add_new_notification',compact('id'));		 
	}

	public function nangal_saveNewNotificationText(Request $request , $id){

		$save_notification = new BirNangalNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->bir_nangal_id = $id;
		$save_notification->save();

		return redirect('/nangal_notification_list/'.$save_notification->bir_nangal_id)->with('success','Notification Text Save successfully');
	}

	public function nangal_notification_pdflist($id){

	$notification_pdf_list = BirNangalNotificationDetail::where('bir_nangal_id',$id)->get();
		return view('protectedArea.wildlifeSantuaries.nangalWildlife.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function nangal_edit_notification_pdf($id){

		$edit = BirNangalNotificationDetail::where('id',$id)->first();
		return view('protectedArea.wildlifeSantuaries.nangalWildlife.edit_notification_pdf',compact('edit','id'));
	}

	public function nangal_update_notification_pdf(Request $request , $id){
		// return $request->all();
		$pdf_url = $request->file('pdf_url');
		$update_pdf = BirNangalNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/nangal_wildlife/', $image);
				$pdf_url = '/nangal_wildlife/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();
	           return redirect('nangal_notification_pdf/'.$update_pdf->bir_nangal_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('nangal_notification_pdf/'.$update_pdf->bir_nangal_id)->with('success','Notification pdf updated successfully');
			}
	}

	public function nangal_delete_notification_pdf($id){

		$delete_pdf = BirNangalNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

    
// end class 
}