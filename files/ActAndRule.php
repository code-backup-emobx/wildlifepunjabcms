<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\ActRuleBanner;
use App\Models\ActRulePdfDetail;
use App\Models\ActRulePdfTwo;
use App\Models\ActRulePdfThree;
use App\Models\ActRuleHeadings;
use App\Models\ActRuleManagementHeading;
use App\Models\ActRuleSubheading;
use Validator;
use Session;

class ActAndRule extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function actRuleList(){
			
		$act_rule_list = ActRuleBanner::where('deleted_status','0')->paginate(10);
		return view('actsRule.listing',compact('act_rule_list'));
	}

	public function addactRule(){

		return view('actsRule.add');
	}
	
	public function saveactRule(Request $request){
	
		// return $request->all;
		 $validated = $request->validate([
            'name' => 'required',
            'pdf_url' => 'required|mimes:pdf',
            'banner_image' => 'required|mimes:jpg,png,jpeg|size:2048',
            'banner_heading' => 'required',
            'wildlife_heading' => 'required',
            'wildlife_title' => 'required',
            'wildlife_description' => 'required',
            // 'management_heading' => 'required',
            // 'management_plan_text' => 'required',
        ]);

		$name = $request->input('name');
		$pdf_url = $request->file('pdf_url');

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');


		$wildlife_heading = $request->input('wildlife_heading');
		$wildlife_title = $request->input('wildlife_title');
		$wildlife_description = $request->input('wildlife_description');
		// $management_heading = $request->input('management_heading');
		// $management_plan_text = $request->input('management_plan_text');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/act_rulepdf/' . $filename;
		}

		$add_act_banner_detail = new ActRuleBanner();
		$add_act_banner_detail->banner_image = $banner_image ;
		$add_act_banner_detail->banner_heading = $banner_heading ;
		$add_act_banner_detail->save();
		$banner_id = $add_act_banner_detail->id;

        $image_data=array();
		if($request->hasFile('pdf_url') != "")
        {
            foreach($request->file('pdf_url') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/act_rulepdf/', $image);
				$image_data[] = '/act_rulepdf/'.$image;
            }
		}

		for($i=0;$i<count($image_data);$i++)
        {
        	$save_act_banner = new ActRulePdfDetail();
        	$save_act_banner->banner_id = $banner_id;
        	$save_act_banner->name=$name[$i];
        	$save_act_banner->pdf_url=$image_data[$i];
        	$save_act_banner->save();
        }


        $save_detail = new ActRuleHeadings();
        $save_detail->banner_id = $banner_id;
        $save_detail->wildlife_heading = $wildlife_heading;
        $save_detail->title = $wildlife_title;
        $save_detail->description = $wildlife_description;
        // $save_detail->management_heading = $management_heading ; 
        // $save_detail->management_nangal_heading	 = $management_plan_text; 
        $save_detail->save() ; 

        return redirect('/act_and_rule')->with('success','uploaded successfully');
		
	}

	public function editactRule($id){

		$editact_rule = ActRuleBanner::where('id',$id)->first();
		return view('actsRule.edit',compact('editact_rule'));
	}

	public function updateactRule(Request $request , $id){
    
    	 $validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|size:2048',
        ]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/act_rulepdf/' . $filename;

            $update_act_banner_detail = ActRuleBanner::find($id);
			$update_act_banner_detail->banner_image = $banner_image;
			$update_act_banner_detail->save();
		}
		else{
			$update_act_banner_detail = ActRuleBanner::find($id);
			$update_act_banner_detail->banner_heading = $banner_heading ;
			$update_act_banner_detail->save();
		}
		
		 return redirect('/act_and_rule')->with('success','Act Rule Updated successfully');
	}

	public function deleteactRule($id){

		$delete_act_banner_detail = ActRuleBanner::find($id);
		$delete_act_banner_detail->deleted_status = '1';
		$delete_act_banner_detail->save();

		return redirect('/act_and_rule')->with('success','Act Rule Deleted successfully');
	}

	public function view_management($id){

	$list = ActRuleManagementHeading::where('act_rule_id',$id)->get();

		 foreach($list as $l){
        $l->total_subheading = ActRuleSubheading::where('management_heading_id',$l->id)->count();

            $l->total_pdf = ActRuleSubheading::where('management_heading_id', $l->id)->count();
        }
        // return $list;

		return view('actsRule.view_management',compact('id','list'));
	}

	public function addNewManagement($id){

		return view('actsRule.addnewmanagement',compact('id'));
	}

	public function saveNewManagement(Request $request , $id){
		// return $request->all();
		$validated = $request->validate([
            'pdf_file' => 'required|max:500000',
            'management_plan_heading' => 'required',
            'subheading' => 'required',
        ]);

		$management_plan_heading = $request->input('management_plan_heading');
		$subheading = $request->input('subheading');
		$pdf_file = $request->file('pdf_file');

		$save_manag_heading = new ActRuleManagementHeading();
		$save_manag_heading->act_rule_id = $id;
		$save_manag_heading->managemt_heading = $management_plan_heading;
		$save_manag_heading->save();

		$management_id = $save_manag_heading->id;

        $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/act_rulepdf/', $image);
				$image_data[] = '/act_rulepdf/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new ActRuleSubheading();
			$save_subheading->management_heading_id = $management_id;
			$save_subheading->subheading = $subheading[$i] ;
			$save_subheading->pdf = $image_data[$i] ;
			$save_subheading->save();
	    }
		return redirect('view_management/'.$id)->with('success','Management Added Successfully');

	}

	public function deleteManagement($id){

		$delete_heading = ActRuleManagementHeading::where('id',$id)->delete();

		$delete_subheading = ActRuleSubheading::where('management_heading_id',$id)->delete();

		return redirect()->back()->with('success','Management Deleted Successfully ');

	}

	public function editManagement($id){

	  $edit_management = ActRuleManagementHeading::with('getsubheading')->where('id',$id)->first();
	  $id = $edit_management->act_rule_id;
	  return view('actsRule.edit_management',compact('edit_management','id'));

	}

	public function updateManagement(Request $request , $id){

		// return $request->all();
		// $validated = $request->validate([
  //           'pdf_file' => 'size:2048',
  //       ]);

		$management_plan_heading = $request->input('management_plan_heading');
		$subheading = $request->input('subheading');
		$pdf_file = $request->file('pdf_file');

		$update_manag_heading = ActRuleManagementHeading::find($id);
		$update_manag_heading->managemt_heading = $management_plan_heading;
		$update_manag_heading->save();

		
	    return redirect('view_management/'.$update_manag_heading->act_rule_id)->with('success','Management Updated Successfully');
	}

	public function listHeading($id){
    
		$listheading = ActRuleSubheading::where('management_heading_id',$id)->paginate(10);

		return view('actsRule.headinglist',compact('listheading','id'));
	}

	public function editSubheading($id){

		$updateSubheading = ActRuleSubheading::where('id',$id)->first();
		$id_m = $updateSubheading->management_heading_id;
		return view('actsRule.editheading',compact('updateSubheading','id_m'));
	}

	public function updateSubheading(Request $request, $id){

		$update_managsub_heading = ActRuleSubheading::find($id);
		$update_managsub_heading->subheading  = $request->subheading;
		$update_managsub_heading->save();

		
	    return redirect('list_mana_heading/'.$request->sub_hed_id)->with('success','Update Management Subheading Successfully');
	}

	public function listpdf($id){


		$listpdf = ActRuleSubheading::where('management_heading_id',$id)->paginate(10);
		return view('actsRule.pdflist',compact('listpdf'));

	}

	public function editSubheadingpdf($id){

		$edit_management_pdf = ActRuleSubheading::where('id',$id)->first();
		$id_m = $edit_management_pdf->management_heading_id;
		return view('actsRule.edit_management_pdf',compact('edit_management_pdf','id_m'));
	 
	}

	public function updateSubheadingpdf(Request $request , $id){

		$pdf = $request->file('pdf');
		if($request->hasFile('pdf') != ""){
        	$filename =$pdf->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf->move($destinationPath, $filename);
            $pdf = '/act_rulepdf/' . $filename;

            $update_act_banner_detail = ActRuleSubheading::find($id);
			$update_act_banner_detail->pdf = $pdf;
			$update_act_banner_detail->save();
		}

		return redirect('list_pdf/'.$request->sub_hed_id)->with('success','Update Management Subheading Successfully');
	}

// end class 
}