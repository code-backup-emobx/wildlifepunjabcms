<?php

use Illuminate\Support\Facades\Route;
//use Hash;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['XSS']], function () {
 Route::get('/', function () {
     return redirect('index');
 });

 Route::get('/aa', function () {
     //return Hash::make('123');
 });
Auth::routes();

Route::get('login',[\App\Http\Controllers\Auth\LoginController::class,'showlogin'])->name('login');
Route::post('login',[\App\Http\Controllers\Auth\LoginController::class,'login'])->name('login');

Route::get('/refresh-captcha', [\App\Http\Controllers\Auth\LoginController::class,'refreshCaptcha']);
Route::post('logout',[\App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');
Route::post('hashing_pass',[\App\Http\Controllers\Auth\LoginController::class,'hashPassword'])->name('hashing_pass');

Route::get('/home',[\App\Http\Controllers\HomeController::class,'index']);
Route::get('/banners_list',[\App\Http\Controllers\HomeController::class,'bannerList']);
Route::get('/add_banner',[\App\Http\Controllers\HomeController::class,'addBanner']);
Route::post('/add_banner',[\App\Http\Controllers\HomeController::class,'saveBanner']);
Route::get('/delete_banner/{id}',[\App\Http\Controllers\HomeController::class,'deleteBanner']);
Route::get('/update_banner/{id}',[\App\Http\Controllers\HomeController::class,'editBanner']);
Route::post('/update_banner/{id}',[\App\Http\Controllers\HomeController::class,'updateBanner']);
Route::get('add_banner_two',[\App\Http\Controllers\HomeController::class,'addBanner_two']);
Route::post('add_banner_two',[\App\Http\Controllers\HomeController::class,'saveBanner_two']);


Route::get('world_day_list',[\App\Http\Controllers\HomeController::class,'worldDayList']);
Route::get('add_worldDay',[\App\Http\Controllers\HomeController::class,'addworldDay']);
Route::post('add_worldDay',[\App\Http\Controllers\HomeController::class,'saveworldDay']);

Route::get('update_worldday/{id}',[\App\Http\Controllers\HomeController::class,'editworldDay']);
Route::post('update_worldday/{id}',[\App\Http\Controllers\HomeController::class,'updateworldDay']);
Route::get('delete_worldday/{id}',[\App\Http\Controllers\HomeController::class,'deleteworldDay']);

Route::get('worldDayDetail',[\App\Http\Controllers\HomeController::class,'addworldDayDetail']);
Route::post('worldDayDetail',[\App\Http\Controllers\HomeController::class,'saveworldDayDetail']);

// minister detail
Route::get('list_minister',[\App\Http\Controllers\HomeController::class,'ministerDetail']);
Route::get('add_ministerdetail',[\App\Http\Controllers\HomeController::class,'addministerDetail']);
Route::post('add_ministerdetail',[\App\Http\Controllers\HomeController::class,'saveministerDetail']);
Route::get('update_minister/{id}',[\App\Http\Controllers\HomeController::class,'editministerDetail']);
Route::post('update_minister/{id}',[\App\Http\Controllers\HomeController::class,'updateministerDetail']);
Route::get('delete_minister/{id}',[\App\Http\Controllers\HomeController::class,'deleteministerDetail']);
// zoo 

Route::get('zoo_list',[\App\Http\Controllers\HomeController::class,'zooListing']);
Route::get('add_zooDetail',[\App\Http\Controllers\HomeController::class,'addzooDetail']);
Route::post('add_zooDetail',[\App\Http\Controllers\HomeController::class,'savezooDetail']);

Route::get('update_zoo/{id}',[\App\Http\Controllers\HomeController::class,'editzooDetail']);
Route::post('update_zoo/{id}',[\App\Http\Controllers\HomeController::class,'updatezooDetail']);
Route::get('delete_zoo/{id}',[\App\Http\Controllers\HomeController::class,'deletezooDetail']);

// zoo gallery 
Route::get('gallery_list',[\App\Http\Controllers\HomeController::class,'zoogallerylist']);
Route::get('add_gallerybanner',[\App\Http\Controllers\HomeController::class,'addgallerybanner']);
Route::post('add_gallerybanner',[\App\Http\Controllers\HomeController::class,'savegallerybanner']);
Route::get('update_gallery_banner/{id}',[\App\Http\Controllers\HomeController::class,'editgallerybanner']);
Route::post('update_gallery_banner/{id}',[\App\Http\Controllers\HomeController::class,'updategallerybanner']);
Route::get('delete_gallery_banner/{id}',[\App\Http\Controllers\HomeController::class,'deletegallerybanner']);

Route::get('add_zooGallery',[\App\Http\Controllers\HomeController::class,'addZooGallery']);
Route::post('add_zooGallery',[\App\Http\Controllers\HomeController::class,'saveZooGallery']);

// upcoming events

// UpcomingEvent
Route::get('event_list',[\App\Http\Controllers\HomeController::class,'eventListing']);
Route::get('add_event',[\App\Http\Controllers\HomeController::class,'eventadd']);
Route::post('add_event',[\App\Http\Controllers\HomeController::class,'saveevent']);
Route::get('update_event/{id}',[\App\Http\Controllers\HomeController::class,'editevent']);
Route::post('update_event/{id}',[\App\Http\Controllers\HomeController::class,'updateevent']);
Route::get('delete_event/{id}',[\App\Http\Controllers\HomeController::class,'deleteevent']);


// Event background Image 
Route::get('event_image',[\App\Http\Controllers\HomeController::class,'eventbgimageListing']);
Route::get('add_eventbgimage',[\App\Http\Controllers\HomeController::class,'eventbgimageadd']);
Route::post('add_eventbgimage',[\App\Http\Controllers\HomeController::class,'eventbgimagesave']);
Route::get('update_eventbgimage/{id}',[\App\Http\Controllers\HomeController::class,'eventbgimageedit']);
Route::post('update_eventbgimage/{id}',[\App\Http\Controllers\HomeController::class,'eventbgimageupdate']);
Route::get('delete_eventbgimage/{id}',[\App\Http\Controllers\HomeController::class,'eventbgimagedelete']);


// Volunteer description 
Route::get('volunteerdetail',[\App\Http\Controllers\HomeController::class,'volunteerdetailListing']);
Route::get('add_volunteerdetail',[\App\Http\Controllers\HomeController::class,'volunteerdetailadd']);
Route::post('add_volunteerdetail',[\App\Http\Controllers\HomeController::class,'volunteerdetailsave']);
Route::get('update_volunteerdetail/{id}',[\App\Http\Controllers\HomeController::class,'volunteerdetailedit']);
Route::post('update_volunteerdetail/{id}',[\App\Http\Controllers\HomeController::class,'volunteerdetailupdate']);
Route::get('delete_volunteerdetail/{id}',[\App\Http\Controllers\HomeController::class,'volunteerdetaildelete']);



// Home Route
Route::get('/home_detail',[\App\Http\Controllers\HomeController::class,'homeDetail']);
Route::get('/add_homepagedetail',[\App\Http\Controllers\HomeController::class,'addHomeDetail']);
Route::post('/add_homepagedetail',[\App\Http\Controllers\HomeController::class,'saveHomeDetail']);
Route::get('/update_homedetail/{id}',[\App\Http\Controllers\HomeController::class,'editHomeDetail']);
Route::post('/update_homedetail/{id}',[\App\Http\Controllers\HomeController::class,'updateHomeDetail']);
Route::get('delete_homedetail/{id}',[\App\Http\Controllers\HomeController::class,'deleteHomeDetail']);

// wildlife Symbol
Route::get('symbol_of_punjab',[\App\Http\Controllers\HomeController::class,'wildlifeDetailListing']);
Route::get('add_wildlifesymbol',[\App\Http\Controllers\HomeController::class,'addwildlifeDetail']);
Route::post('add_wildlifesymbol',[\App\Http\Controllers\HomeController::class,'savewildlifeDetail']);
Route::get('update_wildlifesymbol/{id}',[\App\Http\Controllers\HomeController::class,'editwildlifeDetail']);
Route::post('update_wildlifesymbol/{id}',[\App\Http\Controllers\HomeController::class,'updatewildlifeDetail']);
Route::get('delete_wildlifesymbol/{id}',[\App\Http\Controllers\HomeController::class,'deletewildlifeDetail']);

// videos 
Route::get('videos_list',[\App\Http\Controllers\VideoController::class,'videosList']);
Route::get('add_video',[\App\Http\Controllers\VideoController::class,'addvideos']);
Route::post('add_video',[\App\Http\Controllers\VideoController::class,'savevideos']);
Route::get('delete_video/{id}',[\App\Http\Controllers\VideoController::class,'deleteVideo']);
Route::get('edit_video/{id}',[\App\Http\Controllers\VideoController::class,'editVideo']);
Route::post('edit_video/{id}',[\App\Http\Controllers\VideoController::class,'updateVideo']);

// protected Area
	// Wildlife Santuaries
		// Bir Moti Bagh
Route::get('bir_motibagh_pa',[\App\Http\Controllers\ProtectedArea::class,'birMotiBghList'])->name('bir_motibagh');
Route::get('add_birmotibagh',[\App\Http\Controllers\ProtectedArea::class,'addBirmotibagh']);
Route::post('add_birmotibagh',[\App\Http\Controllers\ProtectedArea::class,'saveBirmotibagh']);
Route::get('edit_birmotibagh/{id}',[\App\Http\Controllers\ProtectedArea::class,'editBirmotibagh']);
Route::post('edit_birmotibagh/{id}',[\App\Http\Controllers\ProtectedArea::class,'updateBirmotibagh']);
Route::get('delete_birmotibagh/{id}',[\App\Http\Controllers\ProtectedArea::class,'deleteBirmotibagh']);

// 
Route::get('notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'notificationtextListing']);
Route::get('delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'deleteListing']);

Route::get('edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'editNotificationText']);
Route::post('edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'updateNotificationText']);

Route::get('add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'addNewNotificationText']);
Route::post('add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'saveNewNotificationText']);

Route::get('notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'notification_pdflist']);
Route::get('edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'edit_notification_pdf']);
Route::post('edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'update_notification_pdf']);
Route::get('delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'delete_notification_pdf']);

    // Gurdiaplur 
Route::get('bir_gurdiaplur_pa',[\App\Http\Controllers\ProtectedArea::class,'bir_gurdiaplurpaList']);   
Route::get('add_gurdiaplur',[\App\Http\Controllers\ProtectedArea::class,'addbir_gurdiaplurpa']); 
Route::post('add_gurdiaplur',[\App\Http\Controllers\ProtectedArea::class,'savebir_gurdiaplurpa']);
Route::get('update_gurdiaplur/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbir_gurdiaplurpa']);
Route::post('update_gurdiaplur/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebir_gurdiaplurpa']);
Route::get('delete_gurdiaplur/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebir_gurdiaplurpa']);

//
Route::get('gur_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_notificationtextListing']);
Route::get('gur_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_deleteListing']);

Route::get('gur_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_editNotificationText']);
Route::post('gur_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_updateNotificationText']);

Route::get('gur_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_addNewNotificationText']);
Route::post('add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_saveNewNotificationText']);

Route::get('gur_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_notification_pdflist']);
Route::get('gur_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_edit_notification_pdf']);
Route::post('gur_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_update_notification_pdf']); 
Route::get('gur_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'gur_delete_notification_pdf']);
// 

  // bhunerheri
Route::get('/bir_bhunerheri_pa',[\App\Http\Controllers\ProtectedArea::class,'birBhunerheriList']);
Route::get('/add_bhunerheri',[\App\Http\Controllers\ProtectedArea::class,'addbirBhunerheri']);
Route::post('/add_bhunerheri',[\App\Http\Controllers\ProtectedArea::class,'savebirBhunerheri']);
Route::get('update_birbhunerheri/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbirBhunerheri']);
Route::post('update_birbhunerheri/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebirBhunerheri']);
Route::get('delete_birbhunerheri/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebirBhunerheri']);

// 
Route::get('bhunerheri_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_notificationtextListing']);
Route::get('bhunerheri_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_deleteListing']);

Route::get('bhunerheri_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_editNotificationText']);
Route::post('bhunerheri_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_updateNotificationText']);

Route::get('bhunerheri_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_addNewNotificationText']);
Route::post('bhunerheri_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_saveNewNotificationText']);

Route::get('bhunerheri_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_notification_pdflist']);
Route::get('bhunerheri_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_edit_notification_pdf']);
Route::post('bhunerheri_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_update_notification_pdf']);
Route::get('bhunerheri_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhunerheri_delete_notification_pdf']);
// 

	//mehas 
Route::get('bir_mehas_pa',[\App\Http\Controllers\ProtectedArea::class,'bir_mehasList']);
Route::get('add_bir_mehas_pa',[\App\Http\Controllers\ProtectedArea::class,'addbir_mehas']);
Route::post('add_bir_mehas_pa',[\App\Http\Controllers\ProtectedArea::class,'savebir_mehas']);
Route::get('edit_birmehas/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbir_mehas']);
Route::post('edit_birmehas/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebir_mehas']);
Route::get('delete_birmehas/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebir_mehas']);
// 

Route::get('mehas_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_notificationtextListing']);
Route::get('mehas_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_deleteListing']);

Route::get('mehas_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_editNotificationText']);
Route::post('mehas_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_updateNotificationText']);

Route::get('mehas_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_addNewNotificationText']);
Route::post('mehas_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_saveNewNotificationText']);

Route::get('mehas_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_notification_pdflist']);
Route::get('mehas_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_edit_notification_pdf']);
Route::post('mehas_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_update_notification_pdf']);
Route::get('mehas_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'mehas_delete_notification_pdf']);
// 


  //dosanjh
Route::get('bir_dosanjh_pa',[\App\Http\Controllers\ProtectedArea::class,'dosanjhList']);
Route::get('add_bir_dosanjh',[\App\Http\Controllers\ProtectedArea::class,'adddosanjh']);
Route::post('add_bir_dosanjh',[\App\Http\Controllers\ProtectedArea::class,'savedosanjh']);
Route::get('update_dosanjh/{id}',[\App\Http\Controllers\ProtectedArea::class,'editdosanjh']);
Route::post('update_dosanjh/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatedosanjh']);
Route::get('delete_dosanjh/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletedosanjh']);
// 
Route::get('dosanjh_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_notificationtextListing']);
Route::get('dosanjh_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_deleteListing']);

Route::get('dosanjh_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_editNotificationText']);
Route::post('dosanjh_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_updateNotificationText']);

Route::get('dosanjh_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_addNewNotificationText']);
Route::post('dosanjh_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_saveNewNotificationText']);

Route::get('dosanjh_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_notification_pdflist']);
Route::get('dosanjh_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_edit_notification_pdf']);
Route::post('dosanjh_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_update_notification_pdf']);
Route::get('dosanjh_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'dosanjh_delete_notification_pdf']);
// 

// bhadson
Route::get('bir_bhadson_pa',[\App\Http\Controllers\ProtectedArea::class,'birBhadsonList']);
Route::get('add_bhadson',[\App\Http\Controllers\ProtectedArea::class,'addbhadson']);
Route::post('add_bhadson',[\App\Http\Controllers\ProtectedArea::class,'savebhadson']);
Route::get('update_birbhadson/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbhadson']);
Route::post('update_birbhadson/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebhadson']);
Route::get('delete_birbhadson/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebhadson']);
// 

Route::get('bhadson_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_notificationtextListing']);
Route::get('bhadson_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_deleteListing']);

Route::get('bhadson_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_editNotificationText']);
Route::post('bhadson_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_updateNotificationText']);

Route::get('bhadson_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_addNewNotificationText']);
Route::post('bhadson_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_saveNewNotificationText']);

Route::get('bhadson_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_notification_pdflist']);
Route::get('bhadson_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_edit_notification_pdf']);
Route::post('bhadson_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_update_notification_pdf']);
Route::get('bhadson_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'bhadson_delete_notification_pdf']);
// 

	// aishwan 
Route::get('bir_aishwan_pa',[\App\Http\Controllers\ProtectedArea::class,'bir_aishwanDetail']);
Route::get('add_bir_aishwan',[\App\Http\Controllers\ProtectedArea::class,'addbir_aishwan']);
Route::post('add_bir_aishwan',[\App\Http\Controllers\ProtectedArea::class,'savebir_aishwan']);
Route::get('update_biraishwan/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbir_aishwan']);
Route::post('update_biraishwan/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebir_aishwan']);
Route::get('delete_biraishwan/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebir_aishwan']);
// 
Route::get('aishwan_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_notificationtextListing']);
Route::get('aishwan_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_deleteListing']);

Route::get('aishwan_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_editNotificationText']);
Route::post('aishwan_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_updateNotificationText']);

Route::get('aishwan_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_addNewNotificationText']);
Route::post('aishwan_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_saveNewNotificationText']);

Route::get('aishwan_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_notification_pdflist']);
Route::get('aishwan_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_edit_notification_pdf']);
Route::post('aishwan_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_update_notification_pdf']);
Route::get('aishwan_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'aishwan_delete_notification_pdf']);
// 
	
	// abohar 
Route::get('bir_abohar_pa',[\App\Http\Controllers\ProtectedArea::class,'bir_aboharDetail']);
Route::get('add_bir_abohar',[\App\Http\Controllers\ProtectedArea::class,'addbir_abohar']);
Route::post('add_bir_abohar',[\App\Http\Controllers\ProtectedArea::class,'savebir_abohar']);
Route::get('update_birabohar/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbir_abohar']);
Route::post('update_birabohar/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebir_abohar']);
Route::get('delete_birabohar/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebir_abohar']);
// 
Route::get('abohar_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_notificationtextListing']);
Route::get('abohar_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_deleteListing']);

Route::get('abohar_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_editNotificationText']);
Route::post('abohar_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_updateNotificationText']);

Route::get('abohar_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_addNewNotificationText']);
Route::post('abohar_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_saveNewNotificationText']);

Route::get('abohar_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_notification_pdflist']);
Route::get('abohar_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_edit_notification_pdf']);
Route::post('abohar_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_update_notification_pdf']);
Route::get('abohar_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'abohar_delete_notification_pdf']);
// 


	// Harike
Route::get('bir_harike_pa',[\App\Http\Controllers\ProtectedArea::class,'bir_harikeDetail']);
Route::get('add_bir_harike',[\App\Http\Controllers\ProtectedArea::class,'addbir_harike']);
Route::post('add_bir_harike',[\App\Http\Controllers\ProtectedArea::class,'savebir_harike']);
Route::get('update_harike/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbir_harike']);
Route::post('update_harike/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebir_harike']);
Route::get('delete_harike/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebir_harike']);

// 
Route::get('harike_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_notificationtextListing']);
Route::get('harike_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_deleteListing']);

Route::get('harike_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_editNotificationText']);
Route::post('harike_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_updateNotificationText']);

Route::get('harike_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_addNewNotificationText']);
Route::post('harike_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_saveNewNotificationText']);

Route::get('harike_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_notification_pdflist']);
Route::get('harike_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_edit_notification_pdf']);
Route::post('harike_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_update_notification_pdf']);
Route::get('harike_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'harike_delete_notification_pdf']);
// 

	//Takhni Rehmpur 
Route::get('takhni_rehmpur_pa',[\App\Http\Controllers\ProtectedArea::class,'bir_takhni_rehmpur']);
Route::get('add_takhni_rehmpur_pa',[\App\Http\Controllers\ProtectedArea::class,'addbir_takhni_rehmpur']);
Route::post('add_takhni_rehmpur_pa',[\App\Http\Controllers\ProtectedArea::class,'savebir_takhni_rehmpur']);
Route::get('edit_takhnipur/{id}',[\App\Http\Controllers\ProtectedArea::class,'editbir_takhni_rehmpur']);
Route::post('edit_takhnipur/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatebir_takhni_rehmpur']);
Route::get('delete_takhnipur/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletebir_takhni_rehmpur']);

// 
Route::get('takhni_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_notificationtextListing']);
Route::get('takhni_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_deleteListing']);

Route::get('takhni_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_editNotificationText']);
Route::post('takhni_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_updateNotificationText']);

Route::get('takhni_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_addNewNotificationText']);
Route::post('takhni_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_saveNewNotificationText']);

Route::get('takhni_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_notification_pdflist']);
Route::get('takhni_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_edit_notification_pdf']);
Route::post('takhni_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_update_notification_pdf']);
Route::get('takhni_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'takhni_delete_notification_pdf']);
// 

	// jhajjarbachauli_pa
Route::get('jhajjarbachauli_pa',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauliList']);
Route::get('add_bir_jhajjarbachauli',[\App\Http\Controllers\ProtectedArea::class,'addjhajjarbachauli']);
Route::post('add_bir_jhajjarbachauli',[\App\Http\Controllers\ProtectedArea::class,'savejhajjarbachauli']);
Route::get('update_jhajjarbachauli/{id}',[\App\Http\Controllers\ProtectedArea::class,'editjhajjarbachauli']);
Route::post('update_jhajjarbachauli/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatejhajjarbachauli']);
Route::get('delete_jhajjarbachauli/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletejhajjarbachauli']);

// 
Route::get('jhajjarbachauli_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_notificationtextListing']);
Route::get('jhajjarbachauli_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_deleteListing']);

Route::get('jhajjarbachauli_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_editNotificationText']);
Route::post('jhajjarbachauli_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_updateNotificationText']);

Route::get('jhajjarbachauli_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_addNewNotificationText']);
Route::post('jhajjarbachauli_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_saveNewNotificationText']);

Route::get('jhajjarbachauli_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_notification_pdflist']);
Route::get('jhajjarbachauli_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_edit_notification_pdf']);
Route::post('jhajjarbachauli_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_update_notification_pdf']);
Route::get('jhajjarbachauli_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'jhajjarbachauli_delete_notification_pdf']);
// 

   // kathlaur_kushlian
Route::get('kathlaur_kushlian_pa',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_kushlianList']);
Route::get('add_bir_kushlian',[\App\Http\Controllers\ProtectedArea::class,'addkathlaur_kushlian']);
Route::post('add_bir_kushlian',[\App\Http\Controllers\ProtectedArea::class,'savekathlaur_kushlian']);
Route::get('update_kushlian/{id}',[\App\Http\Controllers\ProtectedArea::class,'editkathlaur_kushlian']);
Route::post('update_kushlian/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatekathlaur_kushlian']);
Route::get('delete_kushlian/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletekathlaur_kushlian']);

// 
Route::get('kathlaur_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_notificationtextListing']);
Route::get('kathlaur_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_deleteListing']);

Route::get('kathlaur_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_editNotificationText']);
Route::post('kathlaur_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_updateNotificationText']);

Route::get('kathlaur_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_addNewNotificationText']);
Route::post('kathlaur_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_saveNewNotificationText']);

Route::get('kathlaur_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_notification_pdflist']);
Route::get('kathlaur_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_edit_notification_pdf']);
Route::post('kathlaur_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_update_notification_pdf']);
Route::get('kathlaur_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'kathlaur_delete_notification_pdf']);
// 

   // nangal_wildlife
Route::get('nangal_wildlife_pa',[\App\Http\Controllers\ProtectedArea::class,'nangal_wildlifeList']);
Route::get('add_bir_nangal',[\App\Http\Controllers\ProtectedArea::class,'addnangal_wildlife']);
Route::post('add_bir_nangal',[\App\Http\Controllers\ProtectedArea::class,'savenangal_wildlife']);
Route::get('update_nangal/{id}',[\App\Http\Controllers\ProtectedArea::class,'editnangal_wildlife']);
Route::post('update_nangal/{id}',[\App\Http\Controllers\ProtectedArea::class,'updatenangal_wildlife']);
Route::get('delete_nangal/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletenangal_wildlife']);
// Route::get('delete_kushlian/{id}',[\App\Http\Controllers\ProtectedArea::class,'deletekathlaur_kushlian']);

// 

Route::get('nangal_notification_list/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_notificationtextListing']);
Route::get('nangal_delete_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_deleteListing']);

Route::get('nangal_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_editNotificationText']);
Route::post('nangal_edit_notification_text/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_updateNotificationText']);

Route::get('nangal_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_addNewNotificationText']);
Route::post('nangal_add_new_notification/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_saveNewNotificationText']);

Route::get('nangal_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_notification_pdflist']);
Route::get('nangal_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_edit_notification_pdf']);
Route::post('nangal_edit_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_update_notification_pdf']);
Route::get('nangal_delete_notification_pdf/{id}',[\App\Http\Controllers\ProtectedArea::class,'nangal_delete_notification_pdf']);
// 

// Community Reserve 
    // PanniWala 
Route::get('panniwalalist',[\App\Http\Controllers\CommunityReserve::class,'panniwalaListing']);
Route::get('add_panniwala',[\App\Http\Controllers\CommunityReserve::class,'addPanniwala']);
Route::post('add_panniwala',[\App\Http\Controllers\CommunityReserve::class,'savePanniwala']);
Route::get('update_panniwala/{id}',[\App\Http\Controllers\CommunityReserve::class,'editPanniwala']);
Route::post('update_panniwala/{id}',[\App\Http\Controllers\CommunityReserve::class,'updatePanniwala']);
Route::get('delete_panniwala/{id}',[\App\Http\Controllers\CommunityReserve::class,'deletePanniwala']);

// 
Route::get('panniwala_notification_list/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_notificationtextListing']);
Route::get('panniwala_delete_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_deleteListing']);

Route::get('panniwala_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_editNotificationText']);
Route::post('panniwala_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_updateNotificationText']);

Route::get('panniwala_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_addNewNotificationText']);
Route::post('panniwala_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_saveNewNotificationText']);

Route::get('panniwala_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_notification_pdflist']);
Route::get('panniwala_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_edit_notification_pdf']);
Route::post('panniwala_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_update_notification_pdf']);
Route::get('panniwala_delete_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'panniwala_delete_notification_pdf']);
//
	
	//lalwanlist
Route::get('lalwanlist',[\App\Http\Controllers\CommunityReserve::class,'lalwanListing']);
Route::get('add_lalwan',[\App\Http\Controllers\CommunityReserve::class,'addlalwan']);
Route::post('add_lalwan',[\App\Http\Controllers\CommunityReserve::class,'savelalwan']);
Route::get('update_lalwan/{id}',[\App\Http\Controllers\CommunityReserve::class,'editlalwan']);
Route::post('update_lalwan/{id}',[\App\Http\Controllers\CommunityReserve::class,'updatelalwan']);
Route::get('delete_lalwan/{id}',[\App\Http\Controllers\CommunityReserve::class,'deletelalwan']);

// 
Route::get('lalwan_notification_list/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_notificationtextListing']);
Route::get('lalwan_delete_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_deleteListing']);

Route::get('lalwan_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_editNotificationText']);
Route::post('lalwan_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_updateNotificationText']);

Route::get('lalwan_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_addNewNotificationText']);
Route::post('lalwan_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_saveNewNotificationText']);

Route::get('lalwan_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_notification_pdflist']);
Route::get('lalwan_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_edit_notification_pdf']);
Route::post('lalwan_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_update_notification_pdf']);
Route::get('lalwan_delete_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'lalwan_delete_notification_pdf']);
// 

	// Keshopur Miani
Route::get('keshpurmianilist',[\App\Http\Controllers\CommunityReserve::class,'keshpurmilist']);
Route::get('add_keshpurmi',[\App\Http\Controllers\CommunityReserve::class,'addkeshpurmi']);
Route::post('add_keshpurmi',[\App\Http\Controllers\CommunityReserve::class,'savekeshpurmi']);
Route::get('update_keshpurmi/{id}',[\App\Http\Controllers\CommunityReserve::class,'editkeshpurmi']);
Route::post('update_keshpurmi/{id}',[\App\Http\Controllers\CommunityReserve::class,'updatekeshpurmi']);
Route::get('delete_keshpurmi/{id}',[\App\Http\Controllers\CommunityReserve::class,'deletekeshpurmi']);

// 
Route::get('keshopur_notification_list/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_notificationtextListing']);
Route::get('keshopur_delete_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_deleteListing']);

Route::get('keshopur_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_editNotificationText']);
Route::post('keshopur_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_updateNotificationText']);

Route::get('keshopur_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_addNewNotificationText']);
Route::post('keshopur_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_saveNewNotificationText']);

Route::get('keshopur_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_notification_pdflist']);
Route::get('keshopur_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_edit_notification_pdf']);
Route::post('keshopur_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_update_notification_pdf']);
Route::get('keshopur_delete_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'keshopur_delete_notification_pdf']);
// 


	// Siswan Community
Route::get('siswanlist',[\App\Http\Controllers\CommunityReserve::class,'siswanList']);
Route::get('add_siswan',[\App\Http\Controllers\CommunityReserve::class,'addsiswan']);
Route::post('add_siswan',[\App\Http\Controllers\CommunityReserve::class,'savesiswan']);
Route::get('update_siswan/{id}',[\App\Http\Controllers\CommunityReserve::class,'editsiswan']);
Route::post('update_siswan/{id}',[\App\Http\Controllers\CommunityReserve::class,'updatesiswan']);
Route::get('delete_siswan/{id}',[\App\Http\Controllers\CommunityReserve::class,'deletesiswan']);

// 
Route::get('siswan_notification_list/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_notificationtextListing']);
Route::get('siswan_delete_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_deleteListing']);

Route::get('siswan_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_editNotificationText']);
Route::post('siswan_edit_notification_text/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_updateNotificationText']);

Route::get('siswan_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_addNewNotificationText']);
Route::post('siswan_add_new_notification/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_saveNewNotificationText']);

Route::get('siswan_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_notification_pdflist']);
Route::get('siswan_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_edit_notification_pdf']);
Route::post('siswan_edit_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_update_notification_pdf']);
Route::get('siswan_delete_notification_pdf/{id}',[\App\Http\Controllers\CommunityReserve::class,'siswan_delete_notification_pdf']);
// 

	// Conservation Reserve
	   // Rakh Sarai
Route::get('rakh_sarai',[\App\Http\Controllers\ConservationReserve::class,'rakhsaraiListing']);
Route::get('add_rakhsarai',[\App\Http\Controllers\ConservationReserve::class,'addrakhsarai']);
Route::post('add_rakhsarai',[\App\Http\Controllers\ConservationReserve::class,'saverakhsarai']);
Route::get('update_rakhsarai/{id}',[\App\Http\Controllers\ConservationReserve::class,'editrakhsarai']);
Route::post('update_rakhsarai/{id}',[\App\Http\Controllers\ConservationReserve::class,'updaterakhsarai']);
Route::get('delete_rakhsarai/{id}',[\App\Http\Controllers\ConservationReserve::class,'deleterakhsarai']);

// 
Route::get('rakh_notification_list/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_notificationtextListing']);
Route::get('rakh_delete_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_deleteListing']);

Route::get('rakh_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_editNotificationText']);
Route::post('rakh_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_updateNotificationText']);

Route::get('rakh_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_addNewNotificationText']);
Route::post('rakh_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_saveNewNotificationText']);

Route::get('rakh_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_notification_pdflist']);
Route::get('rakh_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_edit_notification_pdf']);
Route::post('rakh_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_update_notification_pdf']);
Route::get('rakh_delete_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'rakh_delete_notification_pdf']);
// 

	// ropar
Route::get('cr_roparlist',[\App\Http\Controllers\ConservationReserve::class,'roparList']);
Route::get('add_roparwetland',[\App\Http\Controllers\ConservationReserve::class,'addropar']);	
Route::post('add_roparwetland',[\App\Http\Controllers\ConservationReserve::class,'saveropar']);
Route::get('update_roparwetland/{id}',[\App\Http\Controllers\ConservationReserve::class,'editropar']);
Route::post('update_roparwetland/{id}',[\App\Http\Controllers\ConservationReserve::class,'updateropar']);
Route::get('delete_roparwetland/{id}',[\App\Http\Controllers\ConservationReserve::class,'deleteropar']);

// 
Route::get('ropar_notification_list/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_notificationtextListing']);
Route::get('ropar_delete_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_deleteListing']);

Route::get('ropar_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_editNotificationText']);
Route::post('ropar_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_updateNotificationText']);

Route::get('ropar_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_addNewNotificationText']);
Route::post('ropar_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_saveNewNotificationText']);

Route::get('ropar_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_notification_pdflist']);
Route::get('ropar_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_edit_notification_pdf']);
Route::post('ropar_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_update_notification_pdf']);
Route::get('ropar_delete_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ropar_delete_notification_pdf']);
// 

	
	// ranjit sagar dam
Route::get('cr_ranjitsagar',[\App\Http\Controllers\ConservationReserve::class,'rangitsagarList']);
Route::get('add_ranjitsagar',[\App\Http\Controllers\ConservationReserve::class,'addrangitsagar']);
Route::post('add_ranjitsagar',[\App\Http\Controllers\ConservationReserve::class,'saverangitsagar']);
Route::get('update_ranjitsagar/{id}',[\App\Http\Controllers\ConservationReserve::class,'editrangitsagar']);
Route::post('update_ranjitsagar/{id}',[\App\Http\Controllers\ConservationReserve::class,'updaterangitsagar']);
Route::get('delete_ranjitsagar/{id}',[\App\Http\Controllers\ConservationReserve::class,'deleterangitsagar']);

// 
Route::get('ranjit_notification_list/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_notificationtextListing']);
Route::get('ranjit_delete_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_deleteListing']);

Route::get('ranjit_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_editNotificationText']);
Route::post('ranjit_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_updateNotificationText']);

Route::get('ranjit_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_addNewNotificationText']);
Route::post('ranjit_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_saveNewNotificationText']);

Route::get('ranjit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_notification_pdflist']);
Route::get('ranjit_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_edit_notification_pdf']);
Route::post('ranjit_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_update_notification_pdf']);
Route::get('ranjit_delete_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'ranjit_delete_notification_pdf']);
// 

	// Beas River
Route::get('cr_beasriver',[\App\Http\Controllers\ConservationReserve::class,'beasRiverList']);
Route::get('add_beasriver',[\App\Http\Controllers\ConservationReserve::class,'addbeasRiver']);
Route::post('add_beasriver',[\App\Http\Controllers\ConservationReserve::class,'savebeasRiver']);
Route::get('update_beasriver/{id}',[\App\Http\Controllers\ConservationReserve::class,'editbeasRiver']);
Route::post('update_beasriver/{id}',[\App\Http\Controllers\ConservationReserve::class,'updatebeasRiver']);
Route::get('delete_beasriver/{id}',[\App\Http\Controllers\ConservationReserve::class,'deletebeasRiver']);

// 
Route::get('beas_notification_list/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_notificationtextListing']);
Route::get('beas_delete_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_deleteListing']);

Route::get('beas_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_editNotificationText']);
Route::post('beas_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_updateNotificationText']);

Route::get('beas_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_addNewNotificationText']);
Route::post('beas_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_saveNewNotificationText']);

Route::get('beas_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_notification_pdflist']);
Route::get('beas_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_edit_notification_pdf']);
Route::post('beas_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_update_notification_pdf']);
Route::get('beas_delete_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'beas_delete_notification_pdf']);
// 

  
	// kalibean
Route::get('cr_kalibean',[\App\Http\Controllers\ConservationReserve::class,'kalibeanList']);
Route::get('add_kalibean',[\App\Http\Controllers\ConservationReserve::class,'addkalibean']);
Route::post('add_kalibean',[\App\Http\Controllers\ConservationReserve::class,'savekalibean']);
Route::get('update_kalibean/{id}',[\App\Http\Controllers\ConservationReserve::class,'editkalibean']);
Route::post('update_kalibean/{id}',[\App\Http\Controllers\ConservationReserve::class,'updatekalibean']);
Route::get('delete_kalibean/{id}',[\App\Http\Controllers\ConservationReserve::class,'deletekalibean']);
// 
Route::get('kalibean_notification_list/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_notificationtextListing']);
Route::get('kalibean_delete_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_deleteListing']);

Route::get('kalibean_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_editNotificationText']);
Route::post('kalibean_edit_notification_text/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_updateNotificationText']);

Route::get('kalibean_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_addNewNotificationText']);
Route::post('kalibean_add_new_notification/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_saveNewNotificationText']);

Route::get('kalibean_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_notification_pdflist']);
Route::get('kalibean_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_edit_notification_pdf']);
Route::post('kalibean_edit_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_update_notification_pdf']);
Route::get('kalibean_delete_notification_pdf/{id}',[\App\Http\Controllers\ConservationReserve::class,'kalibean_delete_notification_pdf']);
// 

  // Protected Wetland
Route::get('harike_wildlife',[\App\Http\Controllers\ProtectedWetland::class,'harikeWildlifeList']);
Route::get('add_harike_wildlife',[\App\Http\Controllers\ProtectedWetland::class,'addharikeWildlife']);
Route::post('add_harike_wildlife',[\App\Http\Controllers\ProtectedWetland::class,'saveharikeWildlife']);
Route::get('update_harika_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'editharikeWildlife']);
Route::post('update_harika_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'updateharikeWildlife']);
Route::get('delete_harika_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'deleteharikeWildlife']);


 //nangal_wildlife_santuaries

Route::get('nangal_wildlife_santuaries',[\App\Http\Controllers\ProtectedWetland::class,'nangalwildlifesantuariesList']);
Route::get('add_nangal_wildlife',[\App\Http\Controllers\ProtectedWetland::class,'addnangalwildlife']);
Route::post('add_nangal_wildlife',[\App\Http\Controllers\ProtectedWetland::class,'savenangalwildlife']);
Route::get('update_nangal_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'editnangalwildlife']);
Route::post('update_nangal_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'updatenangalwildlife']);
Route::get('delete_nangal_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'deletenangalwildlife']);

// keshopur

Route::get('keshopur_santuaries',[\App\Http\Controllers\ProtectedWetland::class,'keshopursantuariesList']);
Route::get('add_keshopur',[\App\Http\Controllers\ProtectedWetland::class,'addkeshopur']);
Route::post('add_keshopur',[\App\Http\Controllers\ProtectedWetland::class,'savekeshopur']);
Route::get('update_keshopur_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'editkeshopurdetail']);
Route::post('update_keshopur_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'updatekeshopurdetail']);
Route::get('delete_keshopur_detail/{id}',[\App\Http\Controllers\ProtectedWetland::class,'deletekeshopurdetail']);

// Zoo
	// chhatbir
Route::get('chattbir_zoo_list',[\App\Http\Controllers\Zoo::class,'chattbirZooList']);
Route::get('add_chhatbirzoo',[\App\Http\Controllers\Zoo::class,'addchattbirZoo']);
Route::post('add_chhatbirzoo',[\App\Http\Controllers\Zoo::class,'savechattbirZoo']);
Route::get('update_chattbir_zoo/{id}',[\App\Http\Controllers\Zoo::class,'editchattbirZoo']);
Route::post('update_chattbir_zoo/{id}',[\App\Http\Controllers\Zoo::class,'updatechattbirZoo']);
Route::get('delete_chattbir_zoo/{id}',[\App\Http\Controllers\Zoo::class,'deletechattbirZoo']);

	// Ludhiana Zoo
Route::get('ludhiana_zoo_list',[\App\Http\Controllers\Zoo::class,'ludhianaZooList']);
Route::get('add_ludhianazoo',[\App\Http\Controllers\Zoo::class,'addludhianaZoo']);
Route::post('add_ludhianazoo',[\App\Http\Controllers\Zoo::class,'saveludhianaZoo']);
Route::get('update_ludhiana_zoo/{id}',[\App\Http\Controllers\Zoo::class,'editludhianaZoo']);
Route::post('update_ludhiana_zoo/{id}',[\App\Http\Controllers\Zoo::class,'updateludhianaZoo']);
Route::get('delete_ludhiana_zoo/{id}',[\App\Http\Controllers\Zoo::class,'deleteludhianaZoo']);

	// patiala Zoo
Route::get('patiala_zoo_list',[\App\Http\Controllers\Zoo::class,'patialaZooList']);
Route::get('add_patialazoo',[\App\Http\Controllers\Zoo::class,'addpatialaZoo']);
Route::post('add_patialazoo',[\App\Http\Controllers\Zoo::class,'savepatialaZoo']);
Route::get('update_patiala_zoo/{id}',[\App\Http\Controllers\Zoo::class,'editpatialaZoo']);
Route::post('update_patiala_zoo/{id}',[\App\Http\Controllers\Zoo::class,'updatepatialaZoo']);
Route::get('delete_patiala_zoo/{id}',[\App\Http\Controllers\Zoo::class,'deletepatialaZoo']);

	// bathinda Zoo
Route::get('bathinda_zoo_list',[\App\Http\Controllers\Zoo::class,'bathindaZooList']);
Route::get('add_bathindazoo',[\App\Http\Controllers\Zoo::class,'addbathindaZoo']);
Route::post('add_bathindazoo',[\App\Http\Controllers\Zoo::class,'savebathindaZoo']);
Route::get('update_bathinda_zoo/{id}',[\App\Http\Controllers\Zoo::class,'editbathindaZoo']);
Route::post('update_bathinda_zoo/{id}',[\App\Http\Controllers\Zoo::class,'updatebathindaZoo']);
Route::get('delete_bathinda_zoo/{id}',[\App\Http\Controllers\Zoo::class,'deletebathindaZoo']);

	// Deer Park Neelon
Route::get('deerpark_zoo_list',[\App\Http\Controllers\Zoo::class,'deerparkZooList']);
Route::get('add_deerparkneelonzoo',[\App\Http\Controllers\Zoo::class,'adddeerparkZoo']);
Route::post('add_deerparkneelonzoo',[\App\Http\Controllers\Zoo::class,'savedeerparkZoo']);
Route::get('update_deerpark_zoo/{id}',[\App\Http\Controllers\Zoo::class,'editdeerparkZoo']);
Route::post('update_deerpark_zoo/{id}',[\App\Http\Controllers\Zoo::class,'updatedeerparkZoo']);
Route::get('delete_deerpark_zoo/{id}',[\App\Http\Controllers\Zoo::class,'deletedeerparkZoo']);

// Acts & Rules
Route::get('act_and_rule',[\App\Http\Controllers\ActAndRule::class,'actRuleList']);
Route::get('addactrule',[\App\Http\Controllers\ActAndRule::class,'addactRule']);
Route::post('addactrule',[\App\Http\Controllers\ActAndRule::class,'saveactRule']);
Route::get('update_actrulebanner/{id}',[\App\Http\Controllers\ActAndRule::class,'editactRule']);
Route::post('update_actrulebanner/{id}',[\App\Http\Controllers\ActAndRule::class,'updateactRule']);
Route::get('delete_actrulebanner/{id}',[\App\Http\Controllers\ActAndRule::class,'deleteactRule']);
// organisation
Route::get('organisation_list',[\App\Http\Controllers\OrganisationChart::class,'organisationList']);
Route::get('add_organisation',[\App\Http\Controllers\OrganisationChart::class,'addorganisation']);
Route::post('add_organisation',[\App\Http\Controllers\OrganisationChart::class,'saveorganisation']);
Route::get('add_org_detail/{id}',[\App\Http\Controllers\OrganisationChart::class,'addorganisationDetail']);
Route::post('add_org_detail/{id}',[\App\Http\Controllers\OrganisationChart::class,'saveorganisationDetail']);
Route::get('update_org_detail/{id}',[\App\Http\Controllers\OrganisationChart::class,'editorganisationDetail']);
Route::post('update_org_detail/{id}',[\App\Http\Controllers\OrganisationChart::class,'updateorganisationDetail']);
Route::get('delete_org_detail/{id}',[\App\Http\Controllers\OrganisationChart::class,'deleteorganisationDetail']);

// tourism
Route::get('tourism_detail',[\App\Http\Controllers\Tourism::class,'tourismList']);
Route::get('add_tourism',[\App\Http\Controllers\Tourism::class,'addtourism']);
Route::post('add_tourism',[\App\Http\Controllers\Tourism::class,'savetourism']);
Route::get('update_tourism/{id}',[\App\Http\Controllers\Tourism::class,'edittourism']);
Route::post('update_tourism/{id}',[\App\Http\Controllers\Tourism::class,'updateTourism']);
Route::get('add_gallery/{id}',[\App\Http\Controllers\Tourism::class,'addtourismgallery']);
Route::post('add_gallery/{id}',[\App\Http\Controllers\Tourism::class,'savetourismgallery']);
Route::get('edit_detail/{id}',[\App\Http\Controllers\Tourism::class,'editTourismDetail']);
Route::post('edit_detail/{id}',[\App\Http\Controllers\Tourism::class,'updateTourismDetail']);
Route::get('view_detail/{id}',[\App\Http\Controllers\Tourism::class,'tourismDetail']);
Route::get('delete_tourism/{id}',[\App\Http\Controllers\Tourism::class,'deletetourismgallery']);
Route::get('delete_detail/{id}',[\App\Http\Controllers\Tourism::class,'deletetourismDetail']);

// footer 
Route::get('scheme_detail',[\App\Http\Controllers\FooterController::class,'scheme_detailList']);
Route::get('add_scheme',[\App\Http\Controllers\FooterController::class,'addschemedetail']);
Route::post('add_scheme',[\App\Http\Controllers\FooterController::class,'saveschemedetail']);
Route::get('update_scheme/{id}',[\App\Http\Controllers\FooterController::class,'editchemedetail']);
Route::post('update_scheme/{id}',[\App\Http\Controllers\FooterController::class,'updatechemedetail']);
Route::get('delete_scheme/{id}',[\App\Http\Controllers\FooterController::class,'deletechemedetail']);

// about-us
Route::get('about_us_detail',[\App\Http\Controllers\FooterController::class,'about_usList']);
Route::get('add_about',[\App\Http\Controllers\FooterController::class,'addabout_us']);
Route::post('add_about',[\App\Http\Controllers\FooterController::class,'saveabout_us']);
Route::get('update_about/{id}',[\App\Http\Controllers\FooterController::class,'edit_about_us']);
Route::post('update_about/{id}',[\App\Http\Controllers\FooterController::class,'update_about_us']);
Route::get('delete_about/{id}',[\App\Http\Controllers\FooterController::class,'delete_about_us']);
// policy guideline

Route::get('policy_guideline',[\App\Http\Controllers\FooterController::class,'policyguidelineList']);
Route::get('add_policy_guideline',[\App\Http\Controllers\FooterController::class,'addpolicyguideline']);
Route::post('add_policy_guideline',[\App\Http\Controllers\FooterController::class,'savepolicyguideline']);
Route::get('update_pdf/{id}',[\App\Http\Controllers\FooterController::class,'editpolicyguideline']);
Route::post('update_pdf/{id}',[\App\Http\Controllers\FooterController::class,'updatepolicyguideline']);
Route::get('delete_pdf/{id}',[\App\Http\Controllers\FooterController::class,'deletepolicyguideline']);

// Notification
Route::get('notification_detail',[\App\Http\Controllers\FooterController::class,'notificationDetail']);
Route::get('add_notifiaction',[\App\Http\Controllers\FooterController::class,'addNotification']);
Route::post('add_notifiaction',[\App\Http\Controllers\FooterController::class,'saveNotification']);
Route::get('update_notification/{id}',[\App\Http\Controllers\FooterController::class,'editNotification']);
Route::post('update_notification/{id}',[\App\Http\Controllers\FooterController::class,'updateNotification']);
Route::get('delete_notification/{id}',[\App\Http\Controllers\FooterController::class,'deleteNotification']);

// Tender Detail 
Route::get('tender_detail',[\App\Http\Controllers\FooterController::class,'tenderDetail']);
Route::get('add_tender_detail',[\App\Http\Controllers\FooterController::class,'addtenderDetail']);
Route::post('add_tender_detail',[\App\Http\Controllers\FooterController::class,'savetenderDetail']);
Route::get('add_tenderDetail/{id}',[\App\Http\Controllers\FooterController::class,'addTender']);
Route::post('add_tenderDetail/{id}',[\App\Http\Controllers\FooterController::class,'saveTender']);

Route::get('update_tender/{id}',[\App\Http\Controllers\FooterController::class,'editTender']);
Route::post('update_tender/{id}',[\App\Http\Controllers\FooterController::class,'updateTender']);
Route::get('delete_tender/{id}',[\App\Http\Controllers\FooterController::class,'deleteTender']);

// IMP LINK
Route::get('imp_list',[\App\Http\Controllers\FooterController::class,'impList']);
Route::get('add_implink',[\App\Http\Controllers\FooterController::class,'addImpLink']);
Route::post('add_implink',[\App\Http\Controllers\FooterController::class,'saveImpLink']);
Route::get('update_implink/{id}',[\App\Http\Controllers\FooterController::class,'editImpLink']);
Route::post('update_implink/{id}',[\App\Http\Controllers\FooterController::class,'updateImpLink']);
Route::get('delete_implink/{id}',[\App\Http\Controllers\FooterController::class,'deleteImpLink']);

// services
Route::get('services_list',[\App\Http\Controllers\FooterController::class,'servicesList']);
Route::get('add_services',[\App\Http\Controllers\FooterController::class,'addservices']);
Route::post('add_services',[\App\Http\Controllers\FooterController::class,'saveservices']);
Route::get('update_services/{id}',[\App\Http\Controllers\FooterController::class,'editservices']);
Route::post('update_services/{id}',[\App\Http\Controllers\FooterController::class,'updateservices']);
Route::get('delete_services/{id}',[\App\Http\Controllers\FooterController::class,'deleteservices']);

// contact-us
Route::get('contact_us',[\App\Http\Controllers\FooterController::class,'contactUslist']);
Route::get('add_contactbanner',[\App\Http\Controllers\FooterController::class,'addcontactBanner']);
Route::post('add_contactbanner',[\App\Http\Controllers\FooterController::class,'savecontactBanner']);
Route::get('add_contactDetail',[\App\Http\Controllers\FooterController::class,'addContactDetail']);
Route::post('add_contactDetail',[\App\Http\Controllers\FooterController::class,'saveContactDetail']);

//************************ frontend route *******************************//

Route::name('front.')->middleware('visitor')->group(function() {
Route::get('/index',[\App\Http\Controllers\FrontendController::class,'index']);
Route::get('/home_page',[\App\Http\Controllers\FrontendController::class,'homePage']);
Route::get('/wildlife_symbol',[\App\Http\Controllers\FrontendController::class,'wildlifeSymbol']);

// protected Wetland
Route::get('/bir_motibagh',[\App\Http\Controllers\FrontendController::class,'birMotiBagh']);
Route::get('/bir_gurdialpur',[\App\Http\Controllers\FrontendController::class,'birGurdialpur']);
Route::get('/bir_bhunerheri',[\App\Http\Controllers\FrontendController::class,'bir_bhunerheri']);
Route::get('/bir_mehas_detail',[\App\Http\Controllers\FrontendController::class,'bir_menhasDetail']);
Route::get('/bir_dosanjh_detail',[\App\Http\Controllers\FrontendController::class,'bir_dosanjhDetail']);
Route::get('/bir_bhadson_detail',[\App\Http\Controllers\FrontendController::class,'bir_bhadsonDetail']);
Route::get('/bir_aishwan_detail',[\App\Http\Controllers\FrontendController::class,'bir_aishwandetail']);
Route::get('/bir_abohar_detail',[\App\Http\Controllers\FrontendController::class,'bir_abohardetail']);
Route::get('/bir_harike_detail',[\App\Http\Controllers\FrontendController::class,'bir_harikedetail']);
Route::get('/bir_takhni_rehmapur',[\App\Http\Controllers\FrontendController::class,'bir_takhni_rehmapurdetail']);
Route::get('/bir_jhajjar_bachauli',[\App\Http\Controllers\FrontendController::class,'bir_jhajjar_bachaulidetail']);
Route::get('/bir_kathlaurkushlian_detail',[\App\Http\Controllers\FrontendController::class,'bir_kathlaurkushlian_detail']);
Route::get('/bir_nangal_wildlife_detail',[\App\Http\Controllers\FrontendController::class,'bir_nangal_wildlife_detail']);

//Community Reserve 
Route::get('/panni-wala',[\App\Http\Controllers\FrontendController::class,'panniwalaDetail']);
Route::get('/lalwan_detail',[\App\Http\Controllers\FrontendController::class,'lalwanDetail']);
Route::get('/keshopurdetail',[\App\Http\Controllers\FrontendController::class,'keshopurDetail']);
Route::get('/siswan_detail',[\App\Http\Controllers\FrontendController::class,'siswanDetail']);
// Conservative Reserves
Route::get('/rakh_sarai-detail',[\App\Http\Controllers\FrontendController::class,'rakhsaraiDetail']);
Route::get('/ropar_wetland_detail',[\App\Http\Controllers\FrontendController::class,'roparetlandDetail']);
Route::get('/ranjit_sagar_dam_detail',[\App\Http\Controllers\FrontendController::class,'ranjitsagarDamDetail']);
Route::get('/beas_river_dam_detail',[\App\Http\Controllers\FrontendController::class,'beasRiverDetail']);
Route::get('/kali_bein_detail',[\App\Http\Controllers\FrontendController::class,'kalibeinDetail']);

Route::get('harika_santuary',[\App\Http\Controllers\FrontendController::class,'harikasantuaryDetail']);
Route::get('/nangal_santuary',[\App\Http\Controllers\FrontendController::class,'nangalsantuaryDetail']);
Route::get('/keshopur_detail',[\App\Http\Controllers\FrontendController::class,'keshopur_detail']);
//zoo
Route::get('/chhhatbir_zoo_detail',[\App\Http\Controllers\FrontendController::class,'chhhatbirbirzoo_Detail']);

Route::get('/ludhiana_zoo_detail',[\App\Http\Controllers\FrontendController::class,'ludhianazoo_Detail']);
Route::get('/patiala_zoo_detail',[\App\Http\Controllers\FrontendController::class,'patialazoo_Detail']);
Route::get('/bathinda_zoo_detail',[\App\Http\Controllers\FrontendController::class,'bathindazoo_Detail']);
Route::get('/deerpark_zoo_detail',[\App\Http\Controllers\FrontendController::class,'deerparkzoo_Detail']);

// footer
Route::get('/schemeDetail',[\App\Http\Controllers\FrontendController::class,'schemedetail']);
Route::get('/aboutus_detail',[\App\Http\Controllers\FrontendController::class,'aboutusdetail']);
// Route::get('/policy_guideline_pdf',[\App\Http\Controllers\FrontendController::class,'policygdetail']);

Route::get('/notification_details',[\App\Http\Controllers\FrontendController::class,'notificationdetail']);

Route::get('/organisation_chart_detail',[\App\Http\Controllers\FrontendController::class,'organisationdetail']);

Route::get('/tourism_details',[\App\Http\Controllers\FrontendController::class,'tourismdetail']);
Route::get('/tourism_page/{id}',[\App\Http\Controllers\FrontendController::class,'tourismGallerydetail']);
Route::get('/tenderDetail',[\App\Http\Controllers\FrontendController::class,'tenderDetails']);

Route::get('/view_gallery',[\App\Http\Controllers\FrontendController::class,'viewGalleryDetail']);
Route::get('getzoo_image',[\App\Http\Controllers\FrontendController::class,'getZooImages']);

Route::get('contact_us',[\App\Http\Controllers\FrontendController::class,'contactUsDetail']);
Route::post('contactus_message',[\App\Http\Controllers\FrontendController::class,'contactUsMessage']);

Route::get('worldday_detail/{id}',[\App\Http\Controllers\FrontendController::class,'getDetail']);

Route::post('how_to_apply_detail',[\App\Http\Controllers\FrontendController::class,'howToApplySave']);

Route::get('imp_link',[\App\Http\Controllers\FrontendController::class,'impLinkDetail']);

Route::get('service_detail',[\App\Http\Controllers\FrontendController::class,'serviceDetail']);

Route::post('services_how_to_apply',[\App\Http\Controllers\FrontendController::class,'serviceHowApply']);

Route::post('add_services',[\App\Http\Controllers\FrontendController::class,'addserviceHowApply']);

Route::get('/tasks',[\App\Http\Controllers\FrontendController::class,'exportCsvServices']);

Route::get('/acts_rule_detail',[\App\Http\Controllers\FrontendController::class,'actsRuleDetail']);

Route::get('calendar',[\App\Http\Controllers\FrontendController::class,'calendarDetail']);

Route::get('not_found',[\App\Http\Controllers\FrontendController::class,'notFound']);

Route::get('demo_page',[\App\Http\Controllers\FrontendController::class,'demoPage']);
Route::get('view_all_videos',[\App\Http\Controllers\FrontendController::class,'view_all_videos']);
Route::get('get_all_videos',[\App\Http\Controllers\FrontendController::class,'getAllVideos']);

});
});


