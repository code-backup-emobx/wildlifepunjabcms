<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\PanniwalaGumjal;
use App\Models\LalwanCommunity;
use App\Models\KeshPurmi;
use App\Models\SiswanCommunity;

use App\Models\PanniwalaGumjalNotificationDetail;
use App\Models\LalwanCommunityNotificationDetail;
use App\Models\KesopurDetailNotificationDetail;
use App\Models\SiswanNotificationDetail;

class CommunityReserve extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function panniwalaListing(){
			
		$listing_panniwala = PanniwalaGumjal::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($listing_panniwala as $list){

			$list->text_count = PanniwalaGumjalNotificationDetail::where('panniwala_id',$list->id)->count();
			$list->pdf_count = PanniwalaGumjalNotificationDetail::where('panniwala_id',$list->id)->count();
		}
		return view('protectedArea.communityReserve.panniwala.listing',compact('listing_panniwala'));
	}

	public function addPanniwala(){

		return view('protectedArea.communityReserve.panniwala.add');
	}

	public function savePanniwala(Request $request){

		// return $request->all();
		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
        'pdf_file' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/panniwala');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/panniwala/' . $filename;
		}

		$save_panniwala = new PanniwalaGumjal();
		$save_panniwala->banner_image = $banner_image;
		$save_panniwala->banner_heading = $banner_heading;
		$save_panniwala->description = $description;
		$save_panniwala->wildlife_heading = $wildlife_heading;
		$save_panniwala->wildlife_title = $wildlife_title;
		$save_panniwala->district = $district;
		$save_panniwala->location = $location;
		$save_panniwala->area = $area;
		$save_panniwala->status_of_land = $status_of_land;
		$save_panniwala->important_fauna = $important_fauna;
		$save_panniwala->important_flora = $important_flora;
		$save_panniwala->save();

		$id = $save_panniwala->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/panniwala/', $image);
				$image_data[] = '/panniwala/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new PanniwalaGumjalNotificationDetail();
			$save_subheading->panniwala_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }


		return redirect('/panniwalalist')->with('success','Panni Wala Community Reserve Detail Added Successfully');
	}

	public function editPanniwala($id){

		$edit_panniwala = PanniwalaGumjal::where('id',$id)->first();
		return view('protectedArea.communityReserve.panniwala.edit',compact('edit_panniwala'));
	}

	public function updatePanniwala(Request $request , $id){

		 // return $request->all();
			
    		$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/panniwala');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/panniwala/' . $filename;

				$update_panniwala = PanniwalaGumjal::find($id);
				$update_panniwala->banner_image = $banner_image;
				$update_panniwala->save();
		}else{

			$update_panniwala = PanniwalaGumjal::find($id);
			$update_panniwala->banner_heading = $banner_heading;
			$update_panniwala->description = $description;
			$update_panniwala->wildlife_heading = $wildlife_heading;
			$update_panniwala->wildlife_title = $wildlife_title;
			$update_panniwala->district = $district;
			$update_panniwala->location = $location;
			$update_panniwala->area = $area;
			$update_panniwala->status_of_land = $status_of_land;
			$update_panniwala->notification_detail = $notification_detail;
			$update_panniwala->important_fauna = $important_fauna;
			$update_panniwala->important_flora = $important_flora;
			$update_panniwala->save();
		}	

		return redirect('/panniwalalist')->with('success','Panni Wala Community Reserve Detail Updated Successfully');
	}
	
	public function deletePanniwala($id){

		$delete_panniwala = PanniwalaGumjal::find($id);
		$delete_panniwala->deleted_status = '1';
		$delete_panniwala->save();
		return redirect('/panniwalalist')->with('success','Panni Wala Community Reserve Detail Delete Successfully');
	}

	// notification text
	public function panniwala_notificationtextListing($id){

		$notification_list = PanniwalaGumjalNotificationDetail::where('panniwala_id',$id)->get();

	return view('protectedArea.communityReserve.panniwala.notification_list',compact('notification_list','id'));
	}

	public function panniwala_deleteListing($id){

		$delete_listing = PanniwalaGumjalNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function panniwala_editNotificationText($id){

		$edit_notification = PanniwalaGumjalNotificationDetail::where('id',$id)->first();

	return view('protectedArea.communityReserve.panniwala.edit_notification',compact('edit_notification'));
	}

	public function panniwala_updateNotificationText(Request $request , $id){

		$update_notification = PanniwalaGumjalNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/panniwala_notification_list/'.$update_notification->panniwala_id)->with('success','Notification Text updated successfully');
	}

	public function panniwala_addNewNotificationText($id){

		return view('protectedArea.communityReserve.panniwala.add_new_notification',compact('id'));		 
	}

	public function panniwala_saveNewNotificationText(Request $request , $id){

		$save_notification = new PanniwalaGumjalNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->panniwala_id = $id;
		$save_notification->save();

		return redirect('/panniwala_notification_list/'.$save_notification->panniwala_id)->with('success','Notification Text Save successfully');
	}

	public function panniwala_notification_pdflist($id){

		$notification_pdf_list = PanniwalaGumjalNotificationDetail::where('panniwala_id',$id)->get();
		return view('protectedArea.communityReserve.panniwala.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function panniwala_edit_notification_pdf($id){

		$edit = PanniwalaGumjalNotificationDetail::where('id',$id)->first();
		return view('protectedArea.communityReserve.panniwala.edit_notification_pdf',compact('edit','id'));
	}

	public function panniwala_update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$update_pdf = PanniwalaGumjalNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/panniwala/', $image);
				$pdf_url = '/panniwala/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

	           return redirect('panniwala_notification_pdf/'.$update_pdf->panniwala_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('panniwala_notification_pdf/'.$update_pdf->panniwala_id)->with('success','Notification pdf updated successfully');
			}

			// return redirect('/notification_pdf/'.$update_pdf->panniwala_id)->with('success','Notification Text updated successfully');
	}

	public function panniwala_delete_notification_pdf($id){

		$delete_pdf = PanniwalaGumjalNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function lalwanListing(){

		$lalwancommunity = LalwanCommunity::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($lalwancommunity as $list){

			$list->text_count = LalwanCommunityNotificationDetail::where('lalwan_id',$list->id)->count();
			$list->pdf_count = LalwanCommunityNotificationDetail::where('lalwan_id',$list->id)->count();
		}
		return view('protectedArea.communityReserve.lalwanCommunity.listing',compact('lalwancommunity'));
	}

	public function addlalwan(){

		return view('protectedArea.communityReserve.lalwanCommunity.add');
	}
	public function savelalwan(Request $request){

		// return $request->all();

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/lalwan');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/lalwan/' . $filename;
		}

		$save_lalwancommunity = new LalwanCommunity();
		$save_lalwancommunity->banner_image = $banner_image;
		$save_lalwancommunity->banner_heading = $banner_heading;
		$save_lalwancommunity->description = $description;
		$save_lalwancommunity->wildlife_heading = $wildlife_heading;
		$save_lalwancommunity->wildlife_title = $wildlife_title;
		$save_lalwancommunity->district = $district;
		$save_lalwancommunity->location = $location;
		$save_lalwancommunity->area = $area;
		$save_lalwancommunity->status_of_land = $status_of_land;
		$save_lalwancommunity->important_fauna = $important_fauna;
		$save_lalwancommunity->important_flora = $important_flora;
		$save_lalwancommunity->save();

		$id = $save_lalwancommunity->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/panniwala/', $image);
				$image_data[] = '/panniwala/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new LalwanCommunityNotificationDetail();
			$save_subheading->lalwan_id  = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/lalwanlist')->with('success','Lalwan Community Reserve Detail Added Successfully');		
	}

	public function editlalwan($id){

		$edit_lalwan = LalwanCommunity::where('id',$id)->first();
		return view('protectedArea.communityReserve.lalwanCommunity.edit',compact('edit_lalwan'));		
	}

	public function updatelalwan(Request $request , $id){

		// return $request->all();
    		
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);		
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/lalwan');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/lalwan/' . $filename;

            $update_lalwancommunity = LalwanCommunity::find($id);
			$update_lalwancommunity->banner_image = $banner_image;
			$update_lalwancommunity->banner_heading = $banner_heading;
			$update_lalwancommunity->description = $description;
			$update_lalwancommunity->wildlife_heading = $wildlife_heading;
			$update_lalwancommunity->wildlife_title = $wildlife_title;
			$update_lalwancommunity->district = $district;
			$update_lalwancommunity->location = $location;
			$update_lalwancommunity->area = $area;
			$update_lalwancommunity->status_of_land = $status_of_land;
			$update_lalwancommunity->notification_detail = $notification_detail;
			$update_lalwancommunity->important_fauna = $important_fauna;
			$update_lalwancommunity->important_flora = $important_flora;
			$update_lalwancommunity->save();
		}
		else{
			$update_lalwancommunity = LalwanCommunity::find($id);
			$update_lalwancommunity->banner_heading = $banner_heading;
			$update_lalwancommunity->description = $description;
			$update_lalwancommunity->wildlife_heading = $wildlife_heading;
			$update_lalwancommunity->wildlife_title = $wildlife_title;
			$update_lalwancommunity->district = $district;
			$update_lalwancommunity->location = $location;
			$update_lalwancommunity->area = $area;
			$update_lalwancommunity->status_of_land = $status_of_land;
			$update_lalwancommunity->notification_detail = $notification_detail;
			$update_lalwancommunity->important_fauna = $important_fauna;
			$update_lalwancommunity->important_flora = $important_flora;
			$update_lalwancommunity->save();
		}

		return redirect('/lalwanlist')->with('success','Lalwan Community Reserve Detail Updated Successfully');	
	}

	public function deletelalwan($id){

		$delete_lalwan = LalwanCommunity::find($id);
		$delete_lalwan->deleted_status = '1';
		$delete_lalwan->save();
		return redirect('/lalwanlist')->with('success','Lalwan Community Reserve Detail Deleted Successfully');	
	}

	// notification text
	public function lalwan_notificationtextListing($id){

		$notification_list = LalwanCommunityNotificationDetail::where('lalwan_id',$id)->get();

	return view('protectedArea.communityReserve.lalwanCommunity.notification_list',compact('notification_list','id'));
	}

	public function lalwan_deleteListing($id){

		$delete_listing = LalwanCommunityNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function lalwan_editNotificationText($id){

		$edit_notification = LalwanCommunityNotificationDetail::where('id',$id)->first();

	return view('protectedArea.communityReserve.lalwanCommunity.edit_notification',compact('edit_notification'));
	}

	public function lalwan_updateNotificationText(Request $request , $id){

		$update_notification = LalwanCommunityNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/lalwan_notification_list/'.$update_notification->lalwan_id)->with('success','Notification Text updated successfully');
	}

	public function lalwan_addNewNotificationText($id){

		return view('protectedArea.communityReserve.lalwanCommunity.add_new_notification',compact('id'));		 
	}

	public function lalwan_saveNewNotificationText(Request $request , $id){

		$save_notification = new LalwanCommunityNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->lalwan_id = $id;
		$save_notification->save();

		return redirect('/lalwan_notification_list/'.$save_notification->lalwan_id)->with('success','Notification Text Save successfully');
	}

	public function lalwan_notification_pdflist($id){

		$notification_pdf_list = LalwanCommunityNotificationDetail::where('lalwan_id',$id)->get();
		return view('protectedArea.communityReserve.lalwanCommunity.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function lalwan_edit_notification_pdf($id){

		$edit = LalwanCommunityNotificationDetail::where('id',$id)->first();
		return view('protectedArea.communityReserve.lalwanCommunity.edit_notification_pdf',compact('edit','id'));
	}

	public function lalwan_update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$update_pdf = LalwanCommunityNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/lalwan/', $image);
				$pdf_url = '/lalwan/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

	           return redirect('lalwan_notification_pdf/'.$update_pdf->lalwan_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('lalwan_notification_pdf/'.$update_pdf->lalwan_id)->with('success','Notification pdf updated successfully');
			}

			// return redirect('/lalwan_notification_pdf/'.$update_pdf->lalwan_id)->with('success','Notification Text updated successfully');
	}

	public function lalwan_delete_notification_pdf($id){

		$delete_pdf = LalwanCommunityNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function keshpurmilist(){

		$keshpurmi = KeshPurmi::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($keshpurmi as $list){

			$list->text_count = KesopurDetailNotificationDetail::where('kesopur_id',$list->id)->count();
			$list->pdf_count = KesopurDetailNotificationDetail::where('kesopur_id',$list->id)->count();
		}
		return view('protectedArea.communityReserve.keshpurmi.listing',compact('keshpurmi'));
	}

	public function addkeshpurmi(){

		return view('protectedArea.communityReserve.keshpurmi.add');
	}

	public function savekeshpurmi(Request $request){

		// return $request->all();
		$validated = $request->validate([
         'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/keshpurmi');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/keshpurmi/' . $filename;
		}

		$save_keshpurmi = new KeshPurmi();
		$save_keshpurmi->banner_image = $banner_image;
		$save_keshpurmi->banner_heading = $banner_heading;
		$save_keshpurmi->description = $description;
		$save_keshpurmi->wildlife_heading = $wildlife_heading;
		$save_keshpurmi->wildlife_title = $wildlife_title;
		$save_keshpurmi->district = $district;
		$save_keshpurmi->location = $location;
		$save_keshpurmi->area = $area;
		$save_keshpurmi->status_of_land = $status_of_land;
		$save_keshpurmi->important_fauna = $important_fauna;
		$save_keshpurmi->important_flora = $important_flora;
		$save_keshpurmi->save();

		$id = $save_keshpurmi->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/panniwala/', $image);
				$image_data[] = '/panniwala/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new KesopurDetailNotificationDetail();
			$save_subheading->kesopur_id  = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }


		return redirect('/keshpurmianilist')->with('success','Keshpurmi Community Reserve Detail Added Successfully');	
	}
	public function editkeshpurmi($id){

		$editkeshpurmi = KeshPurmi::where('id',$id)->first();
		return view('protectedArea.communityReserve.keshpurmi.edit',compact('editkeshpurmi'));
	}

	public function updatekeshpurmi(Request $request , $id){

		// return $request->all();
    
    			
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/keshpurmi');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/keshpurmi/' . $filename;
            $update_keshpurmi = KeshPurmi::find($id);
			$update_keshpurmi->banner_image = $banner_image;
			$update_keshpurmi->banner_heading = $banner_heading;
			$update_keshpurmi->description = $description;
			$update_keshpurmi->wildlife_heading = $wildlife_heading;
			$update_keshpurmi->wildlife_title = $wildlife_title;
			$update_keshpurmi->district = $district;
			$update_keshpurmi->location = $location;
			$update_keshpurmi->area = $area;
			$update_keshpurmi->status_of_land = $status_of_land;
			$update_keshpurmi->notification_detail = $notification_detail;
			$update_keshpurmi->important_fauna = $important_fauna;
			$update_keshpurmi->important_flora = $important_flora;
			$update_keshpurmi->save();
		}
		else{
			$update_keshpurmi = KeshPurmi::find($id);
			$update_keshpurmi->banner_heading = $banner_heading;
			$update_keshpurmi->description = $description;
			$update_keshpurmi->wildlife_heading = $wildlife_heading;
			$update_keshpurmi->wildlife_title = $wildlife_title;
			$update_keshpurmi->district = $district;
			$update_keshpurmi->location = $location;
			$update_keshpurmi->area = $area;
			$update_keshpurmi->status_of_land = $status_of_land;
			$update_keshpurmi->notification_detail = $notification_detail;
			$update_keshpurmi->important_fauna = $important_fauna;
			$update_keshpurmi->important_flora = $important_flora;
			$update_keshpurmi->save();
		}
		
		return redirect('/keshpurmianilist')->with('success','Keshpurmi Community Reserve Detail Added Successfully');		
	}

	public function deletekeshpurmi($id){

		$delete_keshpurmi = KeshPurmi::find($id);
		$delete_keshpurmi->deleted_status = '1';
		$delete_keshpurmi->save();
		return redirect('/keshpurmianilist')->with('success','Keshpurmi Community Reserve Detail Deleted Successfully');	
	}

	// notification text
	public function keshopur_notificationtextListing($id){

		$notification_list = KesopurDetailNotificationDetail::where('kesopur_id',$id)->get();

	return view('protectedArea.communityReserve.keshpurmi.notification_list',compact('notification_list','id'));
	}

	public function keshopur_deleteListing($id){

		$delete_listing = KesopurDetailNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function keshopur_editNotificationText($id){

		$edit_notification = KesopurDetailNotificationDetail::where('id',$id)->first();

	return view('protectedArea.communityReserve.keshpurmi.edit_notification',compact('edit_notification'));
	}

	public function keshopur_updateNotificationText(Request $request , $id){

		$update_notification = KesopurDetailNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/keshopur_notification_list/'.$update_notification->kesopur_id)->with('success','Notification Text updated successfully');
	}

	public function keshopur_addNewNotificationText($id){

		return view('protectedArea.communityReserve.keshpurmi.add_new_notification',compact('id'));		 
	}

	public function keshopur_saveNewNotificationText(Request $request , $id){

		$save_notification = new KesopurDetailNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->kesopur_id = $id;
		$save_notification->save();

		return redirect('/notification_list/'.$save_notification->kesopur_id)->with('success','Notification Text Save successfully');
	}

	public function keshopur_notification_pdflist($id){

		$notification_pdf_list = KesopurDetailNotificationDetail::where('kesopur_id',$id)->get();
		return view('protectedArea.communityReserve.keshpurmi.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function keshopur_edit_notification_pdf($id){

		$edit = KesopurDetailNotificationDetail::where('id',$id)->first();
		return view('protectedArea.communityReserve.keshpurmi.edit_notification_pdf',compact('edit','id'));
	}

	public function keshopur_update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$update_pdf = KesopurDetailNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/keshpurmi/', $image);
				$pdf_url = '/keshpurmi/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

	           return redirect('keshopur_notification_pdf/'.$update_pdf->kesopur_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('keshopur_notification_pdf/'.$update_pdf->kesopur_id)->with('success','Notification pdf updated successfully');
			}

			// return redirect('/notification_pdf/'.$update_pdf->kesopur_id)->with('success','Notification Text updated successfully');
	}

	public function keshopur_delete_notification_pdf($id){

		$delete_pdf = KesopurDetailNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}

	public function siswanList(){

		$siswan_listing = SiswanCommunity::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($siswan_listing as $list){

			$list->text_count = SiswanNotificationDetail::where('siswan_id',$list->id)->count();
			$list->pdf_count = SiswanNotificationDetail::where('siswan_id',$list->id)->count();
		}
		return view('protectedArea.communityReserve.siswan.listing',compact('siswan_listing'));		
	}

	public function addsiswan(){

		return view('protectedArea.communityReserve.siswan.add');
	}

	public function savesiswan(Request $request){

			// return $request->all();
		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
    	]);
    	
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/siswan_community');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/siswan_community/' . $filename;
		}

		$save_siswancommunity = new SiswanCommunity();
		$save_siswancommunity->banner_image = $banner_image;
		$save_siswancommunity->banner_heading = $banner_heading;
		$save_siswancommunity->description = $description;
		$save_siswancommunity->wildlife_heading = $wildlife_heading;
		$save_siswancommunity->wildlife_title = $wildlife_title;
		$save_siswancommunity->district = $district;
		$save_siswancommunity->location = $location;
		$save_siswancommunity->area = $area;
		$save_siswancommunity->status_of_land = $status_of_land;
		$save_siswancommunity->important_fauna = $important_fauna;
		$save_siswancommunity->important_flora = $important_flora;
		$save_siswancommunity->save();

		$id = $save_siswancommunity->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/panniwala/', $image);
				$image_data[] = '/panniwala/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new SiswanNotificationDetail();
			$save_subheading->siswan_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }


		return redirect('/siswanlist')->with('success','Siswan Community Reserve Detail Added Successfully');	
	}

	public function editsiswan($id){

		$edit_siswan = SiswanCommunity::where('id',$id)->first();
		return view('protectedArea.communityReserve.siswan.edit',compact('edit_siswan'));
	}

	public function updatesiswan(Request $request,$id){

				// return $request->all();
    			
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$wildlife_heading = $request->input('wildlife_heading');
		$location = $request->input('location');
		$wildlife_title = $request->input('wildlife_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/siswan_community');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/siswan_community/' . $filename;

            $update_siswancommunity = SiswanCommunity::find($id);
			$update_siswancommunity->banner_image = $banner_image;
			$update_siswancommunity->banner_heading = $banner_heading;
			$update_siswancommunity->description = $description;
			$update_siswancommunity->wildlife_heading = $wildlife_heading;
			$update_siswancommunity->wildlife_title = $wildlife_title;
			$update_siswancommunity->district = $district;
			$update_siswancommunity->location = $location;
			$update_siswancommunity->area = $area;
			$update_siswancommunity->status_of_land = $status_of_land;
			$update_siswancommunity->notification_detail = $notification_detail;
			$update_siswancommunity->important_fauna = $important_fauna;
			$update_siswancommunity->important_flora = $important_flora;
			$update_siswancommunity->update();

		}
		else{
			$update_siswancommunity = SiswanCommunity::find($id);
			$update_siswancommunity->banner_heading = $banner_heading;
			$update_siswancommunity->description = $description;
			$update_siswancommunity->wildlife_heading = $wildlife_heading;
			$update_siswancommunity->wildlife_title = $wildlife_title;
			$update_siswancommunity->district = $district;
			$update_siswancommunity->location = $location;
			$update_siswancommunity->area = $area;
			$update_siswancommunity->status_of_land = $status_of_land;
			$update_siswancommunity->notification_detail = $notification_detail;
			$update_siswancommunity->important_fauna = $important_fauna;
			$update_siswancommunity->important_flora = $important_flora;
			$update_siswancommunity->save();
		}
		
		return redirect('/siswanlist')->with('success','Siswan Community Reserve Detail Update Successfully');	
	}

	public function deletesiswan($id){

		$delete_siswan = SiswanCommunity::find($id);
		$delete_siswan->deleted_status = '1';
		$delete_siswan->save();
		return redirect('/siswanlist')->with('success','Siswan Community Reserve Detail Delete Successfully');	
	}

	// notification text
	public function siswan_notificationtextListing($id){

		$notification_list = SiswanNotificationDetail::where('siswan_id',$id)->get();

	return view('protectedArea.communityReserve.siswan.notification_list',compact('notification_list','id'));
	}

	public function siswan_deleteListing($id){

		$delete_listing = SiswanNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function siswan_editNotificationText($id){

		$edit_notification = SiswanNotificationDetail::where('id',$id)->first();

	return view('protectedArea.communityReserve.siswan.edit_notification',compact('edit_notification'));
	}

	public function siswan_updateNotificationText(Request $request , $id){

		$update_notification = SiswanNotificationDetail::find($id);
		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/siswan_notification_list/'.$update_notification->siswan_id)->with('success','Notification Text updated successfully');
	}

	public function siswan_addNewNotificationText($id){

		return view('protectedArea.communityReserve.siswan.add_new_notification',compact('id'));		 
	}

	public function siswan_saveNewNotificationText(Request $request , $id){

		$save_notification = new SiswanNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->siswan_id = $id;
		$save_notification->save();

		return redirect('/siswan_notification_list/'.$save_notification->siswan_id)->with('success','Notification Text Save successfully');
	}

	public function siswan_notification_pdflist($id){

		$notification_pdf_list = SiswanNotificationDetail::where('siswan_id',$id)->get();
		return view('protectedArea.communityReserve.siswan.notification_pdf_list',compact('notification_pdf_list','id'));
	}

	public function siswan_edit_notification_pdf($id){

		$edit = SiswanNotificationDetail::where('id',$id)->first();
		return view('protectedArea.communityReserve.siswan.edit_notification_pdf',compact('edit','id'));
	}

	public function siswan_update_notification_pdf(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$update_pdf = SiswanNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	        {
	          
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $pdf_url->getClientOriginalName();
				$pdf_url->move(public_path() . '/siswan/', $image);
				$pdf_url = '/siswan/'.$image;

				
			$update_pdf->pdf_url =$pdf_url;
			$update_pdf->save();

	           return redirect('siswan_notification_pdf/'.$update_pdf->siswan_id)->with('success','Notification pdf updated successfully');
			}
			
			else{
				return redirect('siswan_notification_pdf/'.$update_pdf->siswan_id)->with('success','Notification pdf updated successfully');
			}

			// return redirect('/siswan_notification_pdf/'.$update_pdf->siswan_id)->with('success','Notification Text updated successfully');
	}

	public function siswan_delete_notification_pdf($id){

		$delete_pdf = SiswanNotificationDetail::find($id);
		$delete_pdf->delete();
		return redirect()->back()->with('success','Delete Pdf Successfully');
	}
// end class 
}