<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Banner;



use App\Models\VolunteerDetail;
use App\Models\VolunteerDetailImage;

use App\Models\HomePageDetail;
use App\Models\WildlifeSymbol;

use App\Models\BirMotiBagh;

use App\Models\PanniwalaGumjal;
use App\Models\RakhSarai;

use App\Models\HarikaDetail;

use App\Models\NangalImages;
use App\Models\NangalDetail;

use App\Models\KesopurDetail;
use App\Models\KeshopurImages;

use App\Models\ChhatbirZooFrontend;

use App\Models\LudhianaZooFrontend;
use App\Models\PatialaZooFrontend;
use App\Models\BathindaZooFrontend;
use App\Models\DeerParkZooFrontend;

use App\Models\BirGurdialpur;
use App\Models\BirBhunerheri;

use App\Models\BirMihas;
use App\Models\BirDosanjh;
use App\Models\BirBhadson;

use App\Models\BirAishwan;
use App\Models\BirAbohar;
use App\Models\BirHarike;

use App\Models\LalwanCommunity;
use App\Models\KeshPurmi;
use App\Models\SiswanCommunity;


use App\Models\Roparwetland;
use App\Models\RanjitSagar;
use App\Models\BeasRiver;
use App\Models\KaliBein;

use App\Models\BirTakhniRehmpur;
use App\Models\BirJhajjarbachauli;
use App\Models\BirKathlaurKushlian;
use App\Models\BirNangalWildlife;
use App\Models\WorldDayDetail;
class ApiController extends Controller
{

    public function get_banner(){
    	
        $banner_detail = Banner::where('banner_slide','=','slide_1')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $banner_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Banner Detail';
        $result['base_url'] = url('/');
        return response()->json($result);
    }
    public function get_banner_two(){

        $banner_detail_two = Banner::where('banner_slide','=','slide_2')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $banner_detail_two;
        $result['error_code'] = '0';
        $result['message'] = 'Banner Slide 2 Detail';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }
    
    public function get_world_dayDetail(){

        $get_world = WorldDayDetail::where('deleted_status','0')->take(4)->get();

        $result  = array();
        $result['list'] = $get_world;
        $result['error_code'] = '0';
        $result['message'] = 'World Day Detail';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

   

    public function get_picture_detail(){

        $picture_detail = PictureDetail::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $picture_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Picture Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_chhatbirzoo_detail(){

        $chhatbirzoo_detail = ChhatbirZoo::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $chhatbirzoo_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Chhatbir Zoo Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_ludhianazoo_detail(){

        $ludhianazoo_detail = LudhianaZoo::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $ludhianazoo_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Ludhiana Zoo Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_minizoopatiala_detail(){

        $minizoopatialalist = MiniZooPatiala::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $minizoopatialalist;
        $result['error_code'] = '0';
        $result['message'] = 'Mini Zoo Patiala Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_minizoobathinda_detail(){

        $minizoobathinda = MiniZooBathinda::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $minizoobathinda;
        $result['error_code'] = '0';
        $result['message'] = 'Mini Zoo Patiala Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_deerparkneelon_detail(){

        $deerparkneelon_detail = DeerParkNeelon::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $deerparkneelon_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Deer Park Neelon Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);

    }

    public function get_chhatbir_detail(){

        $chhatbir_detail = Chhatbir::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $chhatbir_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Chhatbir Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }
    public function get_eventforest_detail(){

        $event_forestdaydetail = EventForestDay::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $event_forestdaydetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event Forest Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_eventworldwildlife_detail(){

        $event_worldwildlifedetail = EventWorldWildLife::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $event_worldwildlifedetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event World WildLife Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_eventworldwetland_detail(){

        $event_worldwetlanddetail = EventWorldWetland::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $event_worldwetlanddetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event World Wetland Detail List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }

    public function get_eventmigratory_bird(){

        $event_migratorybirddetail = EventMigratoryBirdDay::latest()->first();
        $result  = array();
        $result['list'] = $event_migratorybirddetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event World Migratory Bird Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);

    }

    public function get_event_bgimage(){

        $event_migratorybirddetail = EventBgImages::latest()->first();
        $result  = array();
        $result['list'] = $event_migratorybirddetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event Background Image Detail List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }

    public function get_event_bgimageheading(){

        $event_bgimageheading = EventBgimageHeading::latest()->first();
        $result  = array();
        $result['list'] = $event_bgimageheading;
        $result['error_code'] = '0';
        $result['message'] = 'Event Background Image Heading Detail List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }

    public function get_volunteer_detail(){

        $volunteer_detailheading = VolunteerDetail::latest()->first();
        $result  = array();
        $result['list'] = $volunteer_detailheading;
        $result['error_code'] = '0';
        $result['message'] = 'Volunteer Detail List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }
    public function get_volunteer_images(){

        $volunteer_images_detail = VolunteerDetailImage::latest()->first();
        $result  = array();
        $result['list'] = $volunteer_images_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Volunteer Images Detail List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }
    public function get_homepage_detail(){

        $home_page_detail = HomePageDetail::with('homeGeographicZone')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $home_page_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Home Page Detail List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }
    public function get_wildlife_symbols(){

        $wildlife_symbol_detail = WildlifeSymbol::with('get_wildlife_symbol_images')->where('deleted_status','=','0')->latest()->take(4)->first();
        $result  = array();
        $result['list'] = $wildlife_symbol_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Wild Life Symbol Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);

    }

    public function get_birmoti_bagh(){
           
        $get_birmoti_bagh =  BirMotiBagh::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_birmoti_bagh;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Moti Bagh Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_birgurdialpur(){

        $get_gurdialpur_bagh =  BirGurdialpur::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_gurdialpur_bagh;
        $result['error_code'] = '0';
        $result['message'] = 'Gurdialpur Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_bhunerheri(){

        $get_bhunerheri_bagh =  BirBhunerheri::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_bhunerheri_bagh;
        $result['error_code'] = '0';
        $result['message'] = 'Bhunerheri Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_panniwala_gumjal(){

        $get_panniwala =  PanniwalaGumjal::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_panniwala;
        $result['error_code'] = '0';
        $result['message'] = 'Panniwala-Gumjal-Haripura-Diwankhera Community Reserve Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_lalwan_detail(){

        $get_lalwan_detail = LalwanCommunity::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_lalwan_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Lalwan Community Reserve Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_keshupur_detail(){

        $get_keshupur_detail = KeshPurmi::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_keshupur_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Keshu Pur Community Reserve Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_siswan_detail(){

        $get_siswan_detail = SiswanCommunity::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_siswan_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Siswan Community Reserve Detail List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_rakh_sarai(){

        $get_rakhsarai =  RakhSarai::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_rakhsarai;
        $result['error_code'] = '0';
        $result['message'] = 'Rakh Sarai Amanat Khan Conservation Reserve List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_ropar_wetland(){

        $get_roparwetland =  Roparwetland::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_roparwetland;
        $result['error_code'] = '0';
        $result['message'] = 'Ropar Wetland Conservation Reserve List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_ranjit_sagar_dam(){

        $get_ranjitsagar =  RanjitSagar::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_ranjitsagar;
        $result['error_code'] = '0';
        $result['message'] = 'Ranjit Sagar Dam Conservation Reserve List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_beas_river(){

        $get_beas_river =  BeasRiver::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_beas_river;
        $result['error_code'] = '0';
        $result['message'] = 'Beas River Conservation Reserve List';
        $result['base_url'] = url('/');
        return response()->json($result);        
    }

    public function get_kali_bean_detail(){

        $get_ali_bein =  KaliBein::with('get_notification_detail')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_ali_bein;
        $result['error_code'] = '0';
        $result['message'] = 'Kali Bein List';
        $result['base_url'] = url('/');
        return response()->json($result);    
    }

    public function get_harike_santuary(){

        $get_harike_santuary = HarikaDetail::with('get_harika_images')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_harike_santuary;
        $result['error_code'] = '0';
        $result['message'] = 'Harike Wildlife Santuary List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_nangal_santuary(){

        $get_nangal_detail = NangalDetail::with('get_nangal_images')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_nangal_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Nangal Wildlife Santuary List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

     public function get_keshopur_detail(){

        $get_keshopur_detail = KesopurDetail::with('get_keshopur_images')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_keshopur_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Keshopur Wildlife Santuary List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }


    public function get_chhatbir_zoo(){

        $get_chhatbir_zoo = ChhatbirZooFrontend::where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_chhatbir_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Chhatbir Zoo List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_ludhiana_zoo(){
        
        $get_ludhiana_zoo = LudhianaZooFrontend::where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_ludhiana_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Ludhiana Zoo List';
        $result['base_url'] = url('/');
        return response()->json($result);

    }

    public function get_patiala_zoo(){

        $get_patiala_zoo = PatialaZooFrontend::where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_patiala_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Patiala Zoo List';
        $result['base_url'] = url('/');
        return response()->json($result);

    }
     
    public function get_bathinda_zoo(){

        $get_bathinda_zoo = BathindaZooFrontend::where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_bathinda_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bathinda Zoo List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_deerpark_neelon_zoo(){

        $get_deerpark_zoo = DeerParkZooFrontend::where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_deerpark_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'DeerPark Neelon Zoo List';
        $result['base_url'] = url('/');
        return response()->json($result);
    } 
    public function get_mehas_detail(){

        $get_birmihas_zoo = BirMihas::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_birmihas_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Mihas List';
        $result['base_url'] = url('/');
        return response()->json($result);

    }

    public function get_dosanjh_detail(){

        $get_birdosanjh_zoo = BirDosanjh::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_birdosanjh_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Dosanjh List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_bhadson_detail(){

        $get_birbhadson_zoo = BirBhadson::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_birbhadson_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Bhadson List';
        $result['base_url'] = url('/');
        return response()->json($result);
    }

    public function get_aishwan_detail(){

        $get_biraishwan_zoo = BirAishwan::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_biraishwan_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Aishwan List';
        $result['base_url'] = url('/');
        return response()->json($result);        
    }

    public function get_abohar_detail(){

        $get_abohar_zoo = BirAbohar::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_abohar_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Abohar List';
        $result['base_url'] = url('/');
        return response()->json($result);         
    }

    public function get_takhni_rehmapur_detail(){

        $get_takhni_rehmpur = BirTakhniRehmpur::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_takhni_rehmpur;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Takhni Rehmpur List';
        $result['base_url'] = url('/');
        return response()->json($result);         
    }

    public function get_jhajjar_bachauli_detail(){

        $get_bir_jhajjarbachauli = BirJhajjarbachauli::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_bir_jhajjarbachauli;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Jhajjarbachauli List';
        $result['base_url'] = url('/');
        return response()->json($result);   
    }


    public function get_kathlaur_kushlian_detail(){

        $get_bir_kathlaur_kushlian = BirKathlaurKushlian::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_bir_kathlaur_kushlian;
        $result['error_code'] = '0';
        $result['message'] = 'Bir kathlaur kushlian List';
        $result['base_url'] = url('/');
        return response()->json($result);   
    }

    public function get_nangal_wildlife_detail(){

        $get_bir_nangal_wildlife = BirNangalWildlife::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_bir_nangal_wildlife;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Nangal Wildlife List';
        $result['base_url'] = url('/');
        return response()->json($result);   
    } 

    public function get_harike_detail(){

        $get_harike_zoo = BirHarike::with('get_notification_detail')->where('deleted_status','0')->latest()->first();
        $result  = array();
        $result['list'] = $get_harike_zoo;
        $result['error_code'] = '0';
        $result['message'] = 'Bir Harike List';
        $result['base_url'] = url('/');
        return response()->json($result); 
    }

    public function calendarDetail(){

         $event = WorldDayDetail::where('deleted_status','=','0')->get();
             $data=[];
             foreach($event as $events){
                $subArr=[
                    'id'=> $events->id,
                    'type_name'=> $events->type_name,
                    'date_of_event' => $events->date_of_event
                ];
                array_push($data,$subArr);
             }
             
             return response()->json($data);
    }

// end class 
}