<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Banner;


use App\Models\UpcomingEventDetail;

use App\Models\EventBgImages;

use App\Models\VolunteerDetail;

use App\Models\HomePageDetail;
use App\Models\HomeGeographicZone;
use App\Models\WildlifeSymbol;
use App\Models\WildlifeSymbolTitleImage;

use App\Models\MinisterDetail;
use App\Models\ZooType;
use App\Models\ZooDetail;
use App\Models\ZooGalleryBanner;
use App\Models\ZooGallery;

use App\Models\WorldDay;
use App\Models\WorldDayDetail;
use App\Models\WorldDayDetails;
use App\Models\WorldDayImagesDetail;
use App\Models\WorldDayImagesHonour;
use App\Models\ServicesHowApply;
use DB;
use Carbon\Carbon;
use Image;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
		
	  return view('/home');
	}
	
	public function bannerList(){
		
		$banner_list = Banner::where('deleted_status','=','0')->paginate(10);
		return view('index.banners_list',compact('banner_list'));
	}
	
	public function addBanner(){
		return view('index.add_banner');
	}

	public function saveBanner(Request $request){

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
        'banner_description' => 'required|regex:/^[\pL\s\-]+$/u',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$banner_description = $request->input('banner_description');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/banner_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/banner_image/' . $filename;

            $save_banner = new Banner();
			$save_banner->banner_image = $banner_image;
			$save_banner->banner_heading = $banner_heading;
			$save_banner->banner_description = $banner_description;
			$save_banner->save();
		}	

		return redirect('/banners_list')->with('sucess','File Uploaded Successfully');
	}

	public function deleteBanner($id){

		$deletebanner = Banner::find($id);
		$deletebanner->deleted_status = '1' ;
		$deletebanner->save();
		return redirect('/banners_list')->with('sucess','Delete Record Successfully');
	}
	
	public function editBanner($id){

		$edit_banner = Banner::where('id',$id)->first();
		return view('index.edit_banner',compact('edit_banner'));
	}  

	public function updateBanner(Request $request , $id){
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'banner_description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$banner_description = $request->input('banner_description');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/banner_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/banner_image/' . $filename;

            $update_banner = Banner::find($id);
			$update_banner->banner_image = $banner_image;
			
			$update_banner->save();
			// return $update_banner;
		}
		else{
			$update_banner = Banner::find($id);
			$update_banner->banner_heading = $banner_heading;
			$update_banner->banner_description = $banner_description;
			$update_banner->save();
		}	

		return redirect('/banners_list')->with('sucess','Banner Updated Successfully');
	}

	public function addBanner_two(){

		return view('index.banner_two');
	}
	public function saveBanner_two(Request $request){

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$banner_image = $request->file('banner_image');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/banner_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/banner_image/' . $filename;

            $update_banner = new Banner();
			$update_banner->banner_image = $banner_image;
			$update_banner->banner_slide = 'slide_2';
			$update_banner->save();
		}
		return redirect('/banners_list')->with('sucess','Banner Slide Two Added Successfully');	
	}
	//second portion starts

	public function worldDayList(){
		$current_date =	date('Y-m-d');
		$dayscount = date('Y-m-d', strtotime("-10 days", strtotime($current_date)));

		// $world_list = WorldDayDetail::whereDate('date_of_event','<=',$current_date)->where('deleted_status','0')->take(4)->latest()->get();

		$world_list = WorldDayDetail::where('deleted_status','0')->paginate(10);

		// foreach($world_list as $e){

		// 	$e->total_images = WorldDayImagesDetail::where('world_day_detail_id',$e->world_day_type_id)->count();
		// }
		// return $world_list;
		return view('index.worldDay.listing',compact('world_list'));
	}

	public function addworldDay(){

		$worldDay_list = WorldDay::all();
		return view('index.worldDay.add',compact('worldDay_list'));
	}

	public function saveworldDay(Request $request){

		$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'world_day_type_id' => 'required',
        'description' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'event_date' => 'required',
    	]);

		$image = $request->file('image');
		$world_day_type_id = $request->input('world_day_type_id');
		$description = $request->input('description');
		$event_date = $request->input('event_date');

		$date = Carbon::createFromFormat('Y-m-d', $event_date)->day;

		$monthname = date('F', strtotime($event_date)); 

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/world_day');
            $image->move($destinationPath, $filename);
            $image = '/world_day/' . $filename;
		}
		$name =	WorldDay::where('id',$world_day_type_id)->first();
		$world_name = $name->world_day_type;	

		$save_worldDay = new WorldDayDetail();
		$save_worldDay->image = $image;
		$save_worldDay->world_day_type_id = $world_day_type_id;
		$save_worldDay->type_name = $world_name;
		$save_worldDay->date_of_event = $event_date;
		$save_worldDay->event_date = $date;
		$save_worldDay->month_name = $monthname;
		$save_worldDay->description = $description;
		$save_worldDay->save();

		return redirect('/world_day_list')->with('success','World Day detail added Successfully');
	}

	public function editworldDay($id){

		$worldDay_list = WorldDay::all();
		$edit_world = WorldDayDetail::where('id',$id)->first();
		return view('index.worldDay.edit',compact('edit_world','worldDay_list'));
	}

	public function updateworldDay(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
        'description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
    	]);

		$image = $request->file('image');
		$world_day_type_id = $request->input('world_day_type_id');
		$description = $request->input('description');
		$event_date = $request->input('event_date');

		$date = Carbon::createFromFormat('Y-m-d', $event_date)->day;

		$monthname = date('F', strtotime($event_date)); 
		$name =	WorldDay::where('id',$world_day_type_id)->first();
		$world_name = $name->world_day_type;
		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/world_day');
            $image->move($destinationPath, $filename);
            $image = '/world_day/' . $filename;

            $update_worldDay = WorldDayDetail::find($id);
			$update_worldDay->image = $image;
			$update_worldDay->save();
		}	
		else{

			$update_worldDay = WorldDayDetail::find($id);
			$update_worldDay->world_day_type_id = $world_day_type_id;
			$update_worldDay->type_name = $world_name;
			$update_worldDay->date_of_event = $event_date;
			$update_worldDay->event_date = $date;
			$update_worldDay->month_name = $monthname;
			$update_worldDay->description = $description;
			$update_worldDay->save();
		}
	

		return redirect('/world_day_list')->with('success','World Day detail Update Successfully');
	}

	public function deleteworldDay($id){

		$save_worldDay = WorldDayDetail::find($id);
		$save_worldDay->deleted_status = '1';
		$save_worldDay->save();

		return redirect('/world_day_list')->with('success','World Day detail Deleted Successfully');
	}


	// forest World Day detail 
	public function addworldDayDetail(){

		$worldDay_list = WorldDay::all();
		return view('index.worldDay.add_world_day_detail',compact('worldDay_list'));
	}

	public function saveworldDayDetail(Request $request){

		// return $request->all();
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);
    	
		$world_day_type_id = $request->world_day_type_id;
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$banner_date = $request->input('banner_date');
		$description = $request->input('description');
		$image = $request->file('image');
		$guest_of_honour = $request->file('guest_of_honour');
		$guest_honour_name = $request->input('guest_honour_name');
		$guest_honour_title = $request->input('guest_honour_title');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/forestDay/' . $filename;
		}

		$save_detail = new WorldDayDetails();
		$save_detail->world_day_type_id = $world_day_type_id;
		$save_detail->banner_image = $banner_image;
		$save_detail->banner_heading = $banner_heading;
		$save_detail->banner_date = $banner_date;
		$save_detail->description = $description;
		$save_detail->save();
		$id = $save_detail->id;

		 $image_data=array();
		 $img_data=array();
		if($request->hasFile('image') != ""){
        	foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				        $file->move(public_path() . '/forestDay/', $image);
				        $image_data[] = '/forestDay/'.$image;
            }
		}

		if($request->hasFile('guest_of_honour') != "")
    	{
            foreach($request->file('guest_of_honour') as $key=>$file)
            {
                // $guest_of_honour = $file->getClientOriginalName();
                $guest_of_honour = time() . '.' . $file->getClientOriginalName();
				        $file->move(public_path() . '/forestDay/', $guest_of_honour);
				        $img_data[] = '/forestDay/'.$guest_of_honour;
            }
		}

		// foreach($image as $ci){
              
  //                   $add_c = new WorldDayImagesDetail();
  //                   $add_c->world_day_detail_id = $id;
  //                   $add_c->image = $ci;
  //                   $add_c->save();
                 
  //           }
		for($i=0;$i<count($guest_honour_title);$i++)
        {
        	$save_harika_data = new WorldDayImagesDetail();
        	$save_harika_data->world_day_detail_id = $id;
        	$save_harika_data->world_day_id = $world_day_type_id;
        	$save_harika_data->images=$image_data[$i];
        	
        	$save_harika_data->save();
        }

        for($i=0;$i<count($guest_honour_name);$i++)
        {
        	$save_harika_data = new  WorldDayImagesHonour();
        	$save_harika_data->world_day_detail_id = $id;
        	$save_harika_data->world_day_id =$world_day_type_id;
        	$save_harika_data->guest_of_honour=$img_data[$i];
        	$save_harika_data->guest_honour_name=$guest_honour_name[$i];
        	$save_harika_data->guest_honour_title=$guest_honour_title[$i];
        	
        	$save_harika_data->save();
        }


        return redirect('/world_day_list')->with('success','Detail Added Successfully');
	}

	// minister data
	public function ministerDetail(){
		$minister_detail = MinisterDetail::where('deleted_status','0')->paginate(10);
		return view('index.minister.list',compact('minister_detail'));
	}

	public function addministerDetail(){

		return view('index.minister.add_ministerdetail');
	}

	public function saveministerDetail(Request $request){
    	
    	$validated = $request->validate([
        'minister_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'minister_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'minister_title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'description' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/' ,
    	]);
    	
		// return $request->all();
		$minister_image = $request->file('minister_image');
		$minister_heading = $request->input('minister_heading');
		$minister_title = $request->input('minister_title');
		$description = $request->input('description');

		if($request->hasFile('minister_image') != ""){
        	$filename =$minister_image->getClientOriginalName();
            $destinationPath = public_path('/minister_image');
            $minister_image->move($destinationPath, $filename);
            $minister_image = '/minister_image/' . $filename;
		}

		$save_minister = new MinisterDetail();
		$save_minister->minister_image = $minister_image; 
		$save_minister->minister_heading = $minister_heading; 
		$save_minister->minister_title = $minister_title; 
		$save_minister->description = $description; 
		$save_minister->save(); 

		return redirect('list_minister')->with('success','Minister Detail Added Successfully');
	}

	public function editministerDetail($id){

		$edit_minister = MinisterDetail::where('id',$id)->first();
		return view('index.minister.edit',compact('edit_minister'));
	}

	public function updateministerDetail(Request $request , $id){
    
    	$validated = $request->validate([
        'minister_image' => 'mimes:jpg,png,jpeg|max:2048',
        'minister_heading' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        'minister_title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/' ,
    	]);

		// return $request->all();
		$minister_image = $request->file('minister_image');
		$minister_heading = $request->input('minister_heading');
		$minister_title = $request->input('minister_title');
		$description = $request->input('description');

		if($request->hasFile('minister_image') != ""){
        	$filename =$minister_image->getClientOriginalName();
            $destinationPath = public_path('/minister_image');
            $minister_image->move($destinationPath, $filename);
            $minister_image = '/minister_image/' . $filename;
            $update_minister = MinisterDetail::find($id);
			$update_minister->minister_image = $minister_image; 
			$update_minister->save();
		}
		else{

			$update_minister = MinisterDetail::find($id);
			$update_minister->minister_heading = $minister_heading; 
			$update_minister->minister_title = $minister_title; 
			$update_minister->description = $description; 
			$update_minister->save(); 
		}

		

		return redirect('list_minister')->with('success','Minister Detail Updated Successfully');
	}

	public function deleteministerDetail($id){

		$delete_minister = MinisterDetail::find($id);
		$delete_minister->deleted_status = '1';
		$delete_minister->save(); 

		return redirect('list_minister')->with('success','Minister Detail Deleted Successfully');
	}

	public function zooListing(){

		$zoodetail = ZooDetail::where('deleted_status','0')->paginate(10);
		return view('index.zoo.zooDetail.list',compact('zoodetail'));
	}

	public function addzooDetail(){
        
        $zoo_type_list = ZooType::all();
		return view('index.zoo.zooDetail.add_zooDetail',compact('zoo_type_list'));
	}

	public function savezooDetail(Request $request){
    
    	$validated = $request->validate([
        'zoo_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$zoo_type_id = $request->zoo_type_id;
		$zoo_image = $request->file('zoo_image');

		if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_image');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_image/' . $filename;
		}
		$zoo_type_name = ZooType::where('id',$zoo_type_id)->first();
		$name_zoo = $zoo_type_name->zoo_type_name;
		$save_zooDetail = new ZooDetail();
		$save_zooDetail->zoo_type_id = $zoo_type_id;
		$save_zooDetail->type_name = $name_zoo;
		$save_zooDetail->zoo_image = $zoo_image;
        $save_zooDetail->save();

        return redirect('/zoo_list')->with('success','Zoo Detail Added Successfully');
	}

	public function editzooDetail($id){
		 $zoo_type_list = ZooType::all();
		$edit_zoo = ZooDetail::where('id',$id)->first();
		return view('index.zoo.zooDetail.edit',compact('edit_zoo','zoo_type_list'));
	}

	public function updatezooDetail(Request $request,$id){
		
    	$validated = $request->validate([
        'zoo_image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);
    	
		$zoo_type_id = $request->zoo_type_id;
		$zoo_image = $request->file('zoo_image');

		if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_image');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_image/' . $filename;
		}
		$zoo_type_name = ZooType::where('id',$zoo_type_id)->first();
		$name_zoo = $zoo_type_name->zoo_type_name;
		$update_zooDetail = ZooDetail::find($id);
		$update_zooDetail->zoo_type_id = $zoo_type_id;
		$update_zooDetail->type_name = $name_zoo;
		$update_zooDetail->zoo_image = $zoo_image;
        $update_zooDetail->save();

        return redirect('/zoo_list')->with('success','Zoo Detail Updated Successfully');
	}

	public function deletezooDetail($id){

		$delete_zooDetail = ZooDetail::find($id);
		$delete_zooDetail->deleted_status = '1';
        $delete_zooDetail->save();

        return redirect('/zoo_list')->with('success','Zoo Detail Deleted Successfully');
	}

	public function zoogallerylist(){

		$data['zoogallerybanner_detail'] = ZooGalleryBanner::where('deleted_status','0')->get();

		// $data['zoogallery_detail'] = ZooGallery::where('deleted_status','0')->get();
		$data['zoo_type'] = ZooType::all();
		foreach($data['zoo_type'] as $z){

			$z->total_count = ZooGallery::where('zoo_type_id', $z->id)->count();
		}

        // return $data['zoo_type'];


		// return view('index.zoo.zooGallery.zoogallery',compact('zoogallerybanner_detail','zoogallery_detail','zoo_type'));
		return view('index.zoo.zooGallery.zoogallery',$data);
	}

	public function addgallerybanner(){

		
		return view('index.zoo.zooGallery.addgallerybanner');

	}

	public function savegallerybanner(Request $request){
    
    	$validated = $request->validate([
        'zoo_gallery_banner' => 'required|mimes:jpg,png,jpeg|max:2048',
        'zoo_gallery_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

		$zoo_gallery_banner = $request->file('zoo_gallery_banner');
		$zoo_gallery_heading = $request->input('zoo_gallery_heading');

		if($request->hasFile('zoo_gallery_banner') != ""){
        	$filename =$zoo_gallery_banner->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_gallery_banner->move($destinationPath, $filename);
            $zoo_gallery_banner = '/zoo_gallery/' . $filename;

		}

		$save_banner_detail = new ZooGalleryBanner();
		$save_banner_detail->zoo_gallery_banner = $zoo_gallery_banner;
		$save_banner_detail->zoo_gallery_heading = $zoo_gallery_heading;
		$save_banner_detail->save();

		return redirect('/gallery_list')->with('success','Gallery Banner Added Successfully');	

	}

	public function editgallerybanner($id){

		$edit_gallery_banner = ZooGalleryBanner::where('id',$id)->first();
		return view('index.zoo.zooGallery.editgallerybanner',compact('edit_gallery_banner'));

	}

	public function updategallerybanner(Request $request,$id){
    
    	$validated = $request->validate([
        'zoo_gallery_banner' => 'mimes:jpg,png,jpeg|max:2048',
        'zoo_gallery_heading' => 'regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

        $zoo_gallery_banner = $request->file('zoo_gallery_banner');
		$zoo_gallery_heading = $request->input('zoo_gallery_heading');

		if($request->hasFile('zoo_gallery_banner') != ""){
        	$filename =$zoo_gallery_banner->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_gallery_banner->move($destinationPath, $filename);
            $zoo_gallery_banner = '/zoo_gallery/' . $filename;
            $update_banner_detail = ZooGalleryBanner::find($id);
			$update_banner_detail->zoo_gallery_banner = $zoo_gallery_banner;
			$update_banner_detail->save();
		}
		else{

			$update_banner_detail = ZooGalleryBanner::find($id);
			$update_banner_detail->zoo_gallery_heading = $zoo_gallery_heading;
			$update_banner_detail->save();
		}
	

		return redirect('/gallery_list')->with('success','Gallery Banner Updated Successfully');	
	}

	public function deletegallerybanner($id){
		 
		$delete_banner_detail = ZooGalleryBanner::find($id);
		$delete_banner_detail->deleted_status = '1';
		$delete_banner_detail->save();

		return redirect('/gallery_list')->with('success','Gallery Banner Deleted Successfully');
	}

	public function addZooGallery(){

		$zoo_list = ZooType::all();
		return view('index.zoo.zooGallery.addZooGallery',compact('zoo_list'));
	}

	public function saveZooGallery(Request $request){
		
    	$validated = $request->validate([
        'zoo_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);
		
		// return $request->all();
		$zoo_type_id = $request->zoo_type_id;
		$zoo_image = $request->file('zoo_image');

			 // $folder_path = '/images';
            $destinationPath = public_path('/zoo_gallery');

            $image = $request->file('zoo_image');

            $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

            $img = Image::make($image->getRealPath());
            $img->save($destinationPath.'/'.$img_name);

            //resize image
            $img->resize(288,192)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

            $input['img'] = $img_name;
			
			
		
      
		// if($request->hasFile('zoo_image') != ""){
  //       	$filename =$zoo_image->getClientOriginalName();
  //           $destinationPath = public_path('/zoo_gallery');
  //           $zoo_image->move($destinationPath, $filename);
  //           $zoo_image = '/zoo_gallery/' . $filename;

		// }

		$type_name = ZooType::where('id',$zoo_type_id)->first();
	    $name =	$type_name->zoo_type_name;

		$update_banner_detail = new ZooGallery();
		$update_banner_detail->zoo_image = '/zoo_gallery/'.$input['img'];
		$update_banner_detail->zoo_type_id = $zoo_type_id;
		$update_banner_detail->zoo_type_name = $name;
		$update_banner_detail->save();

		return redirect('/gallery_list')->with('success','Gallery Banner Updated Successfully');	
	}

	// upcoming Events
	// forest day event
	public function eventListing(){

		$eventforestdaylist = UpcomingEventDetail::where('deleted_status','=','0')->get();
		return view('index.events.forestDay.listing',compact('eventforestdaylist'));
	}

	public function eventadd(){

		$worldDay_list = WorldDay::all();		
		return view('index.events.forestDay.add',compact('worldDay_list'));
	}
	public function saveevent(Request $request){
    
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$world_id = $request->world_id;


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;
		}
		$w_name = WorldDay::where('id',$world_id)->first();
		$name = $w_name->world_day_type;
		
        $save_zoodetail = new UpcomingEventDetail();
		$save_zoodetail->image = $image;
		$save_zoodetail->heading = $heading;
		$save_zoodetail->world_id = $world_id;
		$save_zoodetail->world_day_name = $name;
		$save_zoodetail->date = $date;
		$save_zoodetail->save();	

		return redirect('/event_list')->with('success','Event Detail Added Successfully');
	}

	public function editevent($id){
		$worldDay_list = WorldDay::all();
		$editforestday = UpcomingEventDetail::where('id',$id)->first();
		return view('index.events.forestDay.edit',compact('editforestday','worldDay_list'));
	}

	public function updateevent(Request $request, $id){
		
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);
    
		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$world_id = $request->world_id;


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

		}
		$w_name = WorldDay::where('id',$world_id)->first();
		$name = $w_name->world_day_type;
		$save_zoodetail = UpcomingEventDetail::find($id);
		$save_zoodetail->image = $image;
		$save_zoodetail->heading = $heading;
		$save_zoodetail->date = $date;
		$save_zoodetail->world_id = $world_id;
		$save_zoodetail->world_day_name = $name;
		$save_zoodetail->save();
		return redirect('/event_list')->with('success','Event Detail Update Successfully');
	}

	public function deleteevent($id){

		$save_zoodetail = UpcomingEventDetail::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();

		return redirect('/event_list')->with('success','Event Detail Deleted Successfully');
	}

	// WorldWildlife

	public function eventwildlifeListing(){

		$eventwildlife = EventWorldWildLife::where('deleted_status','=','0')->get();
		return view('index.events.worldWildlife.listing',compact('eventwildlife'));
	}

	public function eventwildlifeadd(){

		return view('index.events.worldWildlife.add');
	}

	public function eventwildlifesave(Request $request){
		
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);
    
		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = new EventWorldWildLife();
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wildlife')->with('success','Event World Wildlife Added Successfully');
	}

	public function eventwildlifeedit($id){

		$editworldwild = EventWorldWildLife::where('id',$id)->first();
		return view('index.events.worldWildlife.edit',compact('editworldwild'));
	}

	public function eventwildlifeupdate(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = EventWorldWildLife::find($id);
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wildlife')->with('success','Event World Wildlife Updated Successfully');
	}

	public function eventwildlifdelete($id){

		$save_zoodetail = EventWorldWildLife::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_wildlife')->with('success','Event World Wildlife Deleted Successfully');
	}

	// World Migratory Bird Day

	public function eventwetlandlisting(){

		$eventwetlandlisting = EventWorldWetland::where('deleted_status','=','0')->get();
		return view('index.events.wetland.listing',compact('eventwetlandlisting'));
	}

	public function eventwetlandadd(){

		return view('index.events.wetland.add');
	}
	public function eventwetlandsave(Request $request){
    
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = new EventWorldWetland();
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wetland')->with('success','Event World Wildlife Added Successfully');
	}

	public function eventwetlandedit($id){

		$editwetland = EventWorldWetland::where('id',$id)->first();
		return view('index.events.wetland.edit',compact('editwetland'));
	}

	public function eventwetlandupdate(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = EventWorldWetland::find($id);
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wetland')->with('success','Event World Wildlife Updated Successfully');
	}

	public function eventwetlanddelete($id){

		$save_zoodetail = EventWorldWetland::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_wetland')->with('success','Event World Wildlife Deleted Successfully');
	}

	// migratory bird day 

	public function eventmigratorybirdListing(){

		$migratorybirdListing = EventMigratoryBirdDay::where('deleted_status','=','0')->get();
		return view('index.events.migratorybird.listing',compact('migratorybirdListing'));
	}

	public function eventmigratorybirdadd(){

		return view('index.events.migratorybird.add');
	}
	public function eventmigratorybirdsave(Request $request){
    
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = new EventMigratoryBirdDay();
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_migratorybirdday')->with('success','Event World Wildlife Added Successfully');
	}

	public function eventmigratorybirdedit($id){

		$editmigratorybird = EventMigratoryBirdDay::where('id',$id)->first();
		return view('index.events.migratorybird.edit',compact('editmigratorybird'));
	}

	public function eventmigratorybirdupdate(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = EventMigratoryBirdDay::find($id);
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_migratorybirdday')->with('success','Event World Wildlife Updated Successfully');
	}

	public function eventmigratorybirddelete($id){

		$save_zoodetail = EventMigratoryBirdDay::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_migratorybirdday')->with('success','Event World Wildlife Delete Successfully');
	}

	public function eventbgimageListing(){

		$eventbg_imageList = EventBgImages::where('deleted_status','=','0')->get();
		return view('index.events.bg_images.listing',compact('eventbg_imageList'));
	}

	public function eventbgimageadd(){

		return view('index.events.bg_images.add');
	}
	public function eventbgimagesave(Request $request){
    
    	$validated = $request->validate([
        'bg_imag' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$bg_image = $request->file('bg_image');
		$image_heading = $request->input('image_heading');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/event_bg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/event_bg_image/' . $filename;

		}	

        $save_zoodetail = new EventBgImages();
		$save_zoodetail->bg_image = $bg_image;
		$save_zoodetail->image_heading = $image_heading;
		$save_zoodetail->save();

		return redirect('/bg_imageevent')->with('success','Event Detail Added Successfully');
	}

	public function eventbgimageedit($id){

		$editbg_image = EventBgImages::where('id',$id)->first();
		return view('index.events.bg_images.edit',compact('editbg_image'));
	}

	public function eventbgimageupdate( Request $request , $id){
    
    	$validated = $request->validate([
        'bg_imag' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		// return $request->all();
		$bg_image = $request->file('bg_image');
		$image_heading = $request->input('image_heading');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/event_bg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/event_bg_image/' . $filename;

		}	

        $save_zoodetail = EventBgImages::find($id);
		$save_zoodetail->bg_image = $bg_image;
		$save_zoodetail->image_heading = $image_heading;
		$save_zoodetail->save();

		return redirect('/bg_imageevent')->with('success','Event Detail Updated Successfully');
	}

	public function eventbgimagedelete($id){

		$save_zoodetail = EventBgImages::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/bg_imageevent')->with('success','Event Background Image Deleted Successfully');
	}


	// volunteerdetailListing
	public function volunteerdetailListing(){

		$volunteer_detail = VolunteerDetail::where('deleted_status','=','0')->paginate(10);
		return view('index.volunteerDetail.listing',compact('volunteer_detail'));
	}

	public function volunteerdetailadd(){

		return view('index.volunteerDetail.add');
	}
	public function volunteerdetailsave(Request $request){
    
    	$validated = $request->validate([
        'volunter_image_one' => 'required|mimes:jpg,png,jpeg|max:2048',
        'volunter_image_two' => 'required|mimes:jpg,png,jpeg|max:2048',
        'volunteer_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'volunteer_title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'volunteer_description' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

		$volunteer_heading = $request->input('volunteer_heading');
		$volunteer_title = $request->input('volunteer_title');
		$volunteer_description = $request->input('volunteer_description');
		$volunter_image_one = $request->file('volunter_image_one');
		$volunter_image_two = $request->file('volunter_image_two');

		if($request->hasFile('volunter_image_one') != ""){
        	$filename =$volunter_image_one->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_one->move($destinationPath, $filename);
            $volunter_image_one = '/volunter_image/' . $filename;

		}
		if($request->hasFile('volunter_image_two') != ""){
        	$filename =$volunter_image_two->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_two->move($destinationPath, $filename);
            $volunter_image_two = '/volunter_image/' . $filename;

		}		

		$save_volunteerdetail = new VolunteerDetail();
		$save_volunteerdetail->volunteer_heading = $volunteer_heading;
		$save_volunteerdetail->volunteer_title = $volunteer_title;
		$save_volunteerdetail->volunteer_description = $volunteer_description;
		$save_volunteerdetail->volunter_image_one = $volunter_image_one;
		$save_volunteerdetail->volunter_image_two = $volunter_image_two;
		$save_volunteerdetail->save();

		return redirect('/volunteerdetail')->with('success','Volunteer Detail Added Successfully');

	}

	public function volunteerdetailedit($id){

		$editvolunteerdetail = VolunteerDetail::where('id',$id)->first();
		return view('index.volunteerDetail.edit',compact('editvolunteerdetail'));
	}

	public function volunteerdetailupdate(Request $request , $id){
    
    	$validated = $request->validate([
         'volunter_image_one' => 'mimes:jpg,png,jpeg|max:2048',
        'volunter_image_two' => 'mimes:jpg,png,jpeg|max:2048',
        'volunteer_heading ' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        'volunteer_title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'volunteer_description' => 'regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

		$volunteer_heading = $request->input('volunteer_heading');
		$volunteer_title = $request->input('volunteer_title');
		$volunteer_description = $request->input('volunteer_description');

		$volunter_image_one = $request->file('volunter_image_one');
		$volunter_image_two = $request->file('volunter_image_two');

		if($request->hasFile('volunter_image_one') != ""){
        	$filename =$volunter_image_one->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_one->move($destinationPath, $filename);
            $volunter_image_one = '/volunter_image/' . $filename;
            $update_volunteerdetail = VolunteerDetail::find($id);
            $update_volunteerdetail->volunter_image_one = $volunter_image_one;
            $update_volunteerdetail->save();
		}
		if($request->hasFile('volunter_image_two') != ""){
        	$filename =$volunter_image_two->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_two->move($destinationPath, $filename);
            $volunter_image_two = '/volunter_image/' . $filename;
            $update_volunteerdetail = VolunteerDetail::find($id);
            $update_volunteerdetail->volunter_image_two = $volunter_image_two;
            $update_volunteerdetail->save();
		}		

		$update_volunteerdetail = VolunteerDetail::find($id);
		$update_volunteerdetail->volunteer_heading = $volunteer_heading;
		$update_volunteerdetail->volunteer_title = $volunteer_title;
		$update_volunteerdetail->volunteer_description = $volunteer_description;
		$update_volunteerdetail->save();

		return redirect('/volunteerdetail')->with('success','Volunteer Detail Update Successfully');

	}

	public function volunteerdetaildelete($id){

		$save_volunteerdetail = VolunteerDetail::find($id);
		$save_volunteerdetail->deleted_status = '1';
		$save_volunteerdetail->save();

		return redirect('/volunteerdetail')->with('success','Volunteer Detail Deleted Successfully');
	}

	// Volunteer Image 
	

	// Home Page Detail
	public function homeDetail(){

		$homedetail_list = HomePageDetail::with('homeGeographicZone')->where('deleted_status','=','0')->paginate(10);
		return view('home.listing',compact('homedetail_list'));
	}

	public function addHomeDetail(){

		return view('home.add'); 
	}

	public function saveHomeDetail(Request $request){

		$validated = $request->validate([
        'banner_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'bg_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'intro_description' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wild_life_title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'wild_description' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'geographic_statement' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'geographic_zone' => 'required',
    	]);

		$banner_heading = $request->input('banner_heading');
		$bg_image = $request->file('bg_image');
		$title = $request->input('title');
		$intro_description = $request->input('intro_description');
		$wild_life_title = $request->input('wild_life_title'); 
		$wild_description = $request->input('wild_description');
		$geographic_statement = $request->input('geographic_statement');
		$geographic_zone = $request->input('geographic_zone');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/homebg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/homebg_image/' . $filename;

            $save_homedetail = new HomePageDetail();
		}
		$save_homedetail->banner_heading = $banner_heading;
		$save_homedetail->bg_image = $bg_image;
		$save_homedetail->title = $title;
		$save_homedetail->intro_description = $intro_description;
		$save_homedetail->wild_life_title = $wild_life_title;
		$save_homedetail->wild_description = $wild_description;
		$save_homedetail->geographic_statement = $geographic_statement;
		$save_homedetail->save();
		$home_detail_id = $save_homedetail->id;

		if(!empty($home_detail_id)){

			foreach($geographic_zone as $geographic_names){

				$save_geographic_zone = new HomeGeographicZone();
				$save_geographic_zone->home_id = $home_detail_id;
				$save_geographic_zone->geography_zone = $geographic_names;
				$save_geographic_zone->save();
			}
		}

		return redirect('/home_detail')->with('success','Home Page Detail Added Successfully');

	}

	public function editHomeDetail($id){

		$edithomedetail = HomePageDetail::with('homeGeographicZone')->where('id',$id)->first();
		return view('home.edit',compact('edithomedetail'));
	}

	public function updateHomeDetail(Request $request , $id){
    
    	$validated = $request->validate([
        'banner_heading' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        'bg_image' => 'mimes:jpg,png,jpeg|max:2048',
        // 'title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'intro_description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'wild_life_title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'wild_description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'geographic_statement' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
      
    	]);

		$bg_image = $request->file('bg_image');
		$banner_heading = $request->input('banner_heading');
		$title = $request->input('title');
		$intro_description = $request->input('intro_description');
		$wild_life_title = $request->input('wild_life_title');
		$wild_description = $request->input('wild_description');
		$geographic_zone = $request->input('geographic_zone');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/homebg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/homebg_image/' . $filename;

            $update_homedetail = HomePageDetail::find($id);
            $update_homedetail->bg_image = $bg_image;
            $update_homedetail->save();
		}
	

			$update_homedetail = HomePageDetail::find($id);
			$update_homedetail->banner_heading = $banner_heading;
			$update_homedetail->title = $title;
			$update_homedetail->intro_description = $intro_description;
			$update_homedetail->wild_life_title = $wild_life_title;
			$update_homedetail->wild_description = $wild_description;
			$update_homedetail->save();

			$id = $update_homedetail->id;

				if(!empty($geographic_zone)){

					foreach($geographic_zone as $geographic_names){

						$save_geographic_zone = new HomeGeographicZone();
						$save_geographic_zone->home_id = $id;
						$save_geographic_zone->geography_zone = $geographic_names;
						$save_geographic_zone->save();
					}
				}
					
	
	

		return redirect('/home_detail')->with('success','Home Page Detail Updated Successfully');

	}
	public function deleteHomeDetail($id){

		$delete_homeDetail = HomePageDetail::find($id);
		$delete_homeDetail->deleted_status = '1';
		$delete_homeDetail->save();

		return redirect('/home_detail')->with('success','Home Page Detail Deleted Successfully'); 
	}

	// wildlife Symbols
	public function wildlifeDetailListing(){

		$wildlifesymbol_list = WildlifeSymbol::with('get_wildlife_symbol_images')->where('deleted_status','=','0')->paginate(10);
		return view('wildLifeSymbol.listing',compact('wildlifesymbol_list'));
	}

	public function addwildlifeDetail(){

		return view('wildLifeSymbol.add');
	}

	public function savewildlifeDetail(Request $request){
    
		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
        'banner_title' => 'required|regex:/^[\.a-zA-Z,!?() ]*$/',
        'title' => 'required',
        'description' => 'required',
        'image' => 'required',
    	]);
		
    	// return $request->all();
    
		$banner_image = $request->file('banner_image');
		$banner_title = $request->input('banner_title');
		$title = $request->input('title');
		$description = $request->input('description');
		$image = $request->file('image');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/wildlifesymbol_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/wildlifesymbol_image/' . $filename;
  
		}
		$save_volunteerimage = new WildlifeSymbol();
        $save_volunteerimage->banner_image = $banner_image;
        $save_volunteerimage->banner_title = $banner_title;
		$save_volunteerimage->save();


		$wildlifeid = $save_volunteerimage->id;

          $image_data=array();
		if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/wildlifesymbol_image/', $image);
				$image_data[] = '/wildlifesymbol_image/'.$image;
            }
		}

		for($i=0;$i<count($title);$i++)
        {
        	$save_wildlife_data = new WildlifeSymbolTitleImage();
        	$save_wildlife_data->wildlife_symbol_id = $wildlifeid;
        	$save_wildlife_data->title=$title[$i];
        	$save_wildlife_data->description=$description[$i];
        	$save_wildlife_data->image=$image_data[$i];
        	
        	$save_wildlife_data->save();
        }

		// if(!empty($title)){
		// 	foreach($title as $t){
		// 			foreach($description as $d){
		// 				if(!empty($image)){
		// 					if ($request->hasfile('image')) {
		// 			            foreach ($request->file('image') as $file) {
		// 			                $image = $file->getClientOriginalName();
		// 			                $file->move(public_path() . '/wildlifesymbol_image/', $image);
					            

		// 			                $wildlife_save = new WildlifeSymbolTitleImage();
		// 			                $wildlife_save->wildlife_symbol_id = $wildlifeid;
		// 							$wildlife_save->title = $t;
		// 							$wildlife_save->description = $d;
		// 				            $wildlife_save->image = '/wildlifesymbol_image/'.$image;
		// 				            $wildlife_save->save();

		// 				            return $wildlife_save;die();
		// 			            }
  //          			 			return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Added Successfully');
  //       					}	
		// 				}
		// 			}
		// 	}
		// }
		
		 

		// 	if($file = $request->hasFile('image') != ""){
		// 		    $file = array();
  //       	$filename =$file->getClientOriginalName();
  //           $destinationPath = public_path('/wildlifesymbol_image');
  //           $image->move($destinationPath, $filename);
  //           $image = '/wildlifesymbol_image/' . $filename;

  //           foreach($image as $im){
  //           	$save_image = new WildlifeSymbol();
	 //            $save_image->image = $im;
	 //            $save_image->save();
  //           }
          
		// }
	
		

		// if($request->hasFile('image') != ""){
  //       	$filename =$image->getClientOriginalName();
  //           $destinationPath = public_path('/wildlifesymbol_image');
  //           $image->move($destinationPath, $filename);
  //           $image = '/wildlifesymbol_image/' . $filename;

  //           $save_volunteerimage = new WildlifeSymbol();
		// }		
		
	
		
		// $save_volunteerimage->title = $title;
		// $save_volunteerimage->image = $image;
		// $save_volunteerimage->save();

		return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Added Successfully');
	}

	public function editwildlifeDetail($id){

		$edit_wildlifesymbol = WildlifeSymbol::with('get_wildlife_symbol_images')->where('id',$id)->first();
		return view('wildLifeSymbol.edit',compact('edit_wildlifesymbol'));
	}

	public function updatewildlifeDetail(Request $request , $id){
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpeg,png,jpg,gif|max:2048',
        'banner_title' => 'regex:/^[\.a-zA-Z,!?() ]*$/',
    	]);

		// return $request->all();
		$banner_image = $request->file('banner_image');
		$banner_title = $request->input('banner_title');
		$title = $request->input('title');
		$image = $request->file('image');
		$description = $request->input('description');
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/wildlifesymbol_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/wildlifesymbol_image/' . $filename;

            $update_volunteerimage = WildlifeSymbol::find($id);
			$update_volunteerimage->banner_image = $banner_image;
			$update_volunteerimage->save();
		}
		
		else{
			$update_volunteerimage = WildlifeSymbol::find($id);
			$update_volunteerimage->banner_title = $banner_title;
			$update_volunteerimage->save();
		}
			

			// $wildlifeid = $update_volunteerimage->id;
			//  $image_data=array();

			// if($request->hasFile('image') != "")
	  //       {
	  //           foreach($request->file('image') as $key=>$file)
	  //           {
	  //               // $image = $file->getClientOriginalName();
	  //               $image = time() . '.' . $file->getClientOriginalName();
			// 		$file->move(public_path() . '/wildlifesymbol_image/', $image);
			// 		$image_data[] = '/wildlifesymbol_image/'.$image;
	  //           }
			// }

			// for($i=0;$i<count($title);$i++)
	  //       {
	  //       	$update_wildlife_data = new WildlifeSymbolTitleImage();
	  //       	$update_wildlife_data->title=$title[$i];
	  //       	$update_wildlife_data->description=$description[$i];
	  //       	$update_wildlife_data->image=$image_data[$i];
	  //       	$update_wildlife_data->save();

	  //       }
		
		

		return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Updated Successfully');
		
	}

	public function deletewildlifeDetail($id){

		$save_volunteerimage = WildlifeSymbol::find($id);
		$save_volunteerimage->deleted_status = '1';
		$save_volunteerimage->save();

		return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Deleted Successfully');

	}
// end class 
}