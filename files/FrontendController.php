<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;

use App\Models\DeerParkZooFrontend;

use App\Models\FooterScheme;
use App\Models\FooterAbout;
use App\Models\FooterAboutDetail;
use App\Models\FooterPolicyGuideline;

use App\Models\NotificationBannerDetail;
use App\Models\NotificationDetail;

use App\Models\OrganisationTabs;
use App\Models\OrganisationBannerDetail;
use App\Models\OrganisationTableDetail;

use App\Models\TourismBannerDetail;
use App\Models\TourismData;
use App\Models\TourismTypes;

use App\Models\TourismLinkDetail;
use App\Models\TourismLinkImages;

use App\Models\TenderBannerDetail;
use App\Models\TenderTypes;
use App\Models\TenderDetails;

use App\Models\MinisterDetail;

use App\Models\ZooType;
use App\Models\ZooDetail;

use App\Models\ZooGalleryBanner;
use App\Models\ZooGallery;

use App\Models\ContactUsBannerDetail;
use App\Models\ContactUs;
use App\Models\ContactUsDetail;
use App\Models\ContactUsMessage;



use App\Models\WorldDay;
use App\Models\WorldDayDetail;
use App\Models\WorldDayDetails;
use App\Models\WorldDayImagesDetail;
use App\Models\WorldDayImagesHonour;

use App\Models\HowApplyType;
use App\Models\HowToApplyDetail;

use App\Models\UpcomingEventDetail;
use App\Models\EventBgImages;

use App\Models\VolunteerDetail;

use App\Models\ImpLinkDetail;

use App\Models\ServicesDetail;
use App\Models\ServicesHowApply;

use App\Models\ActRuleBanner;
use App\Models\ActRulePdfDetail;
use App\Models\ActRulePdfTwo;
use App\Models\ActRulePdfThree;
use App\Models\ActRuleHeadings;
use App\Models\ActRuleManagementDetail;
use App\Models\ActManagementNangal;
use App\Models\ActManagementNangalPdf;
use App\Models\District;

use App\Models\Video;
use App\Models\ProtectedAreaSubcategory;

use DB;

class FrontendController extends Controller
{
   
	public function index()
    {
         // pa('hi');die();
        $param = $data = array();
        // pa($param);die;
        $get_banner_one = call_api('banner_list', $data);
        $get_banner_two = call_api('banner_two_list',$data);
    
        $data = array();

        $data['get_banner_one'] = $get_banner_one->list;
        $data['get_banner_two'] = $get_banner_two->list;
        
        $data['base_url'] = $get_banner_one->base_url;
        $data['minister_data'] = MinisterDetail::latest()->where('deleted_status','0')->get();
        $data['zoo_detail'] = ZooDetail::where('deleted_status','0')->take(6)->get();
        $data['video_detail'] = Video::take(6)->get();

        // $data['world_wetland'] = WorldDayDetail::where('deleted_status','0')->take(4)->get();
        $data['how_to_apply_type'] = HowApplyType::all();
        // $data['event_list'] = UpcomingEventDetail::where('deleted_status','0')->get();
        
        $data['event_bg_detail'] = EventBgImages::where('deleted_status','0')->first();
        $data['volunter_detail'] = VolunteerDetail::where('deleted_status','0')->latest()->first();

        $current_date = date('Y-m-d');
        $dayscount = date('Y-m-d', strtotime("-10 days", strtotime($current_date)));

        $data['world_list_previous'] = WorldDayDetail::whereDate('date_of_event','<=',$current_date)->where('deleted_status','0')->take(4)->latest()->get();

        $data['event_list'] = WorldDayDetail::whereDate('date_of_event','>=',$current_date)->where('deleted_status','0')->take(4)->latest()->get();
        $data['district_list'] = District::all();

    // return  $data['volunter_detail'];die;
        return view('frontend.index',$data);

    }
	   

    public function send_grid_email_v4($to, $to_names, $subject, $text,$attachment = null,$filename = null) {

        $to_email = [
            'email'=>$to,
            'name'=>$to_names
            ];
            
        $content = [
           'type'=>'text/html',
            'value'=>$text
            ];    
            
        $personalizations = [
            'to' => array($to_email),
            'subject' =>$subject 
            ]; 
            

        $from = [
            'email'=>'emobxdev@gmail.com',
            'name'=>'Application for wildlife volunteer.'
            ];     
        $reply_to = [
            'email'=>'emobxdev@gmail.com',
            'name'=>'Application for wildlife volunteer.'
            ];     

        $params = array();

        // https://api.sendgrid.com/v3/mail/send

        $params = [
            'personalizations' => array($personalizations),
            'from' => $from,
            'reply_to' => $reply_to,
            'content' => array($content),
            
            ];

        $header = array(
            "authorization: Bearer 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",
            "content-type: application/json"
          ); 
          
        //   echo json_encode($params); die;
        // pa(); die;    

        //           echo $to.$to_names.$subject; die;

            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($params),
          CURLOPT_HTTPHEADER => $header,
        ));



         $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);  

         if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo $response;
           return $response;
        }
         return true;

     }
  

    public function howToApplySave(Request $request){

        $type_id = $request->type_id;
        $description = $request->input('description');
        $email = $request->input('email');
        $mobile_number = $request->input('mobile_number');
        $district_id = $request->input('district_id');

       $type_name =  HowApplyType::where('id',$type_id)->first();
       $t_name = $type_name->type_name;

        $district =  District::where('id',$district_id)->first();
        $d_name = $district->district_name;

        $save = new HowToApplyDetail();
        $save->type_id = $type_id;
        $save->description = $description;
        $save->district_id = $district_id;
        $save->district_name =  $d_name;
        $save->email = $email;
        $save->mobile_number = $mobile_number;
        $save->save();

        

        $html = '<h2> Hi, </br></h2><b>Volunteer Email:</b> '.$request->input('email').'</br><p><b>Mobile Number:</b>&nbsp;&nbsp;'.$request->input('mobile_number').'</p></br><p><b>Wildlife Type:</b>&nbsp;&nbsp;'.$type_name->type_name.'</p></br><p><b> District:</b> &nbsp;&nbsp;'.$district->district_name.'</p></br>'.'<p><b>Division:</b> &nbsp;&nbsp;'.$request->input('description').'</p>';
    

        $this->send_grid_email_v4('neha.kumari@emobx.com',"",'Application for wildlife volunteer.',$html);

        return response()->json($save);
    }

	// Home Menu detail
    public function homePage(){

        $param = $data = array();
        $home_detail = call_api('home', $data);

        $data = array();
        $data['home_detail'] = $home_detail->list;

         $data['base_url'] = $home_detail->base_url;
        // return $data;
        return view('frontend.home.home',$data);
    }

    public function wildlifeSymbol(){
        
        $param = $data = array();
        $wildlife_detail = call_api('wildlife_symbols',$data);

        $data = array();
        $data['wildlife_detail'] = $wildlife_detail->list;

        $data['base_url'] = $wildlife_detail->base_url;
        // return $data;
        return view('frontend.statewildLife.wildlife_symbol',$data);
    }

    public function birMotiBagh(){

        $param = $data = array();
        $birmotibagh_detail = call_api('birmoti_bagh',$data);

        $data = array();
        $data['birmotibagh_detail'] = $birmotibagh_detail->list;

        $data['base_url'] = $birmotibagh_detail->base_url;
        // return $data;

        return view('frontend.protectedArea.wildlifeSantuaries.birmotibagh',$data);
    }

     public function birGurdialpur(){

        $param = $data = array();
        $birgurdialpur_detail = call_api('birgurdialpur',$data);

        $data = array();
        $data['birgurdialpur_detail'] = $birgurdialpur_detail->list;

        $data['base_url'] = $birgurdialpur_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.gurdialpur_detail',$data);
    }

    public function bir_bhunerheri(){

        $param = $data = array();
        $birbhunerheri_detail = call_api('bhunerheri_detail',$data);

        $data = array();
        $data['birbhunerheri_detail'] = $birbhunerheri_detail->list;

        $data['base_url'] = $birbhunerheri_detail->base_url;
         // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.bhunerheri_detail',$data);
    }

    public function bir_menhasDetail(){

        $param = $data = array();
        $mehas_detail = call_api('mehas_detail',$data);

        $data = array();
        $data['mehas_detail'] = $mehas_detail->list;

        $data['base_url'] = $mehas_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.mehas_detail',$data);
    }

    public function bir_dosanjhDetail(){

        $param = $data = array();
        $dosanjh_detail = call_api('dosanjh_detail',$data);

        $data = array();
        $data['dosanjh_detail'] = $dosanjh_detail->list;

        $data['base_url'] = $dosanjh_detail->base_url;
         // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.dosanjh_detail',$data);

    }

    public function bir_bhadsonDetail(){

        $param = $data = array();
        $bhadson_detail = call_api('bhadson_detail',$data);

        $data = array();
        $data['bhadson_detail'] = $bhadson_detail->list;

        $data['base_url'] = $bhadson_detail->base_url;
         // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.bhadson_detail',$data);
    }

    public function bir_aishwandetail(){

        $param = $data = array();
        $aishwan_detail = call_api('aishwan_detail',$data);

        $data = array();
        $data['aishwan_detail'] = $aishwan_detail->list;

        $data['base_url'] = $aishwan_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.aishwan_detail',$data);
    }

    public function bir_abohardetail(){

        $param = $data = array();
        $abohar_detail = call_api('abohar_detail',$data);

        $data = array();
        $data['abohar_detail'] = $abohar_detail->list;

        $data['base_url'] = $abohar_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.abohar_detail',$data);        
    }

    public function bir_harikedetail(){

        $param = $data = array();
        $harike_detail = call_api('harike_detail',$data);

        $data = array();
        $data['harike_detail'] = $harike_detail->list;

        $data['base_url'] = $harike_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.harike_detail',$data); 
    }

    public function bir_takhni_rehmapurdetail(){

        $param = $data = array();
        $takhni_rehmapur_detail = call_api('takhni_rehmapur_detail',$data);

        $data = array();
        $data['takhni_rehmapur_detail'] = $takhni_rehmapur_detail->list;

        $data['base_url'] = $takhni_rehmapur_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.takhni_rehmapurdetail',$data);         
    }

    public function bir_jhajjar_bachaulidetail(){

        $param = $data = array();
        $jhajjar_bachauli_detail = call_api('jhajjar_bachauli_detail',$data);

        $data = array();
        $data['jhajjar_bachauli_detail'] = $jhajjar_bachauli_detail->list;

        $data['base_url'] = $jhajjar_bachauli_detail->base_url;
        // return $data;
       return view('frontend.protectedArea.wildlifeSantuaries.jhajjar_bachaulidetail',$data);   
    }

    public function bir_kathlaurkushlian_detail(){

        $param = $data = array();
        $kathlaur_kushlian_detail = call_api('kathlaur_kushlian_detail',$data);

        $data = array();
        $data['kathlaur_kushlian_detail'] = $kathlaur_kushlian_detail->list;

        $data['base_url'] = $kathlaur_kushlian_detail->base_url;
        // return $data;
        return view('frontend.protectedArea.wildlifeSantuaries.kathlaurkushlian_detail',$data); 
    }

    public function bir_nangal_wildlife_detail(){

        $param = $data = array();
        $nangal_wildlife_detail = call_api('nangal_wildlife_detail',$data);

        $data = array();
        $data['nangal_wildlife_detail'] = $nangal_wildlife_detail->list;

        $data['base_url'] = $nangal_wildlife_detail->base_url;
        // return $data;
      return view('frontend.protectedArea.wildlifeSantuaries.nangal_wildlife_detail',$data);
    }

    public function panniwalaDetail(){

        $param = $data = array();
        $panniwala_detail = call_api('panniwalagumjal',$data);

        $data = array();
        $data['panniwala_detail'] = $panniwala_detail->list;

        $data['base_url'] = $panniwala_detail->base_url;
        // return $data;

        return view('frontend.communityReserve.panniwala.panniwala-detail',$data);
    }

    public function lalwanDetail(){

        $param = $data = array();
        $lalwan_detail = call_api('lalwan_detail',$data);

        $data = array();
        $data['lalwan_detail'] = $lalwan_detail->list;

        $data['base_url'] = $lalwan_detail->base_url;
        // return $data;

        return view('frontend.communityReserve.lalwanCommunity.lalwan_community',$data);
    }

    public function keshopurDetail(){

        $param = $data = array();
        $keshupur_detail = call_api('keshupur_detail',$data);

        $data = array();
        $data['keshupur_detail'] = $keshupur_detail->list;

        $data['base_url'] = $keshupur_detail->base_url;
        // return $data;

        return view('frontend.communityReserve.keshupur.keshupur_detail',$data);
    }

    public function siswanDetail(){

        $param = $data = array();
        $siswan_detail = call_api('siswan_detail',$data);

        $data = array();
        $data['siswan_detail'] = $siswan_detail->list;

        $data['base_url'] = $siswan_detail->base_url;
        // return $data;

        return view('frontend.communityReserve.siswanDetail.siswan_detail',$data);
    }

    public function rakhsaraiDetail(){

        $param = $data = array();
        $rakhsarai_detail = call_api('rakh_sarai',$data);

        $data = array();
        $data['rakhsarai_detail'] = $rakhsarai_detail->list;

        $data['base_url'] = $rakhsarai_detail->base_url;
        // return $data;

        return view('frontend.conservationReserve.rakhsarai_detail',$data);

    }

    public function roparetlandDetail(){

        $param = $data = array();
        $roparwetland_detail = call_api('ropar_wetland',$data);

        $data = array();
        $data['roparwetland_detail'] = $roparwetland_detail->list;

        $data['base_url'] = $roparwetland_detail->base_url;
        // return $data;
        return view('frontend.conservationReserve.ropar_wetland_detail',$data);        
    }

    public function ranjitsagarDamDetail(){

        $param = $data = array();
        $ranjit_sagar_dam = call_api('ranjit_sagar_dam',$data);

        $data = array();
        $data['ranjit_sagar_dam'] = $ranjit_sagar_dam->list;

        $data['base_url'] = $ranjit_sagar_dam->base_url;
         // return $data;
         return view('frontend.conservationReserve.ranjit_sagar_dam_detail',$data);    
    }

    public function beasRiverDetail(){

        $param = $data = array();
        $beas_river = call_api('beas_river',$data);

        $data = array();
        $data['beas_river'] = $beas_river->list;

        $data['base_url'] = $beas_river->base_url;
        // return $data;
         return view('frontend.conservationReserve.beas_river_detail',$data); 
    }
    public function kalibeinDetail(){

        $param = $data = array();
        $kali_bean_detail = call_api('kali_bean_detail',$data);

        $data = array();
        $data['kali_bean_detail'] = $kali_bean_detail->list;

        $data['base_url'] = $kali_bean_detail->base_url;
        // return $data;
         return view('frontend.conservationReserve.kali_bein_detail',$data);
    }

    public function harikasantuaryDetail(){

        $param = $data = array();
        $harike_santuary = call_api('harike_santuary',$data);

        $data = array();
        $data['harike_santuary'] = $harike_santuary->list;

        $data['base_url'] = $harike_santuary->base_url;
         // return $data;

        return view('frontend.protectedWetland.harika-detail',$data);
    }

    public function nangalsantuaryDetail(){

        $param = $data = array();
        $nangal_santuary = call_api('nangal_santuary',$data);

        $data = array();
        $data['nangal_santuary'] = $nangal_santuary->list;

        $data['base_url'] = $nangal_santuary->base_url;
         // return $data;

        return view('frontend.protectedWetland.nangal-detail',$data);
    }

    public function keshopur_detail(){

         $param = $data = array();
        $keshopur_detail = call_api('keshopur_detail',$data);

        $data = array();
        $data['keshopur_detail'] = $keshopur_detail->list;

        $data['base_url'] = $keshopur_detail->base_url;
        // return $data;
         return view('frontend.protectedWetland.keshopur-detail',$data);
    }

    public function chhhatbirbirzoo_Detail(){

        $param = $data = array();
        $chhatbir_zoo = call_api('chhatbir_zoo',$data);

        $data = array();
        $data['chhatbir_zoo'] = $chhatbir_zoo->list;

        $data['base_url'] = $chhatbir_zoo->base_url;
        // return $data;
        return view('frontend.chhatbirzoo.chhatbir_zoo_detail' , $data);        
    }

    public function ludhianazoo_Detail(){
        
        $param = $data = array();
        $ludhiana_zoo = call_api('ludhiana_zoo',$data);

        $data = array();
        $data['ludhiana_zoo'] = $ludhiana_zoo->list;

        $data['base_url'] = $ludhiana_zoo->base_url;
        // return $data;
        return view('frontend.ludhianazoo.ludhiana_zoo_detail',$data);
    }

    public function patialazoo_Detail(){
        
        $param = $data = array();
        $patiala_zoo = call_api('patiala_zoo',$data);

        $data = array();
        $data['patiala_zoo'] = $patiala_zoo->list;

        $data['base_url'] = $patiala_zoo->base_url;
        // return $data;
        return view('frontend.patialazoo.patiala_zoo_detail',$data);
    }

    public function bathindazoo_Detail(){

        $param = $data = array();
        $bathinda_zoo = call_api('bathinda_zoo',$data);

        $data = array();
        $data['bathinda_zoo'] = $bathinda_zoo->list;

        $data['base_url'] = $bathinda_zoo->base_url;
         // return $data;
        return view('frontend.bathindazoo.bathinda_zoo_detail',$data);
    }

    public function deerparkzoo_Detail(){

        $param = $data = array();
        $deerpark_neelon_zoo = call_api('deerpark_neelon_zoo',$data);

        $data = array();
        $data['deerpark_neelon_zoo'] = $deerpark_neelon_zoo->list;

        $data['base_url'] = $deerpark_neelon_zoo->base_url;
        // $data = DeerParkZooFrontend::all();
        // return $data;
        return view('frontend.deerparkNeelon.deerparkneelon_detail',$data);
    }

    public function schemedetail(){

        $list_scheme = FooterScheme::with('get_scheme_name')->where('deleted_status','0')->latest()->first();
        return view('frontend.footer.useful.scheme_detail',compact('list_scheme'));
    }

    public function aboutusdetail(){

         $aboutus_detail = FooterAbout::with('get_footer_about_detail')->where('deleted_status','0')->latest()->first();

        return view('frontend.footer.about.about_detail',compact('aboutus_detail'));
    }


    public function notificationdetail(){

       $notification_detail = NotificationBannerDetail::with('get_details')->latest()->first();
        return view('frontend.notification.notification_detail',compact('notification_detail'));
    }

    public function organisationdetail(){

        $organisation_tab_detail = OrganisationTabs::all();
        
        $data['tab_1'] = OrganisationBannerDetail::with('get_detail_t1')->first();
        $data['tab_2'] = OrganisationBannerDetail::with('get_detail_t2')->first();
        $data['tab_3'] = OrganisationBannerDetail::with('get_detail_t3')->first();
        $data['tab_4'] = OrganisationBannerDetail::with('get_detail_t4')->first();
        $data['tab_5'] = OrganisationBannerDetail::with('get_detail_t5')->first();
        $data['tab_6'] = OrganisationBannerDetail::with('get_detail_t6')->first();
        $data['tab_7'] = OrganisationBannerDetail::with('get_detail_t7')->first();
        // return $data;

        return view('frontend.organisation.organisation_detail',compact('organisation_tab_detail','data'));
    }

    public function tourismdetail(){
        $banner_detail = TourismBannerDetail::latest()->first();
        $tourism_detail = TourismData::with('get_bdata','get_tourism_type')->get();
        return view('frontend.tourism.tourism_detail',compact('tourism_detail','banner_detail'));
    }

   public function tourismGallerydetail($id){

       // $total_image_count = TourismLinkImages::where('tourism_link_id',$id)->count();
        $total_image_count = DB::table('tourism_link_detail')
            ->join('tourism_link_images', 'tourism_link_detail.tourism_type_id', '=', 'tourism_link_images.tourism_type_id')
            ->where('tourism_link_detail.deleted_status',  '=', '0')
            ->count();

       $tourism_image_detail = TourismLinkDetail::with('get_images')->where('tourism_type_id',$id)->where('deleted_status','0')->first();
        return view('frontend.tourism.tourism_page',compact('tourism_image_detail','total_image_count'));
    }
    public function tenderDetails(){

        $tender_type = TenderTypes::all();
        $tender_list_t1 = TenderBannerDetail::with('get_tender_detail_t1')->where('deleted_status','0')->first();

        $tender_list_t2 = TenderBannerDetail::with('get_tender_detail_t2')->where('deleted_status','0')->first();
        return view('frontend.tender.tender_detail',compact('tender_list_t1','tender_list_t2','tender_type'));
    }

    public function viewGalleryDetail(){

        $zoogallerybanner_detail = ZooGalleryBanner::where('deleted_status','0')->latest()->first();
        $zoo_type_detail = ZooType::all();
        $gallery_list = ZooGallery::all()->take(12);
        $gallery_count = ZooGallery::count();
        return view('frontend.zoo_gallery',compact('zoogallerybanner_detail','zoo_type_detail','gallery_list','gallery_count'));
    }

    public function getZooImages(Request $request){

        // return $request->all();
        if($request->zoo_gallery =="0"){
            $gallery_list = ZooGallery::pluck('zoo_image')->take(12);
        }
        else {
             $gallery_list = ZooGallery::where('zoo_type_id',$request->zoo_gallery)->pluck('zoo_image');
         }
       
        return response()->json($gallery_list);
    }

    public function contactUsDetail(){

        $contactbannerdetail  = ContactUsBannerDetail::where('deleted_status','0')->latest()->first();
        $contactdetail = ContactUsDetail::where('deleted_status','0')->take(3)->get();
       return view('frontend.footer.contactUs.contactUsdetail',compact('contactdetail','contactbannerdetail'));
    }

    public function contactUsMessage(Request $request){

        // return $request->all();
        $full_name = $request->input('full_name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $your_message = $request->input('your_message');

        $save_message = new ContactUsMessage();
        $save_message->full_name = $full_name;
        $save_message->email = $email;
        $save_message->subject = $subject;
        $save_message->your_message = $your_message;
        $save_message->save();

        return response()->json($save_message);

    }

    public function getDetail($id){

       $images_world_day = WorldDayDetails::with('get_images_detail','get_images_honour_detail')->where('world_day_type_id',$id)->get();
        return view('frontend.image_detail',compact('images_world_day'));
    }

    public function impLinkDetail(){

        $impDetail = ImpLinkDetail::where('deleted_status','0')->latest()->first();
        return view('frontend.imp_detail',compact('impDetail'));
    }

    public function serviceDetail(){

        $service_detail =  ServicesDetail::where('deleted_status','0')->latest()->first();
        return view('frontend.services.services_detail',compact('service_detail'));
    }

    public function addserviceHowApply(Request $request){

        $name = $request->input('name');
        $email_address = $request->input('email_address');
        $mobile_number = $request->input('mobile_number');
        $aadhar_number = $request->input('aadhar_number');
        $address_one = $request->input('address_one');
        $address_two = $request->input('address_two');
        $city = $request->input('city');
        $zip = $request->input('zip');

        $save_howApplyServices = new ServicesHowApply();
        $save_howApplyServices->name = $name;
        $save_howApplyServices->email_address = $email_address;
        $save_howApplyServices->mobile_number = $mobile_number;
        $save_howApplyServices->aadhar_number = $aadhar_number;
        $save_howApplyServices->address_one = $address_one;
        $save_howApplyServices->address_two = $address_two;
        $save_howApplyServices->city = $city;
        $save_howApplyServices->zip = $zip;
        $save_howApplyServices->save();

    }

    public function serviceHowApply(Request $request){

        $name = $request->input('name');
        $email_address = $request->input('email_address');
        $mobile_number = $request->input('mobile_number');
        $aadhar_number = $request->input('aadhar_number');
        $address_one = $request->input('address_one');
        $address_two = $request->input('address_two');
        $city = $request->input('city');
        $zip = $request->input('zip');

        $save_howApplyServices = new ServicesHowApply();
        $save_howApplyServices->name = $name;
        $save_howApplyServices->email_address = $email_address;
        $save_howApplyServices->mobile_number = $mobile_number;
        $save_howApplyServices->aadhar_number = $aadhar_number;
        $save_howApplyServices->address_one = $address_one;
        $save_howApplyServices->address_two = $address_two;
        $save_howApplyServices->city = $city;
        $save_howApplyServices->zip = $zip;
        $save_howApplyServices->save();

        return response()->json($save_howApplyServices);
    }

    public function exportCsvServices(Request $request)
    {
       $fileName = 'services.csv';
        $task = ServicesHowApply::latest('id')->first();
        // return $tasks;
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('name', 'email_address', 'mobile_number', 'aadhar_number', 'address_one', 'address_two', 'city','zip');

            $callback = function() use($task, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

              
                    $row['name']  = $task->name;
                    $row['email_address']    = $task->email_address;
                    $row['mobile_number']    = $task->mobile_number;
                    $row['aadhar_number']  = $task->aadhar_number;
                    $row['address_one']  = $task->address_one;
                    $row['address_two']  = $task->address_two;
                    $row['city']  = $task->city;
                    $row['zip']  = $task->zip;

                    fputcsv($file, array($row['name'], $row['email_address'], $row['mobile_number'], $row['aadhar_number'], $row['address_one'], $row['address_two'], $row['city'], $row['zip']));
               

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }

		 public function actsRuleDetail(){
            // return "hello";
            $act_rule_list = ActRuleBanner::with('get_pdf_detail_one','get_pdf_two','get_pdf_three','get_actRuleHeadings','get_actRuleManagementHeadings','get_actRulenangalHeadings','get_actRulenangalpdfHeadings','get_management_headings','get_management_headings.getsubheading')->get();    
            return view('frontend.footer.actRule.acts_rule_detail',compact('act_rule_list'));
        }

        // public function calendarDetail(){
           
        //           $event = WorldDayDetail::where('deleted_status','=','0')->get(['type_name','date_of_event']);
        //         // $data['event_detail']= $this->getEventsDetails();
        //         // return response()->json($event);
        //           // return response()->json($event);
        //     return view('frontend.calendar.calendar');
        // }

        public function getEventsDetails(){

      
    

            $event = WorldDayDetail::where('deleted_status','=','0')->get();
             // $data=[];
             // foreach($event as $events){
             //    $subArr=[
             //        'id'=> $events->id,
             //        'type_name'=> $events->type_name,
             //        'date_of_event' => $events->date_of_event
             //    ];
             //    array_push($data,$subArr);
             // }
             // return $data;
        //     foreach($lastTenDates as $date_value ){
        //     $d=array();
        //      // $d['date']=date('l',strtotime($date_value));
        //      $d['type_name'] = WorldDayDetail::where('deleted_status','=','0')->get('type_name');
        //      $d['date_of_event']=WorldDayDetail::whereDate('date_of_event',date('Y-m-d'))->get();
        //      $dd[]=$d;
        //     }
        // return $dd;
   
             foreach($event as $events ){
                $data=array();
             $data['type_name'] = $events->type_name;
             $data['date_of_event'] = $events->date_of_event;
             $dd[]=$data;
        }
        return $dd;
             

        }


        public function indexCalendardetail(Request $request)
        {
            if($request->ajax())
            {
                $data =  WorldDayDetail::whereDate('date_of_event', '>=', $request->start)->where('deleted_status','=','0')->get('date_of_event');
                return response()->json($data);
            }
            return view('frontend.calendar.calendar');
        }

    public function action(Request $request)
    {
        if($request->ajax())
        {
            if($request->type == 'add')
            {
                $event = WorldDayDetail::create([
                    'type_name'     =>  $request->title,
                    'date_of_event'     =>  $request->start
                ]);

                return response()->json($event);
            }

            if($request->type == 'update')
            {
                $event = WorldDayDetail::find($request->id)->update([
                    'type_name'     =>  $request->title,
                    'date_of_event'     =>  $request->start
                ]);

                return response()->json($event);
            }

            if($request->type == 'delete')
            {
                $event = WorldDayDetail::find($request->id)->delete();

                return response()->json($event);
            }
        }
    }

    public function view_all_videos(){

         $zoogallerybanner_detail = ZooGalleryBanner::where('deleted_status','0')->latest()->first();
        $zoo_type_detail = ZooType::all();
        $protected_area = ProtectedAreaSubcategory::get();
       $gallery_list = Video::all()->take(12);
        $gallery_count = ZooGallery::count();
        return view('frontend.all_videos',compact('zoogallerybanner_detail','zoo_type_detail','gallery_list','gallery_count','protected_area'));

       
    }

    public function getAllVideos(Request $request){

        // return $request->all();
        if($request->zoo_gallery =="0"){
            $gallery_list = Videos::pluck('zoo_image')->take(12);
        }
        else {
             $gallery_list = Video::where('type',$request->zoo_gallery)->pluck('video_link');
         }
       
        return response()->json($gallery_list);
    }
		
// end class 
}