<?php

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('banner_list',[\App\Http\Controllers\ApiController::class,'banner_list']);
Route::get('banner_two_list',[\App\Http\Controllers\ApiController::class,'banner_two_list']);

Route::get('get_world_day',[\App\Http\Controllers\ApiController::class,'get_world_day']);

Route::get('picture_detail',[\App\Http\Controllers\ApiController::class,'picture_detail']);
Route::get('chhatbirzoo_detail',[\App\Http\Controllers\ApiController::class,'chhatbirzoo_detail']);
Route::get('ludhianazoo_detail',[\App\Http\Controllers\ApiController::class,'ludhianazoo_detail']);
Route::get('minizoopatiala_detail',[\App\Http\Controllers\ApiController::class,'minizoopatiala_detail']);
Route::get('minizoobathinda_detail',[\App\Http\Controllers\ApiController::class,'minizoobathinda_detail']);
Route::get('deer_park_neelon_detail',[\App\Http\Controllers\ApiController::class,'deer_park_neelon_detail']);
Route::get('chhatbir_detail',[\App\Http\Controllers\ApiController::class,'chhatbir_detail']);

// event day 
Route::get('event_forest_day',[\App\Http\Controllers\ApiController::class,'event_forest_day']);
Route::get('event_worldwildlife',[\App\Http\Controllers\ApiController::class,'event_worldwildlife']);
Route::get('event_world_wetland',[\App\Http\Controllers\ApiController::class,'event_world_wetland']);
Route::get('event_migratory_bird',[\App\Http\Controllers\ApiController::class,'event_migratory_bird']);
Route::get('bg_image',[\App\Http\Controllers\ApiController::class,'bg_image']);
Route::get('bg_imageheading',[\App\Http\Controllers\ApiController::class,'bg_imageheading']);
Route::get('volunteer_detail',[\App\Http\Controllers\ApiController::class,'volunteer_detail']);
Route::get('volunteer_images',[\App\Http\Controllers\ApiController::class,'volunteer_images']);

// home page
Route::get('home',[\App\Http\Controllers\ApiController::class,'home']);

// get wildlife Symbol 

Route::get('/wildlife_symbols',[\App\Http\Controllers\ApiController::class,'wildlife_symbols']);


Route::get('calendar',[\App\Http\Controllers\ApiController::class,'calendarDetail']);







