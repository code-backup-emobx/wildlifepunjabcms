<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FooterAboutDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'footer_about_detail';

}