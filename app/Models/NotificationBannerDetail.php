<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class NotificationBannerDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'notification_banner_detail';

    public function get_details(){

    	return $this->hasMany('App\Models\NotificationDetail','banner_id')->where('deleted_status','0');
    }
}