<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TourismTypes extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tourism_types';
}