<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class WorldDayDetails extends Model
{
    use HasFactory,SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $table = 'world_day_details';

    public function get_images_detail(){

    	return $this->hasMany('App\Models\WorldDayImagesDetail','world_day_id','worldday_type_id');
    }

    public function get_images_honour_detail(){

    	return $this->hasMany('App\Models\WorldDayImagesHonour','world_day_detail_id');
    }

   

    public function get_detail_world(){

    	return $this->belongsTo('App\Models\WorldDayDetail','world_day_type_id');
    }
}