<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ContactUsDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'contact_us_detail';
}