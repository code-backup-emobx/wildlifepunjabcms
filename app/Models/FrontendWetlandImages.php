<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class FrontendWetlandImages extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'frontend_wetland_images';

   public function wetland_name(){

   	return $this->belongsTo('App\Models\ProtectedWetland','wetland_id');
   }
}