<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class ActRuleManagementHeading extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'act_rule_management_heading';

    public function getsubheading(){

    	return $this->hasMany('App\Models\ActRuleSubheading','management_heading_id');
    }
}