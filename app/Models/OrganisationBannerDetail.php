<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OrganisationBannerDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'organisation_banner_detail';

    public function get_tab_detail(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0');
    }
    public function get_tab_name(){

        return $this->hasMany('App\Models\OrganisationTabs','tab_id');
    }

    public function get_detail_t1(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','1');
    }

    public function get_detail_t2(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','2');
    }

    public function get_detail_t3(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','3');
    }

    public function get_detail_t4(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','4');
    }

    public function get_detail_t5(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','5');
    }

    public function get_detail_t6(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','6');
    }

    public function get_detail_t7(){

    	return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','7');
    }
 
    public function get_detail_t8(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','8');
    }
    public function get_detail_t9(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','9');
    }
    public function get_detail_t10(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','10');
    }
    public function get_detail_t11(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','11');
    }
    public function get_detail_t12(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','12');
    }
    public function get_detail_t13(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','13');
    }
    public function get_detail_t14(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','14');
    }
    public function get_detail_t15(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','15');
    }
    public function get_detail_t16(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','16');
    }
    public function get_detail_t17(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','17');
    }
    public function get_detail_t18(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','18');
    }
    public function get_detail_t19(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','19');
    }
    public function get_detail_t20(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','20');
    }
    public function get_detail_t21(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','21');
    }
    public function get_detail_t22(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','22');
    }
    public function get_detail_t23(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','23');
    }
    public function get_detail_t24(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','24');
    }
    public function get_detail_t25(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','25');
    }
    public function get_detail_t26(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','26');
    }
    public function get_detail_t27(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','27');
    }
    public function get_detail_t28(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','28');
    }
    public function get_detail_t29(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','29');
    }
    public function get_detail_t30(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','30');
    }
    public function get_detail_t31(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','31');
    }
    public function get_detail_t32(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','32');
    }
    public function get_detail_t33(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','33');
    }
    public function get_detail_t34(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','34');
    }
    public function get_detail_t35(){

        return $this->hasMany('App\Models\OrganisationTableDetail','banner_id')->where('deleted_status','0')->where('tab_id','35');
    }
}