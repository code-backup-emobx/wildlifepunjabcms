<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class EventBgImages extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'event_bg_images';
}