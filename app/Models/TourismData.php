<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TourismData extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tourism_data';

    public function get_bdata(){

    	return $this->belongsTo('App\Models\TourismBannerDetail','banner_id')->where('deleted_status','0');
    }

    public function get_bannerdata(){

    	return $this->hasOne('App\Models\TourismBannerDetail','id')->where('deleted_status','0');
    }

    public function get_tourism_type(){

    	return $this->belongsTo('App\Models\TourismTypes','tourism_type_id');
    }
}