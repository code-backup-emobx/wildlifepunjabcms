<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PatialaZooFrontend extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'patiala_zoo_frontend';
}