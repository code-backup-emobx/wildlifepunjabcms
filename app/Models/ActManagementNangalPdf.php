<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ActManagementNangalPdf extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'management_nangal_pdf';
}