<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class FrontendZooDetail extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'frontend_zoo_detail';

   public function get_zoo_name(){

   	 return $this->belongsTo('App\Models\ZooType','type_id');
   }
}