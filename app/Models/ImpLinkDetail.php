<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ImpLinkDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'imp_link_detail';

	public function get_links(){
    	return $this->hasMany('App\Models\Implinklist','banner_id');
    }
}