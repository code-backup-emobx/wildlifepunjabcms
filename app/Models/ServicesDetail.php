<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ServicesDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'services_detail';

}