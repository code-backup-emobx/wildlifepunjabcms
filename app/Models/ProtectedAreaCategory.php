<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class ProtectedAreaCategory extends Model
{
	use HasFactory,SoftDeletes;
	
    protected $primaryKey = 'id';
    protected $table = 'protectedarea_category';

    public function get_subcat(){

    	return $this->hasMany('App\Models\ProtectedAreaSubcategory','category_id');
    }
}