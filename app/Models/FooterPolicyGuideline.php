<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FooterPolicyGuideline extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'footer_policy_pdf';
}