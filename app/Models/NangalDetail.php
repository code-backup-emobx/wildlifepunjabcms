<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class NangalDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'nangal_detail';

     public function get_nangal_images(){

    	return $this->hasMany('App\Models\NangalImages','harika_detail_id');
    }
}