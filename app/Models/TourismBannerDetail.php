<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TourismBannerDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tourism_banner_detail';

    public function get_bannerdata(){

    	return $this->hasOne('App\Models\TourismData','banner_id');
    }
}