<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TenderDetails extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tender_details';

    public function get_tender_detail(){

    	return $this->hasMany('App\Models\TenderBannerDetail','banner_id');
    }

    public function get_tender_type(){

    	return $this->hasMany('App\Models\TenderTypes','tender_type_id');
    }
}