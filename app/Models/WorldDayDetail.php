<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WorldDayDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'world_day_detail';
    // use HasFactory;

	protected $fillable = [
		'type_name', 'date_of_event'
	];
}