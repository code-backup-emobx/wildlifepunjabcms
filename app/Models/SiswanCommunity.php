<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SiswanCommunity extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'siswan_community';

    public function get_notification_detail(){

    	return $this->hasMany('App\Models\SiswanNotificationDetail','siswan_id');
    }
 
}