<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class WetlandAuthority extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'wetland_authority';

    public function get_notification(){

    	return $this->hasMany('App\Models\WetlandNotification','wetland_id');
    }

    public function get_rules(){

    	return $this->hasMany('App\Models\WetlandRules','wetland_id');
    	
    }
 
}