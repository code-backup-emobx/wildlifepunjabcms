<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ZooGallery extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'zoo_gallery';

    public function get_count_image(){

    	return $this->hasMany('App\Models\ZooType','zoo_type_id');
    }

}