<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PictureDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'picture_detail';
}