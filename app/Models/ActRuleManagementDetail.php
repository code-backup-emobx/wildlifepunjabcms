<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ActRuleManagementDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'act_rule_management_detail';
}