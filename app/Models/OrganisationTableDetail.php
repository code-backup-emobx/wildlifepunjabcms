<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class OrganisationTableDetail extends Model
{
	use HasFactory,SoftDeletes;	

    protected $primaryKey = 'id';
    protected $table = 'organisation_table_detail';

    public function get_tab_name(){

    	return $this->belongsTo('App\Models\OrganisationTabs','tab_id');
    }

    public function get_detail(){

    	return $this->belongsTo('App\Models\OrganisationBannerDetail','banner_id')->where('deleted_status','0');
    }

}