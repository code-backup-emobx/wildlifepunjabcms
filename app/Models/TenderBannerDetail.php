<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TenderBannerDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tender_banner_detail';

     public function get_tender_detail(){

    	return $this->hasMany('App\Models\TenderDetails','banner_id');
    }

    public function get_tender_detail_t1(){

    	return $this->hasMany('App\Models\TenderDetails','banner_id')->where('tender_type_id','1')->where('deleted_status','0');
    }

    public function get_tender_detail_t2(){

    	return $this->hasMany('App\Models\TenderDetails','banner_id')->where('tender_type_id','2')->where('deleted_status','0');
    }
}