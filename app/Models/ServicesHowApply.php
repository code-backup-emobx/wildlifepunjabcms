<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ServicesHowApply extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'services_how_to_apply';
}