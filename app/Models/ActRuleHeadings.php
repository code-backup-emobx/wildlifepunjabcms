<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ActRuleHeadings extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'act_rule_headings';

    public function get_wildlife_detail(){

    	return $this->hasMany('App\Models\ActRulePdfTwo','wildlife_detail_id')->where('deleted_status','0');
    }
}