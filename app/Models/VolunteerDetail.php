<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class VolunteerDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'volunteer_detail';
}