<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ZooDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'zoo_detail';
}