<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ActRulePdfDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'act_rule_pdf_detail';
}