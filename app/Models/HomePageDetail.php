<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class HomePageDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'home_page_detail';

    public function homeGeographicZone(){

    	return $this->hasMany('App\Models\HomeGeographicZone','home_id')->where('deleted_status','0');
    }
}