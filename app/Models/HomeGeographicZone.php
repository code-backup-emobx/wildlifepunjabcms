<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class  HomeGeographicZone extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'home_geographic_zone';
}