<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ActRuleBanner extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'act_rule_banner';

    public function get_pdf_detail_one(){

    	return $this->hasMany('App\Models\ActRulePdfDetail','banner_id')->where('deleted_status','0')->latest();
    }

    public function get_actRuleHeadings(){

        return $this->hasMany('App\Models\ActRuleHeadings','banner_id')->where('deleted_status','0');
    }

    public function get_pdf_two(){

    	return $this->hasMany('App\Models\ActRulePdfTwo','banner_id')->where('deleted_status','0');
    }

    public function get_actRuleManagementHeadings(){

        return $this->hasMany('App\Models\ActRuleManagementDetail','banner_id')->where('deleted_status','0');
    }

    public function get_pdf_three(){

    	return $this->hasMany('App\Models\ActRulePdfThree','banner_id')->where('deleted_status','0');
    }

   

    public function get_actRulenangalHeadings(){

        return $this->hasMany('App\Models\ActManagementNangal','banner_id')->where('deleted_status','0');
    }

    public function get_actRulenangalpdfHeadings(){

        return $this->hasMany('App\Models\ActManagementNangalPdf','banner_id');
    }

    public function get_management_headings(){

        return $this->hasMany('App\Models\ActRuleManagementHeading','act_rule_id');
    }

}