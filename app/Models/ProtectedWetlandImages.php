<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class ProtectedWetlandImages extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'protected_wetland_images';
 	
}