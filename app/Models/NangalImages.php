<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class NangalImages extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'nangal_images';
}