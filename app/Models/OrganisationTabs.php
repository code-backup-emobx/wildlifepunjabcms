<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class OrganisationTabs extends Model
{
	use HasFactory,SoftDeletes;
	
    protected $primaryKey = 'id';
    protected $table = 'organisation_tabs';
}