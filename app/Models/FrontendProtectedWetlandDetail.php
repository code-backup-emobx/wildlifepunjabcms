<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class FrontendProtectedWetlandDetail extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'frontend_protected_wetland_detail';
 	
 	public function get_detail(){

 		return $this->hasMany('App\Models\ProtectedWetlandNotification','p_w_id');
 	}

 	public function get_type_name(){

 		return $this->belongsTo('App\Models\ProtectedWetland','type_id');
 	}
	 public function get_notification(){

    	return $this->hasMany('App\Models\FrontendProtectedWetlandNotification','front_protwet_id');
    }

}