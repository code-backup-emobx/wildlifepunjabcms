<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WildlifeSymbolTitleImage extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'wildlife_symbol_title_image';
}