<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FooterSchemeName extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'footer_scheme_name';
}