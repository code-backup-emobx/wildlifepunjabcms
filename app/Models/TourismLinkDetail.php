<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TourismLinkDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tourism_link_detail';

    public function get_images(){

    	return $this->hasMany('App\Models\TourismLinkImages','tourism_link_id');
    }
}