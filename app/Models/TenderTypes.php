<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TenderTypes extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tender_types';

}