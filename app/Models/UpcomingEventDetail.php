<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UpcomingEventDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'upcoming_event_detail';
}