<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class NotificationPdfDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'notification_pdf_detail';

}