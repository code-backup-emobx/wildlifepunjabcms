<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WildlifeSymbol extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'wildlife_symbol';

    public function get_wildlife_symbol_images(){

    	return $this->hasmany('App\Models\WildlifeSymbolTitleImage','wildlife_symbol_id');
    }
}