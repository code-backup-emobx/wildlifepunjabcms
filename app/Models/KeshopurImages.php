<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class KeshopurImages extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'keshopur_images';
}