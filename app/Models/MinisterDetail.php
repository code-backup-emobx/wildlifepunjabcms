<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MinisterDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'minister_detail';

}