<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FooterAbout extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'footer_about';

   
    public function get_footer_about_detail(){

    	return $this->hasOne('App\Models\FooterAboutDetail','banner_id');
    }
}