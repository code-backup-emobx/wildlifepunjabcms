<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ContactUsBannerDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'contact_us_banner_detail';
}