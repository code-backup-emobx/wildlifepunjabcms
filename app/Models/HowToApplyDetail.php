<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class HowToApplyDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'how_to_apply_detail';
}