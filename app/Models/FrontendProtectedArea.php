<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class FrontendProtectedArea extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'frontend_protected_area';

    public function get_notification(){

    	return $this->hasMany('App\Models\FrontendProtectedAreaNotification','front_protarea_id');
    }
 	
 	public function get_cat(){

		return $this->belongsTo('App\Models\ProtectedAreaCategory','category_id');
 	}

 	public function get_subcat(){

		return $this->belongsTo('App\Models\ProtectedAreaSubcategory','subcategory_id');
 	}

}