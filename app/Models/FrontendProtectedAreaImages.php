<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class FrontendProtectedAreaImages extends Model
{
	use HasFactory,SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'frontend_protected_area_images';

   public function get_subcat_name(){

   	return $this->belongsTo('App\Models\ProtectedAreaSubcategory','subcat_id');
   }
}