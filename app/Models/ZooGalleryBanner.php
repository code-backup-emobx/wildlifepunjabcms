<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ZooGalleryBanner extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'zoo_gallery_banner';

}