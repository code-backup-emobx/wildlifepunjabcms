<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class ProtectedAreaSubcategory extends Model
{
	use HasFactory,SoftDeletes;
	
    protected $primaryKey = 'id';
    protected $table = 'protected_area_subcategory';

    public function get_cat(){

    	return $this->belongsTo('App\Models\ProtectedAreaCategory','category_id');
    }
}