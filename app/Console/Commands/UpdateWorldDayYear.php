<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProjectSpendHours;

class UpdateWorldDayYear extends Command

{

   /**

    * The name and signature of the console command.

    *

    * @var string

    */

   protected $signature = 'year:update';



   /**

    * The console command description.

    *

    * @var string

    */

   protected $description = 'update the year at end of the year';



   /**

    * Create a new command instance.

    *

    * @return void

    */

   public function __construct()

   {

       parent::__construct();

   }



   /**

    * Execute the console command.

    *

    * @return mixed

    */

   public function handle(){
     
      $update_year = DB::table('word_day_view')->where('deleted_at',null)->get();
   		return $update_year;
   }

}