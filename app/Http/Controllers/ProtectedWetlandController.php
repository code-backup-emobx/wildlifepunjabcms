<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\ProtectedWetland;
use App\Models\FrontendProtectedWetlandDetail;
use App\Models\ProtectedWetlandNotification;
use App\Models\FrontendWetlandImages;
use App\Models\GalleryImages;
use App\Models\GalleryAllImages;

use App\Models\FrontendProtectedWetlandNotification;
use Validator;
use Session;
use Image;

class ProtectedWetlandController extends Controller
{

	public function wetlandMasterList(){

		$list = ProtectedWetland::paginate('10');
		return view('protectedWetland.master.list',compact('list'));
	}

	public function addNewMaster(){

		return view('protectedWetland.master.add');
	}
	public function saveNewMaster(Request $request){

		$rules = [
        	'name' => ['required', 'regex:/^[\.a-zA-Z,!? ]*$/'],
		];

	    $customMessages = [
	        'regex' => 'Please enter the name in letters.'
	    ];

    	$this->validate($request, $rules, $customMessages);

		$name = $request->input('name');
		$save_master = new ProtectedWetland();
		$save_master->name = $name;
		$save_master->save();
		return redirect('/wetland-master-list')->with('success','Protected Wetland Master Name Added Successfully');
	}
	public function editMaster($id){
		$edit = ProtectedWetland::where('id',$id)->first();
		return view('protectedWetland.master.edit',compact('edit','id'));
	}
	public function updateMaster(Request $request , $id){

		$name = $request->input('name');
		$save_master = ProtectedWetland::find($id);
		$save_master->name = $name;
		$save_master->save();
		return redirect('/wetland-master-list')->with('success','Protected Wetland Master Updated Successfully');
	}
	public function deleteMaster($id){

		ProtectedWetland::where('id',$id)->delete();
      FrontendWetlandImages::where('wetland_id',$id)->delete();
    GalleryImages::where('type_id',$id)->delete();
    GalleryAllImages::where('type_id',$id)->delete();
    FrontendProtectedWetlandDetail::where('type_id',$id)->delete();
    FrontendProtectedWetlandNotification::where('type_id',$id)->delete();
		return redirect('/wetland-master-list')->with('success','Protected Wetland Master Deleted Successfully');
	}

	public function protectedWetlandList(){
		
		$list = FrontendProtectedWetlandDetail::with('get_type_name')->paginate(10);
		foreach($list as $count_images){

			$count_images->image_count = ProtectedWetlandNotification::where('p_w_id',$count_images->id)->count();
        $count_images->pdf_count = FrontendProtectedWetlandNotification::where('front_protwet_id',$count_images->id)->count();
		}
		// return $list;
		return view('protectedWetland.list',compact('list'));
	}
	public function deleteprotectedWetland($id , $type_id){
	// return 1; die;
		FrontendProtectedWetlandDetail::where('id',$id)->delete();
		ProtectedWetlandNotification::where('p_w_id',$id)->delete();
        FrontendProtectedWetlandNotification::where('front_protwet_id',$id)->where('type_id',$type_id)->delete();
		return redirect('/protected-wetland-list')->with('success','Protected Wetland Detail Deleted Successfully');
	}
	public function editprotectedWetland($id){
		$type_list = ProtectedWetland::get();
		$edit_wet_prot = FrontendProtectedWetlandDetail::with('get_type_name')->where('id',$id)->first();
		return view('protectedWetland.edit',compact('edit_wet_prot','type_list','id'));
	}
	public function updateprotectedWetlandList(Request $request , $id){
    
    	$validated = $request->validate([
       	'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
      ]);
		// return $request->all();
  		$type_id = $request->type_id;
  		$banner_image = $request->file('banner_image');
  		$description = $request->description;
  		$altitude = $request->input('altitude');
  		$location = $request->input('location');
  		$access = $request->input('access');
  		$latitude = $request->input('latitude');
  		$longitude = $request->input('longitude');
  		$flora = $request->input('flora');
  		$fauna = $request->input('fauna');
  		$map_long = $request->input('map_long');
  		$map_lat = $request->input('map_lat');
  		$zoom_level = $request->input('zoom_level');
  		$history_description = $request->input('history_description');

  		$title = $request->input('title');
  		$images = $request->file('images');

  		$edit_detail = FrontendProtectedWetlandDetail::find($id);
  		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/protected_wetland_images/' . $filename;

  			$edit_detail->banner_image = $banner_image;
  
		}

  		$edit_detail->type_id = $type_id;
  		$edit_detail->description = $description;
  		$edit_detail->altitude = $altitude;
  		$edit_detail->location = $location;
  		$edit_detail->access = $access;
  		$edit_detail->latitude = $latitude;
  		$edit_detail->longitude = $longitude;
  		$edit_detail->flora = $flora;
  		$edit_detail->fauna = $fauna;
  		$edit_detail->map_long = $map_long;
  		$edit_detail->map_lat = $map_lat;
  		$edit_detail->zoom_level = $zoom_level;
  		$edit_detail->history_description = $history_description;
  		$edit_detail->save();

        return redirect('/protected-wetland-list')->with('success','Protected Wetland Detail Updated Successfully');
	}
	public function addprotectedWetland(){
		$type_list = ProtectedWetland::get();
		return view('protectedWetland.add',compact('type_list'));
	}
	public function saveprotectedWetland(Request $request){
		
    	// return $request->all();
    
		$validated = $request->validate([
        'type_id' => 'required',
       	'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'description' => 'required',
        'altitude' => 'required',
        'location' => 'required',
      	'access' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'flora' => 'required',
        'fauna' => 'required',
        'map_lat' => 'required',
        'map_long' => 'required',
        'zoom_level' => 'required',
        'history_description' => 'required',
        'title' => 'required',
        'images' => 'required',
         'title_noti' => 'required',
        'pdf_file' => 'required',
      ]);

		// return $request->all();
  		$type_id = $request->type_id;
  		$banner_image = $request->file('banner_image');
  		$description = $request->input('description');
  		$altitude = $request->input('altitude');
  		$location = $request->input('location');
  		$access = $request->input('access');
  		$latitude = $request->input('latitude');
  		$longitude = $request->input('longitude');
  		$flora = $request->input('flora');
  		$fauna = $request->input('fauna');
  		$map_long = $request->input('map_long');
  		$map_lat = $request->input('map_lat');
  		$zoom_level = $request->input('zoom_level');
  		$history_description = $request->input('history_description');

  		$title = $request->input('title');
  		$images = $request->file('images');
    
    	$title_noti = $request->input('title_noti');
  		$pdf_file = $request->file('pdf_file');

  		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/protected_wetland_images/' . $filename;
  
		}

  		$save_detail = new FrontendProtectedWetlandDetail();
  		$save_detail->type_id = $type_id;
  		$save_detail->banner_image = $banner_image;
  		$save_detail->description = $description;
  		$save_detail->altitude = $altitude;
  		$save_detail->location = $location;
  		$save_detail->access = $access;
  		$save_detail->latitude = $latitude;
  		$save_detail->longitude = $longitude;
  		$save_detail->flora = $flora;
  		$save_detail->fauna = $fauna;
  		$save_detail->map_long = $map_long;
  		$save_detail->map_lat = $map_lat;
  		$save_detail->zoom_level = $zoom_level;
  		$save_detail->history_description = $history_description;
  		$save_detail->save();


		$p_w_id = $save_detail->id;

        $image_data=array();
		if($request->hasFile('images') != "")
	    {
	            foreach($request->file('images') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $images = time() . '.' . $file->getClientOriginalName();
					        $file->move(public_path() . '/protected_wetland_images/', $images);
					        $image_data[] = '/protected_wetland_images/'.$images;
	            }
		}

		for($i=0;$i<count($title);$i++)
        {
        	$save_harika_data = new ProtectedWetlandNotification();
        	$save_harika_data->p_w_id = $p_w_id;
        	$save_harika_data->title=$title[$i];
        	$save_harika_data->images=$image_data[$i];
        	
        	$save_harika_data->save();
        }
    
    	  $image_data_ = array();
		if($request->hasFile('pdf_file') != "")
	    {
	            foreach($request->file('pdf_file') as $key=>$file)
	            {
	                // $image = $file->getClientOriginalName();
	                $pdf_file = time() . '.' . $file->getClientOriginalName();
					        $file->move(public_path() . '/protected_wetland_images/', $pdf_file);
					        $image_data_[] = '/protected_wetland_images/'.$pdf_file;
	            }
		}

		for($j=0;$j<count($title_noti);$j++)
        {
        	$save_data = new FrontendProtectedWetlandNotification();
        	$save_data->front_protwet_id = $p_w_id;
        	$save_data->type_id = $type_id;
        	$save_data->title=$title_noti[$j];
        	$save_data->pdf_file=$image_data_[$j];
        	$save_data->save();
        }
    

		

        return redirect('/protected-wetland-list')->with('success','Protected Wetland Detail Added Successfully');
	}

	public function viewprotectedWetland($id){
		$view_list = FrontendProtectedWetlandDetail::with('get_type_name','get_detail')->where('id',$id)->first();
		return view('protectedWetland.view',compact('view_list','id'));
	}

	public function proWetImagesDetail($p_id){

		$images_list = ProtectedWetlandNotification::where('p_w_id',$p_id)->paginate(10);

		return view('protectedWetland.image-list',compact('images_list','p_id'));
	}

	public function editProtectedWetlandImage($id,$p_id){

		$edit_image = ProtectedWetlandNotification::where('id',$id)->first();
		return view('protectedWetland.image-edit',compact('edit_image','p_id','id'));

	}
	public function updateProtectedWetlandImage(Request $request ,$id,$p_id){
		// return $request->all();
		$images = $request->file('images');
		$title = $request->input('title');

		$edit = ProtectedWetlandNotification::find($id);

		if($request->hasFile('images') != ""){
        	$filename =$images->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $images->move($destinationPath, $filename);
            $images = '/protected_wetland_images/' . $filename;

			$edit->images = $images;
  
		}
		$edit->title = $title;
		$edit->save();

		return redirect('/pro-wet-images-detail/'.$p_id)->with('success','Historical Detail Updated Successfully');
	}

	public function addProtectedWetlandImage($p_id){

		$type_name = FrontendProtectedWetlandDetail::with('get_type_name')->where('id',$p_id)->first();
		return view('protectedWetland.image-add',compact('p_id','type_name'));
	}


	public function saveProtectedWetlandImage(Request $request , $p_id){

		$validated = $request->validate([
       	'images' => 'required|mimes:jpg,png,jpeg|max:2048',
       	'title' => 'required',
      ]);

		$images = $request->file('images');
		$title = $request->input('title');


		if($request->hasFile('images') != ""){
        	$filename =$images->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $images->move($destinationPath, $filename);
            $images = '/protected_wetland_images/' . $filename;

  
		}
		$save = new ProtectedWetlandNotification();
		$save->title = $title;
		$save->images = $images;
		$save->p_w_id = $p_id;
		$save->save();

		return redirect('/pro-wet-images-detail/'.$p_id)->with('success','Historical Detail Updated Successfully');
	}

	public function deleteProtectedWetlandImage($id,$p_id){

		ProtectedWetlandNotification::where('id',$id)->delete();

		return redirect('/pro-wet-images-detail/'.$p_id)->with('success','Historical Detail Deleted Successfully');
	}



    public function wetlandImages(){
 		$list = FrontendWetlandImages::with('wetland_name')->paginate(10);
       return view('protectedWetland.images.list',compact('list'));

    }

    public function addImages(){
        $list = ProtectedWetland::get();
        return view('protectedWetland.images.add',compact('list'));
    }
    public function saveImages(Request $request){

        $validated = $request->validate([
        'image' => 'required|max:2048',
        'wetland_id' => 'required',
        ]);
        
        // return $request->all();
        $wetland_id = $request->wetland_id;
        $image = $request->file('image');

         // $folder_path = '/images';
        //// $destinationPath = public_path('/protected_wetland_images');

       //// $image = $request->file('image');

       //// $img_name = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path($folder_path);

       //// $img = Image::make($image->getRealPath());
       //// $img->save($destinationPath.'/'.$img_name);

        //resize image
       //// $img->resize(288,192)->save($destinationPath.'/'.$img_name);
        //resize thumb image
        // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

        //// $input['img'] = $img_name;
    if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $image->move($destinationPath, $filename);
            $image = '/protected_wetland_images/' . $filename;

  
		}

        $save_image = new FrontendWetlandImages();
        // $save_image->images = '/protected_wetland_images/'.$input['img'];
        $save_image->images = $image;
        $save_image->wetland_id = $wetland_id;
        $save_image->save();
    
      	$protect_wetland = ProtectedWetland::where('id',$wetland_id)->first();
        $cat_name = $protect_wetland->name;
        $check = GalleryImages::where('type_id',$wetland_id)->first();
    	if($check){
        	GalleryImages::where('type_id',$wetland_id)->update(['image' => $save_image->images]);
        }else{
        	$save_images = new GalleryImages();
        	$save_images->type_id = $wetland_id;
        	$save_images->type_name = $cat_name;
        	$save_images->image = $image;
        	$save_images->save();
        }
    
    	 $saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_image->id;
    	$saveall_gallery_image->type_id = $wetland_id;
    	$saveall_gallery_image->image = $image;
    	$saveall_gallery_image->save();

        return redirect('/wetland-images-list')->with('success','Protected Wetland Image Uploaded Successfully');    
    }

    public function editImages($id){

        $edit = FrontendWetlandImages::where('id',$id)->first();
        $list = ProtectedWetland::get();
        return view('protectedWetland.images.edit',compact('list','edit','id'));
    }

    public function updateImages(Request $request ,$id){

        // return $request->all();
         $validated = $request->validate([
        'image' => 'max:2048',
        ]);
        
        $wetland_id = $request->wetland_id;
        $image = $request->file('image');
         $update_image = FrontendWetlandImages::find($id);
//       if($request->hasFile('image') != ""){
//          // $folder_path = '/images';
        
//         $destinationPath = public_path('/protected_wetland_images');

//         $image = $request->file('image');

//         $img_name = time().'.'.$image->getClientOriginalExtension();
//         // $destinationPath = public_path($folder_path);

//         $img = Image::make($image->getRealPath());
//         $img->save($destinationPath.'/'.$img_name);

//         //resize image
//         $img->resize(288,192)->save($destinationPath.'/'.$img_name);
//         //resize thumb image
//         // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

//         $input['img'] = $img_name;
//         $update_image->images = '/protected_wetland_images/'.$input['img'];
//     }
     if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $image->move($destinationPath, $filename);
            $image = '/protected_wetland_images/' . $filename;
			 $update_image->images = $image;
  
		}

       
        $update_image->wetland_id = $wetland_id;
        $update_image->save();
    
        $update_gallery_image = GalleryImages::where('type_id',$wetland_id)->update(['image'=> $update_image->images ]); 
    
     $saveall_gallery_image = GalleryAllImages::where('table_id',$update_image->id)->where('type_id',$wetland_id)->update(['image'=> $update_image->images ]); 
    	// $saveall_gallery_image->table_id = $save_image->id;
    	// $saveall_gallery_image->type_id = $wetland_id;
    	// $saveall_gallery_image->image = '/protected_wetland_images/'.$input['img'];
    	// $saveall_gallery_image->save();

        return redirect('/wetland-images-list')->with('success','Protected Wetland Image Updated Successfully');    
    }

    public function delete_Images($id,$wetland_id){

        FrontendWetlandImages::where('id',$id)->delete();
    
    		GalleryAllImages::where('table_id',$id)->where('type_id',$wetland_id)->delete();
    
    	$get_image = FrontendWetlandImages::where('wetland_id',$wetland_id)->take(1)->latest()->first();
    		if($get_image){
            		$check = GalleryImages::where('type_id',$wetland_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$wetland_id)->update(['image' => $get_image->images]);
        
        }
            
            }else{
            
        	 GalleryImages::where('type_id',$wetland_id)->delete();
            }
    
        return redirect('/wetland-images-list')->with('success','Protected Wetland Image Deleted Successfully');  
    }

  public function protwetlandNotiList($id,$type_id){

      $noti_list = FrontendProtectedWetlandNotification::where('front_protwet_id',$id)->paginate(5);
      return view('protectedWetland.noti-list',compact('noti_list','id','type_id'));
    }

    public function addProtWetNoti($id , $type_id){

    return view('protectedWetland.noti-add',compact('id','type_id'));      
    }

    public function saveProtWetNoti(Request $request , $id , $type_id){

      $validated = $request->validate([
      'pdf_file' => 'required|max:2048',
        'title' => 'required',
        ]);
 
      $title = $request->input('title');
      $pdf_file = $request->file('pdf_file');

      if($request->hasFile('pdf_file') != ""){
          $filename =$pdf_file->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $pdf_file->move($destinationPath, $filename);
            $pdf_file = '/protected_wetland_images/' . $filename;

    }

      $save_noti = new FrontendProtectedWetlandNotification();
      $save_noti->title = $title;
      $save_noti->type_id = $type_id;
      $save_noti->pdf_file = $pdf_file;
      $save_noti->front_protwet_id = $id;
      $save_noti->save();

      return redirect('/pro-wet-pdf-detail/'.$id.'/'.$type_id)->with('success','Notification Detail Added Successfully');
    }

    public function deleteProtWetNoti($id){
      FrontendProtectedWetlandNotification::where('id',$id)->delete();
      return redirect()->back()->with('success','Notification Detail Deleted Successfully');
    }

    public function editProtWetNoti($id,$p_id,$type_id){

      $edit_noti = FrontendProtectedWetlandNotification::where('id',$id)->first();
      return view('protectedWetland.noti-edit',compact('edit_noti','id','p_id','type_id'));    
    }

    public function updateProtWetNoti(Request $request,$id,$p_id , $type_id){

      $title = $request->input('title');
      $pdf_file = $request->file('pdf_file');

      $update_noti = FrontendProtectedWetlandNotification::find($id);
      if($request->hasFile('pdf_file') != ""){
          $filename =$pdf_file->getClientOriginalName();
            $destinationPath = public_path('/protected_wetland_images');
            $pdf_file->move($destinationPath, $filename);
            $pdf_file = '/protected_wetland_images/' . $filename;
        
        $update_noti->pdf_file = $pdf_file;

    }

      $update_noti->title = $title;
      $update_noti->save();

      return redirect('/pro-wet-pdf-detail/'.$p_id.'/'.$type_id)->with('success','Notification Detail Updated Successfully');
    }


// end class
}