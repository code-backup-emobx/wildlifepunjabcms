<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Banner;
use App\Models\LandingBannerImagesText;
use App\Models\UpcomingEventDetail;

use App\Models\EventBgImages;

use App\Models\VolunteerDetail;

use App\Models\HomePageDetail;
use App\Models\HomeGeographicZone;
use App\Models\WildlifeSymbol;
use App\Models\WildlifeSymbolTitleImage;

use App\Models\MinisterDetail;
use App\Models\ZooType;
use App\Models\ZooDetail;
use App\Models\ZooGalleryBanner;
use App\Models\ZooGallery;

use App\Models\WorldDay;
use App\Models\WorldDayDetail;
use App\Models\WorldDayDetails;
use App\Models\WorldDayImagesDetail;
use App\Models\WorldDayImagesHonour;
use App\Models\ServicesHowApply;

use App\Models\ProtectedAreaSubcategory;
use App\Models\ProtectedWetland;
use App\Models\Video;
use App\Models\GalleryImages;
use App\Models\GalleryAllImages;
use App\Models\EventMaster;
use DB;
use Carbon\Carbon;
use Image;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
		$total_zoo = ZooType::count();
		$total_protected_area = ProtectedAreaSubcategory::count();
		$total_wetland = ProtectedWetland::count();
		$total_videos = Video::count();
	  	return view('/home',compact('total_zoo','total_protected_area','total_wetland','total_videos'));
	}
	
	public function bannerList(){
    
    	$get_data = LandingBannerImagesText::latest()->first();
		
		$banner_list = Banner::where('deleted_status','=','0')->paginate(10);
		return view('index.banners_list',compact('banner_list','get_data'));
	}
	
	public function save_text_banner(Request $request){
    
    	$check_id = $request->input('check_id');
    	$title = $request->input('title');
    	$sub_title = $request->input('sub_title');
         if($check_id){
         
         	$update_text = LandingBannerImagesText::find($check_id);
         	$update_text->title = $title;
         	$update_text->subtitle = $sub_title;
         	$update_text->save();
         }else{
         		$update_text = new LandingBannerImagesText();
         	$update_text->title = $title;
         	$update_text->subtitle = $sub_title;
         	$update_text->save();
         
         }
    	return redirect('/banners_list')->with('success','Text Updated');
    }

	public function addBanner(){
		return view('index.add_banner');
	}

	public function saveBanner(Request $request){

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        // 'banner_slide' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		// $banner_heading = $request->input('banner_heading');
		// $banner_description = $request->input('banner_description');
		// $banner_slide = $request->input('banner_slide');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/banner_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/banner_image/' . $filename;
		}	
		
        $save_banner = new Banner();
		$save_banner->banner_image = $banner_image;
		// $save_banner->banner_heading = $banner_heading;
		// $save_banner->banner_slide = $banner_slide;
		// $save_banner->banner_description = $banner_description;
		$save_banner->save();

		return redirect('/banners_list')->with('sucess','File Uploaded Successfully');
	}

	public function deleteBanner($id){

		$deletebanner = Banner::find($id);
		$deletebanner->deleted_status = '1' ;
		$deletebanner->save();
		return redirect('/banners_list')->with('sucess','Delete Record Successfully');
	}
	
	public function editBanner($id){

		$edit_banner = Banner::where('id',$id)->first();
		return view('index.edit_banner',compact('edit_banner','id'));
	}  

	public function updateBanner(Request $request , $id){
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        // 'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'banner_description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
    	]);

		$banner_image = $request->file('banner_image');
		// $banner_heading = $request->input('banner_heading');
		// $banner_description = $request->input('banner_description');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/banner_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/banner_image/' . $filename;

            $update_banner = Banner::find($id);
			$update_banner->banner_image = $banner_image;
			
			$update_banner->save();
			// return $update_banner;
		}
		// else{
		// 	$update_banner = Banner::find($id);
		// 	$update_banner->banner_heading = $banner_heading;
		// 	$update_banner->banner_description = $banner_description;
		// 	$update_banner->save();
		// }	

		return redirect('/banners_list')->with('sucess','Banner Updated Successfully');
	}

	public function addBanner_two(){

		return view('index.banner_two');
	}
	public function saveBanner_two(Request $request){

		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$banner_image = $request->file('banner_image');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/banner_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/banner_image/' . $filename;

            $update_banner = new Banner();
			$update_banner->banner_image = $banner_image;
			$update_banner->banner_slide = 'slide_2';
			$update_banner->save();
		}
		return redirect('/banners_list')->with('sucess','Banner Slide Two Added Successfully');	
	}
	//second portion starts

	public function worldDayList(){
	
		$world_list = WorldDay::paginate(10);

		return view('index.worldDay.listing',compact('world_list'));
	}

	public function addworldDay(){

		$worldDay_list = WorldDay::all();
		return view('index.worldDay.add',compact('worldDay_list'));
	}

	public function saveworldDay(Request $request){

		$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'type_name' => 'required',
        // 'description' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'event_date' => 'required',
    	]);

		$image = $request->file('image');
		$type_name = $request->input('type_name');
		// $description = $request->input('description');
		$event_date = $request->input('event_date');

		$date = Carbon::createFromFormat('Y-m-d', $event_date)->day;

		$monthname = date('F', strtotime($event_date)); 

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/world_day');
            $image->move($destinationPath, $filename);
            $image = '/world_day/' . $filename;
		}

		$save_worldDay = new WorldDay();
		$save_worldDay->type_name = $type_name;
		$save_worldDay->date_of_day = $event_date;
		$save_worldDay->event_date = $date;
		$save_worldDay->month_name = $monthname;
		$save_worldDay->image = $image;
		// $save_worldDay->description = $description;
		$save_worldDay->save();
    
    	if($save_worldDay){
        	DB::table('word_day_view')->insert(array('world_day_id'=>$save_worldDay->id,'type_name'=>$save_worldDay->type_name,'ryear'=>$save_worldDay->date_of_day,'event_date'=>$save_worldDay->event_date,'month_name'=>$save_worldDay->month_name,'image'=>$save_worldDay->image,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')));
        }

		return redirect('/world_day_list')->with('success','World Day detail added Successfully');
	}

	public function editworldDay($id){

		// $worldDay_list = WorldDay::all();
		$edit_world = WorldDay::where('id',$id)->first();
		return view('index.worldDay.edit',compact('edit_world','id'));
	}

	public function updateworldDay(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$type_name = $request->input('type_name');
		$event_date = $request->input('event_date');

		$date = Carbon::createFromFormat('Y-m-d', $event_date)->day;

		$monthname = date('F', strtotime($event_date)); 

        $update_worldDay = WorldDay::find($id);

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/world_day');
            $image->move($destinationPath, $filename);
            $image = '/world_day/' . $filename;

			$update_worldDay->image = $image;
		}	
	
			$update_worldDay->type_name = $type_name;
			$update_worldDay->date_of_day = $event_date;
			$update_worldDay->event_date = $date;
			$update_worldDay->month_name = $monthname;
			$update_worldDay->save();
		
			if($update_worldDay){
            	DB::table('word_day_view')->where('world_day_id',$update_worldDay->id)->update(array('world_day_id'=>$update_worldDay->id,'type_name'=>$update_worldDay->type_name,'ryear'=>$update_worldDay->date_of_day,'event_date'=>$update_worldDay->event_date,'month_name'=>$update_worldDay->month_name,'image'=>$update_worldDay->image,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')));
            }

		return redirect('/world_day_list')->with('success','World Day detail Update Successfully');
	}

	public function deleteworldDay($id){

		$delete_worldDay = WorldDay::where('id',$id)->delete();
        if($delete_worldDay){
        	DB::table('word_day_view')->where('world_day_id',$id)->update(["deleted_at"=>date('Y-m-d H:i:s')]);
        }
		return redirect('/world_day_list')->with('success','World Day detail Deleted Successfully');
	}


	// forest World Day detail 
	public function worldDayDetail(){

	
    
    $world_day_list = WorldDayDetails::paginate(10);
		foreach($world_day_list as $count){

			$count->images_count = WorldDayImagesDetail::where('world_day_id',$count->worldday_type_id)->count();
			$count->honour_count = WorldDayImagesHonour::where('world_day_detail_id',$count->id)->count();
		}


		// return $world_day_list;
		return view('index.worldDay.worlddayslisting',compact('world_day_list'));
	}

	// 04/02/2022
	public function addWorldDayDetail(){

		$worldDay_list = WorldDay::all();
		return view('index.worldDay.add-worldday-detail',compact('worldDay_list'));
	}

	public function saveWorldDayDetail(Request $request){
		// return $request->all();
		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'world_day_type_id' => 'required',
        'description' => 'required',
        // 'image' => 'required',
        // 'guest_honour_name' => 'required',
        // 'guest_honour_title' => 'required',
        // 'honour_image' => 'required',
    	]);

    	$world_day_type_id = $request->world_day_type_id;		
    	$banner_image = $request->file('banner_image');		
    	$description = $request->description;		
    	$image = $request->file('image');
    	$guest_honour_name = $request->input('guest_honour_name');
    	$guest_honour_title =$request->input('guest_honour_title');
    	$honour_image =$request->file('honour_image');

    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/forestDay/' . $filename;
		}

		$worldday = WorldDay::where('id',$world_day_type_id)->first();

		$save_detail = new WorldDayDetails();
		$save_detail->worldday_type_id = $world_day_type_id;
		$save_detail->banner_image = $banner_image;
		$save_detail->worldday_type_name = $worldday->type_name;
		$save_detail->worldday_date = $worldday->event_date .'   '.$worldday->month_name;
		$save_detail->description = $description;
		$save_detail->save();
		$id = $save_detail->id;

		
		  $image_data=array();
		if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/forestDay/', $image);
				$image_data[] = '/forestDay/'.$image;
            }
		
			for($i=0;$i<count($image_data);$i++)
        	{
				$save_image = new WorldDayImagesDetail();
				$save_image->world_day_detail_id = $id;
				$save_image->world_day_id = $world_day_type_id;
				$save_image->images= $image_data[$i];
				$save_image->save();
	    	}
        }

		 $img_data=array();

		if($request->hasFile('honour_image') != "")
    	{
            foreach($request->file('honour_image') as $key=>$file)
            {
                // $honour_image = $file->getClientOriginalName();
                $honour_image = time() . '.' . $file->getClientOriginalName();
				        $file->move(public_path() . '/forestDay/', $honour_image);
				        $img_data[] = '/forestDay/'.$honour_image;
            }
		

	

        	for($i=0;$i<count($guest_honour_name);$i++)
        	{
        		$save_worldday_data = new  WorldDayImagesHonour();
        		$save_worldday_data->world_day_detail_id = $id;
        		$save_worldday_data->world_day_id =$world_day_type_id;
        		$save_worldday_data->guest_of_honour=$img_data[$i];
        		$save_worldday_data->guest_honour_name=$guest_honour_name[$i];
        		$save_worldday_data->guest_honour_title=$guest_honour_title[$i];
        	
        		$save_worldday_data->save();
        	}
        }

		return redirect('/worldDayDetail')->with('success','Detail Added Successfully');
	}

	public function editWorldDayDetail($id){

		$worldDay_list = WorldDay::all();
		$edit_world = WorldDayDetails::where('id',$id)->first();
		return view('index.worldDay.edit-worldday-detail',compact('worldDay_list','edit_world','id'));
	}

	public function updateWorldDayDetail(Request $request , $id){

			// return $request->all();
		$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
       
    	]);

    	$world_day_type_id = $request->world_day_type_id;		
    	$banner_image = $request->file('banner_image');		
    	$description = $request->description;		
    
		$update_detail = WorldDayDetails::find($id);
		$worldday = WorldDay::where('id',$world_day_type_id)->first();

    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/forestDay/' . $filename;

			$update_detail->banner_image = $banner_image;
		}

		
		$update_detail->worldday_type_id = $world_day_type_id;
		$update_detail->worldday_type_name = $worldday->type_name;
		$update_detail->worldday_date = $worldday->event_date .'   '.$worldday->month_name;
		$update_detail->description = $description;
		$update_detail->save();
		
		return redirect('/worldDayDetail')->with('success','Detail Updated Successfully');
	}

	public function deleteWorldDayDetail($id){

		WorldDayDetails::where('id',$id)->delete();
		WorldDayImagesDetail::where('world_day_detail_id',$id)->delete();
		WorldDayImagesHonour::where('world_day_detail_id',$id)->delete();
		return redirect('/worldDayDetail')->with('success','Detail Deleted Successfully');
	}

	public function imagesWorldDayDetail($d_id,$type_id){

		$images_list = WorldDayImagesDetail::where('world_day_id',$type_id)->paginate(10);
		return view('index.worldDay.images-list',compact('images_list','d_id','type_id'));
	}

	public function editImages($id,$d_id,$type_id){

		$edit_image = WorldDayImagesDetail::where('id',$id)->first();
		return view('index.worldDay.edit-image',compact('edit_image','d_id','type_id','id'));
	}

	public function updateImages(Request $request , $id, $d_id , $type_id){

		$images =$request->file('images');

		$update_image = WorldDayImagesDetail::find($id);
    	if($request->hasFile('images') != ""){
        	$filename =$images->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $images->move($destinationPath, $filename);
            $images = '/forestDay/' . $filename;
            $update_image->images = $images; 
		}
		$update_image->save();

		return redirect('/images-list/'.$d_id.'/'.$type_id)->with('success','Image Updated Successfully');

	}

	public function deleteImages($id,$d_id){

		WorldDayImagesDetail::where('id',$id)->delete();
		return redirect()->back()->with('success','Image Deleted Successfully');

	}

	public function addMoreImage($d_id,$type_id){

		$worlddays = WorldDay::where('id',$type_id)->first();
		return view('index.worldDay.add-more-image',compact('worlddays','d_id','type_id'));		
	}

	public function saveMoreImage(Request $request , $d_id, $type_id){

		$validated = $request->validate([
        'images' => 'required|mimes:jpg,png,jpeg|max:2048',
        'world_day_type_id' => 'required',
       
    	]);

		$images =$request->file('images');
		$world_day_type_id =$request->input('world_day_type_id');

    	if($request->hasFile('images') != ""){
        	$filename =$images->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $images->move($destinationPath, $filename);
            $images = '/forestDay/' . $filename;
		}

		$save_image = new WorldDayImagesDetail();
        $save_image->images = $images; 
        $save_image->world_day_id = $world_day_type_id; 
        $save_image->world_day_detail_id = $d_id; 
		$save_image->save();

		return redirect('/images-list/'.$d_id.'/'.$type_id)->with('success','Image Updated Successfully');

	}

	public function honourList($id,$type_id){

		$honour_list = WorldDayImagesHonour::where('world_day_detail_id',$id)->paginate(10);
		return view('index.worldDay.honour-list',compact('id','type_id','honour_list'));

	}

	public function edithonourDetail($id, $d_id,$type_id){

		$edit_honour = WorldDayImagesHonour::where('id',$id)->first();
		return view('index.worldDay.edit-honour',compact('id','d_id','type_id','edit_honour'));		
	}
	public function updatehonourDetail(Request $request ,$id, $d_id, $type_id){

		$validated = $request->validate([
        'guest_of_honour' => 'mimes:jpg,png,jpeg|max:2048',
       
    	]);

		$guest_of_honour =$request->file('guest_of_honour');
		$guest_honour_name =$request->input('guest_honour_name');
		$guest_honour_title = $request->input('guest_honour_title');

		$update_honour_detail = WorldDayImagesHonour::find($id);
    	if($request->hasFile('guest_of_honour') != ""){
        	$filename =$guest_of_honour->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $guest_of_honour->move($destinationPath, $filename);
            $guest_of_honour = '/forestDay/' . $filename;
        $update_honour_detail->guest_of_honour = $guest_of_honour; 
		}

        $update_honour_detail->guest_honour_name = $guest_honour_name; 
        $update_honour_detail->guest_honour_title = $guest_honour_title; 
		$update_honour_detail->save();

		return redirect('/honour-list/'.$d_id.'/'.$type_id)->with('success','Detai Updated Successfully');
	}
 
	public function deletehonourDetail($id,$d_id){

		WorldDayImagesHonour::where('id',$id)->delete();
		return redirect()->back()->with('success','Detail Deleted Successfully');
	}

	public function addmorehonourDetail($d_id,$type_id){

		$worlddays = WorldDay::where('id',$type_id)->first();
		return view('index.worldDay.add-more-honour',compact('d_id','type_id','worlddays'));
	}

	public function savemorehonourDetail(Request $request ,$d_id,$type_id){

		$validated = $request->validate([
        'guest_of_honour' => 'required|mimes:jpg,png,jpeg|max:2048',
        'guest_honour_name' => 'required',
        'guest_honour_title' => 'required',
       
    	]);

		$world_day_type_id = $request->world_day_type_id;
		$guest_of_honour =$request->file('guest_of_honour');
		$guest_honour_name =$request->input('guest_honour_name');
		$guest_honour_title = $request->input('guest_honour_title');

    	if($request->hasFile('guest_of_honour') != ""){
        	$filename =$guest_of_honour->getClientOriginalName();
            $destinationPath = public_path('/forestDay');
            $guest_of_honour->move($destinationPath, $filename);
            $guest_of_honour = '/forestDay/' . $filename;
		}

		$save_image = new WorldDayImagesHonour();
        $save_image->guest_of_honour = $guest_of_honour; 
        $save_image->guest_honour_name = $guest_honour_name; 
        $save_image->guest_honour_title = $guest_honour_title; 
        $save_image->world_day_detail_id = $d_id; 
        $save_image->world_day_id = $world_day_type_id; 
		$save_image->save();

		return redirect('/honour-list/'.$d_id.'/'.$type_id)->with('success','Detail Added Successfully');


	}

	// 

	// minister data
	public function ministerDetail(){
		$minister_detail = MinisterDetail::where('deleted_status','0')->paginate(10);
		return view('index.minister.list',compact('minister_detail'));
	}

	public function addministerDetail(){

		return view('index.minister.add_ministerdetail');
	}

	public function saveministerDetail(Request $request){
    	
    	$validated = $request->validate([
        'minister_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'minister_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'minister_title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
    	]);
    	
		// return $request->all();
		$minister_image = $request->file('minister_image');
		$minister_heading = $request->input('minister_heading');
		$minister_title = $request->input('minister_title');
		$description = $request->input('description');

		if($request->hasFile('minister_image') != ""){
        	$filename =$minister_image->getClientOriginalName();
            $destinationPath = public_path('/minister_image');
            $minister_image->move($destinationPath, $filename);
            $minister_image = '/minister_image/' . $filename;
		}

		$save_minister = new MinisterDetail();
		$save_minister->minister_image = $minister_image; 
		$save_minister->minister_heading = $minister_heading; 
		$save_minister->minister_title = $minister_title; 
		$save_minister->description = $description; 
		$save_minister->save(); 

		return redirect('list_minister')->with('success','Minister Detail Added Successfully');
	}

	public function editministerDetail($id){

		$edit_minister = MinisterDetail::where('id',$id)->first();
		return view('index.minister.edit',compact('edit_minister'));
	}

	public function updateministerDetail(Request $request , $id){
    
    	$validated = $request->validate([
        'minister_image' => 'mimes:jpg,png,jpeg|max:2048',
        'minister_heading' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        'minister_title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/' ,
    	]);

		// return $request->all();
		$minister_image = $request->file('minister_image');
		$minister_heading = $request->input('minister_heading');
		$minister_title = $request->input('minister_title');
		$description = $request->input('description');

            $update_minister = MinisterDetail::find($id);
		if($request->hasFile('minister_image') != ""){
        	$filename =$minister_image->getClientOriginalName();
            $destinationPath = public_path('/minister_image');
            $minister_image->move($destinationPath, $filename);
            $minister_image = '/minister_image/' . $filename;
			$update_minister->minister_image = $minister_image; 
			
		}
		

			$update_minister->minister_heading = $minister_heading; 
			$update_minister->minister_title = $minister_title; 
			$update_minister->description = $description; 
			$update_minister->save(); 
		

		

		return redirect('list_minister')->with('success','Minister Detail Updated Successfully');
	}

	public function deleteministerDetail($id){

		$delete_minister = MinisterDetail::find($id);
		$delete_minister->deleted_status = '1';
		$delete_minister->save(); 

		return redirect('list_minister')->with('success','Minister Detail Deleted Successfully');
	}

	public function zooListing(){

		$zoodetail = ZooDetail::where('deleted_status','0')->paginate(10);
		return view('index.zoo.zooDetail.list',compact('zoodetail'));
	}

	public function addzooDetail(){
        
        $zoo_type_list = ZooType::all();
		return view('index.zoo.zooDetail.add_zooDetail',compact('zoo_type_list'));
	}

	public function savezooDetail(Request $request){
    
    	$validated = $request->validate([
        'zoo_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$zoo_type_id = $request->zoo_type_id;
		$zoo_image = $request->file('zoo_image');

		//// $zoo_image = $request->file('zoo_image');

			 // $folder_path = '/images';
          ////  $destinationPath = public_path('/zoo_gallery');

         ////   $image = $request->file('zoo_image');

          ////  $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

           //// $img = Image::make($image->getRealPath());
         ////   $img->save($destinationPath.'/'.$img_name);

            //resize image
          ////  $img->resize(389,218)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

          ////  $input['img'] = $img_name;

			
		if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_gallery/' . $filename;
		
			
		}

		$zoo_type_name = ZooType::where('id',$zoo_type_id)->first();
		$name_zoo = $zoo_type_name->zoo_type_name;
		$save_zooDetail = new ZooDetail();
		$save_zooDetail->zoo_type_id = $zoo_type_id;
		$save_zooDetail->type_name = $name_zoo;
		// $save_zooDetail->zoo_image = '/zoo_gallery/'.$input['img'];
		$save_zooDetail->zoo_image = $zoo_image;
        $save_zooDetail->save();
    
      

        return redirect('/zoo_list')->with('success','Zoo Detail Added Successfully');
	}

	public function editzooDetail($id){
		 $zoo_type_list = ZooType::all();
		$edit_zoo = ZooDetail::where('id',$id)->first();
		return view('index.zoo.zooDetail.edit',compact('edit_zoo','zoo_type_list','id'));
	}

	public function updatezooDetail(Request $request,$id){
		
    	$validated = $request->validate([
        'zoo_image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);
    	
		$zoo_type_id = $request->zoo_type_id;
		$zoo_image = $request->file('zoo_image');

		$update_zooDetail = ZooDetail::find($id);
// 		if($request->hasFile('zoo_image') != ""){
//         	 // $folder_path = '/images';
//             $destinationPath = public_path('/zoo_gallery');

//             $image = $request->file('zoo_image');

//             $img_name = time().'.'.$image->getClientOriginalExtension();
//             // $destinationPath = public_path($folder_path);

//             $img = Image::make($image->getRealPath());
//             $img->save($destinationPath.'/'.$img_name);

//             //resize image
//             $img->resize(389,218)->save($destinationPath.'/'.$img_name);
//             //resize thumb image
//             // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

//             $input['img'] = $img_name;
            
// 			$update_zooDetail->zoo_image = '/zoo_gallery/'.$input['img'];
// 		}
    	
    	if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_gallery/' . $filename;
		
				$update_zooDetail->zoo_image = $zoo_image;
		}
    
		$zoo_type_name = ZooType::where('id',$zoo_type_id)->first();
		$name_zoo = $zoo_type_name->zoo_type_name;
		$update_zooDetail->zoo_type_id = $zoo_type_id;
		$update_zooDetail->type_name = $name_zoo;
        $update_zooDetail->save();

        return redirect('/zoo_list')->with('success','Zoo Detail Updated Successfully');
	}

	public function deletezooDetail($id){

		$delete_zooDetail = ZooDetail::find($id);
		$delete_zooDetail->deleted_status = '1';
        $delete_zooDetail->save();

        return redirect('/zoo_list')->with('success','Zoo Detail Deleted Successfully');
	}

	

	public function zoogallerybanner(){

		$data['zoogallerybanner_detail'] = ZooGalleryBanner::where('deleted_status','0')->get();

        // return $data['zoo_type'];

		return view('index.zoo.zooGallery.zoogallery',$data);
	}

	public function addgallerybanner(){

		
		return view('index.zoo.zooGallery.addgallerybanner');

	}

	public function savegallerybanner(Request $request){
    
    	$validated = $request->validate([
        'zoo_gallery_banner' => 'required|mimes:jpg,png,jpeg|max:2048',
        'zoo_gallery_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

		$zoo_gallery_banner = $request->file('zoo_gallery_banner');
		$zoo_gallery_heading = $request->input('zoo_gallery_heading');

		if($request->hasFile('zoo_gallery_banner') != ""){
        	$filename =$zoo_gallery_banner->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_gallery_banner->move($destinationPath, $filename);
            $zoo_gallery_banner = '/zoo_gallery/' . $filename;

		}

		$save_banner_detail = new ZooGalleryBanner();
		$save_banner_detail->zoo_gallery_banner = $zoo_gallery_banner;
		$save_banner_detail->zoo_gallery_heading = $zoo_gallery_heading;
		$save_banner_detail->save();

		return redirect('/gallery_banner')->with('success','Gallery Banner Added Successfully');	

	}

	public function editgallerybanner($id){

		$edit_gallery_banner = ZooGalleryBanner::where('id',$id)->first();
		return view('index.zoo.zooGallery.editgallerybanner',compact('edit_gallery_banner','id'));

	}

	public function updategallerybanner(Request $request,$id){
    
    	$validated = $request->validate([
        'zoo_gallery_banner' => 'mimes:jpg,png,jpeg|max:2048',
        'zoo_gallery_heading' => 'regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

        $zoo_gallery_banner = $request->file('zoo_gallery_banner');
		$zoo_gallery_heading = $request->input('zoo_gallery_heading');

		if($request->hasFile('zoo_gallery_banner') != ""){
        	$filename =$zoo_gallery_banner->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_gallery_banner->move($destinationPath, $filename);
            $zoo_gallery_banner = '/zoo_gallery/' . $filename;
            $update_banner_detail = ZooGalleryBanner::find($id);
			$update_banner_detail->zoo_gallery_banner = $zoo_gallery_banner;
			$update_banner_detail->save();
		}
		else{

			$update_banner_detail = ZooGalleryBanner::find($id);
			$update_banner_detail->zoo_gallery_heading = $zoo_gallery_heading;
			$update_banner_detail->save();
		}
	

		return redirect('/gallery_banner')->with('success','Gallery Banner Updated Successfully');	
	}

	public function deletegallerybanner($id){
		 
		$delete_banner_detail = ZooGalleryBanner::find($id);
		$delete_banner_detail->deleted_status = '1';
		$delete_banner_detail->save();

		return redirect('/gallery_banner')->with('success','Gallery Banner Deleted Successfully');
	}

	public function zoogalleryList(){

		$zoo_type = ZooType::all();
		foreach($zoo_type as $z){

			$z->total_count = ZooGallery::where('zoo_type_id', $z->id)->where('deleted_status','0')->count();
		}

		// return $zoo_type;
		return view('index.zoo.zooGallery.gallery_listing',compact('zoo_type'));
	}

	public function imagesDetail($id){

		$zoo_type = ZooType::all();
		$list = ZooGallery::where('zoo_type_id', $id)->where('deleted_status','0')->paginate(10);
		return view('index.zoo.zooGallery.gallery_imagesDetail',compact('zoo_type','list','id'));
	}

	public function deleteGalleryDetail($id,$zoo_type_id){

		$delete_gallery = ZooGallery::find($id);
		$delete_gallery->deleted_status = '1';
		$delete_gallery->save();
    
    		GalleryAllImages::where('table_id',$id)->where('type_id',$zoo_type_id)->delete();
    
    	$get_image = ZooGallery::where('zoo_type_id',$zoo_type_id)->take(1)->latest()->where('deleted_status','0')->first();
    		if($get_image){
            		$check = GalleryImages::where('type_id',$zoo_type_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$zoo_type_id)->update(['image' => $get_image->zoo_image]);
        
        }
            
            }else{
            
        	 GalleryImages::where('type_id',$zoo_type_id)->delete();
            }
    
		return redirect()->back()->with('success','Gallery Detail Delete Successfully');
	}

	public function editGalleryDetail($id,$g_id){
		$edit_gallery = ZooGallery::where('id',$id)->first();
		return view('index.zoo.zooGallery.edit_imagesDetail',compact('edit_gallery','id','g_id'));
	}

	public function updateGalleryDetail(Request $request,$id,$g_id){

		
    	$validated = $request->validate([
        'zoo_image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);
		
		// return $request->all();

		$zoo_image = $request->file('zoo_image');

		
		$update_gallery_detail = ZooGallery::find($id);
// 		if($request->hasFile('zoo_image') != ""){

// 			 // $folder_path = '/images';
//             $destinationPath = public_path('/zoo_gallery');

//             $image = $request->file('zoo_image');

//             $img_name = time().'.'.$image->getClientOriginalExtension();
//             // $destinationPath = public_path($folder_path);

//             $img = Image::make($image->getRealPath());
//             $img->save($destinationPath.'/'.$img_name);

//             //resize image
//             $img->resize(288,192)->save($destinationPath.'/'.$img_name);
//             //resize thumb image
//             // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

//             $input['img'] = $img_name;

// 			$update_gallery_detail->zoo_image = '/zoo_gallery/'.$input['img'];
//         }
			if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_gallery/' . $filename;
			$update_gallery_detail->zoo_image = $zoo_image;
		}
		
			$update_gallery_detail->save();
    		// return $update_gallery_detail;
    
    		$save_gallery_images = GalleryImages::where('type_id',$update_gallery_detail->zoo_type_id)->update(["image"=> $update_gallery_detail->zoo_image]);
    
 		$saveall_gallery_image = GalleryAllImages::where('table_id',$id)->where('type_id',$update_gallery_detail->zoo_type_id)->update(["image"=> $update_gallery_detail->zoo_image]);
    	// $saveall_gallery_image->table_id = $save_gallery_image->id;
    	// $saveall_gallery_image->type_id = $zoo_type_id;
    	// $saveall_gallery_image->image = '/zoo_gallery/'.$input['img'];
    	// $saveall_gallery_image->save();
    

		return redirect('/images_detail/'.$g_id)->with('success','Gallery Image Updated Successfully');
	}

	public function addZooGallery(){

		$zoo_list = ZooType::all();
		return view('index.zoo.zooGallery.addZooGallery',compact('zoo_list'));
	}

	public function saveZooGallery(Request $request){
		
    	$validated = $request->validate([
        'zoo_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);
		
		// return $request->all();
		$zoo_type_id = $request->zoo_type_id;
		$zoo_image = $request->file('zoo_image');

			 // $folder_path = '/images';
           //// $destinationPath = public_path('/zoo_gallery');

        ////    $image = $request->file('zoo_image');

          ////  $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

          ////  $img = Image::make($image->getRealPath());
           //// $img->save($destinationPath.'/'.$img_name);

            //resize image
          ////  $img->resize(288,192)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

          ////  $input['img'] = $img_name;
			
				if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_gallery/' . $filename;
		}
		
      
		// if($request->hasFile('zoo_image') != ""){
  //       	$filename =$zoo_image->getClientOriginalName();
  //           $destinationPath = public_path('/zoo_gallery');
  //           $zoo_image->move($destinationPath, $filename);
  //           $zoo_image = '/zoo_gallery/' . $filename;

		// }
		
		$type_name = ZooType::where('id',$zoo_type_id)->first();
         if($type_name){
         
	    $name =	$type_name->zoo_type_name;
         }
       
    
        $event_master = EventMaster::where('id',$zoo_type_id)->first();
    	if($event_master){
        
		$name = $event_master->name; 
        }
    
		$save_gallery_image = new ZooGallery();
		$save_gallery_image->zoo_image = $zoo_image;
		$save_gallery_image->zoo_type_id = $zoo_type_id;
		$save_gallery_image->zoo_type_name = $name;
		$save_gallery_image->save();
    	
    	$check = GalleryImages::where('type_id',$zoo_type_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$zoo_type_id)->update(['image' => $save_gallery_image->zoo_image]);
        
        }else{
        	$save_gallery_images = new GalleryImages();
       		$save_gallery_images->type_id = $zoo_type_id;
       		$save_gallery_images->type_name = $name;
       		$save_gallery_images->image = $zoo_image;
        	$save_gallery_images->save();
        	
        }
    
    $saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_gallery_image->id;
    	$saveall_gallery_image->type_id = $zoo_type_id;
    	$saveall_gallery_image->image = $zoo_image;
    	$saveall_gallery_image->save();

		return redirect('/gallery_list')->with('success','Gallery Banner Updated Successfully');	
	}

	public function addZooImage($type_id){

		$type_name = ZooType::where('id',$type_id)->first();
		return view('index.zoo.zooGallery.add-image',compact('type_name','type_id'));
	}

	public function saveZooImage(Request $request , $type_id){

		$validated = $request->validate([
        'zoo_image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);
		
		// return $request->all();
		$zoo_image = $request->file('zoo_image');

			 // $folder_path = '/images';
           //// $destinationPath = public_path('/zoo_gallery');

          ////  $image = $request->file('zoo_image');

           //// $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

           //// $img = Image::make($image->getRealPath());
          ////  $img->save($destinationPath.'/'.$img_name);

            //resize image
           //// $img->resize(288,192)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

          ////  $input['img'] = $img_name;
    	if($request->hasFile('zoo_image') != ""){
        	$filename =$zoo_image->getClientOriginalName();
            $destinationPath = public_path('/zoo_gallery');
            $zoo_image->move($destinationPath, $filename);
            $zoo_image = '/zoo_gallery/' . $filename;

		}
			
			
		$type_name = ZooType::where('id',$type_id)->first();
	    $name =	$type_name->zoo_type_name;

		$save_image = new ZooGallery();
		$save_image->zoo_image = $zoo_image;
		$save_image->zoo_type_id = $type_id;
		$save_image->zoo_type_name = $name;
		$save_image->save();
    
		$check = GalleryImages::where('type_id',$type_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$type_id)->update(['image' => $save_image->zoo_image]);
        
        }else{
        	$save_gallery_images = new GalleryImages();
       		$save_gallery_images->type_id = $type_id;
       		$save_gallery_images->type_name = $name;
       		$save_gallery_images->image = $zoo_image;
        	$save_gallery_images->save();
        	
        }
    	
    	$saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_image->id;
    	$saveall_gallery_image->type_id = $type_id;
    	$saveall_gallery_image->image = $zoo_image;
    	$saveall_gallery_image->save();
    
		return redirect('/images_detail/'.$type_id)->with('success','Zoo Image Added Successfully');	
	}

	// upcoming Events
	// forest day event
	public function eventListing(){

		$eventforestdaylist = UpcomingEventDetail::where('deleted_status','=','0')->get();
		return view('index.events.forestDay.listing',compact('eventforestdaylist'));
	}

	public function eventadd(){

		$worldDay_list = WorldDay::all();		
		return view('index.events.forestDay.add',compact('worldDay_list'));
	}
	public function saveevent(Request $request){
    
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$world_id = $request->world_id;


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;
		}
		$w_name = WorldDay::where('id',$world_id)->first();
		$name = $w_name->world_day_type;
		
        $save_zoodetail = new UpcomingEventDetail();
		$save_zoodetail->image = $image;
		$save_zoodetail->heading = $heading;
		$save_zoodetail->world_id = $world_id;
		$save_zoodetail->world_day_name = $name;
		$save_zoodetail->date = $date;
		$save_zoodetail->save();	

		return redirect('/event_list')->with('success','Event Detail Added Successfully');
	}

	public function editevent($id){
		$worldDay_list = WorldDay::all();
		$editforestday = UpcomingEventDetail::where('id',$id)->first();
		return view('index.events.forestDay.edit',compact('editforestday','worldDay_list'));
	}

	public function updateevent(Request $request, $id){
		
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);
    
		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$world_id = $request->world_id;


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

		}
		$w_name = WorldDay::where('id',$world_id)->first();
		$name = $w_name->world_day_type;
		$save_zoodetail = UpcomingEventDetail::find($id);
		$save_zoodetail->image = $image;
		$save_zoodetail->heading = $heading;
		$save_zoodetail->date = $date;
		$save_zoodetail->world_id = $world_id;
		$save_zoodetail->world_day_name = $name;
		$save_zoodetail->save();
		return redirect('/event_list')->with('success','Event Detail Update Successfully');
	}

	public function deleteevent($id){

		$save_zoodetail = UpcomingEventDetail::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();

		return redirect('/event_list')->with('success','Event Detail Deleted Successfully');
	}

	// WorldWildlife

	public function eventwildlifeListing(){

		$eventwildlife = EventWorldWildLife::where('deleted_status','=','0')->get();
		return view('index.events.worldWildlife.listing',compact('eventwildlife'));
	}

	public function eventwildlifeadd(){

		return view('index.events.worldWildlife.add');
	}

	public function eventwildlifesave(Request $request){
		
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);
    
		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = new EventWorldWildLife();
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wildlife')->with('success','Event World Wildlife Added Successfully');
	}

	public function eventwildlifeedit($id){

		$editworldwild = EventWorldWildLife::where('id',$id)->first();
		return view('index.events.worldWildlife.edit',compact('editworldwild'));
	}

	public function eventwildlifeupdate(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = EventWorldWildLife::find($id);
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wildlife')->with('success','Event World Wildlife Updated Successfully');
	}

	public function eventwildlifdelete($id){

		$save_zoodetail = EventWorldWildLife::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_wildlife')->with('success','Event World Wildlife Deleted Successfully');
	}

	// World Migratory Bird Day

	public function eventwetlandlisting(){

		$eventwetlandlisting = EventWorldWetland::where('deleted_status','=','0')->get();
		return view('index.events.wetland.listing',compact('eventwetlandlisting'));
	}

	public function eventwetlandadd(){

		return view('index.events.wetland.add');
	}
	public function eventwetlandsave(Request $request){
    
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = new EventWorldWetland();
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wetland')->with('success','Event World Wildlife Added Successfully');
	}

	public function eventwetlandedit($id){

		$editwetland = EventWorldWetland::where('id',$id)->first();
		return view('index.events.wetland.edit',compact('editwetland'));
	}

	public function eventwetlandupdate(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');


		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = EventWorldWetland::find($id);
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_wetland')->with('success','Event World Wildlife Updated Successfully');
	}

	public function eventwetlanddelete($id){

		$save_zoodetail = EventWorldWetland::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_wetland')->with('success','Event World Wildlife Deleted Successfully');
	}

	// migratory bird day 

	public function eventmigratorybirdListing(){

		$migratorybirdListing = EventMigratoryBirdDay::where('deleted_status','=','0')->get();
		return view('index.events.migratorybird.listing',compact('migratorybirdListing'));
	}

	public function eventmigratorybirdadd(){

		return view('index.events.migratorybird.add');
	}
	public function eventmigratorybirdsave(Request $request){
    
    	$validated = $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = new EventMigratoryBirdDay();
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_migratorybirdday')->with('success','Event World Wildlife Added Successfully');
	}

	public function eventmigratorybirdedit($id){

		$editmigratorybird = EventMigratoryBirdDay::where('id',$id)->first();
		return view('index.events.migratorybird.edit',compact('editmigratorybird'));
	}

	public function eventmigratorybirdupdate(Request $request , $id){
    
    	$validated = $request->validate([
        'image' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		$image = $request->file('image');
		$heading = $request->input('heading');
		$date = $request->input('date');
		$description = $request->input('description');

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/eventforest_image');
            $image->move($destinationPath, $filename);
            $image = '/eventforest_image/' . $filename;

            $save_zoodetail = EventMigratoryBirdDay::find($id);
			$save_zoodetail->image = $image;
			$save_zoodetail->heading = $heading;
			$save_zoodetail->date = $date;
			$save_zoodetail->description = $description;
			$save_zoodetail->save();

		}	

		return redirect('/event_migratorybirdday')->with('success','Event World Wildlife Updated Successfully');
	}

	public function eventmigratorybirddelete($id){

		$save_zoodetail = EventMigratoryBirdDay::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_migratorybirdday')->with('success','Event World Wildlife Delete Successfully');
	}

	public function eventbgimageListing(){

		$eventbg_imageList = EventBgImages::where('deleted_status','=','0')->get();
		return view('index.events.bg_images.listing',compact('eventbg_imageList'));
	}

	public function eventbgimageadd(){

		return view('index.events.bg_images.add');
	}
	public function eventbgimagesave(Request $request){
    
    	$validated = $request->validate([
        'bg_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'image_heading' => 'required',
    	]);

		$bg_image = $request->file('bg_image');
		$image_heading = $request->input('image_heading');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/event_bg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/event_bg_image/' . $filename;

		}	

        $save_zoodetail = new EventBgImages();
		$save_zoodetail->bg_image = $bg_image;
		$save_zoodetail->image_heading = $image_heading;
		$save_zoodetail->save();

		return redirect('/event_image')->with('success','Event Detail Added Successfully');
	}

	public function eventbgimageedit($id){

		 $editbg_image = EventBgImages::where('id',$id)->first();
		return view('index.events.bg_images.edit',compact('editbg_image','id'));
	}

	public function eventbgimageupdate( Request $request , $id){
    
    	$validated = $request->validate([
        'bg_imag' => 'mimes:jpg,png,jpeg|max:2048',
    	]);

		// return $request->all();
		$bg_image = $request->file('bg_image');
		$image_heading = $request->input('image_heading');

        $update_zoodetail = EventBgImages::find($id);
		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/event_bg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/event_bg_image/' . $filename;
			$update_zoodetail->bg_image = $bg_image;

		}	

		$update_zoodetail->image_heading = $image_heading;
		$update_zoodetail->save();

		return redirect('/event_image')->with('success','Event Detail Updated Successfully');
	}

	public function eventbgimagedelete($id){

		$save_zoodetail = EventBgImages::find($id);
		$save_zoodetail->deleted_status = '1';
		$save_zoodetail->save();
		return redirect('/event_image')->with('success','Event Background Image Deleted Successfully');
	}


	// volunteerdetailListing
	public function volunteerdetailListing(){

		$volunteer_detail = VolunteerDetail::where('deleted_status','=','0')->paginate(10);
		return view('index.volunteerDetail.listing',compact('volunteer_detail'));
	}

	public function volunteerdetailadd(){

		return view('index.volunteerDetail.add');
	}
	public function volunteerdetailsave(Request $request){
    
    	$validated = $request->validate([
        'volunter_image_one' => 'required|mimes:jpg,png,jpeg|max:2048',
        'volunter_image_two' => 'required|mimes:jpg,png,jpeg|max:2048',
        'volunteer_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'volunteer_title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'volunteer_description' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

		$volunteer_heading = $request->input('volunteer_heading');
		$volunteer_title = $request->input('volunteer_title');
		$volunteer_description = $request->input('volunteer_description');
		$volunter_image_one = $request->file('volunter_image_one');
		$volunter_image_two = $request->file('volunter_image_two');

		if($request->hasFile('volunter_image_one') != ""){
        	$filename =$volunter_image_one->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_one->move($destinationPath, $filename);
            $volunter_image_one = '/volunter_image/' . $filename;

		}
		if($request->hasFile('volunter_image_two') != ""){
        	$filename =$volunter_image_two->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_two->move($destinationPath, $filename);
            $volunter_image_two = '/volunter_image/' . $filename;

		}		

		$save_volunteerdetail = new VolunteerDetail();
		$save_volunteerdetail->volunteer_heading = $volunteer_heading;
		$save_volunteerdetail->volunteer_title = $volunteer_title;
		$save_volunteerdetail->volunteer_description = $volunteer_description;
		$save_volunteerdetail->volunter_image_one = $volunter_image_one;
		$save_volunteerdetail->volunter_image_two = $volunter_image_two;
		$save_volunteerdetail->save();

		return redirect('/volunteerdetail')->with('success','Volunteer Detail Added Successfully');

	}

	public function volunteerdetailedit($id){

		$editvolunteerdetail = VolunteerDetail::where('id',$id)->first();
		return view('index.volunteerDetail.edit',compact('editvolunteerdetail','id'));
	}

	public function volunteerdetailupdate(Request $request , $id){
    
    	$validated = $request->validate([
         'volunter_image_one' => 'mimes:jpg,png,jpeg|max:2048',
        'volunter_image_two' => 'mimes:jpg,png,jpeg|max:2048',
        'volunteer_heading ' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        'volunteer_title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'volunteer_description' => 'regex:/^[\.a-zA-Z,!? ]*$/',
    	]);

		$volunteer_heading = $request->input('volunteer_heading');
		$volunteer_title = $request->input('volunteer_title');
		$volunteer_description = $request->input('volunteer_description');

		$volunter_image_one = $request->file('volunter_image_one');
		$volunter_image_two = $request->file('volunter_image_two');

		if($request->hasFile('volunter_image_one') != ""){
        	$filename =$volunter_image_one->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_one->move($destinationPath, $filename);
            $volunter_image_one = '/volunter_image/' . $filename;
            $update_volunteerdetail = VolunteerDetail::find($id);
            $update_volunteerdetail->volunter_image_one = $volunter_image_one;
            $update_volunteerdetail->save();
		}
		if($request->hasFile('volunter_image_two') != ""){
        	$filename =$volunter_image_two->getClientOriginalName();
            $destinationPath = public_path('/volunter_image');
            $volunter_image_two->move($destinationPath, $filename);
            $volunter_image_two = '/volunter_image/' . $filename;
            $update_volunteerdetail = VolunteerDetail::find($id);
            $update_volunteerdetail->volunter_image_two = $volunter_image_two;
            $update_volunteerdetail->save();
		}		

		$update_volunteerdetail = VolunteerDetail::find($id);
		$update_volunteerdetail->volunteer_heading = $volunteer_heading;
		$update_volunteerdetail->volunteer_title = $volunteer_title;
		$update_volunteerdetail->volunteer_description = $volunteer_description;
		$update_volunteerdetail->save();

		return redirect('/volunteerdetail')->with('success','Volunteer Detail Update Successfully');

	}

	public function volunteerdetaildelete($id){

		$save_volunteerdetail = VolunteerDetail::find($id);
		$save_volunteerdetail->deleted_status = '1';
		$save_volunteerdetail->save();

		return redirect('/volunteerdetail')->with('success','Volunteer Detail Deleted Successfully');
	}

	// Volunteer Image 
	

	// Home Page Detail
	public function homeDetail(){

		$homedetail_list = HomePageDetail::with('homeGeographicZone')->where('deleted_status','=','0')->paginate(10);
		foreach($homedetail_list as $g_count){
			$g_count->g_count = HomeGeographicZone::where('home_id',$g_count->id)->where('deleted_status','0')->count();
		}
		// return $homedetail_list;
		return view('home.listing',compact('homedetail_list'));
	}

	public function addHomeDetail(){

		return view('home.add'); 
	}

	public function saveHomeDetail(Request $request){

		$validated = $request->validate([
        'banner_heading' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'bg_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'title' => 'required|regex:/^[\.a-zA-Z,!? ]*$/',
        'intro_description' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wild_life_title' => 'required',
        'wild_description' => 'required',
        'geographic_statement' => 'required',
        'geographic_zone' => 'required',
    	]);

		$banner_heading = $request->input('banner_heading');
		$bg_image = $request->file('bg_image');
		$title = $request->input('title');
		$intro_description = $request->input('intro_description');
		$wild_life_title = $request->input('wild_life_title'); 
		$wild_description = $request->input('wild_description');
		$geographic_statement = $request->input('geographic_statement');
		$geographic_zone = $request->input('geographic_zone');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/homebg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/homebg_image/' . $filename;

            $save_homedetail = new HomePageDetail();
		}
		$save_homedetail->banner_heading = $banner_heading;
		$save_homedetail->bg_image = $bg_image;
		$save_homedetail->title = $title;
		$save_homedetail->intro_description = $intro_description;
		$save_homedetail->wild_life_title = $wild_life_title;
		$save_homedetail->wild_description = $wild_description;
		$save_homedetail->geographic_statement = $geographic_statement;
		$save_homedetail->save();
		$home_detail_id = $save_homedetail->id;

		if(!empty($home_detail_id)){

			foreach($geographic_zone as $geographic_names){

				$save_geographic_zone = new HomeGeographicZone();
				$save_geographic_zone->home_id = $home_detail_id;
				$save_geographic_zone->geography_zone = $geographic_names;
				$save_geographic_zone->save();
			}
		}

		return redirect('/home_detail')->with('success','Home Page Detail Added Successfully');

	}

	public function editHomeDetail($id){

		$edithomedetail = HomePageDetail::with('homeGeographicZone')->where('id',$id)->first();
		return view('home.edit',compact('edithomedetail','id'));
	}

	public function updateHomeDetail(Request $request , $id){
    
    	$validated = $request->validate([
        'banner_heading' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        'bg_image' => 'mimes:jpg,png,jpeg|max:2048',
        // 'title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'intro_description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'wild_life_title' => 'regex:/^[\.a-zA-Z,!? ]*$/',
        // 'wild_description' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'geographic_statement' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
      
    	]);

		$bg_image = $request->file('bg_image');
		$banner_heading = $request->input('banner_heading');
		$title = $request->input('title');
		$intro_description = $request->input('intro_description');
		$wild_life_title = $request->input('wild_life_title');
		$wild_description = $request->input('wild_description');
		$geographic_zone = $request->input('geographic_zone');

		if($request->hasFile('bg_image') != ""){
        	$filename =$bg_image->getClientOriginalName();
            $destinationPath = public_path('/homebg_image');
            $bg_image->move($destinationPath, $filename);
            $bg_image = '/homebg_image/' . $filename;

            $update_homedetail = HomePageDetail::find($id);
            $update_homedetail->bg_image = $bg_image;
            $update_homedetail->save();
		}
	

			$update_homedetail = HomePageDetail::find($id);
			$update_homedetail->banner_heading = $banner_heading;
			$update_homedetail->title = $title;
			$update_homedetail->intro_description = $intro_description;
			$update_homedetail->wild_life_title = $wild_life_title;
			$update_homedetail->wild_description = $wild_description;
			$update_homedetail->save();

			$id = $update_homedetail->id;

				if(!empty($geographic_zone)){

					foreach($geographic_zone as $geographic_names){

						$save_geographic_zone = new HomeGeographicZone();
						$save_geographic_zone->home_id = $id;
						$save_geographic_zone->geography_zone = $geographic_names;
						$save_geographic_zone->save();
					}
				}
					
	
	

		return redirect('/home_detail')->with('success','Home Page Detail Updated Successfully');

	}
	public function deleteHomeDetail($id){

		$delete_homeDetail = HomePageDetail::find($id);
		$delete_homeDetail->deleted_status = '1';
		$delete_homeDetail->save();

		return redirect('/home_detail')->with('success','Home Page Detail Deleted Successfully'); 
	}

	// wildlife Symbols
	public function wildlifeDetailListing(){

		$wildlifesymbol_list = WildlifeSymbol::with('get_wildlife_symbol_images')->where('deleted_status','=','0')->paginate(10);
		foreach($wildlifesymbol_list as $w_count){

			$w_count->w_count = WildlifeSymbolTitleImage::where('wildlife_symbol_id',$w_count->id)->where('deleted_status','0')->count();

		}
		// return $wildlifesymbol_list;
		return view('wildLifeSymbol.listing',compact('wildlifesymbol_list'));
	}

	public function addwildlifeDetail(){

		return view('wildLifeSymbol.add');
	}

	public function savewildlifeDetail(Request $request){
    
		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
        'banner_title' => 'required|regex:/^[\.a-zA-Z,!?() ]*$/',
        'title' => 'required',
        'description' => 'required',
        'image' => 'required',
    	]);
		
    	// return $request->all();
    
		$banner_image = $request->file('banner_image');
		$banner_title = $request->input('banner_title');
		$title = $request->input('title');
		$description = $request->input('description');
		$image = $request->file('image');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/wildlifesymbol_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/wildlifesymbol_image/' . $filename;
  
		}
		$save_volunteerimage = new WildlifeSymbol();
        $save_volunteerimage->banner_image = $banner_image;
        $save_volunteerimage->banner_title = $banner_title;
		$save_volunteerimage->save();


		$wildlifeid = $save_volunteerimage->id;

          $image_data=array();
		if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/wildlifesymbol_image/', $image);
				$image_data[] = '/wildlifesymbol_image/'.$image;
            }
		}

		for($i=0;$i<count($title);$i++)
        {
        	$save_wildlife_data = new WildlifeSymbolTitleImage();
        	$save_wildlife_data->wildlife_symbol_id = $wildlifeid;
        	$save_wildlife_data->title=$title[$i];
        	$save_wildlife_data->description=$description[$i];
        	$save_wildlife_data->image=$image_data[$i];
        	
        	$save_wildlife_data->save();
        }

		// if(!empty($title)){
		// 	foreach($title as $t){
		// 			foreach($description as $d){
		// 				if(!empty($image)){
		// 					if ($request->hasfile('image')) {
		// 			            foreach ($request->file('image') as $file) {
		// 			                $image = $file->getClientOriginalName();
		// 			                $file->move(public_path() . '/wildlifesymbol_image/', $image);
					            

		// 			                $wildlife_save = new WildlifeSymbolTitleImage();
		// 			                $wildlife_save->wildlife_symbol_id = $wildlifeid;
		// 							$wildlife_save->title = $t;
		// 							$wildlife_save->description = $d;
		// 				            $wildlife_save->image = '/wildlifesymbol_image/'.$image;
		// 				            $wildlife_save->save();

		// 				            return $wildlife_save;die();
		// 			            }
  //          			 			return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Added Successfully');
  //       					}	
		// 				}
		// 			}
		// 	}
		// }
		
		 

		// 	if($file = $request->hasFile('image') != ""){
		// 		    $file = array();
  //       	$filename =$file->getClientOriginalName();
  //           $destinationPath = public_path('/wildlifesymbol_image');
  //           $image->move($destinationPath, $filename);
  //           $image = '/wildlifesymbol_image/' . $filename;

  //           foreach($image as $im){
  //           	$save_image = new WildlifeSymbol();
	 //            $save_image->image = $im;
	 //            $save_image->save();
  //           }
          
		// }
	
		

		// if($request->hasFile('image') != ""){
  //       	$filename =$image->getClientOriginalName();
  //           $destinationPath = public_path('/wildlifesymbol_image');
  //           $image->move($destinationPath, $filename);
  //           $image = '/wildlifesymbol_image/' . $filename;

  //           $save_volunteerimage = new WildlifeSymbol();
		// }		
		
	
		
		// $save_volunteerimage->title = $title;
		// $save_volunteerimage->image = $image;
		// $save_volunteerimage->save();

		return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Added Successfully');
	}

	public function editwildlifeDetail($id){

		$edit_wildlifesymbol = WildlifeSymbol::with('get_wildlife_symbol_images')->where('id',$id)->first();
		return view('wildLifeSymbol.edit',compact('edit_wildlifesymbol','id'));
	}

	public function updatewildlifeDetail(Request $request , $id){
    
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpeg,png,jpg,gif|max:2048',
        'banner_title' => 'regex:/^[\.a-zA-Z,!?() ]*$/',
    	]);

		// return $request->all();
		$banner_image = $request->file('banner_image');
		$banner_title = $request->input('banner_title');
		$title = $request->input('title');
		$image = $request->file('image');
		$description = $request->input('description');
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/wildlifesymbol_image');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/wildlifesymbol_image/' . $filename;

            $update_volunteerimage = WildlifeSymbol::find($id);
			$update_volunteerimage->banner_image = $banner_image;
			$update_volunteerimage->save();
		}
		
		else{
			$update_volunteerimage = WildlifeSymbol::find($id);
			$update_volunteerimage->banner_title = $banner_title;
			$update_volunteerimage->save();
		}
			

			// $wildlifeid = $update_volunteerimage->id;
			//  $image_data=array();

			// if($request->hasFile('image') != "")
	  //       {
	  //           foreach($request->file('image') as $key=>$file)
	  //           {
	  //               // $image = $file->getClientOriginalName();
	  //               $image = time() . '.' . $file->getClientOriginalName();
			// 		$file->move(public_path() . '/wildlifesymbol_image/', $image);
			// 		$image_data[] = '/wildlifesymbol_image/'.$image;
	  //           }
			// }

			// for($i=0;$i<count($title);$i++)
	  //       {
	  //       	$update_wildlife_data = new WildlifeSymbolTitleImage();
	  //       	$update_wildlife_data->title=$title[$i];
	  //       	$update_wildlife_data->description=$description[$i];
	  //       	$update_wildlife_data->image=$image_data[$i];
	  //       	$update_wildlife_data->save();

	  //       }
		
		

		return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Updated Successfully');
		
	}

	public function deletewildlifeDetail($id){

		$save_volunteerimage = WildlifeSymbol::find($id);
		$save_volunteerimage->deleted_status = '1';
		$save_volunteerimage->save();

		return redirect('/symbol_of_punjab')->with('success','wildLife Symbol Detail Deleted Successfully');

	}

	public function geographicListing($id){

		$listing = HomeGeographicZone::where('home_id',$id)->where('deleted_status','0')->paginate(10);
		return view('home.geo_listing',compact('listing','id'));
	}

	public function deleteGeodetail($id,$h_id){

		$delete_geo = HomeGeographicZone::find($id);
		$delete_geo->deleted_status = '1';
		$delete_geo->save();

		return redirect('geographic_listing/'.$h_id)->with('success','Geographic Detail Deleted Successfully');
	}

	public function editGeodetail($id,$h_id){

		$edit_geo = HomeGeographicZone::where('id',$id)->first();
		return view('home.edit_geo',compact('edit_geo','id','h_id'));
	}

	public function updateGeodetail(Request $request , $id){

		$geography_zone = $request->input('geography_zone');
		$h_id = $request->input('h_id');

		$update = HomeGeographicZone::find($id);
		$update->geography_zone = $geography_zone;
		$update->save();
		return redirect('geographic_listing/'.$h_id)->with('success','Geographic Detail Updated Successfully');
	}

	public function addgeo($id){

		return view('home.add_geo_detail',compact('id'));
	}

	public function savegeo(Request $request , $id){

		$geography_zone = $request->input('geography_zone');

		$add_geo = new HomeGeographicZone();
		$add_geo->geography_zone = $geography_zone;
		$add_geo->home_id = $id;
		$add_geo->save();

		return redirect('geographic_listing/'.$id)->with('success','Added Successfully');
	}

	public function listingImageWildife($id){

		$list = WildlifeSymbolTitleImage::where('wildlife_symbol_id',$id)->where('deleted_status','0')->paginate(10);
		return view('wildLifeSymbol.images_list',compact('list','id'));
	}

	public function deleteWildlifesymbol($id,$w_id){

		$delete_wildlife = WildlifeSymbolTitleImage::find($id);
		$delete_wildlife->deleted_status = '1';
		$delete_wildlife->save();

		return redirect('/listing_image_wildife/'.$w_id)->with('success','Wildlife Symbol Detail Delete Successfully');
	}


	public function edit_wildlifesymbol($id,$w_id){

		$edit_wildlife = WildlifeSymbolTitleImage::where('id',$id)->first();
		return view('wildLifeSymbol.edit_wildlife',compact('edit_wildlife','id','w_id'));
	}

	public function updateWildlifesymbol(Request $request , $id){

		// return $request->all();

		$title = $request->input('title');
		$description = $request->input('description');
		$image = $request->file('image');

		$update_wildlife = WildlifeSymbolTitleImage::find($id);

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/wildlifesymbol_image');
            $image->move($destinationPath, $filename);
            $image = '/wildlifesymbol_image/' . $filename;
			$update_wildlife->image = $image;
  
		}

		$update_wildlife->title = $title;
		$update_wildlife->description = $description;
		$update_wildlife->save();
		return redirect('/listing_image_wildife/'.$request->w_id)->with('success','Wildlife Detail Updated Successfully');
	}

	public function addMoreWildlife_detail($id){

		return view('wildLifeSymbol.add_new_detail',compact('id'));
	}
	public function saveMoreWildlife_detail(Request $request, $id){

		// return $request->all();

		$title = $request->input('title');
		$description = $request->input('description');
		$image = $request->file('image');

		$update_wildlife = new WildlifeSymbolTitleImage();

		if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/wildlifesymbol_image');
            $image->move($destinationPath, $filename);
            $image = '/wildlifesymbol_image/' . $filename;
			$update_wildlife->image = $image;
  
		}

		$update_wildlife->title = $title;
		$update_wildlife->description = $description;
		$update_wildlife->wildlife_symbol_id = $request->id;
		$update_wildlife->save();
		return redirect('/listing_image_wildife/'.$request->id)->with('success','Wildlife Detail Updated Successfully');
	}
// end class 
}