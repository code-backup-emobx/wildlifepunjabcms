<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\FooterScheme;
use App\Models\FooterSchemeName;
use App\Models\FooterAbout;
use App\Models\FooterAboutDetail;
use App\Models\FooterPolicyGuideline;

use App\Models\NotificationBannerDetail;
use App\Models\NotificationDetail;

use App\Models\TenderBannerDetail;
use App\Models\TenderTypes;
use App\Models\TenderDetails;
 
use App\Models\ContactUsBannerDetail;
use App\Models\ContactUs;
use App\Models\ContactUsDetail;

use App\Models\ImpLinkDetail;

use App\Models\ServicesDetail;

use App\Models\Services;

use App\Models\Implinklist;

class FooterController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function scheme_detailList(){

    	$footer_schemelist = FooterScheme::where('deleted_status','0')->paginate(10);
        foreach($footer_schemelist as $s_count){

            $s_count->scheme_count = FooterSchemeName::where('banner_id',$s_count->id)->count();
        }
        // return $footer_schemelist;
    	return view('footer.usefullLink.scheme.listing',compact('footer_schemelist'));
    }

    public function addschemedetail(){

    	return view('footer.usefullLink.scheme.add');
    }

    public function saveschemedetail(Request $request){
	
    	
    
        $validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'name_of_scheme' => 'required',
        ]);

    	$banner_image = $request->file('banner_image'); 
    	$banner_heading = $request->input('banner_heading');
    	$name_of_scheme = $request->input('name_of_scheme');


		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_scheme_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_scheme_images/' . $filename;
		}

    	$add_scheme = new FooterScheme();
    	$add_scheme->banner_image = $banner_image ;
    	$add_scheme->banner_heading = $banner_heading ;
    	$add_scheme->save();

    	$banner_id = $add_scheme->id;
    	foreach($name_of_scheme as $scheme_name){

    		$save_name = new FooterSchemeName();
    		$save_name->banner_id = $banner_id;
    		$save_name->name_of_scheme = $scheme_name;
    		$save_name->save();
    	}

    	return redirect('/scheme_detail')->with('success','Footer Scheme Detail Added Successfully');
    }

    public function editchemedetail($id){

    	$edit_schemelist = FooterScheme::with('get_scheme_name')->where('id',$id)->first();
    	return view('footer.usefullLink.scheme.edit',compact('edit_schemelist','id'));
    }

    public function updatechemedetail(Request $request ,$id){
    
    	 $validated = $request->validate([
         'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        ]);


    	$banner_image = $request->file('banner_image'); 
    	$banner_heading = $request->input('banner_heading');
    	$name_of_scheme = $request->input('name_of_scheme');


		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_scheme_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_scheme_images/' . $filename;
            $add_scheme = FooterScheme::find($id);
    		$add_scheme->banner_image = $banner_image;
    		$add_scheme->save();
		}

    	$add_scheme = FooterScheme::find($id);
    	$add_scheme->banner_heading = $banner_heading ;
    	$add_scheme->save();

    	// $banner_id = $add_scheme->id;
    	// if(!empty($banner_id)){

    	// 	foreach($name_of_scheme as $scheme_name){

	    // 		$save_name = FooterSchemeName::find($banner_id);
	    // 		$save_name->name_of_scheme = $scheme_name;
	    // 		$save_name->save();
    	// 	}

    	// }
    
    	return redirect('/scheme_detail')->with('success','Footer Scheme Detail Added Successfully');
    }

    public function deletechemedetail($id){

    	$delete_scheme = FooterScheme::find($id);
    	$delete_scheme->deleted_status = '1' ;
    	$delete_scheme->save();
    	return redirect('/scheme_detail')->with('success','Footer Scheme Detail Delete Successfully');

    }

    // 30/01/2022
    public function schemeList($s_id){

       $list = FooterSchemeName::where('banner_id',$s_id)->where('deleted_status','0')->paginate(10);
        return view('footer.usefullLink.scheme.schemeList',compact('list','s_id'));
    }

    public function editschemeList($id, $s_id){

         $edit = FooterSchemeName::where('banner_id',$s_id)->first();
        return view('footer.usefullLink.scheme.editschemeList',compact('edit','s_id','id')); 
    }

    public function updateschemeList(Request $request ,$id, $s_id){

        $edit_scheme = FooterSchemeName::find($id);
        $edit_scheme->name_of_scheme = $request->name_of_scheme;
        $edit_scheme->save();

        return redirect('scheme-list/'.$s_id)->with('success','Scheme name updated Successfully');
    }

    public function deleteschemeList($id){

         $delete_scheme = FooterSchemeName::find($id);
        $delete_scheme->deleted_status = '1';
        $delete_scheme->save();

        return redirect('scheme-list/'.$delete_scheme->banner_id)->with('success','Scheme name deleted Successfully');
    }

    public function addmoreschemeList($s_id){

        return view('footer.usefullLink.scheme.addmoreschemeList',compact('s_id')); 
    }

    public function savemoreschemeList(Request $request , $s_id){

        $save_scheme = new FooterSchemeName();
        $save_scheme->name_of_scheme = $request->name_of_scheme;
        $save_scheme->banner_id = $s_id;
        $save_scheme->save();
        return redirect('scheme-list/'.$save_scheme->banner_id)->with('success','Scheme name added Successfully');
    }
    // 

    public function about_usList(){

    	$about_list = FooterAbout::with('get_footer_about_detail')->where('deleted_status','0')->paginate(10);
    	return view('footer.about.listing',compact('about_list'));
    }

    public function addabout_us(){

    	return view('footer.about.add');
    }

    public function saveabout_us(Request $request){
    	
    
        $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required',
            'title' => 'required',
            'description' => 'required',
            // 'protected_area_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
            // 'protected_area_description' => 'required',
        ]);


    	$banner_image = $request->file('banner_image');
    	$banner_heading = $request->input('banner_heading');
    	$title = $request->input('title');
    	$description = $request->input('description');
    	// $protected_area_heading = $request->input('protected_area_heading');
    	// $protected_area_description = $request->input('protected_area_description');

    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_about_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_about_images/' . $filename;
		}

		$save_about_banner_detail = new FooterAbout();
		$save_about_banner_detail->banner_image = $banner_image;
		$save_about_banner_detail->banner_heading = $banner_heading;
		$save_about_banner_detail->save();

		$about_id = $save_about_banner_detail->id;

		if(!empty($about_id)){

	    		$save_name = new FooterAboutDetail();
	    		$save_name->banner_id = $about_id ;
	    		$save_name->title = $title;
	    		$save_name->description = $description;
	    		// $save_name->protected_area_heading = $protected_area_heading;
	    		// $save_name->protected_area_description = $protected_area_description;
	    		$save_name->save();
    	}

		return redirect('/about_us_detail')->with('success','Footer About Detail Added Successfully');
    }

    public function edit_about_us($id){

       $edit_about = FooterAbout::with('get_footer_about_detail')->where('id',$id)->first();
        return view('footer.about.edit',compact('edit_about','id'));
    }

    public function update_about_us(Request $request , $id){
		
    	 $validated = $request->validate([
         'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        ]);
    
        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');
        $title = $request->input('title');
        $description = $request->input('description');

            $update_about_banner_detail = FooterAbout::find($id);
        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/footer_about_images');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/footer_about_images/' . $filename;

            $update_about_banner_detail->banner_image = $banner_image;
          
        }
       

           
            $update_about_banner_detail->banner_heading = $banner_heading;
            $update_about_banner_detail->save();
      
      
        $about_id = $update_about_banner_detail->id;

        if(!empty($about_id)){

                $udate_name = FooterAboutDetail::find($id);
                $udate_name->banner_id = $about_id ;
                $udate_name->title = $title;
                $udate_name->description = $description;
                // $udate_name->protected_area_heading = $protected_area_heading;
                // $udate_name->protected_area_description = $protected_area_description;
                $udate_name->save();
        }

        return redirect('/about_us_detail')->with('success','Footer About Detail Updated Successfully');
    }

    public function delete_about_us($id){

        $delete_about_banner_detail = FooterAbout::find($id);
        $delete_about_banner_detail->deleted_status = '1';
        $delete_about_banner_detail->save();
         return redirect('/about_us_detail')->with('success','Footer About Detail Deleted Successfully');
    }
    // Policy guideline
    public function policyguidelineList(){

        $policy_list = FooterPolicyGuideline::where('deleted_status','0')->paginate(10);
        return view('footer.policy.listing',compact('policy_list'));
    }

    public function addpolicyguideline(){

        return view('footer.policy.add');        
    }

    public function savepolicyguideline(Request $request){

        $validated = $request->validate([
            'policy_guideline_pdf' => 'required|mimes:pdf'
        ]);

        $policy_guideline_pdf = $request->file('policy_guideline_pdf');

        if($request->hasFile('policy_guideline_pdf') != ""){
            $filename =$policy_guideline_pdf->getClientOriginalName();
            $destinationPath = public_path('/footer_about_policypdf');
            $policy_guideline_pdf->move($destinationPath, $filename);
            $policy_guideline_pdf = '/footer_about_policypdf/' . $filename;
        }

        $save_file = new FooterPolicyGuideline();
        $save_file->policy_guideline = $policy_guideline_pdf;
        $save_file->save();

        return redirect('/policy_guideline')->with('success','File Uploaded Successfully');

    }

    public function editpolicyguideline($id){

        $edit_policy = FooterPolicyGuideline::where('id',$id)->first();
        return view('footer.policy.edit',compact('edit_policy','id'));
    }

    public function updatepolicyguideline(Request $request , $id){
    	
    	 $validated = $request->validate([
            'policy_guideline_pdf' => 'mimes:pdf'
        ]);

          $policy_guideline_pdf = $request->file('policy_guideline_pdf');

        if($request->hasFile('policy_guideline_pdf') != ""){
            $filename =$policy_guideline_pdf->getClientOriginalName();
            $destinationPath = public_path('/footer_about_policypdf');
            $policy_guideline_pdf->move($destinationPath, $filename);
            $policy_guideline_pdf = '/footer_about_policypdf/' . $filename;
            $update_file = FooterPolicyGuideline::find($id);
            $update_file->policy_guideline = $policy_guideline_pdf;
            $update_file->save();
        }

        

        return redirect('/policy_guideline')->with('success','File Updated Successfully');        
    }

    public function deletepolicyguideline($id){

        $save_file = FooterPolicyGuideline::find($id);
        $save_file->deleted_status = '1';
        $save_file->save();

        return redirect('/policy_guideline')->with('success','File Delete Successfully'); 
    }


    public function notificationDetail(){
        $notification_detail = NotificationBannerDetail::with('get_details')->where('deleted_status','0')->paginate(10);
        foreach($notification_detail as $n){

            $n->pdf_name_count = NotificationDetail::where('banner_id',$n->id)->count();
            // $n->pdf_files_count = NotificationDetail::where('banner_id',$n->id)->count();
        }
        // return $notification_detail;
        return view('footer.notification.listing',compact('notification_detail'));
    }

    public function notificationpdf_nameDetail($id){

       $pdf_names_list = NotificationDetail::where('banner_id',$id)->where('deleted_status','0')->paginate(10);
      return view('footer.notification.pdf_detail_list',compact('pdf_names_list','id'));
    }

    public function deleteNotdetail($id){

       $delete = NotificationDetail::find($id);
       $delete->deleted_status = '1';
       $delete->save();
        return redirect()->back()->with('success','Notification detail pdf delete Successfully');

    }

    public function editNotdetail($id){

        $edit_detail = NotificationDetail::where('id',$id)->first();
        return view('footer.notification.pdf_detail_edit',compact('edit_detail','id'));
    }

    public function updateNotdetail(Request $request , $id){
        // return $request->all();
        $name = $request->input('name');
        $pdf_url = $request->file('pdf_url');

        $update_pdf = NotificationDetail::find($id);
        if($request->hasFile('pdf_url') != "")
        {
        
            $image = time() . '.' . $pdf_url->getClientOriginalName();
            $pdf_url->move(public_path() . '/notification/', $image);
            $pdf_url = '/notification/'.$image;
            
            $update_pdf->pdf_url = $pdf_url;
        }
        
            $update_pdf->name = $name;

       
         $update_pdf->save();

        return redirect('noti_pdf_name_list/'. $update_pdf->banner_id)->with('success','Detail Updated Successfully');

    }

    public function addNotification(){

        return view('footer.notification.add');
    }

    public function saveNotification(Request $request){
	   
        // return $request->all();die;

        $validated = $request->validate([
            'banner_image' => 'required|max:2048',
            'banner_heading' => 'required',
            'name' => 'required',
            'pdf_url' => 'required',
        ]);

        $banner_heading = $request->input('banner_heading');
        $banner_image = $request->file('banner_image');
        $name = $request->input('name');
        $pdf_url = $request->file('pdf_url');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/notification');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/notification/' . $filename;
        }

        $save_detail = new NotificationBannerDetail();
        $save_detail->banner_heading = $banner_heading;
        $save_detail->banner_image = $banner_image;
        $save_detail->save();
        $id = $save_detail->id;

        // $image_url = array();
        if($request->hasFile('pdf_url') != "")
        {
          
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
            $pdf_url->move(public_path() . '/notification/', $image);
            $pdf_url = '/notification/'.$image;
           
        }

        
        $save_notification_detail = new NotificationDetail();
        $save_notification_detail->banner_id = $id;
        $save_notification_detail->name=$name;
        $save_notification_detail->pdf_url=$pdf_url;
        $save_notification_detail->save();
       
        return redirect('/notification_detail')->with('success','Added Successfully');
    }

    public function editNotification($id){

        $edit_notification = NotificationBannerDetail::where('id',$id)->first();
        return view('footer.notification.edit',compact('edit_notification','id'));
    }

    public function updateNotification(Request $request ,$id){
    
    	  $validated = $request->validate([
            'banner_image' => 'required|max:2048',
          	'banner_heading' => 'required',
        ]);


        $banner_heading = $request->input('banner_heading');
        $banner_image = $request->file('banner_image');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/notification');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/notification/' . $filename;

            $update_detail = NotificationBannerDetail::find($id);
            $update_detail->banner_image = $banner_image;
            $update_detail->save();
        }
        else{
            $update_detail = NotificationBannerDetail::find($id);
            $update_detail->banner_heading = $banner_heading;
            $update_detail->save();
        }
        return redirect('/notification_detail')->with('success','Detail Updated Successfully');
    }

    public function deleteNotification($id){
         
        $delete_detail = NotificationBannerDetail::find($id);
        $delete_detail->deleted_status = '1';
        $delete_detail->save();
         return redirect('/notification_detail')->with('success','Detail Deleted Successfully');

    }

    public function addNewNotification($id){

        return view('footer.notification.add_new_notification',compact('id'));
    }

    public function saveNewNotification(Request $request , $id){

        // return $request->all();
          $validated = $request->validate([
            'name' => 'required',
            'pdf_url' => 'required',
        ]);
        $name = $request->input('name');
        $pdf_url = $request->file('pdf_url');

        
        if($request->hasFile('pdf_url') != "")
        {
            $image = time() . '.' . $pdf_url->getClientOriginalName();
            $pdf_url->move(public_path() . '/notification/', $image);
            $pdf_url = '/notification/'.$image; 
        }

        $save_pdf = new NotificationDetail();
        $save_pdf->banner_id = $id;
        $save_pdf->pdf_url = $pdf_url;
        $save_pdf->name = $name;
         $save_pdf->save();

        return redirect('noti_pdf_name_list/'.$id)->with('success','Detail Saved Successfully');
    }

    // tender detail add 
    public function tenderDetail(){

       $tender_list = TenderBannerDetail::with('get_tender_detail')->where('deleted_status','0')->paginate(10);
        foreach($tender_list as $t){

            $t->t_count = TenderDetails::where('banner_id',$t->id)->count();
        }
        // return $tender_list;
        return view('footer.tender.listing',compact('tender_list'));
    }

    public function addtenderDetail(){

        return view('footer.tender.add');
    }

    public function savetenderDetail(Request $request){
    	
    
         $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tender/' . $filename;
        }

        $save_tenderDetail = new TenderBannerDetail();
        $save_tenderDetail->banner_image = $banner_image;
        $save_tenderDetail->banner_heading = $banner_heading;
        $save_tenderDetail->save();

        return redirect('/tender_detail')->with('success','Tender Banner Detail Added Successfully');
    }

    public function addTender($id){

        $tender_types = TenderTypes::all();
        $banner_detail = TenderBannerDetail::where('id',$id)->first();
        return view('footer.tender.add_tender_detail',compact('tender_types','banner_detail','id'));
    }

    public function saveTender(Request $request ,$id){

        // return $request->all();

         $validated = $request->validate([
            'tender_type_id' => 'required',
            'text' => 'required',
            'tender_pdf' => 'required|mimes:pdf',
        ]);

        $tender_type_id = $request->input('tender_type_id');
        $text = $request->input('text');
        $tender_pdf = $request->file('tender_pdf');

        

        if($request->hasFile('tender_pdf') != ""){
            $filename =$tender_pdf->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $tender_pdf->move($destinationPath, $filename);
            $tender_pdf = '/tender/' . $filename;
        }

        $type_name = TenderTypes::where('id',$tender_type_id)->first();
        $tender_type_name =  $type_name->type_name;
        $add_tenderDetail = new TenderDetails();
        $add_tenderDetail->tender_type_id = $tender_type_id;
        $add_tenderDetail->tender_type_name =  $tender_type_name;
        $add_tenderDetail->banner_id = $id;
        $add_tenderDetail->text = $text;
        $add_tenderDetail->tender_pdf = $tender_pdf;
        $add_tenderDetail->save();

        return redirect('/tender-list/'.$add_tenderDetail->banner_id)->with('success','Tender Detail Added Successfully');
    }

    public function editTender($id){

        $edit_tender = TenderBannerDetail::where('id',$id)->first();

        return view('footer.tender.edit',compact('edit_tender','id'));
    }

    public function updateTender(Request $request , $id){
	
		$validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|size:2048',
        	
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

            $update_tenderDetail = TenderBannerDetail::find($id);
        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tender/' . $filename;
            $update_tenderDetail->banner_image = $banner_image;
            
        }
        

            $update_tenderDetail->banner_heading = $banner_heading;
            $update_tenderDetail->save();
      
    

        return redirect('/tender_detail')->with('success','Tender Banner Detail Updated Successfully');
    }

    public function deleteTender($id){

        $delete_tenderDetail = TenderBannerDetail::find($id);
        $delete_tenderDetail->deleted_status = '1';
        $delete_tenderDetail->save();
        return redirect('/tender_detail')->with('success','Tender Banner Detail Deleted Successfully');
    }
// 
    public function tenderList($t_id){

       $list =   TenderDetails::where('banner_id',$t_id)->where('deleted_status','0')->paginate(10);

       return view('footer.tender.tender_detail',compact('list','t_id'));
    }

    public function editTenderDetail($t_id,$tb_id){

        $tender_types = TenderTypes::get();
       $edit_tender = TenderDetails::where('id',$t_id)->first();
       return view('footer.tender.edittender_detail',compact('t_id','tb_id','tender_types','edit_tender'));
    }

    public function updateTenderDetail(Request $request , $t_id , $tb_id){

        // return $request->all();

         $validated = $request->validate([
            'tender_pdf' => 'mimes:pdf',
        ]);

        $tender_type_id = $request->input('tender_type_id');
        $text = $request->input('text');
        $tender_pdf = $request->file('tender_pdf');

        
        $update_tenderDetail = TenderDetails::find($t_id);

        if($request->hasFile('tender_pdf') != ""){
            $filename =$tender_pdf->getClientOriginalName();
            $destinationPath = public_path('/tender');
            $tender_pdf->move($destinationPath, $filename);
            $tender_pdf = '/tender/' . $filename;
            $update_tenderDetail->tender_pdf = $tender_pdf;
        }

        $type_name = TenderTypes::where('id',$tender_type_id)->first();

        $tender_type_name =  $type_name->type_name;
        $update_tenderDetail->tender_type_id = $tender_type_id;
        $update_tenderDetail->tender_type_name =  $tender_type_name;
        $update_tenderDetail->text = $text;
        $update_tenderDetail->save();

        return redirect('/tender-list/'.$update_tenderDetail->banner_id)->with('success','Tender Detail Updated Successfully');
    }

    public function deleteTenderDetail($t_id){

        $delete_tenderDetail = TenderDetails::find($t_id);
        $delete_tenderDetail->deleted_status ='1';
        $delete_tenderDetail->save();
        return redirect('/tender-list/'.$delete_tenderDetail->banner_id)->with('success','Tender Detail Deleted Successfully');
    }
// 

    public function contactUslist(){

        $contact_us_banner_detail =  ContactUsBannerDetail::all();
        return view('footer.contactUs.list',compact('contact_us_banner_detail'));
    }

    public function addcontactBanner(){

        return view('footer.contactUs.add_banner');
    }

    public function savecontactBanner(Request $request){
    
    	  $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|size:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');


        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/contactUs');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/contactUs/' . $filename;
        }

        $save_banner_detail = new ContactUsBannerDetail();
        $save_banner_detail->banner_image = $banner_image;
        $save_banner_detail->banner_heading = $banner_heading;
        $save_banner_detail->save();

        return redirect('/contact_us')->with('success','Contact Us Banner Detail Successfully');
    }

    public function addContactDetail(){

        $contactus_detail = ContactUs::all();
        return view('footer.contactUs.add_contactdetail',compact('contactus_detail'));
    }
    public function saveContactDetail(Request $request){
    
    	
    	  $validated = $request->validate([
            'image_icon' => 'required|mimes:jpg,png,jpeg|max:2048',
            'content_type_id' => 'required',
            'description' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $content_type_id = $request->content_type_id;
        $image_icon = $request->image_icon;
        $description = $request->description;

        if($request->hasFile('image_icon') != ""){
            $filename =$image_icon->getClientOriginalName();
            $destinationPath = public_path('/contactUs');
            $image_icon->move($destinationPath, $filename);
            $image_icon = '/contactUs/' . $filename;
        }
         $name_content = ContactUs::where('id',$content_type_id)->first();
          $name = $name_content->category_name;
        $save_new_detail = new ContactUsDetail();
        $save_new_detail->content_type_id = $content_type_id;
        $save_new_detail->content_type_name = $name;
        $save_new_detail->image_icon = $image_icon;
        $save_new_detail->description = $description;
        $save_new_detail->save();
			
    	
    
        return redirect('/contact_us')->with('success','Contact Us Detail Successfully');
    }




    public function impList(){
        $impList =  ImpLinkDetail::where('deleted_status','0')->paginate(10);
        return view('impLink.list',compact('impList'));
    }

    public function addImpLink(){

        return view('impLink.add');
    }

    public function saveImpLink(Request $request){

        $validated = $request->validate([
            'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
            'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);

        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/impLink');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/impLink/' . $filename;
        }

        $saveimpLink = new ImpLinkDetail();
        $saveimpLink->banner_image = $banner_image; 
        $saveimpLink->banner_heading = $banner_heading; 
        $saveimpLink->save(); 

        return redirect('/imp_list')->with('success','Implink Detail Added Successfully');
    }

    public function editImpLink($id){

        $edit_imp = ImpLinkDetail::where('id',$id)->first();
        return view('impLink.edit',compact('edit_imp','id'));
    }

    public function updateImpLink(Request $request ,$id){
		
    	$validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        	'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
        ]);
    
        $banner_image = $request->file('banner_image');
        $banner_heading = $request->input('banner_heading');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/impLink');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/impLink/' . $filename;
            $saveimpLink = ImpLinkDetail::find($id);
            $saveimpLink->banner_image = $banner_image; 
            $saveimpLink->save(); 

        }
        else{

            $saveimpLink = ImpLinkDetail::find($id);
            $saveimpLink->banner_heading = $banner_heading; 
            $saveimpLink->save(); 
        }
       

        return redirect('/imp_list')->with('success','Implink Detail Updated Successfully');
    }

    public function deleteImpLink($id){

        $saveimpLink = ImpLinkDetail::find($id);
        $saveimpLink->deleted_status = '1'; 
        $saveimpLink->save(); 

        return redirect('/imp_list')->with('success','Implink Detail Deleted Successfully');
    }

	public function viewImpLink($id){
    		
    	$implink_list = Implinklist::where('banner_id',$id)->paginate(10);
    	return view('impLink.impllink_list',compact('implink_list','id'));
    }
	
	public function create_ImpLink($id){
    
    	return view('impLink.create_impllink',compact('id'));
    }

	public function save_ImpLink(Request $request , $id){
    
    	  $validated = $request->validate([
            'links' => 'required',
            'title' => 'required',
            'logo' => 'required|max:2048',
        ]);
    
    	$links = $request->input('links');
    	$title = $request->input('title');
    	$logo = $request->file('logo');
    
       if($request->hasFile('logo') != ""){
            $filename =$logo->getClientOriginalName();
            $destinationPath = public_path('/impLink');
            $logo->move($destinationPath, $filename);
            $logo = '/impLink/' . $filename;
        }
    
    	$save_links = new Implinklist();
    	$save_links->links = $links;
    	$save_links->title = $title;
    	$save_links->logo = $logo;
    	$save_links->banner_id = $id;
    	$save_links->save();
    	return redirect('/view_implink_list/'.$id)->with('success','Important link URL Added Successfully');
    
    }
	
	public function update_link($id,$banner_id){
       	$edit_link = Implinklist::where('id',$id)->where('banner_id',$banner_id)->first();
    	return view('impLink.edit_link',compact('edit_link','id','banner_id'));
    }

	public function update_imp_link(Request $request , $id, $banner_id){
    		
       $validated = $request->validate([
            'links' => 'required',
            'title' => 'required',
            'logo' => 'required',
        ]);
    
    	$links = $request->input('links');
    	$title = $request->input('title');
    	$logo = $request->file('logo');
    
    	$save_links = Implinklist::where('id',$id)->where('banner_id',$banner_id)->first();
    
    	  if($request->hasFile('logo') != ""){
            $filename =$logo->getClientOriginalName();
            $destinationPath = public_path('/impLink');
            $logo->move($destinationPath, $filename);
            $logo = '/impLink/' . $filename;
          
          	$save_links->logo = $logo;
        }
    
    	$save_links->links = $links;
    	$save_links->title = $title;
    	$save_links->save();
    	return redirect('/view_implink_list/'.$banner_id)->with('success','Important link URL Updated Successfully');
    }

	public function delete_imp_link($id , $banner_id){
    
    Implinklist::where('id',$id)->where('banner_id',$banner_id)->delete();
    return redirect('/view_implink_list/'.$banner_id)->with('success','Important link URL Deleted Successfully');
    }

  //   public function servicesList(){

  //       $services_list  = ServicesDetail::where('deleted_status','0')->paginate(10);
  //       return view('services.list',compact('services_list'));        
  //   }

  //   public function addservices(){

  //       return view('services.add');
  //   }

  //   public function saveservices(Request $request){

  //       $validated = $request->validate([
  //           'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
  //           'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
  //           'title' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
  //           'description' => 'required|regex:/^[\.a-zA-Z0-9,!?() ]*$/',
  //       ]);


  //       $banner_image = $request->file('banner_image');
  //       $banner_heading = $request->input('banner_heading');
  //       $title = $request->input('title');
  //       $description = $request->input('description');

  //       if($request->hasFile('banner_image') != ""){
  //           $filename =$banner_image->getClientOriginalName();
  //           $destinationPath = public_path('/services');
  //           $banner_image->move($destinationPath, $filename);
  //           $banner_image = '/services/' . $filename;
  //       }

  //       $save_services = new ServicesDetail();
  //       $save_services->banner_image = $banner_image;
  //       $save_services->banner_heading = $banner_heading;
  //       $save_services->title = $title;
  //       $save_services->description = $description;
  //       $save_services->save();

  //       return redirect('/services_list')->with('success','Services Added Successfully');
  //   }

  //   public function editservices($id){

  //       $edit_services = ServicesDetail::where('id',$id)->first();

  //       return view('services.edit',compact('edit_services'));
  //   }

  //   public function updateservices(Request $request, $id){

		// $validated = $request->validate([
  //           'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
  //           'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
  //           'title' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
  //           // 'description' => 'regex:/^[\.a-zA-Z0-9,!?() ]*$/',
  //       ]);
  //       $banner_image = $request->file('banner_image');
  //       $banner_heading = $request->input('banner_heading');
  //       $title = $request->input('title');
  //       $description = $request->input('description');

  //       if($request->hasFile('banner_image') != ""){
  //           $filename =$banner_image->getClientOriginalName();
  //           $destinationPath = public_path('/services');
  //           $banner_image->move($destinationPath, $filename);
  //           $banner_image = '/services/' . $filename;
            
  //           $update_services = ServicesDetail::find($id);
  //           $update_services->banner_image = $banner_image;
  //           $update_services->save();
  //       }
  //       else{

  //           $update_services = ServicesDetail::find($id);
  //           $update_services->banner_heading = $banner_heading;
  //           $update_services->title = $title;
  //           $update_services->description = $description;
  //           $update_services->save();

  //       }
      

  //       return redirect('/services_list')->with('success','Services Updated Successfully');
  //   }

  //   public function deleteservices($id){

  //       $delete_services = ServicesDetail::find($id);
  //       $delete_services->deleted_status = '1';
  //       $delete_services->save();

  //       return redirect('/services_list')->with('success','Services Deleted Successfully');
  //   }

    public function servicesList(){

        $list = Services::paginate();
        return view('services.master.list',compact('list'));
    }

    public function addServices(){

        return view('services.master.add');

    }
    public function saveServices(Request $request){

        $validated = $request->validate([
            'status' => 'required',
            'name' => 'required',
        ]);

        $status = $request->status;
        $name = $request->input('name');
        $website_link = $request->input('website_link');

        $save = new Services();
        $save->status = $status;
        $save->name = $name;
        $save->website_link = $website_link;
        $save->save();

        return redirect('/services-master-list')->with('success','Services Name Added Successfully');
    }

    public function editServices($id){

        $edit = Services::where('id',$id)->first();
        return view('services.master.edit',compact('edit','id'));
    }
    public function updateServices(Request $request ,$id){

        $status = $request->status;
        $name = $request->input('name');
        $website_link = $request->input('website_link');


        $update = Services::find($id);
        $update->status = $status;
        $update->name = $name;
        $update->website_link = $website_link;
        $update->save();

        return redirect('/services-master-list')->with('success','Services Name Update Successfully');
    }
    public function deleteServices($id){

        Services::where('id',$id)->delete();
        return redirect('/services-master-list')->with('success','Services Name Delete Successfully');
    }
// end class 
}