<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\ZooType;
use App\Models\FrontendZooDetail;
use App\Models\ZooInventory;
use Validator;
use Session;


class ZooController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function zooMasterList(){

    	$zoolist = ZooType::paginate(10);
    	return view('zoo.master.list',compact('zoolist'));
    }

    public function deletezooMaster($id){

		ZooType::where('id',$id)->delete();
		return redirect('/zoo-master-list')->with('success','Zoo Master Deleted Successfully');    	
    }

    public function editzooMaster($id){
    	$edit_zoo = ZooType::where('id',$id)->first();
    	return view('zoo.master.edit',compact('edit_zoo'));
    }
    public function updatezooMaster(Request $request , $id){

    	
    	$zoo_type_name = $request->input('zoo_type_name');

    	$update_new_zoo = ZooType::find($id);
    	$update_new_zoo->zoo_type_name = $zoo_type_name;
    	$update_new_zoo->save();

    	return redirect('/zoo-master-list')->with('success','Zoo Master Updated Successfully');

    }
    public function addzooMaster(){

    	return view('zoo.master.add');

    }
    public function savezooMaster(Request $request){

    		$rules = [
        'zoo_type_name' => ['required', 'regex:/^[\.a-zA-Z,!? ]*$/'],
    ];

    $customMessages = [
        'regex' => 'Please enter the letters.'
    ];

    $this->validate($request, $rules, $customMessages);

    	$zoo_type_name = $request->input('zoo_type_name');

    	$save_new_zoo = new ZooType();
    	$save_new_zoo->zoo_type_name = $zoo_type_name;
    	$save_new_zoo->save();

    	return redirect('/zoo-master-list')->with('success','Zoo Master Updated Successfully');
    }

    public function zoodetaillist(){
    	
    	$zoo_detail = FrontendZooDetail::with('get_zoo_name')->paginate(10);
        foreach($zoo_detail as $list){

           $list->inv_count = ZooInventory::where('zoo_type_id',$list->type_id)->count();
        }
    	return view('zoo.list',compact('zoo_detail'));
    }
    public function deletezoodetail($id){

    	FrontendZooDetail::where('id',$id)->delete();
    	return redirect('/zoo-list')->with('success','Zoo Detail Updated Successfully');
    }
    public function editzoodetail($id){

		$zoo_list = ZooType::get();
		$edit_zoo =  FrontendZooDetail::where('id',$id)->first();
    	return view('zoo.edit',compact('zoo_list','edit_zoo','id'));    	
    }
    public function updatezoodetail(Request $request,$id){
    	// return $request->all();

 		$banner_image = $request->file('banner_image');
 		$description = $request->input('description');
 		$location = $request->input('location');
 		$access = $request->input('access');
 		$visiting_days = $request->input('visiting_days');
 		$zoo_hours = $request->input('zoo_hours');
 		$zoo_holidays = $request->input('zoo_holidays');
 		$special_attraction = $request->input('special_attraction');
 		$zoo_residence = $request->input('zoo_residence');
 		$latitude = $request->input('latitude');
 		$longitude = $request->input('longitude');
 		$zoo_type = $request->input('zoo_type');
 		$website_link = $request->input('website_link');

 		$update_detail =FrontendZooDetail::find($id);
 		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/frontend_zoo');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/frontend_zoo/' . $filename;
 			$update_detail->banner_image = $banner_image;
		}

 		$update_detail->type_id = $banner_image;
 		$update_detail->description = $description;
 		$update_detail->location = $location;
 		$update_detail->access = $access;
 		$update_detail->visiting_days =$visiting_days;
 		$update_detail->zoo_hours =$zoo_hours;
 		$update_detail->zoo_holidays =$zoo_holidays;
 		$update_detail->special_attractions = $special_attraction;
 		$update_detail->zoo_residence =$zoo_residence;
 		$update_detail->latitude =$latitude;
 		$update_detail->longitude =$longitude;
 		$update_detail->type_id =$zoo_type;
 		$update_detail->website_link =$website_link;
 		$update_detail->save();

 		return redirect('/zoo-list')->with('success','Zoo Detail Updated Successfully');

    }
    public function addzoodetail(){
    	$zoo_list = ZooType::get();
    	return view('zoo.add',compact('zoo_list'));
    }
    public function savezoodetail(Request $request){

    	// return $request->all();
		$validated = $request->validate([
	        'zoo_type' =>'required',
	        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
	        'description' => 'required',
	        'location' => 'required',
	        'access' => 'required',
	        'visiting_days' => 'required',
	        'zoo_hours' => 'required',
	        'zoo_holidays' => 'required',
	        'special_attraction' => 'required',
	        'zoo_residence' => 'required',
	        'latitude' => 'required|numeric',
	        'longitude' => 'required|numeric',
	        'zoo_type' => 'required',
            'website_link' => 'required',
            'animal_name' => 'required',
	        'quantity' => 'required',
    	]);

 		$banner_image = $request->file('banner_image');
 		$description = $request->input('description');
 		$location = $request->input('location');
 		$access = $request->input('access');
 		$visiting_days = $request->input('visiting_days');
 		$zoo_hours = $request->input('zoo_hours');
 		$zoo_holidays = $request->input('zoo_holidays');
 		$special_attraction = $request->input('special_attraction');
 		$zoo_residence = $request->input('zoo_residence');
 		$latitude = $request->input('latitude');
 		$longitude = $request->input('longitude');
 		$zoo_type = $request->input('zoo_type');
 		$website_link = $request->input('website_link');

        $animal_name = $request->input('animal_name');
        $quantity = $request->input('quantity');

 		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/frontend_zoo');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/frontend_zoo/' . $filename;
		}

 		$save_detail = new FrontendZooDetail();
 		$save_detail->banner_image = $banner_image;
 		$save_detail->type_id = $banner_image;
 		$save_detail->description = $description;
 		$save_detail->location = $location;
 		$save_detail->access = $access;
 		$save_detail->visiting_days =$visiting_days;
 		$save_detail->zoo_hours =$zoo_hours;
 		$save_detail->zoo_holidays =$zoo_holidays;
 		$save_detail->special_attractions =$special_attraction;
 		$save_detail->zoo_residence =$zoo_residence;
 		$save_detail->latitude =$latitude;
 		$save_detail->longitude =$longitude;
 		$save_detail->type_id =$zoo_type;
 		$save_detail->website_link =$website_link;
 		$save_detail->save();

        for($i=0;$i<count($animal_name);$i++){

            $save_inventory = new ZooInventory();
            $save_inventory->zoo_type_id = $zoo_type;
            $save_inventory->animal_name = $animal_name[$i];
            $save_inventory->quantity = $quantity[$i];
            $save_inventory->save();

        }

 		return redirect('/zoo-list')->with('success','Zoo Detail Added Successfully');
    }

    public function viewzoodetail($id){

    	$view = FrontendZooDetail::with('get_zoo_name')->where('id',$id)->first();
    	return view('zoo.view',compact('view','id'));
    }

    public function inventoryList($type_id){

        $list = ZooInventory::where('zoo_type_id',$type_id)->paginate(10);
        return view('zoo.inventory.list',compact('list','type_id'));
    }
    public function addInventory($type_id){

        $zoo_type = ZooType::where('id',$type_id)->first();
        return view('zoo.inventory.add',compact('type_id','zoo_type'));

    }
    public function saveInventory(Request $request, $type_id){

        $validated = $request->validate([
            'animal_name' =>'required',
            'quantity' => 'required',
        ]);

        $animal_name = $request->input('animal_name');
        $quantity = $request->input('quantity');

        $save = new ZooInventory();
        $save->animal_name =$animal_name;
        $save->quantity =$quantity;
        $save->zoo_type_id =$type_id;
        $save->save();

        return redirect('inventory-list/'.$type_id)->with('success','Zoo inventory added successfully');

    }
    public function editInventory($id,$type_id){

       $edit = ZooInventory::where('id',$id)->first(); 
        return view('zoo.inventory.edit',compact('edit','type_id','id'));
              
    }
    public function updateInventory(Request $request , $id , $type_id){

         $animal_name = $request->input('animal_name');
        $quantity = $request->input('quantity');

        $save = ZooInventory::find($id);
        $save->animal_name =$animal_name;
        $save->quantity =$quantity;
        $save->zoo_type_id =$type_id;
        $save->save();

        return redirect('inventory-list/'.$type_id)->with('success','Zoo inventory updated successfully');

    }
    public function deleteInventory($id){

        ZooInventory::where('id',$id)->delete();
        return redirect()->back()->with('success','Zoo inventory deleted uccessfully');
    }
 
// end class 
}