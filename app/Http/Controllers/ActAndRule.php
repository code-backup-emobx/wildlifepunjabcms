<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\ActRuleBanner;
use App\Models\ActRulePdfDetail;
use App\Models\ActRulePdfTwo;
use App\Models\ActRulePdfThree;
use App\Models\ActRuleHeadings;
use App\Models\ActRuleManagementHeading;
use App\Models\ActRuleSubheading;
use Validator;
use Session;

class ActAndRule extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function actRuleList(){
			
		$act_rule_list = ActRuleBanner::where('deleted_status','0')->paginate(10);
		return view('actsRule.listing',compact('act_rule_list'));
	}

	public function addactRule(){

		return view('actsRule.add');
	}
	
	public function saveactRule(Request $request){
	
		// return $request->all;
		 $validated = $request->validate([
            // 'name' => 'required',
            // 'pdf_url' => 'required|mimes:pdf',
            'banner_image' => 'required|mimes:jpg,png,jpeg',
            'banner_heading' => 'required',
            // 'wildlife_heading' => 'required',
            // 'wildlife_title' => 'required',
            // 'wildlife_description' => 'required',
            // 'management_heading' => 'required',
            // 'management_plan_text' => 'required',
        ]);

		

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/act_rulepdf/' . $filename;
		}

		$add_act_banner_detail = new ActRuleBanner();
		$add_act_banner_detail->banner_image = $banner_image ;
		$add_act_banner_detail->banner_heading = $banner_heading ;
		$add_act_banner_detail->save();

        


  
        return redirect('/act_and_rule')->with('success','Detail Added successfully');
		
	}

	public function editactRule($id){

		$editact_rule = ActRuleBanner::where('id',$id)->first();
		return view('actsRule.edit',compact('editact_rule','id'));
	}

	public function updateactRule(Request $request , $id){
    
    	 $validated = $request->validate([
            'banner_image' => 'mimes:jpg,png,jpeg|size:2048',
        ]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/act_rulepdf/' . $filename;

            $update_act_banner_detail = ActRuleBanner::find($id);
			$update_act_banner_detail->banner_image = $banner_image;
			$update_act_banner_detail->save();
		}
		else{
			$update_act_banner_detail = ActRuleBanner::find($id);
			$update_act_banner_detail->banner_heading = $banner_heading ;
			$update_act_banner_detail->save();
		}
		
		 return redirect('/act_and_rule')->with('success','Act Rule Updated successfully');
	}

	public function deleteactRule($id){

		$delete_act_banner_detail = ActRuleBanner::find($id);
		$delete_act_banner_detail->deleted_status = '1';
		$delete_act_banner_detail->save();

		return redirect('/act_and_rule')->with('success','Act Rule Deleted successfully');
	}

	public function view_management($id){

	$list = ActRuleManagementHeading::where('act_rule_id',$id)->get();

		 foreach($list as $l){
        $l->total_subheading = ActRuleSubheading::where('management_heading_id',$l->id)->count();

            $l->total_pdf = ActRuleSubheading::where('management_heading_id', $l->id)->count();
        }
        // return $list;

		return view('actsRule.view_management',compact('id','list'));
	}

	public function addNewManagement($id){

		return view('actsRule.addnewmanagement',compact('id'));
	}

	public function saveNewManagement(Request $request , $id){
		// return $request->all();
		$validated = $request->validate([
            'pdf_file' => 'required|max:500000',
            'management_plan_heading' => 'required',
            'subheading' => 'required',
        ]);

		$management_plan_heading = $request->input('management_plan_heading');
		$subheading = $request->input('subheading');
		$pdf_file = $request->file('pdf_file');

		$save_manag_heading = new ActRuleManagementHeading();
		$save_manag_heading->act_rule_id = $id;
		$save_manag_heading->managemt_heading = $management_plan_heading;
		$save_manag_heading->save();

		$management_id = $save_manag_heading->id;

        $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/act_rulepdf/', $image);
				$image_data[] = '/act_rulepdf/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new ActRuleSubheading();
			$save_subheading->management_heading_id = $management_id;
			$save_subheading->subheading = $subheading[$i] ;
			$save_subheading->pdf = $image_data[$i] ;
			$save_subheading->save();
	    }
		return redirect('view_management/'.$id)->with('success','Management Added Successfully');

	}

	public function deleteManagement($id){

		$delete_heading = ActRuleManagementHeading::where('id',$id)->delete();

		$delete_subheading = ActRuleSubheading::where('management_heading_id',$id)->delete();

		return redirect()->back()->with('success','Management Deleted Successfully ');

	}

	public function editManagement($id){

	  $edit_management = ActRuleManagementHeading::with('getsubheading')->where('id',$id)->first();
	  $id = $edit_management->act_rule_id;
	  return view('actsRule.edit_management',compact('edit_management','id'));

	}

	public function updateManagement(Request $request , $id){

		// return $request->all();
		// $validated = $request->validate([
  //           'pdf_file' => 'size:2048',
  //       ]);

		$management_plan_heading = $request->input('management_plan_heading');
		$subheading = $request->input('subheading');
		$pdf_file = $request->file('pdf_file');

		$update_manag_heading = ActRuleManagementHeading::find($id);
		$update_manag_heading->managemt_heading = $management_plan_heading;
		$update_manag_heading->save();

		
	    return redirect('view_management/'.$update_manag_heading->act_rule_id)->with('success','Management Updated Successfully');
	}

	public function listHeading($id,$m_id){
    
		$listheading = ActRuleSubheading::where('management_heading_id',$id)->paginate(10);

		return view('actsRule.headinglist',compact('listheading','id','m_id'));
	}

	public function editSubheading($id,$h_id,$p_id){

		$updateSubheading = ActRuleSubheading::where('id',$id)->first();
		return view('actsRule.editheading',compact('updateSubheading','h_id','id','p_id'));
	}

	public function updateSubheading(Request $request, $id,$h_id,$p_id){

		$validated = $request->validate([
            'pdf_file' => 'size:2048',
        ]);

		$pdf = $request->file('pdf');
		$subheading = $request->input('subheading');
            $update = ActRuleSubheading::find($id);
		if($request->hasFile('pdf') != ""){
        	$filename = time() . '.' .$pdf->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf->move($destinationPath, $filename);
            $pdf = '/act_rulepdf/' . $filename;

			$update->pdf = $pdf;
			
		}

		$update->subheading  = $request->subheading;
		$update->save();

		
	    return redirect('list_mana_heading/'.$h_id.'/'.$p_id)->with('success','Update Management Subheading Successfully');
	}

	public function listpdf($id){


		$listpdf = ActRuleSubheading::where('management_heading_id',$id)->paginate(10);
		return view('actsRule.pdflist',compact('listpdf'));

	}

	public function editSubheadingpdf($id){

		$edit_management_pdf = ActRuleSubheading::where('id',$id)->first();
		$id_m = $edit_management_pdf->management_heading_id;
		return view('actsRule.edit_management_pdf',compact('edit_management_pdf','id_m','id'));
	 
	}

	public function updateSubheadingpdf(Request $request , $id){

		$validated = $request->validate([
            'pdf_file' => 'size:2048',
        ]);
		$pdf = $request->file('pdf');
		if($request->hasFile('pdf') != ""){
        	$filename = time() . '.' .$pdf->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf->move($destinationPath, $filename);
            $pdf = '/act_rulepdf/' . $filename;

            $update_act_banner_detail = ActRuleSubheading::find($id);
			$update_act_banner_detail->pdf = $pdf;
			$update_act_banner_detail->save();
		}

		return redirect('list_pdf/'.$request->sub_hed_id)->with('success','Update Management Subheading Successfully');
	}

	// 01/02/2022
	public function deletePdfManagement($id){

		$delete = ActRuleSubheading::where('id',$id)->delete();
		return redirect()->back()->with('success','Detail Delete Successfully');		
	}

	public function addmorePdfManagement($id,$m_id){

			return view('actsRule.addmorePdfManagement',compact('id','m_id'));
	}

	public function savemorePdfManagement(Request $request , $id,$m_id){


		$validated = $request->validate([
            'pdf' => 'required',
            'subheading' => 'required',
        ]);
		$pdf = $request->file('pdf');
		$subheading = $request->input('subheading');

		if($request->hasFile('pdf') != ""){
        	$filename = time() . '.' .$pdf->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf->move($destinationPath, $filename);
            $pdf = '/act_rulepdf/' . $filename;

            
		}

		$save = new ActRuleSubheading();
		$save->pdf = $pdf;
		$save->subheading = $subheading;
		$save->management_heading_id = $id;
		$save->save();

		return redirect('list_mana_heading/'.$id.'/'.$m_id)->with('success','Detail Added Successfully');
	}
	// 01/02/2022

	public function basicDetailList($a_id){

		$list = ActRulePdfDetail::where('banner_id',$a_id)->where('deleted_status','0')->paginate(10);
		return view('actsRule.basicdetailList',compact('a_id','list'));
	}

	public function deletebasicDetailList($a_id){

		$delete = ActRulePdfDetail::find($a_id);		
		$delete->deleted_status = '1';		
		$delete->save();
		return redirect()->back()->with('success','Detail Deleted Successfully');		
	}

	public function editbasicDetailList($id , $a_id){

		$edit = ActRulePdfDetail::where('id',$id)->first();
		return view('actsRule.editbasicdetailList',compact('id','a_id','edit'));
	}

	public function updatebasicDetailList(Request $request , $id, $a_id){

		$validated = $request->validate([
            'pdf_url' => 'size:2048',
        ]);
		$pdf_url = $request->file('pdf_url');
		$name = $request->input('name');
		$update = ActRulePdfDetail::find($id);
		if($request->hasFile('pdf_url') != ""){
        	$filename = time() . '.' .$pdf_url->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf_url->move($destinationPath, $filename);
            $pdf_url = '/act_rulepdf/' . $filename;
			$update->pdf_url = $pdf_url;
           
		}


		$update->name = $request->name ;
		$update->save();
		return redirect('/basic-actrulepdf/'.$a_id)->with('success','Detail Updated Successfully');
	}

	public function addbasicDetailList($a_id){

		return view('actsRule.addbasicdetailList',compact('a_id'));
	}

	public function savebasicDetailList(Request $request , $a_id){

		$validated = $request->validate([
            'pdf_url' => 'required',
            'name' => 'required',
        ]);
		$pdf_url = $request->file('pdf_url');
		$name = $request->input('name');
		
		if($request->hasFile('pdf_url') != ""){
        	$filename = time() . '.' .$pdf_url->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf_url->move($destinationPath, $filename);
            $pdf_url = '/act_rulepdf/' . $filename;
			
           
		}

		$save = new ActRulePdfDetail();
		$save->name = $request->name;
		$save->pdf_url = $pdf_url;
		$save->banner_id = $a_id;
		$save->save();
		return redirect('/basic-actrulepdf/'.$a_id)->with('success','Detail Added Successfully');
	}

	public function viewWildlife($ab_id){

		$w_list = ActRuleHeadings::where('banner_id',$ab_id)->where('deleted_status','0')->paginate(10);
		foreach($w_list as $w_count){

			$w_count->w_count = ActRulePdfTwo::where('wildlife_detail_id',$w_count->id)->count();
		}
		// return $w_list ; 
		return view('actsRule.wildlifeList',compact('w_list','ab_id'));
	}
	public function editactruleWildlifeDetail($id,$ab_id){

		$edit = ActRuleHeadings::where('id',$id)->first();
		return view('actsRule.edit_wildlife',compact('id','ab_id','edit'));
	}

	public function updateactruleWildlifeDetail(Request $request , $id ,$ab_id){

		$validated = $request->validate([
            'description' => 'required',
            'wildlife_heading' => 'required',
        ]);
		$description = $request->input('description');
		$wildlife_heading = $request->input('name');
		
		
		$update = ActRuleHeadings::find($id);
		$update->wildlife_heading = $request->wildlife_heading;
		$update->description = $description;
		$update->banner_id = $ab_id;
		$update->save();
		return redirect('/view-wildlife/'.$ab_id)->with('success','Act&Rules Wildlife Detail Updated Successfully');
	}

	public function deleteActruleWildlifeDetail($id){

		$delete = ActRuleHeadings::find($id);
		$delete->deleted_status = '1';
		$delete->save();
		return redirect()->back()->with('success','Act&Rules Wildlife Detail Deleted Successfully');
	}

	public function addMoreWildlife($id){

			return view('actsRule.add_morewildlife',compact('id'));
	}

	public function saveMoreWildlife(Request $request , $id){

		$validated = $request->validate([
            'description' => 'required',
            'wildlife_heading' => 'required',
        ]);
		$description = $request->input('description');
		$wildlife_heading = $request->input('name');
		
		
		$update = new ActRuleHeadings();
		$update->wildlife_heading = $request->wildlife_heading;
		$update->description = $description;
		$update->banner_id = $id;
		$update->save();
		return redirect('/view-wildlife/'.$id)->with('success','Act&Rules Wildlife Detail Added Successfully');
	}

	public function pdfDetailWildlife($id, $ab_id){

		$list = ActRulePdfTwo::where('wildlife_detail_id',$id)->where('deleted_status','0')->orderby('id','DESC')->paginate(10);
		return view('actsRule.wildlife_pdf_detail_list',compact('id','ab_id','list'));
	}

	public function deletepdfDetailWildlife($id){

		$delete = ActRulePdfTwo::find($id);
		$delete->deleted_status = '1' ;
		$delete->save();
		return redirect()->back()->with('success','Pdf Detail deleted Successfully');
	}

	public function editpdfDetailWildlife($id, $pdf_id , $ab_id){

		 $edit = ActRulePdfTwo::where('id',$id)->first();
		return view('actsRule.edit_wildlife_pdf_detail_list',compact('id','pdf_id','ab_id','edit'));
	}

	public function updatepdfDetailWildlife(Request $request , $id, $pdf_id , $ab_id){

		// $validated = $request->validate([
  //           'description' => 'required',
  //           'wildlife_heading' => 'required',
  //       ]);
		$name_of_pdf = $request->input('name_of_pdf');
		$pdf_link = $request->file('pdf_link');

		$update = ActRulePdfTwo::find($id);
		if($request->hasFile('pdf_link') != ""){
        	$filename = time() . '.' .$pdf_link->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf_link->move($destinationPath, $filename);
            $pdf_link = '/act_rulepdf/' . $filename;
			
		$update->pdf_link = $pdf_link;
           
		}
		
		$update->name_of_pdf = $name_of_pdf;
		$update->save();
		return redirect('/wildlife-pdf-detail-list/'.$pdf_id.'/'.$ab_id)->with('success','Act&Rules Wildlife Pdf Detail Updated Successfully');
	}

	public function addpdfDetailWildlife($id, $ab_id){

		return view('actsRule.add_wildlife_pdf_detail',compact('id','ab_id'));		
	}

	public function savepdfDetailWildlife(Request $request ,$id,$ab_id){


		$validated = $request->validate([
            'name_of_pdf' => 'required',
            'pdf_link' => 'required',
        ]);
		$name_of_pdf = $request->input('name_of_pdf');
		$pdf_link = $request->file('pdf_link');

		if($request->hasFile('pdf_link') != ""){
        	$filename = time() . '.' .$pdf_link->getClientOriginalName();
            $destinationPath = public_path('/act_rulepdf');
            $pdf_link->move($destinationPath, $filename);
            $pdf_link = '/act_rulepdf/' . $filename;
		}
		
		$update = new ActRulePdfTwo();
		$update->name_of_pdf = $name_of_pdf;
		$update->pdf_link = $pdf_link;
		$update->banner_id = $ab_id;
		$update->wildlife_detail_id = $id;
		$update->save();

		return redirect('/wildlife-pdf-detail-list/'.$id.'/'.$ab_id)->with('success','Act&Rules Wildlife Pdf Detail Added Successfully');
	}
// end class 
}