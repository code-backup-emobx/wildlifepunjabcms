<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\ContactAddress;
use Validator;
use Session;
use Image;


class ContactAddressController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    

   public function contact_address(){
   
   		$listing = ContactAddress::paginate(15);
   
   		return view('contact_address.list',compact('listing'));
   }

	public function add_contact_detail(){
    	
    	return view('contact_address.create');
    
    }

	public function save_contact_detail(Request $request){
    
    	// return $request->all();
    	
    	
		$validated = $request->validate([
        'image' => 'required|max:2048',
        'name' => 'required',
        'designation' => 'required',
    	]);

	
   


        $name = $request->input('name');
        $designation = $request->input('designation');
        $contact = $request->input('contact');

    	

		

			 // $folder_path = '/images';
            $destinationPath = public_path('/contact_address');

            $image = $request->file('image');

            $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

            $img = Image::make($image->getRealPath());
            $img->save($destinationPath.'/'.$img_name);

            //resize image
            $img->resize(70,70)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

            $input['img'] = $img_name;


		$save_contact = new ContactAddress();
		$save_contact->image = '/contact_address/'.$input['img'];
		$save_contact->name = $name;
		$save_contact->designation = $designation;
		$save_contact->contact = $contact;
		$save_contact->save();

		return redirect('/contact-address')->with('success','Contact Address Detail Added Successfully');
    	
    
    }

	public function edit_contact_detail($id){
    
    	$edit = ContactAddress::where('id',$id)->first();
        return view('contact_address.edit',compact('edit'));
    }

	public function update_contact_detail(Request $request , $id){
    // return $request->all();
    	$validated = $request->validate([
        'image' => 'max:2048',
    	]);

	
        // $image = $request->file('image');
        $name = $request->input('name');
        $contact = $request->input('contact');

		$update_contact = ContactAddress::find($id);
    
        if($request->hasFile('image') != ""){
        	// $folder_path = '/images';
            $destinationPath = public_path('/contact_address');

            $image = $request->file('image');

            $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

            $img = Image::make($image->getRealPath());
            $img->save($destinationPath.'/'.$img_name);

            //resize image
            $img->resize(70,70)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

            $input['img'] = $img_name;
    
		// if($request->hasFile('image') != ""){
		// $filename =$image->getClientOriginalName();
		// $destinationPath = public_path('/contact_address');
		// $image->move($destinationPath, $filename);
		// $image = '/contact_address/' . $filename;
		// }
		$update_contact->image = '/contact_address/'.$input['img'];
        
        }
    

		$update_contact->name = $name;
		$update_contact->designation = $request->designation;
		$update_contact->contact = $request->contact;
		$update_contact->save();

		return redirect('/contact-address')->with('success','Contact Address Detail Updated Successfully');
    
    }

	public function delete_contact_detail($id){
    
    	ContactAddress::where('id',$id)->delete();
    	return redirect('/contact-address')->with('success','Contact Address Detail Deleted Successfully');
    }
 
// end class 
}