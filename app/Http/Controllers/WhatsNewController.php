<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use Image;
use App\Models\WhatsNew;

class WhatsNewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function listing(){
    	
    	$list = WhatsNew::paginate(10);
    	return view('whatsNew.listing',compact('list'));
    
    }

	public function add_whatsnew_detail(){
    	
    	return view('whatsNew.create');
    }

	public function save_whatsnew_detail(Request $request){
    	
    $validator = Validator::make($request->all(), [
            'text' => 'required',
           'date' => 'required',
                         
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
    
    	// return $request->all();
    	$text = $request->input('text');
        $text_date = $request->input('date');
    
    	
    
    	
    	$save_detail = new WhatsNew();
    	$save_detail->text = $text;
    	$save_detail->date = $text_date;
    	$save_detail->save();
    	return redirect('/whats_new_list')->with('success','Whats New Detail Added Successfully');	
    
    }

	public function edit_whatsnew_detail($id){
    
    	$edit = WhatsNew::where('id',$id)->first();
    	return view('whatsNew.edit',compact('edit'));
    }

	public function update_whatsnew_detail(Request $request,$id){
    
    
    	// return $request->all();
    	$text = $request->input('text');
        $text_date = $request->input('date');
    
    	
    	$save_detail = WhatsNew::find($id);
    	$save_detail->text = $text;
    	$save_detail->date = $text_date;
    	$save_detail->save();
    
    	return redirect('/whats_new_list')->with('success','Whats New Detail Updated Successfully');	
    }
   
	public function delete_whatnewdetail($id){
    	WhatsNew::find($id)->delete();
    	return redirect('/whats_new_list')->with('success','Whats New Detail Deleted Successfully');	
    }

// end clss
}