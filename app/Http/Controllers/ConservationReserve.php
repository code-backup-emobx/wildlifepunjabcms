<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\RakhSarai;
use App\Models\Roparwetland;
use App\Models\RanjitSagar;
use App\Models\BeasRiver;
use App\Models\KaliBein;

use App\Models\RakhSaraiNotificationDetail;
use App\Models\RoparwetlandNotificationDetail;
use App\Models\RanjitSagarDamNotificationDetail;
use App\Models\BeasRiverNotificationDetail;
use App\Models\KalibeinNotificationDetail;


class ConservationReserve extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function rakhsaraiListing(){

    	$rakhsaralist = RakhSarai::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
    	foreach($rakhsaralist as $list){

    		$list->text_count = RakhSaraiNotificationDetail::where('rakh_sarai_id',$list->id)->count();
    		$list->pdf_count = RakhSaraiNotificationDetail::where('rakh_sarai_id',$list->id)->count();
    	}
    	// return $rakhsaralist;
    	return view('conservationReserves.rakhSarai.listing',compact('rakhsaralist'));
    }
	
	public function addrakhsarai(){

		return view('conservationReserves.rakhSarai.add');		
	}

	public function saverakhsarai(Request $request){
	
    
		$validated = $request->validate([
         'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required',
        'rakhsarai_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'rakhsarai_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
        'notificationtitle' =>'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$pdf_file = $request->file('pdf_file');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/rakhsarai');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/rakhsarai/' . $filename;
		}

		$save_rakhsari = new RakhSarai();
		$save_rakhsari->banner_image = $banner_image;
		$save_rakhsari->banner_heading = $banner_heading;
		$save_rakhsari->description = $description;
		$save_rakhsari->rakhsarai_heading = $rakhsarai_heading;
		$save_rakhsari->rakhsarai_title = $rakhsarai_title;
		$save_rakhsari->district = $district;
		$save_rakhsari->location = $location;
		$save_rakhsari->area = $area;
		$save_rakhsari->status_of_land = $status_of_land;
		$save_rakhsari->important_fauna = $important_fauna;
		$save_rakhsari->important_flora = $important_flora;
		$save_rakhsari->save();

		$id = $save_rakhsari->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/rakhsarai/', $image);
				$image_data[] = '/rakhsarai/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new RakhSaraiNotificationDetail();
			$save_subheading->rakh_sarai_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/rakh_sarai')->with('success','Rakh Sarai Conservation Reserve Detail Added Successfully');
	}

	public function editrakhsarai($id){

		$editrakhsarai = RakhSarai::where('id',$id)->first();
		return view('conservationReserves.rakhSarai.edit',compact('editrakhsarai'));	
	}

	public function updaterakhsarai(Request $request , $id){
    
    		$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		// $notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

            $update_rakhsari = RakhSarai::find($id);
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/rakhsarai');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/rakhsarai/' . $filename;

			$update_rakhsari->banner_image = $banner_image;
		}

		
		$update_rakhsari->banner_heading = $banner_heading;
		$update_rakhsari->description = $description;
		$update_rakhsari->rakhsarai_heading = $rakhsarai_heading;
		$update_rakhsari->rakhsarai_title = $rakhsarai_title;
		$update_rakhsari->district = $district;
		$update_rakhsari->location = $location;
		$update_rakhsari->area = $area;
		$update_rakhsari->status_of_land = $status_of_land;
		// $update_rakhsari->notification_detail = $notification_detail;
		$update_rakhsari->important_fauna = $important_fauna;
		$update_rakhsari->important_flora = $important_flora;
		$update_rakhsari->save();

		return redirect('/rakh_sarai')->with('success','Rakh Sarai Conservation Reserve Detail Added Successfully');

	}

	public function deleterakhsarai($id){

		$delete_rakhsari = RakhSarai::find($id);
		$delete_rakhsari->deleted_status = '1';
		$delete_rakhsari->save();
		return redirect('/rakh_sarai')->with('success','Rakh Sarai Conservation Reserve Detail Delete Successfully');
	}

	// notification text
	public function rakh_notificationtextListing($id){

		$notification_list = RakhSaraiNotificationDetail::where('rakh_sarai_id',$id)->get();

	return view('conservationReserves.rakhSarai.notification_list',compact('notification_list','id'));
	}

	public function rakh_deleteListing($id){

		$delete_listing = RakhSaraiNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function rakh_editNotificationText($id,$rh_id){

		$edit_notification = RakhSaraiNotificationDetail::where('id',$id)->first();

	return view('conservationReserves.rakhSarai.edit_notification',compact('edit_notification','id','rh_id'));
	}

	public function rakh_updateNotificationText(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');

		$update_notification = RakhSaraiNotificationDetail::find($id);

		if($request->hasFile('pdf_url') != "")
        {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/rakhsarai/', $image);
			$pdf_url = '/rakhsarai/'.$image;
			$update_notification->pdf_url = $pdf_url;

		}
		$update_notification->notification_title = $notification_title;
		$update_notification->save();

		return redirect('/rakh_notification_list/'.$update_notification->rakh_sarai_id)->with('success','Notification detail updated successfully');
	}

	public function rakh_addNewNotificationText($id){

		return view('conservationReserves.rakhSarai.add_new_notification',compact('id'));		 
	}

	public function rakh_saveNewNotificationText(Request $request , $id){


		$validated = $request->validate([
        'pdf_url' => 'required|max:2048',
        'notification_title' => 'required',
    	]);

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');

		if($request->hasFile('pdf_url') != "")
        {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/rakhsarai/', $image);
			$pdf_url = '/rakhsarai/'.$image;

		}
		$save_notification = new RakhSaraiNotificationDetail();
		$save_notification->notification_title = $request->notification_title;
		$save_notification->pdf_url = $pdf_url;
		$save_notification->rakh_sarai_id = $id;
		$save_notification->save();

		return redirect('/rakh_notification_list/'.$save_notification->rakh_sarai_id)->with('success','Notification Detail Save successfully');
	}

	
	public function roparList(){

		$roparwetlandlist = Roparwetland::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($roparwetlandlist as $list){

    		$list->text_count = RoparwetlandNotificationDetail::where('roparwetland_id',$list->id)->count();
    		$list->pdf_count = RoparwetlandNotificationDetail::where('roparwetland_id',$list->id)->count();
    	}
		return view('conservationReserves.roparWetland.listing',compact('roparwetlandlist'));
	}

	public function addropar(){

		return view('conservationReserves.roparWetland.add');
	}

	public function saveropar(Request $request){
   

		$validated = $request->validate([
         'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required',
        'rakhsarai_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'rakhsarai_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
        'notificationtitle' => 'required',
        'pdf_file' => 'required',
    	]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$pdf_file = $request->file('pdf_file');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/ropar_wetland');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/ropar_wetland/' . $filename;
		}

		$save_roparwetland = new Roparwetland();
		$save_roparwetland->banner_image = $banner_image;
		$save_roparwetland->banner_heading = $banner_heading;
		$save_roparwetland->description = $description;
		$save_roparwetland->rakhsarai_heading = $rakhsarai_heading;
		$save_roparwetland->rakhsarai_title = $rakhsarai_title;
		$save_roparwetland->district = $district;
		$save_roparwetland->location = $location;
		$save_roparwetland->area = $area;
		$save_roparwetland->status_of_land = $status_of_land;
		$save_roparwetland->important_fauna = $important_fauna;
		$save_roparwetland->important_flora = $important_flora;
		$save_roparwetland->save();

		$id = $save_roparwetland->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/ropar_wetland/', $image);
				$image_data[] = '/ropar_wetland/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new RoparwetlandNotificationDetail();
			$save_subheading->roparwetland_id = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/cr_roparlist')->with('success','Ropar Wetland Conservation Reserve Detail Added Successfully');
	}

	public function editropar($id){

 		$editropar = Roparwetland::where('id',$id)->first();
		return view('conservationReserves.roparWetland.edit',compact('editropar'));	
	}

	public function updateropar(Request $request , $id){
		
    	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	
    
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		// $notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

            $update_ropar_wetland = Roparwetland::find($id);
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/ropar_wetland');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/ropar_wetland/' . $filename;

			$update_ropar_wetland->banner_image = $banner_image;
			
		}
		
			$update_ropar_wetland->banner_heading = $banner_heading;
			$update_ropar_wetland->description = $description;
			$update_ropar_wetland->rakhsarai_heading = $rakhsarai_heading;
			$update_ropar_wetland->rakhsarai_title = $rakhsarai_title;
			$update_ropar_wetland->district = $district;
			$update_ropar_wetland->location = $location;
			$update_ropar_wetland->area = $area;
			$update_ropar_wetland->status_of_land = $status_of_land;
			// $update_ropar_wetland->notification_detail = $notification_detail;
			$update_ropar_wetland->important_fauna = $important_fauna;
			$update_ropar_wetland->important_flora = $important_flora;
			$update_ropar_wetland->save();
	
		return redirect('/cr_roparlist')->with('success','Ropar Wetland Conservation Reserve Detail Updated Successfully');
	}

	public function deleteropar($id){

		$delete_roparwetland = Roparwetland::find($id);
		$delete_roparwetland->deleted_status = '1';
		$delete_roparwetland->save();
		return redirect('/cr_roparlist')->with('success','Ropar Wetland Conservation Reserve Detail Deleted Successfully');		
	}

	// notification text
	public function ropar_notificationtextListing($id){

		$notification_list = RoparwetlandNotificationDetail::where('roparwetland_id',$id)->get();

	return view('conservationReserves.roparWetland.notification_list',compact('notification_list','id'));
	}

	public function ropar_deleteListing($id){

		$delete_listing = RoparwetlandNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function ropar_editNotificationText($id,$r_id){

		$edit_notification = RoparwetlandNotificationDetail::where('id',$id)->first();

	return view('conservationReserves.roparWetland.edit_notification',compact('edit_notification','id','r_id'));
	}

	public function ropar_updateNotificationText(Request $request , $id){

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');
		$update_notification = RoparwetlandNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
        {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/ropar_wetland/', $image);
			$pdf_url = '/ropar_wetland/'.$image;
			$update_notification->pdf_url = $pdf_url;

		}

		$update_notification->notification_title = $notification_title;
		$update_notification->save();

		return redirect('/ropar_notification_list/'.$update_notification->roparwetland_id)->with('success','Notification Detail updated successfully');
	}

	public function ropar_addNewNotificationText($id){

		return view('conservationReserves.roparWetland.add_new_notification',compact('id'));		 
	}

	public function ropar_saveNewNotificationText(Request $request , $id){

		$validated = $request->validate([
        'pdf_url' => 'required|max:2048',
        'notification_title' => 'required',
    	]);

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');

		if($request->hasFile('pdf_url') != "")
        {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/ropar_wetland/', $image);
			$pdf_url = '/ropar_wetland/'.$image;

		}
		$save_notification = new RoparwetlandNotificationDetail();
		$save_notification->notification_title = $notification_title;
		$save_notification->roparwetland_id = $id;
			$save_notification->pdf_url = $pdf_url;
		$save_notification->save();

		return redirect('/ropar_notification_list/'.$save_notification->roparwetland_id)->with('success','Notification Detail Save successfully');
	}


	public function rangitsagarList(){

		$rangitsagarlist = RanjitSagar::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($rangitsagarlist as $list){

    		$list->text_count = RanjitSagarDamNotificationDetail::where('ranjitsagar_id',$list->id)->count();
    		$list->pdf_count = RanjitSagarDamNotificationDetail::where('ranjitsagar_id',$list->id)->count();
    	}
		return view('conservationReserves.ranjitSagar.listing',compact('rangitsagarlist'));
	}

	public function addrangitsagar(){

		return view('conservationReserves.ranjitSagar.add');
	}
	public function saverangitsagar(Request $request){
    
    
		$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required',
        'rakhsarai_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'rakhsarai_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
        'notificationtitle' => 'required',
        'pdf_file' => 'required',
    	]);


		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$pdf_file = $request->file('pdf_file');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/ranjitsagar_wetland');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/ranjitsagar_wetland/' . $filename;

		}

        $save_ranjitsagar = new RanjitSagar();
		$save_ranjitsagar->banner_image = $banner_image;
		$save_ranjitsagar->banner_heading = $banner_heading;
		$save_ranjitsagar->description = $description;
		$save_ranjitsagar->rakhsarai_heading = $rakhsarai_heading;
		$save_ranjitsagar->rakhsarai_title = $rakhsarai_title;
		$save_ranjitsagar->district = $district;
		$save_ranjitsagar->location = $location;
		$save_ranjitsagar->area = $area;
		$save_ranjitsagar->status_of_land = $status_of_land;
		$save_ranjitsagar->important_fauna = $important_fauna;
		$save_ranjitsagar->important_flora = $important_flora;
		$save_ranjitsagar->save();

		$id = $save_ranjitsagar->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/ropar_wetland/', $image);
				$image_data[] = '/ropar_wetland/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new RanjitSagarDamNotificationDetail();
			$save_subheading->ranjitsagar_id  = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/cr_ranjitsagar')->with('success','Ranjit Sagar Wetland Conservation Reserve Detail Added Successfully');
	}

	public function editrangitsagar($id){

		$editrangitsagar = RanjitSagar::with('get_notification_detail')->where('id',$id)->first();
		return view('conservationReserves.ranjitSagar.edit',compact('editrangitsagar'));
	}

	public function updaterangitsagar(Request $request,$id){
    
      	$validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		// $notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

            $update_ranjitsagar = RanjitSagar::find($id);
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/ranjitsagar_wetland');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/ranjitsagar_wetland/' . $filename;
			$update_ranjitsagar->banner_image = $banner_image;
		
			
		}
		
		
			$update_ranjitsagar->banner_heading = $banner_heading;
			$update_ranjitsagar->description = $description;
			$update_ranjitsagar->rakhsarai_heading = $rakhsarai_heading;
			$update_ranjitsagar->rakhsarai_title = $rakhsarai_title;
			$update_ranjitsagar->district = $district;
			$update_ranjitsagar->location = $location;
			$update_ranjitsagar->area = $area;
			$update_ranjitsagar->status_of_land = $status_of_land;
			// $update_ranjitsagar->notification_detail = $notification_detail;
			$update_ranjitsagar->important_fauna = $important_fauna;
			$update_ranjitsagar->important_flora = $important_flora;
			$update_ranjitsagar->save();
		

		return redirect('/cr_ranjitsagar')->with('success','Ranjit Sagar Wetland Conservation Reserve Detail Updated Successfully');
	}

	public function deleterangitsagar($id){

		$delete_ranjitsagar = RanjitSagar::find($id);
		$delete_ranjitsagar->deleted_status = '1';
		$delete_ranjitsagar->save();
		return redirect('/cr_ranjitsagar')->with('success','Ranjit Sagar Wetland Conservation Reserve Detail Deleted Successfully');

	}

	// notification text
	public function ranjit_notificationtextListing($id){

		$notification_list = RanjitSagarDamNotificationDetail::where('ranjitsagar_id',$id)->get();

	return view('conservationReserves.ranjitSagar.notification_list',compact('notification_list','id'));
	}

	public function ranjit_deleteListing($id){

		$delete_listing = RanjitSagarDamNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function ranjit_editNotificationText($id,$rn_id){

		$edit_notification = RanjitSagarDamNotificationDetail::where('id',$id)->first();

	return view('conservationReserves.ranjitSagar.edit_notification',compact('edit_notification','id','rn_id'));
	}

	public function ranjit_updateNotificationText(Request $request , $id ,$rn_id){

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');
		$update_notification = RanjitSagarDamNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	    {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/ranjit/', $image);
			$pdf_url = '/ranjit/'.$image;
			$update_notification->pdf_url = $pdf_url;

		}

		$update_notification->notification_title = $request->notification_title;
		$update_notification->save();

		return redirect('/ranjit_notification_list/'.$update_notification->ranjitsagar_id)->with('success','Notification detail updated successfully');
	}

	public function ranjit_addNewNotificationText($id){

		return view('conservationReserves.ranjitSagar.add_new_notification',compact('id'));		 
	}

	public function ranjit_saveNewNotificationText(Request $request , $id){

		$validated = $request->validate([
        'pdf_url' => 'required|max:2048',
        'notification_title' => 'required',
    	]);

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');
		if($request->hasFile('pdf_url') != "")
	    {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/ranjit/', $image);
			$pdf_url = '/ranjit/'.$image;
		}
		$save_notification = new RanjitSagarDamNotificationDetail();
		$save_notification->notification_title = $notification_title;
		$save_notification->pdf_url = $pdf_url;
		$save_notification->ranjitsagar_id = $id;
		$save_notification->save();

		return redirect('/ranjit_notification_list/'.$save_notification->ranjitsagar_id)->with('success','Notification Text Save successfully');
	}


	public function beasRiverList(){

		$beasriverlist = BeasRiver::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($beasriverlist as $list){

    		$list->text_count = BeasRiverNotificationDetail::where('beas_river_id',$list->id)->count();
    		$list->pdf_count = BeasRiverNotificationDetail::where('beas_river_id',$list->id)->count();
    	}
		return view('conservationReserves.beasRiver.listing',compact('beasriverlist'));
	}

	public function addbeasRiver(){

		return view('conservationReserves.beasRiver.add');
	}

	public function savebeasRiver(Request $request){
    
     
		$validated = $request->validate([
         'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required',
        'rakhsarai_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'rakhsarai_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
        'notificationtitle' => 'required',
        'pdf_file' => 'required',
    	]);


		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$pdf_file = $request->file('pdf_file');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/beas_river');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/beas_river/' . $filename;

		}

        $save_beasriver = new BeasRiver();
		$save_beasriver->banner_image = $banner_image;
		$save_beasriver->banner_heading = $banner_heading;
		$save_beasriver->description = $description;
		$save_beasriver->rakhsarai_heading = $rakhsarai_heading;
		$save_beasriver->rakhsarai_title = $rakhsarai_title;
		$save_beasriver->district = $district;
		$save_beasriver->location = $location;
		$save_beasriver->area = $area;
		$save_beasriver->status_of_land = $status_of_land;
		$save_beasriver->important_fauna = $important_fauna;
		$save_beasriver->important_flora = $important_flora;
		$save_beasriver->save();

		$id = $save_beasriver->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/ropar_wetland/', $image);
				$image_data[] = '/ropar_wetland/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new BeasRiverNotificationDetail();
			$save_subheading->beas_river_id  = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }


		return redirect('/cr_beasriver')->with('success','Beas River Wetland Conservation Reserve Detail Added Successfully');
	}

	public function editbeasRiver($id){

		$editbeas_river = BeasRiver::where('id',$id)->first();
		return view('conservationReserves.beasRiver.edit',compact('editbeas_river'));
	}

	public function updatebeasRiver(Request $request , $id){
    
        $validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);	

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		// $notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

            $update_beasriver = BeasRiver::find($id);
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/beas_river');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/beas_river/' . $filename;

			$update_beasriver->banner_image = $banner_image;
			
		}
		
			$update_beasriver->banner_heading = $banner_heading;
			$update_beasriver->description = $description;
			$update_beasriver->rakhsarai_heading = $rakhsarai_heading;
			$update_beasriver->rakhsarai_title = $rakhsarai_title;
			$update_beasriver->district = $district;
			$update_beasriver->location = $location;
			$update_beasriver->area = $area;
			$update_beasriver->status_of_land = $status_of_land;
			// $update_beasriver->notification_detail = $notification_detail;
			$update_beasriver->important_fauna = $important_fauna;
			$update_beasriver->important_flora = $important_flora;
			$update_beasriver->save();
		

		return redirect('/cr_beasriver')->with('success','Beas River Wetland Conservation Reserve Detail Added Successfully');
	}

	public function deletebeasRiver($id){

		$delete_beasriver = BeasRiver::find($id);
		$delete_beasriver->deleted_status = '1';
		$delete_beasriver->save();
		return redirect('/cr_beasriver')->with('success','Beas River Wetland Conservation Reserve Detail Added Successfully');
	}

	// notification text
	public function beas_notificationtextListing($id){

		$notification_list = BeasRiverNotificationDetail::where('beas_river_id',$id)->get();

	return view('conservationReserves.beasRiver.notification_list',compact('notification_list','id'));
	}

	public function beas_deleteListing($id){

		$delete_listing = BeasRiverNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function beas_editNotificationText($id,$b_id){

		$edit_notification = BeasRiverNotificationDetail::where('id',$id)->first();

	return view('conservationReserves.beasRiver.edit_notification',compact('edit_notification','id','b_id'));
	}

	public function beas_updateNotificationText(Request $request , $id , $b_id){
	

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');
		$update_notification = BeasRiverNotificationDetail::find($id);

		if($request->hasFile('pdf_url') != "")
        {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/beas_river/', $image);
			$pdf_url = '/beas_river/'.$image;
			$update_notification->pdf_url = $pdf_url;

		}

		$update_notification->notification_title = $notification_title;
		$update_notification->save();

		return redirect('/beas_notification_list/'.$update_notification->beas_river_id)->with('success','Notification Detail updated successfully');
	}

	public function beas_addNewNotificationText($id){

		return view('conservationReserves.beasRiver.add_new_notification',compact('id'));		 
	}

	public function beas_saveNewNotificationText(Request $request , $id){

		$validated = $request->validate([
        'pdf_url' => 'required|max:2048',
        'notification_title' => 'required',
    	]);

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');
		
		if($request->hasFile('pdf_url') != "")
        {
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/beas_river/', $image);
			$pdf_url = '/beas_river/'.$image;
		}

		$save_notification = new BeasRiverNotificationDetail();
		$save_notification->notification_title = $notification_title;
		$save_notification->pdf_url = $pdf_url;
		$save_notification->beas_river_id = $id;
		$save_notification->save();

		return redirect('/beas_notification_list/'.$save_notification->beas_river_id)->with('success','Notification Detail Save successfully');
	}

	
	public function kalibeanList(){

		$kali_list = KaliBein::with('get_notification_detail')->where('deleted_status','0')->paginate(10);
		foreach($kali_list as $list){

    		$list->text_count = KalibeinNotificationDetail::where('kalibein_id',$list->id)->count();
    		$list->pdf_count = KalibeinNotificationDetail::where('kalibein_id',$list->id)->count();
    	}
		return view('conservationReserves.kalibean.listing',compact('kali_list'));
	}

	public function addkalibean(){

		return view('conservationReserves.kalibean.add');		
	}

	public function savekalibean(Request $request){
    
		$validated = $request->validate([
         'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required',
        'rakhsarai_heading' => 'required|regex:/^[\.)a-zA-Z,!?( ]*$/',
        'rakhsarai_title' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'status_of_land' => 'required|regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'notificationtitle' => 'required',
        'important_fauna' => 'required',
        'important_flora' => 'required',
        'location' => 'required',
        'notificationtitle' => 'required',
        'pdf_file' => 'required',
    	]);

		
		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$notificationtitle = $request->input('notificationtitle');
		$pdf_file = $request->file('pdf_file');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/kali_bean');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/kali_bean/' . $filename;

		}
	
	    $save_kalibean = new KaliBein();
		$save_kalibean->banner_image = $banner_image;
		$save_kalibean->banner_heading = $banner_heading;
		$save_kalibean->description = $description;
		$save_kalibean->rakhsarai_heading = $rakhsarai_heading;
		$save_kalibean->rakhsarai_title = $rakhsarai_title;
		$save_kalibean->district = $district;
		$save_kalibean->location = $location;
		$save_kalibean->area = $area;
		$save_kalibean->status_of_land = $status_of_land;
		$save_kalibean->important_fauna = $important_fauna;
		$save_kalibean->important_flora = $important_flora;
		$save_kalibean->save();

		$id = $save_kalibean->id;

		$image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/ropar_wetland/', $image);
				$image_data[] = '/ropar_wetland/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new KalibeinNotificationDetail();
			$save_subheading->kalibein_id   = $id;
			$save_subheading->notification_title = $notificationtitle[$i] ;
			$save_subheading->pdf_url = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/cr_kalibean')->with('success','Kali Bein Detail Added Successfully');
	}

	public function editkalibean($id){

		$edit_kalibean = KaliBein::where('id',$id)->first();
		return view('conservationReserves.kalibean.edit',compact('edit_kalibean'));
	}

	public function updatekalibean(Request $request,$id){
		
    	 $validated = $request->validate([
        'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        // 'description' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        'wildlife_heading' => 'regex:/^[\.)a-zA-Z,!?( ]*$/',
        // 'location' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'wildlife_title' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'district' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        'area' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'status_of_land' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'notification_detail' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_fauna' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
        // 'important_flora' => 'regex:/^[\.)a-zA-Z0-9,!?( ]*$/',
    	]);		

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
		$description = $request->input('description');
		$rakhsarai_heading = $request->input('rakhsarai_heading');
		$location = $request->input('location');
		$rakhsarai_title = $request->input('rakhsarai_title');
		$district = $request->input('district');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		// $notification_detail = $request->input('notification_detail');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

            $update_kali_kalibean =KaliBein::find($id);
		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/kali_bean');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/kali_bean/' . $filename;

			$update_kali_kalibean->banner_image = $banner_image;
			
		}
		
			$update_kali_kalibean->banner_heading = $banner_heading;
			$update_kali_kalibean->description = $description;
			$update_kali_kalibean->rakhsarai_heading = $rakhsarai_heading;
			$update_kali_kalibean->rakhsarai_title = $rakhsarai_title;
			$update_kali_kalibean->district = $district;
			$update_kali_kalibean->location = $location;
			$update_kali_kalibean->area = $area;
			$update_kali_kalibean->status_of_land = $status_of_land;
			// $update_kali_kalibean->notification_detail = $notification_detail;
			$update_kali_kalibean->important_fauna = $important_fauna;
			$update_kali_kalibean->important_flora = $important_flora;
			$update_kali_kalibean->save();
		
	   
		return redirect('/cr_kalibean')->with('success','Kali Bein Detail Updated Successfully');
	}

	public function deletekalibean($id){

		$delete_kalibean =KaliBein::find($id);
		$delete_kalibean->deleted_status = '1';
		$delete_kalibean->save();
		return redirect('/cr_kalibean')->with('success','Kali Bein Detail Delete Successfully');
	}

	// notification text
	public function kalibean_notificationtextListing($id){

		$notification_list = KalibeinNotificationDetail::where('kalibein_id',$id)->get();

	return view('conservationReserves.kalibean.notification_list',compact('notification_list','id'));
	}

	public function kalibean_deleteListing($id){

		$delete_listing = KalibeinNotificationDetail::find($id);
		$delete_listing->delete();
		return redirect()->back()->with('success','Notification Delete Successfully');
	}

	public function kalibean_editNotificationText($id,$k_id){

		$edit_notification = KalibeinNotificationDetail::where('id',$id)->first();

	return view('conservationReserves.kalibean.edit_notification',compact('edit_notification','id','k_id'));
	}

	public function kalibean_updateNotificationText(Request $request , $id,$k_id){

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');
		$update_notification = KalibeinNotificationDetail::find($id);
		if($request->hasFile('pdf_url') != "")
	    {
	          
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/kali_bean/', $image);
			$pdf_url = '/kali_bean/'.$image;
			$update_notification->pdf_url = $pdf_url;

		}

		$update_notification->notification_title = $notification_title;
		$update_notification->save();

		return redirect('/kalibean_notification_list/'.$update_notification->kalibein_id)->with('success','Notification detail updated successfully');
	}

	public function kalibean_addNewNotificationText($id){

		return view('conservationReserves.kalibean.add_new_notification',compact('id'));		 
	}

	public function kalibean_saveNewNotificationText(Request $request , $id){

		 $validated = $request->validate([
        'pdf_url' => 'required|max:2048',
        'notification_title' => 'required',
    	]);	

		$pdf_url = $request->file('pdf_url');
		$notification_title = $request->input('notification_title');

		if($request->hasFile('pdf_url') != "")
	    {
	          
            // $image = $file->getClientOriginalName();
            $image = time() . '.' . $pdf_url->getClientOriginalName();
			$pdf_url->move(public_path() . '/kali_bean/', $image);
			$pdf_url = '/kali_bean/'.$image;

		}

		$save_notification = new KalibeinNotificationDetail();
		$save_notification->notification_title = $notification_title;
		$save_notification->kalibein_id = $id;
		$save_notification->pdf_url = $pdf_url;
		$save_notification->save();

		return redirect('/kalibean_notification_list/'.$save_notification->kalibein_id)->with('success','Notification Detail Save successfully');
	}

// end class 
}