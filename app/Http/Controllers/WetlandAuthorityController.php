<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\WetlandAuthority;
use App\Models\WetlandNotification;
use App\Models\WetlandRules;


class WetlandAuthorityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function wetlandList(){

    	$list = WetlandAuthority::paginate(10);
    	foreach($list as $l){
    		$l->notification_count = WetlandNotification::where('wetland_id',$l->id)->count();
    		$l->rule_count = WetlandRules::where('wetland_id',$l->id)->count();

    	}
    	// return $list;
    	return view('wetlandAuthority.list',compact('list'));
    }

    public function addAuthority(){

    	return view('wetlandAuthority.add');
    }

    public function saveAuthority(Request $request){

    	// return $request->all();
    	$validated = $request->validate([
        'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
        'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
        'description' => 'required',
        'notificationtitle' => 'required',
        'pdf_file' => 'required',
        'rule_heading' => 'required',
        'rule_pdf' => 'required',
    	]);

    	$banner_image = $request->file('banner_image');
    	$banner_heading = $request->input('banner_heading');
    	$description = $request->input('description');
    	$notificationtitle = $request->input('notificationtitle');
    	$pdf_file = $request->file('pdf_file');
    	$rule_heading = $request->input('rule_heading');
    	$rule_pdf = $request->file('rule_pdf');


    	if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/wetland_authority');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/wetland_authority/' . $filename;
		}

		$save_detail = new WetlandAuthority();
		$save_detail->image = $banner_image;
		$save_detail->title = $banner_heading;
		$save_detail->description = $description;
		$save_detail->save();

		$id = $save_detail->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/wetland/', $image);
				$image_data[] = '/wetland/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_notification = new WetlandNotification();
			$save_notification->wetland_id = $id;
			$save_notification->notification_text = $notificationtitle[$i] ;
			$save_notification->notification_pdf = $image_data[$i] ;
			$save_notification->save();
	    }

	     $image_data_=array();
		if($request->hasFile('rule_pdf') != "")
        {
            foreach($request->file('rule_pdf') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/wetland_authority/', $image);
				$image_data_[] = '/wetland_authority/'.$image;
            }
		}
		for($i=0;$i<count($image_data_);$i++)
        {
			$save_rules = new WetlandRules();
			$save_rules->wetland_id = $id;
			$save_rules->rule_heading = $rule_heading[$i] ;
			$save_rules->rule_pdf = $image_data_[$i] ;
			$save_rules->save();
	    }

	    return redirect('/wetland_list')->with('success','Wetaln Authority Detail Saved Successfully');
    }

    public function deleteAuthority($id){

		WetlandAuthority::where('id',$id)->delete();    	
		WetlandNotification::where('wetland_id',$id)->delete();    	
		WetlandRules::where('wetland_id',$id)->delete();
		return redirect('/wetland_list')->with('success','Wetland Authority Detail Deleted Successfully');    	
    }

    public function editAuthority($id){

    	$edit = WetlandAuthority::with('get_notification','get_rules')->where('id',$id)->first();

    	return view('wetlandAuthority.edit',compact('edit','id'));
    }

    public function updateAuthority(Request $request , $id){

    	$banner_image = $request->file('banner_image');
    	$banner_heading = $request->input('banner_heading');
    	$description = $request->input('description');

		$update_detail = WetlandAuthority::find($id);

		if($request->hasFile('banner_image') != ""){
        	$filename = time() . '.' .$banner_image->getClientOriginalName();
            $destinationPath = public_path('/wetland_authority');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/wetland_authority/' . $filename;
			$update_detail->image = $banner_image;
		}
		$update_detail->title = $banner_heading;
		$update_detail->description = $description;
		$update_detail->save();

		return redirect('/wetland_list')->with('success','Wetland Detail Updated Successfully');
    }
    public function wetlandnotListing($id){

    	$w_id = $id;
    	$noti_listing = WetlandNotification::where('wetland_id',$id)->paginate(10);
    	return view('wetlandAuthority.notification_listing',compact('noti_listing','w_id'));
    }

    public function editwetlandnotListing($id,$w_id){

    	$edit_noti = WetlandNotification::where('id',$id)->first();
    	return view('wetlandAuthority.edit_noti_wetland',compact('edit_noti','w_id','id'));
    }
    public function updatewetlandnotListing(Request $request ,$id , $w_id){

    	$notification_text = $request->input('notification_text');
    	$notification_pdf = $request->file('notification_pdf');

    	$update_detail = WetlandNotification::find($id);

		if($request->hasFile('notification_pdf') != ""){
        	$filename =time() . '.' .$notification_pdf->getClientOriginalName();
            $destinationPath = public_path('/wetland_authority');
            $notification_pdf->move($destinationPath, $filename);
            $notification_pdf = '/wetland_authority/' . $filename;
			$update_detail->notification_pdf = $notification_pdf;
		}
		$update_detail->notification_text = $notification_text;
		$update_detail->save();
		return redirect('wetland_not_listing/'.$w_id)->with('Successfully Updated Notification Detail');
    }

    public function add_noti_authority($w_id)
    {

    	return view('wetlandAuthority.add_noti_wetland',compact('w_id'));
    }

    public function save_noti_authority(Request $request ,$w_id){

    	$validated = $request->validate([
        'notification_pdf' => 'required|max:2048',
        'notification_text' => 'required',
    	]);

    	$notification_text = $request->input('notification_text');
    	$notification_pdf = $request->file('notification_pdf');


		if($request->hasFile('notification_pdf') != ""){
        	$filename = time() . '.' .$notification_pdf->getClientOriginalName();
            $destinationPath = public_path('/wetland_authority');
            $notification_pdf->move($destinationPath, $filename);
            $notification_pdf = '/wetland_authority/' . $filename;
		}

    	$save_not_detail = new WetlandNotification();
		$save_not_detail->wetland_id = $w_id;
		$save_not_detail->notification_text = $notification_text;
		$save_not_detail->notification_pdf = $notification_pdf;
		$save_not_detail->save();
		return redirect('wetland_not_listing/'.$w_id)->with('Successfully Notification Detail');
    }

    public function delete_noti_authority($id,$w_id){

    	WetlandNotification::where('id',$id)->delete();

    	return redirect('/wetland_not_listing/'.$w_id)->with('success','Notification detail Deleted Successfully');
    }

    public function wetland_rules_authority($id){

    	$w_id = $id;
    	$rule_list = WetlandRules::where('wetland_id',$id)->paginate(10);

    	return view('wetlandAuthority.rules_listing',compact('rule_list','w_id'));
    }

    public function edit_wetlandauthority_rule($id,$w_id){

    	$edit_rule = WetlandRules::where('id',$id)->first();
    	return view('wetlandAuthority.edit_rules',compact('edit_rule','w_id','id'));
    }

    public function update_wetlandauthority_rule(Request $request , $id, $w_id){

    	$validated = $request->validate([
        'rule_pdf' => 'max:2048',
    	]);

    	$rule_heading = $request->input('rule_heading');
    	$rule_pdf = $request->file('rule_pdf');

    	$update_not_detail = WetlandRules::find($id);

		if($request->hasFile('rule_pdf') != ""){
        	$filename = time() . '.' . $rule_pdf->getClientOriginalName();
            $destinationPath = public_path('/wetland_authority');
            $rule_pdf->move($destinationPath, $filename);
            $rule_pdf = '/wetland_authority/' . $filename;
			$update_not_detail->rule_pdf = $rule_pdf;
		}

		$update_not_detail->rule_heading = $rule_heading;
		$update_not_detail->save();
		return redirect('wetland_rules_listing/'.$w_id)->with('Successfully Rule Detail');
    }

    public function add_wetlandauthority_rule($w_id){

    	return view('wetlandAuthority.add_rule',compact('w_id'));
    }

    public function save_wetlandauthority_rule(Request $request , $w_id){

		$validated = $request->validate([
        'rule_pdf' => 'required|max:2048',
        'rule_heading' => 'required',
    	]);

    	$rule_heading = $request->input('rule_heading');
    	$rule_pdf = $request->file('rule_pdf');


		if($request->hasFile('rule_pdf') != ""){
        	$filename = time() . '.' . $rule_pdf->getClientOriginalName();
            $destinationPath = public_path('/wetland_authority');
            $rule_pdf->move($destinationPath, $filename);
            $rule_pdf = '/wetland_authority/' . $filename;
		}

    	$save_rule_detail = new WetlandRules();
		$save_rule_detail->rule_heading = $rule_heading;
		$save_rule_detail->wetland_id = $w_id;
		$save_rule_detail->rule_pdf = $rule_pdf;
		$save_rule_detail->save();
		return redirect('wetland_rules_listing/'.$w_id)->with('Successfully Rule Detail Added');    	
    }
    public function deletewetlandauthority_rule($id,$w_id){
    	WetlandRules::where('id',$id)->delete();
    	return redirect('wetland_rules_listing/'.$w_id)->with('success','Rule Detail Deleted Successfully');

    }

    public function view_wetlandauthority($id){

        $list = WetlandAuthority::with('get_notification','get_rules')->first();
        return view('wetlandAuthority.view',compact('list','id'));
    }

 
// end class 
}