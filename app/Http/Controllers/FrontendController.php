<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;


use App\Models\FooterScheme;
use App\Models\FooterAbout;
use App\Models\FooterAboutDetail;
use App\Models\FooterPolicyGuideline;

use App\Models\NotificationBannerDetail;
use App\Models\NotificationDetail;

use App\Models\OrganisationTabs;
use App\Models\OrganisationBannerDetail;
use App\Models\OrganisationTableDetail;

use App\Models\TourismBannerDetail;
use App\Models\TourismData;
use App\Models\TourismTypes;

use App\Models\TourismLinkDetail;
use App\Models\TourismLinkImages;

use App\Models\TenderBannerDetail;
use App\Models\TenderTypes;
use App\Models\TenderDetails;

use App\Models\MinisterDetail;

use App\Models\ZooType;
use App\Models\ZooDetail;

use App\Models\ZooGalleryBanner;
use App\Models\ZooGallery;

use App\Models\ContactUsBannerDetail;
use App\Models\ContactUs;
use App\Models\ContactUsDetail;
use App\Models\ContactUsMessage;



use App\Models\WorldDay;
use App\Models\WorldDayDetail;
use App\Models\WorldDayDetails;
use App\Models\WorldDayImagesDetail;
use App\Models\WorldDayImagesHonour;

use App\Models\HowApplyType;
use App\Models\HowToApplyDetail;

use App\Models\UpcomingEventDetail;
use App\Models\EventBgImages;

use App\Models\VolunteerDetail;

use App\Models\ImpLinkDetail;

use App\Models\ServicesDetail;
use App\Models\ServicesHowApply;

use App\Models\ActRuleBanner;
use App\Models\ActRulePdfDetail;
use App\Models\ActRulePdfTwo;
use App\Models\ActRulePdfThree;
use App\Models\ActRuleHeadings;
use App\Models\ActRuleManagementDetail;
use App\Models\ActManagementNangal;
use App\Models\ActManagementNangalPdf;
use App\Models\District;

use App\Models\Video;
use App\Models\ProtectedAreaSubcategory;

use App\Models\WetlandAuthority;
use App\Models\WetlandNotification;
use App\Models\WetlandRules;

use App\Models\FrontendZooDetail;
use App\Models\ProtectedWetland;
use App\Models\FrontendProtectedWetlandDetail;
use App\Models\ProtectedWetlandImages;
use App\Models\FrontendProtectedArea;
use App\Models\FrontendProtectedAreaImages;
use App\Models\FrontendWetlandImages;

use App\Models\ZooInventory;
use App\Models\GalleryImages;
use App\Models\GalleryAllImages;
use App\Models\ContactAddress;
use App\Models\EventMaster;
use App\Models\LandingBannerImagesText;
use App\Models\Implinklist;

use App\Models\WhatsNew;

use DB;

class FrontendController extends Controller
{
   
	public function index()
    {
         // pa('hi');die();
        $param = $data = array();
        // pa($param);die;
        $get_banner_one = call_api('banner_list', $data);
        // $get_banner_two = call_api('banner_two_list',$data);
    
        $data = array();

        $data['get_banner_one'] = $get_banner_one->list;
        // $data['get_banner_two'] = $get_banner_two->list;
        
        $data['base_url'] = $get_banner_one->base_url;
        $data['minister_data'] = MinisterDetail::where('deleted_status','0')->get();
        $data['zoo_detail'] = ZooDetail::where('deleted_status','0')->get();
        $data['video_detail'] = Video::get();

        // $data['world_wetland'] = WorldDayDetail::where('deleted_status','0')->take(4)->get();
        $data['how_to_apply_type'] = HowApplyType::all();
        // $data['event_list'] = UpcomingEventDetail::where('deleted_status','0')->get();
        
        $data['event_bg_detail'] = EventBgImages::where('deleted_status','0')->latest()->first();
        $data['volunter_detail'] = VolunteerDetail::where('deleted_status','0')->latest()->first();
	
    	$data['whats_new'] = WhatsNew::get();
  
        $date = date('Y-m-d');
      	

   		

   		$month = date('m');
   		$day = (int)date('d');
       // return  gettype((int)$day);die;
		// whereDate function is vary in this server 
        // $a= DB::table('word_day_view')->whereDate('ryear','<',$date)->orderBy('ryear','DESC')->take(4)->get();
    		// whereDate function is vary in this server
        // $a= DB::table('word_day_view')->where('month_number','<',$month)->where('event_date','<',date('d'))->where('deleted_at',null)->orderBy('month_number','DESC')->orderBy('event_date','DESC')->take(4)->get();
        $a= DB::select( DB::raw("SELECT * FROM (select * from word_day_view where month_number< ".$month." UNION SELECT * from word_day_view where month_number = ".$month." AND event_date <= ".$day.")a where deleted_at is null ORDER by month_number,event_date ASC limit 4") );
    //return $a;
       $data['world_list_previous'] = $a;
       // $data['world_list_previous'] = $a->reverse();
   // return $data['world_list_previous']=$a->sortBy('ryear');
       // whereDate function vary on this server
    	//$data['world_list_upcoming'] = DB::table('word_day_view')->whereDate('ryear','>=',$date)->orderBy('ryear','asc')->take(4)->get();
     // whereDate function vary on this server
        // $data['world_list_upcoming'] = DB::table('word_day_view')->where('month_number','>',$month)->orderBy('event_date','asc')->take(4)->get();
        $data['world_list_upcoming'] = DB::select( DB::raw("SELECT * FROM (select * from word_day_view where month_number>".$month." UNION SELECT * from word_day_view where month_number = ".$month." AND event_date >= ".$day.")a where deleted_at is null ORDER by month_number,event_date ASC limit 4") );
		//return $data['world_list_upcoming'];
        $data['district_list'] = District::all();
        $data['gallery_images'] = GalleryImages::get();
    
    	$data['landing_banner_image_text'] = LandingBannerImagesText::latest()->first();
    
    	$data['minister_and_officials'] = ContactAddress::get();
    	// $x = ContactAddress::orderBy('id', 'DESC')->take(6)->get();
		// $data['contact_address_last_six'] = $x->reverse();
    // return  $data['get_banner_one'];die;
        return view('frontend.index',$data);

    }
	   

    public function send_grid_email_v4($to, $to_names, $subject, $text,$attachment = null,$filename = null) {

        $to_email = [
            'email'=>$to,
            'name'=>$to_names
            ];
            
        $content = [
           'type'=>'text/html',
            'value'=>$text
            ];    
            
        $personalizations = [
            'to' => array($to_email),
            'subject' =>$subject 
            ]; 
            

        $from = [
            'email'=>'emobxdev@gmail.com',
            'name'=>'Application for wildlife volunteer.'
            ];     
        $reply_to = [
            'email'=>'emobxdev@gmail.com',
            'name'=>'Application for wildlife volunteer.'
            ];     

        $params = array();

        // https://api.sendgrid.com/v3/mail/send

        $params = [
            'personalizations' => array($personalizations),
            'from' => $from,
            'reply_to' => $reply_to,
            'content' => array($content),
            
            ];

        $header = array(
            "authorization: Bearer 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",
            "content-type: application/json"
          ); 
          
        //   echo json_encode($params); die;
        // pa(); die;    

        //           echo $to.$to_names.$subject; die;

            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($params),
          CURLOPT_HTTPHEADER => $header,
        ));



         $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);  

         if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo $response;
           return $response;
        }
         return true;

     }
  

    public function howToApplySave(Request $request){

        $type_id = $request->type_id;
        $description = $request->input('description');
        $email = $request->input('email');
        $mobile_number = $request->input('mobile_number');
        $district_id = $request->input('district_id');

       $type_name =  HowApplyType::where('id',$type_id)->first();
       $t_name = $type_name->type_name;

        $district =  District::where('id',$district_id)->first();
        $d_name = $district->district_name;

        $save = new HowToApplyDetail();
        $save->type_id = $type_id;
        $save->description = $description;
        $save->district_id = $district_id;
        $save->district_name =  $d_name;
        $save->email = $email;
        $save->mobile_number = $mobile_number;
        $save->save();

        

        $html = '<h2> Hi, </br></h2><b>Volunteer Email:</b> '.$request->input('email').'</br><p><b>Mobile Number:</b>&nbsp;&nbsp;'.$request->input('mobile_number').'</p></br><p><b>Wildlife Type:</b>&nbsp;&nbsp;'.$type_name->type_name.'</p></br><p><b> District:</b> &nbsp;&nbsp;'.$district->district_name.'</p></br>'.'<p><b>Division:</b> &nbsp;&nbsp;'.$request->input('description').'</p>';
    

        $this->send_grid_email_v4('cwlwpunjab@gmail.com',"",'Application for wildlife volunteer.',$html);

        return response()->json($save);
    }

	// Home Menu detail
    public function homePage(){

        $param = $data = array();
        $home_detail = call_api('home', $data);

        $data = array();
        $data['home_detail'] = $home_detail->list;

         $data['base_url'] = $home_detail->base_url;
        // return $data;
        return view('frontend.home.home',$data);
    }

    public function wildlifeSymbol(){
        
        $param = $data = array();
        $wildlife_detail = call_api('wildlife_symbols',$data);

        $data = array();
        $data['wildlife_detail'] = $wildlife_detail->list;

        $data['base_url'] = $wildlife_detail->base_url;
        // return $data;
        return view('frontend.statewildLife.wildlife_symbol',$data);
    }

    // public function birMotiBagh(){

    //     $param = $data = array();
    //     $birmotibagh_detail = call_api('birmoti_bagh',$data);

    //     $data = array();
    //     $data['birmotibagh_detail'] = $birmotibagh_detail->list;

    //     $data['base_url'] = $birmotibagh_detail->base_url;
    //     // return $data;

    //     return view('frontend.protectedArea.wildlifeSantuaries.birmotibagh',$data);
    // }

    //  public function birGurdialpur(){

    //     $param = $data = array();
    //     $birgurdialpur_detail = call_api('birgurdialpur',$data);

    //     $data = array();
    //     $data['birgurdialpur_detail'] = $birgurdialpur_detail->list;

    //     $data['base_url'] = $birgurdialpur_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.gurdialpur_detail',$data);
    // }

    // public function bir_bhunerheri(){

    //     $param = $data = array();
    //     $birbhunerheri_detail = call_api('bhunerheri_detail',$data);

    //     $data = array();
    //     $data['birbhunerheri_detail'] = $birbhunerheri_detail->list;

    //     $data['base_url'] = $birbhunerheri_detail->base_url;
    //      // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.bhunerheri_detail',$data);
    // }

    // public function bir_menhasDetail(){

    //     $param = $data = array();
    //     $mehas_detail = call_api('mehas_detail',$data);

    //     $data = array();
    //     $data['mehas_detail'] = $mehas_detail->list;

    //     $data['base_url'] = $mehas_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.mehas_detail',$data);
    // }

    // public function bir_dosanjhDetail(){

    //     $param = $data = array();
    //     $dosanjh_detail = call_api('dosanjh_detail',$data);

    //     $data = array();
    //     $data['dosanjh_detail'] = $dosanjh_detail->list;

    //     $data['base_url'] = $dosanjh_detail->base_url;
    //      // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.dosanjh_detail',$data);

    // }

    // public function bir_bhadsonDetail(){

    //     $param = $data = array();
    //     $bhadson_detail = call_api('bhadson_detail',$data);

    //     $data = array();
    //     $data['bhadson_detail'] = $bhadson_detail->list;

    //     $data['base_url'] = $bhadson_detail->base_url;
    //      // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.bhadson_detail',$data);
    // }

    // public function bir_aishwandetail(){

    //     $param = $data = array();
    //     $aishwan_detail = call_api('aishwan_detail',$data);

    //     $data = array();
    //     $data['aishwan_detail'] = $aishwan_detail->list;

    //     $data['base_url'] = $aishwan_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.aishwan_detail',$data);
    // }

    // public function bir_abohardetail(){

    //     $param = $data = array();
    //     $abohar_detail = call_api('abohar_detail',$data);

    //     $data = array();
    //     $data['abohar_detail'] = $abohar_detail->list;

    //     $data['base_url'] = $abohar_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.abohar_detail',$data);        
    // }

    // public function bir_harikedetail(){

    //     $param = $data = array();
    //     $harike_detail = call_api('harike_detail',$data);

    //     $data = array();
    //     $data['harike_detail'] = $harike_detail->list;

    //     $data['base_url'] = $harike_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.harike_detail',$data); 
    // }

    // public function bir_takhni_rehmapurdetail(){

    //     $param = $data = array();
    //     $takhni_rehmapur_detail = call_api('takhni_rehmapur_detail',$data);

    //     $data = array();
    //     $data['takhni_rehmapur_detail'] = $takhni_rehmapur_detail->list;

    //     $data['base_url'] = $takhni_rehmapur_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.takhni_rehmapurdetail',$data);         
    // }

    // public function bir_jhajjar_bachaulidetail(){

    //     $param = $data = array();
    //     $jhajjar_bachauli_detail = call_api('jhajjar_bachauli_detail',$data);

    //     $data = array();
    //     $data['jhajjar_bachauli_detail'] = $jhajjar_bachauli_detail->list;

    //     $data['base_url'] = $jhajjar_bachauli_detail->base_url;
    //     // return $data;
    //    return view('frontend.protectedArea.wildlifeSantuaries.jhajjar_bachaulidetail',$data);   
    // }

    // public function bir_kathlaurkushlian_detail(){

    //     $param = $data = array();
    //     $kathlaur_kushlian_detail = call_api('kathlaur_kushlian_detail',$data);

    //     $data = array();
    //     $data['kathlaur_kushlian_detail'] = $kathlaur_kushlian_detail->list;

    //     $data['base_url'] = $kathlaur_kushlian_detail->base_url;
    //     // return $data;
    //     return view('frontend.protectedArea.wildlifeSantuaries.kathlaurkushlian_detail',$data); 
    // }

    // public function bir_nangal_wildlife_detail(){

    //     $param = $data = array();
    //     $nangal_wildlife_detail = call_api('nangal_wildlife_detail',$data);

    //     $data = array();
    //     $data['nangal_wildlife_detail'] = $nangal_wildlife_detail->list;

    //     $data['base_url'] = $nangal_wildlife_detail->base_url;
    //     // return $data;
    //   return view('frontend.protectedArea.wildlifeSantuaries.nangal_wildlife_detail',$data);
    // }

    // public function panniwalaDetail(){

    //     $param = $data = array();
    //     $panniwala_detail = call_api('panniwalagumjal',$data);

    //     $data = array();
    //     $data['panniwala_detail'] = $panniwala_detail->list;

    //     $data['base_url'] = $panniwala_detail->base_url;
    //     // return $data;

    //     return view('frontend.communityReserve.panniwala.panniwala-detail',$data);
    // }

    // public function lalwanDetail(){

    //     $param = $data = array();
    //     $lalwan_detail = call_api('lalwan_detail',$data);

    //     $data = array();
    //     $data['lalwan_detail'] = $lalwan_detail->list;

    //     $data['base_url'] = $lalwan_detail->base_url;
    //     // return $data;

    //     return view('frontend.communityReserve.lalwanCommunity.lalwan_community',$data);
    // }

    // public function keshopurDetail(){

    //     $param = $data = array();
    //     $keshupur_detail = call_api('keshupur_detail',$data);

    //     $data = array();
    //     $data['keshupur_detail'] = $keshupur_detail->list;

    //     $data['base_url'] = $keshupur_detail->base_url;
    //     // return $data;

    //     return view('frontend.communityReserve.keshupur.keshupur_detail',$data);
    // }

    // public function siswanDetail(){

    //     $param = $data = array();
    //     $siswan_detail = call_api('siswan_detail',$data);

    //     $data = array();
    //     $data['siswan_detail'] = $siswan_detail->list;

    //     $data['base_url'] = $siswan_detail->base_url;
    //     // return $data;

    //     return view('frontend.communityReserve.siswanDetail.siswan_detail',$data);
    // }

    // public function rakhsaraiDetail(){

    //     $param = $data = array();
    //     $rakhsarai_detail = call_api('rakh_sarai',$data);

    //     $data = array();
    //     $data['rakhsarai_detail'] = $rakhsarai_detail->list;

    //     $data['base_url'] = $rakhsarai_detail->base_url;
    //     // return $data;

    //     return view('frontend.conservationReserve.rakhsarai_detail',$data);

    // }

    // public function roparetlandDetail(){

    //     $param = $data = array();
    //     $roparwetland_detail = call_api('ropar_wetland',$data);

    //     $data = array();
    //     $data['roparwetland_detail'] = $roparwetland_detail->list;

    //     $data['base_url'] = $roparwetland_detail->base_url;
    //     // return $data;
    //     return view('frontend.conservationReserve.ropar_wetland_detail',$data);        
    // }

    // public function ranjitsagarDamDetail(){

    //     $param = $data = array();
    //     $ranjit_sagar_dam = call_api('ranjit_sagar_dam',$data);

    //     $data = array();
    //     $data['ranjit_sagar_dam'] = $ranjit_sagar_dam->list;

    //     $data['base_url'] = $ranjit_sagar_dam->base_url;
    //      // return $data;
    //      return view('frontend.conservationReserve.ranjit_sagar_dam_detail',$data);    
    // }

    // public function beasRiverDetail(){

    //     $param = $data = array();
    //     $beas_river = call_api('beas_river',$data);

    //     $data = array();
    //     $data['beas_river'] = $beas_river->list;

    //     $data['base_url'] = $beas_river->base_url;
    //     // return $data;
    //      return view('frontend.conservationReserve.beas_river_detail',$data); 
    // }
    // public function kalibeinDetail(){

    //     $param = $data = array();
    //     $kali_bean_detail = call_api('kali_bean_detail',$data);

    //     $data = array();
    //     $data['kali_bean_detail'] = $kali_bean_detail->list;

    //     $data['base_url'] = $kali_bean_detail->base_url;
    //     // return $data;
    //      return view('frontend.conservationReserve.kali_bein_detail',$data);
    // }

    // public function harikasantuaryDetail(){

    //     $param = $data = array();
    //     $harike_santuary = call_api('harike_santuary',$data);

    //     $data = array();
    //     $data['harike_santuary'] = $harike_santuary->list;

    //     $data['base_url'] = $harike_santuary->base_url;
    //      // return $data;

    //     return view('frontend.protectedWetland.harika-detail',$data);
    // }

    // public function nangalsantuaryDetail(){

    //     $param = $data = array();
    //     $nangal_santuary = call_api('nangal_santuary',$data);

    //     $data = array();
    //     $data['nangal_santuary'] = $nangal_santuary->list;

    //     $data['base_url'] = $nangal_santuary->base_url;
    //      // return $data;

    //     return view('frontend.protectedWetland.nangal-detail',$data);
    // }

    // public function keshopur_detail(){

    //      $param = $data = array();
    //     $keshopur_detail = call_api('keshopur_detail',$data);

    //     $data = array();
    //     $data['keshopur_detail'] = $keshopur_detail->list;

    //     $data['base_url'] = $keshopur_detail->base_url;
    //     // return $data;
    //      return view('frontend.protectedWetland.keshopur-detail',$data);
    // }

    // public function chhhatbirbirzoo_Detail(){

    //     $param = $data = array();
    //     $chhatbir_zoo = call_api('chhatbir_zoo',$data);

    //     $data = array();
    //     $data['chhatbir_zoo'] = $chhatbir_zoo->list;

    //     $data['base_url'] = $chhatbir_zoo->base_url;
    //     // return $data;
    //     return view('frontend.chhatbirzoo.chhatbir_zoo_detail' , $data);        
    // }

    // public function ludhianazoo_Detail(){
        
    //     $param = $data = array();
    //     $ludhiana_zoo = call_api('ludhiana_zoo',$data);

    //     $data = array();
    //     $data['ludhiana_zoo'] = $ludhiana_zoo->list;

    //     $data['base_url'] = $ludhiana_zoo->base_url;
    //     // return $data;
    //     return view('frontend.ludhianazoo.ludhiana_zoo_detail',$data);
    // }

    // public function patialazoo_Detail(){
        
    //     $param = $data = array();
    //     $patiala_zoo = call_api('patiala_zoo',$data);

    //     $data = array();
    //     $data['patiala_zoo'] = $patiala_zoo->list;

    //     $data['base_url'] = $patiala_zoo->base_url;
    //     // return $data;
    //     return view('frontend.patialazoo.patiala_zoo_detail',$data);
    // }

    // public function bathindazoo_Detail(){

    //     $param = $data = array();
    //     $bathinda_zoo = call_api('bathinda_zoo',$data);

    //     $data = array();
    //     $data['bathinda_zoo'] = $bathinda_zoo->list;

    //     $data['base_url'] = $bathinda_zoo->base_url;
    //      // return $data;
    //     return view('frontend.bathindazoo.bathinda_zoo_detail',$data);
    // }

    // public function deerparkzoo_Detail(){

    //     $param = $data = array();
    //     $deerpark_neelon_zoo = call_api('deerpark_neelon_zoo',$data);

    //     $data = array();
    //     $data['deerpark_neelon_zoo'] = $deerpark_neelon_zoo->list;

    //     $data['base_url'] = $deerpark_neelon_zoo->base_url;
    //     // $data = DeerParkZooFrontend::all();
    //     // return $data;
    //     return view('frontend.deerparkNeelon.deerparkneelon_detail',$data);
    // }

    public function schemedetail(){

        $list_scheme = FooterScheme::with('get_scheme_name')->where('deleted_status','0')->latest()->first();
        return view('frontend.footer.useful.scheme_detail',compact('list_scheme'));
    }

    public function aboutusdetail(){

         $aboutus_detail = FooterAbout::with('get_footer_about_detail')->where('deleted_status','0')->latest()->first();

        return view('frontend.footer.about.about_detail',compact('aboutus_detail'));
    }


    public function notificationdetail(){

       $notification_detail = NotificationBannerDetail::with('get_details')->where('deleted_status','0')->latest()->first();
        return view('frontend.notification.notification_detail',compact('notification_detail'));
    }

    public function organisationdetail(){

        $organisation_tab_detail = OrganisationTabs::all();
        
        $data['tab_1'] = OrganisationBannerDetail::with('get_detail_t1')->first();
        $data['tab_2'] = OrganisationBannerDetail::with('get_detail_t2')->first();
        $data['tab_3'] = OrganisationBannerDetail::with('get_detail_t3')->first();
        $data['tab_4'] = OrganisationBannerDetail::with('get_detail_t4')->first();
        $data['tab_5'] = OrganisationBannerDetail::with('get_detail_t5')->first();
        $data['tab_6'] = OrganisationBannerDetail::with('get_detail_t6')->first();
        $data['tab_7'] = OrganisationBannerDetail::with('get_detail_t7')->first();
        $data['tab_8'] = OrganisationBannerDetail::with('get_detail_t8')->first();
        $data['tab_9'] = OrganisationBannerDetail::with('get_detail_t9')->first();
        $data['tab_10'] = OrganisationBannerDetail::with('get_detail_t10')->first();
        $data['tab_11'] = OrganisationBannerDetail::with('get_detail_t11')->first();
        $data['tab_12'] = OrganisationBannerDetail::with('get_detail_t12')->first();
        $data['tab_13'] = OrganisationBannerDetail::with('get_detail_t13')->first();
        $data['tab_14'] = OrganisationBannerDetail::with('get_detail_t14')->first();
        $data['tab_15'] = OrganisationBannerDetail::with('get_detail_t15')->first();
        $data['tab_16'] = OrganisationBannerDetail::with('get_detail_t6')->first();
        $data['tab_17'] = OrganisationBannerDetail::with('get_detail_t17')->first();
        $data['tab_18'] = OrganisationBannerDetail::with('get_detail_t18')->first();
        $data['tab_19'] = OrganisationBannerDetail::with('get_detail_t19')->first();
        $data['tab_20'] = OrganisationBannerDetail::with('get_detail_t20')->first();
        $data['tab_21'] = OrganisationBannerDetail::with('get_detail_t21')->first();
        $data['tab_22'] = OrganisationBannerDetail::with('get_detail_t22')->first();
        $data['tab_23'] = OrganisationBannerDetail::with('get_detail_t23')->first();
        $data['tab_24'] = OrganisationBannerDetail::with('get_detail_t24')->first();
        $data['tab_25'] = OrganisationBannerDetail::with('get_detail_t25')->first();
        $data['tab_26'] = OrganisationBannerDetail::with('get_detail_t26')->first();
        $data['tab_27'] = OrganisationBannerDetail::with('get_detail_t27')->first();
        $data['tab_28'] = OrganisationBannerDetail::with('get_detail_t28')->first();
        $data['tab_29'] = OrganisationBannerDetail::with('get_detail_t29')->first();
        $data['tab_30'] = OrganisationBannerDetail::with('get_detail_t30')->first();
        $data['tab_31'] = OrganisationBannerDetail::with('get_detail_t31')->first();
        $data['tab_32'] = OrganisationBannerDetail::with('get_detail_t32')->first();
        $data['tab_33'] = OrganisationBannerDetail::with('get_detail_t33')->first();
        $data['tab_34'] = OrganisationBannerDetail::with('get_detail_t34')->first();
        $data['tab_35'] = OrganisationBannerDetail::with('get_detail_t35')->first();
      
        
//foreach( $data['tab_10'] as $table_c){
        $data['table_count']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_10']->id)->where('type_of_table','table_data')->where('tab_id','10')->count();
        $data['table_count_1']=  $table_count_1 = OrganisationTableDetail::where('banner_id',$data['tab_1']->id)->where('type_of_table','table_data')->where('tab_id','1')->count();
        $data['table_count_2']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_2']->id)->where('type_of_table','table_data')->where('tab_id','2')->count();
        $data['table_count_3']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_3']->id)->where('type_of_table','table_data')->where('tab_id','3')->count();
        $data['table_count_4']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_4']->id)->where('type_of_table','table_data')->where('tab_id','4')->count();
        $data['table_count_5']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_5']->id)->where('type_of_table','table_data')->where('tab_id','5')->count();
        $data['table_count_6']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_6']->id)->where('type_of_table','table_data')->where('tab_id','6')->count();
        $data['table_count_7']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_7']->id)->where('type_of_table','table_data')->where('tab_id','7')->count();
        $data['table_count_8']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_8']->id)->where('type_of_table','table_data')->where('tab_id','8')->count();
        $data['table_count_9']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_9']->id)->where('type_of_table','table_data')->where('tab_id','9')->count();
        $data['table_count_11']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_11']->id)->where('type_of_table','table_data')->where('tab_id','11')->count();
        $data['table_count_12']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_12']->id)->where('type_of_table','table_data')->where('tab_id','12')->count();
        $data['table_count_13']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_13']->id)->where('type_of_table','table_data')->where('tab_id','13')->count();
        $data['table_count_14']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_14']->id)->where('type_of_table','table_data')->where('tab_id','14')->count();
        $data['table_count_15']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_15']->id)->where('type_of_table','table_data')->where('tab_id','15')->count();
        $data['table_count_16']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_16']->id)->where('type_of_table','table_data')->where('tab_id','16')->count();
        $data['table_count_17']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_17']->id)->where('type_of_table','table_data')->where('tab_id','17')->count();
        $data['table_count_18']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_18']->id)->where('type_of_table','table_data')->where('tab_id','18')->count();
        $data['table_count_19']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_19']->id)->where('type_of_table','table_data')->where('tab_id','19')->count();
        $data['table_count_20']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_20']->id)->where('type_of_table','table_data')->where('tab_id','20')->count();
        $data['table_count_21']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_21']->id)->where('type_of_table','table_data')->where('tab_id','21')->count();
        $data['table_count_22']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_22']->id)->where('type_of_table','table_data')->where('tab_id','22')->count();
        $data['table_count_23']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_23']->id)->where('type_of_table','table_data')->where('tab_id','23')->count();
        $data['table_count_24']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_24']->id)->where('type_of_table','table_data')->where('tab_id','24')->count();
        $data['table_count_25']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_25']->id)->where('type_of_table','table_data')->where('tab_id','25')->count();
        $data['table_count_26']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_26']->id)->where('type_of_table','table_data')->where('tab_id','26')->count();
        $data['table_count_27']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_27']->id)->where('type_of_table','table_data')->where('tab_id','27')->count();
        $data['table_count_28']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_28']->id)->where('type_of_table','table_data')->where('tab_id','28')->count();
        $data['table_count_29']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_29']->id)->where('type_of_table','table_data')->where('tab_id','29')->count();
        $data['table_count_30']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_30']->id)->where('type_of_table','table_data')->where('tab_id','30')->count();
        $data['table_count_31']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_31']->id)->where('type_of_table','table_data')->where('tab_id','31')->count();
        $data['table_count_32']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_32']->id)->where('type_of_table','table_data')->where('tab_id','32')->count();
        $data['table_count_33']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_33']->id)->where('type_of_table','table_data')->where('tab_id','33')->count();
        $data['table_count_34']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_34']->id)->where('type_of_table','table_data')->where('tab_id','34')->count();
        $data['table_count_35']=  $table_count = OrganisationTableDetail::where('banner_id',$data['tab_35']->id)->where('type_of_table','table_data')->where('tab_id','35')->count();
//}
     //return $data['tab_10'];
        return view('frontend.organisation.organisation_detail',compact('organisation_tab_detail','data'));
    }

    public function tourismdetail(){
        $banner_detail = TourismBannerDetail::where('deleted_status','0')->latest()->first();
        $tourism_detail = TourismData::with('get_bdata','get_tourism_type')->where('deleted_status','0')->get();
    	// return $tourism_detail;
        return view('frontend.tourism.tourism_detail',compact('tourism_detail','banner_detail'));
    }

   public function tourismGallerydetail($id,$redirect_url,$redirect_id){
		
       // $total_image_count = TourismLinkImages::where('tourism_link_id',$id)->count();
        $total_image_count = DB::table('tourism_link_detail')
            ->join('tourism_link_images', 'tourism_link_detail.tourism_type_id', '=', 'tourism_link_images.tourism_type_id')
            ->where('tourism_link_detail.deleted_status',  '=', '0')
            ->count();
	// return $total_image_count;
       $tourism_image_detail = TourismLinkDetail::with('get_images')->where('tourism_type_id',$id)->where('deleted_status','0')->first();
   		// return $tourism_image_detail;
        return view('frontend.tourism.tourism_page',compact('tourism_image_detail','total_image_count','redirect_url','redirect_id'));
    }
    public function tenderDetails(){

        $tender_type = TenderTypes::all();
        $tender_list_t1 = TenderBannerDetail::with('get_tender_detail_t1')->where('deleted_status','0')->first();

        $tender_list_t2 = TenderBannerDetail::with('get_tender_detail_t2')->where('deleted_status','0')->first();
        return view('frontend.tender.tender_detail',compact('tender_list_t1','tender_list_t2','tender_type'));
    }

    public function viewGalleryDetail(){

        $zoogallerybanner_detail = ZooGalleryBanner::where('deleted_status','0')->latest()->first();
        $zoo_type_detail = ZooType::all();
        $eventmaster = EventMaster::get();
        $gallery_list = GalleryAllImages::get();
        $gallery_count = GalleryAllImages::count();
    	$protectedAreaSubcategory = ProtectedAreaSubcategory::get();

		$protectedWetland = ProtectedWetland::get();
        return view('frontend.zoo_gallery',compact('zoogallerybanner_detail','zoo_type_detail','gallery_list','gallery_count','protectedAreaSubcategory','protectedWetland','eventmaster'));
    }

    public function getZooImages(Request $request){

        // return $request->all();
        if($request->zoo_gallery =="0"){
            $gallery_list = GalleryAllImages::pluck('image');
        }
        else {
             $gallery_list = GalleryAllImages::where('type_id',$request->zoo_gallery)->latest()->pluck('image');
       

         }
       
        return response()->json($gallery_list);
    }

    public function contactUsDetail(){

        $contactbannerdetail  = ContactUsBannerDetail::where('deleted_status','0')->latest()->first();
        $contactdetail = ContactUsDetail::where('deleted_status','0')->take(3)->get();
       return view('frontend.footer.contactUs.contactUsDetail',compact('contactdetail','contactbannerdetail'));
    }

    public function contactusMessage(Request $request){

        // return $request->all();
        $validated = $request->validate([
        'full_name' => 'required',
        'email' => 'required',
        'subject' => 'required',
        'your_message' => 'required',
        ]);

        $full_name = $request->input('full_name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $your_message = $request->input('your_message');

        $check = ContactUsMessage::where('email',$request->email)->first();
        // if($check){
        //      return redirect('/contact_us')->with('fail','Please enter your unique email');
        // }
        // else{
            $save_message = new ContactUsMessage();
            $save_message->full_name = $full_name;
            $save_message->email = $email;
            $save_message->subject = $subject;
            $save_message->your_message = $your_message;
            $save_message->save();
        // }
      	
    
    	 $html = '<h2> Hi, </br></h2><b>Name:</b> '.$full_name.'</br><p><b>Email:</b>&nbsp;&nbsp;'.$email.'</p></br><p><b>Message:</b>&nbsp;&nbsp;'.$your_message.'</p>';
    

       $this->send_grid_email('cwlwpunjab@gmail.com',"",'Contact Us Message.',$html);

        return redirect('/contact_us')->with('success','Message Save successfully And Email Send To Wildlife Department');

    }

function send_grid_email($to, $to_names, $subject, $text,$attachment = null,$filename = null) {
//           echo $to.$to_names.$subject; die;
    $curl = curl_init();
    $params = array();
  
    $params['from'] = "emobxdev@gmail.com";
    $params['subject'] = $subject;
    $params['html'] = stripcslashes($text);
    $params['to'] = $to;
    $params['toname'] = $to_names;
$to_array = $to_final= $to_final_1= $content_array =  array();
$from_array = ['email'=>'emobxdev@gmail.com'];
//$from_array = ['email'=>'noreply@skitskot.com'];
$to_array[] = ['email'=>$to,'name'=>$to_names];
$to_final['to'] = $to_array;
$to_final_1[] = $to_final;
$content_array[] = [
"type"=>"text/html", "value"=>stripcslashes($text) ];
$result = [
    'personalizations'=>$to_final_1,
'from'=>$from_array,
'subject'=>$subject,
'content'=>$content_array

];
// pa($result); die;
    if(!empty($attachment)){
        $imagedata = file_get_contents($attachment);
        $params['files['.$filename.']'] = $imagedata;

    }

    $query = json_encode($result);

curl_setopt_array($curl, array(CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send", CURLOPT_RETURNTRANSFER => true,CURLOPT_ENCODING => "",CURLOPT_MAXREDIRS => 10,CURLOPT_TIMEOUT => 0,CURLOPT_FOLLOWLOCATION => true,CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,CURLOPT_CUSTOMREQUEST => "POST",CURLOPT_POSTFIELDS => $query, CURLOPT_HTTPHEADER => array("Content-Type: application/json","Authorization: Bearer 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" ) ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
//pa($response);
// pa($err);
    curl_close($curl);
     

  }

   public function getDetail($id){

      $images_world_day = WorldDayDetails::with('get_images_detail','get_images_honour_detail')->where('worldday_type_id',$id)->first();
        return view('frontend.image_detail',compact('images_world_day'));
    }

    public function impLinkDetail(){

    	$impDetail = ImpLinkDetail::with('get_links')->where('deleted_status','0')->latest()->first();
    
        return view('frontend.imp_detail',compact('impDetail'));
    }

    public function serviceDetail(){

        $service_detail =  ServicesDetail::where('deleted_status','0')->latest()->first();
        return view('frontend.services.services_detail',compact('service_detail'));
    }

    public function addserviceHowApply(Request $request){

        $name = $request->input('name');
        $email_address = $request->input('email_address');
        $mobile_number = $request->input('mobile_number');
        $aadhar_number = $request->input('aadhar_number');
        $address_one = $request->input('address_one');
        $address_two = $request->input('address_two');
        $city = $request->input('city');
        $zip = $request->input('zip');

        $save_howApplyServices = new ServicesHowApply();
        $save_howApplyServices->name = $name;
        $save_howApplyServices->email_address = $email_address;
        $save_howApplyServices->mobile_number = $mobile_number;
        $save_howApplyServices->aadhar_number = $aadhar_number;
        $save_howApplyServices->address_one = $address_one;
        $save_howApplyServices->address_two = $address_two;
        $save_howApplyServices->city = $city;
        $save_howApplyServices->zip = $zip;
        $save_howApplyServices->save();

    }

    public function serviceHowApply(Request $request){

        $name = $request->input('name');
        $email_address = $request->input('email_address');
        $mobile_number = $request->input('mobile_number');
        $aadhar_number = $request->input('aadhar_number');
        $address_one = $request->input('address_one');
        $address_two = $request->input('address_two');
        $city = $request->input('city');
        $zip = $request->input('zip');

        $save_howApplyServices = new ServicesHowApply();
        $save_howApplyServices->name = $name;
        $save_howApplyServices->email_address = $email_address;
        $save_howApplyServices->mobile_number = $mobile_number;
        $save_howApplyServices->aadhar_number = $aadhar_number;
        $save_howApplyServices->address_one = $address_one;
        $save_howApplyServices->address_two = $address_two;
        $save_howApplyServices->city = $city;
        $save_howApplyServices->zip = $zip;
        $save_howApplyServices->save();

        return response()->json($save_howApplyServices);
    }

    public function exportCsvServices(Request $request)
    {
       $fileName = 'services.csv';
        $task = ServicesHowApply::latest('id')->first();
        // return $tasks;
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('name', 'email_address', 'mobile_number', 'aadhar_number', 'address_one', 'address_two', 'city','zip');

            $callback = function() use($task, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

              
                    $row['name']  = $task->name;
                    $row['email_address']    = $task->email_address;
                    $row['mobile_number']    = $task->mobile_number;
                    $row['aadhar_number']  = $task->aadhar_number;
                    $row['address_one']  = $task->address_one;
                    $row['address_two']  = $task->address_two;
                    $row['city']  = $task->city;
                    $row['zip']  = $task->zip;

                    fputcsv($file, array($row['name'], $row['email_address'], $row['mobile_number'], $row['aadhar_number'], $row['address_one'], $row['address_two'], $row['city'], $row['zip']));
               

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }

		 public function actsRuleDetail(){
            // return "hello";
            $act_rule_list = ActRuleBanner::with('get_pdf_detail_one','get_pdf_two','get_pdf_three','get_actRuleHeadings','get_actRuleManagementHeadings','get_actRulenangalHeadings','get_actRulenangalpdfHeadings','get_management_headings','get_management_headings.getsubheading')->get();    
            return view('frontend.footer.actRule.acts_rule_detail',compact('act_rule_list'));
        }

        public function calendarDetail(){
           
            
           return view('demo');
        }

        public function getEventsDetails(){

      
    

            $event = WorldDayDetail::where('deleted_status','=','0')->get();
             // $data=[];
             // foreach($event as $events){
             //    $subArr=[
             //        'id'=> $events->id,
             //        'type_name'=> $events->type_name,
             //        'date_of_event' => $events->date_of_event
             //    ];
             //    array_push($data,$subArr);
             // }
             // return $data;
        //     foreach($lastTenDates as $date_value ){
        //     $d=array();
        //      // $d['date']=date('l',strtotime($date_value));
        //      $d['type_name'] = WorldDayDetail::where('deleted_status','=','0')->get('type_name');
        //      $d['date_of_event']=WorldDayDetail::whereDate('date_of_event',date('Y-m-d'))->get();
        //      $dd[]=$d;
        //     }
        // return $dd;
   
             foreach($event as $events ){
                $data=array();
             $data['type_name'] = $events->type_name;
             $data['date_of_event'] = $events->date_of_event;
             $dd[]=$data;
        }
        return $dd;
             

        }


        public function indexCalendardetail(Request $request)
        {
            if($request->ajax())
            {
                $data =  WorldDayDetail::whereDate('date_of_event', '>=', $request->start)->where('deleted_status','=','0')->get('date_of_event');
                return response()->json($data);
            }
            return view('frontend.calendar.calendar');
        }

    public function action(Request $request)
    {
        if($request->ajax())
        {
            if($request->type == 'add')
            {
                $event = WorldDayDetail::create([
                    'type_name'     =>  $request->title,
                    'date_of_event'     =>  $request->start
                ]);

                return response()->json($event);
            }

            if($request->type == 'update')
            {
                $event = WorldDayDetail::find($request->id)->update([
                    'type_name'     =>  $request->title,
                    'date_of_event'     =>  $request->start
                ]);

                return response()->json($event);
            }

            if($request->type == 'delete')
            {
                $event = WorldDayDetail::find($request->id)->delete();

                return response()->json($event);
            }
        }
    }

    public function view_all_videos(){

         $zoogallerybanner_detail = ZooGalleryBanner::where('deleted_status','0')->latest()->first();
        $zoo = ZooType::all();
        $protected_area = ProtectedAreaSubcategory::get();
       $gallery_list = Video::all()->take(12);
        $gallery_count = ZooGallery::count();
        $protected_wetland = ProtectedWetland::get();
        return view('frontend.all_videos',compact('zoogallerybanner_detail','zoo','gallery_list','gallery_count','protected_area','protected_wetland'));

       
    }

    public function getAllVideos(Request $request){

        // return $request->all();
        if($request->zoo_gallery =="0"){
            $gallery_list = Video::get();
        }
        else{
            $gallery_list = Video::where('type',$request->zoo_gallery)->pluck('video_link');
        }
       
        return response()->json($gallery_list);
    }

    public function notFound(){

        return view('not_found');
    }

    public function demoPage(){
        return view('demo');

    }

    public function wetlandAuthority(){

        $list = WetlandAuthority::with('get_notification','get_rules','get_notification')->latest()->first();
    // return $list;
        return view('frontend.wetland_authority',compact('list'));

    }

    // 05/02/2022
    public function zooDetailList($id){

     	$zoo_detail_list = FrontendZooDetail::with('get_zoo_name')->where('type_id',$id)->latest()->first();
      	$zoo_images = ZooGallery::where('zoo_type_id',$id)->where('deleted_status','0')->take(12)->get();
      
       	$video = Video::where('type',$id)->first();
       	$inventory_list = ZooInventory::where('zoo_type_id',$id)->get();
     	$inventory_date = ZooInventory::where('zoo_type_id',$id)->take(1)->latest()->first();
        return view('frontend.zoo.zoo_detail',compact('zoo_detail_list','id','zoo_images','video','inventory_list','inventory_date'));
    }

    public function protectedwetlandDetail($id){

       $wetland_list = FrontendProtectedWetlandDetail::with('get_type_name','get_detail')->where('type_id',$id)->latest()->first();
       $video = Video::where('type',$id)->first();
       $images = FrontendWetlandImages::where('wetland_id',$id)->take(12)->get();
        return view('frontend.protectedWetland.detail',compact('wetland_list','video','images','id'));
    }

    public function protectedAreaDetail($id){

       $detail = FrontendProtectedArea::with('get_cat','get_subcat','get_notification')->where('subcategory_id',$id)->latest()->first();
       $video = Video::where('type',$id)->first();
       $images = FrontendProtectedAreaImages::where('subcat_id',$id)->take(12)->get();
        return view('frontend.protectedArea.detail',compact('detail','video','images','id'));
    }

    public function coffeeTableBook(){

        return view('frontend.coofee-book');
    }

    public function addhowtoapply(){
             $how_to_apply_type = HowApplyType::all();
          $district_list= District::all();
        return view('frontend.how_to_apply',compact('district_list','how_to_apply_type'));
    }

    public function savehowtoapply(Request $request){

        $validated = $request->validate([
        'type_id' => 'required',
        'description' => 'required',
        'email' => 'required',
        'mobile_number' => 'required|digits:10',
        'district_id' => 'required',
        ]);

        $type_id = $request->type_id;
        $description = $request->input('description');
        $email = $request->input('email');
        $mobile_number = $request->input('mobile_number');
        $district_id = $request->input('district_id');

       $type_name =  HowApplyType::where('id',$type_id)->first();
       $t_name = $type_name->type_name;

        $district =  District::where('id',$district_id)->first();
        $d_name = $district->district_name;

        
            if(HowToApplyDetail::where('email',$email)->first()){
                 return redirect()->back()->with('fail','This email id already exist...');
            }else{
                 $save = new HowToApplyDetail();
                $save->type_id = $type_id;
                $save->description = $description;
                $save->district_id = $district_id;
                $save->district_name =  $d_name;
                $save->email = $email;
                $save->mobile_number = $mobile_number;
                $save->save();
                
            }
        

        $html = '<h2> Hi, </br></h2><b>Volunteer Email:</b> '.$request->input('email').'</br><p><b>Mobile Number:</b>&nbsp;&nbsp;'.$request->input('mobile_number').'</p></br><p><b>Wildlife Type:</b>&nbsp;&nbsp;'.$type_name->type_name.'</p></br><p><b> District:</b> &nbsp;&nbsp;'.$district->district_name.'</p></br>'.'<p><b>Division:</b> &nbsp;&nbsp;'.$request->input('description').'</p>';
    

        // $this->send_grid_email_v4('cwlwpunjab@gmail.com',"",'Application for wildlife volunteer.',$html);

        return redirect()->back()->with('success','Your application sent successfully. You will receive response from wildlife department shortly...');
    }

    public function zooviewAllImage($id){

       $images = ZooGallery::where('zoo_type_id',$id)->where('deleted_status','0')->get();
        return view('frontend.zoo.zooviewAllImage',compact('images'));
    }

    public function protectedwetlandviewAllImage($id){

        $images = FrontendWetlandImages::where('wetland_id',$id)->get();
        return view('frontend.protectedWetland.protectedwetlandimages',compact('images'));
    }

    public function protectedareaviewAllImage($id){

        $images = FrontendProtectedAreaImages::where('subcat_id',$id)->get();
        return view('frontend.protectedArea.protectedareaimages',compact('images'));
    }
    // 05/02/2022
    
		
// end class 
}