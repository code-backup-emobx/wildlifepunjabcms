<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Session;
use Validator;
use App\Models\AllUsers;
use App\Models\ApplicantLogin;
use App\Models\User;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Cookie;
use Auth;
use DB;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    
   
   public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showlogin(){
    
       return view('auth.login');
    // return redirect('/index');
    }

    public function login(Request $request){
         // return bcrypt('Welcome');
         
		// return request->all();die();
        $validator = Validator::make($request->all(), [
                    
              'email' => 'required',
                'password' => 'required',
                 'captcha' => 'required|captcha'
                ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
    //return $request->all();
        $email = $request->email;
      $password = Crypt::decrypt($request->input('password'));
     //  $password = $request->password;
		 $user = \DB::table('users')->where('email', $request->input('email'))->first();
        if (\Auth::attempt(['email' => $email, 'password' => $password, 'status'=> '1'])) {
        $new_sessid   = \Session::getId(); //get new session_id after user sign in

            if($user->session_id != '') {
                $last_session = \Session::getHandler()->read($user->session_id); 

                if ($last_session) {
                    if (\Session::getHandler()->destroy($user->session_id)) {
                        
                    }
                }
            }

            \DB::table('users')->where('id', $user->id)->update(['session_id' => $new_sessid]);
            
            $user = auth()->guard('web')->user();
            // Authentication passed...
            return redirect()->intended('/home')->with('success','Welcome Back');
        }else{
            return redirect()->back()->with('fail','!Enter the valid email and password');
        }
    }
	
	public function hashPassword(Request $request){
    	 return Crypt::encrypt($request->password);
     // return $password = $request-input('password');die();
     //  $x = Crypt::encrypt($password);
   
    
    }


	public function logout(Request $request)

{

   $request->session()->flush();


   $request->session()->regenerate();

   return redirect('/login');
}


public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
 
//end class
}

?>