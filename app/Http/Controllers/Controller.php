<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    function call_api($method,$req){
    $client = new \GuzzleHttp\Client();
    $url = env('APP_URL')."api/".$method;
   
    $myBody = $req;
   
    $response = $client->post($url,  ['form_params'=>$myBody]);
    // $response = $request1->json();
    $result_array = json_decode((string) $response->getBody());

    $cont = $result_array;
    return $cont;

}
}
