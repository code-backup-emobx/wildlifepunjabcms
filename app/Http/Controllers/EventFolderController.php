<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\EventMaster;
use App\Models\EventMasterGallery;
use App\Models\GalleryImages;
use App\Models\GalleryAllImages;
use Image;

class EventFolderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function event_master_list(){

	  $listing = EventMaster::paginate(10);
		return view('eventmaster.listing',compact('listing'));
	}
	
	public function add_master_list(){
    
    	return view('eventmaster.add');
    }

	public function save_master_list(Request $request){
    
    	$rules = [
        'name' => 'required',
    ];

    $customMessages = [
        'regex' => 'Please enter the name of event.'
    ];

    $this->validate($request, $rules, $customMessages);
    
    	$name = $request->input('name');
    
    	$save_event_master = new EventMaster();
    	$save_event_master->name = $name;
    	$save_event_master->save();
    return redirect('/event-master-list')->with('success','Event Master Detail Added Successfully');
    }


	public function edit_master_list($id){
    	$edit = EventMaster::where('id',$id)->first();
		return view('eventmaster.edit',compact('edit'));
    }

	public function update_master_list(Request $request , $id){
    	$edit = EventMaster::where('id',$id)->first();
			$name = $request->input('name');
    
    	$save_event_master = EventMaster::find($id);
    	$save_event_master->name = $name;
    	$save_event_master->save();
    return redirect('/event-master-list')->with('success','Event Master Detail Updated Successfully');
    }
	
	public function gallery_list(){
       	$event_master = EventMaster::get();
		foreach($event_master as $e){

			$e->total_count = EventMasterGallery::where('event_master_id', $e->id)->count();
		}

		
		return view('eventmaster.gallery_list',compact('event_master'));
    }

	public function add_event_gallery(){
    	$eventmaster = EventMaster::get();
    	return view('eventmaster.add_gallery',compact('eventmaster'));
    } 

	public function save_event_gallery(Request $request){
    
    	$rules = [
        'event_master_id' => 'required',
        'image' => 'required|max:2048',
    ];

    $customMessages = [
        'regex' => 'Please Choose the event type.'
    ];

    $this->validate($request, $rules, $customMessages);
    
    	$event_master_id = $request->input('event_master_id');
    	 $image = $request->file('image');

         // $folder_path = '/images';
        $destinationPath = public_path('/event_master');

        $image = $request->file('image');

        $img_name = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path($folder_path);

        $img = Image::make($image->getRealPath());
        $img->save($destinationPath.'/'.$img_name);

        //resize image
        $img->resize(288,192)->save($destinationPath.'/'.$img_name);
        //resize thumb image
        // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

        $input['img'] = $img_name;
    	
    	$event_name = EventMaster::where('id',$event_master_id)->first();
    	$name = $event_name->name;
    
        $save_image = new EventMasterGallery();
        $save_image->image = '/event_master/'.$input['img'];
        $save_image->event_master_id = $event_master_id;
        $save_image->event_master_name = $name;
        $save_image->save();
		
    	
    
    		$check = GalleryImages::where('type_id',$event_master_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$event_master_id)->update(['image' => $save_image->image]);
        
        }else{
        	$save_gallery_images = new GalleryImages();
       		$save_gallery_images->type_id = $event_master_id;
       		$save_gallery_images->type_name = $name;
       		$save_gallery_images->image = '/event_master/'.$input['img'];
        	$save_gallery_images->save();
        	
        }
    	
        $saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_image->id;
    	$saveall_gallery_image->type_id = $event_master_id;
    	$saveall_gallery_image->image = '/event_master/'.$input['img'];
    	$saveall_gallery_image->save();
    
    	
    return redirect('/event-gallery-images-list')->with('success','Event Gallery Added Successfully');
    
    }

	public function event_image_detail($id){
    	
    	$list = EventMasterGallery::where('event_master_id',$id)->paginate(10);
    	return view('eventmaster.images_list',compact('list','id'));
    }
		
	public function add_event_gallery_image($id){
    	
    	return view('eventmaster.add_event_image',compact('id'));
    
    }
	
	public function save_event_gallery_image(Request $request , $id){
    
    	$rules = [
        'image' => 'required|max:2048',
    ];

    $customMessages = [
        'regex' => 'Please Choose the event type.'
    ];

    $this->validate($request, $rules, $customMessages);
    
    	
    	 $image = $request->file('image');

         // $folder_path = '/images';
        $destinationPath = public_path('/event_master');

        $image = $request->file('image');

        $img_name = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path($folder_path);

        $img = Image::make($image->getRealPath());
        $img->save($destinationPath.'/'.$img_name);

        //resize image
        $img->resize(288,192)->save($destinationPath.'/'.$img_name);
        //resize thumb image
        // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

        $input['img'] = $img_name;
    	
    	$event_name = EventMaster::where('id',$id)->first();
    	$name = $event_name->name;
    
        $save_image = new EventMasterGallery();
        $save_image->image = '/event_master/'.$input['img'];
        $save_image->event_master_id = $id;
        $save_image->event_master_name = $name;
        $save_image->save();
		
    	
    
    		$check = GalleryImages::where('type_id',$id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$id)->update(['image' => $save_image->image]);
        
        }else{
        	$save_gallery_images = new GalleryImages();
       		$save_gallery_images->type_id = $id;
       		$save_gallery_images->type_name = $name;
       		$save_gallery_images->image = '/event_master/'.$input['img'];
        	$save_gallery_images->save();
        	
        }
    	
        $saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_image->id;
    	$saveall_gallery_image->type_id = $id;
    	$saveall_gallery_image->image = '/event_master/'.$input['img'];
    	$saveall_gallery_image->save();
    
    	
    	return redirect('/event_image_detail/'.$id)->with('success','Event Gallery Image Added Successfully');
    
    }

	public function edit_event_gallery_detail($id , $event_id){
    	
    	$edit_image = EventMasterGallery::where('id',$id)->first();
        return view('eventmaster.edit_event_gallery_image',compact('edit_image','id','event_id'));
    }
	
	public function update_event_gallery_detail(Request $request , $id , $event_id){
    
    			$rules = [
        'image' => 'required|max:2048',
    ];

    $customMessages = [
        'regex' => 'Please Choose the event type.'
    ];

    $this->validate($request, $rules, $customMessages);
    
    	
    	 $image = $request->file('image');

         // $folder_path = '/images';
        $destinationPath = public_path('/event_master');

        $image = $request->file('image');

        $img_name = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path($folder_path);

        $img = Image::make($image->getRealPath());
        $img->save($destinationPath.'/'.$img_name);

        //resize image
        $img->resize(288,192)->save($destinationPath.'/'.$img_name);
        //resize thumb image
        // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

        $input['img'] = $img_name;
    	
    	$event_name = EventMaster::where('id',$event_id)->first();
    	$name = $event_name->name;
    
        $save_image = EventMasterGallery::find($id);
        $save_image->image = '/event_master/'.$input['img'];
        $save_image->event_master_id = $event_id;
        $save_image->event_master_name = $name;
        $save_image->save();
		
    	
    
    		$check = GalleryImages::where('type_id',$event_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$event_id)->update(['image' => $save_image->image]);
        
        }else{
        	$save_gallery_images = new GalleryImages();
       		$save_gallery_images->type_id = $event_id;
       		$save_gallery_images->type_name = $name;
       		$save_gallery_images->image = '/event_master/'.$input['img'];
        	$save_gallery_images->save();
        	
        }
    	
        $saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_image->id;
    	$saveall_gallery_image->type_id = $event_id;
    	$saveall_gallery_image->image = '/event_master/'.$input['img'];
    	$saveall_gallery_image->save();
    
    	
    	return redirect('/event_image_detail/'.$event_id)->with('success','Event Gallery Image Added Successfully');
    
    }

	public function delete_event_gallery_detail($id ,$event_id){
    
    	$delete = EventMasterGallery::where('id',$id)->delete();
    	
    	GalleryAllImages::where('table_id',$id)->where('type_id',$event_id)->delete();
    
    	$get_image = EventMasterGallery::where('event_master_id',$event_id)->take(1)->latest()->first();
    		if($get_image){
            		$check = GalleryImages::where('type_id',$event_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$event_id)->update(['image' => $get_image->image]);
        
        }
            
            }else{
            
        	 GalleryImages::where('type_id',$event_id)->delete();
            }
   
      
    		
    
    		return redirect('/event_image_detail/'.$event_id)->with('success','Gallery Image Delete successfully');
    
    }

// end class 
}