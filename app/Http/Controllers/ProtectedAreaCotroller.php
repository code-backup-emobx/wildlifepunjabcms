<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\ProtectedAreaCategory;
use App\Models\ProtectedAreaSubcategory;
use App\Models\FrontendProtectedArea;
use App\Models\FrontendProtectedAreaNotification;
use App\Models\FrontendProtectedAreaImages;
use App\Models\GalleryImages;
use App\Models\GalleryAllImages;
use Image;

class ProtectedAreaCotroller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function protAreaCateogoryList(){

    	$list = ProtectedAreaCategory::paginate(10);
    	return view('protectedArea.category.list',compact('list'));
    }

    public function addprotAreaCateogory(){

		return view('protectedArea.category.add');    	
    }
    public function saveaddprotAreaCateogory(Request $request){
    	// return $request->all();
    		$rules = [
        	'category_name' => ['required', 'regex:/^[\.a-zA-Z0-9, ]*$/'],
    ];

    $customMessages = [
        'regex' => 'Please enter the category name in letters.'
    ];

    $this->validate($request, $rules, $customMessages);
    	


    	$category_name = $request->input('category_name');
    	$save_category = new ProtectedAreaCategory();
    	$save_category->category_name = $category_name;
    	$save_category->save();

    	return redirect('/prot-area-category-list')->with('success','Protected Area Category Name Added Successfully');
    }
    public function editprotAreaCateogory($id){

    	$edit = ProtectedAreaCategory::where('id',$id)->first();
    	return view('protectedArea.category.edit',compact('edit','id')); 
    }
    public function updateprotAreaCateogory(Request $request , $id){
    	// return $request->all();
    	$category_name = $request->input('category_name');
    	$update_category = ProtectedAreaCategory::find($id);
    	$update_category->category_name = $category_name;
    	$update_category->save();
    	return redirect('/prot-area-category-list')->with('success','Protected Area Category Name Updated Successfully');
    }
    public function deleteprotAreaCateogory($id){

    	ProtectedAreaCategory::where('id',$id)->delete();
    	ProtectedAreaSubcategory::where('category_id',$id)->delete();
    	FrontendProtectedArea::where('subcategory_id',$id)->delete();
		FrontendProtectedAreaNotification::where('subcat_id',$id)->delete();
		FrontendProtectedAreaImages::where('subcat_id',$id)->delete();
		GalleryImages::where('type_id',$id)->delete();
		GalleryAllImages::where('type_id',$id)->delete();
    	
    	return redirect('/prot-area-category-list')->with('success','Protected Area Category Name Deleted Successfully');
    }

    public function protAreaSubCateogoryList(){

    	$list = ProtectedAreaSubcategory::with('get_cat')->orderBy('id','DESC')->paginate(10);
    	return view('protectedArea.subcategory.list',compact('list'));
    }
    public function addprotAreaSubCateogory(){
    	$category_list = ProtectedAreaCategory::get();
    	return view('protectedArea.subcategory.add',compact('category_list'));
    }
    public function saveaddprotAreaSubCateogory(Request $request){

    	// return $request->all();
		$rules = [
    	'category_id' => ['required'],
    	'subcat_name' => ['required', 'regex:/^[\.a-zA-Z0-9, ]*$/'],
    ];

    $customMessages = [
    	'category_id.required' => 'Please choose the category',
    	'subcat_name.required' => 'Please enter the sub category',
        'regex' => 'Please enter the sub category name in letters.',

    ];

    $this->validate($request, $rules, $customMessages);
    	

        $category_id = $request->category_id;
    	$subcat_name = $request->input('subcat_name');
    	$save_subcategory = new ProtectedAreaSubcategory();
    	$save_subcategory->category_id = $category_id;
    	$save_subcategory->name = $subcat_name;
    	$save_subcategory->save();

    	return redirect('/prot-area-subcategory-list')->with('success','Protected Area Sub Category Name Added Successfully');
    }

    public function editprotAreaSubCateogory($id){

    	$category_list = ProtectedAreaCategory::get();
    	$edit_subcat = ProtectedAreaSubcategory::with('get_cat')->where('id',$id)->first();
    	return view('protectedArea.subcategory.edit',compact('edit_subcat','category_list','id'));
    }

     public function updateprotAreaSubCateogory(Request $request,$id){

    	
        $category_id = $request->category_id;
    	$subcat_name = $request->input('subcat_name');
    	$save_subcategory = ProtectedAreaSubcategory::find($id);
    	$save_subcategory->category_id = $category_id;
    	$save_subcategory->name = $subcat_name;
    	$save_subcategory->save();

    	return redirect('/prot-area-subcategory-list')->with('success','Protected Area Sub Category Name Updated Successfully');
    }

    public function deleteprotAreaSubCateogory($id){

    	ProtectedAreaSubcategory::where('id',$id)->delete();
    
    	FrontendProtectedArea::where('subcategory_id',$id)->delete();
		FrontendProtectedAreaNotification::where('subcat_id',$id)->delete();
		FrontendProtectedAreaImages::where('subcat_id',$id)->delete();
		GalleryImages::where('type_id',$id)->delete();
		GalleryAllImages::where('type_id',$id)->delete();
    	
    	return redirect('/prot-area-subcategory-list')->with('success','Protected Area Sub Category Name Deleted Successfully');
    }

    public function protAreaList(){

    	$list = FrontendProtectedArea::with('get_cat','get_subcat')->paginate(10);
    	foreach($list as $l_count){

    		$l_count->pdf_count = FrontendProtectedAreaNotification::where('front_protarea_id',$l_count->id)->count();
    	}
    	// return $list;
    	return view('protectedArea.list',compact('list'));
    }
    public function addProtArea(){

    	$category_list = ProtectedAreaCategory::get();
    	return view('protectedArea.add',compact('category_list'));
    }


    public function saveProtArea(Request $request){

    	// return $request->all();
		$validated = $request->validate([
			'banner_image' => 'required|max:2048',
	    	'category_id' => 'required',
	    	'sub_category_id' => 'required',
	    	'district' => 'required',
	    	'location' => 'required',
	    	'area' => 'required',
	    	'status_of_land' => 'required',
	    	'map_lat' => 'required|numeric',
	    	'map_long' => 'required|numeric',
	    	'zoom_level' => 'required|numeric',
	    	'description' => 'required',
	    	'important_fauna' => 'required',
	    	'important_flora' => 'required',
	    	'title' => 'required',
	    	'pdf_file' => 'required|max:2048',
        ]);

    	
    	$banner_image = $request->file('banner_image');
		$category_id = $request->category_id;
		$sub_category_id = $request->sub_category_id;
		$district = $request->input('district');
		$location = $request->input('location');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$map_lat = $request->input('map_lat');
		$map_long = $request->input('map_long');
		$zoom_level = $request->input('zoom_level');
		$description = $request->input('description');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');
		$title = $request->input('title');
		$pdf_file = $request->file('pdf_file');

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/protected_area');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/protected_area/' . $filename;
		}

		$save_detail = new FrontendProtectedArea();
		$save_detail->banner_image = $banner_image;
		$save_detail->description = $description;
		$save_detail->category_id = $category_id;
		$save_detail->subcategory_id = $sub_category_id;
		$save_detail->district = $district;
		$save_detail->location = $location;
		$save_detail->area = $area;
		$save_detail->status_of_land = $status_of_land;
		$save_detail->imp_fauna = $important_fauna;
		$save_detail->imp_flora = $important_flora;
		$save_detail->map_lat = $map_lat;
		$save_detail->map_long = $map_long;
		$save_detail->zoom_level = $zoom_level;
		$save_detail->save();

		$id = $save_detail->id;

		 $image_data=array();
		if($request->hasFile('pdf_file') != "")
        {
            foreach($request->file('pdf_file') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = time() . '.' . $file->getClientOriginalName();
				$file->move(public_path() . '/protected_area/', $image);
				$image_data[] = '/protected_area/'.$image;
            }
		}
		for($i=0;$i<count($image_data);$i++)
        {
			$save_subheading = new FrontendProtectedAreaNotification();
			$save_subheading->front_protarea_id = $id;
			$save_subheading->subcat_id = $sub_category_id;
			$save_subheading->title = $title[$i] ;
			$save_subheading->pdf_file = $image_data[$i] ;
			$save_subheading->save();
	    }

		return redirect('/prot-area-list')->with('success','Protected Area Detail Added Successfully');
    }
    public function editProtArea($id){
    	 $category = ProtectedAreaCategory::get();
    	 $subcategory = ProtectedAreaSubcategory::get();
    	$edit_protectedareadetail = FrontendProtectedArea::with('get_cat','get_subcat')->where('id',$id)->first();
    	return view('protectedArea.edit',compact('edit_protectedareadetail','category','subcategory','id'));
    }
    public function updateProtArea(Request $request , $id){
    	// return $request->all();
    	$banner_image = $request->file('banner_image');
		$category_id = $request->category_id;
		$sub_category_id = $request->sub_category_id;
		$district = $request->input('district');
		$location = $request->input('location');
		$area = $request->input('area');
		$status_of_land = $request->input('status_of_land');
		$map_lat = $request->input('map_lat');
		$map_long = $request->input('map_long');
		$zoom_level = $request->input('zoom_level');
		$description = $request->input('description');
		$important_fauna = $request->input('important_fauna');
		$important_flora = $request->input('important_flora');

		$update_detail = FrontendProtectedArea::find($id);

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/protected_area');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/protected_area/' . $filename;

			$update_detail->banner_image = $banner_image;
		}

		$update_detail->description = $description;
		$update_detail->category_id = $category_id;
		$update_detail->subcategory_id = $sub_category_id;
		$update_detail->district = $district;
		$update_detail->location = $location;
		$update_detail->area = $area;
		$update_detail->status_of_land = $status_of_land;
		$update_detail->imp_fauna = $important_fauna;
		$update_detail->imp_flora = $important_flora;
		$update_detail->zoom_level = $zoom_level;
		$update_detail->map_lat = $map_lat;
		$update_detail->map_long = $map_long;
		$update_detail->save();
		return redirect('/prot-area-list')->with('success','Protected Area Detail Updated Successfully');
    }

    public function deleteProtArea($id){

    	FrontendProtectedArea::where('id',$id)->delete();
    	FrontendProtectedAreaNotification::where('front_protarea_id',$id)->delete();
    	return redirect('/prot-area-list')->with('success','Protected Area Detail Deleted Successfully');
    }

    public function getsubcategory(Request $request){

    	$list_subcat = ProtectedAreaSubcategory::where('category_id',$request->category_id)->pluck('id','name');
    	return response()->json($list_subcat);
    }

    public function viewProtArea($id){

    	$view = FrontendProtectedArea::with('get_cat','get_subcat','get_notification')->where('id',$id)->first();
    	return view('protectedArea.view',compact('view','id'));
    }

    public function protAreaNotiList($id,$subcat_id){

    	$noti_list = FrontendProtectedAreaNotification::where('front_protarea_id',$id)->paginate(5);
    	return view('protectedArea.noti-list',compact('noti_list','id','subcat_id'));
    }

    public function addProtAreaNoti($id,$subcat_id){

		return view('protectedArea.noti-add',compact('id','subcat_id'));    	
    }

    public function saveProtAreaNoti(Request $request , $id , $subcat_id){

    	$validated = $request->validate([
			'pdf_file' => 'required|max:2048',
	    	'title' => 'required',
        ]);
 
    	$title = $request->input('title');
    	$pdf_file = $request->file('pdf_file');

    	if($request->hasFile('pdf_file') != ""){
        	$filename =$pdf_file->getClientOriginalName();
            $destinationPath = public_path('/protected_area');
            $pdf_file->move($destinationPath, $filename);
            $pdf_file = '/protected_area/' . $filename;

		}

    	$save_noti = new FrontendProtectedAreaNotification();
    	$save_noti->title = $title;
        $save_noti->pdf_file = $pdf_file;
    	$save_noti->subcat_id = $subcat_id;
    	$save_noti->front_protarea_id = $id;
    	$save_noti->save();

    	return redirect('/prot-area-noti-list/'.$id.'/'.$subcat_id)->with('success','Notification Detail Added Successfully');
    }

    public function deleteProtAreaNoti($id){
    	FrontendProtectedAreaNotification::where('id',$id)->delete();
    	return redirect()->back()->with('success','Notification Detail Deleted Successfully');
    }

    public function editProtAreaNoti($id,$p_id,$subcat_id){

    	$edit_noti = FrontendProtectedAreaNotification::where('id',$id)->first();
    	return view('protectedArea.noti-edit',compact('edit_noti','id','p_id','subcat_id'));    
    }

    public function updateProtAreaNoti(Request $request,$id,$p_id,$subcat_id){

    	$title = $request->input('title');
    	$pdf_file = $request->file('pdf_file');

    	$update_noti = FrontendProtectedAreaNotification::find($id);
    	if($request->hasFile('pdf_file') != ""){
        	$filename =$pdf_file->getClientOriginalName();
            $destinationPath = public_path('/protected_area');
            $pdf_file->move($destinationPath, $filename);
            $pdf_file = '/protected_area/' . $filename;
    		
    		$update_noti->pdf_file = $pdf_file;

		}

    	$update_noti->title = $title;
    	$update_noti->save();

    	return redirect('/prot-area-noti-list/'.$p_id.'/'.$subcat_id)->with('success','Notification Detail Updated Successfully');
    }


    public function Imageslist(){

        $list = FrontendProtectedAreaImages::with('get_subcat_name')->paginate(10);
       return view('protectedArea.images.list',compact('list'));

    }

    public function addImages(){
        $subcatlist = ProtectedAreaSubcategory::get();
        return view('protectedArea.images.add',compact('subcatlist'));
    }
    public function saveImages(Request $request){

        $validated = $request->validate([
        'image' => 'required|max:2048',
        'subcat_id' => 'required',
        ]);
        
        // return $request->all();
        $subcat_id = $request->subcat_id;
        $image = $request->file('image');
	
    if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/protected_area');
            $image->move($destinationPath, $filename);
            $image = '/protected_area/' . $filename;
    		
    		

		}
         // $folder_path = '/images';
        //// $destinationPath = public_path('/protected_area');

       //// $image = $request->file('image');

        //// $img_name = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path($folder_path);

        //// $img = Image::make($image->getRealPath());
       //// $img->save($destinationPath.'/'.$img_name);

        //resize image
      ////  $img->resize(288,192)->save($destinationPath.'/'.$img_name);
        //resize thumb image
        // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

       //// $input['img'] = $img_name;

        $save_image = new FrontendProtectedAreaImages();
        $save_image->image = $image;
        $save_image->subcat_id = $subcat_id;
        $save_image->save();
		
    	// $save_gallery_images = new GalleryImages();
    	// $save_gallery_images->type_id = $subcat_id;
    	// $save_gallery_images->image = '/protected_area/'.$input['img'];
    	// $save_gallery_images->save();
      $subcat_name = ProtectedAreaSubcategory::where('id',$subcat_id)->first();
    	$subcatname = $subcat_name->name;
    		$check = GalleryImages::where('type_id',$subcat_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$subcat_id)->update(['image' => $save_image->image]);
        
        }else{
        	$save_gallery_images = new GalleryImages();
       		$save_gallery_images->type_id = $subcat_id;
       		$save_gallery_images->type_name = $subcatname;
       		$save_gallery_images->image = $image;
        	$save_gallery_images->save();
        	
        }
    	
        $saveall_gallery_image = new GalleryAllImages();
    	$saveall_gallery_image->table_id = $save_image->id;
    	$saveall_gallery_image->type_id = $subcat_id;
    	$saveall_gallery_image->image = $image;
    	$saveall_gallery_image->save();

    
        return redirect('/prot-area-images-list')->with('success','Protected Area Image Uploaded Successfully');    
    }

    public function editImages($id){

        $edit = FrontendProtectedAreaImages::where('id',$id)->first();
        $subcatlist = ProtectedAreaSubcategory::get();
        return view('protectedArea.images.edit',compact('subcatlist','edit','id'));
    }

    public function updateImages(Request $request ,$id){
		// return $id;
        // return $request->all();
         $validated = $request->validate([
        'image' => 'max:2048',
        ]);
        
        $subcat_id = $request->subcat_id;
        $image = $request->file('image');
         $update_image = FrontendProtectedAreaImages::find($id);
//       if($request->hasFile('image') != ""){
//          // $folder_path = '/images';
        
//         $destinationPath = public_path('/protected_area');

//         $image = $request->file('image');

//         $img_name = time().'.'.$image->getClientOriginalExtension();
//         // $destinationPath = public_path($folder_path);

//         $img = Image::make($image->getRealPath());
//         $img->save($destinationPath.'/'.$img_name);

//         //resize image
//         $img->resize(288,192)->save($destinationPath.'/'.$img_name);
//         //resize thumb image
//         // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

//         $input['img'] = $img_name;
//         $update_image->image = '/protected_area/'.$input['img'];
      
      	
      
      
//     }
        if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/protected_area');
            $image->move($destinationPath, $filename);
            $image = '/protected_area/' . $filename;
    		
    		  $update_image->image = $image;

		}
        $update_image->subcat_id = $subcat_id;
        $update_image->save();
    // return $update_image;
    
    	$save_gallery_images = GalleryImages::where('type_id',$update_image->subcat_id)->update(['image'=> $update_image->image]);
    
    	 $saveall_gallery_image = GalleryAllImages::where('type_id',$update_image->subcat_id)->where('table_id',$update_image->id)->update(['image'=> $update_image->image]);
    	// $saveall_gallery_image->table_id = $save_image->id;
    	// $saveall_gallery_image->type_id = $subcat_id;
    	// $saveall_gallery_image->image = '/protected_area/'.$input['img'];
    	// $saveall_gallery_image->save();

        return redirect('/prot-area-images-list')->with('success','Protected Area Image Updated Successfully');    
    }

    public function deleteImages($id,$subcat_id){

        FrontendProtectedAreaImages::where('id',$id)->delete();
    	
    	GalleryAllImages::where('table_id',$id)->where('type_id',$subcat_id)->delete();
    
    	$get_image = FrontendProtectedAreaImages::where('subcat_id',$subcat_id)->take(1)->latest()->first();
    		if($get_image){
            		$check = GalleryImages::where('type_id',$subcat_id)->first();
    	if($check){
        	
        	$update_gallery_image = GalleryImages::where('type_id',$subcat_id)->update(['image' => $get_image->image]);
        
        }
            
            }else{
            
        	 GalleryImages::where('type_id',$subcat_id)->delete();
            }
   
    
        return redirect('/prot-area-images-list')->with('success','Protected Area Image Deleted Successfully');  
    }
// end clss
}