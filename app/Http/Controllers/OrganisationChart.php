<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\OrganisationTabs;
use App\Models\OrganisationBannerDetail;
use App\Models\OrganisationTableDetail;
use App\Models\OrganisationMaster;


class OrganisationChart extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function organisationList(){

		$org_detail = OrganisationBannerDetail::with('get_tab_detail','get_tab_detail.get_tab_name')->where('deleted_status','0')->paginate(10);

	    // return $data;
		return view('footer.organisation.listing',compact('org_detail'));
	}

	public function addorganisation(){

		$organisation_tab_detail = OrganisationTabs::all();

		return view('footer.organisation.add',compact('organisation_tab_detail'));
	}

	public function saveorganisation(Request $request){

		// return $request->all();
		// $validated = $request->validate([
		// 'banner_image' => 'required',
		// 'banner_heading' => 'required',
		// 'tab_id' => 'required',
		// 'chart_heading' => 'required',
		// 'designation_of_the_officer' => 'required',
		// 'name_of_the_officer' => 'required',
		// 'range' => 'required',
		// 'block' => 'required',
		// 'beat' => 'required',
		// 'territorial_range_beat_block' => 'required',
		// 'officer_mobile_no' => 'required',
		// ]);

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');

		$tab_id = $request->tab_id;
        $chart_heading = $request->input('chart_heading');
        $designation_of_the_officer = $request->input('designation_of_the_officer');
        $name_of_the_officer = $request->input('name_of_the_officer');
        $range = $request->input('range');
        $block = $request->input('block');
        $beat = $request->input('beat');
        $territorial_range_beat_block = $request->input('territorial_range_beat_block');
        $officer_mobile_no = $request->input('officer_mobile_no');
		
    	
       

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/organisation_chart');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/organisation_chart/' . $filename;
		}
    
    	

		$save_banner_detail = new OrganisationBannerDetail();
		$save_banner_detail->banner_image = $banner_image;
		$save_banner_detail->banner_heading = $banner_heading;
		$save_banner_detail->save();
		$b_id = $save_banner_detail->id;

		 $tab_name = OrganisationTabs::where('id',$tab_id)->first();
		 $t_name = $tab_name->tab_name;
		 $save_table_detail = new OrganisationTableDetail();
		$save_table_detail->banner_id = $b_id;
		$save_table_detail->tab_id = $tab_id;
		$save_table_detail->chart_heading = $t_name ;
		$save_table_detail->designation_of_the_officer =  $designation_of_the_officer ;
		$save_table_detail->name_of_the_officer = $name_of_the_officer;
		$save_table_detail->range = $range;
		$save_table_detail->block = $block ;
		$save_table_detail->beat = $beat ;
		$save_table_detail->territorial_range_beat_block = $territorial_range_beat_block ;
		$save_table_detail->officer_mobile_no = $officer_mobile_no ;
		$save_table_detail->save();

		return redirect('/organisation_list')->with('success','Organisation Chart Detail Added Successfully');
	}

	public function addorganisationDetail($id){

		$detail_id = OrganisationBannerDetail::where('id',$id)->first();
		$organisation_tab_detail = OrganisationTabs::all();
		return view('footer.organisation.add_org_table',compact('organisation_tab_detail','detail_id','id'));
	}

	public function viewDetail($id){

		$tab_details = OrganisationTableDetail::with('get_tab_name')->where('banner_id',$id)->where('deleted_status','0')->paginate(10);
		
		// return $tab_details;
		return view('footer.organisation.tab_listing',compact('tab_details','id'));
	}

	public function deletetabDetail($id){

		$delete_tab_detail = OrganisationTableDetail::where('id',$id)->delete();		
		// $delete_tab_detail->deleted_status = '1';
		// $delete_tab_detail->save();

		return redirect()->back()->with('success','Tab Detail Deleted Successfully');		
	}

	public function edittabDetail($id,$o_id){

		$edit_tab_detail = OrganisationTableDetail::with('get_tab_name')->where('id',$id)->first();
		$tab_name = OrganisationTabs::all();
		return view('footer.organisation.edit_tab_detail',compact('edit_tab_detail','id','tab_name','o_id'));

	}

	public function updatetabDetail(Request $request , $id,$o_id){

		// return $request->all();
		$tab_id = $request->tab_id;
        $chart_heading = $request->input('chart_heading');
        $designation_of_the_officer = $request->input('designation_of_the_officer');
        $name_of_the_officer = $request->input('name_of_the_officer');
        $range = $request->input('range');
        $block = $request->input('block');
        $beat = $request->input('beat');
        $territorial_range_beat_block = $request->input('territorial_range_beat_block');
        $officer_mobile_no = $request->input('officer_mobile_no');
        $image = $request->file('image');
    
       $tab_name = OrganisationTabs::where('id',$tab_id)->first();
       $t_name = $tab_name->tab_name;
		$save_table_detail = OrganisationTableDetail::find($id);
    
    	if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/organisation_chart');
            $image->move($destinationPath, $filename);
            $image = '/organisation_chart/' . $filename;
			$save_table_detail->image = $image ;
		}

		$save_table_detail->tab_id = $tab_id;
		$save_table_detail->chart_heading = $t_name ;
		$save_table_detail->designation_of_the_officer =  $designation_of_the_officer ;
		$save_table_detail->name_of_the_officer = $name_of_the_officer;
		$save_table_detail->range = $range;
		$save_table_detail->block = $block ;
		$save_table_detail->beat = $beat ;
		$save_table_detail->territorial_range_beat_block = $territorial_range_beat_block ;
		$save_table_detail->officer_mobile_no = $officer_mobile_no ;
		$save_table_detail->save();

		return redirect('/view_tabdetail/'.$o_id)->with('success','Tab Detail Updated Successfully');
	}

	public function saveorganisationDetail(Request $request , $id){

		// return $request->all();
		$tab_id = $request->tab_id;
        // $chart_heading = $request->input('chart_heading');
        $designation_of_the_officer = $request->input('designation_of_the_officer');
        $name_of_the_officer = $request->input('name_of_the_officer');
        $range = $request->input('range');
        $block = $request->input('block');
        $beat = $request->input('beat');
        $territorial_range_beat_block = $request->input('territorial_range_beat_block');
        $officer_mobile_no = $request->input('officer_mobile_no');
        $image = $request->file('image');
        $optradio = $request->input('optradio');
    	
    	$tab_name = OrganisationTabs::where('id',$tab_id)->first();
        $t_name = $tab_name->tab_name;
        $save_table_detail = new OrganisationTableDetail();
    
    	if($request->hasFile('image') != ""){
        	$filename =$image->getClientOriginalName();
            $destinationPath = public_path('/organisation_chart');
            $image->move($destinationPath, $filename);
            $image = '/organisation_chart/' . $filename;
        
			$save_table_detail->image = $image ;
		}


		$save_table_detail->banner_id = $id;
		$save_table_detail->tab_id = $tab_id;
		$save_table_detail->chart_heading = $t_name ;
		$save_table_detail->designation_of_the_officer =  $designation_of_the_officer ;
		$save_table_detail->name_of_the_officer = $name_of_the_officer;
		$save_table_detail->range = $range;
		$save_table_detail->block = $block ;
		$save_table_detail->beat = $beat ;
		$save_table_detail->territorial_range_beat_block = $territorial_range_beat_block ;
		$save_table_detail->officer_mobile_no = $officer_mobile_no ;
		$save_table_detail->type_of_table = $optradio;
		$save_table_detail->save();

		return redirect('/view_tabdetail/'.$id)->with('success','Organisation Chart Detail Added Successfully');
	}

	public function editorganisationDetail($id){

		$edit_organisation = OrganisationBannerDetail::where('id',$id)->first();
		return view('footer.organisation.edit',compact('edit_organisation','id'));
	}

	public function updateorganisationDetail(Request $request , $id){

		$banner_image = $request->file('banner_image');
		$banner_heading = $request->input('banner_heading');
       

		if($request->hasFile('banner_image') != ""){
        	$filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/organisation_chart');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/organisation_chart/' . $filename;

            $update_banner_detail = OrganisationBannerDetail::find($id);
			$update_banner_detail->banner_image = $banner_image;
			$update_banner_detail->save();
		}
		else{
			$update_banner_detail =  OrganisationBannerDetail::find($id);
			$update_banner_detail->banner_heading = $banner_heading;
			$update_banner_detail->save();
		}
		return redirect('/organisation_list')->with('success','Organisation Chart Detail Updated Successfully');
	}

	public function deleteorganisationDetail($id){


        $delete_banner_detail = OrganisationBannerDetail::find($id);
		$delete_banner_detail->deleted_status = '1';
		$delete_banner_detail->save();
		return redirect('/organisation_list')->with('success','Organisation Chart Detail Deleted Successfully');
	}

	public function organisationMaster(){

		$organisation_master = OrganisationMaster::paginate(10);
		return view('footer.organisation.master_list',compact('organisation_master'));
	}

	public function addNewMaster(){

		return view('footer.organisation.add_master');
	}

	public function saveNewMaster(Request $request){

		$validated = $request->validate([
            'tab_name' => 'required',
        ]);

		$tab_name = $request->input('tab_name');
		$save_tabname = new OrganisationMaster();
		$save_tabname->tab_name = $tab_name;
		$save_tabname->save();

		return redirect('organisation_master')->with('success','Organisation Master Name Added Successfully');
	}

	public function deletetabname($id){

		OrganisationMaster::where('id',$id)->delete();
		return redirect('organisation_master')->with('success','Organisation Master Name Deleted Successfully');
	}

	public function edittabname($id){

		$edittab = OrganisationMaster::where('id',$id)->first();
		return view('footer.organisation.edit_master',compact('edittab','id'));		
	}

	public function updatetabname(Request $request, $id){

		$tab_name = $request->input('tab_name');

		$edit_tabname = OrganisationMaster::find($id);
		$edit_tabname->tab_name = $tab_name;
		$edit_tabname->save();

		return redirect('organisation_master')->with('success','Organisation Master Updated Added Successfully');
	}

// end class 
}