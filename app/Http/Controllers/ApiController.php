<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Banner;



use App\Models\VolunteerDetail;
use App\Models\VolunteerDetailImage;

use App\Models\HomePageDetail;
use App\Models\WildlifeSymbol;

use App\Models\BirMotiBagh;

use App\Models\PanniwalaGumjal;
use App\Models\RakhSarai;

use App\Models\HarikaDetail;

use App\Models\NangalImages;
use App\Models\NangalDetail;

use App\Models\KesopurDetail;
use App\Models\KeshopurImages;

use App\Models\ChhatbirZooFrontend;

use App\Models\LudhianaZooFrontend;
use App\Models\PatialaZooFrontend;
use App\Models\BathindaZooFrontend;
use App\Models\DeerParkZooFrontend;

use App\Models\BirGurdialpur;
use App\Models\BirBhunerheri;

use App\Models\BirMihas;
use App\Models\BirDosanjh;
use App\Models\BirBhadson;

use App\Models\BirAishwan;
use App\Models\BirAbohar;
use App\Models\BirHarike;

use App\Models\LalwanCommunity;
use App\Models\KeshPurmi;
use App\Models\SiswanCommunity;


use App\Models\Roparwetland;
use App\Models\RanjitSagar;
use App\Models\BeasRiver;
use App\Models\KaliBein;

use App\Models\BirTakhniRehmpur;
use App\Models\BirJhajjarbachauli;
use App\Models\BirKathlaurKushlian;
use App\Models\BirNangalWildlife;
use App\Models\WorldDayDetail;
class ApiController extends Controller
{

    public function banner_list(){
    	
        $banner_detail = Banner::where('deleted_status','0')->inRandomOrder()->limit(2)->get();
        $result  = array();
        $result['list'] = $banner_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Banner Detail';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }
//     public function banner_two_list(){

//         $banner_detail_two = Banner::where('banner_slide','=','slide_2')->where('deleted_status','0')->latest()->first();
//         $result  = array();
//         $result['list'] = $banner_detail_two;
//         $result['error_code'] = '0';
//         $result['message'] = 'Banner Slide 2 Detail';
//         $result['base_url'] = url('/');
//         return json_encode($result);
//         //return response()->json($result); 
//     }
    
    public function get_world_day(){

        $get_world = WorldDayDetail::where('deleted_status','0')->take(4)->get();

        $result  = array();
        $result['list'] = $get_world;
        $result['error_code'] = '0';
        $result['message'] = 'World Day Detail';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

   

    public function picture_detail(){

        $picture_detail = PictureDetail::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $picture_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Picture Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function chhatbirzoo_detail(){

        $chhatbirzoo_detail = ChhatbirZoo::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $chhatbirzoo_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Chhatbir Zoo Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function ludhianazoo_detail(){

        $ludhianazoo_detail = LudhianaZoo::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $ludhianazoo_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Ludhiana Zoo Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function minizoopatiala_detail(){

        $minizoopatialalist = MiniZooPatiala::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $minizoopatialalist;
        $result['error_code'] = '0';
        $result['message'] = 'Mini Zoo Patiala Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function minizoobathinda_detail(){

        $minizoobathinda = MiniZooBathinda::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $minizoobathinda;
        $result['error_code'] = '0';
        $result['message'] = 'Mini Zoo Patiala Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function deer_park_neelon_detail(){

        $deerparkneelon_detail = DeerParkNeelon::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $deerparkneelon_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Deer Park Neelon Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);

    }

    public function chhatbir_detail(){

        $chhatbir_detail = Chhatbir::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $chhatbir_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Chhatbir Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }
    public function event_forest_day(){

        $event_forestdaydetail = EventForestDay::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $event_forestdaydetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event Forest Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function event_worldwildlife(){

        $event_worldwildlifedetail = EventWorldWildLife::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $event_worldwildlifedetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event World WildLife Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);
    }

    public function event_world_wetland(){

        $event_worldwetlanddetail = EventWorldWetland::where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $event_worldwetlanddetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event World Wetland Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result); 
    }

    public function event_migratory_bird(){

        $event_migratorybirddetail = EventMigratoryBirdDay::latest()->first();
        $result  = array();
        $result['list'] = $event_migratorybirddetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event World Migratory Bird Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);

    }

    public function bg_image(){

        $event_migratorybirddetail = EventBgImages::latest()->first();
        $result  = array();
        $result['list'] = $event_migratorybirddetail;
        $result['error_code'] = '0';
        $result['message'] = 'Event Background Image Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result); 
    }

    public function bg_imageheading(){

        $event_bgimageheading = EventBgimageHeading::latest()->first();
        $result  = array();
        $result['list'] = $event_bgimageheading;
        $result['error_code'] = '0';
        $result['message'] = 'Event Background Image Heading Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result); 
    }

    public function volunteer_detail(){

        $volunteer_detailheading = VolunteerDetail::latest()->first();
        $result  = array();
        $result['list'] = $volunteer_detailheading;
        $result['error_code'] = '0';
        $result['message'] = 'Volunteer Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result); 
    }
    public function volunteer_images(){

        $volunteer_images_detail = VolunteerDetailImage::latest()->first();
        $result  = array();
        $result['list'] = $volunteer_images_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Volunteer Images Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result); 
    }
    public function home(){

        $home_page_detail = HomePageDetail::with('homeGeographicZone')->where('deleted_status','=','0')->latest()->first();
        $result  = array();
        $result['list'] = $home_page_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Home Page Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result); 
    }
    
    public function wildlife_symbols(){

        $wildlife_symbol_detail = WildlifeSymbol::with('get_wildlife_symbol_images')->where('deleted_status','=','0')->latest()->take(4)->first();
        $result  = array();
        $result['list'] = $wildlife_symbol_detail;
        $result['error_code'] = '0';
        $result['message'] = 'Wild Life Symbol Detail List';
        $result['base_url'] = url('/');
        return json_encode($result);
        //return response()->json($result);

    }

 


    public function calendarDetail(){

         $event = WorldDayDetail::where('deleted_status','=','0')->get();
             $data=[];
             foreach($event as $events){
                $subArr=[
                    'id'=> $events->id,
                    'type_name'=> $events->type_name,
                    'date_of_event' => $events->date_of_event
                ];
                array_push($data,$subArr);
             }
             return json_encode($data);
             //return response()->json($data);
    }

// end class 
}