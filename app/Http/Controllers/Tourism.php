<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\TourismBannerDetail;
use App\Models\TourismData;
use App\Models\TourismTypes;
use App\Models\TourismLinkDetail;
use App\Models\TourismLinkImages;
use DB;
use Image;

class Tourism extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function tourismList(){

      $tourism_detail = TourismBannerDetail::where('deleted_status','0')->paginate(10);
      return view('tourism.listing',compact('tourism_detail'));
    }

    public function addtourism(){
      $tourism_type = TourismTypes::all();
      return view('tourism.add',compact('tourism_type'));
    }

    public function savetourism(Request $request){
    
      $validated = $request->validate([
          'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
          // 'image' => 'required|mimes:jpg,png,jpeg|max:2048',
          // 'tourism_type_id' => 'required',
          // 'time' => 'required',
          'banner_heading' => 'required|regex:/^[\.a-zA-Z0-9,!? ]*$/',
      ]); 

      $banner_image = $request->file('banner_image');
      $banner_heading = $request->input('banner_heading');
      // $tourism_type_id = $request->tourism_type_id;
      // $image = $request->file('image');
      // $time = $request->input('time');


      if($request->hasFile('banner_image') != ""){
          $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
    }

    // if($request->hasFile('image') != ""){
    //       $filename =$image->getClientOriginalName();
    //         $destinationPath = public_path('/tourism');
    //         $image->move($destinationPath, $filename);
    //         $image = '/tourism/' . $filename;
    // }

    $save_banner = new TourismBannerDetail();
    $save_banner->banner_image = $banner_image;
    $save_banner->banner_heading = $banner_heading ;
    $save_banner->save();
    // $id = $save_banner->id;

    // $save_tourism_data = new TourismData();
    // $save_tourism_data->banner_id = $id;
    // $save_tourism_data->tourism_type_id = $tourism_type_id;
    // $save_tourism_data->image = $image;
    // $save_tourism_data->time = $time;
    // $save_tourism_data->save();

    return redirect('/tourism_detail')->with('success','Tourism Detail Added Successfully');
    }

    public function edittourism($id){
      $tourism_type = TourismTypes::all();
       $edit_tourism = TourismBannerDetail::where('id',$id)->first();
      return view('tourism.edit',compact('edit_tourism','tourism_type','id'));
    }
  
  public function updateTourism(Request $request , $id){
    
      $validated = $request->validate([
          'banner_image' => 'mimes:jpg,png,jpeg|max:2048',
          // 'image' => 'mimes:jpg,png,jpeg|max:2048',
           'banner_heading' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
      ]); 

      $banner_image = $request->file('banner_image');
      $banner_heading = $request->input('banner_heading');
      // $tourism_type_id = $request->tourism_type_id;
      // $image = $request->file('image');
      // $time = $request->input('time');


          $update_banner = TourismBannerDetail::find($id);
      if($request->hasFile('banner_image') != ""){
          $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
        
          $update_banner->banner_image = $banner_image;
         
        }

  
    $update_banner->banner_heading = $banner_heading ;
    $update_banner->save();
    // $b_id = $update_banner->id;
    
    //     if($request->hasFile('image') != ""){
    //       $filename =$image->getClientOriginalName();
    //         $destinationPath = public_path('/tourism');
    //         $image->move($destinationPath, $filename);
    //         $image = '/tourism/' . $filename;
    //         $update_tourism_data = TourismData::find($b_id);
    //         $update_tourism_data->image = $image;
    //         $update_tourism_data->save();
    // }
    // else{
    //       $update_tourism_data = TourismData::find($b_id);
    //       $update_tourism_data->banner_id = $b_id;
    //       $update_tourism_data->tourism_type_id = $tourism_type_id;
    //       $update_tourism_data->image = $image;
    //       $update_tourism_data->time = $time;
    //       $update_tourism_data->save();
    //     }
    

    return redirect('/tourism_detail')->with('success','Tourism Detail Added Successfully');
    }

    public function addtourismgallery($id,$type_id,$m_id){
      // return 1;
        $t_id = TourismData::with('get_bdata','get_tourism_type')->where('id',$id)->first();
        $tourism_type = TourismTypes::all();
        return view('tourism.addgallery',compact('tourism_type','id','m_id','type_id'));
    }

    public function savetourismgallery(Request $request,$id,$type_id,$m_id){
    
      // return $request->all();
    
      $validated = $request->validate([
          'tourism_type_id' => 'required',
          'banner_image' => 'required|mimes:jpg,png,jpeg|max:2048',
          'banner_heading' => 'required',
          'description_heading' => 'required',
          'description' => 'required',
          'heading_how_to_reach' => 'required',
          'detail_how_to_reach' => 'required',
          'heading_permission_required' => 'required',
          'detail_required_permmision' => 'required',
          'heading_hire_a_guide' => 'required',
          'descripton_hire_guide' => 'required',
          'heading_accommodation_options' => 'required',
          'detail_accommodation_options' => 'required',
          'heading_do' => 'required',
          'detail_do' => 'required',
          'heading_dont' => 'required',
          'detail_dont' => 'required',
          'heading_best_time_visit' => 'required',
          'detail_best_time_visit' => 'required',
          'images_heading' => 'required',
          'image' => 'required',
          'map_lat' => 'required',
          'map_long' => 'required',
          'zoom_level' => 'required',
      ]); 
    
       $tourism_type_id = $request->input('tourism_type_id');
       $banner_image = $request->file('banner_image');
       $banner_heading = $request->input('banner_heading');
       $description_heading = $request->input('description_heading');
       $description = $request->input('description');

       $heading_how_to_reach = $request->input('heading_how_to_reach');
       $detail_how_to_reach = $request->input('detail_how_to_reach');

       $heading_permission_required = $request->input('heading_permission_required');
       $detail_required_permmision = $request->input('detail_required_permmision');

       $heading_hire_a_guide = $request->input('heading_hire_a_guide');
       $descripton_hire_guide = $request->input('descripton_hire_guide');

       $heading_accommodation_options = $request->input('heading_accommodation_options');
       $detail_accommodation_options = $request->input('detail_accommodation_options');

       $heading_do = $request->input('heading_do');
       $detail_do = $request->input('detail_do');

       $heading_dont = $request->input('heading_dont');
       $detail_dont = $request->input('detail_dont');

       $heading_best_time_visit = $request->input('heading_best_time_visit');
       $detail_best_time_visit = $request->input('detail_best_time_visit');
       $map_lat = $request->input('map_lat');
       $map_long = $request->input('map_long');
       $zoom_level = $request->input('zoom_level');

       $images_heading = $request->input('images_heading');
       $image = $request->file('image');

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
        }

       $save_detail = new TourismLinkDetail();
       $save_detail->tourism_type_id = $tourism_type_id;
       $save_detail->banner_image = $banner_image;
       $save_detail->banner_id = $m_id;
       $save_detail->banner_heading = $banner_heading;
       $save_detail->description_heading = $description_heading;
       $save_detail->description = $description;

       $save_detail->heading_how_to_reach = $heading_how_to_reach;
       $save_detail->detail_how_to_reach = $detail_how_to_reach;

       $save_detail->heading_permission_required = $heading_permission_required;
       $save_detail->detail_required_permmision = $detail_required_permmision;

       $save_detail->heading_hire_a_guide = $heading_hire_a_guide;
       $save_detail->descripton_hire_guide = $descripton_hire_guide;

       $save_detail->heading_accommodation_options = $heading_accommodation_options;
       $save_detail->detail_accommodation_options = $detail_accommodation_options;

       $save_detail->heading_do = $heading_do;
       $save_detail->detail_do = $detail_do;

       $save_detail->heading_dont = $heading_dont;
       $save_detail->detail_dont = $detail_dont;

        $save_detail->heading_best_time_visit = $heading_best_time_visit;
       $save_detail->detail_best_time_visit = $detail_best_time_visit;

       $save_detail->images_heading = $images_heading;
       $save_detail->map_lat = $map_lat;
       $save_detail->map_long = $map_long;
       $save_detail->zoom_level = $zoom_level;
       $save_detail->save();
       $id = $save_detail->id;

        $image_data=array();
        if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = $file->getClientOriginalName();
                $file->move(public_path() . '/tourism/', $image);
                $image_data[] = '/tourism/'.$image;
            }
        }

        for($i=0;$i<count($image_data);$i++)
        {
            
            $save_images = new TourismLinkImages();
            $save_images->tourism_link_id = $id; 
            $save_images->tourism_type_id = $tourism_type_id ;
            $save_images->image = $image_data[$i];
            $save_images->save();
        }

       return redirect('/view_detail/'.$save_detail->banner_id.'/'.$type_id.'/'.$m_id)->with('success','Gallery Images Added Successfully');
      
   }

   public function deletetourismgallery($id){

          $delete_tourism_detail = TourismBannerDetail::find($id);
          $delete_tourism_detail->deleted_status = '1';
          $delete_tourism_detail->save();
        return redirect('/tourism_detail')->with('success','Tourism Detail Deleted Successfully');
   }

   public function editTourismDetail($id , $b_id , $m_id , $type_id){
	// return $m_id;
      $tourismDetail = TourismLinkDetail::where('tourism_type_id',$b_id)->where('id',$id)->where('deleted_status','0')->first();
    
     return view('tourism.editgalleryDetail',compact('tourismDetail','b_id','m_id','id','type_id'));
   }

   public function updateTourismDetail(Request $request , $id , $b_id , $m_id , $type_id){

       // return $request->all();
   	// return $b_id;
   
      $validated = $request->validate([
          'banner_image' => 'max:2048',
      ]);   
   
       $tourism_type_id = $request->input('tourism_type_id');
       $banner_image = $request->file('banner_image');
       $banner_heading = $request->input('banner_heading');
       $description_heading = $request->input('description_heading');
       $description = $request->input('description');

       $heading_how_to_reach = $request->input('heading_how_to_reach');
       $detail_how_to_reach = $request->input('detail_how_to_reach');

       $heading_permission_required = $request->input('heading_permission_required');
       $detail_required_permmision = $request->input('detail_required_permmision');

       $heading_hire_a_guide = $request->input('heading_hire_a_guide');
       $descripton_hire_guide = $request->input('descripton_hire_guide');

       $heading_accommodation_options = $request->input('heading_accommodation_options');
       $detail_accommodation_options = $request->input('detail_accommodation_options');

       $heading_do = $request->input('heading_do');
       $detail_do = $request->input('detail_do');

       $heading_dont = $request->input('heading_dont');
       $detail_dont = $request->input('detail_dont');

       $heading_best_time_visit = $request->input('heading_best_time_visit');
       $detail_best_time_visit = $request->input('detail_best_time_visit');
       $map_lat = $request->input('map_lat');
       $map_long = $request->input('map_long');
       $zoom_level = $request->input('zoom_level');

      
       $edit_detail = TourismLinkDetail::find($id);

        if($request->hasFile('banner_image') != ""){
            $filename =$banner_image->getClientOriginalName();
            $destinationPath = public_path('/tourism');
            $banner_image->move($destinationPath, $filename);
            $banner_image = '/tourism/' . $filename;
            $edit_detail->banner_image = $banner_image;
           
        }
      

       $edit_detail->banner_heading = $banner_heading;
       $edit_detail->description_heading = $description_heading;
       $edit_detail->description = $description;

       $edit_detail->heading_how_to_reach = $heading_how_to_reach;
       $edit_detail->detail_how_to_reach = $detail_how_to_reach;

       $edit_detail->heading_permission_required = $heading_permission_required;
       $edit_detail->detail_required_permmision = $detail_required_permmision;

       $edit_detail->heading_hire_a_guide = $heading_hire_a_guide;
       $edit_detail->descripton_hire_guide = $descripton_hire_guide;

       $edit_detail->heading_accommodation_options = $heading_accommodation_options;
       $edit_detail->detail_accommodation_options = $detail_accommodation_options;

       $edit_detail->heading_do = $heading_do;
       $edit_detail->detail_do = $detail_do;

       $edit_detail->heading_dont = $heading_dont;
       $edit_detail->detail_dont = $detail_dont;

       $edit_detail->heading_best_time_visit = $heading_best_time_visit;
       $edit_detail->detail_best_time_visit = $detail_best_time_visit;
       $edit_detail->map_lat = $map_lat;
       $edit_detail->map_long = $map_long;
       $edit_detail->zoom_level = $zoom_level;

       
       $edit_detail->save();

       
       
         return redirect('view_detail/'.$b_id.'/'.$type_id.'/'.$m_id)->with('success','Tourism Detail Updated Successfully');
   }

  public function deleteDetail($id){

    $delete = TourismLinkDetail::find($id);
    $delete->deleted_status = '1';
    $delete->save();
    TourismLinkImages::where('tourism_link_id',$delete->id)->delete();
    return redirect()->back()->with('success','Detail Deleted Successfully');
  }

   public function tourismDetail($id,$type_id,$m_id){

    $tourismDetaillist_ = TourismLinkDetail::where('banner_id',$m_id)->where('tourism_type_id',$type_id)->where('deleted_status','0')->get();

      foreach($tourismDetaillist_ as $t){

        $t->image_count = TourismLinkImages::where('tourism_link_id',$t->id)->count();

      }
   // return $tourismDetaillist_;
     return view('tourism.detail_listing',compact('tourismDetaillist_','id','type_id','m_id'));
   }

   public function deletetourismDetail($id){

     $delete_detail = TourismLinkDetail::find($id);
     $delete_detail->deleted_status = '1';
     $delete_detail->save();
     return redirect()->back()->with('success','Delete Successfully');
   }

   public function viewGalleryMaster($id){

      $list = TourismData::with('get_tourism_type')->where('banner_id',$id)->where('deleted_status','0')->paginate(10);
      return view('tourism.viewGalleryMaster',compact('list','id'));
   }

  public function addMoreTourism($t_id){

    $tourism_type = TourismTypes::get();
    return view('tourism.addMoreTourism',compact('t_id','tourism_type'));
  }

  public function saveMoreTourism(Request $request , $t_id){

      $validated = $request->validate([
          'image' => 'required|mimes:jpg,png,jpeg|max:2048',
          'tourism_type_id' => 'required',
          'time' => 'required',
      ]); 

     
      $tourism_type_id = $request->tourism_type_id;
      $image = $request->file('image');
      $time = $request->input('time');



    // if($request->hasFile('image') != ""){
    //       $filename =$image->getClientOriginalName();
    //         $destinationPath = public_path('/tourism');
    //         $image->move($destinationPath, $filename);
    //         $image = '/tourism/' . $filename;
    // }



       // $folder_path = '/images';
            $destinationPath = public_path('/tourism');

            $image = $request->file('image');

            $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

            $img = Image::make($image->getRealPath());
            $img->save($destinationPath.'/'.$img_name);

            //resize image
            $img->resize(395,233)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

            $input['img'] = $img_name;

            $save_data = new TourismData();
            $save_data->banner_id = $t_id;
            $save_data->tourism_type_id = $tourism_type_id;
            $save_data->image =  '/tourism/'.$input['img'];;
            $save_data->time = $time;
            $save_data->save();

    return redirect('/view-gallery-master/'.$t_id)->with('success','Tourism Detail Master Added Successfully');
  }

  public function editMoreTourismMaster($id,$t_id){

    $tourism_type = TourismTypes::get();

    $edit =  TourismData::where('id',$id)->first(); 
       
    return view('tourism.editMoreTourism',compact('id','t_id','edit','tourism_type'));
  }

  public function updateMoreTourismMaster(Request $request , $id, $t_id){

     $validated = $request->validate([
          'image' => 'mimes:jpg,png,jpeg|max:2048',
      ]); 

     
      $tourism_type_id = $request->tourism_type_id;
      $image = $request->file('image');
      $time = $request->input('time');


    $update_data = TourismData::find($id);

    if($request->hasFile('image') != ""){

       // $folder_path = '/images';
            $destinationPath = public_path('/zoo_gallery');

            $image = $request->file('image');

            $img_name = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path($folder_path);

            $img = Image::make($image->getRealPath());
            $img->save($destinationPath.'/'.$img_name);

            //resize image
            $img->resize(395,233)->save($destinationPath.'/'.$img_name);
            //resize thumb image
            // $img->resize(100,100)->save($destinationPath.'/thumb/'.$img_name);

            $input['img'] = $img_name;
      
            $update_data->image =  '/zoo_gallery/'.$input['img'];
    }

    $update_data->banner_id = $t_id;
    $update_data->tourism_type_id = $tourism_type_id;
    $update_data->time = $time;
    $update_data->save();

    return redirect('/view-gallery-master/'.$t_id)->with('success','Tourism Detail Master Updated Successfully');
  }

  public function deleteMasterDetail($id){

    $delete_data = TourismData::find($id);
      $delete_data->deleted_status = '1';
      $delete_data->save();

      return redirect()->back()->with('success','Tourism Detail Master Updated Successfully');
  }

  public function detailImagesList($t_id,$b_id,$m_id){

    $list = TourismLinkImages::where('tourism_link_id',$t_id)->paginate('10');

    return view('tourism.images-list',compact('t_id','b_id','m_id','list'));
  }

  public function editTourismImage($id,$b_id,$m_id){

   
    $image_list = TourismLinkImages::where('id',$id)->first();
    return view('tourism.edit-image-list',compact('id','b_id','m_id','image_list'));
  }

  public function updateTourismImage(Request $request ,$id,$b_id,$m_id){

    $image = $request->file('image');
    $update_image = TourismLinkImages::find($id);

     if($request->hasFile('image') != ""){
      $filename =$image->getClientOriginalName();
      $destinationPath = public_path('/tourism');
      $image->move($destinationPath, $filename);
      $image = '/tourism/' . $filename;
       $update_image->image =$image;
    }

      $update_image->save();

      return redirect('detail-images-list/'.$update_image->tourism_link_id.'/'.$b_id.'/'.$m_id)->with('success','Image Updated Successfully');
  }

  public function deleteTourismImage($id){

    TourismLinkImages::where('id',$id)->delete();
    return redirect()->back()->with('success','Image Deleted Successfully');

  }

  public function addMoreTourismImage($t_id,$b_id,$m_id){

     $tourism_type = TourismTypes::get();
    return view('tourism.addMoreTourismImage',compact('t_id','b_id','m_id','tourism_type'));

     
  }

  public function saveMoreTourismImage(Request $request ,$t_id,$b_id,$m_id){

      $validated = $request->validate([
         
          'image' => 'required|mimes:jpg,png,jpeg|max:2048',
           'tourism_type_id' => 'regex:/^[\.a-zA-Z0-9,!? ]*$/',
      ]); 

      $image = $request->file('image');
      $tourism_type_id = $request->input('tourism_type_id');

     if($request->hasFile('image') != ""){
      $filename =$image->getClientOriginalName();
      $destinationPath = public_path('/tourism');
      $image->move($destinationPath, $filename);
      $image = '/tourism/' . $filename;
      
    }

      $save = new TourismLinkImages();
      $save->image = $image;
      $save->tourism_type_id = $tourism_type_id;
      $save->tourism_link_id = $t_id;
      $save->save();

      return redirect('detail-images-list/'.$t_id.'/'.$b_id.'/'.$m_id)->with('success','New Image Added Successfully');
  }
// end class 
}