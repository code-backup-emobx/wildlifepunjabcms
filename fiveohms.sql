-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 31, 2022 at 09:01 AM
-- Server version: 8.0.25-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fiveohms`
--

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_banner`
--

CREATE TABLE `act_rule_banner` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_banner`
--

INSERT INTO `act_rule_banner` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/act_rulepdf/world_wetland.png', 'Acts & Rules', '2021-06-14 06:21:38', '2022-02-01 06:17:29', '0'),
(2, '/act_rulepdf/5.png', 'hjkhj', '2022-02-01 06:17:16', '2022-02-01 06:17:18', '1');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_headings`
--

CREATE TABLE `act_rule_headings` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `wildlife_heading` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_headings`
--

INSERT INTO `act_rule_headings` (`id`, `banner_id`, `wildlife_heading`, `title`, `description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Wildlife Manual', 'Preface', 'The Bombay Natural History Society which was founded in 1883, through its journal and other publications endeavoured to stimulate public interest in wildlife. Consequently Wild Birds and Game Act (Act XX of 1887) was enacted by Government of India. This was later on replaced by the Wild Birds and Wild Animals Protection Act (Act VIII of 1912) which together with the Indian Forest Act (Act XIV of 1927) became the basis of preservation laws in the country. In the pre-partition Punjab, which extended from Attuck (Pakistan) to Gurgaon (Harayana, India) a great concern was felt about the depleting wildlife. A society, Northern India Association for the Protection of Wildlife was formed and started working for wildlife preservation in the non-forest areas of Punjab. The Association ultimately succeeded in getting the Punjab Wild Birds and Wild Animals Protection Act passed from the Punjab Government in 1933. This act was exclusively for the non-forest areas and the Act VIII of 1912 was consequently repealed in Punjab. Punjab, therefore, became the premier state in India to have a state act and rules on wildlife preservation of its own. District Fauna Committees were formed in the state under the president ship of Deputy Commissioners and Game Wardens were appointed to enforce the game laws. Initially a batch of three Game Inspectors were appointed, one each at Lahore, Simla and Gurdaspur on experimental basis. The experiment proved successful and consequently 21 more Game Inspectors were appointed in September 1942. Thus there were 24 Game Inspectors in 29 districts of the then Punjab State. The Game Wardens were also made Government servants in April 1943 and a full fledged Game Preservation Department was created with the Game Warden Punjab as the State executive which was first made part of the Agricultural Department. In 1951, it however was placed directly under the Secretary to Government, Department of Agriculture. In 1957 it was attached with the Forest Department and in 1958 its name was changed to Wildlife Preservation Department which gained a separate identity and a separate cadre in Punjab. On merging of Pepsu with Punjab in 1956, an anomalous situation arose since the Punjab areas were governed by the Punjab Wild Birds and Wild Animals Protection Act, 1933, whereas, the erstwhile Pepsu areas were governed by the Preservation of the Fauna of Patiala Act, 1939. To attain unity of action in both the -: 2 :- areas of the state a unified Punjab Wildlife Preservation Act, 1959 was passed and enforced in place of the previous two acts. To push up further and safeguard the cause of wildlife conservation in India, the Indian Board for Wildlife was constituted in 1952. The Government of India set up a Wildlife Expert Committee to suggest measures for improving the wildlife management, preservation and conservation in India who in its report suggested the formation of a Wildlife Protection Act for whole of India, which the government finally enacted in Wildlife (Protection) Act, 1972. Though Punjab Forest Department assumed the responsibility of keeping Wildlife Preservation Wing under its control for the conservation and management of wildlife as well as implementation of Wildlife Protection Act in the state, still there were many bottlenecks in the administration particularly with respect to sharing of responsibilities for the implementation of Wildlife (Protection) Act, 1972 by the Divisional Forest Officers in the state. The present Wildlife Manual is an effort of the department to provide guidance to various stake holders in tackling issues related with wildlife conservation and management in the state. With respect to administrative and financial matters, various provisions as contained in Punjab Forest Manual, Punjab Financial Rules, Punjab Civil Services Rules Punjab Budget Manual etc. will be applicable to all concerned responsible for implementing Wildlife (Protection) Act, 1972 in the State. Efforts have been made to make the manual a ready reckoner and user friendly guide to handle the various issues on wildlife matters. Any suggestion in improving the document will be welcomed. I hope this little effort of the department will get befitting and deserving response from all the concerned involved in implementing Wildlife (Protection) Act, 1972 in the State..', '2021-06-14 06:21:39', '2022-02-02 11:21:33', '0');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_management_detail`
--

CREATE TABLE `act_rule_management_detail` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `management_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_management_detail`
--

INSERT INTO `act_rule_management_detail` (`id`, `banner_id`, `management_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Management Plan', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_management_heading`
--

CREATE TABLE `act_rule_management_heading` (
  `id` int NOT NULL,
  `act_rule_id` int NOT NULL,
  `managemt_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_management_heading`
--

INSERT INTO `act_rule_management_heading` (`id`, `act_rule_id`, `managemt_heading`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'update one', '2022-01-03 08:14:37', '2022-01-03 11:25:09', '2022-01-03 11:25:09'),
(2, 1, 'gfhfg', '2022-01-03 10:34:14', '2022-01-03 11:25:13', '2022-01-03 11:25:13'),
(3, 1, 'gjfgfgfgfgfgfgfgfgfgfgfgfgfg', '2022-01-03 10:37:00', '2022-01-03 11:25:18', '2022-01-03 11:25:18'),
(4, 1, 'Management Plan Nangal Wildlife Sanctuary', '2022-01-03 11:35:38', '2022-01-08 04:54:00', NULL),
(5, 1, 'Management Plan Nangal Wildlife Sanctuary two', '2022-01-07 14:12:37', '2022-01-07 14:12:37', NULL),
(6, 1, 'Management Plan Nangal', '2022-01-24 04:41:38', '2022-01-24 04:42:29', '2022-01-24 04:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_pdf_detail`
--

CREATE TABLE `act_rule_pdf_detail` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `pdf_url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_pdf_detail`
--

INSERT INTO `act_rule_pdf_detail` (`id`, `banner_id`, `name`, `pdf_url`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Guidelines on Minimum Dimensions of Enclosures for Housing of Exotic Spp', '/act_rulepdf/1623651698.pdf1.pdf', '2021-06-14 06:21:38', '2021-06-14 06:21:38', '0'),
(2, 1, 'Declaration of Wildlife Stock Rules, 2003', '/act_rulepdf/1623651698.pdf2.pdf', '2021-06-14 06:21:38', '2021-06-14 06:21:38', '0'),
(3, 1, 'Guidelines for Appointment of Honorary Wildlife Wardens', '/act_rulepdf/1623651698.pdf3.pdf', '2021-06-14 06:21:38', '2021-06-14 06:21:38', '0'),
(4, 1, 'National Board for Wildlife Rules, 2003', '/act_rulepdf/1623651698.pdf4.pdf', '2021-06-14 06:21:38', '2021-06-14 06:21:38', '0'),
(5, 1, 'Guidelines on Minimum Dimensions of Enclosures for Housing of Species', '/act_rulepdf/1623651698.pdf5.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(6, 1, 'Orders Prohibiting Sale of Animals by Zoo', '/act_rulepdf/1623651698.pdf6.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(7, 1, 'National Zoo Policy, 1998', '/act_rulepdf/1623651698.pdf7.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(8, 1, 'Wildlife Licensing Rules 1983', '/act_rulepdf/1623651698.pdf8.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(9, 1, 'Recognition of Zoo Rules, 2009', '/act_rulepdf/1623651698.pdf9.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(10, 1, 'Wildlife (Protection) Act, 1972', '/act_rulepdf/1623651698.pdf10.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(11, 1, 'Wildlife Protection Rules, 1995', '/act_rulepdf/1623651698.pdf11.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(12, 1, 'Wildlife Specified Plant Stock Declaration Rules, 1995', '/act_rulepdf/1623651698.pdf12.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(13, 1, 'Wildlife Stock Declaration Central Rules 1973', '/act_rulepdf/1623651698.pdf13.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(14, 1, 'Wildlife Transactions and Taxidermy Rules 1973', '/act_rulepdf/1623651698.pdf14.pdf', '2021-06-14 06:21:39', '2022-02-01 04:56:03', '0'),
(15, 1, 'Wildlife Specified Plants Rules, 1995', '/act_rulepdf/1623651698.pdf15.pdf', '2021-06-14 06:21:39', '2022-02-01 04:49:06', '1'),
(16, 1, 'khuuuuuuuuuuuuuu', '/act_rulepdf/1643691379.pdf3.pdf', '2022-02-01 04:56:19', '2022-02-01 04:58:21', '1'),
(17, 1, 'Wildlife (Protection) Punjab Rules, 1975', '/act_rulepdf/1643791437.WLP Rules 1975.pdf', '2022-02-02 08:43:57', '2022-02-02 08:43:57', '0');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_pdf_three`
--

CREATE TABLE `act_rule_pdf_three` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `management_id` int NOT NULL,
  `name_management_pdf` varchar(255) NOT NULL,
  `management_pdf` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_pdf_three`
--

INSERT INTO `act_rule_pdf_three` (`id`, `banner_id`, `management_id`, `name_management_pdf`, `management_pdf`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 1, 'MANAGEMENT PLAN OF LALWAN COMMUNITY RESERVE', '/act_rulepdf/1623651700.pdf39.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(2, 1, 1, 'MANAGEMENT PLAN OF TAKHNI-REHMAPUR WLS', '/act_rulepdf/1623651700.pdf40.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(3, 1, 1, 'MANAGEMENT PLAN KATHLOUR KUSHLIAN', '/act_rulepdf/1623651700.pdf41.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(4, 1, 1, 'MANAGEMENT PLAN RSDCR', '/act_rulepdf/1623651700.pdf42.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(5, 1, 1, 'MANAGEMENT PLAN KCCR', '/act_rulepdf/1623651700.pdf43.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_pdf_two`
--

CREATE TABLE `act_rule_pdf_two` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `wildlife_detail_id` int NOT NULL,
  `name_of_pdf` varchar(255) NOT NULL,
  `pdf_link` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_pdf_two`
--

INSERT INTO `act_rule_pdf_two` (`id`, `banner_id`, `wildlife_detail_id`, `name_of_pdf`, `pdf_link`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 1, 'CHAPTER-1 SILENT FEATURES OF WILDLIFE (PROTECTION) ACT, 1972', '/act_rulepdf/1623651699.pdf17.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(2, 1, 1, 'CHAPTER-2 MANAGEMENT OF ZOOS.', '/act_rulepdf/1623651699.pdf18.pdf', '2021-06-14 06:21:39', '2022-02-01 05:01:52', '0'),
(3, 1, 1, 'CHAPTER-3 DUTIES AND RESPONSIBILITIES', '/act_rulepdf/1623651699.pdf19.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(4, 1, 1, 'POLICIES', '/act_rulepdf/1623651699.pdf20.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(5, 1, 1, 'DIRECTIVES', '/act_rulepdf/1623651699.pdf21.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(6, 1, 1, 'GUIDELINES', '/act_rulepdf/1623651699.pdf22.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(7, 1, 1, 'WILDLIFE (PROTECTION) ACT, 1972', '/act_rulepdf/1623651699.pdf23.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(8, 1, 1, 'CENTRAL RULES UNDER WILDLIFE (PROTECTION) ACT, 1972', '/act_rulepdf/1623651699.pdf24.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(9, 1, 1, 'STATE RULES UNDER WILD LIFE (PROTECTION) ACT, 1972', '/act_rulepdf/1623651699.pdf25.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(10, 1, 1, 'NOTIFICATION OF PROTECTED AREA', '/act_rulepdf/1623651699.pdf26.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(11, 1, 1, 'IMPORTANT JUDGMENTS FROM HON\'BLE SUPREME COURT OF INDIA AND DIFFRENT HIGH COURTS IN THE COUNTRY', '/act_rulepdf/1623651699.pdf27.pdf', '2021-06-14 06:21:39', '2021-06-14 06:21:39', '0'),
(12, 1, 1, 'MISC NOTIFICATIONS', '/act_rulepdf/1623651699.pdf28.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(13, 1, 1, 'SPECIMEN FORMAT OF APPLICATION FOR PRODUCING THE ACCUSED BEFORE THE COURT', '/act_rulepdf/1623651699.pdf29.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(14, 1, 1, 'SEARCH AND SEIZURE REPORT', '/act_rulepdf/1623651699.pdf30.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(15, 1, 1, 'POST MORTEM REPORT', '/act_rulepdf/1623651699.pdf31.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(16, 1, 1, 'SPECIMEN FOR CASE PROPERTY REGISTER', '/act_rulepdf/1623651699.pdf32.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(17, 1, 1, 'SPECIMEN OF FIRST INFORMATION REPORT', '/act_rulepdf/1623651699.pdf33.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(18, 1, 1, 'SPECIMEN OF FIRST INFORMATION REPORT REGISTER', '/act_rulepdf/1623651699.pdf34.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(19, 1, 1, 'SPECIMEN FOR LODGING COMPAINT IN THE COURT', '/act_rulepdf/1623651699.pdf35.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(20, 1, 1, 'SPECIMEN FOR PROSECUTION REGISTER', '/act_rulepdf/1623651699.pdf36.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(21, 1, 1, 'PROTECTED AREAS OF THE STATE', '/act_rulepdf/1623651699.pdf37.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(22, 1, 1, 'CREATION AND MANAGEMENT OF PROTECTED AREA', '/act_rulepdf/1623651699.pdf38.pdf', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0'),
(23, 1, 1, 'CHAPTER-2 MANAGEMENT OF ZOOS..hkgggggggggg', '/act_rulepdf/1643691762.1623651699.pdf17.pdf', '2022-02-01 05:02:42', '2022-02-01 05:02:56', '1');

-- --------------------------------------------------------

--
-- Table structure for table `act_rule_subheading`
--

CREATE TABLE `act_rule_subheading` (
  `id` int NOT NULL,
  `management_heading_id` int NOT NULL,
  `subheading` varchar(255) NOT NULL,
  `pdf` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `act_rule_subheading`
--

INSERT INTO `act_rule_subheading` (`id`, `management_heading_id`, `subheading`, `pdf`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'update three', '/act_rulepdf/1641208844.download.pdf', '2022-01-03 08:14:37', '2022-01-03 11:25:09', '2022-01-03 11:25:09'),
(2, 1, 'update three', '/act_rulepdf/1641208844.download.pdf', '2022-01-03 08:14:37', '2022-01-03 11:25:09', '2022-01-03 11:25:09'),
(3, 1, 'update three', '/act_rulepdf/1641208844.download.pdf', '2022-01-03 08:14:37', '2022-01-03 11:25:09', '2022-01-03 11:25:09'),
(4, 2, 'hfffffffff', '/act_rulepdf/1641206054.Calendar eMobX 2021.pdf', '2022-01-03 10:34:14', '2022-01-03 11:25:13', '2022-01-03 11:25:13'),
(5, 3, 'jghhhhhhhhhhhhhhhhf', '/act_rulepdf/1641206220.Calendar eMobX 2021.pdf', '2022-01-03 10:37:00', '2022-01-03 11:25:18', '2022-01-03 11:25:18'),
(6, 4, 'MANAGEMENT PLAN AMENDED.', '/act_rulepdf/1641883596.pdf23.pdf', '2022-01-03 11:35:38', '2022-01-11 06:46:36', NULL),
(7, 5, 'heading thress', '/act_rulepdf/1641883444.pdf1.pdf', '2022-01-07 14:12:37', '2022-01-11 06:44:04', NULL),
(8, 5, 'heading thress', '/act_rulepdf/1641883359.pdf20.pdf', '2022-01-07 14:12:37', '2022-01-11 06:42:39', NULL),
(9, 6, 'heading one', '/act_rulepdf/1642999298.pdf15.pdf', '2022-01-24 04:41:38', '2022-01-24 04:42:29', '2022-01-24 04:42:29'),
(10, 4, 'MANAGEMENT PLAN AMENDED', '/act_rulepdf/1643696379.pdf3.pdf', '2022-02-01 06:19:39', '2022-02-01 06:19:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) DEFAULT NULL,
  `banner_description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('1','0') DEFAULT '0' COMMENT '0->not deleted . 1-> deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `banner_image`, `banner_heading`, `banner_description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/banner_image/campaforest.png', NULL, NULL, '2021-04-09 09:23:23', '2021-04-13 09:29:25', '1'),
(2, '/banner_image/2ef951069be537b4f6b9a707bbb1fe54.jpg', NULL, NULL, '2021-04-09 09:24:36', '2021-04-13 09:29:29', '1'),
(3, '/banner_image/8-86028_track-wallpapers-pc.jpg', NULL, NULL, '2021-04-09 09:25:16', '2021-04-13 09:29:31', '1'),
(4, '/banner_image/campaforest.png', NULL, NULL, '2021-04-12 06:09:48', '2021-04-12 06:11:49', '1'),
(5, '/banner_image/PGQmrs.jpg', NULL, NULL, '2021-04-13 05:06:25', '2021-04-13 09:29:34', '1'),
(6, '/banner_image/Best-HD-PC-Desktop-Wallpaper.jpg', NULL, NULL, '2021-04-13 05:06:34', '2021-04-13 09:29:36', '1'),
(7, '/banner_image/58034_cool-pc-backgrounds-hd.jpg', NULL, NULL, '2021-04-13 05:06:45', '2021-04-13 09:29:38', '1'),
(8, '/banner_image/2ef951069be537b4f6b9a707bbb1fe54.jpg', NULL, NULL, '2021-04-13 05:07:27', '2021-04-13 09:29:40', '1'),
(9, '/banner_image/2ef951069be537b4f6b9a707bbb1fe54.jpg', NULL, NULL, '2021-04-13 05:07:45', '2021-04-13 09:29:43', '1'),
(10, '/banner_image/8-86028_track-wallpapers-pc.jpg', NULL, NULL, '2021-04-13 05:07:55', '2021-04-13 05:07:55', '1'),
(11, '/banner_image/8-86028_track-wallpapers-pc.jpg', NULL, NULL, '2021-04-13 05:08:03', '2021-04-13 05:08:03', '1'),
(12, '/banner_image/PGQmrs.jpg', NULL, NULL, '2021-04-13 05:08:14', '2021-04-13 05:08:14', '1'),
(13, '/banner_image/Best-HD-PC-Desktop-Wallpaper.jpg', NULL, NULL, '2021-04-13 09:25:40', '2021-04-19 10:21:02', '1'),
(14, '/banner_image/8-86028_track-wallpapers-pc.jpg', NULL, NULL, '2021-04-19 08:52:53', '2021-04-19 10:21:07', '1'),
(15, '/banner_image/Best-HD-PC-Desktop-Wallpaper.jpg', NULL, NULL, '2021-04-19 09:13:11', '2021-04-19 10:21:11', '1'),
(16, '/banner_image/48.jpg', NULL, NULL, '2021-04-19 09:58:05', '2022-05-06 06:16:35', '1'),
(17, '/banner_image/slide2.png', NULL, NULL, '2021-04-19 10:01:24', '2021-04-29 13:04:57', '1'),
(18, '/banner_image/slide1.png', NULL, NULL, '2021-06-04 09:28:19', '2021-06-04 09:28:50', '1'),
(20, '/banner_image/slide2.png', NULL, NULL, '2021-06-04 09:31:22', '2021-12-22 12:12:51', '1'),
(21, '/banner_image/Ropar_Wetland_03.jpg', NULL, NULL, '2021-06-17 11:27:23', '2021-06-17 11:32:51', '1'),
(22, '/banner_image/img4.jpg', NULL, NULL, '2021-06-17 11:27:33', '2021-06-17 11:32:49', '1'),
(23, '/banner_image/img15.jpg', NULL, NULL, '2021-06-17 12:06:29', '2021-06-17 12:06:33', '1'),
(24, '/banner_image/15349356331sZBfLBLI15349355474.jpg', NULL, NULL, '2021-12-22 12:06:03', '2021-12-22 12:18:34', '1'),
(25, '/banner_image/slide2.png', NULL, NULL, '2021-12-22 12:13:07', '2021-12-22 12:13:07', '1'),
(26, '/banner_image/005 -min.jpg', NULL, NULL, '2021-12-22 12:18:45', '2022-05-06 06:17:09', '1'),
(27, '/banner_image/pexels-photo-247431.jpeg', NULL, NULL, '2022-01-12 06:17:23', '2022-01-12 06:18:36', '1'),
(28, '/banner_image/pexels-photo-247431.jpeg', NULL, NULL, '2022-01-12 06:19:01', '2022-01-12 06:19:37', '1'),
(29, '/banner_image/photo-1575550959106-5a7defe28b56.JPG', NULL, NULL, '2022-02-02 08:41:22', '2022-02-02 08:45:50', '1'),
(30, '/banner_image/pexels-photo-247431.jpeg', NULL, NULL, '2022-02-02 08:45:17', '2022-02-02 08:45:46', '1'),
(31, '/banner_image/DSC_3951.jpg', NULL, NULL, '2022-02-09 07:17:51', '2022-02-09 07:19:37', '1'),
(32, '/banner_image/5.png', NULL, NULL, '2022-05-05 13:05:48', '2022-05-05 13:05:48', '0'),
(33, '/banner_image/4.png', NULL, NULL, '2022-05-05 13:06:22', '2022-05-05 13:06:22', '0'),
(34, '/banner_image/5.png', NULL, NULL, '2022-05-06 05:48:17', '2022-05-06 06:17:23', '1'),
(35, '/banner_image/1.png', NULL, NULL, '2022-05-06 06:16:08', '2022-05-06 06:16:08', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contact_address`
--

CREATE TABLE `contact_address` (
  `id` int NOT NULL,
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contact_address`
--

INSERT INTO `contact_address` (`id`, `image`, `name`, `contact`, `designation`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '/contact_address/1651575139.jpeg', 'Shri.Bhagwant Mann', '', 'Hon’ble Chief Minister', '2022-05-03 10:52:19', '2022-05-03 11:16:11', NULL),
(2, '/contact_address/1651575313.jpg', 'Shri.Lal Chand Kataruchak', '', 'Hon’ble Minister Of Forest & WIldlife Department , Punjab', '2022-05-03 10:55:13', '2022-05-03 10:55:42', NULL),
(3, '/contact_address/1651575414.png', 'Smt. Raji P. Shrivastava, IAS', '0172-2746522 , 9888030111', 'Add. Chief Secretary cum Financial Commissioner Deptt. of Forests & Wildlife Preservation', '2022-05-03 10:56:54', '2022-05-03 11:24:07', NULL),
(4, '/contact_address/1651575499.png', 'Sh. Raman Kant Mishra, IFS', '0172-2298010 , 9910461117', 'Principal Chief Conservator of Forests (Wildlife) & Chief Wildlife Warden, Punjab', '2022-05-03 10:58:19', '2022-05-03 10:58:19', NULL),
(5, '/contact_address/1651575558.png', 'Sh. Charchil Kumar, IFS', '0172-2740852 , 9417305306', 'Chief Conservator of Forests (Wildlife), Punjab', '2022-05-03 10:59:19', '2022-05-03 10:59:19', NULL),
(6, '/contact_address/1651575607.png', 'Sh. T. Gnana Prakash, IFS', '0172-2298010 , 9964471782', 'Conservator of Forests (Wildlife) Shiwalik Hills Circle, Punjab', '2022-05-03 11:00:07', '2022-05-03 11:00:07', NULL),
(7, '/contact_address/1651575657.png', 'Dr. Manish Kumar, IFS,', '0172-2298010, 9781632224', 'Conservator of Forests (Wildlife), Parks & Protected Area Circle, Punjab', '2022-05-03 11:00:57', '2022-05-03 11:00:57', NULL),
(8, '/contact_address/1651575686.png', 'Smt. K. Kalpana, IFS', '9412927001', 'Field Director,\r\nM.C. Zoological Park, Chhatbir', '2022-05-03 11:01:26', '2022-05-03 11:01:26', NULL),
(9, '/contact_address/1651575729.png', 'Sh. Nalin Yadav, IFS', '01632-279412 , 9810215780', 'Divisional Forest Officer (Wildlife), Ferozepur', '2022-05-03 11:02:09', '2022-05-03 11:02:09', NULL),
(10, '/contact_address/1651575766.png', 'Sh. Rajesh Mahajan, IFS', '0186-2250092 , 9814478400', 'Divisional Forest Officer (Wildlife), Pathankot', '2022-05-03 11:02:46', '2022-05-03 11:02:46', NULL),
(11, '/contact_address/1651575810.png', 'Sh. Dharamveer Dairu, IFS', '01881-294313 , 7597706403', 'Divisional Forest Officer (Wildlife), Ropar', '2022-05-03 11:03:30', '2022-05-03 11:03:30', NULL),
(12, '/contact_address/1651575845.png', 'Sh. Neeraj Kumar, PFS', '01826-2220083 , 9463596843', 'Divisional Forest Officer (Wildlife), Phillaur', '2022-05-03 11:04:06', '2022-05-03 11:04:06', NULL),
(13, '/contact_address/1651575898.png', 'Sh. Jugraj Singh, PFS', '0175-2970801 , 9872100828', 'Divisional Forest Officer (Wildlife), Patiala', '2022-05-03 11:04:59', '2022-05-03 11:04:59', NULL),
(14, '/contact_address/1651575936.png', 'Sh. Anjan Singh, PFS', '01882-250453 , 9465665715', 'Divisional Forest Officer (Wildlife), Hoshiarpur', '2022-05-03 11:05:36', '2022-05-03 11:05:36', NULL),
(15, '/contact_address/1652347080.jpg', 'neha', '9412927001', 'jhggggggg', '2022-05-12 09:16:58', '2022-05-12 09:23:24', '2022-05-12 09:23:24');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `category_name`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'Our Location', '2021-06-02 18:09:54', '2021-06-02 18:09:57', '0'),
(2, 'Call Us', '2021-06-02 18:10:00', '2021-06-02 18:10:03', '0'),
(3, 'Write some words', '2021-06-02 18:10:08', '2021-06-02 18:10:12', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_banner_detail`
--

CREATE TABLE `contact_us_banner_detail` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contact_us_banner_detail`
--

INSERT INTO `contact_us_banner_detail` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/contactUs/slide1.png', 'Contact', '2021-06-02 13:09:02', '2021-06-02 13:09:02', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_detail`
--

CREATE TABLE `contact_us_detail` (
  `id` int NOT NULL,
  `content_type_id` int NOT NULL,
  `content_type_name` varchar(255) NOT NULL,
  `image_icon` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contact_us_detail`
--

INSERT INTO `contact_us_detail` (`id`, `content_type_id`, `content_type_name`, `image_icon`, `description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Our Location', '/contactUs/location.png', 'Tower 2, Second floor, Sector 68, Sahibzada Ajit Singh Nagar, Punjab 160062', '2021-06-02 13:38:53', '2021-06-02 13:38:53', '0'),
(2, 2, 'Call Us', '/contactUs/call.png', '(0172) 229-8010', '2021-06-02 13:39:14', '2021-06-02 13:39:14', '0'),
(3, 3, 'Write some words', '/contactUs/message.png', 'support@wildlife.com', '2021-06-02 13:39:34', '2021-06-02 13:39:34', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_message`
--

CREATE TABLE `contact_us_message` (
  `id` int NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `your_message` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contact_us_message`
--

INSERT INTO `contact_us_message` (`id`, `full_name`, `email`, `subject`, `your_message`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'Neha Kumari', 'neha.kumari@emobx.com', 'regarding website', 'Hii', '2021-06-02 14:19:56', '2021-06-02 14:19:56', '0'),
(2, 'pHqghUme', 'sample@email.tst', '1', '20', '2021-07-19 07:24:14', '2021-07-19 07:24:14', '0'),
(3, 'Bhubnesh Kumar', 'bhubnesh.k@emobx.com', 'Testing', 'Testing the form submission,', '2022-02-02 07:14:50', '2022-02-02 07:14:50', '0'),
(4, 'Bhubnesh Kumar', 'bhubnesh.k@emobx.com', 'Testing', 'Testing the form submission,', '2022-02-02 07:15:38', '2022-02-02 07:15:38', '0'),
(5, 'Bhubnesh Kumar', 'bhubnesh.k@emobx.com', 'Testing', 'Testing the form submission,', '2022-02-02 07:15:39', '2022-02-02 07:15:39', '0'),
(6, '', '', '', '', '2022-02-09 14:08:21', '2022-02-09 14:08:21', '0'),
(7, '', '', '', '', '2022-02-09 14:08:23', '2022-02-09 14:08:23', '0'),
(8, '', '', '', '', '2022-02-09 14:08:24', '2022-02-09 14:08:24', '0'),
(9, '', '', '', '', '2022-02-09 14:08:24', '2022-02-09 14:08:24', '0'),
(10, '', '', '', '', '2022-02-09 14:08:24', '2022-02-09 14:08:24', '0'),
(11, '', '', '', '', '2022-02-09 14:08:24', '2022-02-09 14:08:24', '0'),
(12, '', '', '', '', '2022-02-09 14:08:24', '2022-02-09 14:08:24', '0'),
(13, '', '', '', '', '2022-02-09 14:08:25', '2022-02-09 14:08:25', '0'),
(14, '', '', '', '', '2022-02-09 14:08:25', '2022-02-09 14:08:25', '0'),
(15, 'neha', 'vishal@almabay.com', 'ddddddd', 'dddddddddddddddd', '2022-02-10 10:27:53', '2022-02-10 10:27:53', '0'),
(16, 'gdfgd', 'neha.kumari@gmail.com', 'dggggggggg', 'gfgggggggggggggggg', '2022-05-04 08:31:24', '2022-05-04 08:31:24', '0'),
(17, 'gdfgd', 'gdfgd@gmail.com', 'gdfgdf', 'gddddddddddd', '2022-05-04 08:32:35', '2022-05-04 08:32:35', '0'),
(18, 'Neha', 'neha.kumari@gmail.com', 'Wildlife Department', 'fsdddddddd', '2022-05-04 10:26:54', '2022-05-04 10:26:54', '0'),
(19, 'Neha', 'neha.kumari@gmail.com', 'dggggggggg', 'dffffffff', '2022-05-11 06:33:12', '2022-05-11 06:33:12', '0'),
(20, 'Neha', 'neha.kumari@gmail.com', 'testiiiiiiiiiii', 'ccccccccccx', '2022-05-11 06:34:50', '2022-05-11 06:34:50', '0'),
(21, 'Neha', 'neha.kumari@gmail.com', 'testiiiiiiiiiiifssssssssssssssss', 'fssssssssssssssss', '2022-05-11 06:44:05', '2022-05-11 06:44:05', '0'),
(22, 'zddddddd', 'neha.kumari@gmail.com', 'Regarding Department of wildlife punjab', 'dsfffffffffff', '2022-05-11 06:56:30', '2022-05-11 06:56:30', '0'),
(23, 'SonyaDuarp', 'woodthighgire1988@gmail.com', 'Hello Admin!', 'Hi. I\'m love sucks big dick. My profile here https://bunnyvv.space/click?o=6&a=1036', '2022-05-11 14:07:07', '2022-05-11 14:07:07', '0'),
(24, 'Eric', 'eric.jones.z.mail@gmail.com', 'Instead, congrats', 'Good day, \r\n\r\nMy name is Eric and unlike a lot of emails you might get, I wanted to instead provide you with a word of encouragement – Congratulations\r\n\r\nWhat for?  \r\n\r\nPart of my job is to check out websites and the work you’ve done with 5ohms.com definitely stands out. \r\n\r\nIt’s clear you took building a website seriously and made a real investment of time and resources into making it top quality.\r\n\r\nThere is, however, a catch… more accurately, a question…\r\n\r\nSo when someone like me happens to find your site – maybe at the top of the search results (nice job BTW) or just through a random link, how do you know? \r\n\r\nMore importantly, how do you make a connection with that person?\r\n\r\nStudies show that 7 out of 10 visitors don’t stick around – they’re there one second and then gone with the wind.\r\n\r\nHere’s a way to create INSTANT engagement that you may not have known about… \r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It lets you know INSTANTLY that they’re interested – so that you can talk to that lead while they’re literally checking out 5ohms.com.\r\n\r\nCLICK HERE https://jumboleadmagnet.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nIt could be a game-changer for your business – and it gets even better… once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation – immediately (and there’s literally a 100X difference between contacting someone within 5 minutes versus 30 minutes.)\r\n\r\nPlus then, even if you don’t close a deal right away, you can connect later on with text messages for new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is simple, easy, and effective. \r\n\r\nCLICK HERE https://jumboleadmagnet.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE https://jumboleadmagnet.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://jumboleadmagnet.com/unsubscribe.aspx?d=5ohms.com', '2022-05-19 21:14:44', '2022-05-19 21:14:44', '0'),
(25, 'Dr. Lorena Hauck', 'ba15a594dbb9@hubmails.us', 'Mireille Ondricka', 'Rem non quaerat dolorem odio. Autem et enim in odit ut. Praesentium et voluptas dolorem tenetur.', '2022-05-30 22:57:14', '2022-05-30 22:57:14', '0');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int NOT NULL,
  `district_name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `district_name`, `status`) VALUES
(1, 'Amritsar', '1'),
(2, 'Barnala', '1'),
(3, 'Bathinda', '1'),
(4, 'Faridkot', '1'),
(5, 'Fatehgarh Sahib', '1'),
(6, 'Fazilka', '1'),
(7, 'Ferozepur', '1'),
(8, 'Gurdaspur', '1'),
(9, 'Hoshiarpur', '1'),
(10, 'Jalandhar', '1'),
(11, 'Kapurthala', '1'),
(12, 'Ludhiana', '1'),
(13, 'Mansa', '1'),
(14, 'Moga', '1'),
(15, 'Muktsar', '1'),
(16, 'Nawanshahr (Shahid Bhagat Singh Nagar)', '1'),
(17, 'Pathankot', '1'),
(18, 'Patiala', '1'),
(19, 'Rupnagar', '1'),
(20, 'Sahibzada Ajit Singh Nagar (Mohali)', '1'),
(21, 'Sangrur', '1'),
(22, 'Tarn Taran', '1');

-- --------------------------------------------------------

--
-- Table structure for table `event_bg_images`
--

CREATE TABLE `event_bg_images` (
  `id` int NOT NULL,
  `bg_image` varchar(255) NOT NULL,
  `image_heading` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `event_bg_images`
--

INSERT INTO `event_bg_images` (`id`, `bg_image`, `image_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/event_bg_image/dresden-3681378_1920-edit.jpg', 'Wildlife is something which man cannot construct. Once it is gone, it is gone forever.\"\r\n\r\n-Joy Adamson.', '2021-06-04 12:22:41', '2022-01-31 12:03:11', '0');

-- --------------------------------------------------------

--
-- Table structure for table `event_master`
--

CREATE TABLE `event_master` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `event_master`
--

INSERT INTO `event_master` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(80001, 'Wildlife Santuary', '2022-05-02 12:57:01', '2022-05-02 13:03:09', NULL),
(80002, 'erwerw', '2022-05-02 13:31:49', '2022-05-02 13:31:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_master_gallery_images`
--

CREATE TABLE `event_master_gallery_images` (
  `id` int NOT NULL,
  `event_master_id` int NOT NULL,
  `event_master_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `event_master_gallery_images`
--

INSERT INTO `event_master_gallery_images` (`id`, `event_master_id`, `event_master_name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 80001, 'Wildlife Santuary', '/event_master/1651560032.jpg', '2022-05-03 06:40:32', '2022-05-03 07:43:14', '2022-05-03 07:43:14'),
(2, 80001, 'Wildlife Santuary', '/event_master/1651560079.jpg', '2022-05-03 06:41:19', '2022-05-03 07:43:48', '2022-05-03 07:43:48'),
(3, 80002, 'erwerw', '/event_master/1651561247.jpg', '2022-05-03 07:00:21', '2022-05-03 07:42:15', '2022-05-03 07:42:15'),
(4, 80001, 'Wildlife Santuary', '/event_master/1651586377.jpg', '2022-05-03 13:59:37', '2022-05-03 13:59:37', NULL),
(5, 80002, 'erwerw', '/event_master/PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-11 12:31:02', '2022-05-11 12:31:52', NULL),
(6, 80002, 'erwerw', '/event_master/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 12:31:38', '2022-05-11 12:31:43', '2022-05-11 12:31:43');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `footer_about`
--

CREATE TABLE `footer_about` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `footer_about`
--

INSERT INTO `footer_about` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/footer_about_images/slide1.png', 'About', '2021-05-05 14:10:58', '2021-12-28 05:47:18', '0'),
(2, '/footer_about_images/slide2.png', 'About', '2021-06-23 09:58:02', '2021-06-23 10:28:13', '1'),
(3, '/footer_about_images/slide1.png', 'Protected Areas', '2021-08-04 13:03:17', '2021-08-04 13:09:35', '1'),
(4, '/footer_about_images/pexels-photo-247431.jpeg', 'testing', '2022-01-12 09:13:40', '2022-01-12 09:16:23', '1'),
(5, '/footer_about_images/barheaded.png', 'Tourism ghjg', '2022-01-31 15:35:31', '2022-01-31 15:36:08', '1');

-- --------------------------------------------------------

--
-- Table structure for table `footer_about_detail`
--

CREATE TABLE `footer_about_detail` (
  `id` int NOT NULL,
  `banner_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `protected_area_heading` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `protected_area_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `footer_about_detail`
--

INSERT INTO `footer_about_detail` (`id`, `banner_id`, `title`, `description`, `protected_area_heading`, `protected_area_description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '1', 'Introduction', 'A protected area is defined as a geographical area that is protected against exploitation of any of its natural resources. According to IUCN (International Union of Conservation of Nature and Natural Resources), a protected area is a clearly defined geographical space that is recognized, dedicated and managed through legal or other effective means to achieve the long term conservation of nature with associated ecosystem services and cultural values (IUCN, 2008). In other words, these are those clearly demarcated geographical regions where conservation and management are outlined by law. Depending upon the level of protection, any protected area can be classified into six internationally recognized categories Ia. Strict Nature Reserve Ib. Wilderness AreaII National ParkIII Natural Monument or FeatureIV Habitat/Species Management AreaV Protected Landscape/SeascapeVI Protected area with sustainable use of natural resources(Table 1) (Dudley and Stolten, 2008). However, the classification system varies a little from the above mentioned in India. Under Wildlife protection Act (1972), the protected areas can be classified into four major categories i. e. National Parks, Wildlife Sanctuaries, Conservation Reserves and Community Reserves, in India. Protected Area Categories (India): All the four categories of protected areas in India arestriclty aimed toconserve the ecology and wildlife of the area therein. In national parks, various activities that may harm the wildlife residing in the region are not permitted. It includes forestry, poaching, hunting and grazing of livestock. Even private ownership is not allowed. The constitution of a national park emphasizes mainly on maintaining the ecological significance of the area and strictly reserving it for biodiversity and wildlife. Hence, any national park has its boundaries well defined and no alternation of the boundaries of the national can be made except on a resolution passed by Legislative Assembly of the State Government whereas such an alteration maybe done by the orders of state government in a wildlife sanctuary. A wildlife sanctuary is that protected area that is mainly dedicated to protect the wildlife and conserve the animal species. Restricted human activities are permitted inside the sanctuary but killing, shooting, hunting or capturing wildlife is prohibited except at the direction of higher authorities. Private ownership rights and forestry operations are permissible to an extent that do not affect wildlife adversely in a wildlife sanctuary. To improve the protection in and around existing or proposed national parks and wildlife sanctuaries, two new categories of protected areas, conservation reserves and community reserves, were introduced in the amendment to the Wildlife Protection Act of 1972 in, 2002). These protected areas are actually the migration corridors or connectors between the national parks and sanctuaries. Conservation reserves are uninhabited regions that are completely owned by Government of India but can be used by communities for their subsistence in case a part of the land is privately owned.', 'Protected Area Categories (India)', 'All the four categories of protected areas in India arestriclty aimed toconserve the ecology and wildlife of the area therein. In national parks, various activities that may harm the wildlife residing in the region are not permitted. It includes forestry, poaching, hunting and grazing of livestock. Even private ownership is not allowed. The constitution of a national park emphasizes mainly on maintaining the ecological significance of the area and strictly reserving it for biodiversity and wildlife. Hence, any national park has its boundaries well defined and no alternation of the boundaries of the national can be made except on a resolution passed by Legislative Assembly of the State Government whereas such an alteration maybe done by the orders of state government in a wildlife sanctuary. A wildlife sanctuary is that protected area that is mainly dedicated to protect the wildlife and conserve the animal species. Restricted human activities are permitted inside the sanctuary but killing, shooting, hunting or capturing wildlife is prohibited except at the direction of higher authorities. Private ownership rights and forestry operations are permissible to an extent that do not affect wildlife adversely in a wildlife sanctuary. To improve the protection in and around existing or proposed national parks and wildlife sanctuaries, two new categories of protected areas, conservation reserves and community reserves, were introduced in the amendment to the Wildlife Protection Act of 1972 in, 2002). These protected areas are actually the migration corridors or connectors between the national parks and sanctuaries. Conservation reserves are uninhabited regions that are completely owned by Government of India but can be used by communities for their subsistence in case a part of the land is privately owned.', '2021-05-05 14:10:58', '2022-01-31 15:36:01', '0'),
(2, '2', 'Introductionfdfdd', '<p><span style=\"color:rgb(35, 35, 35); font-family:nunito sans; font-size:18px\">A protected area is defined as a geographical area that is protected against exploitation of any of its natural resources. According to IUCN (International Union of Conservation of Nature and Natural Resources), a protected area is a clearly defined geographical space that is recognized, dedicated and managed through legal or other effective means to achieve the long term conservation of nature with associated ecosystem services and cultural values (IUCN, 2008). In other words, these are those clearly demarcated geographical regions where conservation and management are outlined by law. Depending upon the level of protection, any protected area can be classified into six internationally recognized categories Ia. Strict Nature Reserve Ib. Wilderness AreaII National ParkIII Natural Monument or FeatureIV Habitat/Species Management AreaV Protected Landscape/SeascapeVI Protected area with sustainable use of natural resources(Table 1) (Dudley and Stolten, 2008). However, the classification system varies a little from the above mentioned in India. Under Wildlife protection Act (1972), the protected areas can be classified into four major categories i. e. National Parks, Wildlife Sanctuaries, Conservation Reserves and Community Reserves, in India.</span></p>', 'Protected Area Categories (India)', '<p><span style=\"color:rgb(35, 35, 35); font-family:nunito sans; font-size:18px\">All the four categories of protected areas in India arestriclty aimed toconserve the ecology and wildlife of the area therein. In national parks, various activities that may harm the wildlife residing in the region are not permitted. It includes forestry, poaching, hunting and grazing of livestock. Even private ownership is not allowed. The constitution of a national park emphasizes mainly on maintaining the ecological significance of the area and strictly reserving it for biodiversity and wildlife. Hence, any national park has its boundaries well defined and no alternation of the boundaries of the national can be made except on a resolution passed by Legislative Assembly of the State Government whereas such an alteration maybe done by the orders of state government in a wildlife sanctuary. A wildlife sanctuary is that protected area that is mainly dedicated to protect the wildlife and conserve the animal species. Restricted human activities are permitted inside the sanctuary but killing, shooting, hunting or capturing wildlife is prohibited except at the direction of higher authorities. Private ownership rights and forestry operations are permissible to an extent that do not affect wildlife adversely in a wildlife sanctuary. To improve the protection in and around existing or proposed national parks and wildlife sanctuaries, two new categories of protected areas, conservation reserves and community reserves, were introduced in the amendment to the Wildlife Protection Act of 1972 in, 2002). These protected areas are actually the migration corridors or connectors between the national parks and sanctuaries. Conservation reserves are uninhabited regions that are completely owned by Government of India but can be used by communities for their subsistence in case a part of the land is privately owned.</span></p>', '2021-06-23 09:58:02', '2021-06-23 09:58:02', '1'),
(3, '3', 'Introduction', 'A protected area is defined as a geographical area that is protected against exploitation of any of its natural resources. According to IUCN (International Union of Conservation of Nature and Natural Resources), a protected area is a clearly defined geographical space that is recognized, dedicated and managed through legal or other effective means to achieve the long term conservation of nature with associated ecosystem services and cultural values (IUCN, 2008). In other words, these are those clearly demarcated geographical regions where conservation and management are outlined by law. Depending upon the level of protection, any protected area can be classified into six internationally recognized categories Ia. Strict Nature Reserve Ib. Wilderness AreaII National ParkIII Natural Monument or FeatureIV Habitat/Species Management AreaV Protected Landscape/SeascapeVI Protected area with sustainable use of natural resources(Table 1) (Dudley and Stolten, 2008). However, the classification system varies a little from the above mentioned in India. Under Wildlife protection Act (1972), the protected areas can be classified into four major categories i. e. National Parks, Wildlife Sanctuaries, Conservation Reserves and Community Reserves, in India.&nbsp;', 'Protected Area Categories (India)', 'A protected area is defined as a geographical area that is protected against exploitation of any of its natural resources. According to IUCN (International Union of Conservation of Nature and Natural Resources), a protected area is a clearly defined geographical space that is recognized, dedicated and managed through legal or other effective means to achieve the long term conservation of nature with associated ecosystem services and cultural values (IUCN, 2008). In other words, these are those clearly demarcated geographical regions where conservation and management are outlined by law. Depending upon the level of protection, any protected area can be classified into six internationally recognized categories Ia. Strict Nature Reserve Ib. Wilderness AreaII National ParkIII Natural Monument or FeatureIV Habitat/Species Management AreaV Protected Landscape/SeascapeVI Protected area with sustainable use of natural resources(Table 1) (Dudley and Stolten, 2008). However, the classification system varies a little from the above mentioned in India. Under Wildlife protection Act (1972), the protected areas can be classified into four major categories i. e. National Parks, Wildlife Sanctuaries, Conservation Reserves and Community Reserves, in India.&nbsp;', '2021-08-04 13:03:17', '2021-08-04 13:03:17', '0'),
(4, '4', 't', 't', 't', 't', '2022-01-12 09:13:40', '2022-01-12 09:13:40', '0'),
(5, '5', 'mhvghk', 'kghkghkhg', NULL, NULL, '2022-01-31 15:35:31', '2022-01-31 15:35:31', '0');

-- --------------------------------------------------------

--
-- Table structure for table `footer_policy_pdf`
--

CREATE TABLE `footer_policy_pdf` (
  `id` int NOT NULL,
  `policy_guideline` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `footer_policy_pdf`
--

INSERT INTO `footer_policy_pdf` (`id`, `policy_guideline`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/footer_about_policypdf/Policy Guidelines for compliance by industrial units in the ESZ surrounding wildlife sanctuaries, zoos.pdf', '2021-05-06 08:00:44', '2021-05-06 08:09:13', '0'),
(2, '/footer_about_policypdf/pdf18.pdf', '2021-06-23 10:30:48', '2021-06-23 10:34:23', '1'),
(3, '/footer_about_policypdf/dummy (1).pdf', '2022-01-12 09:16:44', '2022-01-12 09:18:14', '1'),
(4, '/footer_about_policypdf/pdf14.pdf', '2022-01-31 14:08:00', '2022-01-31 14:08:14', '1');

-- --------------------------------------------------------

--
-- Table structure for table `footer_scheme`
--

CREATE TABLE `footer_scheme` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `footer_scheme`
--

INSERT INTO `footer_scheme` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/footer_scheme_images/slide1.png', 'Scheme', '2021-05-05 11:22:22', '2021-12-28 05:45:48', '0'),
(2, '/footer_scheme_images/elephant.jpg', 'Harike Wildlife Sanctuary', '2021-08-04 12:59:50', '2021-08-04 12:59:55', '1'),
(3, '/footer_scheme_images/pexels-photo-247431.jpeg', 'testing', '2022-01-12 09:09:08', '2022-01-12 09:11:01', '1');

-- --------------------------------------------------------

--
-- Table structure for table `footer_scheme_name`
--

CREATE TABLE `footer_scheme_name` (
  `id` int NOT NULL,
  `banner_id` varchar(255) NOT NULL,
  `name_of_scheme` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `footer_scheme_name`
--

INSERT INTO `footer_scheme_name` (`id`, `banner_id`, `name_of_scheme`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '1', 'Conservation, Management and Development of Wildlife in the state.', '2021-05-05 11:22:22', '2022-02-02 07:14:01', '0'),
(2, '1', 'Eco-tourism in Harike Wildlife Sanctuary\r\n', '2021-05-05 11:22:22', '2021-05-05 11:22:22', '0'),
(3, '1', 'Assistance for Development of Selected Zoos\r\n', '2021-05-05 11:22:22', '2021-05-05 11:22:22', '0'),
(4, '1', 'Conservation, Management and Development of Wildlife in the State\r\n', '2021-05-05 11:22:22', '2021-05-05 11:22:22', '0'),
(5, '2', 'Assistance to Punjab State Wet Lands Authority for Conservation and Management of Wetlands in the State (60:40)', '2021-08-04 12:59:50', '2021-08-04 12:59:50', '0'),
(6, '3', '', '2022-01-12 09:09:08', '2022-01-12 09:09:08', '0'),
(7, '1', 'hjkhjk', '2022-02-01 04:48:10', '2022-02-01 04:48:24', '1'),
(8, '1', 'Test scheme', '2022-02-02 07:13:22', '2022-02-02 07:13:30', '1');

-- --------------------------------------------------------

--
-- Table structure for table `forest_detail`
--

CREATE TABLE `forest_detail` (
  `id` int NOT NULL,
  `forest_heading` varchar(255) NOT NULL,
  `forest_description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->notdeleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frontend_protectedarea_notification`
--

CREATE TABLE `frontend_protectedarea_notification` (
  `id` int NOT NULL,
  `front_protarea_id` int NOT NULL,
  `subcat_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `pdf_file` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_protectedarea_notification`
--

INSERT INTO `frontend_protectedarea_notification` (`id`, `front_protarea_id`, `subcat_id`, `title`, `pdf_file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/pdf3.pdf', '2022-02-08 09:36:37', '2022-02-09 06:22:39', '2022-02-09 06:22:39'),
(2, 2, '', 'Notification F. No. 150/150 dated 28.02.1952', '/protected_area/pdf13.pdf', '2022-02-08 09:57:01', '2022-02-09 06:23:22', NULL),
(3, 3, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/1644314284.pdf15.pdf', '2022-02-08 09:58:04', '2022-02-08 09:58:04', NULL),
(4, 4, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/1644314411.pdf26.pdf', '2022-02-08 10:00:11', '2022-02-08 10:00:11', NULL),
(5, 5, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/1644314637.pdf15.pdf', '2022-02-08 10:03:57', '2022-02-08 10:03:57', NULL),
(6, 6, '', 'gdddddddd', '/protected_area/1644314963.pdf15.pdf', '2022-02-08 10:09:23', '2022-02-08 10:09:23', NULL),
(7, 7, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/1644315055.pdf14.pdf', '2022-02-08 10:10:55', '2022-02-08 10:10:55', NULL),
(8, 8, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/1644315107.pdf14.pdf', '2022-02-08 10:11:47', '2022-02-08 10:11:47', NULL),
(9, 9, '', 'Flight of Coots at Harike Wildlife Sanctuary', '/protected_area/1644315513.pdf15.pdf', '2022-02-08 10:18:33', '2022-02-10 05:20:28', '2022-02-10 05:20:28'),
(10, 1, '', 'Notification F. No. 150/150 dated 28.02.1952', '/protected_area/pdf15.pdf', '2022-02-08 10:26:13', '2022-02-09 06:22:30', NULL),
(11, 1, '', 'Notification No. S.O. 1055E dated 11.03.2016 reg. Eco Sensitive Zone', '/protected_area/pdf13.pdf', '2022-02-08 10:26:13', '2022-02-09 06:22:36', NULL),
(12, 2, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003', '/protected_area/1644316166.1643612607.Bir gurdialpura 27.08.2003.pdf', '2022-02-08 10:29:26', '2022-02-08 10:47:27', '2022-02-08 10:47:27'),
(13, 2, '', 'Notification No. S.O. 2275(E) dated 01.07.2016 regarding Eco-Sensitive Zone', '/protected_area/1644316166.1643612607.Bir gurdialpura 27.08.2003.pdf', '2022-02-08 10:29:26', '2022-02-08 10:47:27', '2022-02-08 10:47:27'),
(14, 1, '', 'sdfdsfdsfsad fone', '/protected_area/pdf4.pdf', '2022-02-08 12:16:27', '2022-02-09 06:22:43', '2022-02-09 06:22:43'),
(15, 1, '', 'sdfdsfdsfsad fone', '/protected_area/pdf4.pdf', '2022-02-08 12:16:38', '2022-02-09 06:22:46', '2022-02-09 06:22:46'),
(16, 2, '', 'dfvgdfd', '/protected_area/pdf14.pdf', '2022-02-08 12:21:32', '2022-02-08 12:38:24', '2022-02-08 12:38:24'),
(17, 3, '', 'Notification No. 150/50 dated 28.02.1952', '/protected_area/1644388302.pdf14.pdf', '2022-02-09 06:31:42', '2022-02-09 06:31:42', NULL),
(18, 4, '', 'Notification F.No. 150/50 dated 28.02.1952 under Fauna of Patiala Act.', '/protected_area/1644388475.pdf3.pdf', '2022-02-09 06:34:35', '2022-02-09 06:34:35', NULL),
(19, 4, '', 'Govt. of India\'s Notification No. 350(E) Dated 16.02.2017 reg. Declaration of Eco-Sensitive Zone dated 27.8.2003.', '/protected_area/1644388475.pdf13.pdf', '2022-02-09 06:34:35', '2022-02-09 06:34:35', NULL),
(20, 4, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003.', '/protected_area/1644388475.pdf13.pdf', '2022-02-09 06:34:35', '2022-02-09 06:34:35', NULL),
(21, 5, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003.', '/protected_area/1644388628.pdf34.pdf', '2022-02-09 06:37:08', '2022-02-09 06:37:08', NULL),
(22, 5, '', 'Notification F. No. 150/50 dated 28.02.1952 under Fauna of Patiala Act.', '/protected_area/1644388628.pdf13.pdf', '2022-02-09 06:37:08', '2022-02-09 06:37:08', NULL),
(23, 5, '', 'Notification No. 150/50 dated 28.2.1951', '/protected_area/1644388628.pdf24.pdf', '2022-02-09 06:37:08', '2022-02-09 06:37:08', NULL),
(24, 6, '', 'Notification F. No. 150/50 dated 28.02.1952 under Fauna of Patiala Act.', '/protected_area/1644388779.pdf26.pdf', '2022-02-09 06:39:39', '2022-02-09 06:39:39', NULL),
(25, 6, '', 'Govt. of India\'s Notification No. 2483(E) Dated 21.07.2016 reg. Declaration of Eco-Sensitive Zone', '/protected_area/1644388779.pdf24.pdf', '2022-02-09 06:39:39', '2022-02-09 06:39:39', NULL),
(26, 7, '', 'Notification No. 150/50 dated 28.2.1952', '/protected_area/1644388920.pdf2.pdf', '2022-02-09 06:42:00', '2022-02-09 06:42:00', NULL),
(27, 7, '', 'Notification No. 150/50 dated', '/protected_area/1644388920.pdf12.pdf', '2022-02-09 06:42:00', '2022-02-09 06:42:00', NULL),
(28, 8, '', 'Notification No. 150/50 dated 28.2.1951', '/protected_area/1644389285.pdf33.pdf', '2022-02-09 06:48:05', '2022-02-09 06:48:05', NULL),
(29, 8, '', 'Notification No. 150/50 dated 28.2.1950', '/protected_area/1644389285.pdf17.pdf', '2022-02-09 06:48:05', '2022-02-09 06:48:05', NULL),
(30, 9, '', 'Notification No. 34/7/99-Ft-IV/16393 Dated 18.11.1999', '/protected_area/Harike notification 18.11.1999.pdf', '2022-02-09 06:51:26', '2022-02-10 05:21:17', NULL),
(31, 9, '', 'Govt. of India\'s Notification No. S.O. 1568 (E) Dated 15.05.2017 reg. Declaration of 100 Mtr. Eco-Sensitive Zone around sanctuary', '/protected_area/Harike Wildlife Sanctuary, Punjab.pdf', '2022-02-09 06:51:26', '2022-02-10 05:22:53', NULL),
(32, 10, '', 'Notification No. 34/10/99-Ft-IV/7182 Dated 08.06.1999', '/protected_area/WL Sanctuary Tekhni Rehmpura dt. 8.6.1999.pdf', '2022-02-09 06:54:00', '2022-02-10 04:59:40', NULL),
(33, 10, '', 'Govt. of India\'s Notification No. S.O. 3597(E) Dated 29.11.2016 reg. Declaration of 100 Mtr. Eco-Sensitive Zone around sanctuary', '/protected_area/Takhni - Rehmapur Wildlife Sanctuary Punjab, FINAL05-12-2016.pdf', '2022-02-09 06:54:00', '2022-02-10 04:52:37', NULL),
(34, 11, '', 'Notification No. 150/50 dated 28.2.1951', '/protected_area/1644389780.pdf11.pdf', '2022-02-09 06:56:20', '2022-02-09 06:56:20', NULL),
(35, 11, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003', '/protected_area/1644389780.pdf19.pdf', '2022-02-09 06:56:20', '2022-02-09 06:56:20', NULL),
(36, 12, '', 'Notification No. 150/50 dated 28.2.1951', '/protected_area/1644389979.pdf27.pdf', '2022-02-09 06:59:39', '2022-02-09 06:59:39', NULL),
(37, 12, '', 'Notification No. 150/50 dated 28.2.1945', '/protected_area/1644389979.pdf26.pdf', '2022-02-09 06:59:39', '2022-02-09 06:59:39', NULL),
(38, 13, '', 'Notification No. 150/50 dated 28.2.1951', '/protected_area/1644390247.pdf19.pdf', '2022-02-09 07:04:07', '2022-02-09 07:04:07', NULL),
(39, 13, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003', '/protected_area/1644390247.pdf24.pdf', '2022-02-09 07:04:07', '2022-02-09 07:04:07', NULL),
(40, 14, '', 'Notification No.34/4/2015/FT-V/448962/1 dated 27.03.2015', '/protected_area/1644390564.pdf25.pdf', '2022-02-09 07:09:24', '2022-02-09 07:09:24', NULL),
(41, 14, '', 'Notification No. 150/50 dated 28.2', '/protected_area/1644390564.pdf28.pdf', '2022-02-09 07:09:24', '2022-02-09 07:09:24', NULL),
(42, 15, '', 'Notification No.46/78/2007/FT-V/6084 dated 22.06.2007', '/protected_area/1644390734.pdf13.pdf', '2022-02-09 07:12:14', '2022-02-09 07:12:14', NULL),
(43, 15, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003.', '/protected_area/1644390734.pdf23.pdf', '2022-02-09 07:12:14', '2022-02-09 07:12:14', NULL),
(44, 16, '', 'Notification No. 34/13/2007/FT-V/6133 Dated 28.06.2007', '/protected_area/1644391477.pdf6.pdf', '2022-02-09 07:24:37', '2022-02-09 07:24:37', NULL),
(45, 16, '', 'Notification No. 34/31/03/Ft-4/14758 dated 27.8.2003.', '/protected_area/1644391477.pdf22.pdf', '2022-02-09 07:24:37', '2022-02-09 07:24:37', NULL),
(46, 17, '', 'Notification No. 34/12/2017/FT-5/1052786/1 Dated 29.08.2017', '/protected_area/1644391617.pdf6.pdf', '2022-02-09 07:26:57', '2022-02-09 07:26:57', NULL),
(47, 17, '', 'Notification No. 150/50 dated.', '/protected_area/1644391617.pdf15.pdf', '2022-02-09 07:26:57', '2022-02-09 07:26:57', NULL),
(48, 18, '', 'Notification No. 34/8/2010/FT-5/3008 Dated 31.03.2010', '/protected_area/1644391772.pdf6.pdf', '2022-02-09 07:29:32', '2022-02-09 07:29:32', NULL),
(49, 18, '', 'Notification No. 150/50 dated 28.2', '/protected_area/1644391772.pdf19.pdf', '2022-02-09 07:29:32', '2022-02-09 07:29:32', NULL),
(50, 19, '', 'Notification No. 34/13/2017/FT-5/1057481/1 Dated 05.09.2017', '/protected_area/1644391905.pdf27.pdf', '2022-02-09 07:31:45', '2022-02-09 07:31:45', NULL),
(51, 19, '', 'Notification No. 150/50 dated 28.2.1951', '/protected_area/1644391905.pdf4.pdf', '2022-02-09 07:31:45', '2022-02-09 07:31:45', NULL),
(52, 20, '', 'Notification No. 34/11/2017/FT-5/057477/1 Dated 05.09.2017', '/protected_area/1644392034.pdf5.pdf', '2022-02-09 07:33:54', '2022-02-09 07:33:54', NULL),
(53, 20, '', 'Notification No. 34/11/2017-FT-5/1177745/1 Dated 05.03.2018', '/protected_area/1644392034.pdf18.pdf', '2022-02-09 07:33:54', '2022-02-09 07:33:54', NULL),
(54, 21, '', 'Notification No. 34/13/2017-FT-5/1052756/1 Dated 29.08.2017', '/protected_area/1644392165.pdf30.pdf', '2022-02-09 07:36:05', '2022-02-09 07:36:05', NULL),
(55, 21, '', 'Notification No. 150/50 dated 28.2', '/protected_area/1644392165.pdf27.pdf', '2022-02-09 07:36:05', '2022-02-09 07:36:05', NULL),
(56, 22, '', 'Notification No. 34/12/2019-FT-5/1499748/1 Dated 11.06.2019', '/protected_area/1644392303.pdf6.pdf', '2022-02-09 07:38:23', '2022-02-09 07:38:23', NULL),
(57, 22, '', 'Notification No. 150/50 dated 28.2', '/protected_area/1644392303.pdf32.pdf', '2022-02-09 07:38:23', '2022-02-09 07:38:23', NULL),
(58, 23, '', 'hdhd', '/protected_area/1651755837.pdf5.pdf', '2022-05-05 13:03:57', '2022-05-05 13:03:57', NULL),
(59, 1, '10001', 'gdfgdfggfdgdf', '/protected_area/pdf13.pdf', '2022-05-06 14:52:02', '2022-05-06 14:52:23', '2022-05-06 14:52:23'),
(60, 25, '10002', 'sdfsdf', '/protected_area/1652091688.2020-holiday-calendar.pdf', '2022-05-09 10:21:28', '2022-05-09 10:21:46', '2022-05-09 10:21:46'),
(61, 25, '10002', 'vfvdddd', '/protected_area/1652091688.2020-holiday-calendar.pdf', '2022-05-09 10:21:28', '2022-05-09 10:21:46', '2022-05-09 10:21:46'),
(62, 26, '10013', 'hdhd', '/protected_area/1652091746.2020-holiday-calendar.pdf', '2022-05-09 10:22:26', '2022-05-09 10:24:09', '2022-05-09 10:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `frontend_protectedwetland_notification`
--

CREATE TABLE `frontend_protectedwetland_notification` (
  `id` int NOT NULL,
  `front_protwet_id` int NOT NULL,
  `type_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `pdf_file` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_protectedwetland_notification`
--

INSERT INTO `frontend_protectedwetland_notification` (`id`, `front_protwet_id`, `type_id`, `title`, `pdf_file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '150001', 'gddd', '/protected_wetland_images/pdf3.pdf', '2022-05-06 09:56:13', '2022-05-06 14:24:18', NULL),
(2, 1, '150001', 'fggggggghgggggg', '/protected_wetland_images/pdf25.pdf', '2022-05-06 09:56:52', '2022-05-06 14:24:18', NULL),
(3, 2, '150001', 'hhhhhhhhhhhhhhhhhhhhhhhh', '/protected_wetland_images/pdf14.pdf', '2022-05-06 09:57:41', '2022-05-06 14:24:18', NULL),
(4, 1, '150001', 'dgddgdfg', '/protected_wetland_images/pdf13.pdf', '2022-05-06 13:55:14', '2022-05-06 14:24:18', NULL),
(11, 30, '150001', 'fsdddddddddd', '/protected_wetland_images/1652081028.pdf5.pdf', '2022-05-09 07:23:48', '2022-05-09 07:23:48', NULL),
(12, 31, '150001', 'dassssss', '/protected_wetland_images/1652081177.pdf5.pdf', '2022-05-09 07:26:17', '2022-05-09 07:49:01', '2022-05-09 07:49:01'),
(13, 31, '150001', 'dsggggggggg', '/protected_wetland_images/1652081177.pdf17.pdf', '2022-05-09 07:26:17', '2022-05-09 07:49:01', '2022-05-09 07:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `frontend_protected_area`
--

CREATE TABLE `frontend_protected_area` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int NOT NULL,
  `subcategory_id` int NOT NULL,
  `district` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `status_of_land` varchar(255) NOT NULL,
  `imp_fauna` text NOT NULL,
  `imp_flora` text NOT NULL,
  `map_lat` varchar(255) NOT NULL,
  `map_long` text NOT NULL,
  `zoom_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_protected_area`
--

INSERT INTO `frontend_protected_area` (`id`, `banner_image`, `description`, `category_id`, `subcategory_id`, `district`, `location`, `area`, `status_of_land`, `imp_fauna`, `imp_flora`, `map_lat`, `map_long`, `zoom_level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '/protected_area/slide1.png', '<p>Bir Moti Bagh Wildlife Sanctuary falls in the jurisdiction of Patiala Forest Division (Wildlife), in the tehsil Patiala of Patiala district. It is situated on the Patiala &ndash; Dakala Road about 5 km from Patiala town. The area lies between 76 20&rsquo; to 76 25&rsquo; East longitude and 30 15&rsquo; to 30 20&rsquo; North Latitude. The Survey of India toposheet number covering the sanctuary is 53 B/7 (Scale 1&rdquo;= 1 mile). Total area of Bir Moti Bagh is 524.48 ha as notified as protected forest under the section 29, chapter IV of the Indian Forest Act, 1927. It was notified as a Wildlife Sanctuary with notification No. F-150/50 Dt 28/02/1952 under Fauna of Patiala Act. Bir Moti bagh WLS is divided into 24 Compartments for management purpose. Bir Moti Bagh WLS is approachable by road. One can reach to Patiala by Rail and then can take either bus or taxies for Bir Moti Bagh which is 8 Km from Patiala Railway Station. On way to sanctuary one can see historically significant monuments of Qila Mubarak, famous Baradari Gardens, National institute of Sports, Shish Mahal and North Zone Cultural Centre. The entry to the sanctuary is from the Patiala &ndash; Dakala link road which passes along its southern fringe.</p>', 50001, 10001, 'Patiala', '5 km from Patiala on Patiala – Dakala Road', '654.00 Hectare', 'Government', '<p>Main wildlife species found in the sanctuary are Black buck, Chital, Hog deer, Blue bull, Wild boar, Jackal, Rhesus Monkeys, Peafowl, Brahmany Myna, Black and Grey Partridges and Quails.</p>', '<p>As per forest classification of Champion and Seth, the forests in the sanctuary fall under the sub- group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Prosopis juliflora, Acacia nilotica, Delbergia sissoo, Albizia lebbeck etc.</p>', '30.2889659', '76.3852454', '14', '2022-02-08 10:26:13', '2022-05-10 08:46:51', NULL),
(2, '/protected_area/slide1.png', 'Bir Gurdialpura wildlife Sanctuary falls in the jurisdiction of Patiala Forest Division (Wildlife), in the tehsil Samana of Patiala district. The area lies between 76 10&rsquo; E to 76 15&rsquo; E longitude and 30 00&rsquo; N to 30 05&rsquo; N Latitude of the Survey of India toposheet number 53 B/4 (Scale 1&rdquo;= 1 mile). Total area of Bir is 1533 acres (620.53 hectares). It falls in the east of the state boundary adjoining Haryana. The River Gaggar critises the Sanctuary and divided into two dosimeter parts. There is no distinction between core and buffer area. The sanctuary comprises only 17% of total protected areas of Patiala Forest Division (3600.21 hac). The whole tract is plain and on its north, south and east, there are villages inhabiting mostly agricultural communities. Biogeographically, it falls in Indus plain zone of biogeography province -4A, the Punjab plains of Semi-Arid Zone (Rodgers and Panwar, 1988). Bir Gurdialpura WLS is approachable by bus only. It is about 15 Km from Samana, the Tehsil headquarter and approx 45 km from Patiala the Distt Headquarter. Both the private and govt. buses are available to reach the sanctuary from Samana as well as Patiala.', 50001, 10002, 'Patiala', '45 km. from Patiala city and 15 km from Samana on Patiala – Samana road', '620.53 Hectare', 'Government', 'Main wildlife species found in the sanctuary are Blue bull, Hog deer, Hare, Jungle cat, Jackal, Rhesus Monkeys, Peafowl, Black and Grey Partridges, Dove and Spotted owlet.', 'As per forest classification of Champion and Seth, the forests in the sanctuary falls under the sub- group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Kikkar, Phalahi, Reru, Safeda, Neem, Prosopis juliflora, Acacia nilotica, Delbergia sissoo, Albizia lebbeck etc.', '30.0496632', '76.187423', '17', '2022-02-08 10:29:26', '2022-05-03 07:26:11', NULL),
(3, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10003, 'Patiala', '15 km. from Patiala on Patiala Devigarh road', '661.66 Hectare', 'Government', 'Main wildlife species found in the sanctuary are Porcupine, Black Buck, Sambar, Blue bull, Jungle cat, Jackal, Rhesus Monkeys, Peafowl, Rose, Black and Grey Partridges, Dove and Spotted owlet.', 'As per forest classification of Champion and Seth, the forests in the sanctuary falls under the sub - group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Kikkar, Phalahi, Reru, Safeda, Neem, Prosopis juliflora, Acacia nilotica, Delbergia sissoo, Albizia lebbeck etc.', '30.1824595', '76.4302553', '13', '2022-02-09 06:31:42', '2022-05-03 07:27:57', NULL),
(4, '/protected_area/slide1.png', 'Bir Mehas Wildlife Sanctuary falls in the jurisdiction of Patiala Forest Division (Wildlife), in the tehsil Nabha of Patiala district. The area lies between 76 5&rsquo; E to 76 15&rsquo; E longitude and 30 15&rsquo; N to 30 20&rsquo; N Latitude of the Survey of India toposheet number 53 B/3 (Scale 1&rdquo;= 8 mile). Total area of Bir is 123.43 hact. There is no distinction between core and buffer area. The sanctuary comprises only 3.43% of total protected areas of Patiala Forest Division (3600.21 hac). The whole tract is plain and on its north, south and east, there are villages inhabiting mostly agricultural communities. Biogeographically, it falls in Indus plain zone of biogeography province -4A, the Punjab plains of Semi-Arid Zone (Rodgers and Panwar, 1988). Bir Mehas WLS is easily approachable. It is about 1 Km from Nabha Bus Stand, the nearest Tehsil and approx 28km from Patiala the Distt Headquarter. Both the private and govt. buses are available to reach the sanctuary from Nabha as well as Patiala.', 50001, 10004, 'Patiala', 'Nabha-Malerkotla Road', '123.43 hectare', 'Government', 'Main wildlife species found in the sanctuary are Blue bull, Jungle cat, Jackal, Rhesus Monkeys, Peafowl, Black and Grey Partridges etc.', 'As per forest classification of Champion and Seth, the forests in the sanctuary falls under the sub- group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Kikkar, Eucalyptus, Shisham, Khair, Phalahi, Jamun, Beri, Mulberry, etc.', '30.8781485', '75.3125674', '', '2022-02-09 06:34:35', '2022-02-09 06:34:35', NULL),
(5, '/protected_area/slide1.png', 'Bir Dosanjh Wildlife Sanctuary has been derived probably from its proximity to village Dosanjh. This WLS falls in the jurisdiction of Nabha tehsil of Patiala Forest Division (Wildlife) and district. It is situated on the Nabha-Chawdhary Majra-Jorrepul road at about 28-30 km from Patiala city and about 4 km from Nabha town. &nbsp;The area is situated at 30&deg;25&#39; to 30&deg; 27&rsquo; North latitude and 76&deg;08&#39; to 76&deg; 10&rsquo; East longitude. The Survey of India toposheet number covering the sanctuary is 53 B/7 (Scale 1&rdquo;= 1 mile). The total area of the Bir Dosanjh is 517.59 hectare as notified by Punjab Govt. No. 2026/Ft (IV)-61/3654, dated 5th July 1961 by the provision to sub section (3) of section 29 of the Patiala Forest Act, 1999 as second class forest. Bir Dosanjh WLS is divided into 26 Compartments for management purpose.\r\n\r\nBir Dosanjh WLS is approachable both by road and rail. One can reach Patiala or Nabha by rail or road. And then can travel to WLS by road via Nabha town. Also one can reach Nabha town by rail and can travel by road to WLS. Nabha is 28 km by road from Patiala and WLS is about 4 km from Nabha. The entry to the sanctuary is from the Nabha-Chawdhary Majra-Jorrepul', 50001, 10005, 'Patiala', 'Tehsil Nabha on Nabha-Bhadson Road.', '517.59 hectare', 'Government', 'Main wildlife species found in the sanctuary are Blue bull, Jungle cat, Jackal, Rhesus Monkeys, Peafowl, Black and Grey Partridges etc.', 'As per forest classification of Champion and Seth, the forests in the sanctuary falls under the sub- group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Kikkar, Eucalyptus, Shisham, Khair, Phalahi, Jamun, Beri, Mulberry, etc.', '30.8757334', '75.3125543', '', '2022-02-09 06:37:08', '2022-02-09 06:37:08', NULL),
(6, '/protected_area/slide1.png', 'Bir Bhadson Wildlife Sanctuary falls in the jurisdiction of Nabha tehsil of Patiala Forest Division (Wildlife) and district. It is situated on the Nabha-Gobindgarh road at about 30 km from Patiala city. it was first notified as a Wildlife Sanctuary (WLS) vide Punjab Govt. Notification No.F-150/50 dated 28/02/1952. The area is situated at 30 30&#39; to 30 25&rsquo; North latitude and 76 10&#39; to 76 15&rsquo; East longitude. The Survey of India toposheet number covering the sanctuary is 53 B/2 (Scale 1&rdquo;= 1 mile). The total area of the Bir Bhadson is 1022.63 hectare as notified by Punjab Govt. No. 2026/Ft (IV)-61/3654, dated 5th July 1961 by the provision to sub section (3) of section 29 of the Patiala Forest Act, 1999 as second class forest.\r\nBir Bhadson Wild Life Sanctuary is approachable by road. One can reach to Patiala by rail or bus and then can take bus or taxi for Bir Bhadson WLS which is nearly 30 kms from Patiala. One can also reach up to Nabha town also which is the nearest railway station on Patiala Bhatinda railway line to reach Bir Bhadson WLS. The Bir Bhadson WLS is also accessible from the towns of Amloh and Sirhind by road. The entry to the sanctuary is from Nabha-Gobindgarh road which touches its eastern boundary.', 50001, 10006, 'Patiala', 'Left side of Bhadson-Gobindgarh Road.', '1022.63 hectare', 'Government', 'Main wildlife species found in the sanctuary are Blue bull, Jungle cat, Jackal, Rhesus Monkeys, Peafowl, Black and Grey Partridges, Hare, Spotted owlet etc.', 'As per forest classification of Champion and Seth, the forests in the sanctuary falls under the sub- group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Kikkar, Eucalyptus, Phalahi, Jamun, Beri, Mulberry, Dek, Mesquite etc.', '30.5127589', '76.1934153', '', '2022-02-09 06:39:39', '2022-02-09 06:39:39', NULL),
(7, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10007, 'Sangrur', '3 Km from Sangrur on Sohian Road', '264.40 hectare', 'Government', 'Main wildlife species found in the sanctuary are Blue bull, Jungle cat, Jackal, Rhesus Monkeys, Peafowl, Black and Grey Partridges, Hare, Spotted owlet etc.', 'As per forest classification of Champion and Seth, the forests in the sanctuary falls under the sub- group 5(B) Northern Tropical Dry Mixed deciduous forest type. The main tree species are Kikkar, Eucalyptus, Phalahi, Jamun, Beri, Mulberry, Dek, Mesquite etc.', '30.512458', '75.9307239', '', '2022-02-09 06:42:00', '2022-02-09 06:42:00', NULL),
(8, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10008, 'Ferozepur', 'Tehsil Abohar, 8 Km from Abohar City.', '18650 Hectare', 'Private Land of 13 Bishnoi’s Villages', 'Main wildlife species found in the sanctuary are Black buck and Blue bull.', 'As per forest classification the sanctuary falls under the Arid to semi-arid type. The main tree species are A. tortilis, Albizia lebbeck, Azadirachata indica, Dalbergia sissoo, Melia azedarach, Prosopis cineraria and P. juliflora etc.', '30.149641', '74.2046163', '', '2022-02-09 06:48:05', '2022-02-09 06:48:05', NULL),
(9, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10009, 'Ferozepur, Tarn Taran and Kapurthala', '58 km. from Amritsar-Ferozepur Road.', '8600 Hectares', 'Government', 'Main wildlife species found in the sanctuary are Large Cormorant, Darter, Purple Moorhen, Bar Headed Goose, Pintail, Common Teal, Shovellor, Common Poachard, Red Crested Poachard, White-eyed Pochard, Indus River Dolphin etc. Recgnised as Ramsar Site and attracts large number of migratory birds during the winter months.', 'As per forest classification the sanctuary falls under the Northern Tropical Dry Deciduous Forest type. The main tree species are Acacia nilotica, Dalbergia sissoo, Eucalyptus sissoo, Albizia lebbeck, Ficus religiosa, Azadirachata indica, Ficus benghalensis, Mangifera indica, Prosopis spicigera etc.', '31.1468228', '75.0106744', '', '2022-02-09 06:51:26', '2022-02-09 06:51:26', NULL),
(10, '/protected_area/036  (3).jpg', '<p>Takhni-Rehmapur Wildlife Sanctuary&nbsp;is a representative area of Shiwalik range of the Himalayas passing through Ropar, Hoshiarpur and Gurdaspur Districts of Punjab bordering H.P. and J &amp; K states. It is located in district Hoshiarpur near Village Mehngrowal. The Notified area of Takhni Rehmapur Wildlife Sanctuary is 382 ha or 956 acres and falls in Toposheet No.44 M/14. The notified area consists of 498 acre area of Village Takhni and 458 acre area of village Rehmapur. The sanctuary forms a part of the Shiwalik Range running along the border of H.P. and J &amp; K . It is 26 kms from Hoshiarpur via Haryana &nbsp; town. It can also be approached direct from Hoshiarpur after crossing the Mehngrowal choe and thus the distance is 14 KMs. There is no direct rail link and the nearest railhead is Hoshiarpur. The nearest civil airport is Chandigarh/Amritsar which is about 150 KMs from the sanctuary. The State transport buses ply from Hoshiarpur to Haryana&nbsp;from where one &nbsp;can go up to Sanctuary by local means.</p>', 50001, 10010, 'Hoshiarpur', '15 km. from Hoshiarpur on Hoshiarpur-Mehengrowal Road.', '382 hectares (956 Acres), 498 Acre area of Village Takhni and 458 Acre area of village Rehmapur.', 'Government', '<p>Main faunal species are Sambar, Hog deer, Barking deer, Jungle cat, Jackal, Common Indian Hare, Mongoose, Leopard (migratory from HP), Wild boar, Pangolin, Rat Snake, Cobra, Python, Monitor lizard, &nbsp;Garden lizard, etc. and 86 species of birds are also found in Sanctuary.</p>', '<p>The Vegetation is mixed deciduous type comprising tree species like shisham, khair, ber, subabul, acacia, dhak, neem, drek, siris, toot, &nbsp; mango arjan , pipal, bohr, sembal, phalahi, jaman, guava, etc.&nbsp; Most of the area has been covered with lantana. Before declaration of area as wildlife sanctuary, some villagers had grown fruit trees of kinoo and mango which are now inside the sanctuary area. These are good for monkeys and variety of birds.</p>', '31.1455902', '74.5202144', '9', '2022-02-09 06:54:00', '2022-05-10 09:03:44', NULL),
(11, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10011, 'Ropar', '15 Km. from Anandpur Sahib City.', '116 hectares', 'Government', 'Main wildlife species found in the sanctuary are Sambar, Barking deer, Hare, Jackal, Python, Cobra, Rat snake, Leopard (migratory) etc.', 'As per forest classification the sanctuary falls under the Dry Deciduous Forest type. The main tree species are Khair,Shisham, Subabul, Siris, Phalai, Ficus spp., Neem etc.', '31.2626086', '76.5151815', '', '2022-02-09 06:56:20', '2022-02-09 06:56:20', NULL),
(12, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10012, 'Gurdaspur', 'Kathlaur - Kushlian Protected Forests.', '346 acres of Kushlian Protected Forest and 1550 acres of Kathlaur Protected Forest.', 'Government', 'Hog deer, Barking deer, Sambar, Chital, Nilgai, Wild boar, Python, Pangolin and birds like Partridges, Parrots, Hawk Eagle and Vultures.', 'Sarkanda, Kana Kahi (Saccharum spontaneum, S. Officinalis, S. Munja etc.) and young plantations of Khair (Acacia catechu), Shisham (Dalbergia sissoo), Kikar (A. nilotica), Amla (Emblica officinalis). Bambusa bambos &amp; Dendrocalamus strictus, Amrud (Psidium gujava). Terminalia arjuna, Willow (Salix sp.), Safeda (Eucalyptus hybrid) etc. Innumerable shrubs, herbs and other weeds also give shelter to various fauna and birds.', '32.2487095', '75.4473068', '', '2022-02-09 06:59:39', '2022-02-09 06:59:39', NULL),
(13, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50001, 10022, 'Ropar', '15 Km. from Anandpur Sahib City.', '116 hectares', 'Government', 'Main wildlife species found in the sanctuary are Sambar, Barking deer,Hare, Jackal, Python, Cobra, Rat snake, Leopard (migratory) etc.', 'As per forest classification the sanctuary falls under the Dry Deciduous Forest type. The main tree species are Khair, Shisham, Subabul, Siris, Phalai, Ficus spp., Neem etc.', '31.2466639', '75.6577985', '', '2022-02-09 07:04:07', '2022-02-09 07:04:07', NULL),
(14, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50002, 10013, 'Fazilka', 'Fazilka', 'Fazilka', 'Fazilka', 'Main wildlife species found in the sanctuary are Black buck and Blue bull.', 'As per forest classification the sanctuary falls under the Arid to semi-arid type. The main tree species are A. tortilis, Albizia lebbeck, Azadirachata indica, Dalbergia sissoo, Melia azedarach, Prosopis cineraria and P. juliflora etc.', '30.158133', '73.9828975', '', '2022-02-09 07:09:24', '2022-02-09 07:09:24', NULL),
(15, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50002, 10014, 'Hoshiarpur', 'Hoshiarpur', 'Hoshiarpur', 'Hoshiarpur', '--', '--', '31.3877753', '76.0994154', '', '2022-02-09 07:12:14', '2022-02-09 07:12:14', NULL),
(16, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50002, 10015, 'Gurdaspur', 'Gurdaspur', 'Gurdaspur', 'Gurdaspur', 'Bar Headed Goose, Mallard, Pintail, Brahminy Duck, Gadwall, Shovellor, Spotbill, Black Ibis, white eyed Pochard, Common Teal, Blue winged Teal, Gull, Jack Snipe, Fantail Snipe.', 'There are miscellaneous trees such as Mango and Neem in an isolated manner. The wetland is endowed with Lotus, Saccarum offixinalis, S. spontaneum and other weeds.', '32.0853296', '75.3950452', '', '2022-02-09 07:24:37', '2022-02-09 07:24:37', NULL),
(17, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50002, 10016, 'Sahibjada Ajit Singh Nagar', 'Sahibjada Ajit Singh Nagar', 'Sahibjada Ajit Singh Nagar', 'Sahibjada Ajit Singh Nagar', '--', '--', '30.8695625', '76.7438738', '', '2022-02-09 07:26:57', '2022-02-09 07:26:57', NULL),
(18, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats. dfdsfs', 50003, 10017, 'Amritsar', '30 Km. from Amritsar Town.', '1223 acres', 'Government', 'Jackal, Wild hare, Wild boar, Jungle Cat, Mongoose, Partridge, Peafowl, Dove, Pigeon, Parrots etc.', 'Eucalyptus, Shisham, Kikar, Khair, Muscat, Subabool, Lasura, Arjun etc.', '31.5180352', '74.6623553', '', '2022-02-09 07:29:32', '2022-02-09 07:29:32', NULL),
(19, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50003, 10018, 'Ropar', '--', '521.12 Acre', 'Government', '--', '--', '31.0199993', '76.4912452', '', '2022-02-09 07:31:45', '2022-02-09 07:31:45', NULL),
(20, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50003, 10019, 'Gurdaspur', '--', '4559.71 Acre', 'Government', '--', '--', '32.4436566', '75.7318352', '', '2022-02-09 07:33:54', '2022-02-09 07:33:54', NULL),
(21, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50003, 10020, 'River Beas', '--', 'River Beas with all its water channels from 52 Head Talwara to Harike Barrage including all Government areas in river Beas.', 'Government', '--', '--', '31.383333', '75.1811443', '', '2022-02-09 07:36:05', '2022-02-09 07:36:05', NULL),
(22, '/protected_area/slide1.png', 'The importance of in situ conservation of biological diversity through the establishment and management of National Parks, Wildlife Sanctuaries, Community Reserve and Conservation Reserve is now globally recognized. Conservation today is not only important in such Protected Areas (PAs) but also significant in adjoining managed forests (RFs and PFs). In many areas, setting aside areas and granting them legal protection by establishing and declaring them as Protected Areas in perhaps the only way to conserve whatever little natural areas are remaining. The State of Punjab is a prime example of this situation, where intensive agriculture has severely reduced the expanse and integrity of the natural habitats.', 50003, 10021, 'Kapurthala', '--', '520.824 Acre', 'Government', '---', '--', '32.0853296', '75.3950452', '', '2022-02-09 07:38:23', '2022-02-09 07:38:23', NULL),
(23, '/protected_area/5.png', '<p><strong>hgggggggggggggg</strong></p>', 50001, 10001, 'SAS NAGAR', 'ludhiana', 'dfdsfds', 'fsdfs', '<p><strong>fssssssssss</strong></p>', '<p><strong>dgggggggggggf</strong></p>', '30.2889659', '76.402755', '10', '2022-05-05 13:03:57', '2022-05-05 13:03:57', '2022-05-10 18:34:33'),
(24, '/protected_area/neeraj.jpg', '<p>fdddddddddddddddd</p>', 50001, 10002, 'fdddddddd', 'ludhiana', 'dfdsfds', 'fsdfs', '<p>fdvvvvvvvvvvvvvvvvv</p>', '<p>fdvdvdvdvdvdvdvdvdvdvdvdvdvdvdvdv</p>', '30.2889659', '76.402755', '10', '2022-05-09 10:20:22', '2022-05-09 10:21:38', '2022-05-09 10:21:38'),
(25, '/protected_area/neeraj.jpg', '<p>fdddddddddddddddd</p>', 50001, 10002, 'fdddddddd', 'ludhiana', 'dfdsfds', 'fsdfs', '<p>fdvvvvvvvvvvvvvvvvv</p>', '<p>fdvdvdvdvdvdvdvdvdvdvdvdvdvdvdvdv</p>', '30.2889659', '76.402755', '10', '2022-05-09 10:21:28', '2022-05-09 10:21:46', '2022-05-09 10:21:46'),
(26, '/protected_area/RK Mishra.jpg', '<p><strong>addddddddddddd</strong></p>', 50002, 10013, 'SAS NAGAR', 'ludhiana', 'dfdsfds', 'fsdfs', '<p>dfffffffffffffffff</p>', '<p>&nbsp;fggggggggggggggg</p>', '30.288930', '76.402755', '10', '2022-05-09 10:22:26', '2022-05-09 10:24:09', '2022-05-09 10:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `frontend_protected_area_images`
--

CREATE TABLE `frontend_protected_area_images` (
  `id` int NOT NULL,
  `subcat_id` int NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_protected_area_images`
--

INSERT INTO `frontend_protected_area_images` (`id`, `subcat_id`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 10001, '/protected_area/1651564241.jpg', '2022-05-03 07:50:41', '2022-05-03 07:51:13', '2022-05-03 07:51:13'),
(2, 10004, '/protected_area/1651564248.jpg', '2022-05-03 07:50:48', '2022-05-03 07:53:12', '2022-05-03 07:53:12'),
(3, 10001, '/protected_area/PHOTO-2021-06-15-12-49-39_3.jpg', '2022-05-03 07:50:57', '2022-05-11 11:58:15', NULL),
(4, 10002, '/protected_area/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 11:55:20', '2022-05-11 11:55:20', NULL),
(5, 10001, '/protected_area/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 13:39:10', '2022-05-11 13:39:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_protected_wetland_detail`
--

CREATE TABLE `frontend_protected_wetland_detail` (
  `id` int NOT NULL,
  `type_id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `access` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `altitude` varchar(255) NOT NULL,
  `flora` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fauna` text NOT NULL,
  `map_lat` varchar(255) NOT NULL,
  `map_long` varchar(255) NOT NULL,
  `zoom_level` varchar(255) NOT NULL,
  `history_description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_protected_wetland_detail`
--

INSERT INTO `frontend_protected_wetland_detail` (`id`, `type_id`, `banner_image`, `description`, `location`, `access`, `latitude`, `longitude`, `altitude`, `flora`, `fauna`, `map_lat`, `map_long`, `zoom_level`, `history_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 150001, '/protected_wetland_images/slide1.png', '<p>Harike is the largest wetland in northern India in the Tarn Taran District of the Punjab State in India. The wetland and the lake were formed by constructing the head works across the Sutlej rivers, in 1952. The rich biodiversity of the wetland with its vast concentration of migratory waterfowls including a number of globally threatened species has been responsible for the recognition accorded to this wetland in 1990, by the Ramsar Convention, as one of the Ramsar sites in India, for conservation, development and preservation of the ecosystem.</p>', 'District Tarn Taran, Ferozepur & Kapurthala', 'District Tarn Taran, Ferozepur & Kapurthala', '1˚13’ N; 75˚2’ E', '31˚13’ N; 75˚2’ E', '218.83m above MSL', '24 terrestrial and 14 aquatic taxa including Najas, Hydrilla, Ipomoea, Azolla sp., Potamogeton, Vallisneria etc.', '16 Taxa (72 species) of Fishes, 6 Taxa of Frogs and toads, 7 species of Turtles (including IUCN Redlist Testudines Turtles), 13 species of Mammals (including Smooth Indian Otter and Indus River Dolphin), 391 species (59% Migratory) of Birds and 4 species of Snakes.', '31.1471053', '74.9689181', '10', '<p>The presence of many Gurudwaras in the vicinity of the Wetland gives testimony to the legend that on the advice of Sri Guru Nanak Dev Ji, the sacred Rabab was taken by Mardana from Bharoana. Gurudwara Rabasar Sahib, Gurudwara Ishar dham etc are some of the well known gurudwaras.</p>', '2022-02-05 12:08:31', '2022-05-10 08:47:07', NULL),
(2, 150002, '/protected_wetland_images/slide2.png', 'Nangal Wetland is a unique habitat on account of its pristine blue-green water which is a excellent habitat for migratory and resident birds and other aquatic flora and fauna. Recently a group of Sarus cranes were spotted in this area. The landscape of the area is a mixture of hilly terrain and plain land with a huge water body covered with good number of trees, grasses and shrubs. The place is very well connected and lies close of Gurudwara Bhabour Sahib.', 'District Ropar', 'Nearest town/city is Nangal.', '31˚ 23’', '76˚ 31’', '362.71m', '9 species of mammals such as Pale hedge hog, Jungle cat, Mongoose, Rufous tailed hare etc., 35 species of fishes, 154 species of migratory as well as resident birds.', 'The main tree species are Acacia nilotica, Cassia fistula, Magnifera indica, Syzygium cuminii, Acacia catechu, Ficus bengalensis, Ficus religiosa, Salix species etc.', '31.1329733', '74.5427512', '', 'The presence of many Gurudwaras in the vicinity of the Wetland gives testimony to the legend that on the advice of Sri Guru Nanak Dev Ji, the sacred Rabab was taken by Mardana from Bharoana. Gurudwara Rabasar Sahib, Gurudwara Ishar dham etc are some of the well known gurudwaras.', '2022-02-05 12:12:58', '2022-05-04 08:05:59', NULL),
(3, 150003, '/protected_wetland_images/slide1.png', '<p>The reserve comprises of 850 acre area of fresh water marshes (natural wetlands) comprising of two bits. Major bit is located adjoining to Miani, Dalla, Keshopur and Matwa villages and the other smaller bit lies adjoining to Magarmudian village. Entire area of this Wetland Community Reserve is owned by above five villages Panchayats. These wetlands are abode for about 40000 migratory birds that flock the area during winter migratory season starting from November till March every year.</p>', 'District Gurdaspur', 'Gurdaspur – Behrampur Road and Gurdaspur-Gahlari Road', '32˚05’ 16˚3’ N', '75˚24’ 24˚2’ E', '245m', 'The main tree species are Acacia nilotica, Cassia fistula, Magnifera indica, Syzygium cuminii, Acacia catechu, Ficus bengalensis, Ficus religiosa, etc. There are seasonal herbs, shrubs and grasses available in this area.', '16 species of mammals such as Pangolin, Striped palm squirrel, Porcupine, Mongoose, Barking deer, Sambar, Hog deer, Small Indian civet, 27 species of fishes, 245 species of migratory as well as resident birds including Sarus crane.', '32.0856942', '75.3933319', '', '<p>The presence of many Gurudwaras in the vicinity of the Wetland gives testimony to the legend that on the advice of Sri Guru Nanak Dev Ji, the sacred Rabab was taken by Mardana from Bharoana. Gurudwara Rabasar Sahib, Gurudwara Ishar dham etc are some of the well known gurudwaras.</p>', '2022-02-05 12:16:49', '2022-02-05 12:56:43', NULL),
(30, 150001, '/protected_wetland_images/4.png', '<p><strong>ddddddddddddddf</strong></p>', 'gdfgdf', 'bcvbc', '56456', '6456', 'hdfhd', 'hdfhd', 'hdfhd', '30.2889301', '76.4302553', '10', '<p><strong>gfffffffffffffb</strong></p>', '2022-05-09 07:23:48', '2022-05-09 07:41:08', '2022-05-09 07:41:08'),
(31, 150001, '/protected_wetland_images/4.png', '<p><em>gddddddddddddf</em></p>', 'fdfdddddddg', 'fgfgfhfdh', '34', '453', 'hdfhd', 'hdfhd', 'hdfhd', '30.1824595', '30.2889659', '10', '<p><strong>gdddddddddddddddddddddf</strong></p>', '2022-05-09 07:26:17', '2022-05-09 07:49:01', '2022-05-09 07:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `frontend_wetland_images`
--

CREATE TABLE `frontend_wetland_images` (
  `id` int NOT NULL,
  `wetland_id` int NOT NULL,
  `images` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_wetland_images`
--

INSERT INTO `frontend_wetland_images` (`id`, `wetland_id`, `images`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 150001, '/protected_wetland_images/1652079765.png', '2022-02-08 15:32:15', '2022-05-09 07:02:45', NULL),
(2, 150002, '/protected_wetland_images/1644401069.jpg', '2022-02-08 15:32:45', '2022-05-03 08:08:03', NULL),
(3, 150001, '/protected_wetland_images/1650873762.jpg', '2022-04-25 08:02:32', '2022-05-06 14:24:18', NULL),
(4, 150002, '/protected_wetland_images/1650881818.jpg', '2022-04-25 10:16:45', '2022-04-25 10:56:11', NULL),
(5, 150003, '/protected_wetland_images/1650884191.png', '2022-04-25 10:56:32', '2022-05-03 08:06:37', NULL),
(6, 150001, '/protected_wetland_images/1650887898.jpg', '2022-04-25 11:58:04', '2022-05-06 14:24:18', NULL),
(7, 150001, '/protected_wetland_images/1650893013.jpg', '2022-04-25 13:23:33', '2022-05-06 14:24:18', NULL),
(8, 150001, '/protected_wetland_images/1650893077.jpg', '2022-04-25 13:24:37', '2022-05-06 14:24:18', NULL),
(9, 150001, '/protected_wetland_images/1650893643.png', '2022-04-25 13:27:32', '2022-05-06 14:24:18', NULL),
(10, 150001, '/protected_wetland_images/1650893551.png', '2022-04-25 13:32:31', '2022-05-06 14:24:18', NULL),
(11, 150003, '/protected_wetland_images/1650893868.jpg', '2022-04-25 13:37:48', '2022-05-03 08:05:01', NULL),
(12, 150001, '/protected_wetland_images/1651839892.png', '2022-05-06 12:24:52', '2022-05-06 14:24:18', NULL),
(13, 150001, '/protected_wetland_images/1651839898.png', '2022-05-06 12:24:58', '2022-05-06 14:24:18', NULL),
(14, 150002, '/protected_wetland_images/1651840731.png', '2022-05-06 12:38:51', '2022-05-06 12:38:51', NULL),
(15, 150001, '/protected_wetland_images/1651840745.png', '2022-05-06 12:39:05', '2022-05-06 14:24:18', NULL),
(16, 150001, '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 12:06:21', '2022-05-11 12:06:21', NULL),
(17, 150002, '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-11 12:22:38', '2022-05-11 12:23:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_zoo_detail`
--

CREATE TABLE `frontend_zoo_detail` (
  `id` int NOT NULL,
  `type_id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `website_link` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `access` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `visiting_days` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `zoo_hours` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `zoo_holidays` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `special_attractions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `zoo_residence` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `zoom_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `frontend_zoo_detail`
--

INSERT INTO `frontend_zoo_detail` (`id`, `type_id`, `banner_image`, `website_link`, `description`, `location`, `access`, `visiting_days`, `zoo_hours`, `zoo_holidays`, `special_attractions`, `zoo_residence`, `latitude`, `longitude`, `zoom_level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '/frontend_zoo/slide1.png', 'http://chhatbirzoo.gov.in/', '<p>Ludhiana Zoo is located on National Highway No. 1 near Amaltas Tourist Complex, Ludhiana in the protected forest area. It is spread over an area of 25 acres.</p>', '5 km from Patiala on Patiala – Dakala Road.', '8 Km. from district Ludhiana', 'six days of week. Closed on Monday', '9.00 AM to 5.00PM', '15th August, 2nd October and 26th January', 'Lion Safari, Deer Safari, Zoo Lake', '<p>Royal Bengal Tiger, Sambar, Chital, Black Buck, Himalayan Black Bear, Pheasants and Birds.</p>', '30.9601092', '75.8201091', '17', '2022-02-04 15:05:02', '2022-05-10 10:41:15', NULL),
(2, 1, '/frontend_zoo/bodyworn-794111_1920.jpg', 'http://chhatbirzoo.gov.in/', '<p>dffffffffffffff</p>', '5 km from Patiala on Patiala – Dakala Road.', '8 Km. from district Ludhiana', 'six days of week. Closed on Monday', '9.00 AM to 5.00PM', '15th August, 2nd October and 26th January', 'Lion Safari, Deer Safari, Zoo Lake', '<p>dfssssssssss</p>', '30.9590687', '75.8204768', '', '2022-02-04 15:19:06', '2022-02-04 15:27:30', '2022-02-04 15:27:30'),
(3, 1, '/frontend_zoo/black_buck.jpg', 'http://chhatbirzoo.gov.in/', '<p>vfdddddddddddd</p>', '5 km from Patiala on Patiala – Dakala Road.', '8 Km. from district Ludhiana', 'six days of week. Closed on Monday', '9.00 AM to 5.00PM', '15th August, 2nd October and 26th January', 'Lion Safari, Deer Safari, Zoo Lake', '<p>ddddddddddddddfg</p>', '30.9590687', '75.8204768', '', '2022-02-04 15:21:00', '2022-02-04 15:27:08', '2022-02-04 15:27:08'),
(4, 1, '/frontend_zoo/slide1.png', 'http://chhatbirzoo.gov.in/', '<p>M.C. Zoological Park, Chhatbir was setup in an area of 202 hectare of Chhatbir Reserved Forest, once a hunting reserve of Maharaja of Patiala. This is one of the largest Zoos in northern India. The animals are kept in open enclosures wherein their biological and physical needs can be met.</p>', 'District Mohali', '20 km from Chandigarh and 50 km from Patiala on ChandigarhPatiala Highway.', 'six days of week. Closed on Monday', '9.00 AM to 5.00PM', '15th August, 2nd October and 26th January', 'Lion Safari, Deer Safari, Zoo Lake', '<p>Royal Bengal Tiger, Leopard, Hippo, Elephant, Zebra, Chimpanzee, Cape African buffalo, Crocodile, Gharial, various species of Deers, Antelopes, Macaques and birds.</p>', '30.6039176', '76.7902743', '4', '2022-02-08 04:41:53', '2022-05-10 10:41:47', NULL),
(5, 3, '/frontend_zoo/slide1.png', 'http://chhatbirzoo.gov.in/', 'Mini Zoo, Patiala was established in 1968 in compartment No. 10 of Bir Motibagh Wildlife Sanctuary with a pair of Black bucks. Now this zoo holds 357 animals belonging 24 species of animals and birds.', 'District Patiala', '5 Km. from district Patiala City on Patiala-Dakala Road', 'six days of week. Closed on Monday', '9.00 AM to 5.00 PM', '15th August, 2nd October and 26th January', 'Wildlife Interpretation Centre cum Library', 'Sambar, Spotted Deer, Barking Deer, Hog Deer, Black Buck, Black Buck, Crocodile, Porcupine, Emu, Pheasants and Birds.', '30.2959565', '76.3940899', '', '2022-02-08 04:54:28', '2022-02-08 04:54:28', NULL),
(6, 4, '/frontend_zoo/slide1.png', 'http://chhatbirzoo.gov.in/', 'Mini Zoo Bir Talab Bathinda was established in 1978 within Bir Talab Block Forest. Total Area of Bir Talab Forest is 161 Acre. Started in a modest way by Red Cross Society, Bathinda but later on it was adopted by Punjab Forest &amp; Wildlife Preservation Department in the year 1982. It houses 115 animals and 180 Birds to its collection which belong to 15 different species.', 'District Bathinda', '6 KM away from Bathinda town on Bathinda-Multania Road', 'six days of week. Closed on Monday', '9.00 AM to 5.00 PM', '15th August, 2nd October and 26th January', '15th August, 2nd October and 26th January', 'Royal Bengal Tiger, Leopard, Hippo, Elephant, Zebra, Chimpanzee, Cape African buffalo, Crocodile, Gharial, various species of Deers, Antelopes, Macaques and birds.', '30.1893768', '74.8768097', '', '2022-02-08 04:58:34', '2022-02-08 04:58:34', NULL),
(7, 5, '/frontend_zoo/slide1.png', 'http://chhatbirzoo.gov.in/', 'Deer Park Neelon was established in the year 1972-73 in compact forests of 2.5 hectare on the bank of old Sirhind canal near village Neelon of district Ludhiana. Presently it houses 166 animals and birds belonging to 7 species. It is situated 25 Km away from Ludhiana on Ludhiana-Chandigarh Highway or 5 Km from Samrala. The main species are Black buck, Sambar, Porcupine and few bird species.', '--', '--', '--', '--', '--', '--', '----', '30.8498308', '76.1053428', '', '2022-02-08 05:02:51', '2022-05-09 11:08:03', '2022-05-09 11:08:03'),
(8, 1, '/frontend_zoo/2.png', 'hhgggggg', 'hbhjbhjbjh', 'hhhggggggggggg', 'hgggggggggggggggf', 'hfgggggg', 'hggggggggggg', 'hggggggggggggg', 'gghhhhhhhhhhhh', 'fffffffffffffffffffffffff', '30.22222222', '70.22222222', '', '2022-02-15 05:45:35', '2022-02-15 05:46:10', '2022-02-15 05:46:10'),
(9, 1, '/frontend_zoo/RK Mishra.jpg', 'http://jsonviewer.stack.hu/', '<p>dsssssssssssss</p>', 'ludhiana', 'hdfhdf', '45', '54', '54', '5', '<p>dssssssssssss</p>', '56456', '6456', '10', '2022-05-09 10:48:12', '2022-05-09 10:48:44', '2022-05-09 10:48:44'),
(10, 1, '/frontend_zoo/neeraj.jpg', 'http://jsonviewer.stack.hu/', '<p>;lllllllllllllll</p>', 'ludhiana', 'hdfhdf', '45', '54', '54', '5', '<p>;kkkkkkkkkkkkkkkkkkkkk</p>', '56456', '453', '10', '2022-05-10 10:40:53', '2022-05-10 10:48:26', '2022-05-10 10:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_all_images`
--

CREATE TABLE `gallery_all_images` (
  `id` int NOT NULL,
  `table_id` int NOT NULL,
  `type_id` int NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `gallery_all_images`
--

INSERT INTO `gallery_all_images` (`id`, `table_id`, `type_id`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 1, '/zoo_gallery/1650886813.jpg', '2022-04-25 14:01:39', '2022-05-03 08:21:11', '2022-05-03 08:21:11'),
(2, 3, 3, '/zoo_gallery/img3.jpg', '2022-04-25 14:01:39', '2022-04-25 14:01:39', NULL),
(3, 4, 4, '/zoo_gallery/img5.jpg', '2022-04-25 14:05:18', '2022-04-25 14:05:18', NULL),
(4, 7, 7, '/zoo_gallery/img13.jpg', '2022-04-25 14:05:18', '2022-04-25 14:05:18', NULL),
(5, 8, 8, '/zoo_gallery/img14.jpg', '2022-04-25 14:06:36', '2022-04-25 14:06:36', NULL),
(6, 9, 9, '/zoo_gallery/img11.jpg', '2022-04-25 14:06:36', '2022-04-25 14:06:36', NULL),
(7, 10, 1, '/zoo_gallery/img13.jpg', '2022-04-25 14:07:06', '2022-05-03 08:21:50', '2022-05-03 08:21:50'),
(8, 12, 2, '/zoo_gallery/1623911339.jpg', '2022-04-25 14:07:06', '2022-04-25 14:07:06', NULL),
(9, 13, 2, '/zoo_gallery/1623911356.jpg', '2022-04-25 14:07:50', '2022-04-25 14:07:50', NULL),
(10, 14, 2, '/zoo_gallery/1623911363.jpg', '2022-04-25 14:07:50', '2022-04-25 14:07:50', NULL),
(11, 15, 2, '/zoo_gallery/1623911371.jpg', '2022-04-25 14:08:41', '2022-04-25 14:08:41', NULL),
(12, 16, 2, '/zoo_gallery/1623911379.jpg', '2022-04-25 14:08:41', '2022-04-25 14:08:41', NULL),
(13, 17, 2, '/zoo_gallery/1623911385.jpg', '2022-04-25 14:09:08', '2022-04-25 14:09:08', NULL),
(14, 18, 2, '/zoo_gallery/1623911393.jpg', '2022-04-25 14:09:08', '2022-04-25 14:09:08', NULL),
(15, 19, 2, '/zoo_gallery/1623911400.jpg', '2022-04-25 14:09:40', '2022-04-25 14:09:40', NULL),
(16, 20, 2, '/zoo_gallery/1623911406.jpg', '2022-04-25 14:09:40', '2022-04-25 14:09:40', NULL),
(17, 21, 2, '/zoo_gallery/1623911413.jpg', '2022-04-25 14:10:19', '2022-04-25 14:10:19', NULL),
(18, 22, 2, '/zoo_gallery/1623911419.jpg', '2022-04-25 14:10:19', '2022-04-25 14:10:19', NULL),
(19, 23, 2, '/zoo_gallery/1623911426.jpg', '2022-04-25 14:10:53', '2022-04-25 14:10:53', NULL),
(20, 24, 2, '/zoo_gallery/1623911432.jpg', '2022-04-25 14:10:53', '2022-04-25 14:10:53', NULL),
(21, 25, 5, '/zoo_gallery/1623911536.jpg', '2022-04-25 14:11:23', '2022-05-09 11:08:03', NULL),
(22, 26, 5, '/zoo_gallery/1623911544.jpg', '2022-04-25 14:11:23', '2022-05-09 11:08:03', NULL),
(23, 27, 5, '/zoo_gallery/1623911551.jpg', '2022-04-25 14:11:56', '2022-05-09 11:08:03', NULL),
(24, 28, 5, '/zoo_gallery/1623911560.jpg', '2022-04-25 14:11:56', '2022-04-25 14:11:56', '2022-04-25 14:11:56'),
(25, 29, 5, '/zoo_gallery/1623911567.jpg', '2022-04-25 14:12:30', '2022-05-09 11:08:03', NULL),
(26, 30, 5, '/zoo_gallery/1623911573.jpg', '2022-04-25 14:12:30', '2022-04-25 14:12:30', '2022-04-25 14:12:30'),
(27, 31, 1, '/zoo_gallery/01.jpg', '2022-04-25 14:13:17', '2022-05-03 08:21:56', '2022-05-03 08:21:56'),
(28, 32, 6, '/zoo_gallery/1641970796.JPG', '2022-04-25 14:13:17', '2022-04-25 14:13:17', NULL),
(29, 33, 6, '/zoo_gallery/1641970905.jpeg', '2022-04-25 14:13:47', '2022-04-25 14:13:47', NULL),
(30, 34, 1, '/zoo_gallery/1643624425.jpg', '2022-04-25 14:13:47', '2022-05-03 08:22:00', '2022-05-03 08:22:00'),
(31, 35, 8, '/zoo_gallery/1643791901.jpeg', '2022-04-25 14:14:33', '2022-04-25 14:14:33', NULL),
(32, 36, 8, '/zoo_gallery/photo-1575550959106-5a7defe28b56.JPG', '2022-04-25 14:14:33', '2022-04-25 14:14:33', NULL),
(33, 38, 8, '/zoo_gallery/1643804312.JPG', '2022-04-25 14:15:10', '2022-04-25 14:15:10', NULL),
(34, 39, 1, '/zoo_gallery/1643880804.jpeg', '2022-04-25 14:15:10', '2022-05-03 08:22:22', '2022-05-03 08:22:22'),
(35, 40, 1, '/zoo_gallery/1644052568.jpg', '2022-04-25 14:15:58', '2022-04-25 14:15:58', '2022-05-03 13:55:40'),
(36, 41, 11, '/zoo_gallery/1644204951.jpg', '2022-04-25 14:15:58', '2022-04-25 14:15:58', NULL),
(37, 42, 11, '/zoo_gallery/1644204967.jpg', '2022-04-25 14:16:30', '2022-04-25 14:16:30', NULL),
(38, 43, 11, '/zoo_gallery/1644204984.jpg', '2022-04-25 14:16:30', '2022-04-25 14:16:30', NULL),
(39, 44, 11, '/zoo_gallery/1644205009.jpg', '2022-04-25 14:17:13', '2022-04-25 14:17:13', NULL),
(40, 45, 11, '/zoo_gallery/1644205025.jpg', '2022-04-25 14:17:13', '2022-04-25 14:17:13', NULL),
(41, 46, 11, '/zoo_gallery/1644205049.jpg', '2022-04-25 14:18:07', '2022-04-25 14:18:07', NULL),
(42, 47, 11, '/zoo_gallery/1644205062.jpg', '2022-04-25 14:18:07', '2022-04-25 14:18:07', NULL),
(43, 48, 11, '/zoo_gallery/1644205121.jpg', '2022-04-25 14:18:54', '2022-04-25 14:18:54', NULL),
(44, 49, 11, '/zoo_gallery/1644205182.jpg', '2022-04-25 14:18:54', '2022-04-25 14:18:54', NULL),
(45, 50, 7, '/zoo_gallery/1644205275.jpg', '2022-04-25 14:22:30', '2022-04-25 14:22:30', NULL),
(46, 51, 7, '/zoo_gallery/1644205283.jpg', '2022-04-25 14:22:30', '2022-04-25 14:22:30', NULL),
(47, 52, 7, '/zoo_gallery/1644205292.jpg', '2022-04-25 14:23:22', '2022-04-25 14:23:22', NULL),
(48, 53, 7, '/zoo_gallery/1644205302.jpg', '2022-04-25 14:23:22', '2022-04-25 14:23:22', NULL),
(49, 54, 7, '/zoo_gallery/1644205310.jpg', '2022-04-25 14:24:05', '2022-04-25 14:24:05', NULL),
(50, 55, 7, '/zoo_gallery/1644205320.jpg', '2022-04-25 14:24:05', '2022-04-25 14:24:05', NULL),
(51, 56, 7, '/zoo_gallery/1644205332.jpg', '2022-04-25 14:33:48', '2022-04-25 14:33:48', NULL),
(52, 57, 7, '/zoo_gallery/1644205349.jpg', '2022-04-25 14:33:48', '2022-04-25 14:33:48', NULL),
(53, 58, 7, '/zoo_gallery/1644205358.jpg', '2022-04-25 14:34:30', '2022-04-25 14:34:30', NULL),
(54, 59, 7, '/zoo_gallery/1644205366.jpg', '2022-04-25 14:34:30', '2022-04-25 14:34:30', NULL),
(55, 60, 7, '/zoo_gallery/1644205375.jpg', '2022-04-25 14:35:03', '2022-04-25 14:35:03', NULL),
(56, 61, 7, '/zoo_gallery/1644205383.jpg', '2022-04-25 14:35:03', '2022-04-25 14:35:03', NULL),
(57, 62, 7, '/zoo_gallery/1644209942.jpg', '2022-04-25 14:35:40', '2022-04-25 14:35:40', NULL),
(58, 63, 7, '/zoo_gallery/1644209951.jpg', '2022-04-25 14:35:40', '2022-04-25 14:35:40', NULL),
(59, 64, 7, '/zoo_gallery/1644209989.jpg', '2022-04-25 14:36:18', '2022-04-25 14:36:18', NULL),
(60, 65, 7, '/zoo_gallery/1644210006.jpg', '2022-04-25 14:36:18', '2022-04-25 14:36:18', NULL),
(61, 66, 7, '/zoo_gallery/1644210006.jpg', '2022-04-25 14:36:55', '2022-04-25 14:36:55', NULL),
(62, 67, 7, '/zoo_gallery/1644210020.jpg', '2022-04-25 14:36:55', '2022-04-25 14:36:55', NULL),
(63, 68, 7, '/zoo_gallery/1644210029.jpg', '2022-04-25 14:37:33', '2022-04-25 14:37:33', NULL),
(64, 69, 7, '/zoo_gallery/1644210041.jpg', '2022-04-25 14:37:33', '2022-04-25 14:37:33', NULL),
(65, 70, 7, '/zoo_gallery/1644210051.jpg', '2022-04-25 14:38:59', '2022-04-25 14:38:59', NULL),
(66, 71, 7, '/zoo_gallery/1644210069.jpg', '2022-04-25 14:38:59', '2022-04-25 14:38:59', NULL),
(67, 72, 7, '/zoo_gallery/1644210083.jpg', '2022-04-25 14:39:31', '2022-04-25 14:39:31', NULL),
(68, 73, 7, '/zoo_gallery/1644210093.jpg', '2022-04-25 14:39:31', '2022-04-25 14:39:31', NULL),
(69, 74, 7, '/zoo_gallery/1644210182.jpg', '2022-04-25 14:40:29', '2022-04-25 14:40:29', NULL),
(70, 75, 7, '/zoo_gallery/1644210199.jpg', '2022-04-25 14:40:29', '2022-04-25 14:40:29', NULL),
(71, 76, 7, '/zoo_gallery/1644210226.jpg', '2022-04-25 14:41:03', '2022-04-25 14:41:03', NULL),
(72, 77, 7, '/zoo_gallery/1644210240.jpg', '2022-04-25 14:41:03', '2022-04-25 14:41:03', NULL),
(73, 78, 7, '/zoo_gallery/1644210263.jpg', '2022-04-25 14:41:44', '2022-04-25 14:41:44', NULL),
(74, 79, 7, '/zoo_gallery/1644210281.jpg', '2022-04-25 14:41:44', '2022-04-25 14:41:44', NULL),
(75, 80, 7, '/zoo_gallery/1644210301.jpg', '2022-04-25 14:42:42', '2022-04-25 14:42:42', NULL),
(76, 81, 7, '/zoo_gallery/1644210320.jpg', '2022-04-25 14:42:42', '2022-04-25 14:42:42', NULL),
(77, 82, 7, '/zoo_gallery/1644210337.jpg', '2022-04-25 14:43:20', '2022-04-25 14:43:20', NULL),
(78, 83, 7, '/zoo_gallery/1644210352.jpg', '2022-04-25 14:43:20', '2022-04-25 14:43:20', NULL),
(79, 84, 7, '/zoo_gallery/1644210374.jpg', '2022-04-25 14:43:53', '2022-04-25 14:43:53', NULL),
(80, 85, 7, '/zoo_gallery/1644210392.jpg', '2022-04-25 14:43:53', '2022-04-25 14:43:53', NULL),
(81, 86, 7, '/zoo_gallery/1644210491.jpg', '2022-04-25 14:45:01', '2022-04-25 14:45:01', NULL),
(82, 87, 7, '/zoo_gallery/1644215045.jpg', '2022-04-25 14:45:01', '2022-04-25 14:45:01', NULL),
(83, 88, 7, '/zoo_gallery/1644215058.jpg', '2022-04-25 14:45:53', '2022-04-25 14:45:53', NULL),
(84, 89, 7, '/zoo_gallery/1644215077.jpg', '2022-04-25 14:45:53', '2022-04-25 14:45:53', NULL),
(85, 90, 7, '/zoo_gallery/1644215088.jpg', '2022-04-25 14:47:06', '2022-04-25 14:47:06', NULL),
(86, 91, 7, '/zoo_gallery/1644215101.jpg', '2022-04-25 14:47:06', '2022-04-25 14:47:06', NULL),
(87, 92, 7, '/zoo_gallery/1644215113.jpg', '2022-04-25 14:47:40', '2022-04-25 14:47:40', NULL),
(88, 93, 7, '/zoo_gallery/1644215124.jpg', '2022-04-25 14:47:40', '2022-04-25 14:47:40', NULL),
(89, 94, 8, '/zoo_gallery/1644215318.jpeg', '2022-04-25 14:48:12', '2022-04-25 14:48:12', NULL),
(90, 95, 8, '/zoo_gallery/1644215348.jpeg', '2022-04-25 14:48:12', '2022-04-25 14:48:12', NULL),
(91, 96, 8, '/zoo_gallery/1644215661.jpeg', '2022-04-25 14:48:56', '2022-04-25 14:48:56', NULL),
(92, 97, 8, '/zoo_gallery/1644215681.jpg', '2022-04-25 14:48:56', '2022-04-25 14:48:56', NULL),
(93, 98, 8, '/zoo_gallery/1644215692.jpg', '2022-04-25 14:49:33', '2022-04-25 14:49:33', NULL),
(94, 99, 8, '/zoo_gallery/1644215701.jpg', '2022-04-25 14:49:33', '2022-04-25 14:49:33', NULL),
(97, 100, 8, '/zoo_gallery/1644215714.jpeg', '2022-04-25 14:50:15', '2022-04-25 14:50:15', NULL),
(98, 101, 1, '/zoo_gallery/1644403643.jpg', '2022-04-25 14:50:15', '2022-05-03 08:22:25', '2022-05-03 08:22:25'),
(99, 102, 10, '/zoo_gallery/1644413891.jpg', '2022-04-25 14:51:13', '2022-04-25 14:51:13', NULL),
(100, 103, 10, '/zoo_gallery/1644465021.jpg', '2022-04-25 14:51:13', '2022-04-25 14:51:13', NULL),
(101, 104, 10, '/zoo_gallery/1644465035.jpg', '2022-04-25 14:51:50', '2022-04-25 14:51:50', NULL),
(102, 105, 10, '/zoo_gallery/1644465049.jpg', '2022-04-25 14:51:50', '2022-04-25 14:51:50', NULL),
(103, 106, 10, '/zoo_gallery/1644465119.jpg', '2022-04-25 14:52:28', '2022-04-25 14:52:28', NULL),
(104, 107, 10, '/zoo_gallery/1644465129.jpg', '2022-04-25 14:52:28', '2022-04-25 14:52:28', NULL),
(105, 108, 10, '/zoo_gallery/1644465139.jpg', '2022-04-25 14:53:03', '2022-04-25 14:53:03', NULL),
(106, 109, 10, '/zoo_gallery/1644465158.jpg', '2022-04-25 14:53:03', '2022-04-25 14:53:03', NULL),
(107, 110, 10, '/zoo_gallery/1644465169.jpg', '2022-04-25 14:53:34', '2022-04-25 14:53:34', NULL),
(108, 111, 10, '/zoo_gallery/1644465182.jpg', '2022-04-25 14:53:34', '2022-04-25 14:53:34', NULL),
(109, 112, 10, '/zoo_gallery/1644465221.jpg', '2022-04-25 14:54:08', '2022-04-25 14:54:08', NULL),
(110, 113, 10, '/zoo_gallery/1644465302.jpg', '2022-04-25 14:54:08', '2022-04-25 14:54:08', NULL),
(111, 114, 10, '/zoo_gallery/1644465919.jpg', '2022-04-25 14:54:40', '2022-04-25 14:54:40', NULL),
(112, 115, 10, '/zoo_gallery/1644466040.jpg', '2022-04-25 14:54:40', '2022-04-25 14:54:40', NULL),
(113, 116, 10, '/zoo_gallery/1644466055.jpg', '2022-04-25 14:55:12', '2022-04-25 14:55:12', NULL),
(114, 117, 2, '/zoo_gallery/1647247838.jpg', '2022-04-25 14:55:12', '2022-04-25 14:55:12', '2022-05-03 18:24:27'),
(115, 118, 1, '/zoo_gallery/1649229849.jpg', '2022-04-25 14:55:56', '2022-04-25 14:55:56', '2022-05-03 13:56:04'),
(116, 119, 1, '/zoo_gallery/1649230013.jpg', '2022-04-25 14:55:56', '2022-04-25 14:55:56', '2022-05-03 13:56:08'),
(117, 120, 1, '/zoo_gallery/1649507955.jpg', '2022-04-25 14:56:31', '2022-04-25 14:56:31', '2022-05-03 13:56:11'),
(118, 121, 1, '/zoo_gallery/1649508019.jpg', '2022-04-25 14:56:31', '2022-04-25 14:56:31', '2022-05-03 13:56:16'),
(119, 122, 1, '/zoo_gallery/1649508069.jpg', '2022-04-25 14:56:59', '2022-04-25 14:56:59', '2022-05-03 13:56:19'),
(120, 123, 1, '/zoo_gallery/1649508119.jpg', '2022-04-25 14:56:59', '2022-04-25 14:56:59', '2022-05-03 13:56:22'),
(121, 124, 1, '/zoo_gallery/1649508161.jpg', '2022-04-25 14:57:32', '2022-04-25 14:57:32', '2022-05-03 13:56:25'),
(122, 125, 1, '/zoo_gallery/1649508237.jpg', '2022-04-25 14:57:32', '2022-04-25 14:57:32', '2022-05-03 13:56:30'),
(123, 126, 1, '/zoo_gallery/1649508316.jpg', '2022-04-25 14:58:00', '2022-04-25 14:58:00', '2022-05-03 13:56:33'),
(124, 127, 1, '/zoo_gallery/1649508359.jpg', '2022-04-25 14:58:00', '2022-04-25 14:58:00', '2022-05-03 13:56:37'),
(125, 128, 1, '/zoo_gallery/1649508383.jpg', '2022-04-25 14:58:30', '2022-04-25 14:58:30', '2022-05-03 13:56:40'),
(126, 129, 1, '/zoo_gallery/1649508431.jpg', '2022-04-25 14:58:30', '2022-04-25 14:58:30', '2022-05-03 13:56:44'),
(127, 130, 13, '/zoo_gallery/1650437008.jpg', '2022-04-25 14:59:04', '2022-04-25 14:59:04', '2022-05-03 18:24:49'),
(128, 131, 13, '/zoo_gallery/1650437181.jpg', '2022-04-25 14:59:04', '2022-04-25 14:59:04', '2022-05-03 18:25:08'),
(129, 132, 13, '/zoo_gallery/1650437240.jpg', '2022-04-25 14:59:46', '2022-04-25 14:59:46', '2022-05-03 18:25:29'),
(130, 133, 13, '/zoo_gallery/1650437377.JPG', '2022-04-25 14:59:46', '2022-04-25 14:59:46', '2022-05-03 18:25:48'),
(131, 134, 13, '/zoo_gallery/1650602822.jpg', '2022-04-25 15:00:22', '2022-04-25 15:00:22', '2022-05-03 18:26:08'),
(132, 1, 150001, '/protected_wetland_images/1652079765.png', '2022-04-25 15:11:32', '2022-05-09 07:02:45', NULL),
(133, 2, 150002, '/protected_wetland_images/1644401069.jpg', '2022-04-25 15:11:32', '2022-05-03 08:08:03', '2022-05-03 08:08:03'),
(134, 3, 150008, '/protected_wetland_images/1650455003.jpg', '2022-04-25 15:12:25', '2022-04-25 15:12:25', '2022-05-03 18:26:31'),
(138, 8, 150001, '/protected_wetland_images/1650893077.jpg', '2022-04-25 13:24:37', '2022-05-06 14:24:18', '2022-05-06 14:24:18'),
(139, 9, 150001, '/protected_wetland_images/1650893643.png', '2022-04-25 13:27:32', '2022-05-06 14:24:18', '2022-05-06 14:24:18'),
(140, 10, 150001, '/protected_wetland_images/1650893551.png', '2022-04-25 13:32:31', '2022-05-06 14:24:18', '2022-05-06 14:24:18'),
(141, 11, 150003, '/protected_wetland_images/1650893868.jpg', '2022-04-25 13:37:48', '2022-05-03 08:05:01', '2022-05-03 08:05:01'),
(142, 147, 1, '/zoo_gallery/1651498184.jpg', '2022-05-02 13:29:44', '2022-05-03 08:23:22', '2022-05-03 08:23:22'),
(150, 1, 80001, '/event_master/1651560032.jpg', '2022-05-03 06:40:32', '2022-05-03 07:43:14', '2022-05-03 07:43:14'),
(151, 2, 80001, '/event_master/1651560079.jpg', '2022-05-03 06:41:19', '2022-05-03 07:43:48', '2022-05-03 07:43:48'),
(152, 3, 80002, '/event_master/1651561220.jpg', '2022-05-03 07:00:21', '2022-05-03 07:42:15', '2022-05-03 07:42:15'),
(153, 3, 80002, '/event_master/1651561247.jpg', '2022-05-03 07:00:47', '2022-05-03 07:42:15', '2022-05-03 07:42:15'),
(157, 1, 10001, '/protected_area/1651564241.jpg', '2022-05-03 07:50:41', '2022-05-03 07:51:13', '2022-05-03 07:51:13'),
(158, 2, 10004, '/protected_area/1651564248.jpg', '2022-05-03 07:50:48', '2022-05-03 07:53:12', '2022-05-03 07:53:12'),
(159, 3, 10001, '/protected_area/PHOTO-2021-06-15-12-49-39_3.jpg', '2022-05-03 07:50:57', '2022-05-11 11:58:15', NULL),
(160, 150, 1, '/zoo_gallery/1651566302.jpg', '2022-05-03 08:24:46', '2022-05-03 08:30:24', '2022-05-03 08:30:24'),
(161, 151, 1, '/zoo_gallery/1651566804.jpg', '2022-05-03 08:32:44', '2022-05-03 08:33:45', '2022-05-03 08:33:45'),
(162, 4, 80001, '/event_master/1651586377.jpg', '2022-05-03 13:59:37', '2022-05-03 13:59:37', NULL),
(163, 12, 150001, '/protected_wetland_images/1651839892.png', '2022-05-06 12:24:52', '2022-05-06 14:24:18', '2022-05-06 14:24:18'),
(164, 13, 150001, '/protected_wetland_images/1651839898.png', '2022-05-06 12:24:58', '2022-05-06 14:24:18', '2022-05-06 14:24:18'),
(165, 14, 150002, '/protected_wetland_images/1651840731.png', '2022-05-06 12:38:51', '2022-05-06 12:38:51', NULL),
(166, 15, 150001, '/protected_wetland_images/1651840745.png', '2022-05-06 12:39:05', '2022-05-06 14:24:18', '2022-05-06 14:24:18'),
(167, 4, 10002, '/protected_area/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 11:55:20', '2022-05-11 11:55:20', NULL),
(168, 16, 150001, '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 12:06:21', '2022-05-11 12:06:21', NULL),
(169, 17, 150002, '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-11 12:22:38', '2022-05-11 12:23:01', NULL),
(170, 152, 1, '/zoo_gallery/PHOTO-2021-06-15-12-49-39_3.jpg', '2022-05-11 12:25:12', '2022-05-11 12:25:45', NULL),
(171, 6, 80002, '/event_master/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 12:31:38', '2022-05-11 12:31:43', '2022-05-11 12:31:43'),
(172, 5, 80002, '/event_master/PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-11 12:31:52', '2022-05-11 12:31:52', NULL),
(173, 5, 10001, '/protected_area/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-11 13:39:10', '2022-05-11 13:39:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE `gallery_images` (
  `id` int NOT NULL,
  `type_id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `type_id`, `type_name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 'Ludhiana Zoo', '/zoo_gallery/1650883801.jpg', '2022-04-25 10:50:01', '2022-04-25 10:50:01', NULL),
(3, 3, 'Mini Zoo Patiala', '/zoo_gallery/1650883817.jpg', '2022-04-25 10:50:17', '2022-04-25 10:50:17', NULL),
(4, 4, 'Mini Zoo Bathinda', '/zoo_gallery/1650883831.jpg', '2022-04-25 10:50:31', '2022-04-25 10:50:31', NULL),
(5, 5, 'Deer Park Neelon', '/zoo_gallery/1650883843.jpg', '2022-04-25 10:50:43', '2022-05-09 11:08:03', NULL),
(6, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1650883852.jpg', '2022-04-25 10:50:52', '2022-04-25 10:50:52', NULL),
(8, 10002, 'Bir Gurdialpura Wildlife Sanctuary', '/protected_area/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-04-25 10:54:41', '2022-05-11 11:55:20', NULL),
(9, 150003, 'Keshopur Community Reserve, Gurdaspur', '/protected_wetland_images/1650893868.jpg', '2022-04-25 10:56:32', '2022-05-03 08:06:37', '2022-05-03 08:06:37'),
(10, 150001, 'Harike Wildlife Sanctuary', '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-04-25 11:58:04', '2022-05-11 12:06:21', NULL),
(12, 80001, 'Wildlife Santuary', '/event_master/1651560079.jpg', '2022-05-03 06:40:32', '2022-05-03 07:43:48', '2022-05-03 07:43:48'),
(13, 80002, 'erwerw', '/event_master/1651560079.jpg', '2022-05-03 07:00:21', '2022-05-03 07:42:15', '2022-05-03 07:42:15'),
(15, 10001, 'Bir Moti Bagh Wildlife Sanctuary', '/protected_area/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-03 07:50:41', '2022-05-11 13:39:10', NULL),
(16, 10004, 'Bir Mehas Wildlife Sanctuary', '/protected_area/1651564248.jpg', '2022-05-03 07:50:48', '2022-05-03 07:53:12', '2022-05-03 07:53:12'),
(17, 1, 'Chhatbir Zoo', '/zoo_gallery/1651566804.jpg', '2022-05-03 08:24:46', '2022-05-03 08:33:45', '2022-05-03 08:33:45'),
(18, 80001, 'Wildlife Santuary', '/event_master/1651586377.jpg', '2022-05-03 13:59:37', '2022-05-03 13:59:37', NULL),
(19, 150001, 'Harike Wildlife Sanctuary', '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-06 12:24:52', '2022-05-11 12:06:21', NULL),
(20, 150002, 'Nangal Wildlife Sanctuary', '/protected_wetland_images/PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-06 12:38:51', '2022-05-11 12:23:01', NULL),
(21, 1, 'Chhatbir Zoo', '/zoo_gallery/PHOTO-2021-06-15-12-49-39_3.jpg', '2022-05-11 12:25:12', '2022-05-11 12:25:45', NULL),
(22, 80002, 'erwerw', '/event_master/PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-11 12:31:38', '2022-05-11 12:31:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_geographic_zone`
--

CREATE TABLE `home_geographic_zone` (
  `id` int NOT NULL,
  `home_id` int NOT NULL,
  `geography_zone` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `home_geographic_zone`
--

INSERT INTO `home_geographic_zone` (`id`, `home_id`, `geography_zone`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Submountaneous Shiwaliks', '2021-04-26 09:11:11', '2021-04-26 09:11:11', '0'),
(2, 1, 'Central and Eastern Alluvial Plains', '2021-04-26 09:11:11', '2021-04-26 09:11:11', '0'),
(3, 1, 'Western Arid Plains', '2021-04-26 14:51:28', '2021-04-26 14:51:28', '0'),
(6, 5, 'loreum', '2021-08-03 14:16:27', '2021-08-03 14:16:27', '0'),
(7, 5, 'loreum', '2021-08-03 14:16:27', '2021-08-03 14:16:27', '0'),
(8, 5, 'loreum', '2021-08-03 14:16:27', '2021-08-03 14:16:27', '0'),
(9, 5, 'loreum jnghj', '2021-08-03 14:22:46', '2021-08-03 14:22:46', '0'),
(10, 5, 'loreum gjhfg', '2021-08-03 14:22:46', '2021-08-03 14:22:46', '0'),
(11, 5, 'loreum ghfgh', '2021-08-03 14:22:46', '2021-08-03 14:22:46', '0'),
(12, 6, 'loreum', '2021-08-04 04:58:05', '2021-08-04 04:58:05', '0'),
(13, 6, 'loreum', '2021-08-04 04:58:05', '2021-08-04 04:58:05', '0'),
(14, 6, 'loreum', '2021-08-04 04:58:05', '2021-08-04 04:58:05', '0'),
(15, 6, 'loreum', '2021-08-04 04:58:42', '2021-08-04 04:58:42', '0'),
(16, 6, 'loreum', '2021-08-04 04:58:42', '2021-08-04 04:58:42', '0'),
(17, 6, 'loreum', '2021-08-04 04:58:42', '2021-08-04 04:58:42', '0'),
(18, 7, 'test', '2022-01-12 07:17:47', '2022-01-12 07:17:47', '0'),
(19, 7, 'testing', '2022-01-12 07:18:24', '2022-01-12 07:18:24', '0'),
(20, 1, 'Western Arid Plains', '2022-01-31 06:58:24', '2022-01-31 11:11:04', '1'),
(21, 1, 'Submountaneous Shiwaliks', '2022-01-31 06:58:24', '2022-01-31 11:10:15', '1'),
(22, 1, 'Central and Eastern Alluvial Plains', '2022-01-31 06:58:24', '2022-01-31 11:11:02', '1'),
(23, 1, 'Central and Eastern Alluvial Plains', '2022-01-31 06:59:37', '2022-01-31 11:10:55', '1'),
(24, 1, 'Submountaneous Shiwaliks', '2022-01-31 06:59:37', '2022-01-31 11:10:52', '1'),
(25, 1, 'Western Arid Plains', '2022-01-31 06:59:37', '2022-01-31 11:10:26', '1'),
(26, 1, 'Western Arid', '2022-01-31 11:16:44', '2022-01-31 11:17:01', '1'),
(27, 8, 'test', '2022-02-02 09:08:29', '2022-02-02 09:08:29', '0');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_detail`
--

CREATE TABLE `home_page_detail` (
  `id` int NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `bg_image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `intro_description` longtext NOT NULL,
  `wild_life_title` varchar(255) NOT NULL,
  `wild_description` longtext NOT NULL,
  `geographic_statement` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `home_page_detail`
--

INSERT INTO `home_page_detail` (`id`, `banner_heading`, `bg_image`, `title`, `intro_description`, `wild_life_title`, `wild_description`, `geographic_statement`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'Home', '/homebg_image/slide1.png', 'Introduction', 'Wildlife has been revered as a tradition in the history of the country. The concept of Wildlife Conservation dates back to the reign of emperor Ashoka whose rock edicts and pillars are strewn all over the country, urging us to have love and friendship for all creatures.India is unique in terms of biological diversity due to its unique biogeographical location, diversified climatic condition and enormous ecodiversity and geodiversity. India ranks 8th in the list contributing 10% to the world’s Biological Diversity.', 'Wild… Wild…. Punjab', 'Punjab, the ‘land of five rivers’ is a relatively small state of India occupying an area of 50,362 Sq.Km. It is located in the north western part of India between 29º30’ and 32º32’ North latitude 73º55’ and 76º50’ East longitude. Situated between river Ravi in the North-West and Ghaggar in the east, Punjab shares its boundaries with Jammu & Kashmir in the North, Himachal Pradesh in the North-East and Haryana and Rajasthan to its South. The state also shares a 300 km long international border with Pakistan on the western side.', 'Broadly the State can be classified in to three geographic zones.', '2021-04-26 09:11:11', '2022-01-31 11:17:08', '0'),
(2, 'Test update', '/homebg_image/chhatbir.jpg', 'Eagel', 'Loreum ipsum', 'rtdfgdfxcg', 'Loreum ipsum', 'dfsgdfbcbvx', '2021-04-26 10:18:58', '2021-04-26 10:18:58', '1'),
(3, 'Protected Areas', '/homebg_image/state_aquatic.png', 'Introduction', 'Wildlife has been revered as a tradition in the history of the country. The concept of Wildlife Conservation dates back to the reign of emperor Ashoka whose rock edicts and pillars are strewn all over the country, urging us to have love and friendship for all creatures.  India is unique in terms of biological diversity due to its unique biogeographical location, diversified climatic condition and enormous ecodiversity and geodiversity. India ranks 8th in the list contributing 10% to the world’s Biological Diversity.', 'rtdfgdfxcg', 'Punjab, the ‘land of five rivers’ is a relatively small state of India occupying an area of 50,362 Sq.Km. It is located in the north western part of India between 29º30’ and 32º32’ North latitude 73º55’ and 76º50’ East longitude. Situated between river Ravi in the North-West and Ghaggar in the east, Punjab shares its boundaries with Jammu & Kashmir in the North, Himachal Pradesh in the North-East and Haryana and Rajasthan to its South. The state also shares a 300 km long international border with Pakistan on the western side.', 'khjkhgkhkh', '2021-06-21 12:04:24', '2021-06-21 12:04:52', '1'),
(4, 'Home', '/homebg_image/img14.jpg', 'Introduction', 'Wildlife has been revered as a tradition in the history of the country. The concept of Wildlife Conservation dates back to the reign of emperor Ashoka whose rock edicts and pillars are strewn all over the country, urging us to have love and friendship for all creatures.India is unique in terms of biological diversity due to its unique biogeographical location, diversified climatic condition and enormous ecodiversity and geodiversity. India ranks 8th in the list contributing 10% to the world’s Biological Diversity.', 'Wild… Wild…. Punjab', 'Punjab, the ‘land of five rivers’ is a relatively small state of India occupying an area of 50,362 Sq.Km. It is located in the north western part of India between 29º30’ and 32º32’ North latitude 73º55’ and 76º50’ East longitude. Situated between river Ravi in the North-West and Ghaggar in the east, Punjab shares its boundaries with Jammu & Kashmir in the North, Himachal Pradesh in the North-East and Haryana and Rajasthan to its South. The state also shares a 300 km long international border with Pakistan on the western side.', 'Broadly the State can be classified in to three geographic zones.', '2021-06-21 13:12:43', '2021-06-21 13:13:07', '1'),
(5, 'Protected Areas', '/homebg_image/elephant.jpg', 'Eagel', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home.', 'Wild. Wild. Punjab', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home.', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home.', '2021-08-03 14:16:27', '2021-08-03 14:22:52', '1'),
(6, 'Protected Areas', '/homebg_image/chhatbir.jpg', 'Introduction', 'Wildlife has been revered as a tradition in the history of the country. The concept of Wildlife gfdfgd', 'Wild.. Wild.. Punjab', 'Wildlife has been revered as a tradition in the history of the country. The concept of Wildlife gddgdd', 'Wildlife has been revered as a tradition in the history of the country. The concept of Wildlife', '2021-08-04 04:58:05', '2021-08-04 04:58:46', '1'),
(7, 'testing', '/homebg_image/photo-1575550959106-5a7defe28b56.JPG', 'testinmg', 'testing', 'testing', 'testing', 'test', '2022-01-12 07:17:47', '2022-01-12 07:18:53', '1'),
(8, 'test', '/homebg_image/pexels-photo-247431.jpeg', 'test', 'test', 'test', 'test', 'test', '2022-02-02 09:08:29', '2022-02-02 09:08:49', '1');

-- --------------------------------------------------------

--
-- Table structure for table `how_apply_type`
--

CREATE TABLE `how_apply_type` (
  `id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `how_apply_type`
--

INSERT INTO `how_apply_type` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'Wildlife Crime and Trafficking', '2021-06-03 18:51:25', '2021-06-03 18:51:25'),
(2, 'Forest Fire Natural Disaster', '2021-06-03 18:52:00', '2021-06-03 18:52:03'),
(3, 'Population Count / Census', '2021-06-03 18:52:06', '2021-06-03 18:52:06'),
(4, 'Habitat Behaviour of Animal ', '2021-06-03 18:52:36', '2021-06-03 18:52:39'),
(5, 'collaborative partnership', '2021-06-03 18:53:02', '2021-06-03 18:53:05'),
(6, 'Eco Tourism', '2021-06-09 11:15:46', '2021-06-09 11:15:49');

-- --------------------------------------------------------

--
-- Table structure for table `how_to_apply_detail`
--

CREATE TABLE `how_to_apply_detail` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `type_id` int NOT NULL,
  `district_id` int NOT NULL,
  `district_name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `how_to_apply_detail`
--

INSERT INTO `how_to_apply_detail` (`id`, `email`, `mobile_number`, `type_id`, `district_id`, `district_name`, `description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(18, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:40:45', '2021-12-28 12:40:45', '0'),
(19, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:40:46', '2021-12-28 12:40:46', '0'),
(20, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:40:47', '2021-12-28 12:40:47', '0'),
(21, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:40:48', '2021-12-28 12:40:48', '0'),
(22, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:40:50', '2021-12-28 12:40:50', '0'),
(23, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:40:51', '2021-12-28 12:40:51', '0'),
(24, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', '', '2021-12-28 12:41:08', '2021-12-28 12:41:08', '0'),
(25, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:15', '2022-01-04 11:44:15', '0'),
(26, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:17', '2022-01-04 11:44:17', '0'),
(27, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:19', '2022-01-04 11:44:19', '0'),
(28, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:20', '2022-01-04 11:44:20', '0'),
(29, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:20', '2022-01-04 11:44:20', '0'),
(30, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:20', '2022-01-04 11:44:20', '0'),
(31, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:38', '2022-01-04 11:44:38', '0'),
(32, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:40', '2022-01-04 11:44:40', '0'),
(33, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:42', '2022-01-04 11:44:42', '0'),
(34, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:43', '2022-01-04 11:44:43', '0'),
(35, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:43', '2022-01-04 11:44:43', '0'),
(36, 'Akriti.singla2003@gmail.com', '', 4, 15, 'Muktsar', 'I am writing to enquire about any opportunities for volunteering .\r\n\r\nI am currently pursuing bsc( hons.) Zoology and i am very passianate about wildlife and want to  have interaction  and gain valuable skills.', '2022-01-04 11:44:43', '2022-01-04 11:44:43', '0'),
(66, 'jaskaransinghsethi02487@gmail.com', '8437826693', 4, 12, 'Ludhiana', '', '2022-02-03 15:01:18', '2022-02-03 15:01:18', '0'),
(67, 'jaskaransinghsethi02487@gmail.com', '8437826693', 4, 12, 'Ludhiana', '', '2022-02-03 15:01:19', '2022-02-03 15:01:19', '0'),
(68, 'jaskaransinghsethi02487@gmail.com', '8437826693', 4, 12, 'Ludhiana', '', '2022-02-03 15:01:20', '2022-02-03 15:01:20', '0'),
(69, 'neha.k@emobx.com', '5432156789', 2, 12, 'Ludhiana', 'hii,,', '2022-02-11 12:17:31', '2022-02-11 12:17:31', '0');

-- --------------------------------------------------------

--
-- Table structure for table `imp_link_detail`
--

CREATE TABLE `imp_link_detail` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `imp_link_detail`
--

INSERT INTO `imp_link_detail` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/impLink/slide1.png', 'Important Link', '2021-06-07 12:56:53', '2022-01-31 14:04:46', '0'),
(2, '/impLink/4.png', 'ghgh', '2021-06-23 07:50:16', '2021-06-23 07:53:17', '1'),
(3, '/impLink/img1.jpg', 'ghjkhj', '2021-08-04 13:10:53', '2021-08-04 13:10:56', '1');

-- --------------------------------------------------------

--
-- Table structure for table `imp_link_links`
--

CREATE TABLE `imp_link_links` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `logo` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `links` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `imp_link_links`
--

INSERT INTO `imp_link_links` (`id`, `banner_id`, `logo`, `title`, `links`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '/impLink/RK Mishra.jpg', 'gdgsfg', 'https://moef.gov.in/en/', '2022-05-05 10:55:55', '2022-05-19 11:14:25', '2022-05-19 11:14:25'),
(2, 1, '', 'dszfgzdgfdgdgf', 'https://moef.gov.in/en/', '2022-05-05 11:13:45', '2022-05-05 11:16:09', NULL),
(4, 1, '/impLink/youtube_black.png', 'GdfGdfg', 'https://moef.gov.in/en/', '2022-05-19 11:05:23', '2022-05-19 11:13:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `landing_banner_images_text`
--

CREATE TABLE `landing_banner_images_text` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `landing_banner_images_text`
--

INSERT INTO `landing_banner_images_text` (`id`, `title`, `subtitle`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Department of Wildlife Preservation, Punjab.', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home...', '2022-05-05 15:43:48', '2022-05-06 06:13:58', NULL),
(2, 'vfdgvdf', 'dgggggggg', '2022-05-05 14:01:54', '2022-05-05 14:01:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `management_nangal`
--

CREATE TABLE `management_nangal` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `management_nangal` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `management_nangal`
--

INSERT INTO `management_nangal` (`id`, `banner_id`, `management_nangal`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Management Plan Nangal Wildlife Sanctuary', '2021-06-14 06:21:40', '2021-06-14 06:21:40', '0');

-- --------------------------------------------------------

--
-- Table structure for table `management_nangal_pdf`
--

CREATE TABLE `management_nangal_pdf` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `nangal_text` varchar(255) NOT NULL,
  `nangal_pdf` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `management_nangal_pdf`
--

INSERT INTO `management_nangal_pdf` (`id`, `banner_id`, `nangal_text`, `nangal_pdf`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'MANAGEMENT PLAN AMENDED', '/act_rulepdf/1623651701.pdf44.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(2, 1, 'NEW CONTENTS', '/act_rulepdf/1623651701.pdf45.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(3, 1, 'LIST OF VACCINATION', '/act_rulepdf/1623651701.pdf46.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(4, 1, 'BIRD CENSES NANGAL', '/act_rulepdf/1623651701.pdf47.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(5, 1, 'LIST OF 10KM VILLAGES', '/act_rulepdf/1623651701.pdf48.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(6, 1, 'LIST OF VAPPON HOLDER NANGAL', '/act_rulepdf/1623651701.pdf49.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(7, 1, 'NOTIFICATION', '/act_rulepdf/1623651701.pdf50.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(8, 1, 'ANNEXURES WLS NANGAL', '/act_rulepdf/1623651701.pdf51.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(9, 1, 'TEMPRATURE & RAINFALL', '/act_rulepdf/1623651701.pdf52.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(10, 1, 'PHYSICAL & FINANCIAL', '/act_rulepdf/1623651701.pdf53.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(11, 1, 'THE BUDGET( 10 Year)', '/act_rulepdf/1623651701.pdf54.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0'),
(12, 1, 'WATERFOWL CENSUS NANGAL', '/act_rulepdf/1623651701.pdf54.pdf', '2021-06-14 06:21:41', '2021-06-14 06:21:41', '0');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `minister_detail`
--

CREATE TABLE `minister_detail` (
  `id` int NOT NULL,
  `minister_image` varchar(255) NOT NULL,
  `minister_heading` varchar(255) NOT NULL,
  `minister_title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `minister_detail`
--

INSERT INTO `minister_detail` (`id`, `minister_image`, `minister_heading`, `minister_title`, `description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/minister_image/Skype_Picture_2022_04_21T11_59_24_603Z.jpeg', 'Bhagwant Mann', 'Chief Minister, Punjab', '', '2021-06-01 13:48:58', '2022-04-21 12:04:40', '0'),
(2, '/minister_image/images.jpg', 'Lal Chand Kataruchak', 'Minister of Forest', '', '2021-06-01 13:50:53', '2022-04-20 12:39:39', '0'),
(3, '/minister_image/logo_dummy.png', 'Chief Wildlife Warden', 'Warden', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2021-06-01 13:51:25', '2022-04-21 11:09:05', '1'),
(4, '/minister_image/cm.jpg', 'Capt. Amrinder Singh', 'Chief Minister, Punjab', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2021-06-01 13:52:42', '2021-06-04 10:22:52', '1'),
(5, '/minister_image/cm.jpg', 'Capt. Amrinder Singh', 'Chief Minister, Punjab', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2021-06-04 10:32:42', '2021-06-04 10:37:12', '1'),
(6, '/minister_image/elephant.jpg', 'Capt. Amrinder Singh', 'Chief Minister, Punjab', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home.', '2021-08-03 11:03:13', '2021-08-03 11:03:18', '1'),
(7, '/minister_image/3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', 'test', 'test', 'test', '2022-01-12 06:39:20', '2022-01-12 06:41:12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `notification_banner_detail`
--

CREATE TABLE `notification_banner_detail` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `notification_banner_detail`
--

INSERT INTO `notification_banner_detail` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/notification/slide1.png', 'Protected Areas', '2021-05-11 11:41:42', '2022-01-31 13:46:26', '0');

-- --------------------------------------------------------

--
-- Table structure for table `notification_detail`
--

CREATE TABLE `notification_detail` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `pdf_url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `notification_detail`
--

INSERT INTO `notification_detail` (`id`, `banner_id`, `name`, `pdf_url`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Abohar Wildlife Sanctuary', '/notification/1643874014.Abohar WL Sanc not. 7.09.2000.pdf', '2022-01-31 13:55:22', '2022-02-03 07:53:49', '0'),
(2, 1, 'Bir Bhadson Wildlife Sanctuary', '/notification/1643874045.WL sanctuaries noti. 28.2.1952.pdf', '2022-01-31 13:55:35', '2022-02-03 07:53:58', '0'),
(3, 1, 'Bir Aishwan Wildlife Sanctuary', '/notification/1643874059.WL sanctuaries noti. 28.2.1952.pdf', '2022-01-31 13:55:57', '2022-02-03 07:53:37', '0'),
(4, 1, 'Bir Bhunerheri Wildlife Sanctuary', '/notification/1643874072.WL sanctuaries noti. 28.2.1952.pdf', '2022-01-31 13:56:09', '2022-02-03 07:54:04', '0'),
(5, 1, 'Bir Dosanjh Wildlife Sanctuary', '/notification/1643874085.WL sanctuaries noti. 28.2.1952.pdf', '2022-01-31 13:56:24', '2022-02-03 07:54:13', '0'),
(6, 1, 'Bir Moti Bagh Wildlife Sanctuary', '/notification/1643874099.WL sanctuaries noti. 28.2.1952.pdf', '2022-01-31 13:56:38', '2022-02-03 07:54:23', '0'),
(7, 1, 'Bir Gurdialpura Wildlife Sanctuary', '/notification/1643874117.Bir gurdialpura 27.08.2003.pdf', '2022-01-31 13:56:49', '2022-02-03 07:54:32', '0'),
(8, 1, 'Bir Mehas Wildlife Sanctuary', '/notification/1643874129.WL sanctuaries noti. 28.2.1952.pdf', '2022-01-31 13:57:01', '2022-02-03 07:54:42', '0'),
(9, 1, 'Harike Wildlife Sanctuary', '/notification/1643874145.Harike notification 18.11.1999.pdf', '2022-01-31 13:57:13', '2022-02-03 07:54:57', '0'),
(10, 1, 'Kathlaur Kushalia Wildlife Sanctuary', '/notification/1643874163.Kathlaur kushalya WL S 22.06.2007.pdf', '2022-01-31 13:57:23', '2022-02-03 07:42:43', '0'),
(11, 1, 'Jhajjar Bachauli Wildlife Sanctuary', '/notification/1643874179.jhajjar bachhauli 11.12.2003.pdf', '2022-01-31 13:57:35', '2022-02-03 07:55:07', '0'),
(12, 1, 'Takhni Rehmapur Wildlife Sanctuary', '/notification/1643874197.WL Sanctuary Tekhni Rehmpura dt. 8.6.1999.pdf', '2022-01-31 13:57:48', '2022-02-03 07:55:18', '0'),
(13, 1, 'Nangal Wildlife Sanctuary', '/notification/1643637479.pdf34.pdf', '2022-01-31 13:57:59', '2022-02-03 07:55:31', '0'),
(14, 1, 'Beas', '/notification/1643787456.Heritage Booklet.pdf', '2022-02-02 07:37:36', '2022-02-02 07:38:10', '1'),
(15, 1, 'Ropar Wetland Conservation Reserve', '/notification/1643874435.Ropar Conservation Reserve notification.pdf', '2022-02-03 07:47:15', '2022-02-03 07:47:15', '0'),
(16, 1, 'Ranjit Sagar Dam Conservation Reserve', '/notification/1643874474.Ranjit Sagar Dam Conservation Reserve 05.03.2018.pdf', '2022-02-03 07:47:54', '2022-02-03 07:47:54', '0'),
(17, 1, 'Rakh Sarai Amanant Khan Conservation Reserve', '/notification/1643874508.Rakh Sarai Amanat Khan 31.03.2010.pdf', '2022-02-03 07:48:28', '2022-02-03 07:48:28', '0'),
(18, 1, 'Beas River Conservation Reserve', '/notification/1643874536.Beas River Conservation Reserve 29.08.2017.pdf', '2022-02-03 07:48:56', '2022-02-03 07:48:56', '0'),
(19, 1, 'Kali Bein Conservation Reserve', '/notification/1643874568.Kali Bein Conservation Reserve.pdf', '2022-02-03 07:49:28', '2022-02-03 07:49:28', '0'),
(20, 1, 'Keshopur Chhamb Community Reserve', '/notification/1643874596.Keshopur chhamb CR 28.06.2007.pdf', '2022-02-03 07:49:56', '2022-02-03 07:49:56', '0'),
(21, 1, 'Haripura Panniwala Gumjal Diwankhera Community Reserve', '/notification/1643874704.Haripura Panniwala 27.03.2015.pdf', '2022-02-03 07:51:44', '2022-02-03 07:51:44', '0'),
(22, 1, 'Lalwan Community Reserve', '/notification/1643874733.Lalwan Community Reserve 22.06.2007.pdf', '2022-02-03 07:52:13', '2022-02-03 07:52:13', '0'),
(23, 1, 'Siswan Community Reserve', '/notification/1643874773.Siswan Community Reserve 29.08.2017.pdf', '2022-02-03 07:52:53', '2022-02-03 07:52:53', '0');

-- --------------------------------------------------------

--
-- Table structure for table `notification_pdf_detail`
--

CREATE TABLE `notification_pdf_detail` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `name_of_pdf` varchar(255) NOT NULL,
  `pdf_url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organisation_banner_detail`
--

CREATE TABLE `organisation_banner_detail` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `organisation_banner_detail`
--

INSERT INTO `organisation_banner_detail` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/organisation_chart/slide1.png', 'Organization Chart', '2021-06-01 10:38:45', '2022-01-31 13:14:18', '0'),
(2, '/organisation_chart/pexels-photo-247431.jpeg', 'test', '2022-01-12 09:02:57', '2022-01-12 09:05:03', '1');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_table_detail`
--

CREATE TABLE `organisation_table_detail` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `tab_id` int NOT NULL,
  `chart_heading` varchar(255) NOT NULL,
  `designation_of_the_officer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `name_of_the_officer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `range` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `beat` varchar(255) DEFAULT NULL,
  `block` varchar(255) DEFAULT NULL,
  `territorial_range_beat_block` varchar(255) DEFAULT NULL,
  `officer_mobile_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type_of_table` enum('table_data','image') NOT NULL DEFAULT 'table_data',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `organisation_table_detail`
--

INSERT INTO `organisation_table_detail` (`id`, `banner_id`, `tab_id`, `chart_heading`, `designation_of_the_officer`, `name_of_the_officer`, `range`, `beat`, `block`, `territorial_range_beat_block`, `officer_mobile_no`, `image`, `type_of_table`, `created_at`, `updated_at`, `deleted_status`, `deleted_at`) VALUES
(1, 1, 1, 'M.C. Zoological Park, Chhatbir ', 'Field Director, Zoo', 'Smt. K. Kalpana,IFS', 'Office', '', '', '', '9412927001', '/organisation_chart/images.jpg', 'table_data', '2021-06-01 11:20:33', '2022-01-31 13:40:26', '0', NULL),
(2, 1, 1, 'Chhatbir Zoo', 'Forest Guard', 'Sh.Charanjit', 'Office', NULL, NULL, NULL, '7986641790', NULL, 'table_data', '2021-06-01 11:21:15', '2021-06-01 11:21:15', '0', NULL),
(3, 1, 2, 'DFO (WL) PATIALA', 'DFO (WL)', 'Sh. Jugraj Singh P.F.S', 'Division Office', 'NIL', 'Division Office', '', '9872100828', NULL, 'table_data', '2021-06-01 11:22:27', '2022-01-24 09:44:52', '0', NULL),
(4, 1, 2, 'DFO (WL) PATIALA', 'Superintendant', 'Smt. Charanjit Kaur', 'Division Office', 'NIL', 'Division Office', '', '8872135550', NULL, 'table_data', '2021-06-01 11:23:33', '2022-01-24 09:45:48', '0', NULL),
(5, 1, 3, 'Wildlife Division Ferozepur', 'Divisional Forest Officer', 'Sh. Nalin Yadav IFS', 'Incharge of Wildlife Division Ferozepur', NULL, '--', NULL, '9810215781', NULL, 'table_data', '2021-06-01 11:24:37', '2021-06-01 11:24:37', '0', NULL),
(6, 1, 3, 'Wildlife Division Ferozepur', 'Block Officer', 'Sh. Kamaljeet Singh', 'Incharge of Wildlife Range Harike', NULL, '--', NULL, '9814107711', NULL, 'table_data', '2021-06-01 11:25:50', '2021-06-01 11:25:50', '0', NULL),
(7, 1, 4, 'Wildlife Division Phillaur', 'Divisional Forest Officer Wild Life Phillar', 'Sh. Neeraj Kumar, P.F.S.', 'Wild Life division Phillaur', 'Wild Life division Phillaur', 'Wild Life division Phillaur', 'Wild Life division Phillaur', '9463596843', NULL, 'table_data', '2021-06-01 11:26:58', '2021-06-01 11:26:58', '0', NULL),
(8, 1, 4, 'Forest Range Officer', 'Sh. Rajinder Kumar', 'Wild Life Range Jalandhar', '---', NULL, NULL, NULL, '9855966661', NULL, 'table_data', '2021-06-01 11:27:54', '2021-06-01 11:27:54', '0', NULL),
(9, 1, 5, 'Divisional Forest Officer(W/L)Ropar', 'Divisional Forest Officer (WL) Ropar', 'Sh. Dharamveer Dearu, IFS', '---', '', '', '', '7597706403', NULL, 'table_data', '2021-06-01 11:28:53', '2022-01-24 09:51:29', '0', NULL),
(10, 1, 5, 'Divisional Forest Officer(W/L)Ropar', 'Divisonal Forest Officer (W/I) Ropar', 'Sh. Malkeet Singh', 'Wildlife Range Ropar', NULL, NULL, NULL, '9410573044', NULL, 'table_data', '2021-06-01 11:30:17', '2021-06-01 11:30:17', '0', NULL),
(11, 1, 6, 'Wild Life Division Hoshiarpur', 'Forest Range Officer', 'Sh. Rajinder Kumar', 'Dasuya', NULL, '---', NULL, '9855966662', NULL, 'table_data', '2021-06-01 11:32:36', '2021-06-01 11:32:36', '0', NULL),
(12, 1, 6, 'Wild Life Division Hoshiarpur', 'Dy. Ranger', 'Sh Ram Dass', 'Hoshiarpur', NULL, NULL, NULL, '9463438002', NULL, 'table_data', '2021-06-01 11:33:36', '2021-06-01 11:33:36', '0', NULL),
(13, 1, 7, 'Wildlife Division, Pathankot', 'Divisional Forest Officer (WL) Pathankot', 'Sh. Rajesh Kumar, IFS', '---', '', '', '', '9814478400', NULL, 'table_data', '2021-06-01 11:34:23', '2022-01-24 09:52:04', '0', NULL),
(14, 1, 7, 'Wild Life Division Pathankot', 'Forest Ranger', 'Randhir Singh Randhawa', 'Range officer Wildlife Range Pathankot', NULL, NULL, NULL, '9878888553', NULL, 'table_data', '2021-06-01 11:36:05', '2021-06-01 11:36:05', '0', NULL),
(15, 1, 1, 'Chhatbir Zoo', 'Forest Guard', 'Smt.Kulwinder Kaur', 'Office', NULL, NULL, NULL, '6239012741', NULL, 'table_data', '2021-06-01 12:22:02', '2021-06-01 12:22:02', '0', NULL),
(17, 2, 1, 't', 't', 't', 't', 't', 't', 't', '5324362362', NULL, 'table_data', '2022-01-12 09:04:07', '2022-01-12 09:04:07', '0', NULL),
(18, 1, 6, 'Wild Life Division Hoshiarpur', 'Divisional Forest Officer (WL) Hoshiarpur', 'Sh. Gursharan Singh, PFS', '', '', '', '', '9872800071', NULL, 'table_data', '2022-01-24 09:56:33', '2022-01-24 09:56:33', '0', NULL),
(19, 1, 1, 'M.C. Zoological Park, Chhatbir ', 'Deputy Ranger', 'Sh. Arshdeep Singh Rana', '', '', '', '', '9417033139', NULL, 'table_data', '2022-01-24 09:58:59', '2022-01-24 09:58:59', '0', NULL),
(21, 1, 10, 'Administrative', 'Field Director, Zoo', 'Smt. K. Kalpana,IFS', '', '', '', '', '', NULL, 'table_data', '2022-04-21 07:52:55', '2022-04-21 08:13:45', '1', NULL),
(22, 1, 10, 'Administrative', '', '', '', '', '', '', '', NULL, 'table_data', '2022-04-21 08:14:06', '2022-04-21 08:19:43', '1', '2022-05-09 18:58:19'),
(23, 1, 10, 'Administrative', '', '', '', '', '', '', '', NULL, 'table_data', '2022-04-21 08:15:55', '2022-04-21 08:19:49', '1', '2022-05-06 21:11:57'),
(24, 1, 10, 'Administrative', '', '', '', '', '', '', '', '/organisation_chart/images.jpg', 'table_data', '2022-04-21 08:19:15', '2022-04-21 09:33:43', '1', '2022-05-06 21:12:02'),
(25, 1, 10, 'Administrative', '', '', '', '', '', '', '', '/organisation_chart/Administrative Setup.jpg', 'table_data', '2022-04-21 09:34:04', '2022-04-21 09:46:16', '0', '2022-05-06 21:12:08'),
(26, 1, 1, 'M.C. Zoological Park, Chhatbir ', 'Field Director, Zoo', 'Smt. K. Kalpana,IFS', 'Office', '--', '--', '--', '8284878813', NULL, 'table_data', '2022-04-22 07:57:04', '2022-04-22 07:57:04', '0', NULL),
(27, 1, 2, 'DFO (WL) PATIALA', '', '', '', '', '', '', '', '/organisation_chart/sss.png', 'image', '2022-04-22 07:59:44', '2022-04-22 07:59:44', '0', NULL),
(28, 1, 1, 'M.C. Zoological Park, Chhatbir ', '', '', '', '', '', '', '', '/organisation_chart/Skype_Picture_2022_04_21T11_59_24_603Z.jpeg', 'image', '2022-04-22 08:58:53', '2022-04-22 08:58:53', '0', NULL),
(29, 1, 1, 'M.C. Zoological Park, Chhatbir ', '', '', '', '', '', '', '', '/organisation_chart/Skype_Picture_2022_04_21T11_59_24_603Z.jpeg', 'image', '2022-04-22 09:02:05', '2022-04-22 09:02:05', '0', NULL),
(30, 1, 1, 'M.C. Zoological Park, Chhatbir ', '', '', '', '', '', '', '', '/organisation_chart/Skype_Picture_2022_04_21T11_59_24_603Z.jpeg', 'image', '2022-04-22 09:04:39', '2022-04-22 09:04:39', '0', NULL),
(31, 1, 1, 'M.C. Zoological Park, Chhatbir ', '', '', '', '', '', '', '', '/organisation_chart/Skype_Picture_2022_04_21T11_59_24_603Z.jpeg', 'image', '2022-04-22 09:05:05', '2022-04-22 09:05:05', '0', NULL),
(32, 1, 2, 'DFO (WL) PATIALA', '', '', '', '', '', '', '', '/organisation_chart/Administrative Setup.jpg', 'image', '2022-04-22 09:42:07', '2022-05-19 04:36:00', '0', '2022-05-19 04:36:00'),
(33, 1, 3, 'Wildlife Division Ferozepur', '', '', '', '', '', '', '', '/organisation_chart/Administrative Setup.jpg', 'image', '2022-04-22 09:45:46', '2022-05-19 04:35:55', '0', '2022-05-19 04:35:55'),
(34, 1, 4, 'Wildlife Division Phillaur', '', '', '', '', '', '', '', '/organisation_chart/images.jpg', 'image', '2022-04-22 10:03:00', '2022-04-22 10:03:00', '0', NULL),
(35, 1, 10, 'Administrative', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/organisation_chart/Administrative Setup.jpg', 'image', '2022-04-22 10:14:09', '2022-04-22 12:28:24', '0', NULL),
(36, 1, 10, 'Administrative', 'Field Director, Zoo', 'Smt. K. Kalpana,IFS', 'Office', '--', '--', '--', '8294887744', NULL, 'table_data', '2022-04-22 10:15:04', '2022-05-06 15:30:20', '0', '2022-05-02 10:33:24'),
(37, 1, 1, 'M.C. Zoological Park, Chhatbir ', 'gfffffffffffff', 'gddddddddd', 'gddddd', 'gggggggg', 'ddgdddddddf', 'gggggggggggf', '2', NULL, 'table_data', '2022-05-18 11:45:05', '2022-05-18 11:45:40', '0', '2022-05-18 11:45:40');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_tabs`
--

CREATE TABLE `organisation_tabs` (
  `id` int NOT NULL,
  `tab_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `organisation_tabs`
--

INSERT INTO `organisation_tabs` (`id`, `tab_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'M.C. Zoological Park, Chhatbir ', '2021-05-12 14:31:19', '2021-05-12 14:31:23', NULL),
(2, 'DFO (WL) PATIALA', '2021-05-12 14:31:25', '2021-05-12 14:31:27', NULL),
(3, 'Wildlife Division Ferozepur', '2021-05-12 14:31:52', '2021-05-12 14:31:55', NULL),
(4, 'Wildlife Division Phillaur', '2021-05-12 14:31:58', '2021-05-12 14:32:01', NULL),
(5, 'Divisional Forest Officer(W/L)Ropar', '2021-05-12 14:32:26', '2021-05-12 14:32:29', NULL),
(6, 'Wild Life Division Hoshiarpur', '2021-05-12 14:32:33', '2021-05-12 14:32:36', NULL),
(7, 'Wildlife Division, Pathankot', '2021-05-12 14:34:30', '2022-01-25 14:17:20', NULL),
(8, 'ttttttttt', '2022-01-31 13:04:48', '2022-01-31 13:04:52', '2022-01-31 13:04:52'),
(9, 'testing', '2022-02-02 11:33:45', '2022-02-02 11:34:04', '2022-02-02 11:34:04'),
(10, 'Administrative', '2022-04-21 07:52:12', '2022-04-21 07:52:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `protectedarea_category`
--

CREATE TABLE `protectedarea_category` (
  `id` int NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `protectedarea_category`
--

INSERT INTO `protectedarea_category` (`id`, `category_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(50001, 'Wildlife Santuaries', '2022-01-06 11:03:21', '2022-02-08 06:43:59', NULL),
(50002, 'Community  Reserves', '2022-01-06 11:03:28', '2022-01-06 11:03:32', NULL),
(50003, 'Conservation Reserves', '2022-01-06 11:03:54', '2022-01-06 11:03:58', NULL),
(50004, 'kmkkk', '2022-02-08 10:51:32', '2022-02-08 10:51:32', '2022-02-08 10:51:40'),
(50005, 'gdfgd', '2022-02-08 06:31:01', '2022-02-08 06:40:40', '2022-02-08 06:40:40'),
(50006, 'dfg4546456', '2022-02-08 06:33:02', '2022-02-08 06:40:37', '2022-02-08 06:40:37'),
(50007, 'jhjhf!', '2022-02-08 06:34:56', '2022-02-08 06:40:33', '2022-02-08 06:40:33'),
(50008, 'fgfgfgfgfgfgfgfg gfffffff', '2022-02-08 06:35:45', '2022-02-08 06:40:29', '2022-02-08 06:40:29'),
(50009, 'Wildlife Santuaries.', '2022-02-08 06:42:36', '2022-02-08 06:44:07', '2022-02-08 06:44:07'),
(50010, 'Wildlife Santuaries.', '2022-02-08 06:43:08', '2022-02-08 06:44:04', '2022-02-08 06:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `protected_area_subcategory`
--

CREATE TABLE `protected_area_subcategory` (
  `id` int NOT NULL,
  `category_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `protected_area_subcategory`
--

INSERT INTO `protected_area_subcategory` (`id`, `category_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(10001, 50001, 'Bir Moti Bagh Wildlife Sanctuary', '2022-01-06 11:05:39', '2022-01-06 11:05:39', NULL),
(10002, 50001, 'Bir Gurdialpura Wildlife Sanctuary', '2022-01-06 11:05:39', '2022-01-06 11:05:39', NULL),
(10003, 50001, 'Bir Bhunerheri Wildlife Sanctuary', '2022-01-06 11:06:27', '2022-01-06 11:06:27', NULL),
(10004, 50001, 'Bir Mehas Wildlife Sanctuary', '2022-01-06 11:06:27', '2022-01-06 11:06:27', NULL),
(10005, 50001, 'Bir Dosanjh Wildlife Sanctuary', '2022-01-06 11:07:15', '2022-01-06 11:07:15', NULL),
(10006, 50001, 'Bir Bhadson Wildlife Sanctuary ', '2022-01-06 11:07:15', '2022-01-06 11:07:15', NULL),
(10007, 50001, ' Bir Aishwan Wildlife Sanctuary ', '2022-01-06 11:08:07', '2022-01-06 11:08:07', NULL),
(10008, 50001, ' Abohar Wildlife Sanctuary', '2022-01-06 11:08:07', '2022-01-06 11:08:07', NULL),
(10009, 50001, ' Harike Wildlife Sanctuary ', '2022-01-06 11:08:55', '2022-01-06 11:08:55', NULL),
(10010, 50001, ' Takhni Rehmapur Wildlife Sanctuary', '2022-01-06 11:08:55', '2022-01-06 11:08:55', NULL),
(10011, 50001, ' Jhajjar-Bachauli Wildlife Sanctuary', '2022-01-06 11:09:40', '2022-01-06 11:09:40', NULL),
(10012, 50001, ' Kathlaur-Kushlian Wildlife Sanctuary ', '2022-01-06 11:09:40', '2022-01-06 11:09:40', NULL),
(10013, 50002, 'Panniwala-Gumjal-Haripura-Diwankhera Community Reserve', '2022-01-06 11:19:20', '2022-01-06 11:19:20', NULL),
(10014, 50002, 'Lalwan Community Reserve', '2022-01-06 11:19:20', '2022-01-06 11:19:20', NULL),
(10015, 50002, 'Keshopur-Miani Community Reserve', '2022-01-06 11:19:52', '2022-01-06 11:19:52', NULL),
(10016, 50002, 'Siswan Community Reserve', '2022-01-06 11:19:52', '2022-01-06 11:19:52', NULL),
(10017, 50003, 'Rakh Sarai Amanat Khan Conservation Reserve', '2022-01-06 11:20:31', '2022-01-06 11:20:31', NULL),
(10018, 50003, 'Ropar Wetland Conservation Reserve', '2022-01-06 11:20:31', '2022-01-06 11:20:31', NULL),
(10019, 50003, 'Ranjit Sagar Dam Conservation Reserve', '2022-01-06 11:21:27', '2022-01-06 11:21:27', NULL),
(10020, 50003, 'Beas River Conservation Reserve', '2022-01-06 11:21:27', '2022-01-06 11:21:27', NULL),
(10021, 50003, 'Kali Bein Conservation Reserve', '2022-01-06 11:22:08', '2022-01-06 11:22:08', NULL),
(10022, 50001, 'Nangal Wildlife Sanctuary', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10023, 50002, 'fffffffddddddddddddddddddd', '2022-02-08 07:31:32', '2022-02-08 07:39:20', '2022-02-08 13:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `protected_wetland`
--

CREATE TABLE `protected_wetland` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `protected_wetland`
--

INSERT INTO `protected_wetland` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(150001, 'Harike Wildlife Sanctuary', '2022-01-07 16:20:18', '2022-05-06 14:24:18', NULL),
(150002, 'Nangal Wildlife Sanctuary', '2022-01-07 16:20:18', '2022-01-07 16:20:18', NULL),
(150003, 'Keshopur Community Reserve, Gurdaspur', '2022-01-07 16:21:48', '2022-01-07 16:21:48', NULL),
(150004, 'apexchart', '2022-02-05 10:36:19', '2022-02-05 10:48:39', '2022-02-05 10:48:39');

-- --------------------------------------------------------

--
-- Table structure for table `protected_wetland_images`
--

CREATE TABLE `protected_wetland_images` (
  `id` int NOT NULL,
  `p_w_id` int NOT NULL,
  `images` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `protected_wetland_images`
--

INSERT INTO `protected_wetland_images` (`id`, `p_w_id`, `images`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '/protected_wetland_images/img1.jpg', 'Flight of Coots at Harike Wildlife Sanctuary.', '2022-02-05 12:08:31', '2022-02-05 14:50:32', NULL),
(2, 1, '/protected_wetland_images/img8.jpg', 'Flight of Barheaded Goose at Harike Wildlife Sanctuary', '2022-02-05 12:08:31', '2022-02-05 14:50:40', NULL),
(3, 1, '/protected_wetland_images/kingfisher.png', 'Pied Kingfisher at Harike Wildlife Sanctuary', '2022-02-05 12:08:31', '2022-02-05 14:50:48', NULL),
(4, 2, '/protected_wetland_images/barheaded.png', 'View of Nangal Wildlife Sanctuary', '2022-02-05 12:12:58', '2022-02-05 14:51:04', NULL),
(5, 2, '/protected_wetland_images/img3.jpg', 'Close up of Barheaded Goose', '2022-02-05 12:12:59', '2022-02-05 14:51:15', NULL),
(6, 2, '/protected_wetland_images/kingfisher.png', 'Barheaded Goose', '2022-02-05 12:12:59', '2022-02-05 14:51:22', NULL),
(7, 3, '/protected_wetland_images/barheaded.png', 'Sandpiper at Keshopur Community Reserve', '2022-02-05 12:16:49', '2022-02-05 14:51:32', NULL),
(8, 3, '/protected_wetland_images/img3.jpg', 'Coots at Keshopur Community Reserve', '2022-02-05 12:16:49', '2022-02-05 14:51:38', NULL),
(9, 3, '/protected_wetland_images/img2.jpg', 'Sarus Crane at Keshopur Community Reserve', '2022-02-05 12:16:49', '2022-02-05 14:51:43', NULL),
(10, 1, '/protected_wetland_images/bodyworn-794111_1920.jpg', 'resert', '2022-02-05 13:55:43', '2022-02-05 14:24:25', '2022-02-05 14:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `protected_wetland_noti_images`
--

CREATE TABLE `protected_wetland_noti_images` (
  `id` int NOT NULL,
  `p_w_id` int NOT NULL,
  `images` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `protected_wetland_noti_images`
--

INSERT INTO `protected_wetland_noti_images` (`id`, `p_w_id`, `images`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '/protected_wetland_images/barheaded.png', 'Flight of Coots at Harike Wildlife Sanctuary.', '2022-02-05 12:08:31', '2022-02-05 13:45:22', NULL),
(2, 1, '/protected_wetland_images/img1.jpg', 'Flight of Barheaded Goose at Harike Wildlife Sanctuary', '2022-02-05 12:08:31', '2022-02-09 06:10:32', NULL),
(3, 1, '/protected_wetland_images/kingfisher.png', 'Pied Kingfisher at Harike Wildlife Sanctuary', '2022-02-05 12:08:31', '2022-02-09 06:10:52', NULL),
(4, 2, '/protected_wetland_images/img3.jpg', 'View of Nangal Wildlife Sanctuary', '2022-02-05 12:12:58', '2022-02-09 06:11:06', NULL),
(5, 2, '/protected_wetland_images/img2.jpg', 'Close up of Barheaded Goose', '2022-02-05 12:12:59', '2022-02-09 06:11:22', NULL),
(6, 2, '/protected_wetland_images/img1.jpg', 'Barheaded Goose', '2022-02-05 12:12:59', '2022-02-09 06:11:27', NULL),
(7, 3, '/protected_wetland_images/img16.jpg', 'Sandpiper at Keshopur Community Reserve', '2022-02-05 12:16:49', '2022-02-09 06:11:40', NULL),
(8, 3, '/protected_wetland_images/img6.jpg', 'Coots at Keshopur Community Reserve', '2022-02-05 12:16:49', '2022-02-09 06:11:45', NULL),
(9, 3, '/protected_wetland_images/img5.jpg', 'Sarus Crane at Keshopur Community Reserve', '2022-02-05 12:16:49', '2022-02-09 06:11:52', NULL),
(10, 1, '/protected_wetland_images/bodyworn-794111_1920.jpg', 'resert', '2022-02-05 13:55:43', '2022-02-05 14:24:25', '2022-02-05 14:24:25'),
(11, 4, '/protected_wetland_images/1650448617.wildlife.jpg', 'hbnnnnnnnnnnnnnnnnm', '2022-04-20 09:56:57', '2022-04-20 09:57:08', '2022-04-20 09:57:08'),
(12, 5, '/protected_wetland_images/1651651864.dummy2.jpg', 'hdhd', '2022-05-04 08:11:04', '2022-05-04 08:11:04', NULL),
(13, 6, '/protected_wetland_images/1651652244.dummy2.jpg', 'hdhd', '2022-05-04 08:17:24', '2022-05-04 08:17:24', NULL),
(14, 7, '/protected_wetland_images/1651652327.dummy2.jpg', 'hdhd', '2022-05-04 08:18:47', '2022-05-04 08:18:47', NULL),
(15, 8, '/protected_wetland_images/1651755279.5.png', 'hdhd', '2022-05-05 12:54:39', '2022-05-05 12:54:39', NULL),
(16, 9, '/protected_wetland_images/1652076686.5.png', 'dsaaaaaa', '2022-05-09 06:11:26', '2022-05-09 07:04:03', '2022-05-09 07:04:03'),
(17, 9, '/protected_wetland_images/1652076686.pdf20.pdf', 'dasssss', '2022-05-09 06:11:26', '2022-05-09 07:04:03', '2022-05-09 07:04:03'),
(18, 10, '/protected_wetland_images/1652076731.5.png', 'hffdh', '2022-05-09 06:12:11', '2022-05-09 07:03:58', '2022-05-09 07:03:58'),
(19, 11, '/protected_wetland_images/1652076767.5.png', 'fdssssssssss', '2022-05-09 06:12:47', '2022-05-09 07:03:54', '2022-05-09 07:03:54'),
(20, 12, '/protected_wetland_images/1652078022.5.png', 'sdfsdf', '2022-05-09 06:33:42', '2022-05-09 07:03:47', '2022-05-09 07:03:47'),
(21, 13, '/protected_wetland_images/1652078705.1.png', 'hffdh', '2022-05-09 06:45:05', '2022-05-09 07:03:43', '2022-05-09 07:03:43'),
(22, 14, '/protected_wetland_images/1652078870.5.png', 'dsaaaaaa', '2022-05-09 06:47:50', '2022-05-09 07:03:01', '2022-05-09 07:03:01'),
(23, 15, '/protected_wetland_images/1652079168.5.png', 'hffdh', '2022-05-09 06:52:48', '2022-05-09 07:03:06', '2022-05-09 07:03:06'),
(24, 16, '/protected_wetland_images/1652079259.5.png', 'sdfsdf', '2022-05-09 06:54:19', '2022-05-09 07:03:12', '2022-05-09 07:03:12'),
(25, 17, '/protected_wetland_images/1652079277.5.png', 'sdfsdf', '2022-05-09 06:54:37', '2022-05-09 07:03:19', '2022-05-09 07:03:19'),
(26, 18, '/protected_wetland_images/1652079368.5.png', 'dsaaaaaa', '2022-05-09 06:56:08', '2022-05-09 07:03:24', '2022-05-09 07:03:24'),
(27, 19, '/protected_wetland_images/1652079489.5.png', 'dsaaaaaa', '2022-05-09 06:58:09', '2022-05-09 07:03:30', '2022-05-09 07:03:30'),
(28, 20, '/protected_wetland_images/1652079561.4.png', 'fsdfs', '2022-05-09 06:59:21', '2022-05-09 07:03:33', '2022-05-09 07:03:33'),
(29, 21, '/protected_wetland_images/1652079680.4.png', 'ggggggg', '2022-05-09 07:01:20', '2022-05-09 07:03:38', '2022-05-09 07:03:38'),
(38, 30, '/protected_wetland_images/1652081028.5.png', 'dsaaaaaa', '2022-05-09 07:23:48', '2022-05-09 07:41:08', '2022-05-09 07:41:08'),
(39, 31, '/protected_wetland_images/1652081177.5.png', 'hffdh', '2022-05-09 07:26:17', '2022-05-09 07:49:01', '2022-05-09 07:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `website_link` varchar(255) DEFAULT NULL,
  `status` enum('online','offline') NOT NULL DEFAULT 'online',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `website_link`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Issuance of Hunting Permit for crop damages wild animals like Blue Bull and Wild Boar.', 'https://play.google.com/store/apps/details?id=com.app.wildlifedepartment', 'online', '2022-02-10 15:52:00', '2022-02-10 11:46:24', NULL),
(2, 'Issuance of Permission/NOC for taking Arms License.', 'https://play.google.com/store/apps/details?id=com.app.wildlifedepartment', 'online', '2022-02-10 15:52:00', '2022-02-10 11:46:43', NULL),
(3, 'Research, Study, Survey .', '', 'offline', '2022-02-10 15:52:30', '2022-02-10 11:30:49', NULL),
(4, 'Filmshooting, Flash photography, Flash videography ', '', 'offline', '2022-02-10 15:52:30', '2022-02-10 15:52:30', NULL),
(5, 'Developmental', 'http://parivesh.nic.in/', 'online', '2022-02-10 15:53:13', '2022-02-10 11:47:29', NULL),
(6, 'apexchart', '', 'online', '2022-02-10 11:23:24', '2022-02-10 11:31:25', '2022-02-10 11:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `services_detail`
--

CREATE TABLE `services_detail` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `services_detail`
--

INSERT INTO `services_detail` (`id`, `banner_image`, `banner_heading`, `title`, `description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/services/slide1.png', 'Service', 'Issuance of Hunting Permit for crop damages wild animals like Blue Bull and Wild Boar.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-06-09 08:14:51', '2021-06-23 07:36:17', '0'),
(2, '/services/5.png', 'hgjgh', 'jhgjgh', 'jhgh', '2021-06-23 07:36:39', '2021-06-23 07:38:40', '1'),
(3, '/services/slide1.png', 'Demo', 'Demo', 'DEO', '2021-07-06 06:38:45', '2022-01-31 14:03:57', '1');

-- --------------------------------------------------------

--
-- Table structure for table `services_how_to_apply`
--

CREATE TABLE `services_how_to_apply` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `aadhar_number` varchar(255) NOT NULL,
  `address_one` varchar(255) NOT NULL,
  `address_two` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `services_how_to_apply`
--

INSERT INTO `services_how_to_apply` (`id`, `name`, `email_address`, `mobile_number`, `aadhar_number`, `address_one`, `address_two`, `city`, `zip`, `ip`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'Niya Sharma', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhianaa', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.20.128', '2021-07-23 11:42:20', '2021-07-23 11:42:20', '0'),
(2, 'Niya Sharma', 'nehakumari@emobx.com', '8965745692', '896574596692', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.212.70', '2021-08-17 11:35:38', '2021-08-17 11:35:38', '0'),
(3, 'test', 'nehakumari@emobx.com', '8965745692', '896574596692', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.212.70', '2021-08-17 12:41:24', '2021-08-17 12:41:24', '0'),
(4, 'report', 'nehakumari@emobx.com', '8965745692', '896574596692', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.212.70', '2021-08-17 12:46:19', '2021-08-17 12:46:19', '0'),
(5, 'report', 'nehakumari@emobx.com', '8965745692', '896574596692', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.212.70', '2021-08-17 13:00:07', '2021-08-17 13:00:07', '0'),
(6, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 06:13:24', '2021-08-18 06:13:24', '0'),
(7, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 06:16:50', '2021-08-18 06:16:50', '0'),
(8, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 06:17:43', '2021-08-18 06:17:43', '0'),
(9, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 06:18:53', '2021-08-18 06:18:53', '0'),
(10, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhianaa', 'house no - 125, stno- 2 , shanker Colony , Ludhianaa', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 07:24:55', '2021-08-18 07:24:55', '0'),
(11, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhian.', 'house no - 125, stno- 2 , shanker Colony , Ludhian.', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 12:13:15', '2021-08-18 12:13:15', '0'),
(12, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 12:14:36', '2021-08-18 12:14:36', '0'),
(13, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 12:35:26', '2021-08-18 12:35:26', '0'),
(14, 'neha', 'neha.kumari@emobx.com', '8521479632', '852741963243', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'house no - 125, stno- 2 , shanker Colony , Ludhiana', 'Ludhiana', '141015', '124.253.55.205', '2021-08-18 12:51:56', '2021-08-18 12:51:56', '0'),
(15, 'apexchart', 'hgd@gmail.com', '8285749658', '852147859632', 'ytgjgdhjhg dgjgjgh', 'ytgjgdhjhg dgjgjgh', 'Ludhiana', '141015', '124.253.154.113', '2021-12-29 06:19:06', '2021-12-29 06:19:06', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tender_banner_detail`
--

CREATE TABLE `tender_banner_detail` (
  `id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tender_banner_detail`
--

INSERT INTO `tender_banner_detail` (`id`, `banner_image`, `banner_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/tender/slide1.png', 'Tender', '2021-05-31 10:56:38', '2021-12-28 05:55:59', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tender_details`
--

CREATE TABLE `tender_details` (
  `id` int NOT NULL,
  `tender_type_id` int NOT NULL,
  `tender_type_name` varchar(255) NOT NULL,
  `banner_id` int NOT NULL,
  `text` varchar(255) NOT NULL,
  `tender_pdf` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tender_details`
--

INSERT INTO `tender_details` (`id`, `tender_type_id`, `tender_type_name`, `banner_id`, `text`, `tender_pdf`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Upcoming Tender', 1, 'E-Tender for e-rickshaw at Bathinda Zoo, Bathinda (Year 2021-2022)', '/tender/bathinda of e-rickshaw 2021-2022 (2).pdf', '2021-05-31 13:22:17', '2021-05-31 13:22:17', '0'),
(2, 1, 'Upcoming Tender', 1, 'E-Tender for managing parking at Bathinda Zoo, Bathinda (Year 2021-2022)', '/tender/bathinda of e-rickshaw 2021-2022 (2).pdf', '2021-05-31 13:22:54', '2021-05-31 13:22:54', '0'),
(3, 2, 'Previous Tender', 1, 'E-Tender for Operating the Lion Safari Canteen at Chhatbir Zoo (Year 2021-2022)', '/tender/bathinda of e-rickshaw 2021-2022 (2).pdf', '2021-06-01 07:16:57', '2021-06-01 07:16:57', '0'),
(4, 1, 'Upcoming Tender', 1, 'gdfgdf', '/tender/pdf14.pdf', '2022-01-31 14:06:23', '2022-01-31 15:39:29', '1'),
(5, 1, 'Upcoming Tender', 1, 'khgkhkhj', '/tender/pdf3.pdf', '2022-01-31 15:39:15', '2022-01-31 15:43:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tender_types`
--

CREATE TABLE `tender_types` (
  `id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tender_types`
--

INSERT INTO `tender_types` (`id`, `type_name`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'Upcoming Tender', '2021-05-31 17:01:36', '2021-05-31 17:01:36', '0'),
(2, 'Previous Tender', '2021-05-31 17:01:36', '2021-05-31 17:01:36', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tourism_banner_detail`
--

CREATE TABLE `tourism_banner_detail` (
  `id` int NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tourism_banner_detail`
--

INSERT INTO `tourism_banner_detail` (`id`, `banner_heading`, `banner_image`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'Tourism', '/tourism/slide1.png', '2022-02-02 05:35:56', '2022-02-02 05:39:04', '0'),
(2, 'testing', '/tourism/photo-1575550959106-5a7defe28b56.JPG', '2022-02-02 10:53:10', '2022-02-02 10:56:27', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tourism_data`
--

CREATE TABLE `tourism_data` (
  `id` int NOT NULL,
  `banner_id` int NOT NULL,
  `tourism_type_id` int NOT NULL,
  `image` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tourism_data`
--

INSERT INTO `tourism_data` (`id`, `banner_id`, `tourism_type_id`, `image`, `time`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 1, '/tourism/tourism1.jpg', '9:00am - 5:00pm', '2021-05-13 12:43:10', '2022-02-02 05:04:25', '0'),
(2, 1, 2, '/tourism/Ropar_Wetland_03.jpg', '9:00am - 5:00pm', '2021-05-13 12:43:57', '2021-05-13 12:43:57', '0'),
(3, 1, 3, '/tourism/Nangal-Wetlands-Punjab.jpg', '9:00am - 5:00pm', '2021-05-13 12:45:23', '2021-05-13 12:45:23', '0'),
(4, 1, 4, '/tourism/keshopur.jpeg', '9:00am - 5:00pm', '2021-05-13 12:46:14', '2021-05-13 12:46:14', '0'),
(5, 1, 5, '/tourism/ranjit-sagar.jpg', '9:00am - 5:00pm', '2021-05-13 12:47:14', '2021-05-13 12:47:14', '0'),
(6, 1, 6, '/tourism/siswan.jpg', '9:00am - 5:00pm', '2021-05-13 12:47:44', '2021-05-13 12:47:44', '0'),
(7, 1, 1, '/tourism/pexels-photo-247431.jpeg', 't', '2022-01-12 08:19:27', '2022-02-02 05:48:21', '1'),
(8, 2, 1, '/tourism/pexels-photo-247431.jpeg', 'test', '2022-02-02 10:54:31', '2022-02-02 10:55:46', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tourism_link_detail`
--

CREATE TABLE `tourism_link_detail` (
  `id` int NOT NULL,
  `tourism_type_id` int NOT NULL,
  `banner_id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `description_heading` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `heading_how_to_reach` varchar(255) NOT NULL,
  `detail_how_to_reach` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `heading_permission_required` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `detail_required_permmision` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `heading_hire_a_guide` varchar(255) NOT NULL,
  `descripton_hire_guide` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `heading_accommodation_options` varchar(255) NOT NULL,
  `detail_accommodation_options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `heading_do` varchar(255) NOT NULL,
  `detail_do` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `heading_dont` varchar(255) NOT NULL,
  `detail_dont` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `heading_best_time_visit` varchar(255) NOT NULL,
  `detail_best_time_visit` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `images_heading` varchar(255) NOT NULL,
  `map_lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0',
  `map_long` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `zoom_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tourism_link_detail`
--

INSERT INTO `tourism_link_detail` (`id`, `tourism_type_id`, `banner_id`, `banner_image`, `banner_heading`, `description_heading`, `description`, `heading_how_to_reach`, `detail_how_to_reach`, `heading_permission_required`, `detail_required_permmision`, `heading_hire_a_guide`, `descripton_hire_guide`, `heading_accommodation_options`, `detail_accommodation_options`, `heading_do`, `detail_do`, `heading_dont`, `detail_dont`, `heading_best_time_visit`, `detail_best_time_visit`, `images_heading`, `map_lat`, `map_long`, `zoom_level`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 1, '/tourism/world_wetland.png', 'Harike Wildlife Sanctuary', 'Description', '<p>Harike Wildlife Sanctuary also locally known as &quot;Hari-ke-Pattan&quot; is one of the largest wetlands in northern India situated at the confluence of two major rivers of Punjab i.e., Sutlej and Beas. Harike wetland came into existence in 1953 due to the construction of a barrage on River Sutlej. The Beas and Sutlej rivers together bring in about 25 million-acre feet of water per annum to the Harike Wildlife Sanctuary. This man-made, riverine wetland spreads across three districts of Punjab i.e., Tarn Taran, Ferozepur and Kapurthala.</p>', 'How to Reach?', '<p>Near Bus Stop&nbsp;:&nbsp;<strong>Firozpur</strong></p>\r\n\r\n<p>Near Railway Station&nbsp;:&nbsp;<strong>Firozpur Railway Station</strong></p>\r\n\r\n<p>Near Airport&nbsp;:&nbsp;<strong>Amritsar</strong></p>', 'Permission Required', '<p><strong>Officer to be contacted</strong></p>\r\n\r\n<p>DFO&nbsp;:&nbsp;<strong>Firozpur</strong></p>\r\n\r\n<p>Mobile Number&nbsp;:&nbsp;<strong>9965321285</strong></p>', 'How to hire a Guide?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Accommodation options available?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Do\'s', '<p><strong>Noticed Fire</strong></p>\r\n\r\n<p>Immediately call Forest Fire Disaster Management and try to control with community participation.</p>\r\n\r\n<p><strong>Plastic Garbage</strong></p>\r\n\r\n<p>Inform the nearest officer</p>', 'Don\'ts', '<p>Don&#39;t cut any Tree</p>\r\n\r\n<p>Don&#39;t Loudly Music</p>\r\n\r\n<p>Don&#39;t hit Animals &amp; Birds</p>', 'Best Time to Visit', '<p>Best months to visit Harike Wildlife Sanctuary are September and October</p>', 'Images', '30.2889659', '76.402755', '9', '2021-06-10 11:49:53', '2022-05-12 11:25:48', '0'),
(2, 3, 1, '/tourism/slide1.png', 'Nangal Wildlife Sanctuary', 'Description', '<p>Harike Wildlife Sanctuary also locally known as &quot;Hari-ke-Pattan&quot; is one of the largest wetlands in northern India situated at the confluence of two major rivers of Punjab i.e., Sutlej and Beas. Harike wetland came into existence in 1953 due to the construction of a barrage on River Sutlej. The Beas and Sutlej rivers together bring in about 25 million-acre feet of water per annum to the Harike Wildlife Sanctuary. This man-made, riverine wetland spreads across three districts of Punjab i.e., Tarn Taran, Ferozepur and Kapurthala.</p>', 'How to Reach?', '<p>Near Bus Stop&nbsp;:&nbsp;Firozpur Near Railway Station&nbsp;:&nbsp;Firozpur Railway Station Near Airport&nbsp;:&nbsp;Amritsar</p>', 'Permission Required', '<p>Officer to be contacted DFO&nbsp;:&nbsp;Firozpur Mobile Number&nbsp;:&nbsp;9965321285</p>', 'How to hire a Guide?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Accommodation options available?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Do\'s', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Don\'ts', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Best Time to Visit', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Images', '30.2889659', '76.402755', '10', '2021-08-04 12:37:20', '2022-05-18 08:51:16', '0'),
(3, 2, 1, '/tourism/slide1.png', 'Ropar Conservation Reserve', 'Description.', '<p>Harike Wildlife Sanctuary also locally known as &quot;Hari-ke-Pattan&quot; is one of the largest wetlands in northern India situated at the confluence of two major rivers of Punjab i.e., Sutlej and Beas. Harike wetland came into existence in 1953 due to the construction of a barrage on River Sutlej. The Beas and Sutlej rivers together bring in about 25 million-acre feet of water per annum to the Harike Wildlife Sanctuary. This man-made, riverine wetland spreads across three districts of Punjab i.e., Tarn Taran, Ferozepur and Kapurthala.</p>', 'How to Reach?', '<p>Near Bus Stop&nbsp;:&nbsp;Firozpur Near Railway Station&nbsp;:&nbsp;Firozpur Railway Station Near Airport&nbsp;:&nbsp;Amritsar</p>', 'Permission Required', '<p>Officer to be contacted DFO&nbsp;:&nbsp;Firozpur Mobile Number&nbsp;:&nbsp;9965321285</p>', 'How to hire a Guide?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Accommodation options available?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Do\'s', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Don\'ts', '<p>Don&#39;t cut any Tree Don&#39;t Loudly Music Don&#39;t hit Animals &amp; Birds</p>', 'Best Time to Visit', '<p>Best months to visit Harike Wildlife Sanctuary are September and October</p>', 'Images', '30.2889659', '76.402755', '10', '2021-06-10 11:49:53', '2022-05-18 08:48:04', '0'),
(4, 4, 1, '/tourism/slide1.png', 'Keshopur-miani community Reserve', 'Description', '<p>Harike Wildlife Sanctuary also locally known as &quot;Hari-ke-Pattan&quot; is one of the largest wetlands in northern India situated at the confluence of two major rivers of Punjab i.e., Sutlej and Beas. Harike wetland came into existence in 1953 due to the construction of a barrage on River Sutlej. The Beas and Sutlej rivers together bring in about 25 million-acre feet of water per annum to the Harike Wildlife Sanctuary. This man-made, riverine wetland spreads across three districts of Punjab i.e., Tarn Taran, Ferozepur and Kapurthala.</p>', 'How to Reach?', '<p>Near Bus Stop&nbsp;:&nbsp;Firozpur Near Railway Station&nbsp;:&nbsp;Firozpur Railway Station Near Airport&nbsp;:&nbsp;Amritsar</p>', 'Permission Required', '<p>Officer to be contacted DFO&nbsp;:&nbsp;Firozpur Mobile Number&nbsp;:&nbsp;9965321285</p>', 'How to hire a Guide?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Accommodation options available?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Do\'s', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Don\'ts', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Best Time to Visit', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Images', NULL, NULL, NULL, '2021-08-04 12:37:20', '2022-05-18 08:52:03', '0'),
(5, 5, 1, '/tourism/world_wetland.png', 'Ranjit Sagar Conservation Reserve', 'Description', '<p>Harike Wildlife Sanctuary also locally known as &quot;Hari-ke-Pattan&quot; is one of the largest wetlands in northern India situated at the confluence of two major rivers of Punjab i.e., Sutlej and Beas. Harike wetland came into existence in 1953 due to the construction of a barrage on River Sutlej. The Beas and Sutlej rivers together bring in about 25 million-acre feet of water per annum to the Harike Wildlife Sanctuary. This man-made, riverine wetland spreads across three districts of Punjab i.e., Tarn Taran, Ferozepur and Kapurthala.</p>', 'How to Reach?', '<p>Near Bus Stop&nbsp;:&nbsp;<strong>Firozpur</strong></p>\r\n\r\n<p>Near Railway Station&nbsp;:&nbsp;<strong>Firozpur Railway Station</strong></p>\r\n\r\n<p>Near Airport&nbsp;:&nbsp;<strong>Amritsar</strong></p>', 'Permission Required', '<p><strong>Officer to be contacted</strong></p>\r\n\r\n<p>DFO&nbsp;:&nbsp;<strong>Firozpur</strong></p>\r\n\r\n<p>Mobile Number&nbsp;:&nbsp;<strong>9965321285</strong></p>', 'How to hire a Guide?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Accommodation options available?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Do\'s', '<p><strong>Noticed Fire</strong></p>\r\n\r\n<p>Immediately call Forest Fire Disaster Management and try to control with community participation.</p>\r\n\r\n<p><strong>Plastic Garbage</strong></p>\r\n\r\n<p>Inform the nearest officer</p>', 'Don\'ts', '<p>Don&#39;t cut any Tree</p>\r\n\r\n<p>Don&#39;t Loudly Music</p>\r\n\r\n<p>Don&#39;t hit Animals &amp; Birds</p>', 'Best Time to Visit', '<p>Best months to visit Harike Wildlife Sanctuary are September and October</p>', 'Images', NULL, NULL, NULL, '2021-06-10 11:49:53', '2022-05-18 08:52:30', '0'),
(6, 6, 1, '/tourism/slide1.png', 'Siswan Community Reserve', 'Description', '<p>Harike Wildlife Sanctuary also locally known as &quot;Hari-ke-Pattan&quot; is one of the largest wetlands in northern India situated at the confluence of two major rivers of Punjab i.e., Sutlej and Beas. Harike wetland came into existence in 1953 due to the construction of a barrage on River Sutlej. The Beas and Sutlej rivers together bring in about 25 million-acre feet of water per annum to the Harike Wildlife Sanctuary. This man-made, riverine wetland spreads across three districts of Punjab i.e., Tarn Taran, Ferozepur and Kapurthala.</p>', 'How to Reach?', '<p>Near Bus Stop&nbsp;:&nbsp;Firozpur Near Railway Station&nbsp;:&nbsp;Firozpur Railway Station Near Airport&nbsp;:&nbsp;Amritsar</p>', 'Permission Required', '<p>Officer to be contacted DFO&nbsp;:&nbsp;Firozpur Mobile Number&nbsp;:&nbsp;9965321285</p>', 'How to hire a Guide?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Accommodation options available?', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>', 'Do\'s', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Don\'ts', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Best Time to Visit', '<p>Noticed Fire Immediately call Forest Fire Disaster Management and try to control with community participation. Plastic Garbage Inform the nearest officer</p>', 'Images', '30.2889659', '76.402755', '10', '2021-08-04 12:37:20', '2022-05-18 07:50:01', '0'),
(7, 1, 1, '/tourism/RK Mishra.jpg', 'bfgfdgd', 'Description.', '<p>gfffffffffff</p>', 'jggggggg', '<p>fhhhhhhhh</p>', 'jhggggggggggggggggggg', '<p>hggggggggggggc</p>', 'jhghghghghghghghg', '<p>hfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd</p>', 'jhggggggggg', '<p>hgcddddddddddd</p>', 'jhgggggggggg', '<p>fgggggggggggggg</p>', 'jhggggggggg', '<p>drfrfrfrfrfrfrfrf</p>', 'jhgggggggggg', '<p>fssssssssssssss</p>', 'jhhhhhhhhhhhhhhhh', '', '', '', '2022-05-12 10:35:18', '2022-05-12 10:35:24', '1'),
(8, 1, 1, '/tourism/black_buck.jpg', 'hg', 'Description.', '<p>fsddddddddddd</p>', 'jggggggg', '<p>fssssssssssssss</p>', 'jhggggggggggggggggggg', '<p>fsdddddddddd</p>', 'jhghghghghghghghgfds', '<p>fffffffffffffffff</p>', 'jhggggggggg', '<p>fdsdsdsdsdsdsdsdsdsdsdsdsdsds</p>', 'jhgggggggggg', '<p>fdsdsdsdsdsdsdsdsdsdsdsds</p>', 'jhggggggggg', '<p>sfdddddddddddd</p>', 'jhgggggggggg', '<p>sfddddddddddd</p>', 'jhhhhhhhhhhhhhhhh', '30.2889659', '76.402755', '93', '2022-05-12 11:15:21', '2022-05-12 11:26:07', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tourism_link_images`
--

CREATE TABLE `tourism_link_images` (
  `id` int NOT NULL,
  `tourism_link_id` int NOT NULL,
  `tourism_type_id` int NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tourism_link_images`
--

INSERT INTO `tourism_link_images` (`id`, `tourism_link_id`, `tourism_type_id`, `image`, `created_at`, `updated_at`, `deleted_at`, `deleted_status`) VALUES
(1, 1, 1, '/tourism/img1.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(2, 1, 1, '/tourism/img2.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(3, 1, 1, '/tourism/img3.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(4, 1, 1, '/tourism/img4.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(5, 1, 1, '/tourism/img5.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(6, 1, 1, '/tourism/img6.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(7, 1, 1, '/tourism/img7.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(8, 1, 1, '/tourism/img8.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(9, 1, 1, '/tourism/img9.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(10, 1, 1, '/tourism/img10.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(11, 1, 1, '/tourism/img11.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(12, 1, 1, '/tourism/img12.jpg', '2021-06-26 08:45:39', '2021-06-26 08:45:39', NULL, '0'),
(13, 2, 3, '/tourism/img1.jpg', '2021-08-04 12:37:20', '2021-08-04 12:37:20', NULL, '0'),
(14, 2, 3, '/tourism/img2.jpg', '2021-08-04 12:37:20', '2021-08-04 12:37:20', NULL, '0'),
(15, 2, 3, '/tourism/img12.jpg', '2021-08-04 12:37:20', '2021-08-04 12:37:20', NULL, '0'),
(16, 2, 3, '/tourism/img13.jpg', '2021-08-04 12:37:20', '2021-08-04 12:37:20', NULL, '0'),
(17, 7, 1, '/tourism/bodyworn-794111_1920.jpg', '2022-05-12 10:35:18', '2022-05-12 10:35:24', '2022-05-12 10:35:24', '0'),
(18, 7, 1, '/tourism/img5.jpg', '2022-05-12 10:35:18', '2022-05-12 10:35:24', '2022-05-12 10:35:24', '0'),
(19, 8, 1, '/tourism/duck.jpg', '2022-05-12 11:15:21', '2022-05-12 11:26:07', '2022-05-12 11:26:07', '0'),
(20, 8, 1, '/tourism/elephant.jpg', '2022-05-12 11:15:21', '2022-05-12 11:26:07', '2022-05-12 11:26:07', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tourism_types`
--

CREATE TABLE `tourism_types` (
  `id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tourism_types`
--

INSERT INTO `tourism_types` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'Harike Wildlife Sanctuary', '2021-05-12 17:24:53', '2021-05-12 17:24:57'),
(2, 'Ropar Conservation Reserve', '2021-05-12 17:25:01', '2021-05-12 17:25:05'),
(3, 'Nangal Wildlife Sanctuary', '2021-05-12 17:25:34', '2021-05-12 17:25:38'),
(4, 'Keshopur-miani community Reserve', '2021-05-12 17:25:43', '2021-05-12 17:25:46'),
(5, 'Ranjit Sagar Conservation Reserve', '2021-05-12 17:26:11', '2021-05-12 17:26:14'),
(6, 'Siswan Community Reserve', '2021-05-12 17:26:18', '2021-05-12 17:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `upcoming_event_detail`
--

CREATE TABLE `upcoming_event_detail` (
  `id` int NOT NULL,
  `world_id` int NOT NULL,
  `world_day_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non-deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `upcoming_event_detail`
--

INSERT INTO `upcoming_event_detail` (`id`, `world_id`, `world_day_name`, `image`, `heading`, `date`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Forest day', '/eventforest_image/forestday-event.jpg', 'International Day of Forests', '12 March', '2021-06-04 12:02:29', '2021-06-04 12:05:40', '0'),
(2, 5, 'World Wildlife', '/eventforest_image/worldwildevent.jpg', 'World Wildlife Day', '3 March', '2021-06-04 12:03:19', '2021-06-04 12:06:00', '0'),
(3, 2, 'World Wetland', '/eventforest_image/tourism1.jpg', 'World Wetlands Day', '2 February', '2021-06-04 12:03:57', '2021-06-04 12:03:57', '0'),
(4, 3, 'World Migratory Bird Day', '/eventforest_image/migratorybirddayevent.jpg', 'World Migratory Bird Day', '12 May', '2021-06-04 12:04:44', '2021-06-04 12:06:39', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `session_id`, `created_at`, `updated_at`) VALUES
(1, 'Supeadmin', 'admin@admin.com', '$2y$10$TQM/oujbmBh/ZU/iGUiwgeYr22/H3i6ZIQ9rYP4mivByoJ9bVNEde', '1', 'LBwjkog2uMfad70i7tCoejVzuBkAkmbcNGmQaaKV', '0000-00-00 00:00:00', '2021-04-09 05:50:14'),
(2, 'wildlife', 'punjab@wildlife.com', '$2y$10$YveubJbyUglnQ/R0/JkzJuL9cQs0HVXxqbM4rEMIY4IyNvn1wQvq2', '1', 'Iv1bL8qwxgYjffqsXZnA67n4NmaciZZ5gk7z1GVb', '2021-09-10 18:21:48', '2021-04-09 05:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `type` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_title`, `video_link`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BIRD FEST', 'v89Y71mZn5U', 10001, '2022-02-09 06:25:53', '2022-02-09 06:25:53', NULL),
(2, 'chattbir', 'GcRKREorGSc', 1, '2022-04-22 06:28:18', '2022-04-22 06:28:18', NULL),
(3, 'chattbir', 'yWeasduvdgc', 1, '2022-04-25 07:16:01', '2022-04-25 07:16:35', '2022-04-25 07:16:35'),
(4, 'Wildlife Santuary', 'yWeasduvdgc', 1, '2022-05-02 13:09:59', '2022-05-02 13:12:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int NOT NULL,
  `date` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(1, '2022-01-10 00:00:00', '223.178.212.218', '2022-01-10 07:17:12', '2022-01-10 07:17:12'),
(2, '2022-01-10 00:00:00', '66.249.74.93', '2022-01-10 07:48:04', '2022-01-10 07:48:04'),
(3, '2022-01-10 00:00:00', '47.15.0.10', '2022-01-10 08:32:16', '2022-01-10 08:32:16'),
(4, '2022-01-10 00:00:00', '66.249.74.89', '2022-01-10 08:33:05', '2022-01-10 08:33:05'),
(5, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 09:18:50', '2022-01-10 09:18:50'),
(6, '2022-01-10 00:00:00', '223.178.212.218', '2022-01-10 09:28:19', '2022-01-10 09:28:19'),
(7, '2022-01-10 00:00:00', '66.249.74.93', '2022-01-10 10:01:13', '2022-01-10 10:01:13'),
(8, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 10:42:50', '2022-01-10 10:42:50'),
(9, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 11:01:39', '2022-01-10 11:01:39'),
(10, '2022-01-10 00:00:00', '117.239.5.129', '2022-01-10 11:14:57', '2022-01-10 11:14:57'),
(11, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 11:15:01', '2022-01-10 11:15:01'),
(12, '2022-01-10 00:00:00', '223.178.212.218', '2022-01-10 11:16:39', '2022-01-10 11:16:39'),
(13, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 11:26:31', '2022-01-10 11:26:31'),
(14, '2022-01-10 00:00:00', '66.249.68.59', '2022-01-10 11:31:13', '2022-01-10 11:31:13'),
(15, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 11:49:14', '2022-01-10 11:49:14'),
(16, '2022-01-10 00:00:00', '52.114.32.212', '2022-01-10 11:50:55', '2022-01-10 11:50:55'),
(17, '2022-01-10 00:00:00', '66.249.68.61', '2022-01-10 12:16:11', '2022-01-10 12:16:11'),
(18, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 12:25:13', '2022-01-10 12:25:13'),
(19, '2022-01-10 00:00:00', '52.114.14.71', '2022-01-10 12:34:33', '2022-01-10 12:34:33'),
(20, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 12:36:43', '2022-01-10 12:36:43'),
(21, '2022-01-10 00:00:00', '223.178.212.218', '2022-01-10 12:41:13', '2022-01-10 12:41:13'),
(22, '2022-01-10 00:00:00', '47.15.0.10', '2022-01-10 12:43:48', '2022-01-10 12:43:48'),
(23, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 12:49:57', '2022-01-10 12:49:57'),
(24, '2022-01-10 00:00:00', '66.249.68.61', '2022-01-10 13:01:11', '2022-01-10 13:01:11'),
(25, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 13:06:00', '2022-01-10 13:06:00'),
(26, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 13:17:20', '2022-01-10 13:17:20'),
(27, '2022-01-10 00:00:00', '223.178.212.218', '2022-01-10 13:23:35', '2022-01-10 13:23:35'),
(28, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 13:30:29', '2022-01-10 13:30:29'),
(29, '2022-01-10 00:00:00', '66.249.68.61', '2022-01-10 13:46:11', '2022-01-10 13:46:11'),
(30, '2022-01-10 00:00:00', '27.255.190.57', '2022-01-10 13:52:32', '2022-01-10 13:52:32'),
(31, '2022-01-10 00:00:00', '66.249.68.59', '2022-01-10 14:31:11', '2022-01-10 14:31:11'),
(32, '2022-01-10 00:00:00', '66.249.68.57', '2022-01-10 21:28:20', '2022-01-10 21:28:20'),
(33, '2022-01-10 00:00:00', '66.249.68.61', '2022-01-10 22:41:22', '2022-01-10 22:41:22'),
(34, '2022-01-11 00:00:00', '66.249.68.61', '2022-01-11 03:54:16', '2022-01-11 03:54:16'),
(35, '2022-01-11 00:00:00', '124.253.120.235', '2022-01-11 04:34:00', '2022-01-11 04:34:00'),
(36, '2022-01-11 00:00:00', '103.41.44.227', '2022-01-11 04:49:17', '2022-01-11 04:49:17'),
(37, '2022-01-11 00:00:00', '64.233.173.6', '2022-01-11 04:49:48', '2022-01-11 04:49:48'),
(38, '2022-01-11 00:00:00', '64.233.173.9', '2022-01-11 04:49:49', '2022-01-11 04:49:49'),
(39, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 05:24:16', '2022-01-11 05:24:16'),
(40, '2022-01-11 00:00:00', '124.253.120.235', '2022-01-11 06:05:37', '2022-01-11 06:05:37'),
(41, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 06:09:16', '2022-01-11 06:09:16'),
(42, '2022-01-11 00:00:00', '124.253.120.235', '2022-01-11 06:18:11', '2022-01-11 06:18:11'),
(43, '2022-01-11 00:00:00', '124.253.120.235', '2022-01-11 06:45:02', '2022-01-11 06:45:02'),
(44, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 08:26:21', '2022-01-11 08:26:21'),
(45, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 08:51:03', '2022-01-11 08:51:03'),
(46, '2022-01-11 00:00:00', '203.115.91.7', '2022-01-11 09:46:26', '2022-01-11 09:46:26'),
(47, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 10:51:06', '2022-01-11 10:51:06'),
(48, '2022-01-11 00:00:00', '49.36.219.5', '2022-01-11 11:03:50', '2022-01-11 11:03:50'),
(49, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 11:06:03', '2022-01-11 11:06:03'),
(50, '2022-01-11 00:00:00', '117.223.230.150', '2022-01-11 11:19:13', '2022-01-11 11:19:13'),
(51, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 11:21:03', '2022-01-11 11:21:03'),
(52, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 11:36:03', '2022-01-11 11:36:03'),
(53, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 11:51:03', '2022-01-11 11:51:03'),
(54, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 12:06:04', '2022-01-11 12:06:04'),
(55, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 12:21:03', '2022-01-11 12:21:03'),
(56, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 12:51:03', '2022-01-11 12:51:03'),
(57, '2022-01-11 00:00:00', '157.55.39.108', '2022-01-11 13:30:54', '2022-01-11 13:30:54'),
(58, '2022-01-11 00:00:00', '124.253.120.235', '2022-01-11 14:02:16', '2022-01-11 14:02:16'),
(59, '2022-01-11 00:00:00', '203.134.196.103', '2022-01-11 14:19:59', '2022-01-11 14:19:59'),
(60, '2022-01-11 00:00:00', '52.114.32.212', '2022-01-11 14:31:10', '2022-01-11 14:31:10'),
(61, '2022-01-11 00:00:00', '66.249.68.61', '2022-01-11 14:51:06', '2022-01-11 14:51:06'),
(62, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 17:51:03', '2022-01-11 17:51:03'),
(63, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 19:21:03', '2022-01-11 19:21:03'),
(64, '2022-01-11 00:00:00', '207.46.13.80', '2022-01-11 20:43:43', '2022-01-11 20:43:43'),
(65, '2022-01-11 00:00:00', '66.249.68.61', '2022-01-11 23:09:11', '2022-01-11 23:09:11'),
(66, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 23:22:43', '2022-01-11 23:22:43'),
(67, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 23:36:11', '2022-01-11 23:36:11'),
(68, '2022-01-11 00:00:00', '66.249.68.57', '2022-01-11 23:45:11', '2022-01-11 23:45:11'),
(69, '2022-01-11 00:00:00', '66.249.68.59', '2022-01-11 23:54:13', '2022-01-11 23:54:13'),
(70, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 00:12:15', '2022-01-12 00:12:15'),
(71, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 00:30:11', '2022-01-12 00:30:11'),
(72, '2022-01-12 00:00:00', '118.191.130.148', '2022-01-12 00:34:16', '2022-01-12 00:34:16'),
(73, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 00:57:11', '2022-01-12 00:57:11'),
(74, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 01:06:11', '2022-01-12 01:06:11'),
(75, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 01:15:11', '2022-01-12 01:15:11'),
(76, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 01:24:11', '2022-01-12 01:24:11'),
(77, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 01:33:11', '2022-01-12 01:33:11'),
(78, '2022-01-12 00:00:00', '157.55.39.126', '2022-01-12 01:42:09', '2022-01-12 01:42:09'),
(79, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 01:51:11', '2022-01-12 01:51:11'),
(80, '2022-01-12 00:00:00', '223.178.213.51', '2022-01-12 01:52:48', '2022-01-12 01:52:48'),
(81, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 02:00:11', '2022-01-12 02:00:11'),
(82, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 02:09:11', '2022-01-12 02:09:11'),
(83, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 02:36:13', '2022-01-12 02:36:13'),
(84, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 03:03:11', '2022-01-12 03:03:11'),
(85, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 03:21:11', '2022-01-12 03:21:11'),
(86, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 03:30:11', '2022-01-12 03:30:11'),
(87, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 03:39:11', '2022-01-12 03:39:11'),
(88, '2022-01-12 00:00:00', '117.220.18.53', '2022-01-12 03:44:47', '2022-01-12 03:44:47'),
(89, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 03:57:11', '2022-01-12 03:57:11'),
(90, '2022-01-12 00:00:00', '207.46.13.31', '2022-01-12 04:05:56', '2022-01-12 04:05:56'),
(91, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 04:06:11', '2022-01-12 04:06:11'),
(92, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 04:15:11', '2022-01-12 04:15:11'),
(93, '2022-01-12 00:00:00', '203.134.196.103', '2022-01-12 04:55:17', '2022-01-12 04:55:17'),
(94, '2022-01-12 00:00:00', '51.222.253.1', '2022-01-12 05:31:53', '2022-01-12 05:31:53'),
(95, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 05:52:13', '2022-01-12 05:52:13'),
(96, '2022-01-12 00:00:00', '10.43.252.152', '2022-01-12 05:52:58', '2022-01-12 05:52:58'),
(97, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 05:55:12', '2022-01-12 05:55:12'),
(98, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 06:17:28', '2022-01-12 06:17:28'),
(99, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 06:34:00', '2022-01-12 06:34:00'),
(100, '2022-01-12 00:00:00', '203.134.196.103', '2022-01-12 06:35:32', '2022-01-12 06:35:32'),
(101, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 06:47:08', '2022-01-12 06:47:08'),
(102, '2022-01-12 00:00:00', '51.222.253.9', '2022-01-12 06:48:54', '2022-01-12 06:48:54'),
(103, '2022-01-12 00:00:00', '203.134.196.103', '2022-01-12 06:50:59', '2022-01-12 06:50:59'),
(104, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 07:00:02', '2022-01-12 07:00:02'),
(105, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 07:15:06', '2022-01-12 07:15:06'),
(106, '2022-01-12 00:00:00', '157.55.39.188', '2022-01-12 07:26:21', '2022-01-12 07:26:21'),
(107, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 07:26:49', '2022-01-12 07:26:49'),
(108, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 07:38:53', '2022-01-12 07:38:53'),
(109, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 07:52:55', '2022-01-12 07:52:55'),
(110, '2022-01-12 00:00:00', '51.222.253.15', '2022-01-12 07:59:51', '2022-01-12 07:59:51'),
(111, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 08:23:29', '2022-01-12 08:23:29'),
(112, '2022-01-12 00:00:00', '203.134.196.103', '2022-01-12 08:23:43', '2022-01-12 08:23:43'),
(113, '2022-01-12 00:00:00', '203.134.196.103', '2022-01-12 08:36:26', '2022-01-12 08:36:26'),
(114, '2022-01-12 00:00:00', '103.118.160.5', '2022-01-12 08:42:05', '2022-01-12 08:42:05'),
(115, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 09:04:11', '2022-01-12 09:04:11'),
(116, '2022-01-12 00:00:00', '203.134.196.103', '2022-01-12 09:28:27', '2022-01-12 09:28:27'),
(117, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 09:45:00', '2022-01-12 09:45:00'),
(118, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 10:10:20', '2022-01-12 10:10:20'),
(119, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 10:18:53', '2022-01-12 10:18:53'),
(120, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 10:26:42', '2022-01-12 10:26:42'),
(121, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 10:47:16', '2022-01-12 10:47:16'),
(122, '2022-01-12 00:00:00', '157.39.1.77', '2022-01-12 10:50:33', '2022-01-12 10:50:33'),
(123, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 10:51:14', '2022-01-12 10:51:14'),
(124, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 10:55:20', '2022-01-12 10:55:20'),
(125, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 11:11:42', '2022-01-12 11:11:42'),
(126, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 11:19:53', '2022-01-12 11:19:53'),
(127, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 11:36:14', '2022-01-12 11:36:14'),
(128, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 11:36:15', '2022-01-12 11:36:15'),
(129, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 11:57:58', '2022-01-12 11:57:58'),
(130, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 12:08:30', '2022-01-12 12:08:30'),
(131, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 12:19:04', '2022-01-12 12:19:04'),
(132, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 12:29:40', '2022-01-12 12:29:40'),
(133, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 12:40:15', '2022-01-12 12:40:15'),
(134, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 12:45:33', '2022-01-12 12:45:33'),
(135, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 12:56:08', '2022-01-12 12:56:08'),
(136, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 13:06:43', '2022-01-12 13:06:43'),
(137, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 13:27:54', '2022-01-12 13:27:54'),
(138, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 13:38:29', '2022-01-12 13:38:29'),
(139, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 13:54:23', '2022-01-12 13:54:23'),
(140, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 13:59:40', '2022-01-12 13:59:40'),
(141, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 14:15:32', '2022-01-12 14:15:32'),
(142, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 14:20:50', '2022-01-12 14:20:50'),
(143, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 14:31:25', '2022-01-12 14:31:25'),
(144, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 14:42:19', '2022-01-12 14:42:19'),
(145, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 14:57:54', '2022-01-12 14:57:54'),
(146, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 15:03:11', '2022-01-12 15:03:11'),
(147, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 15:19:04', '2022-01-12 15:19:04'),
(148, '2022-01-12 00:00:00', '205.253.125.161', '2022-01-12 15:28:27', '2022-01-12 15:28:27'),
(149, '2022-01-12 00:00:00', '205.253.121.149', '2022-01-12 15:29:38', '2022-01-12 15:29:38'),
(150, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 15:29:39', '2022-01-12 15:29:39'),
(151, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 15:45:33', '2022-01-12 15:45:33'),
(152, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 16:01:26', '2022-01-12 16:01:26'),
(153, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 16:17:18', '2022-01-12 16:17:18'),
(154, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 16:43:46', '2022-01-12 16:43:46'),
(155, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 16:49:04', '2022-01-12 16:49:04'),
(156, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 17:18:11', '2022-01-12 17:18:11'),
(157, '2022-01-12 00:00:00', '66.249.68.59', '2022-01-12 17:26:08', '2022-01-12 17:26:08'),
(158, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 17:31:25', '2022-01-12 17:31:25'),
(159, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 17:36:43', '2022-01-12 17:36:43'),
(160, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 18:11:18', '2022-01-12 18:11:18'),
(161, '2022-01-12 00:00:00', '223.190.84.78', '2022-01-12 18:51:12', '2022-01-12 18:51:12'),
(162, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 21:54:30', '2022-01-12 21:54:30'),
(163, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 22:14:40', '2022-01-12 22:14:40'),
(164, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 22:37:27', '2022-01-12 22:37:27'),
(165, '2022-01-12 00:00:00', '66.249.68.61', '2022-01-12 22:48:27', '2022-01-12 22:48:27'),
(166, '2022-01-12 00:00:00', '66.249.68.57', '2022-01-12 23:10:54', '2022-01-12 23:10:54'),
(167, '2022-01-12 00:00:00', '207.46.13.31', '2022-01-12 23:28:15', '2022-01-12 23:28:15'),
(168, '2022-01-13 00:00:00', '207.46.13.80', '2022-01-13 00:01:10', '2022-01-13 00:01:10'),
(169, '2022-01-13 00:00:00', '66.249.68.57', '2022-01-13 00:10:31', '2022-01-13 00:10:31'),
(170, '2022-01-13 00:00:00', '157.55.39.108', '2022-01-13 00:13:46', '2022-01-13 00:13:46'),
(171, '2022-01-13 00:00:00', '66.249.68.57', '2022-01-13 00:55:29', '2022-01-13 00:55:29'),
(172, '2022-01-13 00:00:00', '51.222.253.5', '2022-01-13 04:00:19', '2022-01-13 04:00:19'),
(173, '2022-01-13 00:00:00', '51.222.253.18', '2022-01-13 04:00:21', '2022-01-13 04:00:21'),
(174, '2022-01-13 00:00:00', '54.36.148.137', '2022-01-13 06:17:25', '2022-01-13 06:17:25'),
(175, '2022-01-13 00:00:00', '103.159.184.173', '2022-01-13 06:30:10', '2022-01-13 06:30:10'),
(176, '2022-01-13 00:00:00', '66.249.68.57', '2022-01-13 07:26:14', '2022-01-13 07:26:14'),
(177, '2022-01-13 00:00:00', '66.249.68.57', '2022-01-13 08:38:06', '2022-01-13 08:38:06'),
(178, '2022-01-13 00:00:00', '66.249.68.61', '2022-01-13 09:23:07', '2022-01-13 09:23:07'),
(179, '2022-01-13 00:00:00', '66.249.68.61', '2022-01-13 10:08:05', '2022-01-13 10:08:05'),
(180, '2022-01-13 00:00:00', '66.249.68.57', '2022-01-13 10:52:59', '2022-01-13 10:52:59'),
(181, '2022-01-13 00:00:00', '49.36.207.248', '2022-01-13 11:14:30', '2022-01-13 11:14:30'),
(182, '2022-01-13 00:00:00', '66.249.68.59', '2022-01-13 11:38:05', '2022-01-13 11:38:05'),
(183, '2022-01-13 00:00:00', '207.46.13.133', '2022-01-13 11:45:55', '2022-01-13 11:45:55'),
(184, '2022-01-13 00:00:00', '66.249.68.61', '2022-01-13 12:45:28', '2022-01-13 12:45:28'),
(185, '2022-01-13 00:00:00', '207.46.13.80', '2022-01-13 15:53:35', '2022-01-13 15:53:35'),
(186, '2022-01-13 00:00:00', '66.249.68.57', '2022-01-13 16:15:28', '2022-01-13 16:15:28'),
(187, '2022-01-13 00:00:00', '223.185.18.206', '2022-01-13 17:13:20', '2022-01-13 17:13:20'),
(188, '2022-01-13 00:00:00', '157.55.39.188', '2022-01-13 18:19:31', '2022-01-13 18:19:31'),
(189, '2022-01-13 00:00:00', '54.36.148.137', '2022-01-13 18:28:42', '2022-01-13 18:28:42'),
(190, '2022-01-13 00:00:00', '207.46.13.31', '2022-01-13 23:24:22', '2022-01-13 23:24:22'),
(191, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 00:28:17', '2022-01-14 00:28:17'),
(192, '2022-01-14 00:00:00', '162.215.209.208', '2022-01-14 03:24:51', '2022-01-14 03:24:51'),
(193, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 05:07:10', '2022-01-14 05:07:10'),
(194, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 05:31:50', '2022-01-14 05:31:50'),
(195, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 05:43:10', '2022-01-14 05:43:10'),
(196, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 05:45:45', '2022-01-14 05:45:45'),
(197, '2022-01-14 00:00:00', '124.253.74.141', '2022-01-14 06:08:06', '2022-01-14 06:08:06'),
(198, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 06:23:52', '2022-01-14 06:23:52'),
(199, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 06:35:02', '2022-01-14 06:35:02'),
(200, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 06:54:39', '2022-01-14 06:54:39'),
(201, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 07:08:56', '2022-01-14 07:08:56'),
(202, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 07:20:11', '2022-01-14 07:20:11'),
(203, '2022-01-14 00:00:00', '124.253.74.141', '2022-01-14 07:31:40', '2022-01-14 07:31:40'),
(204, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 07:33:11', '2022-01-14 07:33:11'),
(205, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 07:33:54', '2022-01-14 07:33:54'),
(206, '2022-01-14 00:00:00', '157.39.150.160', '2022-01-14 07:45:32', '2022-01-14 07:45:32'),
(207, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 08:18:52', '2022-01-14 08:18:52'),
(208, '2022-01-14 00:00:00', '124.253.61.88', '2022-01-14 09:03:32', '2022-01-14 09:03:32'),
(209, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 09:03:53', '2022-01-14 09:03:53'),
(210, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 09:48:54', '2022-01-14 09:48:54'),
(211, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 11:41:22', '2022-01-14 11:41:22'),
(212, '2022-01-14 00:00:00', '124.253.74.141', '2022-01-14 12:28:41', '2022-01-14 12:28:41'),
(213, '2022-01-14 00:00:00', '223.187.96.27', '2022-01-14 12:32:13', '2022-01-14 12:32:13'),
(214, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 12:33:16', '2022-01-14 12:33:16'),
(215, '2022-01-14 00:00:00', '124.253.74.141', '2022-01-14 12:43:15', '2022-01-14 12:43:15'),
(216, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 12:55:46', '2022-01-14 12:55:46'),
(217, '2022-01-14 00:00:00', '66.249.64.61', '2022-01-14 13:45:09', '2022-01-14 13:45:09'),
(218, '2022-01-14 00:00:00', '66.249.68.93', '2022-01-14 14:56:22', '2022-01-14 14:56:22'),
(219, '2022-01-14 00:00:00', '49.36.230.177', '2022-01-14 14:59:17', '2022-01-14 14:59:17'),
(220, '2022-01-14 00:00:00', '223.178.212.155', '2022-01-14 15:00:15', '2022-01-14 15:00:15'),
(221, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 15:56:24', '2022-01-14 15:56:24'),
(222, '2022-01-14 00:00:00', '66.249.68.89', '2022-01-14 16:26:22', '2022-01-14 16:26:22'),
(223, '2022-01-14 00:00:00', '66.249.68.91', '2022-01-14 16:56:22', '2022-01-14 16:56:22'),
(224, '2022-01-14 00:00:00', '66.249.68.61', '2022-01-14 21:16:36', '2022-01-14 21:16:36'),
(225, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 22:46:36', '2022-01-14 22:46:36'),
(226, '2022-01-14 00:00:00', '66.249.68.59', '2022-01-14 23:16:35', '2022-01-14 23:16:35'),
(227, '2022-01-14 00:00:00', '66.249.68.57', '2022-01-14 23:46:36', '2022-01-14 23:46:36'),
(228, '2022-01-14 00:00:00', '207.46.13.80', '2022-01-14 23:51:23', '2022-01-14 23:51:23'),
(229, '2022-01-15 00:00:00', '142.132.139.251', '2022-01-15 00:32:28', '2022-01-15 00:32:28'),
(230, '2022-01-15 00:00:00', '66.249.68.57', '2022-01-15 01:16:35', '2022-01-15 01:16:35'),
(231, '2022-01-15 00:00:00', '66.249.68.61', '2022-01-15 06:11:22', '2022-01-15 06:11:22'),
(232, '2022-01-15 00:00:00', '66.249.68.59', '2022-01-15 07:41:24', '2022-01-15 07:41:24'),
(233, '2022-01-15 00:00:00', '106.222.118.37', '2022-01-15 10:50:04', '2022-01-15 10:50:04'),
(234, '2022-01-15 00:00:00', '66.249.68.59', '2022-01-15 11:01:17', '2022-01-15 11:01:17'),
(235, '2022-01-15 00:00:00', '66.249.68.61', '2022-01-15 12:56:23', '2022-01-15 12:56:23'),
(236, '2022-01-15 00:00:00', '66.249.68.59', '2022-01-15 13:41:22', '2022-01-15 13:41:22'),
(237, '2022-01-15 00:00:00', '66.249.68.59', '2022-01-15 14:26:22', '2022-01-15 14:26:22'),
(238, '2022-01-15 00:00:00', '66.249.68.57', '2022-01-15 15:11:24', '2022-01-15 15:11:24'),
(239, '2022-01-15 00:00:00', '66.249.68.61', '2022-01-15 15:56:22', '2022-01-15 15:56:22'),
(240, '2022-01-15 00:00:00', '66.249.68.61', '2022-01-15 16:41:22', '2022-01-15 16:41:22'),
(241, '2022-01-15 00:00:00', '66.249.68.61', '2022-01-15 17:26:22', '2022-01-15 17:26:22'),
(242, '2022-01-15 00:00:00', '66.249.68.57', '2022-01-15 20:05:43', '2022-01-15 20:05:43'),
(243, '2022-01-15 00:00:00', '66.249.68.57', '2022-01-15 20:50:44', '2022-01-15 20:50:44'),
(244, '2022-01-15 00:00:00', '66.249.68.57', '2022-01-15 21:13:13', '2022-01-15 21:13:13'),
(245, '2022-01-15 00:00:00', '54.36.148.137', '2022-01-15 22:24:00', '2022-01-15 22:24:00'),
(246, '2022-01-15 00:00:00', '66.249.68.59', '2022-01-15 22:41:22', '2022-01-15 22:41:22'),
(247, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 00:11:22', '2022-01-16 00:11:22'),
(248, '2022-01-16 00:00:00', '207.46.13.31', '2022-01-16 00:35:23', '2022-01-16 00:35:23'),
(249, '2022-01-16 00:00:00', '207.46.13.31', '2022-01-16 01:19:18', '2022-01-16 01:19:18'),
(250, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 01:41:21', '2022-01-16 01:41:21'),
(251, '2022-01-16 00:00:00', '114.119.142.100', '2022-01-16 04:21:14', '2022-01-16 04:21:14'),
(252, '2022-01-16 00:00:00', '207.46.13.194', '2022-01-16 04:39:20', '2022-01-16 04:39:20'),
(253, '2022-01-16 00:00:00', '66.249.68.59', '2022-01-16 06:07:08', '2022-01-16 06:07:08'),
(254, '2022-01-16 00:00:00', '66.249.68.59', '2022-01-16 06:52:07', '2022-01-16 06:52:07'),
(255, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 07:07:07', '2022-01-16 07:07:07'),
(256, '2022-01-16 00:00:00', '66.249.68.59', '2022-01-16 07:22:07', '2022-01-16 07:22:07'),
(257, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 07:37:07', '2022-01-16 07:37:07'),
(258, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 10:11:41', '2022-01-16 10:11:41'),
(259, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 10:41:41', '2022-01-16 10:41:41'),
(260, '2022-01-16 00:00:00', '66.249.68.57', '2022-01-16 10:56:41', '2022-01-16 10:56:41'),
(261, '2022-01-16 00:00:00', '66.249.68.57', '2022-01-16 11:11:43', '2022-01-16 11:11:43'),
(262, '2022-01-16 00:00:00', '157.55.39.0', '2022-01-16 11:47:33', '2022-01-16 11:47:33'),
(263, '2022-01-16 00:00:00', '157.55.39.126', '2022-01-16 13:08:12', '2022-01-16 13:08:12'),
(264, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 15:11:43', '2022-01-16 15:11:43'),
(265, '2022-01-16 00:00:00', '207.46.13.31', '2022-01-16 15:14:36', '2022-01-16 15:14:36'),
(266, '2022-01-16 00:00:00', '66.249.68.57', '2022-01-16 15:57:14', '2022-01-16 15:57:14'),
(267, '2022-01-16 00:00:00', '66.249.68.59', '2022-01-16 16:41:42', '2022-01-16 16:41:42'),
(268, '2022-01-16 00:00:00', '66.249.68.61', '2022-01-16 18:11:40', '2022-01-16 18:11:40'),
(269, '2022-01-16 00:00:00', '66.249.68.59', '2022-01-16 21:08:08', '2022-01-16 21:08:08'),
(270, '2022-01-16 00:00:00', '66.249.68.59', '2022-01-16 21:53:08', '2022-01-16 21:53:08'),
(271, '2022-01-16 00:00:00', '66.249.68.57', '2022-01-16 22:41:40', '2022-01-16 22:41:40'),
(272, '2022-01-17 00:00:00', '66.249.68.57', '2022-01-17 00:11:40', '2022-01-17 00:11:40'),
(273, '2022-01-17 00:00:00', '66.249.68.61', '2022-01-17 01:41:42', '2022-01-17 01:41:42'),
(274, '2022-01-17 00:00:00', '66.249.68.59', '2022-01-17 03:34:11', '2022-01-17 03:34:11'),
(275, '2022-01-17 00:00:00', '66.249.68.57', '2022-01-17 03:56:40', '2022-01-17 03:56:40'),
(276, '2022-01-17 00:00:00', '66.249.68.61', '2022-01-17 04:41:40', '2022-01-17 04:41:40'),
(277, '2022-01-17 00:00:00', '66.249.68.59', '2022-01-17 06:12:02', '2022-01-17 06:12:02'),
(278, '2022-01-17 00:00:00', '66.249.68.59', '2022-01-17 07:41:40', '2022-01-17 07:41:40'),
(279, '2022-01-17 00:00:00', '157.55.39.0', '2022-01-17 08:13:39', '2022-01-17 08:13:39'),
(280, '2022-01-17 00:00:00', '124.253.82.245', '2022-01-17 10:37:31', '2022-01-17 10:37:31'),
(281, '2022-01-17 00:00:00', '203.115.91.247', '2022-01-17 12:27:51', '2022-01-17 12:27:51'),
(282, '2022-01-17 00:00:00', '66.249.68.61', '2022-01-17 14:28:11', '2022-01-17 14:28:11'),
(283, '2022-01-17 00:00:00', '66.249.68.61', '2022-01-17 15:08:13', '2022-01-17 15:08:13'),
(284, '2022-01-17 00:00:00', '66.249.68.57', '2022-01-17 15:30:43', '2022-01-17 15:30:43'),
(285, '2022-01-17 00:00:00', '66.249.68.57', '2022-01-17 15:53:13', '2022-01-17 15:53:13'),
(286, '2022-01-17 00:00:00', '66.249.68.59', '2022-01-17 16:15:43', '2022-01-17 16:15:43'),
(287, '2022-01-17 00:00:00', '162.215.209.208', '2022-01-17 16:21:10', '2022-01-17 16:21:10'),
(288, '2022-01-17 00:00:00', '66.249.68.61', '2022-01-17 16:38:13', '2022-01-17 16:38:13'),
(289, '2022-01-17 00:00:00', '157.55.39.0', '2022-01-17 20:10:17', '2022-01-17 20:10:17'),
(290, '2022-01-17 00:00:00', '114.119.150.16', '2022-01-17 21:49:37', '2022-01-17 21:49:37'),
(291, '2022-01-17 00:00:00', '66.249.68.57', '2022-01-17 22:44:13', '2022-01-17 22:44:13'),
(292, '2022-01-18 00:00:00', '66.249.68.57', '2022-01-18 00:14:12', '2022-01-18 00:14:12'),
(293, '2022-01-18 00:00:00', '66.249.68.57', '2022-01-18 01:44:12', '2022-01-18 01:44:12'),
(294, '2022-01-18 00:00:00', '66.249.68.59', '2022-01-18 03:10:32', '2022-01-18 03:10:32'),
(295, '2022-01-18 00:00:00', '66.249.68.61', '2022-01-18 03:26:52', '2022-01-18 03:26:52'),
(296, '2022-01-18 00:00:00', '66.249.68.57', '2022-01-18 03:43:11', '2022-01-18 03:43:11'),
(297, '2022-01-18 00:00:00', '66.249.68.57', '2022-01-18 03:59:31', '2022-01-18 03:59:31'),
(298, '2022-01-18 00:00:00', '207.46.13.197', '2022-01-18 04:50:13', '2022-01-18 04:50:13'),
(299, '2022-01-18 00:00:00', '66.249.68.59', '2022-01-18 05:00:09', '2022-01-18 05:00:09'),
(300, '2022-01-18 00:00:00', '40.77.167.6', '2022-01-18 05:16:11', '2022-01-18 05:16:11'),
(301, '2022-01-18 00:00:00', '66.249.68.57', '2022-01-18 05:32:48', '2022-01-18 05:32:48'),
(302, '2022-01-18 00:00:00', '157.55.39.0', '2022-01-18 06:03:09', '2022-01-18 06:03:09'),
(303, '2022-01-18 00:00:00', '66.249.68.61', '2022-01-18 06:05:28', '2022-01-18 06:05:28'),
(304, '2022-01-18 00:00:00', '103.118.160.5', '2022-01-18 06:19:14', '2022-01-18 06:19:14'),
(305, '2022-01-18 00:00:00', '66.249.68.61', '2022-01-18 06:38:09', '2022-01-18 06:38:09'),
(306, '2022-01-18 00:00:00', '10.43.252.152', '2022-01-18 06:39:36', '2022-01-18 06:39:36'),
(307, '2022-01-18 00:00:00', '10.43.252.152', '2022-01-18 06:57:14', '2022-01-18 06:57:14'),
(308, '2022-01-18 00:00:00', '10.43.252.152', '2022-01-18 07:08:14', '2022-01-18 07:08:14'),
(309, '2022-01-18 00:00:00', '66.249.68.61', '2022-01-18 08:16:06', '2022-01-18 08:16:06'),
(310, '2022-01-18 00:00:00', '66.249.68.61', '2022-01-18 08:48:46', '2022-01-18 08:48:46'),
(311, '2022-01-18 00:00:00', '10.43.252.152', '2022-01-18 08:57:28', '2022-01-18 08:57:28'),
(312, '2022-01-18 00:00:00', '66.249.68.61', '2022-01-18 10:18:45', '2022-01-18 10:18:45'),
(313, '2022-01-18 00:00:00', '117.222.221.34', '2022-01-18 10:36:46', '2022-01-18 10:36:46'),
(314, '2022-01-18 00:00:00', '10.43.252.152', '2022-01-18 11:19:46', '2022-01-18 11:19:46'),
(315, '2022-01-18 00:00:00', '117.222.221.34', '2022-01-18 11:19:52', '2022-01-18 11:19:52'),
(316, '2022-01-18 00:00:00', '117.222.221.34', '2022-01-18 11:31:25', '2022-01-18 11:31:25'),
(317, '2022-01-18 00:00:00', '124.253.83.53', '2022-01-18 11:41:42', '2022-01-18 11:41:42'),
(318, '2022-01-18 00:00:00', '124.253.83.53', '2022-01-18 12:23:53', '2022-01-18 12:23:53'),
(319, '2022-01-18 00:00:00', '117.222.221.34', '2022-01-18 13:10:29', '2022-01-18 13:10:29'),
(320, '2022-01-18 00:00:00', '66.249.66.71', '2022-01-18 15:04:39', '2022-01-18 15:04:39'),
(321, '2022-01-18 00:00:00', '66.249.66.27', '2022-01-18 16:34:39', '2022-01-18 16:34:39'),
(322, '2022-01-18 00:00:00', '66.249.66.73', '2022-01-18 16:52:52', '2022-01-18 16:52:52'),
(323, '2022-01-18 00:00:00', '66.249.66.71', '2022-01-18 17:19:39', '2022-01-18 17:19:39'),
(324, '2022-01-18 00:00:00', '157.55.39.0', '2022-01-18 18:21:39', '2022-01-18 18:21:39'),
(325, '2022-01-18 00:00:00', '66.249.66.27', '2022-01-18 18:49:39', '2022-01-18 18:49:39'),
(326, '2022-01-18 00:00:00', '66.249.66.27', '2022-01-18 22:44:32', '2022-01-18 22:44:32'),
(327, '2022-01-18 00:00:00', '66.249.66.133', '2022-01-18 22:44:36', '2022-01-18 22:44:36'),
(328, '2022-01-18 00:00:00', '66.249.66.29', '2022-01-18 23:14:33', '2022-01-18 23:14:33'),
(329, '2022-01-18 00:00:00', '66.249.66.25', '2022-01-18 23:44:34', '2022-01-18 23:44:34'),
(330, '2022-01-19 00:00:00', '66.249.66.135', '2022-01-19 00:14:32', '2022-01-19 00:14:32'),
(331, '2022-01-19 00:00:00', '66.249.66.69', '2022-01-19 00:44:32', '2022-01-19 00:44:32'),
(332, '2022-01-19 00:00:00', '117.222.221.34', '2022-01-19 06:22:22', '2022-01-19 06:22:22'),
(333, '2022-01-19 00:00:00', '124.253.83.53', '2022-01-19 06:54:33', '2022-01-19 06:54:33'),
(334, '2022-01-19 00:00:00', '66.249.66.137', '2022-01-19 07:07:48', '2022-01-19 07:07:48'),
(335, '2022-01-19 00:00:00', '124.253.83.53', '2022-01-19 07:38:52', '2022-01-19 07:38:52'),
(336, '2022-01-19 00:00:00', '66.249.66.69', '2022-01-19 08:15:18', '2022-01-19 08:15:18'),
(337, '2022-01-19 00:00:00', '66.249.66.73', '2022-01-19 08:37:45', '2022-01-19 08:37:45'),
(338, '2022-01-19 00:00:00', '66.249.66.69', '2022-01-19 09:00:15', '2022-01-19 09:00:15'),
(339, '2022-01-19 00:00:00', '66.249.66.215', '2022-01-19 12:14:32', '2022-01-19 12:14:32'),
(340, '2022-01-19 00:00:00', '66.249.66.25', '2022-01-19 12:14:33', '2022-01-19 12:14:33'),
(341, '2022-01-19 00:00:00', '66.249.66.27', '2022-01-19 12:23:38', '2022-01-19 12:23:38'),
(342, '2022-01-19 00:00:00', '66.249.66.217', '2022-01-19 12:32:47', '2022-01-19 12:32:47'),
(343, '2022-01-19 00:00:00', '66.249.75.155', '2022-01-19 12:42:05', '2022-01-19 12:42:05'),
(344, '2022-01-19 00:00:00', '66.249.75.153', '2022-01-19 13:00:14', '2022-01-19 13:00:14'),
(345, '2022-01-19 00:00:00', '66.249.75.155', '2022-01-19 13:09:23', '2022-01-19 13:09:23'),
(346, '2022-01-19 00:00:00', '66.249.75.155', '2022-01-19 13:27:41', '2022-01-19 13:27:41'),
(347, '2022-01-19 00:00:00', '66.249.75.157', '2022-01-19 13:45:59', '2022-01-19 13:45:59'),
(348, '2022-01-19 00:00:00', '66.249.75.221', '2022-01-19 14:25:46', '2022-01-19 14:25:46'),
(349, '2022-01-19 00:00:00', '66.249.75.155', '2022-01-19 15:05:31', '2022-01-19 15:05:31'),
(350, '2022-01-19 00:00:00', '66.249.75.153', '2022-01-19 15:45:16', '2022-01-19 15:45:16'),
(351, '2022-01-19 00:00:00', '20.191.45.212', '2022-01-19 16:37:55', '2022-01-19 16:37:55'),
(352, '2022-01-19 00:00:00', '157.55.39.0', '2022-01-19 17:20:19', '2022-01-19 17:20:19'),
(353, '2022-01-19 00:00:00', '66.249.75.223', '2022-01-19 20:50:46', '2022-01-19 20:50:46'),
(354, '2022-01-19 00:00:00', '66.249.75.221', '2022-01-19 22:20:33', '2022-01-19 22:20:33'),
(355, '2022-01-19 00:00:00', '66.249.75.223', '2022-01-19 22:24:40', '2022-01-19 22:24:40'),
(356, '2022-01-19 00:00:00', '66.249.75.221', '2022-01-19 23:05:30', '2022-01-19 23:05:30'),
(357, '2022-01-19 00:00:00', '66.249.75.222', '2022-01-19 23:28:00', '2022-01-19 23:28:00'),
(358, '2022-01-19 00:00:00', '66.249.75.223', '2022-01-19 23:50:31', '2022-01-19 23:50:31'),
(359, '2022-01-19 00:00:00', '66.249.75.222', '2022-01-19 23:54:41', '2022-01-19 23:54:41'),
(360, '2022-01-20 00:00:00', '66.249.75.222', '2022-01-20 00:13:00', '2022-01-20 00:13:00'),
(361, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 00:54:19', '2022-01-20 00:54:19'),
(362, '2022-01-20 00:00:00', '66.249.75.223', '2022-01-20 01:35:35', '2022-01-20 01:35:35'),
(363, '2022-01-20 00:00:00', '66.249.75.223', '2022-01-20 01:56:17', '2022-01-20 01:56:17'),
(364, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 02:37:32', '2022-01-20 02:37:32'),
(365, '2022-01-20 00:00:00', '66.249.75.222', '2022-01-20 02:54:39', '2022-01-20 02:54:39'),
(366, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 02:58:11', '2022-01-20 02:58:11'),
(367, '2022-01-20 00:00:00', '54.36.148.137', '2022-01-20 04:48:05', '2022-01-20 04:48:05'),
(368, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 05:21:52', '2022-01-20 05:21:52'),
(369, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 05:51:52', '2022-01-20 05:51:52'),
(370, '2022-01-20 00:00:00', '124.253.188.21', '2022-01-20 06:12:01', '2022-01-20 06:12:01'),
(371, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 06:21:53', '2022-01-20 06:21:53'),
(372, '2022-01-20 00:00:00', '66.249.75.223', '2022-01-20 06:21:54', '2022-01-20 06:21:54'),
(373, '2022-01-20 00:00:00', '117.239.5.129', '2022-01-20 06:27:25', '2022-01-20 06:27:25'),
(374, '2022-01-20 00:00:00', '66.249.75.223', '2022-01-20 06:58:10', '2022-01-20 06:58:10'),
(375, '2022-01-20 00:00:00', '162.215.209.208', '2022-01-20 08:39:21', '2022-01-20 08:39:21'),
(376, '2022-01-20 00:00:00', '66.249.75.153', '2022-01-20 09:28:42', '2022-01-20 09:28:42'),
(377, '2022-01-20 00:00:00', '66.249.75.157', '2022-01-20 09:33:03', '2022-01-20 09:33:03'),
(378, '2022-01-20 00:00:00', '66.249.75.155', '2022-01-20 09:58:42', '2022-01-20 09:58:42'),
(379, '2022-01-20 00:00:00', '117.239.5.129', '2022-01-20 10:49:04', '2022-01-20 10:49:04'),
(380, '2022-01-20 00:00:00', '66.249.75.157', '2022-01-20 10:58:42', '2022-01-20 10:58:42'),
(381, '2022-01-20 00:00:00', '66.249.75.221', '2022-01-20 11:38:01', '2022-01-20 11:38:01'),
(382, '2022-01-20 00:00:00', '66.249.75.153', '2022-01-20 11:58:10', '2022-01-20 11:58:10'),
(383, '2022-01-20 00:00:00', '171.48.116.193', '2022-01-20 12:04:12', '2022-01-20 12:04:12'),
(384, '2022-01-20 00:00:00', '106.205.181.49', '2022-01-20 12:04:38', '2022-01-20 12:04:38'),
(385, '2022-01-20 00:00:00', '207.46.13.189', '2022-01-20 12:08:16', '2022-01-20 12:08:16'),
(386, '2022-01-20 00:00:00', '106.205.173.92', '2022-01-20 13:03:05', '2022-01-20 13:03:05'),
(387, '2022-01-20 00:00:00', '171.48.108.92', '2022-01-20 13:03:12', '2022-01-20 13:03:12'),
(388, '2022-01-20 00:00:00', '66.249.68.59', '2022-01-20 13:49:06', '2022-01-20 13:49:06'),
(389, '2022-01-20 00:00:00', '207.46.13.194', '2022-01-20 15:02:27', '2022-01-20 15:02:27'),
(390, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 15:49:06', '2022-01-20 15:49:06'),
(391, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 16:03:20', '2022-01-20 16:03:20'),
(392, '2022-01-20 00:00:00', '66.249.68.59', '2022-01-20 16:09:42', '2022-01-20 16:09:42'),
(393, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 16:19:07', '2022-01-20 16:19:07'),
(394, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 16:34:07', '2022-01-20 16:34:07'),
(395, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 16:49:05', '2022-01-20 16:49:05'),
(396, '2022-01-20 00:00:00', '162.215.209.208', '2022-01-20 16:51:16', '2022-01-20 16:51:16'),
(397, '2022-01-20 00:00:00', '207.46.13.164', '2022-01-20 16:53:33', '2022-01-20 16:53:33'),
(398, '2022-01-20 00:00:00', '66.249.68.59', '2022-01-20 16:54:42', '2022-01-20 16:54:42'),
(399, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 17:09:42', '2022-01-20 17:09:42'),
(400, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 17:19:07', '2022-01-20 17:19:07'),
(401, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 17:49:26', '2022-01-20 17:49:26'),
(402, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 18:04:29', '2022-01-20 18:04:29'),
(403, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 18:25:44', '2022-01-20 18:25:44'),
(404, '2022-01-20 00:00:00', '207.46.13.194', '2022-01-20 19:38:34', '2022-01-20 19:38:34'),
(405, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 19:55:46', '2022-01-20 19:55:46'),
(406, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 21:03:20', '2022-01-20 21:03:20'),
(407, '2022-01-20 00:00:00', '66.249.68.59', '2022-01-20 21:18:20', '2022-01-20 21:18:20'),
(408, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 21:33:20', '2022-01-20 21:33:20'),
(409, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 21:40:50', '2022-01-20 21:40:50'),
(410, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 22:10:44', '2022-01-20 22:10:44'),
(411, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 22:25:44', '2022-01-20 22:25:44'),
(412, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 22:55:44', '2022-01-20 22:55:44'),
(413, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 23:10:46', '2022-01-20 23:10:46'),
(414, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 23:25:44', '2022-01-20 23:25:44'),
(415, '2022-01-20 00:00:00', '66.249.68.61', '2022-01-20 23:28:47', '2022-01-20 23:28:47'),
(416, '2022-01-20 00:00:00', '66.249.68.57', '2022-01-20 23:58:51', '2022-01-20 23:58:51'),
(417, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 00:25:44', '2022-01-21 00:25:44'),
(418, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 00:58:47', '2022-01-21 00:58:47'),
(419, '2022-01-21 00:00:00', '66.249.68.61', '2022-01-21 01:25:44', '2022-01-21 01:25:44'),
(420, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 01:28:48', '2022-01-21 01:28:48'),
(421, '2022-01-21 00:00:00', '54.36.148.137', '2022-01-21 02:12:53', '2022-01-21 02:12:53'),
(422, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 02:25:44', '2022-01-21 02:25:44'),
(423, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 05:40:22', '2022-01-21 05:40:22'),
(424, '2022-01-21 00:00:00', '66.249.68.61', '2022-01-21 05:46:03', '2022-01-21 05:46:03'),
(425, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 05:55:48', '2022-01-21 05:55:48'),
(426, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 06:10:21', '2022-01-21 06:10:21'),
(427, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 06:25:44', '2022-01-21 06:25:44'),
(428, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 06:55:46', '2022-01-21 06:55:46'),
(429, '2022-01-21 00:00:00', '122.173.24.249', '2022-01-21 07:27:32', '2022-01-21 07:27:32'),
(430, '2022-01-21 00:00:00', '207.46.13.197', '2022-01-21 07:32:29', '2022-01-21 07:32:29'),
(431, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 07:55:45', '2022-01-21 07:55:45'),
(432, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 10:55:43', '2022-01-21 10:55:43'),
(433, '2022-01-21 00:00:00', '66.249.68.61', '2022-01-21 11:08:52', '2022-01-21 11:08:52'),
(434, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 11:13:52', '2022-01-21 11:13:52'),
(435, '2022-01-21 00:00:00', '66.249.68.61', '2022-01-21 11:28:52', '2022-01-21 11:28:52'),
(436, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 11:43:53', '2022-01-21 11:43:53'),
(437, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 11:45:43', '2022-01-21 11:45:43'),
(438, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 11:55:43', '2022-01-21 11:55:43'),
(439, '2022-01-21 00:00:00', '66.249.68.61', '2022-01-21 12:03:52', '2022-01-21 12:03:52'),
(440, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 12:03:53', '2022-01-21 12:03:53'),
(441, '2022-01-21 00:00:00', '66.249.68.57', '2022-01-21 12:13:55', '2022-01-21 12:13:55'),
(442, '2022-01-21 00:00:00', '66.249.68.59', '2022-01-21 12:23:52', '2022-01-21 12:23:52'),
(443, '2022-01-21 00:00:00', '114.119.148.0', '2022-01-21 12:36:31', '2022-01-21 12:36:31'),
(444, '2022-01-21 00:00:00', '66.249.65.189', '2022-01-21 12:43:56', '2022-01-21 12:43:56'),
(445, '2022-01-21 00:00:00', '66.249.65.189', '2022-01-21 13:03:53', '2022-01-21 13:03:53'),
(446, '2022-01-21 00:00:00', '66.249.74.25', '2022-01-21 13:41:42', '2022-01-21 13:41:42'),
(447, '2022-01-21 00:00:00', '66.249.74.27', '2022-01-21 14:19:32', '2022-01-21 14:19:32'),
(448, '2022-01-21 00:00:00', '66.249.74.25', '2022-01-21 14:57:20', '2022-01-21 14:57:20'),
(449, '2022-01-21 00:00:00', '66.249.74.25', '2022-01-21 15:11:05', '2022-01-21 15:11:05'),
(450, '2022-01-21 00:00:00', '66.249.74.25', '2022-01-21 15:57:22', '2022-01-21 15:57:22'),
(451, '2022-01-21 00:00:00', '66.249.74.29', '2022-01-21 16:57:20', '2022-01-21 16:57:20'),
(452, '2022-01-21 00:00:00', '114.119.150.168', '2022-01-21 17:02:38', '2022-01-21 17:02:38'),
(453, '2022-01-21 00:00:00', '207.46.13.164', '2022-01-21 18:10:13', '2022-01-21 18:10:13'),
(454, '2022-01-21 00:00:00', '66.249.74.29', '2022-01-21 21:34:50', '2022-01-21 21:34:50'),
(455, '2022-01-21 00:00:00', '66.249.65.61', '2022-01-21 22:04:47', '2022-01-21 22:04:47'),
(456, '2022-01-21 00:00:00', '66.249.65.57', '2022-01-21 22:04:49', '2022-01-21 22:04:49'),
(457, '2022-01-21 00:00:00', '66.249.65.61', '2022-01-21 22:34:47', '2022-01-21 22:34:47'),
(458, '2022-01-21 00:00:00', '66.249.65.61', '2022-01-21 23:03:48', '2022-01-21 23:03:48'),
(459, '2022-01-21 00:00:00', '66.249.65.57', '2022-01-21 23:18:48', '2022-01-21 23:18:48'),
(460, '2022-01-21 00:00:00', '66.249.65.61', '2022-01-21 23:26:18', '2022-01-21 23:26:18'),
(461, '2022-01-21 00:00:00', '66.249.65.59', '2022-01-21 23:33:48', '2022-01-21 23:33:48'),
(462, '2022-01-21 00:00:00', '66.249.65.57', '2022-01-21 23:41:18', '2022-01-21 23:41:18'),
(463, '2022-01-21 00:00:00', '66.249.65.59', '2022-01-21 23:48:48', '2022-01-21 23:48:48'),
(464, '2022-01-22 00:00:00', '66.249.65.57', '2022-01-22 00:03:48', '2022-01-22 00:03:48'),
(465, '2022-01-22 00:00:00', '66.249.65.61', '2022-01-22 00:18:48', '2022-01-22 00:18:48'),
(466, '2022-01-22 00:00:00', '66.249.65.57', '2022-01-22 00:33:48', '2022-01-22 00:33:48'),
(467, '2022-01-22 00:00:00', '66.249.65.61', '2022-01-22 01:02:52', '2022-01-22 01:02:52'),
(468, '2022-01-22 00:00:00', '157.55.39.56', '2022-01-22 02:02:52', '2022-01-22 02:02:52'),
(469, '2022-01-22 00:00:00', '66.249.68.59', '2022-01-22 03:14:02', '2022-01-22 03:14:02'),
(470, '2022-01-22 00:00:00', '66.249.65.61', '2022-01-22 03:29:03', '2022-01-22 03:29:03'),
(471, '2022-01-22 00:00:00', '66.249.65.61', '2022-01-22 03:45:04', '2022-01-22 03:45:04'),
(472, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 04:00:04', '2022-01-22 04:00:04'),
(473, '2022-01-22 00:00:00', '157.55.39.56', '2022-01-22 04:19:46', '2022-01-22 04:19:46'),
(474, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 04:30:05', '2022-01-22 04:30:05'),
(475, '2022-01-22 00:00:00', '157.55.39.56', '2022-01-22 05:40:24', '2022-01-22 05:40:24'),
(476, '2022-01-22 00:00:00', '101.0.55.36', '2022-01-22 06:58:07', '2022-01-22 06:58:07'),
(477, '2022-01-22 00:00:00', '157.55.39.56', '2022-01-22 07:10:13', '2022-01-22 07:10:13'),
(478, '2022-01-22 00:00:00', '157.39.155.126', '2022-01-22 08:32:10', '2022-01-22 08:32:10'),
(479, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 09:05:57', '2022-01-22 09:05:57'),
(480, '2022-01-22 00:00:00', '66.249.65.61', '2022-01-22 09:20:56', '2022-01-22 09:20:56'),
(481, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 09:35:56', '2022-01-22 09:35:56'),
(482, '2022-01-22 00:00:00', '27.255.186.65', '2022-01-22 09:39:06', '2022-01-22 09:39:06'),
(483, '2022-01-22 00:00:00', '132.154.180.139', '2022-01-22 09:39:12', '2022-01-22 09:39:12'),
(484, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 09:50:56', '2022-01-22 09:50:56'),
(485, '2022-01-22 00:00:00', '40.77.167.6', '2022-01-22 09:53:00', '2022-01-22 09:53:00'),
(486, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 10:05:56', '2022-01-22 10:05:56'),
(487, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 10:20:56', '2022-01-22 10:20:56'),
(488, '2022-01-22 00:00:00', '66.249.65.57', '2022-01-22 10:55:50', '2022-01-22 10:55:50'),
(489, '2022-01-22 00:00:00', '66.249.65.61', '2022-01-22 11:10:50', '2022-01-22 11:10:50'),
(490, '2022-01-22 00:00:00', '66.249.65.59', '2022-01-22 11:35:57', '2022-01-22 11:35:57'),
(491, '2022-01-22 00:00:00', '27.255.186.65', '2022-01-22 12:24:47', '2022-01-22 12:24:47'),
(492, '2022-01-22 00:00:00', '101.0.53.182', '2022-01-22 13:49:42', '2022-01-22 13:49:42'),
(493, '2022-01-22 00:00:00', '66.249.68.61', '2022-01-22 13:57:52', '2022-01-22 13:57:52'),
(494, '2022-01-22 00:00:00', '66.249.68.61', '2022-01-22 14:12:51', '2022-01-22 14:12:51'),
(495, '2022-01-22 00:00:00', '66.249.68.61', '2022-01-22 14:27:51', '2022-01-22 14:27:51'),
(496, '2022-01-22 00:00:00', '66.249.68.57', '2022-01-22 14:42:51', '2022-01-22 14:42:51'),
(497, '2022-01-22 00:00:00', '27.255.186.65', '2022-01-22 14:50:06', '2022-01-22 14:50:06'),
(498, '2022-01-22 00:00:00', '101.0.54.170', '2022-01-22 14:51:27', '2022-01-22 14:51:27'),
(499, '2022-01-22 00:00:00', '66.249.68.61', '2022-01-22 14:57:51', '2022-01-22 14:57:51'),
(500, '2022-01-22 00:00:00', '66.249.68.57', '2022-01-22 15:12:51', '2022-01-22 15:12:51'),
(501, '2022-01-22 00:00:00', '27.255.186.65', '2022-01-22 15:27:07', '2022-01-22 15:27:07'),
(502, '2022-01-22 00:00:00', '66.249.68.57', '2022-01-22 15:27:51', '2022-01-22 15:27:51'),
(503, '2022-01-22 00:00:00', '124.253.122.248', '2022-01-22 16:19:06', '2022-01-22 16:19:06'),
(504, '2022-01-22 00:00:00', '66.249.68.57', '2022-01-22 18:03:53', '2022-01-22 18:03:53'),
(505, '2022-01-22 00:00:00', '122.161.74.100', '2022-01-22 18:18:45', '2022-01-22 18:18:45'),
(506, '2022-01-22 00:00:00', '66.249.68.57', '2022-01-22 18:18:53', '2022-01-22 18:18:53'),
(507, '2022-01-22 00:00:00', '66.249.68.59', '2022-01-22 18:33:53', '2022-01-22 18:33:53'),
(508, '2022-01-22 00:00:00', '157.55.39.56', '2022-01-22 19:47:46', '2022-01-22 19:47:46'),
(509, '2022-01-22 00:00:00', '157.55.39.188', '2022-01-22 20:53:00', '2022-01-22 20:53:00'),
(510, '2022-01-22 00:00:00', '66.249.68.61', '2022-01-22 21:25:51', '2022-01-22 21:25:51'),
(511, '2022-01-22 00:00:00', '66.249.68.57', '2022-01-22 21:55:55', '2022-01-22 21:55:55'),
(512, '2022-01-22 00:00:00', '49.14.98.43', '2022-01-22 22:04:37', '2022-01-22 22:04:37'),
(513, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 00:53:51', '2022-01-23 00:53:51'),
(514, '2022-01-23 00:00:00', '207.46.13.189', '2022-01-23 01:07:43', '2022-01-23 01:07:43'),
(515, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 01:13:51', '2022-01-23 01:13:51'),
(516, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 01:33:51', '2022-01-23 01:33:51'),
(517, '2022-01-23 00:00:00', '66.249.68.61', '2022-01-23 02:03:52', '2022-01-23 02:03:52'),
(518, '2022-01-23 00:00:00', '66.249.68.61', '2022-01-23 02:23:51', '2022-01-23 02:23:51'),
(519, '2022-01-23 00:00:00', '54.36.148.137', '2022-01-23 02:55:16', '2022-01-23 02:55:16'),
(520, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 03:03:51', '2022-01-23 03:03:51'),
(521, '2022-01-23 00:00:00', '54.36.148.137', '2022-01-23 03:13:10', '2022-01-23 03:13:10'),
(522, '2022-01-23 00:00:00', '54.36.148.137', '2022-01-23 06:42:11', '2022-01-23 06:42:11'),
(523, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 08:25:54', '2022-01-23 08:25:54'),
(524, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 08:35:54', '2022-01-23 08:35:54'),
(525, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 08:45:54', '2022-01-23 08:45:54'),
(526, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 09:05:53', '2022-01-23 09:05:53'),
(527, '2022-01-23 00:00:00', '66.249.68.61', '2022-01-23 09:15:53', '2022-01-23 09:15:53'),
(528, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 09:45:53', '2022-01-23 09:45:53'),
(529, '2022-01-23 00:00:00', '162.215.209.208', '2022-01-23 10:14:15', '2022-01-23 10:14:15'),
(530, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 12:25:53', '2022-01-23 12:25:53'),
(531, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 13:05:53', '2022-01-23 13:05:53'),
(532, '2022-01-23 00:00:00', '66.249.68.61', '2022-01-23 13:45:53', '2022-01-23 13:45:53'),
(533, '2022-01-23 00:00:00', '124.253.7.117', '2022-01-23 13:50:10', '2022-01-23 13:50:10'),
(534, '2022-01-23 00:00:00', '157.55.39.188', '2022-01-23 15:50:08', '2022-01-23 15:50:08'),
(535, '2022-01-23 00:00:00', '162.215.209.208', '2022-01-23 16:53:53', '2022-01-23 16:53:53'),
(536, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 17:57:14', '2022-01-23 17:57:14'),
(537, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 18:12:13', '2022-01-23 18:12:13'),
(538, '2022-01-23 00:00:00', '66.249.68.59', '2022-01-23 18:27:12', '2022-01-23 18:27:12'),
(539, '2022-01-23 00:00:00', '66.249.68.61', '2022-01-23 18:42:12', '2022-01-23 18:42:12'),
(540, '2022-01-23 00:00:00', '66.249.68.57', '2022-01-23 19:27:13', '2022-01-23 19:27:13'),
(541, '2022-01-23 00:00:00', '66.249.68.61', '2022-01-23 19:57:12', '2022-01-23 19:57:12'),
(542, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 00:09:27', '2022-01-24 00:09:27'),
(543, '2022-01-24 00:00:00', '66.249.68.61', '2022-01-24 00:37:12', '2022-01-24 00:37:12'),
(544, '2022-01-24 00:00:00', '66.249.68.61', '2022-01-24 00:57:12', '2022-01-24 00:57:12'),
(545, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 01:37:12', '2022-01-24 01:37:12'),
(546, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 01:57:12', '2022-01-24 01:57:12'),
(547, '2022-01-24 00:00:00', '66.249.68.59', '2022-01-24 02:09:27', '2022-01-24 02:09:27'),
(548, '2022-01-24 00:00:00', '40.77.167.34', '2022-01-24 02:40:46', '2022-01-24 02:40:46'),
(549, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 02:57:11', '2022-01-24 02:57:11'),
(550, '2022-01-24 00:00:00', '216.244.66.203', '2022-01-24 03:26:08', '2022-01-24 03:26:08'),
(551, '2022-01-24 00:00:00', '114.119.146.87', '2022-01-24 04:25:47', '2022-01-24 04:25:47'),
(552, '2022-01-24 00:00:00', '124.253.200.91', '2022-01-24 04:36:34', '2022-01-24 04:36:34'),
(553, '2022-01-24 00:00:00', '223.225.168.166', '2022-01-24 04:59:25', '2022-01-24 04:59:25');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(554, '2022-01-24 00:00:00', '124.253.200.91', '2022-01-24 04:59:31', '2022-01-24 04:59:31'),
(555, '2022-01-24 00:00:00', '117.239.5.129', '2022-01-24 05:28:20', '2022-01-24 05:28:20'),
(556, '2022-01-24 00:00:00', '223.178.208.229', '2022-01-24 05:54:31', '2022-01-24 05:54:31'),
(557, '2022-01-24 00:00:00', '223.178.208.229', '2022-01-24 06:10:34', '2022-01-24 06:10:34'),
(558, '2022-01-24 00:00:00', '223.225.168.166', '2022-01-24 06:16:32', '2022-01-24 06:16:32'),
(559, '2022-01-24 00:00:00', '124.253.200.91', '2022-01-24 06:26:17', '2022-01-24 06:26:17'),
(560, '2022-01-24 00:00:00', '223.225.168.166', '2022-01-24 06:33:37', '2022-01-24 06:33:37'),
(561, '2022-01-24 00:00:00', '40.77.167.6', '2022-01-24 06:36:44', '2022-01-24 06:36:44'),
(562, '2022-01-24 00:00:00', '124.253.200.91', '2022-01-24 06:38:31', '2022-01-24 06:38:31'),
(563, '2022-01-24 00:00:00', '157.55.39.188', '2022-01-24 07:18:18', '2022-01-24 07:18:18'),
(564, '2022-01-24 00:00:00', '117.239.5.129', '2022-01-24 09:02:15', '2022-01-24 09:02:15'),
(565, '2022-01-24 00:00:00', '124.253.200.91', '2022-01-24 09:22:54', '2022-01-24 09:22:54'),
(566, '2022-01-24 00:00:00', '117.239.5.129', '2022-01-24 09:55:26', '2022-01-24 09:55:26'),
(567, '2022-01-24 00:00:00', '124.253.200.91', '2022-01-24 10:16:24', '2022-01-24 10:16:24'),
(568, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 10:18:01', '2022-01-24 10:18:01'),
(569, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 10:47:59', '2022-01-24 10:47:59'),
(570, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 11:17:59', '2022-01-24 11:17:59'),
(571, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 12:30:58', '2022-01-24 12:30:58'),
(572, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 12:50:59', '2022-01-24 12:50:59'),
(573, '2022-01-24 00:00:00', '66.249.68.61', '2022-01-24 13:30:57', '2022-01-24 13:30:57'),
(574, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 14:10:58', '2022-01-24 14:10:58'),
(575, '2022-01-24 00:00:00', '66.249.68.61', '2022-01-24 15:00:57', '2022-01-24 15:00:57'),
(576, '2022-01-24 00:00:00', '66.249.68.61', '2022-01-24 15:30:57', '2022-01-24 15:30:57'),
(577, '2022-01-24 00:00:00', '66.249.68.59', '2022-01-24 15:45:57', '2022-01-24 15:45:57'),
(578, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 16:00:57', '2022-01-24 16:00:57'),
(579, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 23:00:00', '2022-01-24 23:00:00'),
(580, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 23:18:10', '2022-01-24 23:18:10'),
(581, '2022-01-24 00:00:00', '157.55.39.187', '2022-01-24 23:22:05', '2022-01-24 23:22:05'),
(582, '2022-01-24 00:00:00', '66.249.68.57', '2022-01-24 23:45:25', '2022-01-24 23:45:25'),
(583, '2022-01-25 00:00:00', '66.249.68.59', '2022-01-25 00:12:40', '2022-01-25 00:12:40'),
(584, '2022-01-25 00:00:00', '66.249.68.57', '2022-01-25 00:30:50', '2022-01-25 00:30:50'),
(585, '2022-01-25 00:00:00', '66.249.68.59', '2022-01-25 00:49:00', '2022-01-25 00:49:00'),
(586, '2022-01-25 00:00:00', '66.249.68.57', '2022-01-25 02:07:10', '2022-01-25 02:07:10'),
(587, '2022-01-25 00:00:00', '66.249.68.61', '2022-01-25 03:07:10', '2022-01-25 03:07:10'),
(588, '2022-01-25 00:00:00', '66.249.68.57', '2022-01-25 03:19:11', '2022-01-25 03:19:11'),
(589, '2022-01-25 00:00:00', '132.154.184.176', '2022-01-25 03:41:33', '2022-01-25 03:41:33'),
(590, '2022-01-25 00:00:00', '66.249.68.57', '2022-01-25 03:49:11', '2022-01-25 03:49:11'),
(591, '2022-01-25 00:00:00', '207.46.13.50', '2022-01-25 04:37:01', '2022-01-25 04:37:01'),
(592, '2022-01-25 00:00:00', '223.239.30.133', '2022-01-25 05:31:13', '2022-01-25 05:31:13'),
(593, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 06:37:21', '2022-01-25 06:37:21'),
(594, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 06:48:27', '2022-01-25 06:48:27'),
(595, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 06:59:56', '2022-01-25 06:59:56'),
(596, '2022-01-25 00:00:00', '157.55.39.26', '2022-01-25 07:24:33', '2022-01-25 07:24:33'),
(597, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 07:27:01', '2022-01-25 07:27:01'),
(598, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 08:15:35', '2022-01-25 08:15:35'),
(599, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 08:27:12', '2022-01-25 08:27:12'),
(600, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 08:39:56', '2022-01-25 08:39:56'),
(601, '2022-01-25 00:00:00', '223.225.173.97', '2022-01-25 10:05:47', '2022-01-25 10:05:47'),
(602, '2022-01-25 00:00:00', '117.239.5.129', '2022-01-25 10:07:39', '2022-01-25 10:07:39'),
(603, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 10:16:43', '2022-01-25 10:16:43'),
(604, '2022-01-25 00:00:00', '157.39.54.100', '2022-01-25 10:20:01', '2022-01-25 10:20:01'),
(605, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 10:32:03', '2022-01-25 10:32:03'),
(606, '2022-01-25 00:00:00', '66.249.68.57', '2022-01-25 11:21:11', '2022-01-25 11:21:11'),
(607, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 13:43:38', '2022-01-25 13:43:38'),
(608, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 13:56:08', '2022-01-25 13:56:08'),
(609, '2022-01-25 00:00:00', '157.55.39.187', '2022-01-25 14:03:00', '2022-01-25 14:03:00'),
(610, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 14:12:57', '2022-01-25 14:12:57'),
(611, '2022-01-25 00:00:00', '103.40.196.36', '2022-01-25 14:16:30', '2022-01-25 14:16:30'),
(612, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 14:25:42', '2022-01-25 14:25:42'),
(613, '2022-01-25 00:00:00', '124.253.153.88', '2022-01-25 14:36:51', '2022-01-25 14:36:51'),
(614, '2022-01-25 00:00:00', '66.249.64.59', '2022-01-25 17:12:12', '2022-01-25 17:12:12'),
(615, '2022-01-25 00:00:00', '54.36.148.137', '2022-01-25 21:00:57', '2022-01-25 21:00:57'),
(616, '2022-01-25 00:00:00', '40.77.167.2', '2022-01-25 22:07:44', '2022-01-25 22:07:44'),
(617, '2022-01-25 00:00:00', '66.249.64.61', '2022-01-25 23:26:47', '2022-01-25 23:26:47'),
(618, '2022-01-26 00:00:00', '66.249.64.59', '2022-01-26 00:26:47', '2022-01-26 00:26:47'),
(619, '2022-01-26 00:00:00', '114.119.147.184', '2022-01-26 02:01:57', '2022-01-26 02:01:57'),
(620, '2022-01-26 00:00:00', '114.119.141.77', '2022-01-26 05:09:29', '2022-01-26 05:09:29'),
(621, '2022-01-26 00:00:00', '157.55.39.28', '2022-01-26 05:34:58', '2022-01-26 05:34:58'),
(622, '2022-01-26 00:00:00', '54.36.148.137', '2022-01-26 05:43:56', '2022-01-26 05:43:56'),
(623, '2022-01-26 00:00:00', '66.249.64.57', '2022-01-26 07:21:19', '2022-01-26 07:21:19'),
(624, '2022-01-26 00:00:00', '223.225.56.194', '2022-01-26 08:34:33', '2022-01-26 08:34:33'),
(625, '2022-01-26 00:00:00', '223.185.14.71', '2022-01-26 08:38:24', '2022-01-26 08:38:24'),
(626, '2022-01-26 00:00:00', '54.36.148.137', '2022-01-26 09:38:21', '2022-01-26 09:38:21'),
(627, '2022-01-26 00:00:00', '162.215.209.208', '2022-01-26 10:44:21', '2022-01-26 10:44:21'),
(628, '2022-01-26 00:00:00', '66.249.68.61', '2022-01-26 11:26:50', '2022-01-26 11:26:50'),
(629, '2022-01-26 00:00:00', '66.249.68.59', '2022-01-26 14:26:47', '2022-01-26 14:26:47'),
(630, '2022-01-26 00:00:00', '66.249.68.57', '2022-01-26 16:56:46', '2022-01-26 16:56:46'),
(631, '2022-01-26 00:00:00', '162.215.209.208', '2022-01-26 17:08:09', '2022-01-26 17:08:09'),
(632, '2022-01-26 00:00:00', '66.249.68.61', '2022-01-26 20:58:40', '2022-01-26 20:58:40'),
(633, '2022-01-27 00:00:00', '66.249.68.59', '2022-01-27 00:04:22', '2022-01-27 00:04:22'),
(634, '2022-01-27 00:00:00', '103.217.123.199', '2022-01-27 02:58:47', '2022-01-27 02:58:47'),
(635, '2022-01-27 00:00:00', '117.239.5.129', '2022-01-27 04:06:08', '2022-01-27 04:06:08'),
(636, '2022-01-27 00:00:00', '117.239.5.129', '2022-01-27 04:22:23', '2022-01-27 04:22:23'),
(637, '2022-01-27 00:00:00', '114.119.142.7', '2022-01-27 04:46:33', '2022-01-27 04:46:33'),
(638, '2022-01-27 00:00:00', '117.239.5.129', '2022-01-27 04:54:20', '2022-01-27 04:54:20'),
(639, '2022-01-27 00:00:00', '117.239.5.129', '2022-01-27 05:05:39', '2022-01-27 05:05:39'),
(640, '2022-01-27 00:00:00', '10.43.252.152', '2022-01-27 06:31:33', '2022-01-27 06:31:33'),
(641, '2022-01-27 00:00:00', '10.43.252.152', '2022-01-27 06:53:14', '2022-01-27 06:53:14'),
(642, '2022-01-27 00:00:00', '157.55.39.28', '2022-01-27 10:20:52', '2022-01-27 10:20:52'),
(643, '2022-01-27 00:00:00', '124.253.102.242', '2022-01-27 10:50:09', '2022-01-27 10:50:09'),
(644, '2022-01-27 00:00:00', '66.249.68.59', '2022-01-27 11:22:35', '2022-01-27 11:22:35'),
(645, '2022-01-27 00:00:00', '157.43.202.135', '2022-01-27 12:20:20', '2022-01-27 12:20:20'),
(646, '2022-01-27 00:00:00', '66.249.68.61', '2022-01-27 12:28:53', '2022-01-27 12:28:53'),
(647, '2022-01-27 00:00:00', '66.249.68.57', '2022-01-27 13:28:53', '2022-01-27 13:28:53'),
(648, '2022-01-27 00:00:00', '207.46.13.7', '2022-01-27 13:40:16', '2022-01-27 13:40:16'),
(649, '2022-01-27 00:00:00', '216.244.66.203', '2022-01-27 14:22:05', '2022-01-27 14:22:05'),
(650, '2022-01-27 00:00:00', '138.201.195.62', '2022-01-27 16:54:06', '2022-01-27 16:54:06'),
(651, '2022-01-27 00:00:00', '66.249.68.59', '2022-01-27 22:51:54', '2022-01-27 22:51:54'),
(652, '2022-01-27 00:00:00', '66.249.68.59', '2022-01-27 23:19:35', '2022-01-27 23:19:35'),
(653, '2022-01-27 00:00:00', '66.249.68.57', '2022-01-27 23:47:17', '2022-01-27 23:47:17'),
(654, '2022-01-28 00:00:00', '66.249.68.57', '2022-01-28 01:10:15', '2022-01-28 01:10:15'),
(655, '2022-01-28 00:00:00', '66.249.68.61', '2022-01-28 02:05:32', '2022-01-28 02:05:32'),
(656, '2022-01-28 00:00:00', '66.249.68.61', '2022-01-28 02:25:32', '2022-01-28 02:25:32'),
(657, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 02:45:32', '2022-01-28 02:45:32'),
(658, '2022-01-28 00:00:00', '66.249.68.61', '2022-01-28 03:05:32', '2022-01-28 03:05:32'),
(659, '2022-01-28 00:00:00', '66.249.68.57', '2022-01-28 04:25:32', '2022-01-28 04:25:32'),
(660, '2022-01-28 00:00:00', '66.249.68.61', '2022-01-28 05:25:32', '2022-01-28 05:25:32'),
(661, '2022-01-28 00:00:00', '114.119.150.226', '2022-01-28 05:51:03', '2022-01-28 05:51:03'),
(662, '2022-01-28 00:00:00', '124.253.123.132', '2022-01-28 09:37:32', '2022-01-28 09:37:32'),
(663, '2022-01-28 00:00:00', '124.253.123.132', '2022-01-28 09:52:32', '2022-01-28 09:52:32'),
(664, '2022-01-28 00:00:00', '124.253.123.132', '2022-01-28 11:14:43', '2022-01-28 11:14:43'),
(665, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 12:10:36', '2022-01-28 12:10:36'),
(666, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 12:40:36', '2022-01-28 12:40:36'),
(667, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 13:00:36', '2022-01-28 13:00:36'),
(668, '2022-01-28 00:00:00', '66.249.68.57', '2022-01-28 13:10:36', '2022-01-28 13:10:36'),
(669, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 13:30:36', '2022-01-28 13:30:36'),
(670, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 13:50:36', '2022-01-28 13:50:36'),
(671, '2022-01-28 00:00:00', '66.249.68.61', '2022-01-28 14:10:36', '2022-01-28 14:10:36'),
(672, '2022-01-28 00:00:00', '207.46.13.150', '2022-01-28 14:34:35', '2022-01-28 14:34:35'),
(673, '2022-01-28 00:00:00', '66.249.68.59', '2022-01-28 16:20:36', '2022-01-28 16:20:36'),
(674, '2022-01-28 00:00:00', '207.46.13.7', '2022-01-28 17:32:55', '2022-01-28 17:32:55'),
(675, '2022-01-28 00:00:00', '66.249.68.57', '2022-01-28 19:09:32', '2022-01-28 19:09:32'),
(676, '2022-01-28 00:00:00', '207.46.13.7', '2022-01-28 22:06:00', '2022-01-28 22:06:00'),
(677, '2022-01-28 00:00:00', '66.249.68.61', '2022-01-28 23:07:32', '2022-01-28 23:07:32'),
(678, '2022-01-29 00:00:00', '207.46.13.185', '2022-01-29 03:29:56', '2022-01-29 03:29:56'),
(679, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 03:35:35', '2022-01-29 03:35:35'),
(680, '2022-01-29 00:00:00', '66.249.68.61', '2022-01-29 04:19:55', '2022-01-29 04:19:55'),
(681, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 05:07:31', '2022-01-29 05:07:31'),
(682, '2022-01-29 00:00:00', '66.249.68.61', '2022-01-29 06:13:32', '2022-01-29 06:13:32'),
(683, '2022-01-29 00:00:00', '132.154.167.123', '2022-01-29 06:42:33', '2022-01-29 06:42:33'),
(684, '2022-01-29 00:00:00', '207.46.13.7', '2022-01-29 07:05:25', '2022-01-29 07:05:25'),
(685, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 07:32:33', '2022-01-29 07:32:33'),
(686, '2022-01-29 00:00:00', '180.188.224.210', '2022-01-29 09:30:03', '2022-01-29 09:30:03'),
(687, '2022-01-29 00:00:00', '157.39.218.83', '2022-01-29 10:00:28', '2022-01-29 10:00:28'),
(688, '2022-01-29 00:00:00', '106.196.127.73', '2022-01-29 11:23:43', '2022-01-29 11:23:43'),
(689, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 11:31:35', '2022-01-29 11:31:35'),
(690, '2022-01-29 00:00:00', '162.215.209.208', '2022-01-29 11:40:56', '2022-01-29 11:40:56'),
(691, '2022-01-29 00:00:00', '66.249.68.61', '2022-01-29 12:31:36', '2022-01-29 12:31:36'),
(692, '2022-01-29 00:00:00', '66.249.68.57', '2022-01-29 12:46:35', '2022-01-29 12:46:35'),
(693, '2022-01-29 00:00:00', '114.119.145.78', '2022-01-29 13:26:24', '2022-01-29 13:26:24'),
(694, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 15:46:35', '2022-01-29 15:46:35'),
(695, '2022-01-29 00:00:00', '162.215.209.208', '2022-01-29 17:08:19', '2022-01-29 17:08:19'),
(696, '2022-01-29 00:00:00', '66.249.68.61', '2022-01-29 22:00:38', '2022-01-29 22:00:38'),
(697, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 22:30:37', '2022-01-29 22:30:37'),
(698, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 23:00:36', '2022-01-29 23:00:36'),
(699, '2022-01-29 00:00:00', '66.249.68.59', '2022-01-29 23:30:36', '2022-01-29 23:30:36'),
(700, '2022-01-30 00:00:00', '66.249.68.59', '2022-01-30 00:00:36', '2022-01-30 00:00:36'),
(701, '2022-01-30 00:00:00', '66.249.68.57', '2022-01-30 00:15:37', '2022-01-30 00:15:37'),
(702, '2022-01-30 00:00:00', '66.249.68.61', '2022-01-30 00:27:37', '2022-01-30 00:27:37'),
(703, '2022-01-30 00:00:00', '66.249.68.59', '2022-01-30 00:45:37', '2022-01-30 00:45:37'),
(704, '2022-01-30 00:00:00', '66.249.68.59', '2022-01-30 01:21:37', '2022-01-30 01:21:37'),
(705, '2022-01-30 00:00:00', '66.249.68.61', '2022-01-30 01:45:37', '2022-01-30 01:45:37'),
(706, '2022-01-30 00:00:00', '66.249.68.61', '2022-01-30 01:57:37', '2022-01-30 01:57:37'),
(707, '2022-01-30 00:00:00', '66.249.68.61', '2022-01-30 02:09:37', '2022-01-30 02:09:37'),
(708, '2022-01-30 00:00:00', '66.249.68.59', '2022-01-30 02:51:37', '2022-01-30 02:51:37'),
(709, '2022-01-30 00:00:00', '20.191.45.212', '2022-01-30 04:19:30', '2022-01-30 04:19:30'),
(710, '2022-01-30 00:00:00', '157.39.236.78', '2022-01-30 05:33:05', '2022-01-30 05:33:05'),
(711, '2022-01-30 00:00:00', '157.39.236.78', '2022-01-30 05:44:14', '2022-01-30 05:44:14'),
(712, '2022-01-30 00:00:00', '132.154.128.174', '2022-01-30 08:44:57', '2022-01-30 08:44:57'),
(713, '2022-01-30 00:00:00', '64.233.173.26', '2022-01-30 08:45:00', '2022-01-30 08:45:00'),
(714, '2022-01-30 00:00:00', '64.233.172.189', '2022-01-30 08:45:01', '2022-01-30 08:45:01'),
(715, '2022-01-30 00:00:00', '66.249.68.57', '2022-01-30 10:52:38', '2022-01-30 10:52:38'),
(716, '2022-01-30 00:00:00', '66.249.66.217', '2022-01-30 11:22:38', '2022-01-30 11:22:38'),
(717, '2022-01-30 00:00:00', '66.249.66.219', '2022-01-30 11:58:41', '2022-01-30 11:58:41'),
(718, '2022-01-30 00:00:00', '66.249.66.217', '2022-01-30 12:13:40', '2022-01-30 12:13:40'),
(719, '2022-01-30 00:00:00', '66.249.66.219', '2022-01-30 12:43:41', '2022-01-30 12:43:41'),
(720, '2022-01-30 00:00:00', '157.55.39.126', '2022-01-30 14:04:11', '2022-01-30 14:04:11'),
(721, '2022-01-30 00:00:00', '49.36.226.128', '2022-01-30 15:15:55', '2022-01-30 15:15:55'),
(722, '2022-01-30 00:00:00', '66.249.66.92', '2022-01-30 15:58:43', '2022-01-30 15:58:43'),
(723, '2022-01-30 00:00:00', '66.249.66.135', '2022-01-30 16:38:40', '2022-01-30 16:38:40'),
(724, '2022-01-30 00:00:00', '66.249.66.61', '2022-01-30 17:18:43', '2022-01-30 17:18:43'),
(725, '2022-01-30 00:00:00', '54.36.148.137', '2022-01-30 18:49:48', '2022-01-30 18:49:48'),
(726, '2022-01-30 00:00:00', '66.249.66.63', '2022-01-30 18:58:40', '2022-01-30 18:58:40'),
(727, '2022-01-30 00:00:00', '66.249.66.221', '2022-01-30 20:46:40', '2022-01-30 20:46:40'),
(728, '2022-01-30 00:00:00', '66.249.66.219', '2022-01-30 23:15:40', '2022-01-30 23:15:40'),
(729, '2022-01-31 00:00:00', '66.249.66.221', '2022-01-31 01:58:40', '2022-01-31 01:58:40'),
(730, '2022-01-31 00:00:00', '66.249.66.217', '2022-01-31 02:50:41', '2022-01-31 02:50:41'),
(731, '2022-01-31 00:00:00', '66.249.66.219', '2022-01-31 02:58:40', '2022-01-31 02:58:40'),
(732, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 03:43:00', '2022-01-31 03:43:00'),
(733, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 04:03:58', '2022-01-31 04:03:58'),
(734, '2022-01-31 00:00:00', '66.249.66.217', '2022-01-31 04:24:36', '2022-01-31 04:24:36'),
(735, '2022-01-31 00:00:00', '207.46.13.7', '2022-01-31 04:47:34', '2022-01-31 04:47:34'),
(736, '2022-01-31 00:00:00', '66.249.66.217', '2022-01-31 04:54:36', '2022-01-31 04:54:36'),
(737, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 05:00:15', '2022-01-31 05:00:15'),
(738, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 05:13:41', '2022-01-31 05:13:41'),
(739, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 05:30:23', '2022-01-31 05:30:23'),
(740, '2022-01-31 00:00:00', '66.249.66.221', '2022-01-31 05:50:41', '2022-01-31 05:50:41'),
(741, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 06:00:05', '2022-01-31 06:00:05'),
(742, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 06:17:44', '2022-01-31 06:17:44'),
(743, '2022-01-31 00:00:00', '66.249.66.221', '2022-01-31 06:20:41', '2022-01-31 06:20:41'),
(744, '2022-01-31 00:00:00', '103.21.55.38', '2022-01-31 06:25:33', '2022-01-31 06:25:33'),
(745, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 06:26:06', '2022-01-31 06:26:06'),
(746, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 06:47:29', '2022-01-31 06:47:29'),
(747, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 06:58:29', '2022-01-31 06:58:29'),
(748, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 08:43:04', '2022-01-31 08:43:04'),
(749, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 09:33:40', '2022-01-31 09:33:40'),
(750, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 09:50:38', '2022-01-31 09:50:38'),
(751, '2022-01-31 00:00:00', '117.239.5.129', '2022-01-31 09:56:48', '2022-01-31 09:56:48'),
(752, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 10:08:06', '2022-01-31 10:08:06'),
(753, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 10:20:27', '2022-01-31 10:20:27'),
(754, '2022-01-31 00:00:00', '66.249.66.217', '2022-01-31 10:29:14', '2022-01-31 10:29:14'),
(755, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 10:32:13', '2022-01-31 10:32:13'),
(756, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 10:44:58', '2022-01-31 10:44:58'),
(757, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 10:59:54', '2022-01-31 10:59:54'),
(758, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 11:11:06', '2022-01-31 11:11:06'),
(759, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 12:02:55', '2022-01-31 12:02:55'),
(760, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 12:18:41', '2022-01-31 12:18:41'),
(761, '2022-01-31 00:00:00', '106.204.210.85', '2022-01-31 12:50:15', '2022-01-31 12:50:15'),
(762, '2022-01-31 00:00:00', '66.249.75.221', '2022-01-31 12:52:12', '2022-01-31 12:52:12'),
(763, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 13:02:12', '2022-01-31 13:02:12'),
(764, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 13:13:43', '2022-01-31 13:13:43'),
(765, '2022-01-31 00:00:00', '66.249.75.222', '2022-01-31 13:18:43', '2022-01-31 13:18:43'),
(766, '2022-01-31 00:00:00', '114.119.144.132', '2022-01-31 13:21:05', '2022-01-31 13:21:05'),
(767, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 13:40:30', '2022-01-31 13:40:30'),
(768, '2022-01-31 00:00:00', '66.249.75.222', '2022-01-31 13:48:43', '2022-01-31 13:48:43'),
(769, '2022-01-31 00:00:00', '124.253.106.161', '2022-01-31 13:53:53', '2022-01-31 13:53:53'),
(770, '2022-01-31 00:00:00', '49.156.98.151', '2022-01-31 13:55:06', '2022-01-31 13:55:06'),
(771, '2022-01-31 00:00:00', '49.156.98.151', '2022-01-31 14:06:27', '2022-01-31 14:06:27'),
(772, '2022-01-31 00:00:00', '66.249.75.223', '2022-01-31 14:09:42', '2022-01-31 14:09:42'),
(773, '2022-01-31 00:00:00', '66.249.75.222', '2022-01-31 14:29:42', '2022-01-31 14:29:42'),
(774, '2022-01-31 00:00:00', '49.156.98.151', '2022-01-31 15:36:12', '2022-01-31 15:36:12'),
(775, '2022-01-31 00:00:00', '157.55.39.28', '2022-01-31 15:58:12', '2022-01-31 15:58:12'),
(776, '2022-01-31 00:00:00', '114.119.146.198', '2022-01-31 15:58:12', '2022-01-31 15:58:12'),
(777, '2022-01-31 00:00:00', '49.36.228.49', '2022-01-31 17:38:11', '2022-01-31 17:38:11'),
(778, '2022-01-31 00:00:00', '66.249.75.221', '2022-01-31 21:45:47', '2022-01-31 21:45:47'),
(779, '2022-01-31 00:00:00', '66.249.75.221', '2022-01-31 22:05:45', '2022-01-31 22:05:45'),
(780, '2022-01-31 00:00:00', '66.249.75.221', '2022-01-31 22:25:44', '2022-01-31 22:25:44'),
(781, '2022-01-31 00:00:00', '66.249.75.223', '2022-01-31 22:59:46', '2022-01-31 22:59:46'),
(782, '2022-01-31 00:00:00', '66.249.75.223', '2022-01-31 23:11:27', '2022-01-31 23:11:27'),
(783, '2022-01-31 00:00:00', '66.249.75.222', '2022-01-31 23:28:35', '2022-01-31 23:28:35'),
(784, '2022-01-31 00:00:00', '66.249.75.221', '2022-01-31 23:45:44', '2022-01-31 23:45:44'),
(785, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 00:02:52', '2022-02-01 00:02:52'),
(786, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 00:28:35', '2022-02-01 00:28:35'),
(787, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 00:45:44', '2022-02-01 00:45:44'),
(788, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 01:02:52', '2022-02-01 01:02:52'),
(789, '2022-02-01 00:00:00', '66.249.75.221', '2022-02-01 01:11:26', '2022-02-01 01:11:26'),
(790, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 01:20:01', '2022-02-01 01:20:01'),
(791, '2022-02-01 00:00:00', '66.249.75.221', '2022-02-01 01:28:35', '2022-02-01 01:28:35'),
(792, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 01:37:09', '2022-02-01 01:37:09'),
(793, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 02:02:52', '2022-02-01 02:02:52'),
(794, '2022-02-01 00:00:00', '66.249.75.221', '2022-02-01 02:20:01', '2022-02-01 02:20:01'),
(795, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 02:37:09', '2022-02-01 02:37:09'),
(796, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 02:54:18', '2022-02-01 02:54:18'),
(797, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 03:02:52', '2022-02-01 03:02:52'),
(798, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 03:11:26', '2022-02-01 03:11:26'),
(799, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 03:28:35', '2022-02-01 03:28:35'),
(800, '2022-02-01 00:00:00', '66.249.75.221', '2022-02-01 03:37:09', '2022-02-01 03:37:09'),
(801, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 03:45:43', '2022-02-01 03:45:43'),
(802, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 03:54:18', '2022-02-01 03:54:18'),
(803, '2022-02-01 00:00:00', '66.249.75.221', '2022-02-01 04:11:27', '2022-02-01 04:11:27'),
(804, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 04:20:01', '2022-02-01 04:20:01'),
(805, '2022-02-01 00:00:00', '66.249.75.223', '2022-02-01 04:28:35', '2022-02-01 04:28:35'),
(806, '2022-02-01 00:00:00', '66.249.75.222', '2022-02-01 04:37:09', '2022-02-01 04:37:09'),
(807, '2022-02-01 00:00:00', '66.249.75.221', '2022-02-01 04:45:43', '2022-02-01 04:45:43'),
(808, '2022-02-01 00:00:00', '49.156.98.151', '2022-02-01 04:48:13', '2022-02-01 04:48:13'),
(809, '2022-02-01 00:00:00', '49.156.98.151', '2022-02-01 05:00:02', '2022-02-01 05:00:02'),
(810, '2022-02-01 00:00:00', '106.204.197.47', '2022-02-01 05:14:02', '2022-02-01 05:14:02'),
(811, '2022-02-01 00:00:00', '59.89.153.107', '2022-02-01 05:34:08', '2022-02-01 05:34:08'),
(812, '2022-02-01 00:00:00', '42.109.224.22', '2022-02-01 08:18:33', '2022-02-01 08:18:33'),
(813, '2022-02-01 00:00:00', '124.253.45.210', '2022-02-01 09:34:53', '2022-02-01 09:34:53'),
(814, '2022-02-01 00:00:00', '124.253.45.210', '2022-02-01 10:15:09', '2022-02-01 10:15:09'),
(815, '2022-02-01 00:00:00', '157.38.243.192', '2022-02-01 11:06:41', '2022-02-01 11:06:41'),
(816, '2022-02-01 00:00:00', '117.229.151.5', '2022-02-01 12:22:42', '2022-02-01 12:22:42'),
(817, '2022-02-01 00:00:00', '162.215.209.208', '2022-02-01 12:38:23', '2022-02-01 12:38:23'),
(818, '2022-02-01 00:00:00', '106.204.197.47', '2022-02-01 12:50:58', '2022-02-01 12:50:58'),
(819, '2022-02-01 00:00:00', '124.253.45.210', '2022-02-01 12:51:07', '2022-02-01 12:51:07'),
(820, '2022-02-01 00:00:00', '106.204.197.47', '2022-02-01 13:01:58', '2022-02-01 13:01:58'),
(821, '2022-02-01 00:00:00', '107.178.194.221', '2022-02-01 13:21:37', '2022-02-01 13:21:37'),
(822, '2022-02-01 00:00:00', '107.178.194.223', '2022-02-01 13:21:45', '2022-02-01 13:21:45'),
(823, '2022-02-01 00:00:00', '204.13.200.200', '2022-02-01 13:22:03', '2022-02-01 13:22:03'),
(824, '2022-02-01 00:00:00', '107.178.194.221', '2022-02-01 14:08:02', '2022-02-01 14:08:02'),
(825, '2022-02-01 00:00:00', '176.254.66.99', '2022-02-01 14:26:00', '2022-02-01 14:26:00'),
(826, '2022-02-01 00:00:00', '66.249.93.252', '2022-02-01 14:26:41', '2022-02-01 14:26:41'),
(827, '2022-02-01 00:00:00', '66.249.93.225', '2022-02-01 14:26:41', '2022-02-01 14:26:41'),
(828, '2022-02-01 00:00:00', '66.102.9.57', '2022-02-01 14:26:41', '2022-02-01 14:26:41'),
(829, '2022-02-01 00:00:00', '66.102.9.59', '2022-02-01 14:26:41', '2022-02-01 14:26:41'),
(830, '2022-02-01 00:00:00', '66.249.93.254', '2022-02-01 14:27:00', '2022-02-01 14:27:00'),
(831, '2022-02-01 00:00:00', '101.0.41.116', '2022-02-01 15:44:47', '2022-02-01 15:44:47'),
(832, '2022-02-01 00:00:00', '66.249.68.59', '2022-02-01 16:05:51', '2022-02-01 16:05:51'),
(833, '2022-02-01 00:00:00', '66.249.68.59', '2022-02-01 16:35:50', '2022-02-01 16:35:50'),
(834, '2022-02-01 00:00:00', '66.249.68.61', '2022-02-01 16:35:51', '2022-02-01 16:35:51'),
(835, '2022-02-01 00:00:00', '66.249.68.59', '2022-02-01 17:05:50', '2022-02-01 17:05:50'),
(836, '2022-02-01 00:00:00', '162.215.209.208', '2022-02-01 17:08:21', '2022-02-01 17:08:21'),
(837, '2022-02-01 00:00:00', '66.249.68.57', '2022-02-01 17:35:50', '2022-02-01 17:35:50'),
(838, '2022-02-01 00:00:00', '66.249.68.59', '2022-02-01 17:35:51', '2022-02-01 17:35:51'),
(839, '2022-02-01 00:00:00', '203.115.91.129', '2022-02-01 18:16:46', '2022-02-01 18:16:46'),
(840, '2022-02-01 00:00:00', '66.249.68.57', '2022-02-01 19:10:50', '2022-02-01 19:10:50'),
(841, '2022-02-01 00:00:00', '66.249.68.59', '2022-02-01 19:30:51', '2022-02-01 19:30:51'),
(842, '2022-02-01 00:00:00', '66.249.68.59', '2022-02-01 19:50:50', '2022-02-01 19:50:50'),
(843, '2022-02-01 00:00:00', '114.119.148.175', '2022-02-01 21:29:23', '2022-02-01 21:29:23'),
(844, '2022-02-01 00:00:00', '66.249.68.61', '2022-02-01 22:05:52', '2022-02-01 22:05:52'),
(845, '2022-02-01 00:00:00', '66.249.68.57', '2022-02-01 22:05:53', '2022-02-01 22:05:53'),
(846, '2022-02-01 00:00:00', '66.249.68.57', '2022-02-01 22:35:53', '2022-02-01 22:35:53'),
(847, '2022-02-01 00:00:00', '66.249.68.57', '2022-02-01 23:05:53', '2022-02-01 23:05:53'),
(848, '2022-02-01 00:00:00', '66.249.68.57', '2022-02-01 23:35:52', '2022-02-01 23:35:52'),
(849, '2022-02-02 00:00:00', '66.249.68.61', '2022-02-02 00:00:19', '2022-02-02 00:00:19'),
(850, '2022-02-02 00:00:00', '66.249.68.57', '2022-02-02 00:15:19', '2022-02-02 00:15:19'),
(851, '2022-02-02 00:00:00', '66.249.68.61', '2022-02-02 00:22:49', '2022-02-02 00:22:49'),
(852, '2022-02-02 00:00:00', '66.249.68.59', '2022-02-02 00:30:19', '2022-02-02 00:30:19'),
(853, '2022-02-02 00:00:00', '66.249.68.61', '2022-02-02 00:52:49', '2022-02-02 00:52:49'),
(854, '2022-02-02 00:00:00', '66.249.68.59', '2022-02-02 01:07:49', '2022-02-02 01:07:49'),
(855, '2022-02-02 00:00:00', '66.249.68.61', '2022-02-02 01:22:49', '2022-02-02 01:22:49'),
(856, '2022-02-02 00:00:00', '66.249.68.57', '2022-02-02 03:45:38', '2022-02-02 03:45:38'),
(857, '2022-02-02 00:00:00', '151.80.195.102', '2022-02-02 04:36:18', '2022-02-02 04:36:18'),
(858, '2022-02-02 00:00:00', '124.253.45.210', '2022-02-02 04:42:25', '2022-02-02 04:42:25'),
(859, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 05:13:10', '2022-02-02 05:13:10'),
(860, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 05:28:39', '2022-02-02 05:28:39'),
(861, '2022-02-02 00:00:00', '103.204.171.222', '2022-02-02 05:28:39', '2022-02-02 05:28:39'),
(862, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 05:39:09', '2022-02-02 05:39:09'),
(863, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 05:39:50', '2022-02-02 05:39:50'),
(864, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 05:50:51', '2022-02-02 05:50:51'),
(865, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 05:52:44', '2022-02-02 05:52:44'),
(866, '2022-02-02 00:00:00', '157.39.56.47', '2022-02-02 05:59:03', '2022-02-02 05:59:03'),
(867, '2022-02-02 00:00:00', '157.39.16.167', '2022-02-02 06:00:15', '2022-02-02 06:00:15'),
(868, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 06:03:05', '2022-02-02 06:03:05'),
(869, '2022-02-02 00:00:00', '52.114.32.28', '2022-02-02 06:03:58', '2022-02-02 06:03:58'),
(870, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 06:04:12', '2022-02-02 06:04:12'),
(871, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 06:04:45', '2022-02-02 06:04:45'),
(872, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 06:08:03', '2022-02-02 06:08:03'),
(873, '2022-02-02 00:00:00', '66.249.68.61', '2022-02-02 06:14:39', '2022-02-02 06:14:39'),
(874, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 06:16:21', '2022-02-02 06:16:21'),
(875, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 06:16:29', '2022-02-02 06:16:29'),
(876, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 06:27:29', '2022-02-02 06:27:29'),
(877, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 06:27:56', '2022-02-02 06:27:56'),
(878, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 06:41:28', '2022-02-02 06:41:28'),
(879, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 06:44:51', '2022-02-02 06:44:51'),
(880, '2022-02-02 00:00:00', '157.39.16.167', '2022-02-02 06:46:37', '2022-02-02 06:46:37'),
(881, '2022-02-02 00:00:00', '106.220.107.162', '2022-02-02 06:47:52', '2022-02-02 06:47:52'),
(882, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 06:52:41', '2022-02-02 06:52:41'),
(883, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 06:56:00', '2022-02-02 06:56:00'),
(884, '2022-02-02 00:00:00', '157.39.52.199', '2022-02-02 06:57:23', '2022-02-02 06:57:23'),
(885, '2022-02-02 00:00:00', '106.220.107.162', '2022-02-02 06:59:21', '2022-02-02 06:59:21'),
(886, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 07:05:24', '2022-02-02 07:05:24'),
(887, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 07:07:55', '2022-02-02 07:07:55'),
(888, '2022-02-02 00:00:00', '157.39.16.167', '2022-02-02 07:09:07', '2022-02-02 07:09:07'),
(889, '2022-02-02 00:00:00', '157.39.52.199', '2022-02-02 07:10:47', '2022-02-02 07:10:47'),
(890, '2022-02-02 00:00:00', '66.249.68.57', '2022-02-02 07:15:12', '2022-02-02 07:15:12'),
(891, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 07:18:43', '2022-02-02 07:18:43'),
(892, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 07:21:24', '2022-02-02 07:21:24'),
(893, '2022-02-02 00:00:00', '157.39.52.199', '2022-02-02 07:24:29', '2022-02-02 07:24:29'),
(894, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 07:26:28', '2022-02-02 07:26:28'),
(895, '2022-02-02 00:00:00', '157.39.16.167', '2022-02-02 07:28:49', '2022-02-02 07:28:49'),
(896, '2022-02-02 00:00:00', '47.8.90.155', '2022-02-02 07:30:13', '2022-02-02 07:30:13'),
(897, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 07:33:57', '2022-02-02 07:33:57'),
(898, '2022-02-02 00:00:00', '157.39.52.199', '2022-02-02 07:35:31', '2022-02-02 07:35:31'),
(899, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 07:37:49', '2022-02-02 07:37:49'),
(900, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 07:46:13', '2022-02-02 07:46:13'),
(901, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 07:49:53', '2022-02-02 07:49:53'),
(902, '2022-02-02 00:00:00', '157.39.52.199', '2022-02-02 07:52:38', '2022-02-02 07:52:38'),
(903, '2022-02-02 00:00:00', '203.115.91.129', '2022-02-02 07:53:35', '2022-02-02 07:53:35'),
(904, '2022-02-02 00:00:00', '157.39.16.167', '2022-02-02 07:57:30', '2022-02-02 07:57:30'),
(905, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 08:01:02', '2022-02-02 08:01:02'),
(906, '2022-02-02 00:00:00', '157.55.39.28', '2022-02-02 08:09:48', '2022-02-02 08:09:48'),
(907, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 08:41:01', '2022-02-02 08:41:01'),
(908, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 08:44:01', '2022-02-02 08:44:01'),
(909, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 08:52:43', '2022-02-02 08:52:43'),
(910, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 09:04:29', '2022-02-02 09:04:29'),
(911, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 09:13:18', '2022-02-02 09:13:18'),
(912, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 09:15:40', '2022-02-02 09:15:40'),
(913, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 09:26:02', '2022-02-02 09:26:02'),
(914, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 09:29:35', '2022-02-02 09:29:35'),
(915, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 09:37:43', '2022-02-02 09:37:43'),
(916, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 09:42:29', '2022-02-02 09:42:29'),
(917, '2022-02-02 00:00:00', '122.179.244.229', '2022-02-02 09:45:11', '2022-02-02 09:45:11'),
(918, '2022-02-02 00:00:00', '117.239.5.129', '2022-02-02 10:06:10', '2022-02-02 10:06:10'),
(919, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 10:53:36', '2022-02-02 10:53:36'),
(920, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 11:17:03', '2022-02-02 11:17:03'),
(921, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 11:34:41', '2022-02-02 11:34:41'),
(922, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 11:47:12', '2022-02-02 11:47:12'),
(923, '2022-02-02 00:00:00', '66.249.73.153', '2022-02-02 11:58:30', '2022-02-02 11:58:30'),
(924, '2022-02-02 00:00:00', '66.249.73.155', '2022-02-02 12:05:59', '2022-02-02 12:05:59'),
(925, '2022-02-02 00:00:00', '132.154.190.68', '2022-02-02 12:13:18', '2022-02-02 12:13:18'),
(926, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 12:19:02', '2022-02-02 12:19:02'),
(927, '2022-02-02 00:00:00', '66.249.73.157', '2022-02-02 12:21:00', '2022-02-02 12:21:00'),
(928, '2022-02-02 00:00:00', '66.249.73.155', '2022-02-02 12:28:28', '2022-02-02 12:28:28'),
(929, '2022-02-02 00:00:00', '66.249.73.153', '2022-02-02 12:35:58', '2022-02-02 12:35:58'),
(930, '2022-02-02 00:00:00', '66.249.73.155', '2022-02-02 13:13:28', '2022-02-02 13:13:28'),
(931, '2022-02-02 00:00:00', '66.249.73.155', '2022-02-02 13:43:28', '2022-02-02 13:43:28'),
(932, '2022-02-02 00:00:00', '66.249.73.157', '2022-02-02 13:58:28', '2022-02-02 13:58:28'),
(933, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 14:09:52', '2022-02-02 14:09:52'),
(934, '2022-02-02 00:00:00', '66.249.73.155', '2022-02-02 14:13:28', '2022-02-02 14:13:28'),
(935, '2022-02-02 00:00:00', '106.204.198.65', '2022-02-02 14:20:20', '2022-02-02 14:20:20'),
(936, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 14:21:00', '2022-02-02 14:21:00'),
(937, '2022-02-02 00:00:00', '66.249.73.153', '2022-02-02 14:28:30', '2022-02-02 14:28:30'),
(938, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 14:37:17', '2022-02-02 14:37:17'),
(939, '2022-02-02 00:00:00', '49.156.91.14', '2022-02-02 14:48:28', '2022-02-02 14:48:28'),
(940, '2022-02-02 00:00:00', '157.55.39.126', '2022-02-02 17:42:38', '2022-02-02 17:42:38'),
(941, '2022-02-02 00:00:00', '54.36.148.137', '2022-02-02 17:49:04', '2022-02-02 17:49:04'),
(942, '2022-02-02 00:00:00', '138.197.194.139', '2022-02-02 17:56:41', '2022-02-02 17:56:41'),
(943, '2022-02-02 00:00:00', '54.36.148.137', '2022-02-02 18:08:35', '2022-02-02 18:08:35'),
(944, '2022-02-02 00:00:00', '66.249.73.157', '2022-02-02 22:14:35', '2022-02-02 22:14:35'),
(945, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 00:08:48', '2022-02-03 00:08:48'),
(946, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 02:05:03', '2022-02-03 02:05:03'),
(947, '2022-02-03 00:00:00', '66.249.73.157', '2022-02-03 02:05:44', '2022-02-03 02:05:44'),
(948, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 02:19:18', '2022-02-03 02:19:18'),
(949, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 02:26:48', '2022-02-03 02:26:48'),
(950, '2022-02-03 00:00:00', '66.249.73.157', '2022-02-03 02:49:18', '2022-02-03 02:49:18'),
(951, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 03:04:18', '2022-02-03 03:04:18'),
(952, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 03:49:18', '2022-02-03 03:49:18'),
(953, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 04:04:18', '2022-02-03 04:04:18'),
(954, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 04:05:44', '2022-02-03 04:05:44'),
(955, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 04:34:20', '2022-02-03 04:34:20'),
(956, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 05:04:18', '2022-02-03 05:04:18'),
(957, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 05:15:39', '2022-02-03 05:15:39'),
(958, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 05:50:03', '2022-02-03 05:50:03'),
(959, '2022-02-03 00:00:00', '66.249.73.157', '2022-02-03 06:24:18', '2022-02-03 06:24:18'),
(960, '2022-02-03 00:00:00', '106.204.195.87', '2022-02-03 06:28:40', '2022-02-03 06:28:40'),
(961, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 06:45:51', '2022-02-03 06:45:51'),
(962, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 06:54:38', '2022-02-03 06:54:38'),
(963, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 07:04:17', '2022-02-03 07:04:17'),
(964, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 07:04:25', '2022-02-03 07:04:25'),
(965, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 07:24:18', '2022-02-03 07:24:18'),
(966, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 07:35:08', '2022-02-03 07:35:08'),
(967, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 07:39:32', '2022-02-03 07:39:32'),
(968, '2022-02-03 00:00:00', '66.249.73.157', '2022-02-03 07:44:19', '2022-02-03 07:44:19'),
(969, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 07:51:48', '2022-02-03 07:51:48'),
(970, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 07:52:23', '2022-02-03 07:52:23'),
(971, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 08:05:44', '2022-02-03 08:05:44'),
(972, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 08:18:10', '2022-02-03 08:18:10'),
(973, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 08:24:17', '2022-02-03 08:24:17'),
(974, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 08:31:49', '2022-02-03 08:31:49'),
(975, '2022-02-03 00:00:00', '66.249.73.155', '2022-02-03 08:44:17', '2022-02-03 08:44:17'),
(976, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 08:48:39', '2022-02-03 08:48:39'),
(977, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 09:04:25', '2022-02-03 09:04:25'),
(978, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 09:21:20', '2022-02-03 09:21:20'),
(979, '2022-02-03 00:00:00', '66.249.73.157', '2022-02-03 09:24:20', '2022-02-03 09:24:20'),
(980, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 10:03:54', '2022-02-03 10:03:54'),
(981, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 10:24:18', '2022-02-03 10:24:18'),
(982, '2022-02-03 00:00:00', '10.43.252.152', '2022-02-03 10:44:43', '2022-02-03 10:44:43'),
(983, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 10:58:30', '2022-02-03 10:58:30'),
(984, '2022-02-03 00:00:00', '54.36.148.137', '2022-02-03 10:59:33', '2022-02-03 10:59:33'),
(985, '2022-02-03 00:00:00', '223.225.128.120', '2022-02-03 11:01:33', '2022-02-03 11:01:33'),
(986, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 11:11:35', '2022-02-03 11:11:35'),
(987, '2022-02-03 00:00:00', '23.106.219.5', '2022-02-03 11:18:56', '2022-02-03 11:18:56'),
(988, '2022-02-03 00:00:00', '66.249.73.153', '2022-02-03 11:24:17', '2022-02-03 11:24:17'),
(989, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 11:25:59', '2022-02-03 11:25:59'),
(990, '2022-02-03 00:00:00', '52.114.32.28', '2022-02-03 11:36:09', '2022-02-03 11:36:09'),
(991, '2022-02-03 00:00:00', '205.253.125.67', '2022-02-03 11:36:16', '2022-02-03 11:36:16'),
(992, '2022-02-03 00:00:00', '157.39.47.187', '2022-02-03 11:37:47', '2022-02-03 11:37:47'),
(993, '2022-02-03 00:00:00', '157.39.47.187', '2022-02-03 11:49:02', '2022-02-03 11:49:02'),
(994, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 11:51:46', '2022-02-03 11:51:46'),
(995, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 12:03:05', '2022-02-03 12:03:05'),
(996, '2022-02-03 00:00:00', '157.39.47.187', '2022-02-03 12:18:46', '2022-02-03 12:18:46'),
(997, '2022-02-03 00:00:00', '207.46.13.150', '2022-02-03 13:02:00', '2022-02-03 13:02:00'),
(998, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 13:17:06', '2022-02-03 13:17:06'),
(999, '2022-02-03 00:00:00', '23.106.219.111', '2022-02-03 13:26:50', '2022-02-03 13:26:50'),
(1000, '2022-02-03 00:00:00', '114.119.150.126', '2022-02-03 13:36:36', '2022-02-03 13:36:36'),
(1001, '2022-02-03 00:00:00', '49.156.127.232', '2022-02-03 14:09:11', '2022-02-03 14:09:11'),
(1002, '2022-02-03 00:00:00', '223.178.212.167', '2022-02-03 14:56:49', '2022-02-03 14:56:49'),
(1003, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 17:19:45', '2022-02-03 17:19:45'),
(1004, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 17:37:54', '2022-02-03 17:37:54'),
(1005, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 18:04:32', '2022-02-03 18:04:32'),
(1006, '2022-02-03 00:00:00', '205.253.125.17', '2022-02-03 18:06:16', '2022-02-03 18:06:16'),
(1007, '2022-02-03 00:00:00', '205.253.123.251', '2022-02-03 18:07:25', '2022-02-03 18:07:25'),
(1008, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 18:31:10', '2022-02-03 18:31:10'),
(1009, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 18:57:47', '2022-02-03 18:57:47'),
(1010, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 19:24:25', '2022-02-03 19:24:25'),
(1011, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 19:38:27', '2022-02-03 19:38:27'),
(1012, '2022-02-03 00:00:00', '66.249.68.61', '2022-02-03 19:52:38', '2022-02-03 19:52:38'),
(1013, '2022-02-03 00:00:00', '66.249.68.61', '2022-02-03 20:06:45', '2022-02-03 20:06:45'),
(1014, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 20:08:26', '2022-02-03 20:08:26'),
(1015, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 20:20:51', '2022-02-03 20:20:51'),
(1016, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 20:43:54', '2022-02-03 20:43:54'),
(1017, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 20:58:07', '2022-02-03 20:58:07'),
(1018, '2022-02-03 00:00:00', '66.249.68.59', '2022-02-03 21:12:07', '2022-02-03 21:12:07'),
(1019, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 21:42:09', '2022-02-03 21:42:09'),
(1020, '2022-02-03 00:00:00', '195.123.241.30', '2022-02-03 22:04:43', '2022-02-03 22:04:43'),
(1021, '2022-02-03 00:00:00', '66.249.68.57', '2022-02-03 22:12:07', '2022-02-03 22:12:07'),
(1022, '2022-02-03 00:00:00', '66.249.68.61', '2022-02-03 22:42:07', '2022-02-03 22:42:07'),
(1023, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 00:42:06', '2022-02-04 00:42:06'),
(1024, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 00:57:06', '2022-02-04 00:57:06'),
(1025, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 01:12:06', '2022-02-04 01:12:06'),
(1026, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 02:28:11', '2022-02-04 02:28:11'),
(1027, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 03:27:06', '2022-02-04 03:27:06'),
(1028, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 03:39:06', '2022-02-04 03:39:06'),
(1029, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 04:03:06', '2022-02-04 04:03:06'),
(1030, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 04:15:06', '2022-02-04 04:15:06'),
(1031, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 04:30:07', '2022-02-04 04:30:07'),
(1032, '2022-02-04 00:00:00', '117.239.5.129', '2022-02-04 04:30:35', '2022-02-04 04:30:35'),
(1033, '2022-02-04 00:00:00', '157.39.62.229', '2022-02-04 04:57:42', '2022-02-04 04:57:42'),
(1034, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 05:00:06', '2022-02-04 05:00:06'),
(1035, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 05:15:06', '2022-02-04 05:15:06'),
(1036, '2022-02-04 00:00:00', '223.185.130.163', '2022-02-04 05:25:44', '2022-02-04 05:25:44'),
(1037, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 05:45:06', '2022-02-04 05:45:06'),
(1038, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 06:15:06', '2022-02-04 06:15:06'),
(1039, '2022-02-04 00:00:00', '117.239.5.129', '2022-02-04 06:42:03', '2022-02-04 06:42:03'),
(1040, '2022-02-04 00:00:00', '117.239.5.129', '2022-02-04 07:26:48', '2022-02-04 07:26:48'),
(1041, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 08:11:49', '2022-02-04 08:11:49'),
(1042, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 08:31:49', '2022-02-04 08:31:49'),
(1043, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 08:51:49', '2022-02-04 08:51:49'),
(1044, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 09:01:49', '2022-02-04 09:01:49'),
(1045, '2022-02-04 00:00:00', '223.178.209.155', '2022-02-04 09:07:08', '2022-02-04 09:07:08'),
(1046, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 09:41:50', '2022-02-04 09:41:50'),
(1047, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 10:01:50', '2022-02-04 10:01:50'),
(1048, '2022-02-04 00:00:00', '66.159.202.134', '2022-02-04 10:08:20', '2022-02-04 10:08:20'),
(1049, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 10:16:45', '2022-02-04 10:16:45'),
(1050, '2022-02-04 00:00:00', '66.159.202.134', '2022-02-04 10:19:35', '2022-02-04 10:19:35'),
(1051, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 10:46:46', '2022-02-04 10:46:46'),
(1052, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 11:01:48', '2022-02-04 11:01:48'),
(1053, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 12:24:47', '2022-02-04 12:24:47'),
(1054, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 12:24:49', '2022-02-04 12:24:49'),
(1055, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 12:49:46', '2022-02-04 12:49:46'),
(1056, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 13:21:48', '2022-02-04 13:21:48'),
(1057, '2022-02-04 00:00:00', '66.249.68.61', '2022-02-04 13:24:46', '2022-02-04 13:24:46'),
(1058, '2022-02-04 00:00:00', '171.78.247.231', '2022-02-04 13:27:04', '2022-02-04 13:27:04'),
(1059, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 13:39:47', '2022-02-04 13:39:47'),
(1060, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 14:28:50', '2022-02-04 14:28:50'),
(1061, '2022-02-04 00:00:00', '49.156.127.89', '2022-02-04 14:43:05', '2022-02-04 14:43:05'),
(1062, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 16:02:58', '2022-02-04 16:02:58'),
(1063, '2022-02-04 00:00:00', '66.249.68.57', '2022-02-04 16:49:59', '2022-02-04 16:49:59'),
(1064, '2022-02-04 00:00:00', '114.119.150.120', '2022-02-04 17:10:33', '2022-02-04 17:10:33'),
(1065, '2022-02-04 00:00:00', '66.249.68.59', '2022-02-04 17:49:56', '2022-02-04 17:49:56'),
(1066, '2022-02-04 00:00:00', '132.154.188.214', '2022-02-04 17:52:14', '2022-02-04 17:52:14'),
(1067, '2022-02-04 00:00:00', '162.215.209.208', '2022-02-04 20:42:14', '2022-02-04 20:42:14'),
(1068, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 01:09:56', '2022-02-05 01:09:56'),
(1069, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 02:09:56', '2022-02-05 02:09:56'),
(1070, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 03:29:56', '2022-02-05 03:29:56'),
(1071, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 03:41:18', '2022-02-05 03:41:18'),
(1072, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 03:59:56', '2022-02-05 03:59:56'),
(1073, '2022-02-05 00:00:00', '40.77.167.2', '2022-02-05 04:30:39', '2022-02-05 04:30:39'),
(1074, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 04:32:53', '2022-02-05 04:32:53'),
(1075, '2022-02-05 00:00:00', '114.31.139.57', '2022-02-05 05:11:23', '2022-02-05 05:11:23'),
(1076, '2022-02-05 00:00:00', '114.31.139.135', '2022-02-05 05:17:11', '2022-02-05 05:17:11'),
(1077, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 05:29:56', '2022-02-05 05:29:56'),
(1078, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 05:49:56', '2022-02-05 05:49:56'),
(1079, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 06:29:56', '2022-02-05 06:29:56'),
(1080, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 06:59:56', '2022-02-05 06:59:56'),
(1081, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 07:29:57', '2022-02-05 07:29:57'),
(1082, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 07:53:03', '2022-02-05 07:53:03'),
(1083, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 07:59:56', '2022-02-05 07:59:56'),
(1084, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 08:38:47', '2022-02-05 08:38:47'),
(1085, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 08:59:57', '2022-02-05 08:59:57'),
(1086, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 09:11:22', '2022-02-05 09:11:22'),
(1087, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 09:23:32', '2022-02-05 09:23:32'),
(1088, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 09:59:56', '2022-02-05 09:59:56'),
(1089, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 10:29:03', '2022-02-05 10:29:03'),
(1090, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 10:44:56', '2022-02-05 10:44:56'),
(1091, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 10:59:00', '2022-02-05 10:59:00'),
(1092, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 10:59:03', '2022-02-05 10:59:03'),
(1093, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 10:59:20', '2022-02-05 10:59:20'),
(1094, '2022-02-05 00:00:00', '106.204.207.105', '2022-02-05 11:10:01', '2022-02-05 11:10:01'),
(1095, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 11:14:58', '2022-02-05 11:14:58'),
(1096, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 11:29:01', '2022-02-05 11:29:01'),
(1097, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 11:29:02', '2022-02-05 11:29:02'),
(1098, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 11:29:56', '2022-02-05 11:29:56'),
(1099, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 12:10:49', '2022-02-05 12:10:49'),
(1100, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 12:25:59', '2022-02-05 12:25:59'),
(1101, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 12:39:41', '2022-02-05 12:39:41');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(1102, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 12:59:00', '2022-02-05 12:59:00'),
(1103, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 13:07:27', '2022-02-05 13:07:27'),
(1104, '2022-02-05 00:00:00', '213.180.203.82', '2022-02-05 13:18:06', '2022-02-05 13:18:06'),
(1105, '2022-02-05 00:00:00', '117.220.76.182', '2022-02-05 13:21:38', '2022-02-05 13:21:38'),
(1106, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 13:22:25', '2022-02-05 13:22:25'),
(1107, '2022-02-05 00:00:00', '66.249.68.61', '2022-02-05 13:29:01', '2022-02-05 13:29:01'),
(1108, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 13:32:46', '2022-02-05 13:32:46'),
(1109, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 14:15:55', '2022-02-05 14:15:55'),
(1110, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 14:46:02', '2022-02-05 14:46:02'),
(1111, '2022-02-05 00:00:00', '49.156.79.159', '2022-02-05 15:00:00', '2022-02-05 15:00:00'),
(1112, '2022-02-05 00:00:00', '117.222.251.89', '2022-02-05 15:05:15', '2022-02-05 15:05:15'),
(1113, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 16:15:55', '2022-02-05 16:15:55'),
(1114, '2022-02-05 00:00:00', '49.36.224.161', '2022-02-05 16:23:22', '2022-02-05 16:23:22'),
(1115, '2022-02-05 00:00:00', '180.188.249.221', '2022-02-05 17:22:39', '2022-02-05 17:22:39'),
(1116, '2022-02-05 00:00:00', '66.249.68.57', '2022-02-05 17:53:26', '2022-02-05 17:53:26'),
(1117, '2022-02-05 00:00:00', '66.249.68.59', '2022-02-05 21:45:59', '2022-02-05 21:45:59'),
(1118, '2022-02-05 00:00:00', '114.119.151.151', '2022-02-05 22:47:21', '2022-02-05 22:47:21'),
(1119, '2022-02-05 00:00:00', '54.36.148.137', '2022-02-05 23:17:27', '2022-02-05 23:17:27'),
(1120, '2022-02-06 00:00:00', '66.249.68.61', '2022-02-06 00:16:00', '2022-02-06 00:16:00'),
(1121, '2022-02-06 00:00:00', '157.55.39.28', '2022-02-06 01:03:50', '2022-02-06 01:03:50'),
(1122, '2022-02-06 00:00:00', '132.154.138.197', '2022-02-06 01:57:23', '2022-02-06 01:57:23'),
(1123, '2022-02-06 00:00:00', '180.188.249.221', '2022-02-06 04:10:40', '2022-02-06 04:10:40'),
(1124, '2022-02-06 00:00:00', '37.0.15.229', '2022-02-06 04:56:50', '2022-02-06 04:56:50'),
(1125, '2022-02-06 00:00:00', '66.249.68.59', '2022-02-06 05:05:14', '2022-02-06 05:05:14'),
(1126, '2022-02-06 00:00:00', '66.249.68.57', '2022-02-06 05:35:14', '2022-02-06 05:35:14'),
(1127, '2022-02-06 00:00:00', '66.249.68.61', '2022-02-06 05:35:15', '2022-02-06 05:35:15'),
(1128, '2022-02-06 00:00:00', '66.249.68.57', '2022-02-06 07:32:16', '2022-02-06 07:32:16'),
(1129, '2022-02-06 00:00:00', '66.249.68.57', '2022-02-06 08:46:00', '2022-02-06 08:46:00'),
(1130, '2022-02-06 00:00:00', '66.249.68.59', '2022-02-06 09:46:00', '2022-02-06 09:46:00'),
(1131, '2022-02-06 00:00:00', '223.130.31.85', '2022-02-06 10:42:13', '2022-02-06 10:42:13'),
(1132, '2022-02-06 00:00:00', '223.130.31.85', '2022-02-06 10:55:58', '2022-02-06 10:55:58'),
(1133, '2022-02-06 00:00:00', '66.249.64.57', '2022-02-06 11:01:13', '2022-02-06 11:01:13'),
(1134, '2022-02-06 00:00:00', '223.130.31.85', '2022-02-06 11:13:08', '2022-02-06 11:13:08'),
(1135, '2022-02-06 00:00:00', '180.188.249.221', '2022-02-06 11:24:07', '2022-02-06 11:24:07'),
(1136, '2022-02-06 00:00:00', '66.249.64.57', '2022-02-06 11:31:18', '2022-02-06 11:31:18'),
(1137, '2022-02-06 00:00:00', '114.119.144.85', '2022-02-06 12:17:00', '2022-02-06 12:17:00'),
(1138, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 13:57:30', '2022-02-06 13:57:30'),
(1139, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 14:16:24', '2022-02-06 14:16:24'),
(1140, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 16:00:58', '2022-02-06 16:00:58'),
(1141, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 16:15:18', '2022-02-06 16:15:18'),
(1142, '2022-02-06 00:00:00', '66.249.64.57', '2022-02-06 16:29:38', '2022-02-06 16:29:38'),
(1143, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 16:58:19', '2022-02-06 16:58:19'),
(1144, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 17:41:16', '2022-02-06 17:41:16'),
(1145, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 17:55:35', '2022-02-06 17:55:35'),
(1146, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 18:38:34', '2022-02-06 18:38:34'),
(1147, '2022-02-06 00:00:00', '66.249.64.57', '2022-02-06 18:52:58', '2022-02-06 18:52:58'),
(1148, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 19:28:43', '2022-02-06 19:28:43'),
(1149, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 19:46:02', '2022-02-06 19:46:02'),
(1150, '2022-02-06 00:00:00', '66.249.64.57', '2022-02-06 21:12:32', '2022-02-06 21:12:32'),
(1151, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 21:42:32', '2022-02-06 21:42:32'),
(1152, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 21:42:42', '2022-02-06 21:42:42'),
(1153, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 22:12:34', '2022-02-06 22:12:34'),
(1154, '2022-02-06 00:00:00', '207.46.13.7', '2022-02-06 23:09:30', '2022-02-06 23:09:30'),
(1155, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 23:10:19', '2022-02-06 23:10:19'),
(1156, '2022-02-06 00:00:00', '66.249.64.59', '2022-02-06 23:12:31', '2022-02-06 23:12:31'),
(1157, '2022-02-06 00:00:00', '66.249.64.61', '2022-02-06 23:25:19', '2022-02-06 23:25:19'),
(1158, '2022-02-07 00:00:00', '66.249.64.57', '2022-02-07 00:10:21', '2022-02-07 00:10:21'),
(1159, '2022-02-07 00:00:00', '66.249.64.59', '2022-02-07 00:25:19', '2022-02-07 00:25:19'),
(1160, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 01:10:19', '2022-02-07 01:10:19'),
(1161, '2022-02-07 00:00:00', '114.119.150.16', '2022-02-07 01:24:13', '2022-02-07 01:24:13'),
(1162, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 01:32:06', '2022-02-07 01:32:06'),
(1163, '2022-02-07 00:00:00', '66.249.64.57', '2022-02-07 01:32:07', '2022-02-07 01:32:07'),
(1164, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 01:46:19', '2022-02-07 01:46:19'),
(1165, '2022-02-07 00:00:00', '117.239.5.129', '2022-02-07 03:37:50', '2022-02-07 03:37:50'),
(1166, '2022-02-07 00:00:00', '66.249.64.57', '2022-02-07 04:20:26', '2022-02-07 04:20:26'),
(1167, '2022-02-07 00:00:00', '117.239.5.129', '2022-02-07 04:58:06', '2022-02-07 04:58:06'),
(1168, '2022-02-07 00:00:00', '66.249.64.59', '2022-02-07 05:16:08', '2022-02-07 05:16:08'),
(1169, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 05:20:26', '2022-02-07 05:20:26'),
(1170, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 05:46:05', '2022-02-07 05:46:05'),
(1171, '2022-02-07 00:00:00', '223.187.105.154', '2022-02-07 05:49:27', '2022-02-07 05:49:27'),
(1172, '2022-02-07 00:00:00', '207.46.13.7', '2022-02-07 06:02:17', '2022-02-07 06:02:17'),
(1173, '2022-02-07 00:00:00', '157.55.39.126', '2022-02-07 06:02:30', '2022-02-07 06:02:30'),
(1174, '2022-02-07 00:00:00', '157.55.39.187', '2022-02-07 06:02:30', '2022-02-07 06:02:30'),
(1175, '2022-02-07 00:00:00', '157.55.39.28', '2022-02-07 06:02:32', '2022-02-07 06:02:32'),
(1176, '2022-02-07 00:00:00', '40.77.167.2', '2022-02-07 06:02:34', '2022-02-07 06:02:34'),
(1177, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 06:16:04', '2022-02-07 06:16:04'),
(1178, '2022-02-07 00:00:00', '157.55.39.187', '2022-02-07 06:17:53', '2022-02-07 06:17:53'),
(1179, '2022-02-07 00:00:00', '117.239.5.129', '2022-02-07 06:22:45', '2022-02-07 06:22:45'),
(1180, '2022-02-07 00:00:00', '223.187.105.154', '2022-02-07 06:33:32', '2022-02-07 06:33:32'),
(1181, '2022-02-07 00:00:00', '117.239.5.129', '2022-02-07 06:36:16', '2022-02-07 06:36:16'),
(1182, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 09:00:14', '2022-02-07 09:00:14'),
(1183, '2022-02-07 00:00:00', '66.249.64.57', '2022-02-07 09:46:06', '2022-02-07 09:46:06'),
(1184, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 10:00:11', '2022-02-07 10:00:11'),
(1185, '2022-02-07 00:00:00', '207.46.13.150', '2022-02-07 10:16:27', '2022-02-07 10:16:27'),
(1186, '2022-02-07 00:00:00', '66.249.64.61', '2022-02-07 10:46:05', '2022-02-07 10:46:05'),
(1187, '2022-02-07 00:00:00', '202.173.126.165', '2022-02-07 10:46:41', '2022-02-07 10:46:41'),
(1188, '2022-02-07 00:00:00', '117.239.5.129', '2022-02-07 10:54:50', '2022-02-07 10:54:50'),
(1189, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 12:16:06', '2022-02-07 12:16:06'),
(1190, '2022-02-07 00:00:00', '157.55.39.187', '2022-02-07 12:46:39', '2022-02-07 12:46:39'),
(1191, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 13:27:06', '2022-02-07 13:27:06'),
(1192, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 13:35:40', '2022-02-07 13:35:40'),
(1193, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 13:39:57', '2022-02-07 13:39:57'),
(1194, '2022-02-07 00:00:00', '66.249.68.61', '2022-02-07 13:48:31', '2022-02-07 13:48:31'),
(1195, '2022-02-07 00:00:00', '223.185.32.92', '2022-02-07 13:50:50', '2022-02-07 13:50:50'),
(1196, '2022-02-07 00:00:00', '124.253.113.25', '2022-02-07 14:09:37', '2022-02-07 14:09:37'),
(1197, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 14:09:57', '2022-02-07 14:09:57'),
(1198, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 14:27:06', '2022-02-07 14:27:06'),
(1199, '2022-02-07 00:00:00', '66.249.68.61', '2022-02-07 14:27:07', '2022-02-07 14:27:07'),
(1200, '2022-02-07 00:00:00', '124.253.113.25', '2022-02-07 14:42:45', '2022-02-07 14:42:45'),
(1201, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 14:44:14', '2022-02-07 14:44:14'),
(1202, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 14:52:48', '2022-02-07 14:52:48'),
(1203, '2022-02-07 00:00:00', '124.253.113.25', '2022-02-07 15:00:16', '2022-02-07 15:00:16'),
(1204, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 15:09:57', '2022-02-07 15:09:57'),
(1205, '2022-02-07 00:00:00', '157.55.39.28', '2022-02-07 15:15:26', '2022-02-07 15:15:26'),
(1206, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 15:39:41', '2022-02-07 15:39:41'),
(1207, '2022-02-07 00:00:00', '66.249.68.61', '2022-02-07 15:59:40', '2022-02-07 15:59:40'),
(1208, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 16:05:04', '2022-02-07 16:05:04'),
(1209, '2022-02-07 00:00:00', '66.249.68.61', '2022-02-07 16:11:40', '2022-02-07 16:11:40'),
(1210, '2022-02-07 00:00:00', '66.249.68.61', '2022-02-07 16:23:40', '2022-02-07 16:23:40'),
(1211, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 16:35:40', '2022-02-07 16:35:40'),
(1212, '2022-02-07 00:00:00', '66.249.68.63', '2022-02-07 17:23:40', '2022-02-07 17:23:40'),
(1213, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 17:35:40', '2022-02-07 17:35:40'),
(1214, '2022-02-07 00:00:00', '223.178.219.212', '2022-02-07 18:52:48', '2022-02-07 18:52:48'),
(1215, '2022-02-07 00:00:00', '66.249.68.57', '2022-02-07 21:09:19', '2022-02-07 21:09:19'),
(1216, '2022-02-07 00:00:00', '114.119.142.210', '2022-02-07 21:26:00', '2022-02-07 21:26:00'),
(1217, '2022-02-07 00:00:00', '54.36.148.137', '2022-02-07 21:31:32', '2022-02-07 21:31:32'),
(1218, '2022-02-07 00:00:00', '54.36.148.137', '2022-02-07 22:30:04', '2022-02-07 22:30:04'),
(1219, '2022-02-07 00:00:00', '66.249.68.59', '2022-02-07 23:08:09', '2022-02-07 23:08:09'),
(1220, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 00:08:11', '2022-02-08 00:08:11'),
(1221, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 01:08:09', '2022-02-08 01:08:09'),
(1222, '2022-02-08 00:00:00', '92.118.160.61', '2022-02-08 01:28:53', '2022-02-08 01:28:53'),
(1223, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 03:21:48', '2022-02-08 03:21:48'),
(1224, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 03:30:09', '2022-02-08 03:30:09'),
(1225, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 03:41:18', '2022-02-08 03:41:18'),
(1226, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 03:55:11', '2022-02-08 03:55:11'),
(1227, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 04:11:53', '2022-02-08 04:11:53'),
(1228, '2022-02-08 00:00:00', '203.134.197.14', '2022-02-08 04:21:14', '2022-02-08 04:21:14'),
(1229, '2022-02-08 00:00:00', '223.187.96.47', '2022-02-08 04:21:21', '2022-02-08 04:21:21'),
(1230, '2022-02-08 00:00:00', '203.134.197.14', '2022-02-08 04:45:26', '2022-02-08 04:45:26'),
(1231, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 04:53:37', '2022-02-08 04:53:37'),
(1232, '2022-02-08 00:00:00', '203.134.197.14', '2022-02-08 05:03:03', '2022-02-08 05:03:03'),
(1233, '2022-02-08 00:00:00', '162.215.209.208', '2022-02-08 05:17:28', '2022-02-08 05:17:28'),
(1234, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 05:19:22', '2022-02-08 05:19:22'),
(1235, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 05:53:43', '2022-02-08 05:53:43'),
(1236, '2022-02-08 00:00:00', '203.134.197.14', '2022-02-08 05:55:00', '2022-02-08 05:55:00'),
(1237, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 06:02:18', '2022-02-08 06:02:18'),
(1238, '2022-02-08 00:00:00', '157.55.39.213', '2022-02-08 06:07:01', '2022-02-08 06:07:01'),
(1239, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 06:28:04', '2022-02-08 06:28:04'),
(1240, '2022-02-08 00:00:00', '117.239.5.129', '2022-02-08 06:32:26', '2022-02-08 06:32:26'),
(1241, '2022-02-08 00:00:00', '162.215.209.208', '2022-02-08 06:35:41', '2022-02-08 06:35:41'),
(1242, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 06:36:39', '2022-02-08 06:36:39'),
(1243, '2022-02-08 00:00:00', '157.39.199.113', '2022-02-08 06:52:36', '2022-02-08 06:52:36'),
(1244, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 06:53:49', '2022-02-08 06:53:49'),
(1245, '2022-02-08 00:00:00', '117.239.5.129', '2022-02-08 06:59:18', '2022-02-08 06:59:18'),
(1246, '2022-02-08 00:00:00', '157.39.29.79', '2022-02-08 07:06:40', '2022-02-08 07:06:40'),
(1247, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 07:10:59', '2022-02-08 07:10:59'),
(1248, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 07:19:34', '2022-02-08 07:19:34'),
(1249, '2022-02-08 00:00:00', '122.173.29.134', '2022-02-08 07:21:05', '2022-02-08 07:21:05'),
(1250, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 07:28:11', '2022-02-08 07:28:11'),
(1251, '2022-02-08 00:00:00', '117.239.5.129', '2022-02-08 07:31:48', '2022-02-08 07:31:48'),
(1252, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 07:36:45', '2022-02-08 07:36:45'),
(1253, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 07:45:21', '2022-02-08 07:45:21'),
(1254, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 08:02:30', '2022-02-08 08:02:30'),
(1255, '2022-02-08 00:00:00', '157.55.39.41', '2022-02-08 08:03:34', '2022-02-08 08:03:34'),
(1256, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 08:11:10', '2022-02-08 08:11:10'),
(1257, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 08:28:18', '2022-02-08 08:28:18'),
(1258, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 08:36:51', '2022-02-08 08:36:51'),
(1259, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 09:20:30', '2022-02-08 09:20:30'),
(1260, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 09:28:22', '2022-02-08 09:28:22'),
(1261, '2022-02-08 00:00:00', '114.119.146.29', '2022-02-08 09:36:51', '2022-02-08 09:36:51'),
(1262, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 09:45:33', '2022-02-08 09:45:33'),
(1263, '2022-02-08 00:00:00', '207.46.13.138', '2022-02-08 10:03:10', '2022-02-08 10:03:10'),
(1264, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 10:16:13', '2022-02-08 10:16:13'),
(1265, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 10:16:14', '2022-02-08 10:16:14'),
(1266, '2022-02-08 00:00:00', '203.134.197.14', '2022-02-08 10:20:31', '2022-02-08 10:20:31'),
(1267, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 10:37:41', '2022-02-08 10:37:41'),
(1268, '2022-02-08 00:00:00', '117.239.5.129', '2022-02-08 10:43:07', '2022-02-08 10:43:07'),
(1269, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 10:45:38', '2022-02-08 10:45:38'),
(1270, '2022-02-08 00:00:00', '117.239.5.129', '2022-02-08 10:56:39', '2022-02-08 10:56:39'),
(1271, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 11:11:24', '2022-02-08 11:11:24'),
(1272, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 11:19:59', '2022-02-08 11:19:59'),
(1273, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 12:02:55', '2022-02-08 12:02:55'),
(1274, '2022-02-08 00:00:00', '119.160.56.173', '2022-02-08 12:14:50', '2022-02-08 12:14:50'),
(1275, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 12:20:05', '2022-02-08 12:20:05'),
(1276, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 12:54:26', '2022-02-08 12:54:26'),
(1277, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 13:52:27', '2022-02-08 13:52:27'),
(1278, '2022-02-08 00:00:00', '122.173.29.134', '2022-02-08 14:15:30', '2022-02-08 14:15:30'),
(1279, '2022-02-08 00:00:00', '66.249.68.59', '2022-02-08 15:54:25', '2022-02-08 15:54:25'),
(1280, '2022-02-08 00:00:00', '183.87.33.46', '2022-02-08 17:02:50', '2022-02-08 17:02:50'),
(1281, '2022-02-08 00:00:00', '103.40.198.44', '2022-02-08 17:04:37', '2022-02-08 17:04:37'),
(1282, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 17:39:25', '2022-02-08 17:39:25'),
(1283, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 17:41:33', '2022-02-08 17:41:33'),
(1284, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 18:09:25', '2022-02-08 18:09:25'),
(1285, '2022-02-08 00:00:00', '114.119.141.206', '2022-02-08 18:40:47', '2022-02-08 18:40:47'),
(1286, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 18:41:33', '2022-02-08 18:41:33'),
(1287, '2022-02-08 00:00:00', '66.249.68.61', '2022-02-08 18:54:25', '2022-02-08 18:54:25'),
(1288, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 22:00:26', '2022-02-08 22:00:26'),
(1289, '2022-02-08 00:00:00', '54.36.148.137', '2022-02-08 22:41:33', '2022-02-08 22:41:33'),
(1290, '2022-02-08 00:00:00', '66.249.68.57', '2022-02-08 23:48:15', '2022-02-08 23:48:15'),
(1291, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 00:18:18', '2022-02-09 00:18:18'),
(1292, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 00:48:16', '2022-02-09 00:48:16'),
(1293, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 01:12:18', '2022-02-09 01:12:18'),
(1294, '2022-02-09 00:00:00', '40.77.167.13', '2022-02-09 02:02:40', '2022-02-09 02:02:40'),
(1295, '2022-02-09 00:00:00', '114.119.141.146', '2022-02-09 02:54:36', '2022-02-09 02:54:36'),
(1296, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 04:04:25', '2022-02-09 04:04:25'),
(1297, '2022-02-09 00:00:00', '117.239.5.129', '2022-02-09 04:40:15', '2022-02-09 04:40:15'),
(1298, '2022-02-09 00:00:00', '154.54.249.195', '2022-02-09 05:36:02', '2022-02-09 05:36:02'),
(1299, '2022-02-09 00:00:00', '117.239.5.129', '2022-02-09 06:03:19', '2022-02-09 06:03:19'),
(1300, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 06:04:53', '2022-02-09 06:04:53'),
(1301, '2022-02-09 00:00:00', '49.36.205.101', '2022-02-09 06:07:38', '2022-02-09 06:07:38'),
(1302, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 06:17:10', '2022-02-09 06:17:10'),
(1303, '2022-02-09 00:00:00', '54.36.148.137', '2022-02-09 06:26:56', '2022-02-09 06:26:56'),
(1304, '2022-02-09 00:00:00', '117.239.5.129', '2022-02-09 06:26:57', '2022-02-09 06:26:57'),
(1305, '2022-02-09 00:00:00', '115.42.79.130', '2022-02-09 06:35:09', '2022-02-09 06:35:09'),
(1306, '2022-02-09 00:00:00', '223.178.208.153', '2022-02-09 06:38:39', '2022-02-09 06:38:39'),
(1307, '2022-02-09 00:00:00', '117.239.5.129', '2022-02-09 06:39:27', '2022-02-09 06:39:27'),
(1308, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 06:42:02', '2022-02-09 06:42:02'),
(1309, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 06:54:25', '2022-02-09 06:54:25'),
(1310, '2022-02-09 00:00:00', '106.196.81.73', '2022-02-09 07:08:10', '2022-02-09 07:08:10'),
(1311, '2022-02-09 00:00:00', '223.225.135.76', '2022-02-09 07:12:04', '2022-02-09 07:12:04'),
(1312, '2022-02-09 00:00:00', '117.239.5.129', '2022-02-09 07:18:33', '2022-02-09 07:18:33'),
(1313, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 07:38:28', '2022-02-09 07:38:28'),
(1314, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 07:54:25', '2022-02-09 07:54:25'),
(1315, '2022-02-09 00:00:00', '132.154.180.241', '2022-02-09 07:54:32', '2022-02-09 07:54:32'),
(1316, '2022-02-09 00:00:00', '132.154.180.241', '2022-02-09 08:06:04', '2022-02-09 08:06:04'),
(1317, '2022-02-09 00:00:00', '132.154.180.241', '2022-02-09 08:27:46', '2022-02-09 08:27:46'),
(1318, '2022-02-09 00:00:00', '103.40.198.44', '2022-02-09 08:36:21', '2022-02-09 08:36:21'),
(1319, '2022-02-09 00:00:00', '132.154.180.241', '2022-02-09 08:38:46', '2022-02-09 08:38:46'),
(1320, '2022-02-09 00:00:00', '103.40.198.44', '2022-02-09 08:49:14', '2022-02-09 08:49:14'),
(1321, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 08:50:11', '2022-02-09 08:50:11'),
(1322, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 09:02:15', '2022-02-09 09:02:15'),
(1323, '2022-02-09 00:00:00', '132.154.180.241', '2022-02-09 09:03:20', '2022-02-09 09:03:20'),
(1324, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 09:15:23', '2022-02-09 09:15:23'),
(1325, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 09:53:41', '2022-02-09 09:53:41'),
(1326, '2022-02-09 00:00:00', '117.239.5.129', '2022-02-09 10:49:15', '2022-02-09 10:49:15'),
(1327, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 11:48:38', '2022-02-09 11:48:38'),
(1328, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 12:18:08', '2022-02-09 12:18:08'),
(1329, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 12:24:24', '2022-02-09 12:24:24'),
(1330, '2022-02-09 00:00:00', '49.156.89.19', '2022-02-09 13:34:04', '2022-02-09 13:34:04'),
(1331, '2022-02-09 00:00:00', '124.253.209.42', '2022-02-09 13:36:37', '2022-02-09 13:36:37'),
(1332, '2022-02-09 00:00:00', '124.253.209.42', '2022-02-09 13:59:37', '2022-02-09 13:59:37'),
(1333, '2022-02-09 00:00:00', '52.114.32.28', '2022-02-09 14:00:21', '2022-02-09 14:00:21'),
(1334, '2022-02-09 00:00:00', '157.39.130.17', '2022-02-09 14:01:02', '2022-02-09 14:01:02'),
(1335, '2022-02-09 00:00:00', '157.39.130.17', '2022-02-09 14:52:27', '2022-02-09 14:52:27'),
(1336, '2022-02-09 00:00:00', '124.253.209.42', '2022-02-09 14:58:15', '2022-02-09 14:58:15'),
(1337, '2022-02-09 00:00:00', '40.77.167.13', '2022-02-09 15:00:43', '2022-02-09 15:00:43'),
(1338, '2022-02-09 00:00:00', '49.36.231.246', '2022-02-09 15:15:02', '2022-02-09 15:15:02'),
(1339, '2022-02-09 00:00:00', '66.102.9.59', '2022-02-09 15:19:11', '2022-02-09 15:19:11'),
(1340, '2022-02-09 00:00:00', '66.102.9.57', '2022-02-09 15:19:12', '2022-02-09 15:19:12'),
(1341, '2022-02-09 00:00:00', '40.77.167.13', '2022-02-09 15:21:33', '2022-02-09 15:21:33'),
(1342, '2022-02-09 00:00:00', '223.178.208.153', '2022-02-09 15:31:06', '2022-02-09 15:31:06'),
(1343, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 15:40:42', '2022-02-09 15:40:42'),
(1344, '2022-02-09 00:00:00', '40.77.167.13', '2022-02-09 15:47:37', '2022-02-09 15:47:37'),
(1345, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 16:01:27', '2022-02-09 16:01:27'),
(1346, '2022-02-09 00:00:00', '66.249.68.61', '2022-02-09 16:14:57', '2022-02-09 16:14:57'),
(1347, '2022-02-09 00:00:00', '223.178.208.153', '2022-02-09 16:23:11', '2022-02-09 16:23:11'),
(1348, '2022-02-09 00:00:00', '66.249.68.61', '2022-02-09 16:50:24', '2022-02-09 16:50:24'),
(1349, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 17:01:18', '2022-02-09 17:01:18'),
(1350, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 17:17:40', '2022-02-09 17:17:40'),
(1351, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 17:28:35', '2022-02-09 17:28:35'),
(1352, '2022-02-09 00:00:00', '66.249.68.61', '2022-02-09 17:34:02', '2022-02-09 17:34:02'),
(1353, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 17:50:24', '2022-02-09 17:50:24'),
(1354, '2022-02-09 00:00:00', '66.249.68.61', '2022-02-09 17:56:01', '2022-02-09 17:56:01'),
(1355, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 18:10:39', '2022-02-09 18:10:39'),
(1356, '2022-02-09 00:00:00', '66.249.68.61', '2022-02-09 19:15:41', '2022-02-09 19:15:41'),
(1357, '2022-02-09 00:00:00', '66.249.68.59', '2022-02-09 19:25:51', '2022-02-09 19:25:51'),
(1358, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 19:55:51', '2022-02-09 19:55:51'),
(1359, '2022-02-09 00:00:00', '66.249.68.57', '2022-02-09 21:28:28', '2022-02-09 21:28:28'),
(1360, '2022-02-09 00:00:00', '114.119.146.16', '2022-02-09 21:38:46', '2022-02-09 21:38:46'),
(1361, '2022-02-09 00:00:00', '114.119.151.14', '2022-02-09 21:49:55', '2022-02-09 21:49:55'),
(1362, '2022-02-09 00:00:00', '114.119.149.116', '2022-02-09 22:14:20', '2022-02-09 22:14:20'),
(1363, '2022-02-09 00:00:00', '66.249.68.61', '2022-02-09 23:55:46', '2022-02-09 23:55:46'),
(1364, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 01:17:37', '2022-02-10 01:17:37'),
(1365, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 02:45:33', '2022-02-10 02:45:33'),
(1366, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 02:45:40', '2022-02-10 02:45:40'),
(1367, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 03:15:50', '2022-02-10 03:15:50'),
(1368, '2022-02-10 00:00:00', '114.119.145.8', '2022-02-10 03:28:00', '2022-02-10 03:28:00'),
(1369, '2022-02-10 00:00:00', '117.239.5.129', '2022-02-10 03:47:23', '2022-02-10 03:47:23'),
(1370, '2022-02-10 00:00:00', '117.239.5.129', '2022-02-10 04:05:22', '2022-02-10 04:05:22'),
(1371, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 04:05:54', '2022-02-10 04:05:54'),
(1372, '2022-02-10 00:00:00', '117.239.5.129', '2022-02-10 04:31:34', '2022-02-10 04:31:34'),
(1373, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 04:35:54', '2022-02-10 04:35:54'),
(1374, '2022-02-10 00:00:00', '117.239.5.129', '2022-02-10 04:43:41', '2022-02-10 04:43:41'),
(1375, '2022-02-10 00:00:00', '117.239.5.129', '2022-02-10 04:56:28', '2022-02-10 04:56:28'),
(1376, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 05:13:22', '2022-02-10 05:13:22'),
(1377, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 05:31:29', '2022-02-10 05:31:29'),
(1378, '2022-02-10 00:00:00', '157.39.251.28', '2022-02-10 05:38:02', '2022-02-10 05:38:02'),
(1379, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 05:43:29', '2022-02-10 05:43:29'),
(1380, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 05:55:29', '2022-02-10 05:55:29'),
(1381, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 06:07:29', '2022-02-10 06:07:29'),
(1382, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 06:26:07', '2022-02-10 06:26:07'),
(1383, '2022-02-10 00:00:00', '124.253.209.42', '2022-02-10 06:51:11', '2022-02-10 06:51:11'),
(1384, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 06:55:29', '2022-02-10 06:55:29'),
(1385, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 07:19:29', '2022-02-10 07:19:29'),
(1386, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 07:43:29', '2022-02-10 07:43:29'),
(1387, '2022-02-10 00:00:00', '202.14.121.190', '2022-02-10 07:47:50', '2022-02-10 07:47:50'),
(1388, '2022-02-10 00:00:00', '223.225.171.169', '2022-02-10 07:52:51', '2022-02-10 07:52:51'),
(1389, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 07:55:29', '2022-02-10 07:55:29'),
(1390, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 08:07:30', '2022-02-10 08:07:30'),
(1391, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 08:18:04', '2022-02-10 08:18:04'),
(1392, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 08:32:46', '2022-02-10 08:32:46'),
(1393, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 08:45:37', '2022-02-10 08:45:37'),
(1394, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 09:33:12', '2022-02-10 09:33:12'),
(1395, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 09:50:20', '2022-02-10 09:50:20'),
(1396, '2022-02-10 00:00:00', '106.220.102.168', '2022-02-10 09:56:16', '2022-02-10 09:56:16'),
(1397, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 10:16:03', '2022-02-10 10:16:03'),
(1398, '2022-02-10 00:00:00', '66.249.75.223', '2022-02-10 10:24:37', '2022-02-10 10:24:37'),
(1399, '2022-02-10 00:00:00', '124.253.209.42', '2022-02-10 10:27:34', '2022-02-10 10:27:34'),
(1400, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 10:33:11', '2022-02-10 10:33:11'),
(1401, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 11:07:28', '2022-02-10 11:07:28'),
(1402, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 11:24:38', '2022-02-10 11:24:38'),
(1403, '2022-02-10 00:00:00', '114.119.145.212', '2022-02-10 11:48:03', '2022-02-10 11:48:03'),
(1404, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 12:24:37', '2022-02-10 12:24:37'),
(1405, '2022-02-10 00:00:00', '114.119.145.198', '2022-02-10 12:26:20', '2022-02-10 12:26:20'),
(1406, '2022-02-10 00:00:00', '114.119.142.20', '2022-02-10 12:42:33', '2022-02-10 12:42:33'),
(1407, '2022-02-10 00:00:00', '124.253.209.42', '2022-02-10 12:47:52', '2022-02-10 12:47:52'),
(1408, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 12:54:37', '2022-02-10 12:54:37'),
(1409, '2022-02-10 00:00:00', '114.119.147.87', '2022-02-10 13:17:37', '2022-02-10 13:17:37'),
(1410, '2022-02-10 00:00:00', '114.119.141.84', '2022-02-10 13:27:11', '2022-02-10 13:27:11'),
(1411, '2022-02-10 00:00:00', '27.255.176.55', '2022-02-10 13:48:26', '2022-02-10 13:48:26'),
(1412, '2022-02-10 00:00:00', '114.119.141.71', '2022-02-10 14:26:07', '2022-02-10 14:26:07'),
(1413, '2022-02-10 00:00:00', '124.253.96.36', '2022-02-10 14:27:46', '2022-02-10 14:27:46'),
(1414, '2022-02-10 00:00:00', '124.253.96.36', '2022-02-10 14:44:07', '2022-02-10 14:44:07'),
(1415, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 14:54:37', '2022-02-10 14:54:37'),
(1416, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 15:24:37', '2022-02-10 15:24:37'),
(1417, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 15:54:37', '2022-02-10 15:54:37'),
(1418, '2022-02-10 00:00:00', '114.119.148.34', '2022-02-10 16:19:44', '2022-02-10 16:19:44'),
(1419, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 16:24:37', '2022-02-10 16:24:37'),
(1420, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 16:54:37', '2022-02-10 16:54:37'),
(1421, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 17:45:38', '2022-02-10 17:45:38'),
(1422, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 17:48:55', '2022-02-10 17:48:55'),
(1423, '2022-02-10 00:00:00', '114.119.143.119', '2022-02-10 18:04:00', '2022-02-10 18:04:00'),
(1424, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 18:12:56', '2022-02-10 18:12:56'),
(1425, '2022-02-10 00:00:00', '114.119.142.20', '2022-02-10 18:16:19', '2022-02-10 18:16:19'),
(1426, '2022-02-10 00:00:00', '114.119.133.53', '2022-02-10 18:16:39', '2022-02-10 18:16:39'),
(1427, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 18:17:00', '2022-02-10 18:17:00'),
(1428, '2022-02-10 00:00:00', '114.119.143.135', '2022-02-10 19:18:14', '2022-02-10 19:18:14'),
(1429, '2022-02-10 00:00:00', '114.119.141.27', '2022-02-10 19:24:07', '2022-02-10 19:24:07'),
(1430, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 19:45:38', '2022-02-10 19:45:38'),
(1431, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 20:01:46', '2022-02-10 20:01:46'),
(1432, '2022-02-10 00:00:00', '114.119.143.51', '2022-02-10 20:19:43', '2022-02-10 20:19:43'),
(1433, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 20:21:24', '2022-02-10 20:21:24'),
(1434, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 20:27:57', '2022-02-10 20:27:57'),
(1435, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 20:34:30', '2022-02-10 20:34:30'),
(1436, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 21:00:41', '2022-02-10 21:00:41'),
(1437, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 21:07:14', '2022-02-10 21:07:14'),
(1438, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 21:14:17', '2022-02-10 21:14:17'),
(1439, '2022-02-10 00:00:00', '54.36.148.137', '2022-02-10 21:16:40', '2022-02-10 21:16:40'),
(1440, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 21:20:20', '2022-02-10 21:20:20'),
(1441, '2022-02-10 00:00:00', '114.119.144.55', '2022-02-10 21:24:41', '2022-02-10 21:24:41'),
(1442, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 21:39:59', '2022-02-10 21:39:59'),
(1443, '2022-02-10 00:00:00', '114.119.147.42', '2022-02-10 21:43:24', '2022-02-10 21:43:24'),
(1444, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 21:46:32', '2022-02-10 21:46:32'),
(1445, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 22:09:12', '2022-02-10 22:09:12'),
(1446, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 23:01:35', '2022-02-10 23:01:35'),
(1447, '2022-02-10 00:00:00', '66.249.68.61', '2022-02-10 23:08:08', '2022-02-10 23:08:08'),
(1448, '2022-02-10 00:00:00', '66.249.68.59', '2022-02-10 23:27:46', '2022-02-10 23:27:46'),
(1449, '2022-02-10 00:00:00', '66.249.68.57', '2022-02-10 23:34:19', '2022-02-10 23:34:19'),
(1450, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 00:39:48', '2022-02-11 00:39:48'),
(1451, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 00:52:58', '2022-02-11 00:52:58'),
(1452, '2022-02-11 00:00:00', '66.249.68.57', '2022-02-11 01:51:49', '2022-02-11 01:51:49'),
(1453, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 02:24:33', '2022-02-11 02:24:33'),
(1454, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 02:47:39', '2022-02-11 02:47:39'),
(1455, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 03:27:39', '2022-02-11 03:27:39'),
(1456, '2022-02-11 00:00:00', '114.119.132.23', '2022-02-11 03:44:32', '2022-02-11 03:44:32'),
(1457, '2022-02-11 00:00:00', '114.119.144.179', '2022-02-11 04:09:24', '2022-02-11 04:09:24'),
(1458, '2022-02-11 00:00:00', '66.249.68.57', '2022-02-11 04:18:41', '2022-02-11 04:18:41'),
(1459, '2022-02-11 00:00:00', '114.119.150.65', '2022-02-11 04:36:50', '2022-02-11 04:36:50'),
(1460, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 04:51:25', '2022-02-11 04:51:25'),
(1461, '2022-02-11 00:00:00', '114.119.144.11', '2022-02-11 05:01:46', '2022-02-11 05:01:46'),
(1462, '2022-02-11 00:00:00', '162.215.209.208', '2022-02-11 05:26:22', '2022-02-11 05:26:22'),
(1463, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 05:27:15', '2022-02-11 05:27:15'),
(1464, '2022-02-11 00:00:00', '124.253.96.36', '2022-02-11 05:35:03', '2022-02-11 05:35:03'),
(1465, '2022-02-11 00:00:00', '114.119.140.64', '2022-02-11 05:35:29', '2022-02-11 05:35:29'),
(1466, '2022-02-11 00:00:00', '203.115.91.247', '2022-02-11 06:00:35', '2022-02-11 06:00:35'),
(1467, '2022-02-11 00:00:00', '162.215.209.208', '2022-02-11 06:45:48', '2022-02-11 06:45:48'),
(1468, '2022-02-11 00:00:00', '124.253.96.36', '2022-02-11 06:49:17', '2022-02-11 06:49:17'),
(1469, '2022-02-11 00:00:00', '114.119.149.228', '2022-02-11 07:35:24', '2022-02-11 07:35:24'),
(1470, '2022-02-11 00:00:00', '207.46.13.90', '2022-02-11 08:09:22', '2022-02-11 08:09:22'),
(1471, '2022-02-11 00:00:00', '207.46.13.203', '2022-02-11 08:09:33', '2022-02-11 08:09:33'),
(1472, '2022-02-11 00:00:00', '114.119.146.220', '2022-02-11 08:19:51', '2022-02-11 08:19:51'),
(1473, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 09:27:39', '2022-02-11 09:27:39'),
(1474, '2022-02-11 00:00:00', '124.253.96.36', '2022-02-11 09:30:08', '2022-02-11 09:30:08'),
(1475, '2022-02-11 00:00:00', '54.36.148.137', '2022-02-11 09:40:01', '2022-02-11 09:40:01'),
(1476, '2022-02-11 00:00:00', '54.36.148.137', '2022-02-11 10:03:49', '2022-02-11 10:03:49'),
(1477, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 10:07:40', '2022-02-11 10:07:40'),
(1478, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 11:04:43', '2022-02-11 11:04:43'),
(1479, '2022-02-11 00:00:00', '66.249.68.57', '2022-02-11 11:04:44', '2022-02-11 11:04:44'),
(1480, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 11:47:39', '2022-02-11 11:47:39'),
(1481, '2022-02-11 00:00:00', '124.253.96.36', '2022-02-11 12:10:09', '2022-02-11 12:10:09'),
(1482, '2022-02-11 00:00:00', '27.255.178.92', '2022-02-11 12:50:35', '2022-02-11 12:50:35'),
(1483, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 13:02:39', '2022-02-11 13:02:39'),
(1484, '2022-02-11 00:00:00', '66.249.68.57', '2022-02-11 13:37:39', '2022-02-11 13:37:39'),
(1485, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 14:04:19', '2022-02-11 14:04:19'),
(1486, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 14:30:59', '2022-02-11 14:30:59'),
(1487, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 14:37:39', '2022-02-11 14:37:39'),
(1488, '2022-02-11 00:00:00', '66.249.68.59', '2022-02-11 14:50:59', '2022-02-11 14:50:59'),
(1489, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 14:57:39', '2022-02-11 14:57:39'),
(1490, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 15:16:19', '2022-02-11 15:16:19'),
(1491, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 15:52:19', '2022-02-11 15:52:19'),
(1492, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 16:52:18', '2022-02-11 16:52:18'),
(1493, '2022-02-11 00:00:00', '54.36.148.137', '2022-02-11 20:53:13', '2022-02-11 20:53:13'),
(1494, '2022-02-11 00:00:00', '66.249.68.57', '2022-02-11 21:01:18', '2022-02-11 21:01:18'),
(1495, '2022-02-11 00:00:00', '114.119.141.178', '2022-02-11 22:10:50', '2022-02-11 22:10:50'),
(1496, '2022-02-11 00:00:00', '114.119.142.236', '2022-02-11 22:48:25', '2022-02-11 22:48:25'),
(1497, '2022-02-11 00:00:00', '114.119.148.174', '2022-02-11 22:51:42', '2022-02-11 22:51:42'),
(1498, '2022-02-11 00:00:00', '66.249.68.61', '2022-02-11 23:26:49', '2022-02-11 23:26:49'),
(1499, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 00:26:49', '2022-02-12 00:26:49'),
(1500, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 01:41:40', '2022-02-12 01:41:40'),
(1501, '2022-02-12 00:00:00', '223.178.210.11', '2022-02-12 01:43:53', '2022-02-12 01:43:53'),
(1502, '2022-02-12 00:00:00', '66.249.68.57', '2022-02-12 01:51:25', '2022-02-12 01:51:25'),
(1503, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 02:49:54', '2022-02-12 02:49:54'),
(1504, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 03:09:26', '2022-02-12 03:09:26'),
(1505, '2022-02-12 00:00:00', '114.119.147.42', '2022-02-12 05:03:36', '2022-02-12 05:03:36'),
(1506, '2022-02-12 00:00:00', '114.119.150.158', '2022-02-12 05:38:07', '2022-02-12 05:38:07'),
(1507, '2022-02-12 00:00:00', '66.249.68.57', '2022-02-12 05:47:55', '2022-02-12 05:47:55'),
(1508, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 08:12:29', '2022-02-12 08:12:29'),
(1509, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 09:49:48', '2022-02-12 09:49:48'),
(1510, '2022-02-12 00:00:00', '114.119.145.139', '2022-02-12 11:32:56', '2022-02-12 11:32:56'),
(1511, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 12:04:59', '2022-02-12 12:04:59'),
(1512, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 12:14:59', '2022-02-12 12:14:59'),
(1513, '2022-02-12 00:00:00', '114.119.145.130', '2022-02-12 12:45:25', '2022-02-12 12:45:25'),
(1514, '2022-02-12 00:00:00', '114.119.142.188', '2022-02-12 12:59:50', '2022-02-12 12:59:50'),
(1515, '2022-02-12 00:00:00', '118.191.130.108', '2022-02-12 13:36:04', '2022-02-12 13:36:04'),
(1516, '2022-02-12 00:00:00', '118.191.130.10', '2022-02-12 13:36:16', '2022-02-12 13:36:16'),
(1517, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 13:39:59', '2022-02-12 13:39:59'),
(1518, '2022-02-12 00:00:00', '114.119.146.40', '2022-02-12 13:58:21', '2022-02-12 13:58:21'),
(1519, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 14:01:59', '2022-02-12 14:01:59'),
(1520, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 14:13:59', '2022-02-12 14:13:59'),
(1521, '2022-02-12 00:00:00', '114.119.148.169', '2022-02-12 14:18:03', '2022-02-12 14:18:03'),
(1522, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 14:25:59', '2022-02-12 14:25:59'),
(1523, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 14:38:07', '2022-02-12 14:38:07'),
(1524, '2022-02-12 00:00:00', '66.249.68.59', '2022-02-12 15:01:58', '2022-02-12 15:01:58'),
(1525, '2022-02-12 00:00:00', '122.177.117.38', '2022-02-12 15:04:44', '2022-02-12 15:04:44'),
(1526, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 15:37:59', '2022-02-12 15:37:59'),
(1527, '2022-02-12 00:00:00', '66.249.68.61', '2022-02-12 19:07:59', '2022-02-12 19:07:59'),
(1528, '2022-02-12 00:00:00', '114.119.146.16', '2022-02-12 19:17:51', '2022-02-12 19:17:51'),
(1529, '2022-02-12 00:00:00', '114.119.148.208', '2022-02-12 19:48:51', '2022-02-12 19:48:51'),
(1530, '2022-02-12 00:00:00', '142.132.139.251', '2022-02-12 20:56:00', '2022-02-12 20:56:00'),
(1531, '2022-02-12 00:00:00', '114.119.146.70', '2022-02-12 22:02:26', '2022-02-12 22:02:26'),
(1532, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 00:41:11', '2022-02-13 00:41:11'),
(1533, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 01:11:11', '2022-02-13 01:11:11'),
(1534, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 02:19:13', '2022-02-13 02:19:13'),
(1535, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 02:19:17', '2022-02-13 02:19:17'),
(1536, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 02:48:41', '2022-02-13 02:48:41'),
(1537, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 03:03:42', '2022-02-13 03:03:42'),
(1538, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 03:18:41', '2022-02-13 03:18:41'),
(1539, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 03:26:11', '2022-02-13 03:26:11'),
(1540, '2022-02-13 00:00:00', '114.119.143.14', '2022-02-13 03:41:44', '2022-02-13 03:41:44'),
(1541, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 03:48:41', '2022-02-13 03:48:41'),
(1542, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 04:11:11', '2022-02-13 04:11:11'),
(1543, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 04:18:41', '2022-02-13 04:18:41'),
(1544, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 04:58:02', '2022-02-13 04:58:02'),
(1545, '2022-02-13 00:00:00', '114.119.143.14', '2022-02-13 06:35:22', '2022-02-13 06:35:22'),
(1546, '2022-02-13 00:00:00', '154.54.249.195', '2022-02-13 07:28:44', '2022-02-13 07:28:44'),
(1547, '2022-02-13 00:00:00', '114.119.151.179', '2022-02-13 07:45:48', '2022-02-13 07:45:48'),
(1548, '2022-02-13 00:00:00', '114.119.144.132', '2022-02-13 08:33:59', '2022-02-13 08:33:59'),
(1549, '2022-02-13 00:00:00', '114.119.148.242', '2022-02-13 09:04:30', '2022-02-13 09:04:30'),
(1550, '2022-02-13 00:00:00', '114.119.148.175', '2022-02-13 09:58:42', '2022-02-13 09:58:42'),
(1551, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 11:28:07', '2022-02-13 11:28:07'),
(1552, '2022-02-13 00:00:00', '157.55.39.4', '2022-02-13 12:16:59', '2022-02-13 12:16:59'),
(1553, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 13:12:04', '2022-02-13 13:12:04'),
(1554, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 13:30:05', '2022-02-13 13:30:05'),
(1555, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 14:18:04', '2022-02-13 14:18:04'),
(1556, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 14:55:47', '2022-02-13 14:55:47'),
(1557, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 15:12:55', '2022-02-13 15:12:55'),
(1558, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 15:21:30', '2022-02-13 15:21:30'),
(1559, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 15:38:39', '2022-02-13 15:38:39'),
(1560, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 15:53:38', '2022-02-13 15:53:38'),
(1561, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 16:08:38', '2022-02-13 16:08:38'),
(1562, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 16:23:38', '2022-02-13 16:23:38'),
(1563, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 16:38:39', '2022-02-13 16:38:39'),
(1564, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 17:08:38', '2022-02-13 17:08:38'),
(1565, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 17:23:38', '2022-02-13 17:23:38'),
(1566, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 18:23:38', '2022-02-13 18:23:38'),
(1567, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 18:38:38', '2022-02-13 18:38:38'),
(1568, '2022-02-13 00:00:00', '66.249.68.59', '2022-02-13 18:58:38', '2022-02-13 18:58:38'),
(1569, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 19:18:38', '2022-02-13 19:18:38'),
(1570, '2022-02-13 00:00:00', '54.36.148.137', '2022-02-13 20:09:47', '2022-02-13 20:09:47'),
(1571, '2022-02-13 00:00:00', '54.36.148.137', '2022-02-13 20:34:18', '2022-02-13 20:34:18'),
(1572, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 20:51:16', '2022-02-13 20:51:16'),
(1573, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 21:08:38', '2022-02-13 21:08:38'),
(1574, '2022-02-13 00:00:00', '54.36.148.137', '2022-02-13 21:21:52', '2022-02-13 21:21:52'),
(1575, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 22:08:39', '2022-02-13 22:08:39'),
(1576, '2022-02-13 00:00:00', '114.119.141.249', '2022-02-13 22:20:00', '2022-02-13 22:20:00'),
(1577, '2022-02-13 00:00:00', '114.119.149.232', '2022-02-13 22:47:06', '2022-02-13 22:47:06'),
(1578, '2022-02-13 00:00:00', '92.118.160.37', '2022-02-13 23:21:17', '2022-02-13 23:21:17'),
(1579, '2022-02-13 00:00:00', '66.249.68.57', '2022-02-13 23:38:35', '2022-02-13 23:38:35'),
(1580, '2022-02-13 00:00:00', '66.249.68.61', '2022-02-13 23:48:35', '2022-02-13 23:48:35'),
(1581, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 00:08:35', '2022-02-14 00:08:35'),
(1582, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 00:18:35', '2022-02-14 00:18:35'),
(1583, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 00:58:35', '2022-02-14 00:58:35'),
(1584, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 01:38:35', '2022-02-14 01:38:35'),
(1585, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 02:18:35', '2022-02-14 02:18:35'),
(1586, '2022-02-14 00:00:00', '66.249.68.59', '2022-02-14 02:38:35', '2022-02-14 02:38:35'),
(1587, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 03:38:34', '2022-02-14 03:38:34'),
(1588, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 04:08:35', '2022-02-14 04:08:35'),
(1589, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 04:38:37', '2022-02-14 04:38:37'),
(1590, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 05:38:35', '2022-02-14 05:38:35'),
(1591, '2022-02-14 00:00:00', '66.249.68.59', '2022-02-14 06:18:36', '2022-02-14 06:18:36'),
(1592, '2022-02-14 00:00:00', '203.134.196.101', '2022-02-14 06:22:15', '2022-02-14 06:22:15'),
(1593, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 06:38:35', '2022-02-14 06:38:35'),
(1594, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 06:58:34', '2022-02-14 06:58:34'),
(1595, '2022-02-14 00:00:00', '54.36.148.137', '2022-02-14 07:03:49', '2022-02-14 07:03:49'),
(1596, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 07:18:34', '2022-02-14 07:18:34'),
(1597, '2022-02-14 00:00:00', '203.134.196.101', '2022-02-14 07:34:35', '2022-02-14 07:34:35'),
(1598, '2022-02-14 00:00:00', '92.118.160.37', '2022-02-14 07:49:25', '2022-02-14 07:49:25'),
(1599, '2022-02-14 00:00:00', '66.249.68.59', '2022-02-14 07:58:34', '2022-02-14 07:58:34'),
(1600, '2022-02-14 00:00:00', '106.196.90.88', '2022-02-14 08:16:48', '2022-02-14 08:16:48'),
(1601, '2022-02-14 00:00:00', '42.111.9.198', '2022-02-14 08:20:57', '2022-02-14 08:20:57'),
(1602, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 10:39:18', '2022-02-14 10:39:18'),
(1603, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 13:16:27', '2022-02-14 13:16:27'),
(1604, '2022-02-14 00:00:00', '66.249.68.59', '2022-02-14 13:16:29', '2022-02-14 13:16:29'),
(1605, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 13:34:34', '2022-02-14 13:34:34'),
(1606, '2022-02-14 00:00:00', '66.249.68.59', '2022-02-14 13:50:59', '2022-02-14 13:50:59'),
(1607, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 13:58:31', '2022-02-14 13:58:31'),
(1608, '2022-02-14 00:00:00', '54.36.148.137', '2022-02-14 14:03:20', '2022-02-14 14:03:20'),
(1609, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 14:05:59', '2022-02-14 14:05:59'),
(1610, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 14:28:33', '2022-02-14 14:28:33'),
(1611, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 16:04:29', '2022-02-14 16:04:29'),
(1612, '2022-02-14 00:00:00', '223.178.211.234', '2022-02-14 16:32:34', '2022-02-14 16:32:34'),
(1613, '2022-02-14 00:00:00', '162.215.209.208', '2022-02-14 16:38:21', '2022-02-14 16:38:21'),
(1614, '2022-02-14 00:00:00', '54.36.148.137', '2022-02-14 16:57:48', '2022-02-14 16:57:48'),
(1615, '2022-02-14 00:00:00', '114.119.138.199', '2022-02-14 17:01:14', '2022-02-14 17:01:14'),
(1616, '2022-02-14 00:00:00', '66.249.68.33', '2022-02-14 17:10:29', '2022-02-14 17:10:29'),
(1617, '2022-02-14 00:00:00', '157.55.39.4', '2022-02-14 18:45:10', '2022-02-14 18:45:10'),
(1618, '2022-02-14 00:00:00', '207.46.13.102', '2022-02-14 20:03:00', '2022-02-14 20:03:00'),
(1619, '2022-02-14 00:00:00', '54.36.148.137', '2022-02-14 20:15:02', '2022-02-14 20:15:02'),
(1620, '2022-02-14 00:00:00', '54.36.148.137', '2022-02-14 20:41:05', '2022-02-14 20:41:05'),
(1621, '2022-02-14 00:00:00', '207.46.13.102', '2022-02-14 20:46:24', '2022-02-14 20:46:24'),
(1622, '2022-02-14 00:00:00', '66.249.68.57', '2022-02-14 22:24:44', '2022-02-14 22:24:44'),
(1623, '2022-02-14 00:00:00', '66.249.68.61', '2022-02-14 23:55:53', '2022-02-14 23:55:53'),
(1624, '2022-02-15 00:00:00', '157.55.39.204', '2022-02-15 01:58:50', '2022-02-15 01:58:50'),
(1625, '2022-02-15 00:00:00', '54.36.148.137', '2022-02-15 02:33:30', '2022-02-15 02:33:30'),
(1626, '2022-02-15 00:00:00', '54.36.148.137', '2022-02-15 03:05:27', '2022-02-15 03:05:27'),
(1627, '2022-02-15 00:00:00', '66.249.68.57', '2022-02-15 03:55:29', '2022-02-15 03:55:29'),
(1628, '2022-02-15 00:00:00', '66.249.68.61', '2022-02-15 04:25:29', '2022-02-15 04:25:29'),
(1629, '2022-02-15 00:00:00', '54.36.148.137', '2022-02-15 04:56:48', '2022-02-15 04:56:48'),
(1630, '2022-02-15 00:00:00', '124.253.24.174', '2022-02-15 05:38:41', '2022-02-15 05:38:41'),
(1631, '2022-02-15 00:00:00', '10.44.83.153', '2022-02-15 05:53:49', '2022-02-15 05:53:49'),
(1632, '2022-02-15 00:00:00', '47.9.133.61', '2022-02-15 05:55:26', '2022-02-15 05:55:26'),
(1633, '2022-02-15 00:00:00', '124.253.24.174', '2022-02-15 06:04:02', '2022-02-15 06:04:02'),
(1634, '2022-02-15 00:00:00', '223.178.211.234', '2022-02-15 06:05:41', '2022-02-15 06:05:41'),
(1635, '2022-02-15 00:00:00', '10.44.83.135', '2022-02-15 06:09:34', '2022-02-15 06:09:34'),
(1636, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 13:02:17', '2022-02-15 13:02:17'),
(1637, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 13:02:53', '2022-02-15 13:02:53'),
(1638, '2022-02-15 00:00:00', '172.68.39.236', '2022-02-15 13:08:43', '2022-02-15 13:08:43'),
(1639, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 13:15:13', '2022-02-15 13:15:13'),
(1640, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 13:17:21', '2022-02-15 13:17:21'),
(1641, '2022-02-15 00:00:00', '141.101.98.240', '2022-02-15 13:26:03', '2022-02-15 13:26:03'),
(1642, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 13:28:46', '2022-02-15 13:28:46'),
(1643, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 13:32:11', '2022-02-15 13:32:11'),
(1644, '2022-02-15 00:00:00', '172.70.85.142', '2022-02-15 13:32:12', '2022-02-15 13:32:12'),
(1645, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 13:34:50', '2022-02-15 13:34:50');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(1646, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 13:45:57', '2022-02-15 13:45:57'),
(1647, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 13:48:17', '2022-02-15 13:48:17'),
(1648, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 13:55:06', '2022-02-15 13:55:06'),
(1649, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 13:57:40', '2022-02-15 13:57:40'),
(1650, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 14:03:40', '2022-02-15 14:03:40'),
(1651, '2022-02-15 00:00:00', '172.68.39.236', '2022-02-15 14:08:16', '2022-02-15 14:08:16'),
(1652, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 14:14:52', '2022-02-15 14:14:52'),
(1653, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 14:15:40', '2022-02-15 14:15:40'),
(1654, '2022-02-15 00:00:00', '172.68.39.236', '2022-02-15 14:26:48', '2022-02-15 14:26:48'),
(1655, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 14:31:26', '2022-02-15 14:31:26'),
(1656, '2022-02-15 00:00:00', '172.68.39.236', '2022-02-15 14:43:23', '2022-02-15 14:43:23'),
(1657, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 14:44:34', '2022-02-15 14:44:34'),
(1658, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 14:46:06', '2022-02-15 14:46:06'),
(1659, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 14:51:08', '2022-02-15 14:51:08'),
(1660, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 15:03:30', '2022-02-15 15:03:30'),
(1661, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 15:04:08', '2022-02-15 15:04:08'),
(1662, '2022-02-15 00:00:00', '172.68.39.236', '2022-02-15 15:04:54', '2022-02-15 15:04:54'),
(1663, '2022-02-15 00:00:00', '172.68.39.236', '2022-02-15 15:30:31', '2022-02-15 15:30:31'),
(1664, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 15:32:35', '2022-02-15 15:32:35'),
(1665, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 15:32:37', '2022-02-15 15:32:37'),
(1666, '2022-02-15 00:00:00', '172.68.39.216', '2022-02-15 15:43:37', '2022-02-15 15:43:37'),
(1667, '2022-02-15 00:00:00', '172.68.39.134', '2022-02-15 16:02:18', '2022-02-15 16:02:18'),
(1668, '2022-02-15 00:00:00', '172.68.39.150', '2022-02-15 16:10:50', '2022-02-15 16:10:50'),
(1669, '2022-02-15 00:00:00', '172.70.51.147', '2022-02-15 16:22:56', '2022-02-15 16:22:56'),
(1670, '2022-02-15 00:00:00', '172.70.85.230', '2022-02-15 16:33:17', '2022-02-15 16:33:17'),
(1671, '2022-02-15 00:00:00', '172.70.91.97', '2022-02-15 17:36:23', '2022-02-15 17:36:23'),
(1672, '2022-02-15 00:00:00', '162.158.31.204', '2022-02-15 18:23:42', '2022-02-15 18:23:42'),
(1673, '2022-02-15 00:00:00', '172.70.210.226', '2022-02-15 19:44:10', '2022-02-15 19:44:10'),
(1674, '2022-02-15 00:00:00', '162.158.238.240', '2022-02-15 19:45:47', '2022-02-15 19:45:47'),
(1675, '2022-02-15 00:00:00', '172.70.85.142', '2022-02-15 22:58:46', '2022-02-15 22:58:46'),
(1676, '2022-02-15 00:00:00', '172.69.234.134', '2022-02-15 23:37:59', '2022-02-15 23:37:59'),
(1677, '2022-02-16 00:00:00', '172.70.91.75', '2022-02-16 00:44:28', '2022-02-16 00:44:28'),
(1678, '2022-02-16 00:00:00', '172.70.130.118', '2022-02-16 02:13:35', '2022-02-16 02:13:35'),
(1679, '2022-02-16 00:00:00', '172.69.239.164', '2022-02-16 04:09:22', '2022-02-16 04:09:22'),
(1680, '2022-02-16 00:00:00', '172.69.239.150', '2022-02-16 04:17:28', '2022-02-16 04:17:28'),
(1681, '2022-02-16 00:00:00', '172.69.239.164', '2022-02-16 05:00:30', '2022-02-16 05:00:30'),
(1682, '2022-02-16 00:00:00', '172.70.188.166', '2022-02-16 05:00:53', '2022-02-16 05:00:53'),
(1683, '2022-02-16 00:00:00', '172.69.239.146', '2022-02-16 05:01:51', '2022-02-16 05:01:51'),
(1684, '2022-02-16 00:00:00', '162.158.159.34', '2022-02-16 05:02:14', '2022-02-16 05:02:14'),
(1685, '2022-02-16 00:00:00', '172.70.85.230', '2022-02-16 05:05:27', '2022-02-16 05:05:27'),
(1686, '2022-02-16 00:00:00', '141.101.99.17', '2022-02-16 05:07:57', '2022-02-16 05:07:57'),
(1687, '2022-02-16 00:00:00', '172.70.91.97', '2022-02-16 05:18:47', '2022-02-16 05:18:47'),
(1688, '2022-02-16 00:00:00', '172.69.239.146', '2022-02-16 06:26:49', '2022-02-16 06:26:49'),
(1689, '2022-02-16 00:00:00', '141.101.104.187', '2022-02-16 06:36:20', '2022-02-16 06:36:20'),
(1690, '2022-02-16 00:00:00', '141.101.77.68', '2022-02-16 06:36:28', '2022-02-16 06:36:28'),
(1691, '2022-02-16 00:00:00', '172.70.162.240', '2022-02-16 07:47:39', '2022-02-16 07:47:39'),
(1692, '2022-02-16 00:00:00', '162.158.159.88', '2022-02-16 07:56:37', '2022-02-16 07:56:37'),
(1693, '2022-02-16 00:00:00', '172.70.92.176', '2022-02-16 08:25:24', '2022-02-16 08:25:24'),
(1694, '2022-02-16 00:00:00', '172.70.91.75', '2022-02-16 09:43:58', '2022-02-16 09:43:58'),
(1695, '2022-02-16 00:00:00', '172.70.250.138', '2022-02-16 09:50:23', '2022-02-16 09:50:23'),
(1696, '2022-02-16 00:00:00', '172.70.242.68', '2022-02-16 09:50:24', '2022-02-16 09:50:24'),
(1697, '2022-02-16 00:00:00', '172.68.39.150', '2022-02-16 09:54:59', '2022-02-16 09:54:59'),
(1698, '2022-02-16 00:00:00', '172.68.39.216', '2022-02-16 09:57:29', '2022-02-16 09:57:29'),
(1699, '2022-02-16 00:00:00', '172.70.91.113', '2022-02-16 10:04:16', '2022-02-16 10:04:16'),
(1700, '2022-02-16 00:00:00', '172.68.39.236', '2022-02-16 10:11:23', '2022-02-16 10:11:23'),
(1701, '2022-02-16 00:00:00', '172.70.91.113', '2022-02-16 10:15:39', '2022-02-16 10:15:39'),
(1702, '2022-02-16 00:00:00', '172.68.39.134', '2022-02-16 10:18:14', '2022-02-16 10:18:14'),
(1703, '2022-02-16 00:00:00', '172.68.39.216', '2022-02-16 10:24:08', '2022-02-16 10:24:08'),
(1704, '2022-02-16 00:00:00', '172.70.85.230', '2022-02-16 10:49:41', '2022-02-16 10:49:41'),
(1705, '2022-02-16 00:00:00', '172.70.91.91', '2022-02-16 12:35:28', '2022-02-16 12:35:28'),
(1706, '2022-02-16 00:00:00', '172.70.85.212', '2022-02-16 13:54:28', '2022-02-16 13:54:28'),
(1707, '2022-02-16 00:00:00', '172.68.39.150', '2022-02-16 14:11:54', '2022-02-16 14:11:54'),
(1708, '2022-02-16 00:00:00', '172.70.250.16', '2022-02-16 16:48:32', '2022-02-16 16:48:32'),
(1709, '2022-02-16 00:00:00', '172.70.206.204', '2022-02-16 18:41:09', '2022-02-16 18:41:09'),
(1710, '2022-02-16 00:00:00', '172.70.206.170', '2022-02-16 19:10:15', '2022-02-16 19:10:15'),
(1711, '2022-02-16 00:00:00', '172.68.238.112', '2022-02-16 19:20:27', '2022-02-16 19:20:27'),
(1712, '2022-02-17 00:00:00', '172.68.10.182', '2022-02-17 02:07:01', '2022-02-17 02:07:01'),
(1713, '2022-02-17 00:00:00', '172.70.85.230', '2022-02-17 05:38:33', '2022-02-17 05:38:33'),
(1714, '2022-02-17 00:00:00', '172.70.85.212', '2022-02-17 05:40:16', '2022-02-17 05:40:16'),
(1715, '2022-02-17 00:00:00', '172.70.92.240', '2022-02-17 05:49:54', '2022-02-17 05:49:54'),
(1716, '2022-02-17 00:00:00', '172.70.34.202', '2022-02-17 05:57:25', '2022-02-17 05:57:25'),
(1717, '2022-02-17 00:00:00', '172.68.39.150', '2022-02-17 06:09:28', '2022-02-17 06:09:28'),
(1718, '2022-02-17 00:00:00', '162.158.119.169', '2022-02-17 06:09:50', '2022-02-17 06:09:50'),
(1719, '2022-02-17 00:00:00', '172.69.239.150', '2022-02-17 06:11:04', '2022-02-17 06:11:04'),
(1720, '2022-02-17 00:00:00', '172.70.34.2', '2022-02-17 12:17:17', '2022-02-17 12:17:17'),
(1721, '2022-02-17 00:00:00', '162.158.78.16', '2022-02-17 12:19:13', '2022-02-17 12:19:13'),
(1722, '2022-02-17 00:00:00', '108.162.246.105', '2022-02-17 14:24:43', '2022-02-17 14:24:43'),
(1723, '2022-02-17 00:00:00', '141.101.84.190', '2022-02-17 15:00:53', '2022-02-17 15:00:53'),
(1724, '2022-02-18 00:00:00', '162.158.90.244', '2022-02-18 02:31:09', '2022-02-18 02:31:09'),
(1725, '2022-02-18 00:00:00', '172.70.85.142', '2022-02-18 07:19:30', '2022-02-18 07:19:30'),
(1726, '2022-02-18 00:00:00', '162.158.159.126', '2022-02-18 07:19:44', '2022-02-18 07:19:44'),
(1727, '2022-02-18 00:00:00', '172.70.85.212', '2022-02-18 07:19:54', '2022-02-18 07:19:54'),
(1728, '2022-02-18 00:00:00', '172.70.162.22', '2022-02-18 07:24:31', '2022-02-18 07:24:31'),
(1729, '2022-02-18 00:00:00', '141.101.98.244', '2022-02-18 07:32:48', '2022-02-18 07:32:48'),
(1730, '2022-02-18 00:00:00', '172.70.230.96', '2022-02-18 08:18:33', '2022-02-18 08:18:33'),
(1731, '2022-02-18 00:00:00', '172.70.251.7', '2022-02-18 09:11:26', '2022-02-18 09:11:26'),
(1732, '2022-02-18 00:00:00', '162.158.90.80', '2022-02-18 09:11:36', '2022-02-18 09:11:36'),
(1733, '2022-02-18 00:00:00', '172.70.130.100', '2022-02-18 14:31:45', '2022-02-18 14:31:45'),
(1734, '2022-02-18 00:00:00', '172.70.126.20', '2022-02-18 14:33:29', '2022-02-18 14:33:29'),
(1735, '2022-02-18 00:00:00', '172.70.126.140', '2022-02-18 14:34:19', '2022-02-18 14:34:19'),
(1736, '2022-02-18 00:00:00', '172.70.178.186', '2022-02-18 14:34:41', '2022-02-18 14:34:41'),
(1737, '2022-02-18 00:00:00', '162.158.203.49', '2022-02-18 17:10:28', '2022-02-18 17:10:28'),
(1738, '2022-02-18 00:00:00', '162.158.78.150', '2022-02-18 18:57:08', '2022-02-18 18:57:08'),
(1739, '2022-02-18 00:00:00', '108.162.245.40', '2022-02-18 20:59:14', '2022-02-18 20:59:14'),
(1740, '2022-02-18 00:00:00', '172.70.230.138', '2022-02-18 20:59:15', '2022-02-18 20:59:15'),
(1741, '2022-02-18 00:00:00', '172.70.210.180', '2022-02-18 22:14:20', '2022-02-18 22:14:20'),
(1742, '2022-02-18 00:00:00', '108.162.238.187', '2022-02-18 23:06:41', '2022-02-18 23:06:41'),
(1743, '2022-02-19 00:00:00', '108.162.238.103', '2022-02-19 07:29:14', '2022-02-19 07:29:14'),
(1744, '2022-02-19 00:00:00', '172.70.34.202', '2022-02-19 18:55:27', '2022-02-19 18:55:27'),
(1745, '2022-02-19 00:00:00', '162.158.107.105', '2022-02-19 20:16:43', '2022-02-19 20:16:43'),
(1746, '2022-02-19 00:00:00', '162.158.107.133', '2022-02-19 20:16:44', '2022-02-19 20:16:44'),
(1747, '2022-02-19 00:00:00', '108.162.245.214', '2022-02-19 20:16:45', '2022-02-19 20:16:45'),
(1748, '2022-02-19 00:00:00', '162.158.107.209', '2022-02-19 20:16:45', '2022-02-19 20:16:45'),
(1749, '2022-02-19 00:00:00', '162.158.107.163', '2022-02-19 20:16:46', '2022-02-19 20:16:46'),
(1750, '2022-02-19 00:00:00', '108.162.245.92', '2022-02-19 20:16:46', '2022-02-19 20:16:46'),
(1751, '2022-02-19 00:00:00', '162.158.106.146', '2022-02-19 20:16:46', '2022-02-19 20:16:46'),
(1752, '2022-02-19 00:00:00', '108.162.245.36', '2022-02-19 20:16:47', '2022-02-19 20:16:47'),
(1753, '2022-02-19 00:00:00', '108.162.246.7', '2022-02-19 20:16:47', '2022-02-19 20:16:47'),
(1754, '2022-02-19 00:00:00', '162.158.106.94', '2022-02-19 20:16:47', '2022-02-19 20:16:47'),
(1755, '2022-02-19 00:00:00', '162.158.107.181', '2022-02-19 20:16:48', '2022-02-19 20:16:48'),
(1756, '2022-02-19 00:00:00', '162.158.106.124', '2022-02-19 20:16:48', '2022-02-19 20:16:48'),
(1757, '2022-02-19 00:00:00', '108.162.246.117', '2022-02-19 20:16:48', '2022-02-19 20:16:48'),
(1758, '2022-02-19 00:00:00', '108.162.246.49', '2022-02-19 20:16:49', '2022-02-19 20:16:49'),
(1759, '2022-02-19 00:00:00', '162.158.107.21', '2022-02-19 20:16:51', '2022-02-19 20:16:51'),
(1760, '2022-02-19 00:00:00', '108.162.246.105', '2022-02-19 20:16:52', '2022-02-19 20:16:52'),
(1761, '2022-02-19 00:00:00', '108.162.246.45', '2022-02-19 20:16:54', '2022-02-19 20:16:54'),
(1762, '2022-02-19 00:00:00', '162.158.107.151', '2022-02-19 20:16:55', '2022-02-19 20:16:55'),
(1763, '2022-02-19 00:00:00', '162.158.107.69', '2022-02-19 20:16:58', '2022-02-19 20:16:58'),
(1764, '2022-02-19 00:00:00', '108.162.245.126', '2022-02-19 20:17:01', '2022-02-19 20:17:01'),
(1765, '2022-02-19 00:00:00', '162.158.106.212', '2022-02-19 20:17:04', '2022-02-19 20:17:04'),
(1766, '2022-02-20 00:00:00', '172.70.130.100', '2022-02-20 00:27:52', '2022-02-20 00:27:52'),
(1767, '2022-02-20 00:00:00', '172.68.10.248', '2022-02-20 01:30:23', '2022-02-20 01:30:23'),
(1768, '2022-02-20 00:00:00', '108.162.237.74', '2022-02-20 03:04:20', '2022-02-20 03:04:20'),
(1769, '2022-02-20 00:00:00', '172.68.10.248', '2022-02-20 04:16:49', '2022-02-20 04:16:49'),
(1770, '2022-02-20 00:00:00', '172.70.130.100', '2022-02-20 12:44:19', '2022-02-20 12:44:19'),
(1771, '2022-02-20 00:00:00', '172.70.130.96', '2022-02-20 12:44:20', '2022-02-20 12:44:20'),
(1772, '2022-02-20 00:00:00', '172.70.131.161', '2022-02-20 12:44:20', '2022-02-20 12:44:20'),
(1773, '2022-02-20 00:00:00', '172.70.178.28', '2022-02-20 12:44:27', '2022-02-20 12:44:27'),
(1774, '2022-02-20 00:00:00', '172.70.126.44', '2022-02-20 12:44:27', '2022-02-20 12:44:27'),
(1775, '2022-02-20 00:00:00', '172.70.178.156', '2022-02-20 12:44:27', '2022-02-20 12:44:27'),
(1776, '2022-02-20 00:00:00', '108.162.238.83', '2022-02-20 14:38:14', '2022-02-20 14:38:14'),
(1777, '2022-02-20 00:00:00', '172.70.126.190', '2022-02-20 17:20:49', '2022-02-20 17:20:49'),
(1778, '2022-02-20 00:00:00', '172.70.130.236', '2022-02-20 18:45:14', '2022-02-20 18:45:14'),
(1779, '2022-02-20 00:00:00', '108.162.237.188', '2022-02-20 22:25:24', '2022-02-20 22:25:24'),
(1780, '2022-02-20 00:00:00', '108.162.238.187', '2022-02-20 22:25:44', '2022-02-20 22:25:44'),
(1781, '2022-02-20 00:00:00', '172.70.242.72', '2022-02-20 22:30:10', '2022-02-20 22:30:10'),
(1782, '2022-02-21 00:00:00', '172.70.35.53', '2022-02-21 01:57:00', '2022-02-21 01:57:00'),
(1783, '2022-02-21 00:00:00', '172.70.51.179', '2022-02-21 04:32:06', '2022-02-21 04:32:06'),
(1784, '2022-02-21 00:00:00', '162.158.251.50', '2022-02-21 04:32:08', '2022-02-21 04:32:08'),
(1785, '2022-02-21 00:00:00', '162.158.251.84', '2022-02-21 04:32:09', '2022-02-21 04:32:09'),
(1786, '2022-02-21 00:00:00', '172.70.51.143', '2022-02-21 04:32:10', '2022-02-21 04:32:10'),
(1787, '2022-02-21 00:00:00', '172.70.147.172', '2022-02-21 07:39:50', '2022-02-21 07:39:50'),
(1788, '2022-02-21 00:00:00', '172.70.86.49', '2022-02-21 08:55:02', '2022-02-21 08:55:02'),
(1789, '2022-02-21 00:00:00', '162.158.159.126', '2022-02-21 09:33:57', '2022-02-21 09:33:57'),
(1790, '2022-02-21 00:00:00', '141.101.99.85', '2022-02-21 12:12:49', '2022-02-21 12:12:49'),
(1791, '2022-02-21 00:00:00', '172.70.126.140', '2022-02-21 20:22:39', '2022-02-21 20:22:39'),
(1792, '2022-02-21 00:00:00', '172.70.162.12', '2022-02-21 21:23:06', '2022-02-21 21:23:06'),
(1793, '2022-02-22 00:00:00', '172.68.39.150', '2022-02-22 04:31:26', '2022-02-22 04:31:26'),
(1794, '2022-02-22 00:00:00', '172.68.39.150', '2022-02-22 05:12:12', '2022-02-22 05:12:12'),
(1795, '2022-02-22 00:00:00', '172.68.39.236', '2022-02-22 05:35:13', '2022-02-22 05:35:13'),
(1796, '2022-02-22 00:00:00', '172.70.86.49', '2022-02-22 05:46:08', '2022-02-22 05:46:08'),
(1797, '2022-02-22 00:00:00', '172.68.39.216', '2022-02-22 06:04:28', '2022-02-22 06:04:28'),
(1798, '2022-02-22 00:00:00', '172.70.135.105', '2022-02-22 15:08:41', '2022-02-22 15:08:41'),
(1799, '2022-02-22 00:00:00', '172.70.126.44', '2022-02-22 15:19:41', '2022-02-22 15:19:41'),
(1800, '2022-02-22 00:00:00', '162.158.74.103', '2022-02-22 15:19:48', '2022-02-22 15:19:48'),
(1801, '2022-02-22 00:00:00', '172.70.178.156', '2022-02-22 15:19:55', '2022-02-22 15:19:55'),
(1802, '2022-02-22 00:00:00', '172.70.126.190', '2022-02-22 15:19:57', '2022-02-22 15:19:57'),
(1803, '2022-02-22 00:00:00', '172.70.178.28', '2022-02-22 15:20:10', '2022-02-22 15:20:10'),
(1804, '2022-02-22 00:00:00', '172.70.130.188', '2022-02-22 15:20:15', '2022-02-22 15:20:15'),
(1805, '2022-02-22 00:00:00', '172.70.130.236', '2022-02-22 15:20:24', '2022-02-22 15:20:24'),
(1806, '2022-02-22 00:00:00', '172.70.178.158', '2022-02-22 15:20:30', '2022-02-22 15:20:30'),
(1807, '2022-02-22 00:00:00', '172.70.126.46', '2022-02-22 15:20:33', '2022-02-22 15:20:33'),
(1808, '2022-02-22 00:00:00', '162.158.74.213', '2022-02-22 15:20:59', '2022-02-22 15:20:59'),
(1809, '2022-02-22 00:00:00', '172.70.126.238', '2022-02-22 15:21:03', '2022-02-22 15:21:03'),
(1810, '2022-02-22 00:00:00', '172.70.130.234', '2022-02-22 15:21:33', '2022-02-22 15:21:33'),
(1811, '2022-02-22 00:00:00', '162.158.75.130', '2022-02-22 15:22:08', '2022-02-22 15:22:08'),
(1812, '2022-02-22 00:00:00', '162.158.74.227', '2022-02-22 15:22:36', '2022-02-22 15:22:36'),
(1813, '2022-02-22 00:00:00', '162.158.74.209', '2022-02-22 15:22:44', '2022-02-22 15:22:44'),
(1814, '2022-02-22 00:00:00', '108.162.216.76', '2022-02-22 15:23:05', '2022-02-22 15:23:05'),
(1815, '2022-02-22 00:00:00', '172.70.178.30', '2022-02-22 15:23:48', '2022-02-22 15:23:48'),
(1816, '2022-02-22 00:00:00', '162.158.74.145', '2022-02-22 15:25:14', '2022-02-22 15:25:14'),
(1817, '2022-02-22 00:00:00', '172.70.130.118', '2022-02-22 15:26:17', '2022-02-22 15:26:17'),
(1818, '2022-02-22 00:00:00', '162.158.74.147', '2022-02-22 15:29:48', '2022-02-22 15:29:48'),
(1819, '2022-02-22 00:00:00', '162.158.74.207', '2022-02-22 15:30:29', '2022-02-22 15:30:29'),
(1820, '2022-02-22 00:00:00', '172.70.126.44', '2022-02-22 15:31:15', '2022-02-22 15:31:15'),
(1821, '2022-02-22 00:00:00', '172.70.130.236', '2022-02-22 15:31:48', '2022-02-22 15:31:48'),
(1822, '2022-02-22 00:00:00', '162.158.238.4', '2022-02-22 22:23:44', '2022-02-22 22:23:44'),
(1823, '2022-02-22 00:00:00', '172.70.35.11', '2022-02-22 23:13:29', '2022-02-22 23:13:29'),
(1824, '2022-02-23 00:00:00', '162.158.107.181', '2022-02-23 00:39:15', '2022-02-23 00:39:15'),
(1825, '2022-02-23 00:00:00', '162.158.107.105', '2022-02-23 02:16:21', '2022-02-23 02:16:21'),
(1826, '2022-02-23 00:00:00', '172.70.242.58', '2022-02-23 03:28:32', '2022-02-23 03:28:32'),
(1827, '2022-02-23 00:00:00', '172.68.39.216', '2022-02-23 05:18:12', '2022-02-23 05:18:12'),
(1828, '2022-02-23 00:00:00', '172.68.39.150', '2022-02-23 05:54:11', '2022-02-23 05:54:11'),
(1829, '2022-02-23 00:00:00', '108.162.241.92', '2022-02-23 07:45:01', '2022-02-23 07:45:01'),
(1830, '2022-02-23 00:00:00', '108.162.241.152', '2022-02-23 07:51:09', '2022-02-23 07:51:09'),
(1831, '2022-02-23 00:00:00', '108.162.241.4', '2022-02-23 08:55:46', '2022-02-23 08:55:46'),
(1832, '2022-02-23 00:00:00', '108.162.241.38', '2022-02-23 08:58:05', '2022-02-23 08:58:05'),
(1833, '2022-02-23 00:00:00', '108.162.241.38', '2022-02-23 09:18:12', '2022-02-23 09:18:12'),
(1834, '2022-02-23 00:00:00', '108.162.241.38', '2022-02-23 09:41:23', '2022-02-23 09:41:23'),
(1835, '2022-02-23 00:00:00', '172.68.39.216', '2022-02-23 10:40:59', '2022-02-23 10:40:59'),
(1836, '2022-02-23 00:00:00', '172.70.162.188', '2022-02-23 10:41:23', '2022-02-23 10:41:23'),
(1837, '2022-02-23 00:00:00', '172.70.85.142', '2022-02-23 10:41:29', '2022-02-23 10:41:29'),
(1838, '2022-02-23 00:00:00', '162.158.238.212', '2022-02-23 15:26:53', '2022-02-23 15:26:53'),
(1839, '2022-02-23 00:00:00', '162.158.203.73', '2022-02-23 15:38:48', '2022-02-23 15:38:48'),
(1840, '2022-02-23 00:00:00', '172.70.242.128', '2022-02-23 15:45:27', '2022-02-23 15:45:27'),
(1841, '2022-02-23 00:00:00', '162.158.90.134', '2022-02-23 15:48:16', '2022-02-23 15:48:16'),
(1842, '2022-02-23 00:00:00', '162.158.202.248', '2022-02-23 15:49:05', '2022-02-23 15:49:05'),
(1843, '2022-02-23 00:00:00', '172.70.246.84', '2022-02-23 21:46:05', '2022-02-23 21:46:05'),
(1844, '2022-02-24 00:00:00', '172.70.35.61', '2022-02-24 05:18:13', '2022-02-24 05:18:13'),
(1845, '2022-02-24 00:00:00', '172.68.238.76', '2022-02-24 10:32:26', '2022-02-24 10:32:26'),
(1846, '2022-02-24 00:00:00', '108.162.241.4', '2022-02-24 10:53:04', '2022-02-24 10:53:04'),
(1847, '2022-02-24 00:00:00', '172.70.147.172', '2022-02-24 16:29:12', '2022-02-24 16:29:12'),
(1848, '2022-02-24 00:00:00', '172.70.174.190', '2022-02-24 21:51:35', '2022-02-24 21:51:35'),
(1849, '2022-02-25 00:00:00', '108.162.241.146', '2022-02-25 05:02:53', '2022-02-25 05:02:53'),
(1850, '2022-02-25 00:00:00', '172.70.147.172', '2022-02-25 06:58:12', '2022-02-25 06:58:12'),
(1851, '2022-02-25 00:00:00', '162.158.89.251', '2022-02-25 07:41:20', '2022-02-25 07:41:20'),
(1852, '2022-02-25 00:00:00', '172.70.142.184', '2022-02-25 11:13:36', '2022-02-25 11:13:36'),
(1853, '2022-02-25 00:00:00', '172.69.33.170', '2022-02-25 11:36:46', '2022-02-25 11:36:46'),
(1854, '2022-02-25 00:00:00', '172.70.130.234', '2022-02-25 13:42:00', '2022-02-25 13:42:00'),
(1855, '2022-02-25 00:00:00', '172.70.126.190', '2022-02-25 13:42:14', '2022-02-25 13:42:14'),
(1856, '2022-02-25 00:00:00', '162.158.106.212', '2022-02-25 15:04:43', '2022-02-25 15:04:43'),
(1857, '2022-02-25 00:00:00', '162.158.107.181', '2022-02-25 15:10:20', '2022-02-25 15:10:20'),
(1858, '2022-02-25 00:00:00', '172.70.250.108', '2022-02-25 19:14:42', '2022-02-25 19:14:42'),
(1859, '2022-02-25 00:00:00', '162.158.90.62', '2022-02-25 19:15:23', '2022-02-25 19:15:23'),
(1860, '2022-02-25 00:00:00', '172.70.242.72', '2022-02-25 19:15:24', '2022-02-25 19:15:24'),
(1861, '2022-02-25 00:00:00', '162.158.91.57', '2022-02-25 19:15:35', '2022-02-25 19:15:35'),
(1862, '2022-02-25 00:00:00', '172.70.246.168', '2022-02-25 19:15:39', '2022-02-25 19:15:39'),
(1863, '2022-02-25 00:00:00', '162.158.90.208', '2022-02-25 19:15:44', '2022-02-25 19:15:44'),
(1864, '2022-02-25 00:00:00', '162.158.91.155', '2022-02-25 19:15:45', '2022-02-25 19:15:45'),
(1865, '2022-02-25 00:00:00', '162.158.90.126', '2022-02-25 19:15:45', '2022-02-25 19:15:45'),
(1866, '2022-02-25 00:00:00', '162.158.91.125', '2022-02-25 19:15:46', '2022-02-25 19:15:46'),
(1867, '2022-02-25 00:00:00', '172.70.250.38', '2022-02-25 19:15:46', '2022-02-25 19:15:46'),
(1868, '2022-02-25 00:00:00', '162.158.89.251', '2022-02-25 19:15:47', '2022-02-25 19:15:47'),
(1869, '2022-02-25 00:00:00', '172.70.246.132', '2022-02-25 19:15:47', '2022-02-25 19:15:47'),
(1870, '2022-02-25 00:00:00', '172.70.242.128', '2022-02-25 19:15:47', '2022-02-25 19:15:47'),
(1871, '2022-02-25 00:00:00', '162.158.90.224', '2022-02-25 19:15:47', '2022-02-25 19:15:47'),
(1872, '2022-02-25 00:00:00', '172.70.242.58', '2022-02-25 19:15:48', '2022-02-25 19:15:48'),
(1873, '2022-02-25 00:00:00', '162.158.92.2', '2022-02-25 19:15:49', '2022-02-25 19:15:49'),
(1874, '2022-02-25 00:00:00', '172.70.242.210', '2022-02-25 19:15:49', '2022-02-25 19:15:49'),
(1875, '2022-02-25 00:00:00', '172.70.250.134', '2022-02-25 19:15:50', '2022-02-25 19:15:50'),
(1876, '2022-02-25 00:00:00', '162.158.91.169', '2022-02-25 19:15:51', '2022-02-25 19:15:51'),
(1877, '2022-02-25 00:00:00', '172.70.250.16', '2022-02-25 19:15:51', '2022-02-25 19:15:51'),
(1878, '2022-02-25 00:00:00', '172.70.250.16', '2022-02-25 19:15:51', '2022-02-25 19:15:51'),
(1879, '2022-02-25 00:00:00', '162.158.90.142', '2022-02-25 19:15:51', '2022-02-25 19:15:51'),
(1880, '2022-02-25 00:00:00', '162.158.90.156', '2022-02-25 19:15:51', '2022-02-25 19:15:51'),
(1881, '2022-02-25 00:00:00', '172.70.210.180', '2022-02-25 21:30:23', '2022-02-25 21:30:23'),
(1882, '2022-02-26 00:00:00', '172.70.242.68', '2022-02-26 05:48:09', '2022-02-26 05:48:09'),
(1883, '2022-02-26 00:00:00', '172.70.130.234', '2022-02-26 05:58:10', '2022-02-26 05:58:10'),
(1884, '2022-02-26 00:00:00', '162.158.222.164', '2022-02-26 07:31:45', '2022-02-26 07:31:45'),
(1885, '2022-02-26 00:00:00', '172.70.246.32', '2022-02-26 09:21:46', '2022-02-26 09:21:46'),
(1886, '2022-02-26 00:00:00', '172.70.126.140', '2022-02-26 11:33:55', '2022-02-26 11:33:55'),
(1887, '2022-02-27 00:00:00', '108.162.238.161', '2022-02-27 02:46:51', '2022-02-27 02:46:51'),
(1888, '2022-02-27 00:00:00', '162.158.187.149', '2022-02-27 02:47:04', '2022-02-27 02:47:04'),
(1889, '2022-02-27 00:00:00', '162.158.74.113', '2022-02-27 04:00:47', '2022-02-27 04:00:47'),
(1890, '2022-02-27 00:00:00', '141.101.68.64', '2022-02-27 07:11:58', '2022-02-27 07:11:58'),
(1891, '2022-02-27 00:00:00', '108.162.241.130', '2022-02-27 07:58:10', '2022-02-27 07:58:10'),
(1892, '2022-02-27 00:00:00', '172.70.92.176', '2022-02-27 12:00:23', '2022-02-27 12:00:23'),
(1893, '2022-02-27 00:00:00', '162.158.183.231', '2022-02-27 15:37:44', '2022-02-27 15:37:44'),
(1894, '2022-02-27 00:00:00', '162.158.63.48', '2022-02-27 18:07:19', '2022-02-27 18:07:19'),
(1895, '2022-02-27 00:00:00', '108.162.219.122', '2022-02-27 23:14:00', '2022-02-27 23:14:00'),
(1896, '2022-02-28 00:00:00', '162.158.187.235', '2022-02-28 00:51:42', '2022-02-28 00:51:42'),
(1897, '2022-02-28 00:00:00', '172.70.135.105', '2022-02-28 01:29:23', '2022-02-28 01:29:23'),
(1898, '2022-02-28 00:00:00', '172.70.188.206', '2022-02-28 02:01:45', '2022-02-28 02:01:45'),
(1899, '2022-02-28 00:00:00', '108.162.246.7', '2022-02-28 08:08:36', '2022-02-28 08:08:36'),
(1900, '2022-02-28 00:00:00', '108.162.245.92', '2022-02-28 08:08:36', '2022-02-28 08:08:36'),
(1901, '2022-02-28 00:00:00', '108.162.245.214', '2022-02-28 08:08:37', '2022-02-28 08:08:37'),
(1902, '2022-02-28 00:00:00', '108.162.245.126', '2022-02-28 08:08:38', '2022-02-28 08:08:38'),
(1903, '2022-02-28 00:00:00', '162.158.107.133', '2022-02-28 08:08:38', '2022-02-28 08:08:38'),
(1904, '2022-02-28 00:00:00', '162.158.107.69', '2022-02-28 08:08:39', '2022-02-28 08:08:39'),
(1905, '2022-02-28 00:00:00', '162.158.107.181', '2022-02-28 08:08:39', '2022-02-28 08:08:39'),
(1906, '2022-02-28 00:00:00', '108.162.246.105', '2022-02-28 08:08:41', '2022-02-28 08:08:41'),
(1907, '2022-02-28 00:00:00', '108.162.246.49', '2022-02-28 08:08:41', '2022-02-28 08:08:41'),
(1908, '2022-02-28 00:00:00', '162.158.107.127', '2022-02-28 08:08:41', '2022-02-28 08:08:41'),
(1909, '2022-02-28 00:00:00', '108.162.245.36', '2022-02-28 08:08:41', '2022-02-28 08:08:41'),
(1910, '2022-02-28 00:00:00', '162.158.107.21', '2022-02-28 08:08:41', '2022-02-28 08:08:41'),
(1911, '2022-02-28 00:00:00', '162.158.106.146', '2022-02-28 08:08:42', '2022-02-28 08:08:42'),
(1912, '2022-02-28 00:00:00', '162.158.107.151', '2022-02-28 08:08:42', '2022-02-28 08:08:42'),
(1913, '2022-02-28 00:00:00', '162.158.107.167', '2022-02-28 08:08:44', '2022-02-28 08:08:44'),
(1914, '2022-02-28 00:00:00', '108.162.245.204', '2022-02-28 08:08:45', '2022-02-28 08:08:45'),
(1915, '2022-02-28 00:00:00', '162.158.106.124', '2022-02-28 08:08:45', '2022-02-28 08:08:45'),
(1916, '2022-02-28 00:00:00', '162.158.107.105', '2022-02-28 08:08:47', '2022-02-28 08:08:47'),
(1917, '2022-02-28 00:00:00', '108.162.246.45', '2022-02-28 08:08:48', '2022-02-28 08:08:48'),
(1918, '2022-02-28 00:00:00', '162.158.106.94', '2022-02-28 08:08:50', '2022-02-28 08:08:50'),
(1919, '2022-02-28 00:00:00', '108.162.246.117', '2022-02-28 08:08:51', '2022-02-28 08:08:51'),
(1920, '2022-02-28 00:00:00', '162.158.107.209', '2022-02-28 08:08:53', '2022-02-28 08:08:53'),
(1921, '2022-02-28 00:00:00', '172.70.206.94', '2022-02-28 11:07:21', '2022-02-28 11:07:21'),
(1922, '2022-02-28 00:00:00', '172.70.250.38', '2022-02-28 11:27:10', '2022-02-28 11:27:10'),
(1923, '2022-02-28 00:00:00', '162.158.92.22', '2022-02-28 11:57:30', '2022-02-28 11:57:30'),
(1924, '2022-02-28 00:00:00', '172.70.142.246', '2022-02-28 15:57:36', '2022-02-28 15:57:36'),
(1925, '2022-02-28 00:00:00', '108.162.237.188', '2022-02-28 22:42:44', '2022-02-28 22:42:44'),
(1926, '2022-02-28 00:00:00', '108.162.237.106', '2022-02-28 22:42:51', '2022-02-28 22:42:51'),
(1927, '2022-03-01 00:00:00', '172.69.33.86', '2022-03-01 03:24:41', '2022-03-01 03:24:41'),
(1928, '2022-03-01 00:00:00', '172.70.131.161', '2022-03-01 04:26:35', '2022-03-01 04:26:35'),
(1929, '2022-03-01 00:00:00', '172.70.130.188', '2022-03-01 06:57:44', '2022-03-01 06:57:44'),
(1930, '2022-03-01 00:00:00', '172.68.10.182', '2022-03-01 10:47:27', '2022-03-01 10:47:27'),
(1931, '2022-03-01 00:00:00', '162.158.107.181', '2022-03-01 14:35:21', '2022-03-01 14:35:21'),
(1932, '2022-03-01 00:00:00', '172.70.130.236', '2022-03-01 14:55:41', '2022-03-01 14:55:41'),
(1933, '2022-03-01 00:00:00', '162.158.107.105', '2022-03-01 15:34:45', '2022-03-01 15:34:45'),
(1934, '2022-03-02 00:00:00', '162.158.74.147', '2022-03-02 04:10:39', '2022-03-02 04:10:39'),
(1935, '2022-03-02 00:00:00', '162.158.179.164', '2022-03-02 05:05:57', '2022-03-02 05:05:57'),
(1936, '2022-03-02 00:00:00', '172.70.134.32', '2022-03-02 05:48:28', '2022-03-02 05:48:28'),
(1937, '2022-03-02 00:00:00', '162.158.187.175', '2022-03-02 05:49:19', '2022-03-02 05:49:19'),
(1938, '2022-03-02 00:00:00', '108.162.238.119', '2022-03-02 05:49:27', '2022-03-02 05:49:27'),
(1939, '2022-03-02 00:00:00', '172.70.85.150', '2022-03-02 09:37:18', '2022-03-02 09:37:18'),
(1940, '2022-03-02 00:00:00', '172.70.251.179', '2022-03-02 09:43:47', '2022-03-02 09:43:47'),
(1941, '2022-03-02 00:00:00', '172.70.246.24', '2022-03-02 09:43:47', '2022-03-02 09:43:47'),
(1942, '2022-03-02 00:00:00', '172.70.206.154', '2022-03-02 11:10:21', '2022-03-02 11:10:21'),
(1943, '2022-03-02 00:00:00', '172.70.135.45', '2022-03-02 23:48:45', '2022-03-02 23:48:45'),
(1944, '2022-03-03 00:00:00', '141.101.99.239', '2022-03-03 01:29:57', '2022-03-03 01:29:57'),
(1945, '2022-03-03 00:00:00', '172.68.254.38', '2022-03-03 01:42:23', '2022-03-03 01:42:23'),
(1946, '2022-03-03 00:00:00', '108.162.246.7', '2022-03-03 10:43:54', '2022-03-03 10:43:54'),
(1947, '2022-03-03 00:00:00', '162.158.106.146', '2022-03-03 10:43:55', '2022-03-03 10:43:55'),
(1948, '2022-03-03 00:00:00', '108.162.245.204', '2022-03-03 10:43:56', '2022-03-03 10:43:56'),
(1949, '2022-03-03 00:00:00', '162.158.222.184', '2022-03-03 10:44:10', '2022-03-03 10:44:10'),
(1950, '2022-03-03 00:00:00', '172.70.110.104', '2022-03-03 13:45:51', '2022-03-03 13:45:51'),
(1951, '2022-03-03 00:00:00', '172.70.110.44', '2022-03-03 13:45:55', '2022-03-03 13:45:55'),
(1952, '2022-03-03 00:00:00', '172.70.230.2', '2022-03-03 13:45:58', '2022-03-03 13:45:58'),
(1953, '2022-03-03 00:00:00', '172.70.114.236', '2022-03-03 13:46:12', '2022-03-03 13:46:12'),
(1954, '2022-03-03 00:00:00', '162.158.178.58', '2022-03-03 14:57:42', '2022-03-03 14:57:42'),
(1955, '2022-03-03 00:00:00', '172.70.210.126', '2022-03-03 17:04:03', '2022-03-03 17:04:03'),
(1956, '2022-03-03 00:00:00', '172.70.189.41', '2022-03-03 18:00:54', '2022-03-03 18:00:54'),
(1957, '2022-03-04 00:00:00', '162.158.107.151', '2022-03-04 00:43:38', '2022-03-04 00:43:38'),
(1958, '2022-03-04 00:00:00', '172.70.114.106', '2022-03-04 02:31:35', '2022-03-04 02:31:35'),
(1959, '2022-03-04 00:00:00', '162.158.222.156', '2022-03-04 07:40:58', '2022-03-04 07:40:58'),
(1960, '2022-03-04 00:00:00', '172.68.39.150', '2022-03-04 12:11:46', '2022-03-04 12:11:46'),
(1961, '2022-03-04 00:00:00', '172.70.85.150', '2022-03-04 12:11:51', '2022-03-04 12:11:51'),
(1962, '2022-03-04 00:00:00', '172.70.162.240', '2022-03-04 12:12:14', '2022-03-04 12:12:14'),
(1963, '2022-03-04 00:00:00', '172.70.162.22', '2022-03-04 12:12:36', '2022-03-04 12:12:36'),
(1964, '2022-03-04 00:00:00', '172.70.85.40', '2022-03-04 12:12:41', '2022-03-04 12:12:41'),
(1965, '2022-03-04 00:00:00', '172.68.39.216', '2022-03-04 12:15:06', '2022-03-04 12:15:06'),
(1966, '2022-03-04 00:00:00', '172.70.242.54', '2022-03-04 12:33:42', '2022-03-04 12:33:42'),
(1967, '2022-03-04 00:00:00', '162.158.179.178', '2022-03-04 13:56:25', '2022-03-04 13:56:25'),
(1968, '2022-03-04 00:00:00', '172.70.162.240', '2022-03-04 22:01:43', '2022-03-04 22:01:43'),
(1969, '2022-03-05 00:00:00', '172.70.126.190', '2022-03-05 00:53:34', '2022-03-05 00:53:34'),
(1970, '2022-03-05 00:00:00', '172.70.162.22', '2022-03-05 01:00:23', '2022-03-05 01:00:23'),
(1971, '2022-03-05 00:00:00', '172.70.142.220', '2022-03-05 03:00:54', '2022-03-05 03:00:54'),
(1972, '2022-03-05 00:00:00', '172.70.206.34', '2022-03-05 04:24:12', '2022-03-05 04:24:12'),
(1973, '2022-03-05 00:00:00', '172.70.210.226', '2022-03-05 04:24:15', '2022-03-05 04:24:15'),
(1974, '2022-03-05 00:00:00', '172.70.42.68', '2022-03-05 12:31:46', '2022-03-05 12:31:46'),
(1975, '2022-03-05 00:00:00', '162.158.107.21', '2022-03-05 14:28:48', '2022-03-05 14:28:48'),
(1976, '2022-03-05 00:00:00', '172.70.230.2', '2022-03-05 18:29:33', '2022-03-05 18:29:33'),
(1977, '2022-03-05 00:00:00', '172.70.134.210', '2022-03-05 19:34:45', '2022-03-05 19:34:45'),
(1978, '2022-03-05 00:00:00', '172.70.34.2', '2022-03-05 19:34:46', '2022-03-05 19:34:46'),
(1979, '2022-03-05 00:00:00', '172.70.175.95', '2022-03-05 19:34:47', '2022-03-05 19:34:47'),
(1980, '2022-03-05 00:00:00', '172.70.130.188', '2022-03-05 20:30:06', '2022-03-05 20:30:06'),
(1981, '2022-03-05 00:00:00', '172.70.178.156', '2022-03-05 23:48:44', '2022-03-05 23:48:44'),
(1982, '2022-03-06 00:00:00', '172.70.206.154', '2022-03-06 02:50:09', '2022-03-06 02:50:09'),
(1983, '2022-03-06 00:00:00', '162.158.222.184', '2022-03-06 03:08:08', '2022-03-06 03:08:08'),
(1984, '2022-03-06 00:00:00', '172.70.211.157', '2022-03-06 04:34:25', '2022-03-06 04:34:25'),
(1985, '2022-03-06 00:00:00', '141.101.77.44', '2022-03-06 13:27:48', '2022-03-06 13:27:48'),
(1986, '2022-03-06 00:00:00', '172.70.250.62', '2022-03-06 13:43:52', '2022-03-06 13:43:52'),
(1987, '2022-03-06 00:00:00', '108.162.237.96', '2022-03-06 15:03:18', '2022-03-06 15:03:18'),
(1988, '2022-03-06 00:00:00', '172.70.246.74', '2022-03-06 22:28:55', '2022-03-06 22:28:55'),
(1989, '2022-03-06 00:00:00', '172.69.33.72', '2022-03-06 23:28:58', '2022-03-06 23:28:58'),
(1990, '2022-03-07 00:00:00', '162.158.38.12', '2022-03-07 03:50:56', '2022-03-07 03:50:56'),
(1991, '2022-03-07 00:00:00', '141.101.77.200', '2022-03-07 07:15:58', '2022-03-07 07:15:58'),
(1992, '2022-03-07 00:00:00', '172.70.206.32', '2022-03-07 11:21:05', '2022-03-07 11:21:05'),
(1993, '2022-03-07 00:00:00', '172.69.33.196', '2022-03-07 11:22:02', '2022-03-07 11:22:02'),
(1994, '2022-03-07 00:00:00', '172.70.130.96', '2022-03-07 12:35:41', '2022-03-07 12:35:41'),
(1995, '2022-03-07 00:00:00', '172.70.130.100', '2022-03-07 12:35:41', '2022-03-07 12:35:41'),
(1996, '2022-03-07 00:00:00', '172.70.130.142', '2022-03-07 12:35:42', '2022-03-07 12:35:42'),
(1997, '2022-03-07 00:00:00', '172.69.234.134', '2022-03-07 13:07:08', '2022-03-07 13:07:08'),
(1998, '2022-03-07 00:00:00', '172.70.114.154', '2022-03-07 13:23:48', '2022-03-07 13:23:48'),
(1999, '2022-03-07 00:00:00', '172.70.110.220', '2022-03-07 13:23:49', '2022-03-07 13:23:49'),
(2000, '2022-03-07 00:00:00', '172.70.114.148', '2022-03-07 13:23:54', '2022-03-07 13:23:54'),
(2001, '2022-03-07 00:00:00', '172.70.110.104', '2022-03-07 13:23:56', '2022-03-07 13:23:56'),
(2002, '2022-03-07 00:00:00', '172.70.110.202', '2022-03-07 13:23:58', '2022-03-07 13:23:58'),
(2003, '2022-03-07 00:00:00', '172.70.114.172', '2022-03-07 13:24:00', '2022-03-07 13:24:00'),
(2004, '2022-03-07 00:00:00', '172.70.110.146', '2022-03-07 13:24:01', '2022-03-07 13:24:01'),
(2005, '2022-03-07 00:00:00', '108.162.219.116', '2022-03-07 13:24:05', '2022-03-07 13:24:05'),
(2006, '2022-03-07 00:00:00', '108.162.219.192', '2022-03-07 13:24:07', '2022-03-07 13:24:07'),
(2007, '2022-03-07 00:00:00', '162.158.62.73', '2022-03-07 13:24:15', '2022-03-07 13:24:15'),
(2008, '2022-03-07 00:00:00', '172.70.114.108', '2022-03-07 13:24:16', '2022-03-07 13:24:16'),
(2009, '2022-03-07 00:00:00', '162.158.63.12', '2022-03-07 13:24:20', '2022-03-07 13:24:20'),
(2010, '2022-03-07 00:00:00', '172.70.230.62', '2022-03-07 13:24:22', '2022-03-07 13:24:22'),
(2011, '2022-03-07 00:00:00', '172.70.230.64', '2022-03-07 13:24:27', '2022-03-07 13:24:27'),
(2012, '2022-03-07 00:00:00', '172.70.162.188', '2022-03-07 16:08:54', '2022-03-07 16:08:54'),
(2013, '2022-03-08 00:00:00', '172.70.230.54', '2022-03-08 01:41:19', '2022-03-08 01:41:19'),
(2014, '2022-03-08 00:00:00', '162.158.179.67', '2022-03-08 05:01:41', '2022-03-08 05:01:41'),
(2015, '2022-03-08 00:00:00', '172.69.234.138', '2022-03-08 05:31:41', '2022-03-08 05:31:41'),
(2016, '2022-03-08 00:00:00', '172.68.234.132', '2022-03-08 05:46:15', '2022-03-08 05:46:15'),
(2017, '2022-03-08 00:00:00', '172.70.188.196', '2022-03-08 05:55:24', '2022-03-08 05:55:24'),
(2018, '2022-03-08 00:00:00', '172.70.162.188', '2022-03-08 07:35:29', '2022-03-08 07:35:29'),
(2019, '2022-03-08 00:00:00', '162.158.162.30', '2022-03-08 08:55:44', '2022-03-08 08:55:44'),
(2020, '2022-03-08 00:00:00', '172.68.253.40', '2022-03-08 10:00:55', '2022-03-08 10:00:55'),
(2021, '2022-03-08 00:00:00', '172.68.189.37', '2022-03-08 11:30:44', '2022-03-08 11:30:44'),
(2022, '2022-03-08 00:00:00', '162.158.107.163', '2022-03-08 11:30:45', '2022-03-08 11:30:45'),
(2023, '2022-03-08 00:00:00', '162.158.107.133', '2022-03-08 11:30:45', '2022-03-08 11:30:45'),
(2024, '2022-03-08 00:00:00', '172.69.134.108', '2022-03-08 11:30:45', '2022-03-08 11:30:45'),
(2025, '2022-03-08 00:00:00', '172.68.132.137', '2022-03-08 11:30:45', '2022-03-08 11:30:45'),
(2026, '2022-03-08 00:00:00', '172.69.134.70', '2022-03-08 11:30:46', '2022-03-08 11:30:46'),
(2027, '2022-03-08 00:00:00', '108.162.246.49', '2022-03-08 11:30:47', '2022-03-08 11:30:47'),
(2028, '2022-03-08 00:00:00', '172.68.143.212', '2022-03-08 11:30:47', '2022-03-08 11:30:47'),
(2029, '2022-03-08 00:00:00', '162.158.107.69', '2022-03-08 11:30:47', '2022-03-08 11:30:47'),
(2030, '2022-03-08 00:00:00', '172.69.134.28', '2022-03-08 11:30:48', '2022-03-08 11:30:48'),
(2031, '2022-03-08 00:00:00', '172.68.133.70', '2022-03-08 11:30:49', '2022-03-08 11:30:49'),
(2032, '2022-03-08 00:00:00', '172.69.134.94', '2022-03-08 11:30:49', '2022-03-08 11:30:49'),
(2033, '2022-03-08 00:00:00', '172.69.33.60', '2022-03-08 11:30:50', '2022-03-08 11:30:50'),
(2034, '2022-03-08 00:00:00', '172.69.134.52', '2022-03-08 11:30:50', '2022-03-08 11:30:50'),
(2035, '2022-03-08 00:00:00', '172.70.206.170', '2022-03-08 11:30:51', '2022-03-08 11:30:51'),
(2036, '2022-03-08 00:00:00', '172.69.33.14', '2022-03-08 11:30:51', '2022-03-08 11:30:51'),
(2037, '2022-03-08 00:00:00', '108.162.246.7', '2022-03-08 11:30:52', '2022-03-08 11:30:52'),
(2038, '2022-03-08 00:00:00', '108.162.245.92', '2022-03-08 11:30:53', '2022-03-08 11:30:53'),
(2039, '2022-03-08 00:00:00', '172.70.214.32', '2022-03-08 11:30:53', '2022-03-08 11:30:53'),
(2040, '2022-03-08 00:00:00', '162.158.107.181', '2022-03-08 11:30:54', '2022-03-08 11:30:54'),
(2041, '2022-03-08 00:00:00', '172.70.214.152', '2022-03-08 11:30:54', '2022-03-08 11:30:54'),
(2042, '2022-03-08 00:00:00', '108.162.246.105', '2022-03-08 11:30:55', '2022-03-08 11:30:55'),
(2043, '2022-03-08 00:00:00', '172.70.210.126', '2022-03-08 11:30:56', '2022-03-08 11:30:56'),
(2044, '2022-03-08 00:00:00', '172.70.211.157', '2022-03-08 11:30:56', '2022-03-08 11:30:56'),
(2045, '2022-03-08 00:00:00', '162.158.107.105', '2022-03-08 11:30:57', '2022-03-08 11:30:57'),
(2046, '2022-03-08 00:00:00', '108.162.245.36', '2022-03-08 11:30:58', '2022-03-08 11:30:58'),
(2047, '2022-03-08 00:00:00', '108.162.246.45', '2022-03-08 11:30:59', '2022-03-08 11:30:59'),
(2048, '2022-03-08 00:00:00', '108.162.245.126', '2022-03-08 11:30:59', '2022-03-08 11:30:59'),
(2049, '2022-03-08 00:00:00', '162.158.106.212', '2022-03-08 11:31:00', '2022-03-08 11:31:00'),
(2050, '2022-03-08 00:00:00', '108.162.245.214', '2022-03-08 11:31:00', '2022-03-08 11:31:00'),
(2051, '2022-03-08 00:00:00', '172.69.33.54', '2022-03-08 11:31:01', '2022-03-08 11:31:01'),
(2052, '2022-03-08 00:00:00', '172.70.214.188', '2022-03-08 11:31:02', '2022-03-08 11:31:02'),
(2053, '2022-03-08 00:00:00', '172.68.143.228', '2022-03-08 11:31:02', '2022-03-08 11:31:02'),
(2054, '2022-03-08 00:00:00', '172.69.134.36', '2022-03-08 11:31:02', '2022-03-08 11:31:02'),
(2055, '2022-03-08 00:00:00', '172.70.211.39', '2022-03-08 11:31:03', '2022-03-08 11:31:03'),
(2056, '2022-03-08 00:00:00', '162.158.106.146', '2022-03-08 11:31:03', '2022-03-08 11:31:03'),
(2057, '2022-03-08 00:00:00', '172.70.206.154', '2022-03-08 11:31:04', '2022-03-08 11:31:04'),
(2058, '2022-03-08 00:00:00', '172.70.211.23', '2022-03-08 11:31:04', '2022-03-08 11:31:04'),
(2059, '2022-03-08 00:00:00', '162.158.107.209', '2022-03-08 11:31:05', '2022-03-08 11:31:05'),
(2060, '2022-03-08 00:00:00', '172.70.35.11', '2022-03-08 13:56:39', '2022-03-08 13:56:39'),
(2061, '2022-03-08 00:00:00', '172.69.234.136', '2022-03-08 14:22:57', '2022-03-08 14:22:57'),
(2062, '2022-03-08 00:00:00', '108.162.229.90', '2022-03-08 16:53:53', '2022-03-08 16:53:53'),
(2063, '2022-03-08 00:00:00', '162.158.78.188', '2022-03-08 18:41:44', '2022-03-08 18:41:44'),
(2064, '2022-03-08 00:00:00', '162.158.179.89', '2022-03-08 18:59:42', '2022-03-08 18:59:42'),
(2065, '2022-03-09 00:00:00', '172.70.135.81', '2022-03-09 01:25:58', '2022-03-09 01:25:58'),
(2066, '2022-03-09 00:00:00', '172.70.188.206', '2022-03-09 03:42:30', '2022-03-09 03:42:30'),
(2067, '2022-03-09 00:00:00', '172.68.254.60', '2022-03-09 07:17:34', '2022-03-09 07:17:34'),
(2068, '2022-03-09 00:00:00', '172.70.130.188', '2022-03-09 09:44:56', '2022-03-09 09:44:56'),
(2069, '2022-03-09 00:00:00', '162.158.38.46', '2022-03-09 11:10:48', '2022-03-09 11:10:48'),
(2070, '2022-03-09 00:00:00', '162.158.38.82', '2022-03-09 11:10:52', '2022-03-09 11:10:52'),
(2071, '2022-03-09 00:00:00', '172.69.234.138', '2022-03-09 14:05:02', '2022-03-09 14:05:02'),
(2072, '2022-03-09 00:00:00', '172.70.147.44', '2022-03-09 18:24:51', '2022-03-09 18:24:51'),
(2073, '2022-03-09 00:00:00', '172.69.234.138', '2022-03-09 18:36:40', '2022-03-09 18:36:40'),
(2074, '2022-03-09 00:00:00', '172.70.126.46', '2022-03-09 20:05:35', '2022-03-09 20:05:35'),
(2075, '2022-03-10 00:00:00', '172.70.206.32', '2022-03-10 01:44:38', '2022-03-10 01:44:38'),
(2076, '2022-03-10 00:00:00', '108.162.246.7', '2022-03-10 01:45:24', '2022-03-10 01:45:24'),
(2077, '2022-03-10 00:00:00', '172.70.134.154', '2022-03-10 02:22:55', '2022-03-10 02:22:55'),
(2078, '2022-03-10 00:00:00', '172.70.135.81', '2022-03-10 02:22:55', '2022-03-10 02:22:55'),
(2079, '2022-03-10 00:00:00', '172.70.135.15', '2022-03-10 02:49:24', '2022-03-10 02:49:24'),
(2080, '2022-03-10 00:00:00', '172.70.174.128', '2022-03-10 02:49:38', '2022-03-10 02:49:38'),
(2081, '2022-03-10 00:00:00', '172.68.10.248', '2022-03-10 07:12:18', '2022-03-10 07:12:18'),
(2082, '2022-03-10 00:00:00', '172.70.242.58', '2022-03-10 07:15:41', '2022-03-10 07:15:41'),
(2083, '2022-03-10 00:00:00', '172.70.211.157', '2022-03-10 10:17:27', '2022-03-10 10:17:27'),
(2084, '2022-03-10 00:00:00', '162.158.106.146', '2022-03-10 14:24:59', '2022-03-10 14:24:59'),
(2085, '2022-03-10 00:00:00', '172.70.214.182', '2022-03-10 17:56:41', '2022-03-10 17:56:41'),
(2086, '2022-03-10 00:00:00', '172.70.206.204', '2022-03-10 18:16:38', '2022-03-10 18:16:38'),
(2087, '2022-03-10 00:00:00', '172.70.210.226', '2022-03-10 21:22:00', '2022-03-10 21:22:00'),
(2088, '2022-03-10 00:00:00', '108.162.246.105', '2022-03-10 23:50:30', '2022-03-10 23:50:30'),
(2089, '2022-03-11 00:00:00', '172.69.234.138', '2022-03-11 01:11:33', '2022-03-11 01:11:33'),
(2090, '2022-03-11 00:00:00', '172.70.211.157', '2022-03-11 01:15:11', '2022-03-11 01:15:11'),
(2091, '2022-03-11 00:00:00', '172.70.246.82', '2022-03-11 02:13:12', '2022-03-11 02:13:12'),
(2092, '2022-03-11 00:00:00', '172.70.214.66', '2022-03-11 02:20:10', '2022-03-11 02:20:10'),
(2093, '2022-03-11 00:00:00', '162.158.107.105', '2022-03-11 02:45:17', '2022-03-11 02:45:17'),
(2094, '2022-03-11 00:00:00', '172.68.10.182', '2022-03-11 03:33:45', '2022-03-11 03:33:45'),
(2095, '2022-03-11 00:00:00', '162.158.222.164', '2022-03-11 06:06:16', '2022-03-11 06:06:16'),
(2096, '2022-03-11 00:00:00', '172.70.246.52', '2022-03-11 08:06:14', '2022-03-11 08:06:14'),
(2097, '2022-03-11 00:00:00', '162.158.107.105', '2022-03-11 14:28:50', '2022-03-11 14:28:50'),
(2098, '2022-03-11 00:00:00', '172.69.33.68', '2022-03-11 20:54:39', '2022-03-11 20:54:39'),
(2099, '2022-03-12 00:00:00', '172.70.246.180', '2022-03-12 02:42:13', '2022-03-12 02:42:13'),
(2100, '2022-03-12 00:00:00', '162.158.90.132', '2022-03-12 02:42:18', '2022-03-12 02:42:18'),
(2101, '2022-03-12 00:00:00', '172.68.10.182', '2022-03-12 05:32:01', '2022-03-12 05:32:01'),
(2102, '2022-03-12 00:00:00', '172.70.230.72', '2022-03-12 07:37:38', '2022-03-12 07:37:38'),
(2103, '2022-03-12 00:00:00', '162.158.129.74', '2022-03-12 12:08:13', '2022-03-12 12:08:13'),
(2104, '2022-03-12 00:00:00', '162.158.134.70', '2022-03-12 12:23:27', '2022-03-12 12:23:27'),
(2105, '2022-03-12 00:00:00', '172.70.189.41', '2022-03-12 15:12:23', '2022-03-12 15:12:23'),
(2106, '2022-03-12 00:00:00', '172.70.142.224', '2022-03-12 15:12:36', '2022-03-12 15:12:36'),
(2107, '2022-03-12 00:00:00', '172.70.92.160', '2022-03-12 15:12:47', '2022-03-12 15:12:47'),
(2108, '2022-03-12 00:00:00', '172.70.142.34', '2022-03-12 15:13:03', '2022-03-12 15:13:03'),
(2109, '2022-03-12 00:00:00', '172.70.135.15', '2022-03-12 18:40:39', '2022-03-12 18:40:39'),
(2110, '2022-03-12 00:00:00', '162.158.78.32', '2022-03-12 18:40:41', '2022-03-12 18:40:41'),
(2111, '2022-03-12 00:00:00', '172.70.142.42', '2022-03-12 21:19:54', '2022-03-12 21:19:54'),
(2112, '2022-03-13 00:00:00', '172.70.92.160', '2022-03-13 04:43:35', '2022-03-13 04:43:35'),
(2113, '2022-03-13 00:00:00', '172.70.250.64', '2022-03-13 08:53:20', '2022-03-13 08:53:20'),
(2114, '2022-03-13 00:00:00', '172.70.110.184', '2022-03-13 09:10:17', '2022-03-13 09:10:17'),
(2115, '2022-03-13 00:00:00', '162.158.38.12', '2022-03-13 09:29:31', '2022-03-13 09:29:31'),
(2116, '2022-03-13 00:00:00', '162.158.38.82', '2022-03-13 09:29:32', '2022-03-13 09:29:32'),
(2117, '2022-03-13 00:00:00', '172.69.170.26', '2022-03-13 14:22:58', '2022-03-13 14:22:58'),
(2118, '2022-03-13 00:00:00', '172.70.214.118', '2022-03-13 20:53:43', '2022-03-13 20:53:43'),
(2119, '2022-03-14 00:00:00', '141.101.69.73', '2022-03-14 04:47:01', '2022-03-14 04:47:01'),
(2120, '2022-03-14 00:00:00', '172.70.142.34', '2022-03-14 06:51:48', '2022-03-14 06:51:48'),
(2121, '2022-03-14 00:00:00', '172.70.211.121', '2022-03-14 08:47:26', '2022-03-14 08:47:26'),
(2122, '2022-03-14 00:00:00', '141.101.77.200', '2022-03-14 13:43:18', '2022-03-14 13:43:18'),
(2123, '2022-03-14 00:00:00', '162.158.78.10', '2022-03-14 14:30:02', '2022-03-14 14:30:02'),
(2124, '2022-03-14 00:00:00', '172.70.35.11', '2022-03-14 14:30:12', '2022-03-14 14:30:12'),
(2125, '2022-03-14 00:00:00', '172.70.211.157', '2022-03-14 15:29:24', '2022-03-14 15:29:24'),
(2126, '2022-03-14 00:00:00', '172.70.233.14', '2022-03-14 16:49:11', '2022-03-14 16:49:11'),
(2127, '2022-03-14 00:00:00', '172.70.92.160', '2022-03-14 21:10:54', '2022-03-14 21:10:54'),
(2128, '2022-03-14 00:00:00', '141.101.77.44', '2022-03-14 23:05:20', '2022-03-14 23:05:20'),
(2129, '2022-03-15 00:00:00', '162.158.106.146', '2022-03-15 05:58:35', '2022-03-15 05:58:35'),
(2130, '2022-03-15 00:00:00', '108.162.241.100', '2022-03-15 07:38:44', '2022-03-15 07:38:44'),
(2131, '2022-03-15 00:00:00', '172.70.251.51', '2022-03-15 08:40:42', '2022-03-15 08:40:42'),
(2132, '2022-03-15 00:00:00', '141.101.98.244', '2022-03-15 09:26:00', '2022-03-15 09:26:00'),
(2133, '2022-03-15 00:00:00', '172.70.91.113', '2022-03-15 11:36:31', '2022-03-15 11:36:31'),
(2134, '2022-03-15 00:00:00', '172.70.174.190', '2022-03-15 14:51:34', '2022-03-15 14:51:34'),
(2135, '2022-03-16 00:00:00', '172.70.174.166', '2022-03-16 00:05:51', '2022-03-16 00:05:51'),
(2136, '2022-03-16 00:00:00', '172.70.134.206', '2022-03-16 00:41:33', '2022-03-16 00:41:33'),
(2137, '2022-03-16 00:00:00', '172.70.174.154', '2022-03-16 01:17:16', '2022-03-16 01:17:16'),
(2138, '2022-03-16 00:00:00', '172.70.35.11', '2022-03-16 01:53:00', '2022-03-16 01:53:00'),
(2139, '2022-03-16 00:00:00', '172.70.174.116', '2022-03-16 02:52:59', '2022-03-16 02:52:59'),
(2140, '2022-03-16 00:00:00', '162.158.78.242', '2022-03-16 03:52:59', '2022-03-16 03:52:59'),
(2141, '2022-03-16 00:00:00', '172.70.130.84', '2022-03-16 04:28:42', '2022-03-16 04:28:42'),
(2142, '2022-03-16 00:00:00', '172.70.134.112', '2022-03-16 05:04:34', '2022-03-16 05:04:34'),
(2143, '2022-03-16 00:00:00', '172.70.135.67', '2022-03-16 05:40:07', '2022-03-16 05:40:07'),
(2144, '2022-03-16 00:00:00', '172.70.233.172', '2022-03-16 06:10:01', '2022-03-16 06:10:01'),
(2145, '2022-03-16 00:00:00', '172.70.174.60', '2022-03-16 06:15:50', '2022-03-16 06:15:50'),
(2146, '2022-03-16 00:00:00', '172.70.134.140', '2022-03-16 06:51:33', '2022-03-16 06:51:33'),
(2147, '2022-03-16 00:00:00', '172.70.42.134', '2022-03-16 07:27:16', '2022-03-16 07:27:16'),
(2148, '2022-03-16 00:00:00', '172.70.134.176', '2022-03-16 08:02:59', '2022-03-16 08:02:59'),
(2149, '2022-03-16 00:00:00', '172.70.135.29', '2022-03-16 08:38:42', '2022-03-16 08:38:42'),
(2150, '2022-03-16 00:00:00', '172.70.34.128', '2022-03-16 09:14:24', '2022-03-16 09:14:24'),
(2151, '2022-03-16 00:00:00', '162.158.89.249', '2022-03-16 09:42:50', '2022-03-16 09:42:50'),
(2152, '2022-03-16 00:00:00', '172.70.250.72', '2022-03-16 09:42:51', '2022-03-16 09:42:51'),
(2153, '2022-03-16 00:00:00', '172.70.131.83', '2022-03-16 09:50:07', '2022-03-16 09:50:07'),
(2154, '2022-03-16 00:00:00', '172.70.130.98', '2022-03-16 11:01:33', '2022-03-16 11:01:33'),
(2155, '2022-03-16 00:00:00', '172.70.188.14', '2022-03-16 11:03:35', '2022-03-16 11:03:35'),
(2156, '2022-03-16 00:00:00', '172.70.126.172', '2022-03-16 13:24:24', '2022-03-16 13:24:24'),
(2157, '2022-03-16 00:00:00', '172.70.130.92', '2022-03-16 14:00:07', '2022-03-16 14:00:07'),
(2158, '2022-03-16 00:00:00', '172.70.92.228', '2022-03-16 14:11:20', '2022-03-16 14:11:20'),
(2159, '2022-03-16 00:00:00', '172.70.178.152', '2022-03-16 15:11:33', '2022-03-16 15:11:33'),
(2160, '2022-03-16 00:00:00', '162.158.35.7', '2022-03-16 15:12:15', '2022-03-16 15:12:15'),
(2161, '2022-03-16 00:00:00', '172.70.206.94', '2022-03-16 15:21:08', '2022-03-16 15:21:08'),
(2162, '2022-03-16 00:00:00', '172.70.126.228', '2022-03-16 15:47:16', '2022-03-16 15:47:16'),
(2163, '2022-03-16 00:00:00', '162.158.179.24', '2022-03-16 15:50:10', '2022-03-16 15:50:10'),
(2164, '2022-03-16 00:00:00', '172.70.178.12', '2022-03-16 16:22:59', '2022-03-16 16:22:59'),
(2165, '2022-03-16 00:00:00', '162.158.89.247', '2022-03-16 17:03:12', '2022-03-16 17:03:12'),
(2166, '2022-03-16 00:00:00', '172.70.251.51', '2022-03-16 17:03:13', '2022-03-16 17:03:13'),
(2167, '2022-03-16 00:00:00', '172.70.246.52', '2022-03-16 17:03:20', '2022-03-16 17:03:20'),
(2168, '2022-03-16 00:00:00', '162.158.90.190', '2022-03-16 17:03:21', '2022-03-16 17:03:21'),
(2169, '2022-03-16 00:00:00', '172.70.242.58', '2022-03-16 17:03:21', '2022-03-16 17:03:21'),
(2170, '2022-03-16 00:00:00', '162.158.2.216', '2022-03-16 17:16:02', '2022-03-16 17:16:02'),
(2171, '2022-03-16 00:00:00', '172.70.178.58', '2022-03-16 17:34:25', '2022-03-16 17:34:25'),
(2172, '2022-03-16 00:00:00', '108.162.216.138', '2022-03-16 18:10:07', '2022-03-16 18:10:07'),
(2173, '2022-03-16 00:00:00', '162.158.166.100', '2022-03-16 18:34:38', '2022-03-16 18:34:38'),
(2174, '2022-03-16 00:00:00', '108.162.246.117', '2022-03-16 18:34:39', '2022-03-16 18:34:39'),
(2175, '2022-03-16 00:00:00', '108.162.246.7', '2022-03-16 18:34:40', '2022-03-16 18:34:40'),
(2176, '2022-03-16 00:00:00', '108.162.246.105', '2022-03-16 18:34:40', '2022-03-16 18:34:40'),
(2177, '2022-03-16 00:00:00', '162.158.106.94', '2022-03-16 18:34:41', '2022-03-16 18:34:41'),
(2178, '2022-03-16 00:00:00', '162.158.166.118', '2022-03-16 18:34:41', '2022-03-16 18:34:41'),
(2179, '2022-03-16 00:00:00', '172.68.133.156', '2022-03-16 18:34:41', '2022-03-16 18:34:41'),
(2180, '2022-03-16 00:00:00', '172.70.206.32', '2022-03-16 18:34:42', '2022-03-16 18:34:42'),
(2181, '2022-03-16 00:00:00', '172.70.210.54', '2022-03-16 18:34:43', '2022-03-16 18:34:43'),
(2182, '2022-03-16 00:00:00', '162.158.107.167', '2022-03-16 18:34:43', '2022-03-16 18:34:43'),
(2183, '2022-03-16 00:00:00', '162.158.106.146', '2022-03-16 18:34:44', '2022-03-16 18:34:44');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(2184, '2022-03-16 00:00:00', '172.68.132.171', '2022-03-16 18:34:44', '2022-03-16 18:34:44'),
(2185, '2022-03-16 00:00:00', '108.162.245.204', '2022-03-16 18:34:45', '2022-03-16 18:34:45'),
(2186, '2022-03-16 00:00:00', '162.158.107.151', '2022-03-16 18:34:46', '2022-03-16 18:34:46'),
(2187, '2022-03-16 00:00:00', '172.70.210.226', '2022-03-16 18:34:46', '2022-03-16 18:34:46'),
(2188, '2022-03-16 00:00:00', '162.158.107.127', '2022-03-16 18:34:46', '2022-03-16 18:34:46'),
(2189, '2022-03-16 00:00:00', '162.158.106.124', '2022-03-16 18:34:47', '2022-03-16 18:34:47'),
(2190, '2022-03-16 00:00:00', '172.69.34.7', '2022-03-16 18:34:51', '2022-03-16 18:34:51'),
(2191, '2022-03-16 00:00:00', '172.70.214.118', '2022-03-16 18:34:52', '2022-03-16 18:34:52'),
(2192, '2022-03-16 00:00:00', '172.70.206.172', '2022-03-16 18:34:54', '2022-03-16 18:34:54'),
(2193, '2022-03-16 00:00:00', '172.68.143.252', '2022-03-16 18:34:54', '2022-03-16 18:34:54'),
(2194, '2022-03-16 00:00:00', '162.158.166.92', '2022-03-16 18:34:55', '2022-03-16 18:34:55'),
(2195, '2022-03-16 00:00:00', '172.70.130.206', '2022-03-16 18:45:50', '2022-03-16 18:45:50'),
(2196, '2022-03-16 00:00:00', '172.70.130.228', '2022-03-16 19:21:33', '2022-03-16 19:21:33'),
(2197, '2022-03-16 00:00:00', '172.70.131.127', '2022-03-16 19:57:16', '2022-03-16 19:57:16'),
(2198, '2022-03-16 00:00:00', '108.162.216.192', '2022-03-16 20:32:59', '2022-03-16 20:32:59'),
(2199, '2022-03-16 00:00:00', '172.70.130.120', '2022-03-16 21:08:41', '2022-03-16 21:08:41'),
(2200, '2022-03-16 00:00:00', '172.70.131.33', '2022-03-16 21:44:25', '2022-03-16 21:44:25'),
(2201, '2022-03-16 00:00:00', '172.70.126.210', '2022-03-16 22:20:07', '2022-03-16 22:20:07'),
(2202, '2022-03-16 00:00:00', '108.162.216.220', '2022-03-16 22:55:51', '2022-03-16 22:55:51'),
(2203, '2022-03-16 00:00:00', '172.70.126.186', '2022-03-16 23:31:33', '2022-03-16 23:31:33'),
(2204, '2022-03-17 00:00:00', '108.162.216.228', '2022-03-17 00:07:16', '2022-03-17 00:07:16'),
(2205, '2022-03-17 00:00:00', '172.70.126.94', '2022-03-17 01:18:41', '2022-03-17 01:18:41'),
(2206, '2022-03-17 00:00:00', '172.70.206.34', '2022-03-17 01:53:13', '2022-03-17 01:53:13'),
(2207, '2022-03-17 00:00:00', '172.70.126.38', '2022-03-17 01:54:24', '2022-03-17 01:54:24'),
(2208, '2022-03-17 00:00:00', '108.162.216.6', '2022-03-17 02:30:07', '2022-03-17 02:30:07'),
(2209, '2022-03-17 00:00:00', '172.70.130.70', '2022-03-17 03:05:50', '2022-03-17 03:05:50'),
(2210, '2022-03-17 00:00:00', '172.70.131.45', '2022-03-17 05:28:41', '2022-03-17 05:28:41'),
(2211, '2022-03-17 00:00:00', '172.70.130.186', '2022-03-17 06:04:25', '2022-03-17 06:04:25'),
(2212, '2022-03-17 00:00:00', '172.70.178.134', '2022-03-17 06:40:07', '2022-03-17 06:40:07'),
(2213, '2022-03-17 00:00:00', '172.70.130.242', '2022-03-17 07:15:50', '2022-03-17 07:15:50'),
(2214, '2022-03-17 00:00:00', '172.70.178.52', '2022-03-17 07:51:33', '2022-03-17 07:51:33'),
(2215, '2022-03-17 00:00:00', '172.70.178.74', '2022-03-17 08:27:16', '2022-03-17 08:27:16'),
(2216, '2022-03-17 00:00:00', '108.162.216.228', '2022-03-17 09:38:41', '2022-03-17 09:38:41'),
(2217, '2022-03-17 00:00:00', '172.70.135.225', '2022-03-17 10:50:07', '2022-03-17 10:50:07'),
(2218, '2022-03-17 00:00:00', '172.69.170.92', '2022-03-17 11:17:03', '2022-03-17 11:17:03'),
(2219, '2022-03-17 00:00:00', '172.69.170.26', '2022-03-17 11:17:04', '2022-03-17 11:17:04'),
(2220, '2022-03-17 00:00:00', '172.70.135.217', '2022-03-17 12:01:33', '2022-03-17 12:01:33'),
(2221, '2022-03-17 00:00:00', '162.158.162.252', '2022-03-17 12:24:48', '2022-03-17 12:24:48'),
(2222, '2022-03-17 00:00:00', '172.70.134.244', '2022-03-17 12:37:16', '2022-03-17 12:37:16'),
(2223, '2022-03-17 00:00:00', '172.70.246.52', '2022-03-17 15:35:42', '2022-03-17 15:35:42'),
(2224, '2022-03-17 00:00:00', '172.70.174.170', '2022-03-17 16:47:16', '2022-03-17 16:47:16'),
(2225, '2022-03-17 00:00:00', '172.70.134.232', '2022-03-17 23:20:08', '2022-03-17 23:20:08'),
(2226, '2022-03-17 00:00:00', '172.70.34.74', '2022-03-17 23:24:48', '2022-03-17 23:24:48'),
(2227, '2022-03-17 00:00:00', '172.70.135.45', '2022-03-17 23:24:49', '2022-03-17 23:24:49'),
(2228, '2022-03-17 00:00:00', '172.70.135.201', '2022-03-17 23:24:50', '2022-03-17 23:24:50'),
(2229, '2022-03-17 00:00:00', '172.70.175.135', '2022-03-17 23:24:50', '2022-03-17 23:24:50'),
(2230, '2022-03-17 00:00:00', '172.70.134.8', '2022-03-17 23:24:50', '2022-03-17 23:24:50'),
(2231, '2022-03-17 00:00:00', '172.70.134.162', '2022-03-17 23:24:50', '2022-03-17 23:24:50'),
(2232, '2022-03-17 00:00:00', '172.70.85.34', '2022-03-17 23:40:50', '2022-03-17 23:40:50'),
(2233, '2022-03-17 00:00:00', '172.70.210.54', '2022-03-17 23:49:56', '2022-03-17 23:49:56'),
(2234, '2022-03-17 00:00:00', '172.70.174.2', '2022-03-17 23:55:50', '2022-03-17 23:55:50'),
(2235, '2022-03-18 00:00:00', '162.158.78.158', '2022-03-18 00:31:33', '2022-03-18 00:31:33'),
(2236, '2022-03-18 00:00:00', '172.70.134.192', '2022-03-18 01:07:16', '2022-03-18 01:07:16'),
(2237, '2022-03-18 00:00:00', '172.70.134.194', '2022-03-18 01:42:58', '2022-03-18 01:42:58'),
(2238, '2022-03-18 00:00:00', '172.70.134.194', '2022-03-18 04:05:50', '2022-03-18 04:05:50'),
(2239, '2022-03-18 00:00:00', '172.70.135.17', '2022-03-18 04:41:33', '2022-03-18 04:41:33'),
(2240, '2022-03-18 00:00:00', '108.162.241.44', '2022-03-18 05:18:26', '2022-03-18 05:18:26'),
(2241, '2022-03-18 00:00:00', '172.70.242.58', '2022-03-18 06:56:17', '2022-03-18 06:56:17'),
(2242, '2022-03-18 00:00:00', '172.70.34.104', '2022-03-18 07:04:24', '2022-03-18 07:04:24'),
(2243, '2022-03-18 00:00:00', '172.70.174.56', '2022-03-18 07:40:07', '2022-03-18 07:40:07'),
(2244, '2022-03-18 00:00:00', '172.70.134.102', '2022-03-18 08:15:50', '2022-03-18 08:15:50'),
(2245, '2022-03-18 00:00:00', '172.70.135.175', '2022-03-18 08:51:33', '2022-03-18 08:51:33'),
(2246, '2022-03-18 00:00:00', '162.158.222.184', '2022-03-18 09:04:55', '2022-03-18 09:04:55'),
(2247, '2022-03-18 00:00:00', '172.70.134.112', '2022-03-18 09:27:16', '2022-03-18 09:27:16'),
(2248, '2022-03-18 00:00:00', '172.70.175.223', '2022-03-18 10:02:59', '2022-03-18 10:02:59'),
(2249, '2022-03-18 00:00:00', '172.70.35.17', '2022-03-18 10:38:42', '2022-03-18 10:38:42'),
(2250, '2022-03-18 00:00:00', '162.158.111.11', '2022-03-18 11:13:25', '2022-03-18 11:13:25'),
(2251, '2022-03-18 00:00:00', '162.158.63.48', '2022-03-18 11:43:27', '2022-03-18 11:43:27'),
(2252, '2022-03-18 00:00:00', '108.162.245.4', '2022-03-18 11:50:07', '2022-03-18 11:50:07'),
(2253, '2022-03-18 00:00:00', '141.101.104.111', '2022-03-18 11:50:31', '2022-03-18 11:50:31'),
(2254, '2022-03-18 00:00:00', '172.70.251.125', '2022-03-18 12:06:52', '2022-03-18 12:06:52'),
(2255, '2022-03-18 00:00:00', '162.158.107.63', '2022-03-18 12:25:50', '2022-03-18 12:25:50'),
(2256, '2022-03-18 00:00:00', '172.70.175.175', '2022-03-18 12:55:52', '2022-03-18 12:55:52'),
(2257, '2022-03-18 00:00:00', '162.158.107.231', '2022-03-18 13:01:33', '2022-03-18 13:01:33'),
(2258, '2022-03-18 00:00:00', '172.70.91.113', '2022-03-18 14:06:14', '2022-03-18 14:06:14'),
(2259, '2022-03-18 00:00:00', '172.70.142.224', '2022-03-18 15:59:46', '2022-03-18 15:59:46'),
(2260, '2022-03-18 00:00:00', '108.162.246.205', '2022-03-18 16:00:07', '2022-03-18 16:00:07'),
(2261, '2022-03-18 00:00:00', '172.70.162.110', '2022-03-18 16:26:11', '2022-03-18 16:26:11'),
(2262, '2022-03-18 00:00:00', '108.162.245.222', '2022-03-18 17:47:16', '2022-03-18 17:47:16'),
(2263, '2022-03-18 00:00:00', '172.69.33.214', '2022-03-18 18:05:05', '2022-03-18 18:05:05'),
(2264, '2022-03-18 00:00:00', '172.68.245.143', '2022-03-18 18:51:49', '2022-03-18 18:51:49'),
(2265, '2022-03-18 00:00:00', '172.68.11.47', '2022-03-18 18:51:49', '2022-03-18 18:51:49'),
(2266, '2022-03-18 00:00:00', '172.68.10.202', '2022-03-18 18:51:50', '2022-03-18 18:51:50'),
(2267, '2022-03-18 00:00:00', '108.162.245.116', '2022-03-18 18:58:42', '2022-03-18 18:58:42'),
(2268, '2022-03-18 00:00:00', '162.158.107.181', '2022-03-18 19:57:59', '2022-03-18 19:57:59'),
(2269, '2022-03-18 00:00:00', '108.162.245.208', '2022-03-18 21:21:33', '2022-03-18 21:21:33'),
(2270, '2022-03-18 00:00:00', '172.69.33.180', '2022-03-18 21:45:08', '2022-03-18 21:45:08'),
(2271, '2022-03-18 00:00:00', '162.158.106.130', '2022-03-18 22:31:21', '2022-03-18 22:31:21'),
(2272, '2022-03-18 00:00:00', '162.158.106.164', '2022-03-18 22:32:58', '2022-03-18 22:32:58'),
(2273, '2022-03-18 00:00:00', '172.70.162.22', '2022-03-18 22:52:49', '2022-03-18 22:52:49'),
(2274, '2022-03-19 00:00:00', '108.162.246.73', '2022-03-19 00:20:07', '2022-03-19 00:20:07'),
(2275, '2022-03-19 00:00:00', '162.158.107.89', '2022-03-19 01:31:33', '2022-03-19 01:31:33'),
(2276, '2022-03-19 00:00:00', '162.158.106.230', '2022-03-19 02:42:59', '2022-03-19 02:42:59'),
(2277, '2022-03-19 00:00:00', '108.162.246.73', '2022-03-19 06:17:16', '2022-03-19 06:17:16'),
(2278, '2022-03-19 00:00:00', '108.162.238.43', '2022-03-19 09:20:54', '2022-03-19 09:20:54'),
(2279, '2022-03-19 00:00:00', '108.162.237.154', '2022-03-19 09:21:02', '2022-03-19 09:21:02'),
(2280, '2022-03-19 00:00:00', '162.158.79.119', '2022-03-19 11:01:23', '2022-03-19 11:01:23'),
(2281, '2022-03-19 00:00:00', '162.158.78.52', '2022-03-19 11:38:41', '2022-03-19 11:38:41'),
(2282, '2022-03-19 00:00:00', '172.70.135.191', '2022-03-19 14:37:16', '2022-03-19 14:37:16'),
(2283, '2022-03-19 00:00:00', '172.70.250.206', '2022-03-19 15:37:38', '2022-03-19 15:37:38'),
(2284, '2022-03-19 00:00:00', '141.101.107.166', '2022-03-19 15:45:15', '2022-03-19 15:45:15'),
(2285, '2022-03-19 00:00:00', '172.70.174.66', '2022-03-19 15:48:42', '2022-03-19 15:48:42'),
(2286, '2022-03-19 00:00:00', '172.70.135.135', '2022-03-19 16:24:24', '2022-03-19 16:24:24'),
(2287, '2022-03-19 00:00:00', '162.158.178.28', '2022-03-19 16:57:53', '2022-03-19 16:57:53'),
(2288, '2022-03-19 00:00:00', '172.70.34.232', '2022-03-19 17:35:50', '2022-03-19 17:35:50'),
(2289, '2022-03-19 00:00:00', '172.70.175.159', '2022-03-19 18:11:33', '2022-03-19 18:11:33'),
(2290, '2022-03-19 00:00:00', '172.70.174.82', '2022-03-19 18:47:16', '2022-03-19 18:47:16'),
(2291, '2022-03-19 00:00:00', '172.70.174.140', '2022-03-19 19:36:36', '2022-03-19 19:36:36'),
(2292, '2022-03-19 00:00:00', '172.70.175.107', '2022-03-19 19:58:41', '2022-03-19 19:58:41'),
(2293, '2022-03-19 00:00:00', '172.70.34.6', '2022-03-19 20:21:05', '2022-03-19 20:21:05'),
(2294, '2022-03-19 00:00:00', '162.158.38.100', '2022-03-19 21:37:13', '2022-03-19 21:37:13'),
(2295, '2022-03-19 00:00:00', '162.158.38.46', '2022-03-19 21:37:15', '2022-03-19 21:37:15'),
(2296, '2022-03-19 00:00:00', '172.70.34.142', '2022-03-19 21:50:38', '2022-03-19 21:50:38'),
(2297, '2022-03-20 00:00:00', '172.70.135.45', '2022-03-20 00:15:25', '2022-03-20 00:15:25'),
(2298, '2022-03-20 00:00:00', '172.70.135.35', '2022-03-20 00:32:47', '2022-03-20 00:32:47'),
(2299, '2022-03-20 00:00:00', '172.70.35.17', '2022-03-20 01:05:23', '2022-03-20 01:05:23'),
(2300, '2022-03-20 00:00:00', '162.158.78.40', '2022-03-20 01:14:14', '2022-03-20 01:14:14'),
(2301, '2022-03-20 00:00:00', '162.158.79.77', '2022-03-20 01:24:48', '2022-03-20 01:24:48'),
(2302, '2022-03-20 00:00:00', '172.70.174.66', '2022-03-20 01:24:49', '2022-03-20 01:24:49'),
(2303, '2022-03-20 00:00:00', '162.158.78.10', '2022-03-20 01:24:49', '2022-03-20 01:24:49'),
(2304, '2022-03-20 00:00:00', '172.70.35.27', '2022-03-20 01:24:50', '2022-03-20 01:24:50'),
(2305, '2022-03-20 00:00:00', '172.70.174.174', '2022-03-20 01:24:50', '2022-03-20 01:24:50'),
(2306, '2022-03-20 00:00:00', '172.70.134.210', '2022-03-20 01:24:50', '2022-03-20 01:24:50'),
(2307, '2022-03-20 00:00:00', '172.70.175.35', '2022-03-20 01:24:50', '2022-03-20 01:24:50'),
(2308, '2022-03-20 00:00:00', '172.70.174.168', '2022-03-20 01:24:51', '2022-03-20 01:24:51'),
(2309, '2022-03-20 00:00:00', '172.70.34.10', '2022-03-20 01:37:59', '2022-03-20 01:37:59'),
(2310, '2022-03-20 00:00:00', '172.70.34.180', '2022-03-20 01:43:38', '2022-03-20 01:43:38'),
(2311, '2022-03-20 00:00:00', '172.70.174.94', '2022-03-20 02:33:01', '2022-03-20 02:33:01'),
(2312, '2022-03-20 00:00:00', '172.70.135.55', '2022-03-20 04:02:32', '2022-03-20 04:02:32'),
(2313, '2022-03-20 00:00:00', '172.70.175.159', '2022-03-20 04:38:15', '2022-03-20 04:38:15'),
(2314, '2022-03-20 00:00:00', '172.70.174.60', '2022-03-20 05:13:58', '2022-03-20 05:13:58'),
(2315, '2022-03-20 00:00:00', '108.162.246.7', '2022-03-20 05:27:06', '2022-03-20 05:27:06'),
(2316, '2022-03-20 00:00:00', '172.70.188.206', '2022-03-20 05:28:21', '2022-03-20 05:28:21'),
(2317, '2022-03-20 00:00:00', '172.70.206.34', '2022-03-20 05:47:07', '2022-03-20 05:47:07'),
(2318, '2022-03-20 00:00:00', '172.70.134.158', '2022-03-20 05:49:41', '2022-03-20 05:49:41'),
(2319, '2022-03-20 00:00:00', '172.70.175.61', '2022-03-20 06:25:23', '2022-03-20 06:25:23'),
(2320, '2022-03-20 00:00:00', '172.70.34.146', '2022-03-20 07:01:06', '2022-03-20 07:01:06'),
(2321, '2022-03-20 00:00:00', '162.158.78.184', '2022-03-20 08:48:15', '2022-03-20 08:48:15'),
(2322, '2022-03-20 00:00:00', '172.70.34.132', '2022-03-20 09:59:41', '2022-03-20 09:59:41'),
(2323, '2022-03-20 00:00:00', '172.70.162.22', '2022-03-20 11:03:54', '2022-03-20 11:03:54'),
(2324, '2022-03-20 00:00:00', '172.70.162.240', '2022-03-20 11:38:37', '2022-03-20 11:38:37'),
(2325, '2022-03-20 00:00:00', '172.70.135.151', '2022-03-20 11:46:49', '2022-03-20 11:46:49'),
(2326, '2022-03-20 00:00:00', '172.70.162.30', '2022-03-20 12:09:44', '2022-03-20 12:09:44'),
(2327, '2022-03-20 00:00:00', '172.70.134.224', '2022-03-20 12:27:50', '2022-03-20 12:27:50'),
(2328, '2022-03-20 00:00:00', '162.158.78.182', '2022-03-20 12:34:15', '2022-03-20 12:34:15'),
(2329, '2022-03-20 00:00:00', '172.70.134.90', '2022-03-20 13:03:45', '2022-03-20 13:03:45'),
(2330, '2022-03-20 00:00:00', '172.70.175.69', '2022-03-20 13:16:34', '2022-03-20 13:16:34'),
(2331, '2022-03-20 00:00:00', '172.70.34.142', '2022-03-20 13:29:23', '2022-03-20 13:29:23'),
(2332, '2022-03-20 00:00:00', '172.70.174.224', '2022-03-20 13:42:12', '2022-03-20 13:42:12'),
(2333, '2022-03-20 00:00:00', '162.158.78.10', '2022-03-20 13:55:02', '2022-03-20 13:55:02'),
(2334, '2022-03-20 00:00:00', '172.70.135.217', '2022-03-20 14:24:09', '2022-03-20 14:24:09'),
(2335, '2022-03-20 00:00:00', '172.70.35.9', '2022-03-20 14:56:46', '2022-03-20 14:56:46'),
(2336, '2022-03-20 00:00:00', '172.70.134.246', '2022-03-20 15:13:04', '2022-03-20 15:13:04'),
(2337, '2022-03-20 00:00:00', '162.158.79.79', '2022-03-20 16:01:59', '2022-03-20 16:01:59'),
(2338, '2022-03-20 00:00:00', '172.70.135.21', '2022-03-20 16:18:17', '2022-03-20 16:18:17'),
(2339, '2022-03-20 00:00:00', '162.158.179.147', '2022-03-20 17:25:37', '2022-03-20 17:25:37'),
(2340, '2022-03-20 00:00:00', '172.70.175.181', '2022-03-20 17:29:42', '2022-03-20 17:29:42'),
(2341, '2022-03-20 00:00:00', '162.158.111.23', '2022-03-20 17:40:16', '2022-03-20 17:40:16'),
(2342, '2022-03-20 00:00:00', '172.70.174.214', '2022-03-20 18:05:25', '2022-03-20 18:05:25'),
(2343, '2022-03-20 00:00:00', '172.70.206.94', '2022-03-20 18:07:09', '2022-03-20 18:07:09'),
(2344, '2022-03-20 00:00:00', '172.70.34.162', '2022-03-20 19:52:34', '2022-03-20 19:52:34'),
(2345, '2022-03-20 00:00:00', '108.162.245.204', '2022-03-20 20:03:32', '2022-03-20 20:03:32'),
(2346, '2022-03-20 00:00:00', '172.70.175.233', '2022-03-20 20:28:17', '2022-03-20 20:28:17'),
(2347, '2022-03-20 00:00:00', '172.70.162.22', '2022-03-20 20:39:39', '2022-03-20 20:39:39'),
(2348, '2022-03-20 00:00:00', '172.70.175.147', '2022-03-20 22:15:25', '2022-03-20 22:15:25'),
(2349, '2022-03-20 00:00:00', '141.101.99.17', '2022-03-20 22:37:39', '2022-03-20 22:37:39'),
(2350, '2022-03-20 00:00:00', '172.70.134.98', '2022-03-20 22:51:08', '2022-03-20 22:51:08'),
(2351, '2022-03-20 00:00:00', '172.70.135.45', '2022-03-20 23:26:51', '2022-03-20 23:26:51'),
(2352, '2022-03-21 00:00:00', '172.70.135.103', '2022-03-21 00:02:34', '2022-03-21 00:02:34'),
(2353, '2022-03-21 00:00:00', '172.70.175.175', '2022-03-21 00:14:33', '2022-03-21 00:14:33'),
(2354, '2022-03-21 00:00:00', '172.70.174.14', '2022-03-21 00:31:21', '2022-03-21 00:31:21'),
(2355, '2022-03-21 00:00:00', '172.70.134.218', '2022-03-21 00:47:20', '2022-03-21 00:47:20'),
(2356, '2022-03-21 00:00:00', '172.70.35.25', '2022-03-21 01:09:44', '2022-03-21 01:09:44'),
(2357, '2022-03-21 00:00:00', '172.70.34.254', '2022-03-21 01:54:33', '2022-03-21 01:54:33'),
(2358, '2022-03-21 00:00:00', '172.70.34.244', '2022-03-21 02:16:54', '2022-03-21 02:16:54'),
(2359, '2022-03-21 00:00:00', '172.70.34.48', '2022-03-21 04:08:51', '2022-03-21 04:08:51'),
(2360, '2022-03-21 00:00:00', '141.101.105.132', '2022-03-21 04:14:54', '2022-03-21 04:14:54'),
(2361, '2022-03-21 00:00:00', '172.70.35.17', '2022-03-21 04:53:37', '2022-03-21 04:53:37'),
(2362, '2022-03-21 00:00:00', '162.158.78.108', '2022-03-21 05:16:00', '2022-03-21 05:16:00'),
(2363, '2022-03-21 00:00:00', '172.70.175.15', '2022-03-21 05:38:23', '2022-03-21 05:38:23'),
(2364, '2022-03-21 00:00:00', '172.70.174.16', '2022-03-21 06:00:46', '2022-03-21 06:00:46'),
(2365, '2022-03-21 00:00:00', '172.70.135.51', '2022-03-21 07:21:16', '2022-03-21 07:21:16'),
(2366, '2022-03-21 00:00:00', '172.70.175.69', '2022-03-21 09:08:24', '2022-03-21 09:08:24'),
(2367, '2022-03-21 00:00:00', '172.70.218.204', '2022-03-21 09:43:50', '2022-03-21 09:43:50'),
(2368, '2022-03-21 00:00:00', '172.70.34.120', '2022-03-21 10:19:50', '2022-03-21 10:19:50'),
(2369, '2022-03-21 00:00:00', '172.70.175.5', '2022-03-21 12:06:59', '2022-03-21 12:06:59'),
(2370, '2022-03-21 00:00:00', '172.70.175.39', '2022-03-21 13:01:23', '2022-03-21 13:01:23'),
(2371, '2022-03-21 00:00:00', '172.70.135.183', '2022-03-21 13:18:24', '2022-03-21 13:18:24'),
(2372, '2022-03-21 00:00:00', '162.158.78.62', '2022-03-21 15:52:39', '2022-03-21 15:52:39'),
(2373, '2022-03-21 00:00:00', '172.70.174.84', '2022-03-21 17:03:16', '2022-03-21 17:03:16'),
(2374, '2022-03-21 00:00:00', '172.70.175.221', '2022-03-21 17:38:59', '2022-03-21 17:38:59'),
(2375, '2022-03-21 00:00:00', '172.70.147.170', '2022-03-21 17:47:09', '2022-03-21 17:47:09'),
(2376, '2022-03-21 00:00:00', '172.69.33.60', '2022-03-21 18:23:55', '2022-03-21 18:23:55'),
(2377, '2022-03-21 00:00:00', '172.69.33.254', '2022-03-21 18:32:23', '2022-03-21 18:32:23'),
(2378, '2022-03-21 00:00:00', '172.70.135.175', '2022-03-21 19:50:01', '2022-03-21 19:50:01'),
(2379, '2022-03-21 00:00:00', '172.70.34.80', '2022-03-21 23:46:42', '2022-03-21 23:46:42'),
(2380, '2022-03-22 00:00:00', '141.101.77.200', '2022-03-22 00:07:16', '2022-03-22 00:07:16'),
(2381, '2022-03-22 00:00:00', '172.70.34.58', '2022-03-22 00:09:05', '2022-03-22 00:09:05'),
(2382, '2022-03-22 00:00:00', '172.70.135.79', '2022-03-22 00:31:28', '2022-03-22 00:31:28'),
(2383, '2022-03-22 00:00:00', '172.70.135.99', '2022-03-22 01:16:15', '2022-03-22 01:16:15'),
(2384, '2022-03-22 00:00:00', '172.69.33.44', '2022-03-22 01:18:29', '2022-03-22 01:18:29'),
(2385, '2022-03-22 00:00:00', '172.70.35.87', '2022-03-22 01:38:38', '2022-03-22 01:38:38'),
(2386, '2022-03-22 00:00:00', '172.70.174.78', '2022-03-22 02:01:01', '2022-03-22 02:01:01'),
(2387, '2022-03-22 00:00:00', '162.158.78.246', '2022-03-22 02:23:24', '2022-03-22 02:23:24'),
(2388, '2022-03-22 00:00:00', '172.70.207.19', '2022-03-22 03:59:38', '2022-03-22 03:59:38'),
(2389, '2022-03-22 00:00:00', '172.70.134.140', '2022-03-22 04:16:33', '2022-03-22 04:16:33'),
(2390, '2022-03-22 00:00:00', '141.101.68.230', '2022-03-22 04:22:12', '2022-03-22 04:22:12'),
(2391, '2022-03-22 00:00:00', '141.101.68.72', '2022-03-22 04:22:40', '2022-03-22 04:22:40'),
(2392, '2022-03-22 00:00:00', '162.158.50.47', '2022-03-22 04:22:50', '2022-03-22 04:22:50'),
(2393, '2022-03-22 00:00:00', '172.70.134.140', '2022-03-22 04:48:31', '2022-03-22 04:48:31'),
(2394, '2022-03-22 00:00:00', '172.70.134.140', '2022-03-22 06:06:45', '2022-03-22 06:06:45'),
(2395, '2022-03-22 00:00:00', '172.70.174.120', '2022-03-22 07:18:11', '2022-03-22 07:18:11'),
(2396, '2022-03-22 00:00:00', '172.70.114.10', '2022-03-22 09:38:40', '2022-03-22 09:38:40'),
(2397, '2022-03-22 00:00:00', '172.70.142.224', '2022-03-22 10:48:33', '2022-03-22 10:48:33'),
(2398, '2022-03-22 00:00:00', '172.70.38.156', '2022-03-22 11:28:11', '2022-03-22 11:28:11'),
(2399, '2022-03-22 00:00:00', '172.70.34.104', '2022-03-22 12:39:37', '2022-03-22 12:39:37'),
(2400, '2022-03-22 00:00:00', '172.70.91.97', '2022-03-22 13:09:21', '2022-03-22 13:09:21'),
(2401, '2022-03-22 00:00:00', '172.70.85.154', '2022-03-22 13:09:22', '2022-03-22 13:09:22'),
(2402, '2022-03-22 00:00:00', '162.158.106.212', '2022-03-22 14:16:26', '2022-03-22 14:16:26'),
(2403, '2022-03-22 00:00:00', '172.70.126.46', '2022-03-22 14:44:33', '2022-03-22 14:44:33'),
(2404, '2022-03-22 00:00:00', '172.70.126.44', '2022-03-22 14:44:34', '2022-03-22 14:44:34'),
(2405, '2022-03-22 00:00:00', '172.70.130.188', '2022-03-22 14:44:35', '2022-03-22 14:44:35'),
(2406, '2022-03-22 00:00:00', '172.70.178.30', '2022-03-22 14:44:35', '2022-03-22 14:44:35'),
(2407, '2022-03-22 00:00:00', '172.70.174.254', '2022-03-22 16:54:25', '2022-03-22 16:54:25'),
(2408, '2022-03-22 00:00:00', '172.70.134.84', '2022-03-22 18:05:51', '2022-03-22 18:05:51'),
(2409, '2022-03-22 00:00:00', '172.70.34.226', '2022-03-22 20:24:26', '2022-03-22 20:24:26'),
(2410, '2022-03-22 00:00:00', '172.70.135.85', '2022-03-22 21:16:27', '2022-03-22 21:16:27'),
(2411, '2022-03-22 00:00:00', '141.101.77.44', '2022-03-22 21:59:42', '2022-03-22 21:59:42'),
(2412, '2022-03-22 00:00:00', '172.70.135.155', '2022-03-22 22:01:13', '2022-03-22 22:01:13'),
(2413, '2022-03-22 00:00:00', '172.70.174.230', '2022-03-22 22:23:37', '2022-03-22 22:23:37'),
(2414, '2022-03-23 00:00:00', '172.70.174.84', '2022-03-23 01:00:48', '2022-03-23 01:00:48'),
(2415, '2022-03-23 00:00:00', '141.101.105.98', '2022-03-23 01:56:04', '2022-03-23 01:56:04'),
(2416, '2022-03-23 00:00:00', '172.70.175.165', '2022-03-23 02:12:13', '2022-03-23 02:12:13'),
(2417, '2022-03-23 00:00:00', '172.70.134.220', '2022-03-23 05:10:47', '2022-03-23 05:10:47'),
(2418, '2022-03-23 00:00:00', '141.101.77.44', '2022-03-23 06:50:13', '2022-03-23 06:50:13'),
(2419, '2022-03-23 00:00:00', '172.70.175.165', '2022-03-23 08:09:24', '2022-03-23 08:09:24'),
(2420, '2022-03-23 00:00:00', '172.70.34.160', '2022-03-23 10:32:13', '2022-03-23 10:32:13'),
(2421, '2022-03-23 00:00:00', '172.70.174.40', '2022-03-23 11:43:38', '2022-03-23 11:43:38'),
(2422, '2022-03-23 00:00:00', '172.70.175.159', '2022-03-23 13:30:47', '2022-03-23 13:30:47'),
(2423, '2022-03-23 00:00:00', '172.70.135.67', '2022-03-23 14:03:10', '2022-03-23 14:03:10'),
(2424, '2022-03-23 00:00:00', '172.70.135.141', '2022-03-23 14:37:57', '2022-03-23 14:37:57'),
(2425, '2022-03-23 00:00:00', '172.70.175.117', '2022-03-23 15:00:20', '2022-03-23 15:00:20'),
(2426, '2022-03-23 00:00:00', '172.70.38.224', '2022-03-23 15:27:22', '2022-03-23 15:27:22'),
(2427, '2022-03-23 00:00:00', '162.158.78.62', '2022-03-23 15:45:07', '2022-03-23 15:45:07'),
(2428, '2022-03-23 00:00:00', '162.158.251.56', '2022-03-23 16:35:01', '2022-03-23 16:35:01'),
(2429, '2022-03-23 00:00:00', '162.158.251.60', '2022-03-23 16:35:30', '2022-03-23 16:35:30'),
(2430, '2022-03-23 00:00:00', '162.158.251.126', '2022-03-23 16:35:32', '2022-03-23 16:35:32'),
(2431, '2022-03-23 00:00:00', '172.70.51.147', '2022-03-23 16:35:35', '2022-03-23 16:35:35'),
(2432, '2022-03-23 00:00:00', '172.70.51.147', '2022-03-23 16:35:35', '2022-03-23 16:35:35'),
(2433, '2022-03-23 00:00:00', '108.162.249.150', '2022-03-23 16:45:29', '2022-03-23 16:45:29'),
(2434, '2022-03-23 00:00:00', '172.70.38.254', '2022-03-23 17:54:38', '2022-03-23 17:54:38'),
(2435, '2022-03-23 00:00:00', '172.70.162.22', '2022-03-23 19:15:31', '2022-03-23 19:15:31'),
(2436, '2022-03-23 00:00:00', '172.70.174.166', '2022-03-23 19:41:47', '2022-03-23 19:41:47'),
(2437, '2022-03-23 00:00:00', '172.70.134.182', '2022-03-23 20:17:30', '2022-03-23 20:17:30'),
(2438, '2022-03-23 00:00:00', '172.70.174.166', '2022-03-23 22:03:00', '2022-03-23 22:03:00'),
(2439, '2022-03-23 00:00:00', '162.158.79.49', '2022-03-23 22:38:43', '2022-03-23 22:38:43'),
(2440, '2022-03-23 00:00:00', '172.70.135.217', '2022-03-23 23:14:26', '2022-03-23 23:14:26'),
(2441, '2022-03-23 00:00:00', '172.70.175.135', '2022-03-23 23:50:09', '2022-03-23 23:50:09'),
(2442, '2022-03-24 00:00:00', '172.70.34.164', '2022-03-24 00:25:52', '2022-03-24 00:25:52'),
(2443, '2022-03-24 00:00:00', '172.70.175.173', '2022-03-24 01:01:35', '2022-03-24 01:01:35'),
(2444, '2022-03-24 00:00:00', '172.70.135.21', '2022-03-24 01:37:18', '2022-03-24 01:37:18'),
(2445, '2022-03-24 00:00:00', '162.158.78.64', '2022-03-24 02:13:01', '2022-03-24 02:13:01'),
(2446, '2022-03-24 00:00:00', '172.70.251.51', '2022-03-24 03:39:19', '2022-03-24 03:39:19'),
(2447, '2022-03-24 00:00:00', '172.70.251.51', '2022-03-24 03:53:09', '2022-03-24 03:53:09'),
(2448, '2022-03-24 00:00:00', '172.70.246.180', '2022-03-24 03:53:17', '2022-03-24 03:53:17'),
(2449, '2022-03-24 00:00:00', '162.158.91.131', '2022-03-24 03:53:24', '2022-03-24 03:53:24'),
(2450, '2022-03-24 00:00:00', '162.158.91.29', '2022-03-24 03:53:31', '2022-03-24 03:53:31'),
(2451, '2022-03-24 00:00:00', '172.70.246.52', '2022-03-24 03:53:47', '2022-03-24 03:53:47'),
(2452, '2022-03-24 00:00:00', '172.70.246.74', '2022-03-24 03:53:52', '2022-03-24 03:53:52'),
(2453, '2022-03-24 00:00:00', '162.158.91.189', '2022-03-24 03:53:58', '2022-03-24 03:53:58'),
(2454, '2022-03-24 00:00:00', '162.158.92.46', '2022-03-24 03:54:12', '2022-03-24 03:54:12'),
(2455, '2022-03-24 00:00:00', '172.70.250.206', '2022-03-24 03:54:16', '2022-03-24 03:54:16'),
(2456, '2022-03-24 00:00:00', '162.158.90.108', '2022-03-24 03:54:21', '2022-03-24 03:54:21'),
(2457, '2022-03-24 00:00:00', '172.70.251.125', '2022-03-24 03:54:26', '2022-03-24 03:54:26'),
(2458, '2022-03-24 00:00:00', '162.158.90.208', '2022-03-24 03:54:39', '2022-03-24 03:54:39'),
(2459, '2022-03-24 00:00:00', '162.158.90.242', '2022-03-24 03:55:26', '2022-03-24 03:55:26'),
(2460, '2022-03-24 00:00:00', '172.70.242.128', '2022-03-24 03:57:18', '2022-03-24 03:57:18'),
(2461, '2022-03-24 00:00:00', '172.70.242.210', '2022-03-24 03:57:56', '2022-03-24 03:57:56'),
(2462, '2022-03-24 00:00:00', '162.158.90.154', '2022-03-24 03:58:38', '2022-03-24 03:58:38'),
(2463, '2022-03-24 00:00:00', '172.70.242.72', '2022-03-24 03:58:41', '2022-03-24 03:58:41'),
(2464, '2022-03-24 00:00:00', '162.158.91.31', '2022-03-24 03:58:50', '2022-03-24 03:58:50'),
(2465, '2022-03-24 00:00:00', '162.158.90.188', '2022-03-24 04:04:21', '2022-03-24 04:04:21'),
(2466, '2022-03-24 00:00:00', '172.70.250.64', '2022-03-24 04:04:34', '2022-03-24 04:04:34'),
(2467, '2022-03-24 00:00:00', '162.158.92.46', '2022-03-24 04:05:35', '2022-03-24 04:05:35'),
(2468, '2022-03-24 00:00:00', '172.70.251.125', '2022-03-24 04:12:30', '2022-03-24 04:12:30'),
(2469, '2022-03-24 00:00:00', '172.70.250.206', '2022-03-24 04:12:42', '2022-03-24 04:12:42'),
(2470, '2022-03-24 00:00:00', '162.158.90.20', '2022-03-24 04:13:21', '2022-03-24 04:13:21'),
(2471, '2022-03-24 00:00:00', '172.70.251.51', '2022-03-24 04:13:54', '2022-03-24 04:13:54'),
(2472, '2022-03-24 00:00:00', '162.158.90.26', '2022-03-24 04:13:57', '2022-03-24 04:13:57'),
(2473, '2022-03-24 00:00:00', '172.70.242.128', '2022-03-24 04:14:04', '2022-03-24 04:14:04'),
(2474, '2022-03-24 00:00:00', '162.158.90.108', '2022-03-24 04:14:09', '2022-03-24 04:14:09'),
(2475, '2022-03-24 00:00:00', '162.158.91.31', '2022-03-24 04:14:16', '2022-03-24 04:14:16'),
(2476, '2022-03-24 00:00:00', '172.70.242.72', '2022-03-24 04:14:21', '2022-03-24 04:14:21'),
(2477, '2022-03-24 00:00:00', '172.70.242.210', '2022-03-24 04:15:47', '2022-03-24 04:15:47'),
(2478, '2022-03-24 00:00:00', '172.70.246.52', '2022-03-24 04:16:19', '2022-03-24 04:16:19'),
(2479, '2022-03-24 00:00:00', '162.158.92.46', '2022-03-24 04:18:49', '2022-03-24 04:18:49'),
(2480, '2022-03-24 00:00:00', '162.158.92.188', '2022-03-24 04:19:00', '2022-03-24 04:19:00'),
(2481, '2022-03-24 00:00:00', '172.70.242.58', '2022-03-24 04:19:11', '2022-03-24 04:19:11'),
(2482, '2022-03-24 00:00:00', '172.70.251.51', '2022-03-24 04:33:04', '2022-03-24 04:33:04'),
(2483, '2022-03-24 00:00:00', '162.158.92.46', '2022-03-24 04:53:34', '2022-03-24 04:53:34'),
(2484, '2022-03-24 00:00:00', '108.162.246.45', '2022-03-24 06:03:50', '2022-03-24 06:03:50'),
(2485, '2022-03-24 00:00:00', '172.70.174.16', '2022-03-24 16:44:47', '2022-03-24 16:44:47'),
(2486, '2022-03-24 00:00:00', '162.158.222.124', '2022-03-24 17:57:38', '2022-03-24 17:57:38'),
(2487, '2022-03-24 00:00:00', '172.70.175.15', '2022-03-24 18:14:47', '2022-03-24 18:14:47'),
(2488, '2022-03-24 00:00:00', '172.69.33.202', '2022-03-24 19:15:19', '2022-03-24 19:15:19'),
(2489, '2022-03-24 00:00:00', '162.158.106.212', '2022-03-24 20:30:46', '2022-03-24 20:30:46'),
(2490, '2022-03-24 00:00:00', '108.162.246.105', '2022-03-24 20:30:47', '2022-03-24 20:30:47'),
(2491, '2022-03-24 00:00:00', '108.162.245.126', '2022-03-24 20:30:48', '2022-03-24 20:30:48'),
(2492, '2022-03-24 00:00:00', '162.158.107.127', '2022-03-24 20:30:50', '2022-03-24 20:30:50'),
(2493, '2022-03-24 00:00:00', '162.158.107.209', '2022-03-24 20:30:50', '2022-03-24 20:30:50'),
(2494, '2022-03-24 00:00:00', '108.162.245.204', '2022-03-24 20:30:52', '2022-03-24 20:30:52'),
(2495, '2022-03-24 00:00:00', '172.70.206.204', '2022-03-24 22:06:02', '2022-03-24 22:06:02'),
(2496, '2022-03-24 00:00:00', '172.70.134.176', '2022-03-24 23:17:37', '2022-03-24 23:17:37'),
(2497, '2022-03-24 00:00:00', '172.70.135.131', '2022-03-24 23:47:37', '2022-03-24 23:47:37'),
(2498, '2022-03-25 00:00:00', '172.70.142.42', '2022-03-25 00:04:57', '2022-03-25 00:04:57'),
(2499, '2022-03-25 00:00:00', '172.70.210.180', '2022-03-25 06:15:29', '2022-03-25 06:15:29'),
(2500, '2022-03-25 00:00:00', '162.158.78.220', '2022-03-25 07:09:44', '2022-03-25 07:09:44'),
(2501, '2022-03-25 00:00:00', '172.70.250.206', '2022-03-25 09:25:59', '2022-03-25 09:25:59'),
(2502, '2022-03-25 00:00:00', '172.70.250.64', '2022-03-25 09:26:00', '2022-03-25 09:26:00'),
(2503, '2022-03-25 00:00:00', '172.70.251.51', '2022-03-25 09:26:00', '2022-03-25 09:26:00'),
(2504, '2022-03-25 00:00:00', '172.68.245.17', '2022-03-25 12:00:43', '2022-03-25 12:00:43'),
(2505, '2022-03-25 00:00:00', '108.162.241.168', '2022-03-25 13:29:02', '2022-03-25 13:29:02'),
(2506, '2022-03-25 00:00:00', '172.70.211.39', '2022-03-25 16:55:15', '2022-03-25 16:55:15'),
(2507, '2022-03-25 00:00:00', '162.158.107.167', '2022-03-25 16:55:16', '2022-03-25 16:55:16'),
(2508, '2022-03-25 00:00:00', '162.158.107.151', '2022-03-25 16:55:17', '2022-03-25 16:55:17'),
(2509, '2022-03-25 00:00:00', '162.158.107.21', '2022-03-25 16:55:17', '2022-03-25 16:55:17'),
(2510, '2022-03-25 00:00:00', '162.158.107.69', '2022-03-25 16:55:17', '2022-03-25 16:55:17'),
(2511, '2022-03-25 00:00:00', '162.158.107.127', '2022-03-25 16:55:18', '2022-03-25 16:55:18'),
(2512, '2022-03-25 00:00:00', '162.158.107.133', '2022-03-25 16:55:18', '2022-03-25 16:55:18'),
(2513, '2022-03-25 00:00:00', '162.158.107.163', '2022-03-25 16:55:19', '2022-03-25 16:55:19'),
(2514, '2022-03-25 00:00:00', '172.70.211.157', '2022-03-25 16:55:19', '2022-03-25 16:55:19'),
(2515, '2022-03-25 00:00:00', '108.162.245.36', '2022-03-25 16:55:19', '2022-03-25 16:55:19'),
(2516, '2022-03-25 00:00:00', '162.158.107.209', '2022-03-25 16:55:20', '2022-03-25 16:55:20'),
(2517, '2022-03-25 00:00:00', '108.162.246.117', '2022-03-25 16:55:20', '2022-03-25 16:55:20'),
(2518, '2022-03-25 00:00:00', '172.70.211.23', '2022-03-25 16:55:20', '2022-03-25 16:55:20'),
(2519, '2022-03-25 00:00:00', '162.158.106.124', '2022-03-25 16:55:21', '2022-03-25 16:55:21'),
(2520, '2022-03-25 00:00:00', '108.162.245.204', '2022-03-25 16:55:21', '2022-03-25 16:55:21'),
(2521, '2022-03-25 00:00:00', '162.158.106.212', '2022-03-25 16:55:22', '2022-03-25 16:55:22'),
(2522, '2022-03-25 00:00:00', '172.70.206.94', '2022-03-25 16:55:22', '2022-03-25 16:55:22'),
(2523, '2022-03-25 00:00:00', '108.162.245.126', '2022-03-25 16:55:23', '2022-03-25 16:55:23'),
(2524, '2022-03-25 00:00:00', '108.162.245.214', '2022-03-25 16:55:23', '2022-03-25 16:55:23'),
(2525, '2022-03-25 00:00:00', '108.162.246.105', '2022-03-25 16:55:23', '2022-03-25 16:55:23'),
(2526, '2022-03-25 00:00:00', '108.162.246.49', '2022-03-25 16:55:25', '2022-03-25 16:55:25'),
(2527, '2022-03-25 00:00:00', '172.69.33.14', '2022-03-25 16:55:26', '2022-03-25 16:55:26'),
(2528, '2022-03-25 00:00:00', '172.69.33.242', '2022-03-25 16:55:28', '2022-03-25 16:55:28'),
(2529, '2022-03-25 00:00:00', '108.162.245.92', '2022-03-25 16:55:28', '2022-03-25 16:55:28'),
(2530, '2022-03-25 00:00:00', '162.158.107.181', '2022-03-25 16:55:28', '2022-03-25 16:55:28'),
(2531, '2022-03-25 00:00:00', '108.162.246.45', '2022-03-25 16:55:29', '2022-03-25 16:55:29'),
(2532, '2022-03-25 00:00:00', '172.70.210.226', '2022-03-25 16:55:29', '2022-03-25 16:55:29'),
(2533, '2022-03-25 00:00:00', '172.68.143.252', '2022-03-25 16:55:30', '2022-03-25 16:55:30'),
(2534, '2022-03-25 00:00:00', '162.158.166.232', '2022-03-25 16:55:30', '2022-03-25 16:55:30'),
(2535, '2022-03-25 00:00:00', '162.158.106.146', '2022-03-25 16:55:31', '2022-03-25 16:55:31'),
(2536, '2022-03-25 00:00:00', '172.68.133.64', '2022-03-25 16:55:31', '2022-03-25 16:55:31'),
(2537, '2022-03-25 00:00:00', '172.68.133.98', '2022-03-25 16:55:31', '2022-03-25 16:55:31'),
(2538, '2022-03-25 00:00:00', '172.68.133.70', '2022-03-25 16:55:34', '2022-03-25 16:55:34'),
(2539, '2022-03-25 00:00:00', '172.70.85.204', '2022-03-25 17:00:00', '2022-03-25 17:00:00'),
(2540, '2022-03-25 00:00:00', '172.70.142.224', '2022-03-25 17:21:35', '2022-03-25 17:21:35'),
(2541, '2022-03-25 00:00:00', '162.158.163.151', '2022-03-25 17:35:18', '2022-03-25 17:35:18'),
(2542, '2022-03-25 00:00:00', '141.101.105.98', '2022-03-25 17:37:47', '2022-03-25 17:37:47'),
(2543, '2022-03-25 00:00:00', '172.69.33.178', '2022-03-25 20:22:42', '2022-03-25 20:22:42'),
(2544, '2022-03-25 00:00:00', '172.70.211.23', '2022-03-25 21:35:43', '2022-03-25 21:35:43'),
(2545, '2022-03-26 00:00:00', '172.70.142.218', '2022-03-26 00:10:07', '2022-03-26 00:10:07'),
(2546, '2022-03-26 00:00:00', '172.70.92.226', '2022-03-26 00:42:17', '2022-03-26 00:42:17'),
(2547, '2022-03-26 00:00:00', '162.158.162.172', '2022-03-26 09:37:56', '2022-03-26 09:37:56'),
(2548, '2022-03-26 00:00:00', '172.70.142.34', '2022-03-26 10:14:25', '2022-03-26 10:14:25'),
(2549, '2022-03-26 00:00:00', '172.70.188.14', '2022-03-26 10:21:19', '2022-03-26 10:21:19'),
(2550, '2022-03-26 00:00:00', '172.70.188.14', '2022-03-26 11:46:35', '2022-03-26 11:46:35'),
(2551, '2022-03-26 00:00:00', '172.70.142.42', '2022-03-26 12:18:15', '2022-03-26 12:18:15'),
(2552, '2022-03-26 00:00:00', '172.69.170.150', '2022-03-26 12:18:58', '2022-03-26 12:18:58'),
(2553, '2022-03-26 00:00:00', '162.158.163.167', '2022-03-26 12:36:31', '2022-03-26 12:36:31'),
(2554, '2022-03-26 00:00:00', '172.70.206.34', '2022-03-26 13:05:45', '2022-03-26 13:05:45'),
(2555, '2022-03-26 00:00:00', '162.158.107.105', '2022-03-26 14:18:53', '2022-03-26 14:18:53'),
(2556, '2022-03-26 00:00:00', '141.101.69.75', '2022-03-26 15:14:44', '2022-03-26 15:14:44'),
(2557, '2022-03-26 00:00:00', '141.101.68.22', '2022-03-26 15:14:44', '2022-03-26 15:14:44'),
(2558, '2022-03-26 00:00:00', '162.158.22.166', '2022-03-26 15:14:45', '2022-03-26 15:14:45'),
(2559, '2022-03-26 00:00:00', '141.101.68.86', '2022-03-26 15:14:46', '2022-03-26 15:14:46'),
(2560, '2022-03-26 00:00:00', '162.158.22.116', '2022-03-26 15:14:47', '2022-03-26 15:14:47'),
(2561, '2022-03-26 00:00:00', '141.101.69.211', '2022-03-26 15:14:47', '2022-03-26 15:14:47'),
(2562, '2022-03-26 00:00:00', '162.158.50.29', '2022-03-26 15:14:48', '2022-03-26 15:14:48'),
(2563, '2022-03-26 00:00:00', '172.70.147.168', '2022-03-26 17:10:24', '2022-03-26 17:10:24'),
(2564, '2022-03-26 00:00:00', '162.158.163.15', '2022-03-26 17:28:21', '2022-03-26 17:28:21'),
(2565, '2022-03-26 00:00:00', '172.70.147.72', '2022-03-26 17:54:13', '2022-03-26 17:54:13'),
(2566, '2022-03-26 00:00:00', '172.70.142.224', '2022-03-26 19:58:01', '2022-03-26 19:58:01'),
(2567, '2022-03-26 00:00:00', '162.158.78.22', '2022-03-26 20:00:07', '2022-03-26 20:00:07'),
(2568, '2022-03-26 00:00:00', '172.70.142.218', '2022-03-26 20:04:28', '2022-03-26 20:04:28'),
(2569, '2022-03-26 00:00:00', '162.158.222.160', '2022-03-26 20:06:23', '2022-03-26 20:06:23'),
(2570, '2022-03-26 00:00:00', '172.70.134.180', '2022-03-26 20:39:32', '2022-03-26 20:39:32'),
(2571, '2022-03-26 00:00:00', '172.70.34.74', '2022-03-26 20:39:35', '2022-03-26 20:39:35'),
(2572, '2022-03-26 00:00:00', '172.70.134.210', '2022-03-26 20:39:35', '2022-03-26 20:39:35'),
(2573, '2022-03-26 00:00:00', '172.70.134.44', '2022-03-26 20:40:16', '2022-03-26 20:40:16'),
(2574, '2022-03-27 00:00:00', '172.70.130.188', '2022-03-27 00:11:38', '2022-03-27 00:11:38'),
(2575, '2022-03-27 00:00:00', '172.70.188.14', '2022-03-27 01:33:38', '2022-03-27 01:33:38'),
(2576, '2022-03-27 00:00:00', '172.70.134.90', '2022-03-27 01:34:38', '2022-03-27 01:34:38'),
(2577, '2022-03-27 00:00:00', '172.70.175.155', '2022-03-27 02:04:38', '2022-03-27 02:04:38'),
(2578, '2022-03-27 00:00:00', '172.70.147.28', '2022-03-27 02:28:04', '2022-03-27 02:28:04'),
(2579, '2022-03-27 00:00:00', '172.70.134.80', '2022-03-27 03:04:38', '2022-03-27 03:04:38'),
(2580, '2022-03-27 00:00:00', '172.70.61.200', '2022-03-27 03:38:27', '2022-03-27 03:38:27'),
(2581, '2022-03-27 00:00:00', '162.158.163.43', '2022-03-27 03:51:28', '2022-03-27 03:51:28'),
(2582, '2022-03-27 00:00:00', '172.70.174.194', '2022-03-27 04:04:38', '2022-03-27 04:04:38'),
(2583, '2022-03-27 00:00:00', '172.70.142.42', '2022-03-27 04:11:26', '2022-03-27 04:11:26'),
(2584, '2022-03-27 00:00:00', '172.70.188.14', '2022-03-27 04:17:07', '2022-03-27 04:17:07'),
(2585, '2022-03-27 00:00:00', '172.70.188.206', '2022-03-27 06:20:37', '2022-03-27 06:20:37'),
(2586, '2022-03-27 00:00:00', '172.70.142.34', '2022-03-27 08:30:51', '2022-03-27 08:30:51'),
(2587, '2022-03-27 00:00:00', '162.158.163.31', '2022-03-27 11:28:46', '2022-03-27 11:28:46'),
(2588, '2022-03-27 00:00:00', '172.70.110.148', '2022-03-27 11:53:34', '2022-03-27 11:53:34'),
(2589, '2022-03-27 00:00:00', '172.70.147.28', '2022-03-27 12:08:44', '2022-03-27 12:08:44'),
(2590, '2022-03-27 00:00:00', '172.70.134.116', '2022-03-27 12:49:35', '2022-03-27 12:49:35'),
(2591, '2022-03-27 00:00:00', '172.70.38.70', '2022-03-27 13:19:36', '2022-03-27 13:19:36'),
(2592, '2022-03-27 00:00:00', '172.70.174.190', '2022-03-27 13:49:35', '2022-03-27 13:49:35'),
(2593, '2022-03-27 00:00:00', '108.162.245.204', '2022-03-27 14:10:36', '2022-03-27 14:10:36'),
(2594, '2022-03-27 00:00:00', '172.70.34.160', '2022-03-27 14:19:35', '2022-03-27 14:19:35'),
(2595, '2022-03-27 00:00:00', '108.162.246.105', '2022-03-27 14:20:09', '2022-03-27 14:20:09'),
(2596, '2022-03-27 00:00:00', '172.70.34.120', '2022-03-27 14:49:35', '2022-03-27 14:49:35'),
(2597, '2022-03-27 00:00:00', '172.70.34.82', '2022-03-27 15:19:35', '2022-03-27 15:19:35'),
(2598, '2022-03-27 00:00:00', '141.101.76.44', '2022-03-27 16:27:19', '2022-03-27 16:27:19'),
(2599, '2022-03-27 00:00:00', '172.70.206.34', '2022-03-27 16:48:54', '2022-03-27 16:48:54'),
(2600, '2022-03-27 00:00:00', '172.70.114.144', '2022-03-27 19:31:41', '2022-03-27 19:31:41'),
(2601, '2022-03-27 00:00:00', '162.158.162.12', '2022-03-27 20:37:27', '2022-03-27 20:37:27'),
(2602, '2022-03-27 00:00:00', '172.70.188.138', '2022-03-27 20:37:27', '2022-03-27 20:37:27'),
(2603, '2022-03-28 00:00:00', '172.70.134.140', '2022-03-28 00:50:52', '2022-03-28 00:50:52'),
(2604, '2022-03-28 00:00:00', '172.70.135.81', '2022-03-28 00:50:57', '2022-03-28 00:50:57'),
(2605, '2022-03-28 00:00:00', '162.158.91.29', '2022-03-28 04:12:30', '2022-03-28 04:12:30'),
(2606, '2022-03-28 00:00:00', '162.158.106.146', '2022-03-28 06:50:24', '2022-03-28 06:50:24'),
(2607, '2022-03-28 00:00:00', '172.70.142.218', '2022-03-28 09:17:50', '2022-03-28 09:17:50'),
(2608, '2022-03-28 00:00:00', '162.158.158.247', '2022-03-28 10:36:29', '2022-03-28 10:36:29'),
(2609, '2022-03-28 00:00:00', '108.162.246.7', '2022-03-28 10:38:59', '2022-03-28 10:38:59'),
(2610, '2022-03-28 00:00:00', '108.162.246.105', '2022-03-28 10:39:03', '2022-03-28 10:39:03'),
(2611, '2022-03-28 00:00:00', '108.162.245.126', '2022-03-28 10:39:19', '2022-03-28 10:39:19'),
(2612, '2022-03-28 00:00:00', '162.158.106.146', '2022-03-28 10:39:34', '2022-03-28 10:39:34'),
(2613, '2022-03-28 00:00:00', '172.70.85.154', '2022-03-28 10:46:08', '2022-03-28 10:46:08'),
(2614, '2022-03-28 00:00:00', '172.70.246.180', '2022-03-28 10:46:19', '2022-03-28 10:46:19'),
(2615, '2022-03-28 00:00:00', '172.70.90.6', '2022-03-28 11:10:58', '2022-03-28 11:10:58'),
(2616, '2022-03-28 00:00:00', '172.70.214.66', '2022-03-28 13:00:51', '2022-03-28 13:00:51'),
(2617, '2022-03-28 00:00:00', '108.162.246.105', '2022-03-28 14:34:19', '2022-03-28 14:34:19'),
(2618, '2022-03-28 00:00:00', '172.70.162.12', '2022-03-28 15:55:09', '2022-03-28 15:55:09'),
(2619, '2022-03-28 00:00:00', '162.158.162.216', '2022-03-28 16:07:48', '2022-03-28 16:07:48'),
(2620, '2022-03-28 00:00:00', '172.70.188.14', '2022-03-28 16:39:21', '2022-03-28 16:39:21'),
(2621, '2022-03-28 00:00:00', '172.70.188.196', '2022-03-28 19:20:29', '2022-03-28 19:20:29'),
(2622, '2022-03-28 00:00:00', '172.70.142.218', '2022-03-28 19:58:01', '2022-03-28 19:58:01'),
(2623, '2022-03-28 00:00:00', '162.158.163.167', '2022-03-28 21:10:41', '2022-03-28 21:10:41'),
(2624, '2022-03-28 00:00:00', '172.70.188.138', '2022-03-28 21:10:42', '2022-03-28 21:10:42'),
(2625, '2022-03-29 00:00:00', '172.68.10.220', '2022-03-29 01:36:45', '2022-03-29 01:36:45'),
(2626, '2022-03-29 00:00:00', '172.70.147.168', '2022-03-29 02:07:41', '2022-03-29 02:07:41'),
(2627, '2022-03-29 00:00:00', '172.68.11.17', '2022-03-29 02:09:57', '2022-03-29 02:09:57'),
(2628, '2022-03-29 00:00:00', '172.70.147.28', '2022-03-29 02:43:56', '2022-03-29 02:43:56'),
(2629, '2022-03-29 00:00:00', '172.68.245.221', '2022-03-29 03:26:07', '2022-03-29 03:26:07'),
(2630, '2022-03-29 00:00:00', '172.70.85.150', '2022-03-29 03:45:37', '2022-03-29 03:45:37'),
(2631, '2022-03-29 00:00:00', '172.70.142.224', '2022-03-29 04:39:45', '2022-03-29 04:39:45'),
(2632, '2022-03-29 00:00:00', '172.70.189.41', '2022-03-29 04:59:48', '2022-03-29 04:59:48'),
(2633, '2022-03-29 00:00:00', '172.70.178.116', '2022-03-29 07:09:02', '2022-03-29 07:09:02'),
(2634, '2022-03-29 00:00:00', '172.70.178.194', '2022-03-29 07:14:50', '2022-03-29 07:14:50'),
(2635, '2022-03-29 00:00:00', '172.70.126.66', '2022-03-29 07:47:36', '2022-03-29 07:47:36'),
(2636, '2022-03-29 00:00:00', '172.70.126.42', '2022-03-29 08:26:51', '2022-03-29 08:26:51'),
(2637, '2022-03-29 00:00:00', '141.101.98.240', '2022-03-29 11:12:28', '2022-03-29 11:12:28'),
(2638, '2022-03-29 00:00:00', '172.70.130.142', '2022-03-29 12:48:27', '2022-03-29 12:48:27'),
(2639, '2022-03-29 00:00:00', '172.70.179.27', '2022-03-29 12:48:28', '2022-03-29 12:48:28'),
(2640, '2022-03-29 00:00:00', '172.70.130.100', '2022-03-29 12:48:29', '2022-03-29 12:48:29'),
(2641, '2022-03-29 00:00:00', '172.70.130.102', '2022-03-29 13:12:12', '2022-03-29 13:12:12'),
(2642, '2022-03-29 00:00:00', '172.70.85.150', '2022-03-29 13:44:06', '2022-03-29 13:44:06'),
(2643, '2022-03-29 00:00:00', '172.70.126.158', '2022-03-29 14:12:12', '2022-03-29 14:12:12'),
(2644, '2022-03-29 00:00:00', '162.158.107.21', '2022-03-29 14:34:35', '2022-03-29 14:34:35'),
(2645, '2022-03-29 00:00:00', '108.162.216.6', '2022-03-29 14:42:12', '2022-03-29 14:42:12'),
(2646, '2022-03-29 00:00:00', '172.70.126.98', '2022-03-29 15:12:12', '2022-03-29 15:12:12'),
(2647, '2022-03-29 00:00:00', '108.162.216.96', '2022-03-29 15:42:12', '2022-03-29 15:42:12'),
(2648, '2022-03-29 00:00:00', '108.162.216.38', '2022-03-29 16:12:12', '2022-03-29 16:12:12'),
(2649, '2022-03-29 00:00:00', '162.158.107.209', '2022-03-29 16:18:09', '2022-03-29 16:18:09'),
(2650, '2022-03-29 00:00:00', '172.70.206.94', '2022-03-29 16:20:26', '2022-03-29 16:20:26'),
(2651, '2022-03-29 00:00:00', '172.70.142.218', '2022-03-29 17:16:32', '2022-03-29 17:16:32'),
(2652, '2022-03-29 00:00:00', '172.70.147.72', '2022-03-29 17:22:07', '2022-03-29 17:22:07'),
(2653, '2022-03-29 00:00:00', '172.70.147.72', '2022-03-29 17:39:12', '2022-03-29 17:39:12'),
(2654, '2022-03-29 00:00:00', '172.70.142.34', '2022-03-29 17:50:41', '2022-03-29 17:50:41'),
(2655, '2022-03-29 00:00:00', '172.70.130.118', '2022-03-29 18:12:12', '2022-03-29 18:12:12'),
(2656, '2022-03-29 00:00:00', '172.69.33.214', '2022-03-29 19:56:53', '2022-03-29 19:56:53'),
(2657, '2022-03-29 00:00:00', '172.70.211.23', '2022-03-29 23:03:52', '2022-03-29 23:03:52'),
(2658, '2022-03-29 00:00:00', '172.70.147.72', '2022-03-29 23:37:02', '2022-03-29 23:37:02'),
(2659, '2022-03-30 00:00:00', '172.69.69.79', '2022-03-30 01:06:09', '2022-03-30 01:06:09'),
(2660, '2022-03-30 00:00:00', '172.70.142.42', '2022-03-30 01:26:37', '2022-03-30 01:26:37'),
(2661, '2022-03-30 00:00:00', '172.70.126.46', '2022-03-30 02:15:43', '2022-03-30 02:15:43'),
(2662, '2022-03-30 00:00:00', '172.70.126.168', '2022-03-30 02:45:44', '2022-03-30 02:45:44'),
(2663, '2022-03-30 00:00:00', '172.70.162.240', '2022-03-30 03:08:32', '2022-03-30 03:08:32'),
(2664, '2022-03-30 00:00:00', '172.70.131.51', '2022-03-30 03:15:43', '2022-03-30 03:15:43'),
(2665, '2022-03-30 00:00:00', '172.70.142.34', '2022-03-30 03:43:23', '2022-03-30 03:43:23'),
(2666, '2022-03-30 00:00:00', '172.70.214.182', '2022-03-30 04:29:03', '2022-03-30 04:29:03'),
(2667, '2022-03-30 00:00:00', '172.70.92.226', '2022-03-30 04:33:05', '2022-03-30 04:33:05'),
(2668, '2022-03-30 00:00:00', '162.158.222.160', '2022-03-30 07:22:37', '2022-03-30 07:22:37'),
(2669, '2022-03-30 00:00:00', '162.158.163.163', '2022-03-30 12:19:28', '2022-03-30 12:19:28'),
(2670, '2022-03-30 00:00:00', '172.70.188.206', '2022-03-30 14:11:38', '2022-03-30 14:11:38'),
(2671, '2022-03-30 00:00:00', '172.70.110.148', '2022-03-30 15:07:09', '2022-03-30 15:07:09'),
(2672, '2022-03-30 00:00:00', '172.70.230.50', '2022-03-30 15:07:28', '2022-03-30 15:07:28'),
(2673, '2022-03-30 00:00:00', '172.70.142.218', '2022-03-30 18:58:40', '2022-03-30 18:58:40'),
(2674, '2022-03-30 00:00:00', '172.70.142.48', '2022-03-30 18:58:45', '2022-03-30 18:58:45'),
(2675, '2022-03-30 00:00:00', '172.70.174.152', '2022-03-30 18:59:22', '2022-03-30 18:59:22'),
(2676, '2022-03-30 00:00:00', '172.70.175.117', '2022-03-30 20:59:22', '2022-03-30 20:59:22'),
(2677, '2022-03-30 00:00:00', '162.158.162.8', '2022-03-30 22:18:47', '2022-03-30 22:18:47'),
(2678, '2022-03-31 00:00:00', '108.162.242.61', '2022-03-31 00:16:16', '2022-03-31 00:16:16'),
(2679, '2022-03-31 00:00:00', '108.162.242.11', '2022-03-31 00:16:26', '2022-03-31 00:16:26'),
(2680, '2022-03-31 00:00:00', '162.158.163.171', '2022-03-31 01:06:55', '2022-03-31 01:06:55'),
(2681, '2022-03-31 00:00:00', '172.70.174.190', '2022-03-31 01:40:45', '2022-03-31 01:40:45'),
(2682, '2022-03-31 00:00:00', '172.70.188.206', '2022-03-31 02:09:08', '2022-03-31 02:09:08'),
(2683, '2022-03-31 00:00:00', '172.70.147.72', '2022-03-31 04:54:40', '2022-03-31 04:54:40'),
(2684, '2022-03-31 00:00:00', '172.70.142.34', '2022-03-31 05:00:20', '2022-03-31 05:00:20'),
(2685, '2022-03-31 00:00:00', '172.70.175.221', '2022-03-31 05:01:34', '2022-03-31 05:01:34'),
(2686, '2022-03-31 00:00:00', '172.70.142.42', '2022-03-31 05:25:41', '2022-03-31 05:25:41'),
(2687, '2022-03-31 00:00:00', '172.70.174.82', '2022-03-31 05:31:34', '2022-03-31 05:31:34'),
(2688, '2022-03-31 00:00:00', '172.70.135.131', '2022-03-31 06:01:34', '2022-03-31 06:01:34'),
(2689, '2022-03-31 00:00:00', '172.70.134.94', '2022-03-31 06:31:34', '2022-03-31 06:31:34'),
(2690, '2022-03-31 00:00:00', '172.70.34.218', '2022-03-31 07:01:34', '2022-03-31 07:01:34'),
(2691, '2022-03-31 00:00:00', '108.162.241.44', '2022-03-31 07:57:25', '2022-03-31 07:57:25'),
(2692, '2022-03-31 00:00:00', '108.162.241.16', '2022-03-31 07:57:29', '2022-03-31 07:57:29'),
(2693, '2022-03-31 00:00:00', '108.162.242.61', '2022-03-31 07:57:32', '2022-03-31 07:57:32'),
(2694, '2022-03-31 00:00:00', '172.70.142.224', '2022-03-31 09:02:34', '2022-03-31 09:02:34'),
(2695, '2022-03-31 00:00:00', '172.70.147.28', '2022-03-31 12:37:36', '2022-03-31 12:37:36'),
(2696, '2022-03-31 00:00:00', '172.70.147.28', '2022-03-31 13:16:19', '2022-03-31 13:16:19'),
(2697, '2022-03-31 00:00:00', '172.70.188.14', '2022-03-31 13:32:29', '2022-03-31 13:32:29'),
(2698, '2022-03-31 00:00:00', '172.70.147.168', '2022-03-31 13:38:31', '2022-03-31 13:38:31'),
(2699, '2022-03-31 00:00:00', '162.158.107.209', '2022-03-31 14:11:53', '2022-03-31 14:11:53'),
(2700, '2022-03-31 00:00:00', '162.158.159.88', '2022-03-31 15:35:16', '2022-03-31 15:35:16'),
(2701, '2022-03-31 00:00:00', '108.162.245.28', '2022-03-31 15:41:37', '2022-03-31 15:41:37'),
(2702, '2022-03-31 00:00:00', '108.162.245.222', '2022-03-31 16:11:36', '2022-03-31 16:11:36'),
(2703, '2022-03-31 00:00:00', '162.158.107.219', '2022-03-31 16:41:37', '2022-03-31 16:41:37'),
(2704, '2022-03-31 00:00:00', '162.158.106.212', '2022-03-31 17:11:36', '2022-03-31 17:11:36'),
(2705, '2022-03-31 00:00:00', '172.70.142.224', '2022-03-31 17:28:33', '2022-03-31 17:28:33'),
(2706, '2022-03-31 00:00:00', '141.101.77.200', '2022-03-31 23:41:37', '2022-03-31 23:41:37'),
(2707, '2022-04-01 00:00:00', '162.158.163.165', '2022-04-01 00:39:30', '2022-04-01 00:39:30'),
(2708, '2022-04-01 00:00:00', '172.68.10.4', '2022-04-01 01:55:48', '2022-04-01 01:55:48'),
(2709, '2022-04-01 00:00:00', '172.70.147.72', '2022-04-01 02:43:06', '2022-04-01 02:43:06'),
(2710, '2022-04-01 00:00:00', '172.68.10.248', '2022-04-01 04:02:09', '2022-04-01 04:02:09'),
(2711, '2022-04-01 00:00:00', '162.158.79.11', '2022-04-01 04:41:54', '2022-04-01 04:41:54'),
(2712, '2022-04-01 00:00:00', '172.70.206.34', '2022-04-01 04:48:07', '2022-04-01 04:48:07'),
(2713, '2022-04-01 00:00:00', '172.70.147.72', '2022-04-01 10:17:45', '2022-04-01 10:17:45'),
(2714, '2022-04-01 00:00:00', '172.70.189.41', '2022-04-01 11:18:15', '2022-04-01 11:18:15'),
(2715, '2022-04-01 00:00:00', '172.70.135.45', '2022-04-01 11:26:33', '2022-04-01 11:26:33'),
(2716, '2022-04-01 00:00:00', '162.158.163.41', '2022-04-01 12:31:57', '2022-04-01 12:31:57'),
(2717, '2022-04-01 00:00:00', '162.158.162.196', '2022-04-01 15:19:35', '2022-04-01 15:19:35'),
(2718, '2022-04-01 00:00:00', '172.70.142.224', '2022-04-01 15:19:40', '2022-04-01 15:19:40'),
(2719, '2022-04-01 00:00:00', '172.70.142.224', '2022-04-01 16:38:32', '2022-04-01 16:38:32'),
(2720, '2022-04-01 00:00:00', '172.70.142.218', '2022-04-01 16:54:10', '2022-04-01 16:54:10'),
(2721, '2022-04-01 00:00:00', '172.70.131.175', '2022-04-01 19:17:17', '2022-04-01 19:17:17');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(2722, '2022-04-01 00:00:00', '172.70.188.196', '2022-04-01 20:00:58', '2022-04-01 20:00:58'),
(2723, '2022-04-01 00:00:00', '172.69.69.151', '2022-04-01 20:17:17', '2022-04-01 20:17:17'),
(2724, '2022-04-01 00:00:00', '172.69.68.228', '2022-04-01 21:17:17', '2022-04-01 21:17:17'),
(2725, '2022-04-01 00:00:00', '108.162.221.216', '2022-04-01 22:17:17', '2022-04-01 22:17:17'),
(2726, '2022-04-01 00:00:00', '172.70.142.42', '2022-04-01 22:21:09', '2022-04-01 22:21:09'),
(2727, '2022-04-01 00:00:00', '172.70.188.14', '2022-04-01 22:22:35', '2022-04-01 22:22:35'),
(2728, '2022-04-01 00:00:00', '172.70.51.143', '2022-04-01 23:21:36', '2022-04-01 23:21:36'),
(2729, '2022-04-02 00:00:00', '172.70.210.226', '2022-04-02 01:51:09', '2022-04-02 01:51:09'),
(2730, '2022-04-02 00:00:00', '162.158.90.78', '2022-04-02 01:58:10', '2022-04-02 01:58:10'),
(2731, '2022-04-02 00:00:00', '172.70.251.125', '2022-04-02 02:25:24', '2022-04-02 02:25:24'),
(2732, '2022-04-02 00:00:00', '172.68.110.126', '2022-04-02 02:25:25', '2022-04-02 02:25:25'),
(2733, '2022-04-02 00:00:00', '172.70.242.128', '2022-04-02 02:25:26', '2022-04-02 02:25:26'),
(2734, '2022-04-02 00:00:00', '162.158.179.24', '2022-04-02 02:38:39', '2022-04-02 02:38:39'),
(2735, '2022-04-02 00:00:00', '162.158.107.105', '2022-04-02 02:51:27', '2022-04-02 02:51:27'),
(2736, '2022-04-02 00:00:00', '172.70.251.125', '2022-04-02 02:54:54', '2022-04-02 02:54:54'),
(2737, '2022-04-02 00:00:00', '172.70.242.54', '2022-04-02 02:55:38', '2022-04-02 02:55:38'),
(2738, '2022-04-02 00:00:00', '108.162.246.7', '2022-04-02 02:57:16', '2022-04-02 02:57:16'),
(2739, '2022-04-02 00:00:00', '172.70.142.218', '2022-04-02 04:31:21', '2022-04-02 04:31:21'),
(2740, '2022-04-02 00:00:00', '108.162.229.120', '2022-04-02 05:14:22', '2022-04-02 05:14:22'),
(2741, '2022-04-02 00:00:00', '108.162.229.150', '2022-04-02 05:23:29', '2022-04-02 05:23:29'),
(2742, '2022-04-02 00:00:00', '162.158.50.5', '2022-04-02 05:23:32', '2022-04-02 05:23:32'),
(2743, '2022-04-02 00:00:00', '141.101.69.95', '2022-04-02 05:24:01', '2022-04-02 05:24:01'),
(2744, '2022-04-02 00:00:00', '108.162.229.148', '2022-04-02 05:24:12', '2022-04-02 05:24:12'),
(2745, '2022-04-02 00:00:00', '162.158.50.109', '2022-04-02 05:24:15', '2022-04-02 05:24:15'),
(2746, '2022-04-02 00:00:00', '141.101.68.154', '2022-04-02 05:25:09', '2022-04-02 05:25:09'),
(2747, '2022-04-02 00:00:00', '141.101.68.50', '2022-04-02 05:25:42', '2022-04-02 05:25:42'),
(2748, '2022-04-02 00:00:00', '141.101.69.249', '2022-04-02 05:25:53', '2022-04-02 05:25:53'),
(2749, '2022-04-02 00:00:00', '108.162.246.7', '2022-04-02 06:15:29', '2022-04-02 06:15:29'),
(2750, '2022-04-02 00:00:00', '172.69.68.181', '2022-04-02 07:04:33', '2022-04-02 07:04:33'),
(2751, '2022-04-02 00:00:00', '172.70.206.34', '2022-04-02 07:08:17', '2022-04-02 07:08:17'),
(2752, '2022-04-02 00:00:00', '172.70.131.145', '2022-04-02 07:34:33', '2022-04-02 07:34:33'),
(2753, '2022-04-02 00:00:00', '172.69.68.121', '2022-04-02 08:34:34', '2022-04-02 08:34:34'),
(2754, '2022-04-02 00:00:00', '172.70.179.5', '2022-04-02 09:17:31', '2022-04-02 09:17:31'),
(2755, '2022-04-02 00:00:00', '172.70.147.42', '2022-04-02 09:35:40', '2022-04-02 09:35:40'),
(2756, '2022-04-02 00:00:00', '108.162.216.48', '2022-04-02 10:17:31', '2022-04-02 10:17:31'),
(2757, '2022-04-02 00:00:00', '172.70.175.9', '2022-04-02 11:17:31', '2022-04-02 11:17:31'),
(2758, '2022-04-02 00:00:00', '172.70.135.201', '2022-04-02 12:17:32', '2022-04-02 12:17:32'),
(2759, '2022-04-02 00:00:00', '172.70.174.200', '2022-04-02 12:43:36', '2022-04-02 12:43:36'),
(2760, '2022-04-02 00:00:00', '172.70.135.201', '2022-04-02 13:17:31', '2022-04-02 13:17:31'),
(2761, '2022-04-02 00:00:00', '172.70.142.218', '2022-04-02 17:25:47', '2022-04-02 17:25:47'),
(2762, '2022-04-02 00:00:00', '172.70.130.236', '2022-04-02 17:51:27', '2022-04-02 17:51:27'),
(2763, '2022-04-02 00:00:00', '162.158.107.163', '2022-04-02 19:38:34', '2022-04-02 19:38:34'),
(2764, '2022-04-02 00:00:00', '172.70.174.182', '2022-04-02 20:09:06', '2022-04-02 20:09:06'),
(2765, '2022-04-02 00:00:00', '172.70.135.19', '2022-04-02 20:09:29', '2022-04-02 20:09:29'),
(2766, '2022-04-02 00:00:00', '172.70.38.146', '2022-04-02 20:09:30', '2022-04-02 20:09:30'),
(2767, '2022-04-02 00:00:00', '172.70.38.98', '2022-04-02 20:09:31', '2022-04-02 20:09:31'),
(2768, '2022-04-02 00:00:00', '172.70.134.134', '2022-04-02 20:09:31', '2022-04-02 20:09:31'),
(2769, '2022-04-02 00:00:00', '172.70.134.8', '2022-04-02 20:09:31', '2022-04-02 20:09:31'),
(2770, '2022-04-02 00:00:00', '172.70.210.180', '2022-04-02 21:52:40', '2022-04-02 21:52:40'),
(2771, '2022-04-03 00:00:00', '162.158.163.9', '2022-04-03 01:14:00', '2022-04-03 01:14:00'),
(2772, '2022-04-03 00:00:00', '162.158.222.140', '2022-04-03 01:45:04', '2022-04-03 01:45:04'),
(2773, '2022-04-03 00:00:00', '172.70.188.206', '2022-04-03 03:23:30', '2022-04-03 03:23:30'),
(2774, '2022-04-03 00:00:00', '172.70.142.42', '2022-04-03 04:04:06', '2022-04-03 04:04:06'),
(2775, '2022-04-03 00:00:00', '141.101.99.85', '2022-04-03 05:56:39', '2022-04-03 05:56:39'),
(2776, '2022-04-03 00:00:00', '172.70.85.152', '2022-04-03 05:56:42', '2022-04-03 05:56:42'),
(2777, '2022-04-03 00:00:00', '172.69.134.94', '2022-04-03 07:18:41', '2022-04-03 07:18:41'),
(2778, '2022-04-03 00:00:00', '108.162.245.92', '2022-04-03 07:18:41', '2022-04-03 07:18:41'),
(2779, '2022-04-03 00:00:00', '108.162.246.49', '2022-04-03 07:18:42', '2022-04-03 07:18:42'),
(2780, '2022-04-03 00:00:00', '162.158.107.163', '2022-04-03 07:18:42', '2022-04-03 07:18:42'),
(2781, '2022-04-03 00:00:00', '162.158.106.94', '2022-04-03 07:18:42', '2022-04-03 07:18:42'),
(2782, '2022-04-03 00:00:00', '108.162.245.126', '2022-04-03 07:18:44', '2022-04-03 07:18:44'),
(2783, '2022-04-03 00:00:00', '172.70.211.23', '2022-04-03 07:18:44', '2022-04-03 07:18:44'),
(2784, '2022-04-03 00:00:00', '172.70.210.180', '2022-04-03 07:18:45', '2022-04-03 07:18:45'),
(2785, '2022-04-03 00:00:00', '108.162.246.45', '2022-04-03 07:18:45', '2022-04-03 07:18:45'),
(2786, '2022-04-03 00:00:00', '162.158.107.151', '2022-04-03 07:18:46', '2022-04-03 07:18:46'),
(2787, '2022-04-03 00:00:00', '162.158.107.127', '2022-04-03 07:18:46', '2022-04-03 07:18:46'),
(2788, '2022-04-03 00:00:00', '172.68.143.220', '2022-04-03 07:18:47', '2022-04-03 07:18:47'),
(2789, '2022-04-03 00:00:00', '172.69.33.60', '2022-04-03 07:18:47', '2022-04-03 07:18:47'),
(2790, '2022-04-03 00:00:00', '108.162.245.214', '2022-04-03 07:18:47', '2022-04-03 07:18:47'),
(2791, '2022-04-03 00:00:00', '162.158.107.133', '2022-04-03 07:18:48', '2022-04-03 07:18:48'),
(2792, '2022-04-03 00:00:00', '162.158.106.124', '2022-04-03 07:18:48', '2022-04-03 07:18:48'),
(2793, '2022-04-03 00:00:00', '162.158.107.167', '2022-04-03 07:18:49', '2022-04-03 07:18:49'),
(2794, '2022-04-03 00:00:00', '172.68.133.246', '2022-04-03 07:18:49', '2022-04-03 07:18:49'),
(2795, '2022-04-03 00:00:00', '108.162.246.105', '2022-04-03 07:18:50', '2022-04-03 07:18:50'),
(2796, '2022-04-03 00:00:00', '172.70.214.32', '2022-04-03 07:18:50', '2022-04-03 07:18:50'),
(2797, '2022-04-03 00:00:00', '162.158.107.105', '2022-04-03 07:18:52', '2022-04-03 07:18:52'),
(2798, '2022-04-03 00:00:00', '172.70.206.94', '2022-04-03 07:18:52', '2022-04-03 07:18:52'),
(2799, '2022-04-03 00:00:00', '172.69.134.28', '2022-04-03 07:18:54', '2022-04-03 07:18:54'),
(2800, '2022-04-03 00:00:00', '172.68.133.64', '2022-04-03 07:18:56', '2022-04-03 07:18:56'),
(2801, '2022-04-03 00:00:00', '162.158.107.21', '2022-04-03 07:18:59', '2022-04-03 07:18:59'),
(2802, '2022-04-03 00:00:00', '172.68.143.212', '2022-04-03 07:18:59', '2022-04-03 07:18:59'),
(2803, '2022-04-03 00:00:00', '172.70.142.34', '2022-04-03 07:41:37', '2022-04-03 07:41:37'),
(2804, '2022-04-03 00:00:00', '172.70.142.224', '2022-04-03 08:51:45', '2022-04-03 08:51:45'),
(2805, '2022-04-03 00:00:00', '172.70.147.42', '2022-04-03 10:29:50', '2022-04-03 10:29:50'),
(2806, '2022-04-03 00:00:00', '162.158.107.21', '2022-04-03 14:02:38', '2022-04-03 14:02:38'),
(2807, '2022-04-03 00:00:00', '162.158.222.160', '2022-04-03 15:47:57', '2022-04-03 15:47:57'),
(2808, '2022-04-03 00:00:00', '172.69.70.76', '2022-04-03 16:09:26', '2022-04-03 16:09:26'),
(2809, '2022-04-03 00:00:00', '172.70.142.34', '2022-04-03 16:37:05', '2022-04-03 16:37:05'),
(2810, '2022-04-03 00:00:00', '172.70.189.41', '2022-04-03 16:49:19', '2022-04-03 16:49:19'),
(2811, '2022-04-03 00:00:00', '172.70.130.234', '2022-04-03 19:48:20', '2022-04-03 19:48:20'),
(2812, '2022-04-03 00:00:00', '172.70.142.42', '2022-04-03 23:36:34', '2022-04-03 23:36:34'),
(2813, '2022-04-04 00:00:00', '172.70.210.180', '2022-04-04 00:10:41', '2022-04-04 00:10:41'),
(2814, '2022-04-04 00:00:00', '172.70.147.42', '2022-04-04 01:21:49', '2022-04-04 01:21:49'),
(2815, '2022-04-04 00:00:00', '172.70.251.7', '2022-04-04 03:07:11', '2022-04-04 03:07:11'),
(2816, '2022-04-04 00:00:00', '172.70.92.156', '2022-04-04 03:32:51', '2022-04-04 03:32:51'),
(2817, '2022-04-04 00:00:00', '172.70.92.150', '2022-04-04 04:59:03', '2022-04-04 04:59:03'),
(2818, '2022-04-04 00:00:00', '162.158.106.212', '2022-04-04 05:27:05', '2022-04-04 05:27:05'),
(2819, '2022-04-04 00:00:00', '172.70.211.23', '2022-04-04 07:56:46', '2022-04-04 07:56:46'),
(2820, '2022-04-04 00:00:00', '172.70.242.68', '2022-04-04 08:17:58', '2022-04-04 08:17:58'),
(2821, '2022-04-04 00:00:00', '172.70.130.234', '2022-04-04 11:01:52', '2022-04-04 11:01:52'),
(2822, '2022-04-04 00:00:00', '172.70.174.200', '2022-04-04 11:16:33', '2022-04-04 11:16:33'),
(2823, '2022-04-04 00:00:00', '162.158.106.212', '2022-04-04 14:21:49', '2022-04-04 14:21:49'),
(2824, '2022-04-04 00:00:00', '141.101.105.176', '2022-04-04 14:45:40', '2022-04-04 14:45:40'),
(2825, '2022-04-04 00:00:00', '172.70.92.150', '2022-04-04 17:24:21', '2022-04-04 17:24:21'),
(2826, '2022-04-04 00:00:00', '172.70.214.182', '2022-04-04 21:23:50', '2022-04-04 21:23:50'),
(2827, '2022-04-04 00:00:00', '172.70.135.45', '2022-04-04 22:09:29', '2022-04-04 22:09:29'),
(2828, '2022-04-04 00:00:00', '172.70.134.232', '2022-04-04 22:09:31', '2022-04-04 22:09:31'),
(2829, '2022-04-04 00:00:00', '172.70.34.110', '2022-04-04 22:09:31', '2022-04-04 22:09:31'),
(2830, '2022-04-04 00:00:00', '172.70.188.196', '2022-04-04 22:52:12', '2022-04-04 22:52:12'),
(2831, '2022-04-04 00:00:00', '172.69.71.168', '2022-04-04 23:34:32', '2022-04-04 23:34:32'),
(2832, '2022-04-05 00:00:00', '162.158.162.34', '2022-04-05 00:37:29', '2022-04-05 00:37:29'),
(2833, '2022-04-05 00:00:00', '172.70.174.106', '2022-04-05 00:50:49', '2022-04-05 00:50:49'),
(2834, '2022-04-05 00:00:00', '162.158.163.67', '2022-04-05 01:54:18', '2022-04-05 01:54:18'),
(2835, '2022-04-05 00:00:00', '172.70.135.81', '2022-04-05 03:05:44', '2022-04-05 03:05:44'),
(2836, '2022-04-05 00:00:00', '172.70.135.45', '2022-04-05 08:01:53', '2022-04-05 08:01:53'),
(2837, '2022-04-05 00:00:00', '172.70.92.150', '2022-04-05 12:50:02', '2022-04-05 12:50:02'),
(2838, '2022-04-05 00:00:00', '172.70.135.69', '2022-04-05 13:21:23', '2022-04-05 13:21:23'),
(2839, '2022-04-05 00:00:00', '172.70.174.62', '2022-04-05 13:51:23', '2022-04-05 13:51:23'),
(2840, '2022-04-05 00:00:00', '172.69.33.94', '2022-04-05 13:54:41', '2022-04-05 13:54:41'),
(2841, '2022-04-05 00:00:00', '172.70.135.201', '2022-04-05 15:51:23', '2022-04-05 15:51:23'),
(2842, '2022-04-05 00:00:00', '162.158.162.238', '2022-04-05 16:11:38', '2022-04-05 16:11:38'),
(2843, '2022-04-05 00:00:00', '172.70.174.116', '2022-04-05 16:21:23', '2022-04-05 16:21:23'),
(2844, '2022-04-05 00:00:00', '172.70.92.156', '2022-04-05 17:29:51', '2022-04-05 17:29:51'),
(2845, '2022-04-05 00:00:00', '172.70.92.156', '2022-04-05 18:02:39', '2022-04-05 18:02:39'),
(2846, '2022-04-05 00:00:00', '162.158.162.6', '2022-04-05 20:13:17', '2022-04-05 20:13:17'),
(2847, '2022-04-05 00:00:00', '172.70.142.34', '2022-04-05 20:16:09', '2022-04-05 20:16:09'),
(2848, '2022-04-05 00:00:00', '172.70.206.34', '2022-04-05 20:41:23', '2022-04-05 20:41:23'),
(2849, '2022-04-05 00:00:00', '172.70.142.42', '2022-04-05 22:07:04', '2022-04-05 22:07:04'),
(2850, '2022-04-05 00:00:00', '172.70.251.125', '2022-04-05 23:18:32', '2022-04-05 23:18:32'),
(2851, '2022-04-06 00:00:00', '172.70.34.200', '2022-04-06 00:09:37', '2022-04-06 00:09:37'),
(2852, '2022-04-06 00:00:00', '162.158.91.217', '2022-04-06 01:06:40', '2022-04-06 01:06:40'),
(2853, '2022-04-06 00:00:00', '172.70.134.184', '2022-04-06 01:09:37', '2022-04-06 01:09:37'),
(2854, '2022-04-06 00:00:00', '172.70.134.90', '2022-04-06 02:09:37', '2022-04-06 02:09:37'),
(2855, '2022-04-06 00:00:00', '172.70.135.141', '2022-04-06 03:09:37', '2022-04-06 03:09:37'),
(2856, '2022-04-06 00:00:00', '172.70.147.42', '2022-04-06 03:12:50', '2022-04-06 03:12:50'),
(2857, '2022-04-06 00:00:00', '162.158.78.62', '2022-04-06 04:09:37', '2022-04-06 04:09:37'),
(2858, '2022-04-06 00:00:00', '172.70.189.41', '2022-04-06 04:14:01', '2022-04-06 04:14:01'),
(2859, '2022-04-06 00:00:00', '172.70.189.41', '2022-04-06 04:40:24', '2022-04-06 04:40:24'),
(2860, '2022-04-06 00:00:00', '162.158.162.194', '2022-04-06 06:06:27', '2022-04-06 06:06:27'),
(2861, '2022-04-06 00:00:00', '172.70.135.99', '2022-04-06 06:38:10', '2022-04-06 06:38:10'),
(2862, '2022-04-06 00:00:00', '172.70.134.220', '2022-04-06 07:08:10', '2022-04-06 07:08:10'),
(2863, '2022-04-06 00:00:00', '172.70.134.98', '2022-04-06 07:23:10', '2022-04-06 07:23:10'),
(2864, '2022-04-06 00:00:00', '162.158.78.88', '2022-04-06 07:38:10', '2022-04-06 07:38:10'),
(2865, '2022-04-06 00:00:00', '172.70.246.170', '2022-04-06 08:14:14', '2022-04-06 08:14:14'),
(2866, '2022-04-06 00:00:00', '172.70.134.248', '2022-04-06 08:53:10', '2022-04-06 08:53:10'),
(2867, '2022-04-06 00:00:00', '172.70.38.228', '2022-04-06 09:23:11', '2022-04-06 09:23:11'),
(2868, '2022-04-06 00:00:00', '172.70.34.50', '2022-04-06 09:53:11', '2022-04-06 09:53:11'),
(2869, '2022-04-06 00:00:00', '172.70.38.246', '2022-04-06 10:53:10', '2022-04-06 10:53:10'),
(2870, '2022-04-06 00:00:00', '172.70.174.128', '2022-04-06 11:29:21', '2022-04-06 11:29:21'),
(2871, '2022-04-06 00:00:00', '172.70.134.4', '2022-04-06 11:53:10', '2022-04-06 11:53:10'),
(2872, '2022-04-06 00:00:00', '172.70.34.224', '2022-04-06 12:27:22', '2022-04-06 12:27:22'),
(2873, '2022-04-06 00:00:00', '172.70.134.8', '2022-04-06 12:27:23', '2022-04-06 12:27:23'),
(2874, '2022-04-06 00:00:00', '172.70.134.28', '2022-04-06 12:28:52', '2022-04-06 12:28:52'),
(2875, '2022-04-06 00:00:00', '172.70.147.42', '2022-04-06 12:58:01', '2022-04-06 12:58:01'),
(2876, '2022-04-06 00:00:00', '172.70.188.206', '2022-04-06 12:58:14', '2022-04-06 12:58:14'),
(2877, '2022-04-06 00:00:00', '162.158.162.66', '2022-04-06 12:58:29', '2022-04-06 12:58:29'),
(2878, '2022-04-06 00:00:00', '172.70.189.41', '2022-04-06 12:58:38', '2022-04-06 12:58:38'),
(2879, '2022-04-06 00:00:00', '172.70.135.35', '2022-04-06 13:04:42', '2022-04-06 13:04:42'),
(2880, '2022-04-06 00:00:00', '172.70.38.192', '2022-04-06 13:40:19', '2022-04-06 13:40:19'),
(2881, '2022-04-06 00:00:00', '162.158.106.212', '2022-04-06 14:19:28', '2022-04-06 14:19:28'),
(2882, '2022-04-06 00:00:00', '172.70.38.180', '2022-04-06 14:40:18', '2022-04-06 14:40:18'),
(2883, '2022-04-06 00:00:00', '172.70.38.20', '2022-04-06 15:40:18', '2022-04-06 15:40:18'),
(2884, '2022-04-06 00:00:00', '172.70.35.31', '2022-04-06 16:16:01', '2022-04-06 16:16:01'),
(2885, '2022-04-06 00:00:00', '172.70.147.42', '2022-04-06 17:29:09', '2022-04-06 17:29:09'),
(2886, '2022-04-06 00:00:00', '172.70.175.125', '2022-04-06 18:03:10', '2022-04-06 18:03:10'),
(2887, '2022-04-06 00:00:00', '162.158.78.6', '2022-04-06 18:38:54', '2022-04-06 18:38:54'),
(2888, '2022-04-06 00:00:00', '172.70.135.139', '2022-04-06 19:14:35', '2022-04-06 19:14:35'),
(2889, '2022-04-06 00:00:00', '172.70.188.14', '2022-04-06 19:40:44', '2022-04-06 19:40:44'),
(2890, '2022-04-06 00:00:00', '172.70.174.24', '2022-04-06 20:26:01', '2022-04-06 20:26:01'),
(2891, '2022-04-06 00:00:00', '172.70.174.136', '2022-04-06 21:01:44', '2022-04-06 21:01:44'),
(2892, '2022-04-06 00:00:00', '172.70.38.170', '2022-04-06 22:13:11', '2022-04-06 22:13:11'),
(2893, '2022-04-06 00:00:00', '172.70.135.57', '2022-04-06 22:48:52', '2022-04-06 22:48:52'),
(2894, '2022-04-06 00:00:00', '172.70.174.58', '2022-04-06 23:24:35', '2022-04-06 23:24:35'),
(2895, '2022-04-06 00:00:00', '172.70.210.180', '2022-04-06 23:57:29', '2022-04-06 23:57:29'),
(2896, '2022-04-07 00:00:00', '172.70.175.35', '2022-04-07 00:09:06', '2022-04-07 00:09:06'),
(2897, '2022-04-07 00:00:00', '172.70.174.18', '2022-04-07 00:09:29', '2022-04-07 00:09:29'),
(2898, '2022-04-07 00:00:00', '162.158.79.77', '2022-04-07 00:09:31', '2022-04-07 00:09:31'),
(2899, '2022-04-07 00:00:00', '172.70.175.47', '2022-04-07 00:09:31', '2022-04-07 00:09:31'),
(2900, '2022-04-07 00:00:00', '172.70.175.135', '2022-04-07 00:36:01', '2022-04-07 00:36:01'),
(2901, '2022-04-07 00:00:00', '172.70.34.236', '2022-04-07 01:11:44', '2022-04-07 01:11:44'),
(2902, '2022-04-07 00:00:00', '162.158.163.67', '2022-04-07 02:10:17', '2022-04-07 02:10:17'),
(2903, '2022-04-07 00:00:00', '172.70.142.224', '2022-04-07 02:16:14', '2022-04-07 02:16:14'),
(2904, '2022-04-07 00:00:00', '172.70.38.98', '2022-04-07 02:23:10', '2022-04-07 02:23:10'),
(2905, '2022-04-07 00:00:00', '172.70.188.196', '2022-04-07 02:40:50', '2022-04-07 02:40:50'),
(2906, '2022-04-07 00:00:00', '172.70.142.218', '2022-04-07 03:03:15', '2022-04-07 03:03:15'),
(2907, '2022-04-07 00:00:00', '162.158.78.170', '2022-04-07 03:34:35', '2022-04-07 03:34:35'),
(2908, '2022-04-07 00:00:00', '172.70.214.188', '2022-04-07 03:45:43', '2022-04-07 03:45:43'),
(2909, '2022-04-07 00:00:00', '172.70.188.196', '2022-04-07 04:04:30', '2022-04-07 04:04:30'),
(2910, '2022-04-07 00:00:00', '162.158.78.46', '2022-04-07 04:10:18', '2022-04-07 04:10:18'),
(2911, '2022-04-07 00:00:00', '172.70.92.156', '2022-04-07 04:30:36', '2022-04-07 04:30:36'),
(2912, '2022-04-07 00:00:00', '172.70.34.214', '2022-04-07 05:21:44', '2022-04-07 05:21:44'),
(2913, '2022-04-07 00:00:00', '172.70.135.141', '2022-04-07 05:57:27', '2022-04-07 05:57:27'),
(2914, '2022-04-07 00:00:00', '172.70.174.168', '2022-04-07 06:15:48', '2022-04-07 06:15:48'),
(2915, '2022-04-07 00:00:00', '172.70.175.173', '2022-04-07 06:50:22', '2022-04-07 06:50:22'),
(2916, '2022-04-07 00:00:00', '162.158.162.80', '2022-04-07 07:29:47', '2022-04-07 07:29:47'),
(2917, '2022-04-07 00:00:00', '172.70.134.172', '2022-04-07 08:36:11', '2022-04-07 08:36:11'),
(2918, '2022-04-07 00:00:00', '172.68.10.28', '2022-04-07 09:48:17', '2022-04-07 09:48:17'),
(2919, '2022-04-07 00:00:00', '172.70.127.23', '2022-04-07 10:02:19', '2022-04-07 10:02:19'),
(2920, '2022-04-07 00:00:00', '172.70.54.242', '2022-04-07 10:47:02', '2022-04-07 10:47:02'),
(2921, '2022-04-07 00:00:00', '172.70.254.26', '2022-04-07 10:47:03', '2022-04-07 10:47:03'),
(2922, '2022-04-07 00:00:00', '172.70.38.118', '2022-04-07 10:51:53', '2022-04-07 10:51:53'),
(2923, '2022-04-07 00:00:00', '162.158.162.112', '2022-04-07 11:42:16', '2022-04-07 11:42:16'),
(2924, '2022-04-07 00:00:00', '162.158.162.238', '2022-04-07 11:47:49', '2022-04-07 11:47:49'),
(2925, '2022-04-07 00:00:00', '172.70.131.39', '2022-04-07 12:20:13', '2022-04-07 12:20:13'),
(2926, '2022-04-07 00:00:00', '162.158.103.185', '2022-04-07 12:54:07', '2022-04-07 12:54:07'),
(2927, '2022-04-07 00:00:00', '162.158.103.163', '2022-04-07 12:54:07', '2022-04-07 12:54:07'),
(2928, '2022-04-07 00:00:00', '162.158.103.187', '2022-04-07 12:54:08', '2022-04-07 12:54:08'),
(2929, '2022-04-07 00:00:00', '172.70.147.34', '2022-04-07 13:27:56', '2022-04-07 13:27:56'),
(2930, '2022-04-07 00:00:00', '172.70.92.150', '2022-04-07 14:13:27', '2022-04-07 14:13:27'),
(2931, '2022-04-07 00:00:00', '141.101.98.240', '2022-04-07 15:01:08', '2022-04-07 15:01:08'),
(2932, '2022-04-07 00:00:00', '172.70.178.132', '2022-04-07 15:18:47', '2022-04-07 15:18:47'),
(2933, '2022-04-07 00:00:00', '172.70.131.3', '2022-04-07 16:30:13', '2022-04-07 16:30:13'),
(2934, '2022-04-07 00:00:00', '162.158.163.167', '2022-04-07 18:52:55', '2022-04-07 18:52:55'),
(2935, '2022-04-07 00:00:00', '172.70.131.199', '2022-04-07 19:28:47', '2022-04-07 19:28:47'),
(2936, '2022-04-07 00:00:00', '172.70.147.34', '2022-04-07 20:21:53', '2022-04-07 20:21:53'),
(2937, '2022-04-07 00:00:00', '172.70.178.56', '2022-04-07 20:40:14', '2022-04-07 20:40:14'),
(2938, '2022-04-07 00:00:00', '172.70.131.79', '2022-04-07 21:15:56', '2022-04-07 21:15:56'),
(2939, '2022-04-07 00:00:00', '141.101.104.128', '2022-04-07 21:38:50', '2022-04-07 21:38:50'),
(2940, '2022-04-07 00:00:00', '172.69.33.42', '2022-04-07 21:49:05', '2022-04-07 21:49:05'),
(2941, '2022-04-07 00:00:00', '172.70.131.69', '2022-04-07 22:27:22', '2022-04-07 22:27:22'),
(2942, '2022-04-07 00:00:00', '172.70.178.148', '2022-04-07 22:27:41', '2022-04-07 22:27:41'),
(2943, '2022-04-07 00:00:00', '172.70.178.100', '2022-04-07 23:38:48', '2022-04-07 23:38:48'),
(2944, '2022-04-07 00:00:00', '108.162.221.216', '2022-04-07 23:46:09', '2022-04-07 23:46:09'),
(2945, '2022-04-07 00:00:00', '172.70.130.186', '2022-04-07 23:50:46', '2022-04-07 23:50:46'),
(2946, '2022-04-07 00:00:00', '172.70.131.39', '2022-04-07 23:52:55', '2022-04-07 23:52:55'),
(2947, '2022-04-07 00:00:00', '172.70.142.42', '2022-04-07 23:54:04', '2022-04-07 23:54:04'),
(2948, '2022-04-07 00:00:00', '172.68.11.73', '2022-04-07 23:58:09', '2022-04-07 23:58:09'),
(2949, '2022-04-07 00:00:00', '172.68.11.179', '2022-04-07 23:58:09', '2022-04-07 23:58:09'),
(2950, '2022-04-07 00:00:00', '172.68.244.176', '2022-04-07 23:58:10', '2022-04-07 23:58:10'),
(2951, '2022-04-08 00:00:00', '172.70.210.54', '2022-04-08 00:09:09', '2022-04-08 00:09:09'),
(2952, '2022-04-08 00:00:00', '108.162.216.102', '2022-04-08 01:15:14', '2022-04-08 01:15:14'),
(2953, '2022-04-08 00:00:00', '172.70.126.42', '2022-04-08 01:44:40', '2022-04-08 01:44:40'),
(2954, '2022-04-08 00:00:00', '172.70.131.101', '2022-04-08 01:52:54', '2022-04-08 01:52:54'),
(2955, '2022-04-08 00:00:00', '172.70.178.56', '2022-04-08 02:14:04', '2022-04-08 02:14:04'),
(2956, '2022-04-08 00:00:00', '162.158.107.209', '2022-04-08 03:10:58', '2022-04-08 03:10:58'),
(2957, '2022-04-08 00:00:00', '172.70.188.206', '2022-04-08 04:29:56', '2022-04-08 04:29:56'),
(2958, '2022-04-08 00:00:00', '172.70.188.206', '2022-04-08 04:51:46', '2022-04-08 04:51:46'),
(2959, '2022-04-08 00:00:00', '172.70.130.166', '2022-04-08 06:11:39', '2022-04-08 06:11:39'),
(2960, '2022-04-08 00:00:00', '172.70.131.147', '2022-04-08 07:58:47', '2022-04-08 07:58:47'),
(2961, '2022-04-08 00:00:00', '172.70.179.7', '2022-04-08 08:34:31', '2022-04-08 08:34:31'),
(2962, '2022-04-08 00:00:00', '172.70.126.228', '2022-04-08 09:10:13', '2022-04-08 09:10:13'),
(2963, '2022-04-08 00:00:00', '172.70.179.9', '2022-04-08 09:32:37', '2022-04-08 09:32:37'),
(2964, '2022-04-08 00:00:00', '172.70.38.170', '2022-04-08 10:39:46', '2022-04-08 10:39:46'),
(2965, '2022-04-08 00:00:00', '172.70.134.140', '2022-04-08 11:02:09', '2022-04-08 11:02:09'),
(2966, '2022-04-08 00:00:00', '172.70.134.254', '2022-04-08 11:24:34', '2022-04-08 11:24:34'),
(2967, '2022-04-08 00:00:00', '172.70.35.51', '2022-04-08 11:46:56', '2022-04-08 11:46:56'),
(2968, '2022-04-08 00:00:00', '172.70.38.196', '2022-04-08 12:09:19', '2022-04-08 12:09:19'),
(2969, '2022-04-08 00:00:00', '172.70.35.7', '2022-04-08 12:31:43', '2022-04-08 12:31:43'),
(2970, '2022-04-08 00:00:00', '172.70.34.110', '2022-04-08 13:29:49', '2022-04-08 13:29:49'),
(2971, '2022-04-08 00:00:00', '172.70.174.4', '2022-04-08 14:05:32', '2022-04-08 14:05:32'),
(2972, '2022-04-08 00:00:00', '172.70.175.207', '2022-04-08 14:27:22', '2022-04-08 14:27:22'),
(2973, '2022-04-08 00:00:00', '172.70.175.113', '2022-04-08 14:27:23', '2022-04-08 14:27:23'),
(2974, '2022-04-08 00:00:00', '172.70.174.180', '2022-04-08 15:52:40', '2022-04-08 15:52:40'),
(2975, '2022-04-08 00:00:00', '172.70.135.15', '2022-04-08 17:04:06', '2022-04-08 17:04:06'),
(2976, '2022-04-08 00:00:00', '172.70.188.206', '2022-04-08 17:18:29', '2022-04-08 17:18:29'),
(2977, '2022-04-08 00:00:00', '172.70.189.41', '2022-04-08 17:26:51', '2022-04-08 17:26:51'),
(2978, '2022-04-08 00:00:00', '172.70.38.68', '2022-04-08 17:39:49', '2022-04-08 17:39:49'),
(2979, '2022-04-08 00:00:00', '172.70.175.131', '2022-04-08 18:15:31', '2022-04-08 18:15:31'),
(2980, '2022-04-08 00:00:00', '172.70.134.166', '2022-04-08 18:51:14', '2022-04-08 18:51:14'),
(2981, '2022-04-08 00:00:00', '162.158.78.64', '2022-04-08 19:26:57', '2022-04-08 19:26:57'),
(2982, '2022-04-08 00:00:00', '172.70.175.43', '2022-04-08 20:02:40', '2022-04-08 20:02:40'),
(2983, '2022-04-08 00:00:00', '172.70.162.22', '2022-04-08 20:06:02', '2022-04-08 20:06:02'),
(2984, '2022-04-08 00:00:00', '172.70.135.113', '2022-04-08 20:38:23', '2022-04-08 20:38:23'),
(2985, '2022-04-08 00:00:00', '141.101.98.222', '2022-04-08 21:12:35', '2022-04-08 21:12:35'),
(2986, '2022-04-08 00:00:00', '172.70.135.91', '2022-04-08 22:25:32', '2022-04-08 22:25:32'),
(2987, '2022-04-08 00:00:00', '172.70.175.43', '2022-04-08 23:01:14', '2022-04-08 23:01:14'),
(2988, '2022-04-08 00:00:00', '172.70.135.189', '2022-04-08 23:09:43', '2022-04-08 23:09:43'),
(2989, '2022-04-08 00:00:00', '172.70.135.85', '2022-04-08 23:36:57', '2022-04-08 23:36:57'),
(2990, '2022-04-09 00:00:00', '172.70.174.116', '2022-04-09 00:12:40', '2022-04-09 00:12:40'),
(2991, '2022-04-09 00:00:00', '172.70.34.254', '2022-04-09 00:48:23', '2022-04-09 00:48:23'),
(2992, '2022-04-09 00:00:00', '172.70.174.196', '2022-04-09 01:48:23', '2022-04-09 01:48:23'),
(2993, '2022-04-09 00:00:00', '172.70.142.218', '2022-04-09 01:53:05', '2022-04-09 01:53:05'),
(2994, '2022-04-09 00:00:00', '172.70.175.171', '2022-04-09 02:09:31', '2022-04-09 02:09:31'),
(2995, '2022-04-09 00:00:00', '172.70.38.184', '2022-04-09 02:09:33', '2022-04-09 02:09:33'),
(2996, '2022-04-09 00:00:00', '172.70.135.63', '2022-04-09 02:48:23', '2022-04-09 02:48:23'),
(2997, '2022-04-09 00:00:00', '172.68.50.104', '2022-04-09 03:09:56', '2022-04-09 03:09:56'),
(2998, '2022-04-09 00:00:00', '172.70.175.65', '2022-04-09 05:19:18', '2022-04-09 05:19:18'),
(2999, '2022-04-09 00:00:00', '172.70.175.17', '2022-04-09 06:19:18', '2022-04-09 06:19:18'),
(3000, '2022-04-09 00:00:00', '172.70.134.220', '2022-04-09 06:49:18', '2022-04-09 06:49:18'),
(3001, '2022-04-09 00:00:00', '172.70.34.228', '2022-04-09 08:15:47', '2022-04-09 08:15:47'),
(3002, '2022-04-09 00:00:00', '172.70.134.54', '2022-04-09 08:48:23', '2022-04-09 08:48:23'),
(3003, '2022-04-09 00:00:00', '162.158.78.90', '2022-04-09 09:22:27', '2022-04-09 09:22:27'),
(3004, '2022-04-09 00:00:00', '162.158.78.70', '2022-04-09 09:48:23', '2022-04-09 09:48:23'),
(3005, '2022-04-09 00:00:00', '172.70.206.94', '2022-04-09 10:16:50', '2022-04-09 10:16:50'),
(3006, '2022-04-09 00:00:00', '172.70.142.218', '2022-04-09 10:28:43', '2022-04-09 10:28:43'),
(3007, '2022-04-09 00:00:00', '172.70.131.111', '2022-04-09 11:24:06', '2022-04-09 11:24:06'),
(3008, '2022-04-09 00:00:00', '162.158.163.9', '2022-04-09 11:38:07', '2022-04-09 11:38:07'),
(3009, '2022-04-09 00:00:00', '172.70.178.204', '2022-04-09 12:41:35', '2022-04-09 12:41:35'),
(3010, '2022-04-09 00:00:00', '172.70.130.220', '2022-04-09 12:41:36', '2022-04-09 12:41:36'),
(3011, '2022-04-09 00:00:00', '172.70.179.27', '2022-04-09 12:41:37', '2022-04-09 12:41:37'),
(3012, '2022-04-09 00:00:00', '172.70.131.19', '2022-04-09 12:41:37', '2022-04-09 12:41:37'),
(3013, '2022-04-09 00:00:00', '172.70.142.224', '2022-04-09 12:44:25', '2022-04-09 12:44:25'),
(3014, '2022-04-09 00:00:00', '172.70.92.156', '2022-04-09 12:50:11', '2022-04-09 12:50:11'),
(3015, '2022-04-09 00:00:00', '172.70.189.41', '2022-04-09 13:01:06', '2022-04-09 13:01:06'),
(3016, '2022-04-09 00:00:00', '172.70.126.68', '2022-04-09 13:11:14', '2022-04-09 13:11:14'),
(3017, '2022-04-09 00:00:00', '172.70.131.223', '2022-04-09 14:22:40', '2022-04-09 14:22:40'),
(3018, '2022-04-09 00:00:00', '172.70.147.34', '2022-04-09 14:57:21', '2022-04-09 14:57:21'),
(3019, '2022-04-09 00:00:00', '162.158.126.118', '2022-04-09 15:19:37', '2022-04-09 15:19:37'),
(3020, '2022-04-09 00:00:00', '108.162.216.42', '2022-04-09 15:34:06', '2022-04-09 15:34:06'),
(3021, '2022-04-09 00:00:00', '141.101.77.151', '2022-04-09 16:12:18', '2022-04-09 16:12:18'),
(3022, '2022-04-09 00:00:00', '141.101.104.97', '2022-04-09 16:12:18', '2022-04-09 16:12:18'),
(3023, '2022-04-09 00:00:00', '172.70.126.196', '2022-04-09 16:45:32', '2022-04-09 16:45:32'),
(3024, '2022-04-09 00:00:00', '172.70.34.46', '2022-04-09 17:20:42', '2022-04-09 17:20:42'),
(3025, '2022-04-09 00:00:00', '172.70.130.90', '2022-04-09 17:21:14', '2022-04-09 17:21:14'),
(3026, '2022-04-09 00:00:00', '172.70.127.39', '2022-04-09 17:56:57', '2022-04-09 17:56:57'),
(3027, '2022-04-09 00:00:00', '172.70.188.206', '2022-04-09 18:37:59', '2022-04-09 18:37:59'),
(3028, '2022-04-09 00:00:00', '172.70.130.122', '2022-04-09 19:08:23', '2022-04-09 19:08:23'),
(3029, '2022-04-09 00:00:00', '172.69.33.126', '2022-04-09 19:11:43', '2022-04-09 19:11:43'),
(3030, '2022-04-09 00:00:00', '172.70.211.121', '2022-04-09 19:34:16', '2022-04-09 19:34:16'),
(3031, '2022-04-09 00:00:00', '172.69.33.120', '2022-04-09 19:35:51', '2022-04-09 19:35:51'),
(3032, '2022-04-09 00:00:00', '172.70.92.150', '2022-04-09 19:42:20', '2022-04-09 19:42:20'),
(3033, '2022-04-09 00:00:00', '172.70.147.34', '2022-04-09 20:56:02', '2022-04-09 20:56:02'),
(3034, '2022-04-09 00:00:00', '172.70.178.242', '2022-04-09 21:31:14', '2022-04-09 21:31:14'),
(3035, '2022-04-09 00:00:00', '162.158.2.216', '2022-04-09 21:37:46', '2022-04-09 21:37:46'),
(3036, '2022-04-09 00:00:00', '172.70.210.180', '2022-04-09 21:42:22', '2022-04-09 21:42:22'),
(3037, '2022-04-09 00:00:00', '172.70.131.63', '2022-04-09 22:42:40', '2022-04-09 22:42:40'),
(3038, '2022-04-09 00:00:00', '172.70.188.206', '2022-04-09 23:12:59', '2022-04-09 23:12:59'),
(3039, '2022-04-09 00:00:00', '172.70.130.190', '2022-04-09 23:18:23', '2022-04-09 23:18:23'),
(3040, '2022-04-10 00:00:00', '172.70.126.92', '2022-04-10 00:29:49', '2022-04-10 00:29:49'),
(3041, '2022-04-10 00:00:00', '172.70.130.86', '2022-04-10 01:05:31', '2022-04-10 01:05:31'),
(3042, '2022-04-10 00:00:00', '172.70.178.150', '2022-04-10 01:41:14', '2022-04-10 01:41:14'),
(3043, '2022-04-10 00:00:00', '172.70.142.42', '2022-04-10 02:44:51', '2022-04-10 02:44:51'),
(3044, '2022-04-10 00:00:00', '172.70.130.198', '2022-04-10 03:27:52', '2022-04-10 03:27:52'),
(3045, '2022-04-10 00:00:00', '172.70.130.180', '2022-04-10 03:27:53', '2022-04-10 03:27:53'),
(3046, '2022-04-10 00:00:00', '108.162.216.38', '2022-04-10 03:39:04', '2022-04-10 03:39:04'),
(3047, '2022-04-10 00:00:00', '172.70.126.254', '2022-04-10 03:59:50', '2022-04-10 03:59:50'),
(3048, '2022-04-10 00:00:00', '172.70.130.234', '2022-04-10 04:44:36', '2022-04-10 04:44:36'),
(3049, '2022-04-10 00:00:00', '172.70.178.116', '2022-04-10 05:59:24', '2022-04-10 05:59:24'),
(3050, '2022-04-10 00:00:00', '172.70.178.118', '2022-04-10 06:29:13', '2022-04-10 06:29:13'),
(3051, '2022-04-10 00:00:00', '172.70.142.224', '2022-04-10 09:35:18', '2022-04-10 09:35:18'),
(3052, '2022-04-10 00:00:00', '162.158.162.66', '2022-04-10 10:17:33', '2022-04-10 10:17:33'),
(3053, '2022-04-10 00:00:00', '108.162.246.45', '2022-04-10 11:11:45', '2022-04-10 11:11:45'),
(3054, '2022-04-10 00:00:00', '172.70.126.22', '2022-04-10 12:40:40', '2022-04-10 12:40:40'),
(3055, '2022-04-10 00:00:00', '162.158.106.146', '2022-04-10 14:08:29', '2022-04-10 14:08:29'),
(3056, '2022-04-10 00:00:00', '172.69.69.65', '2022-04-10 14:54:55', '2022-04-10 14:54:55'),
(3057, '2022-04-10 00:00:00', '172.70.131.19', '2022-04-10 16:27:23', '2022-04-10 16:27:23'),
(3058, '2022-04-10 00:00:00', '172.70.135.15', '2022-04-10 17:22:19', '2022-04-10 17:22:19'),
(3059, '2022-04-10 00:00:00', '172.70.131.137', '2022-04-10 19:15:16', '2022-04-10 19:15:16'),
(3060, '2022-04-10 00:00:00', '172.70.126.190', '2022-04-10 19:42:41', '2022-04-10 19:42:41'),
(3061, '2022-04-10 00:00:00', '172.70.130.98', '2022-04-10 19:52:07', '2022-04-10 19:52:07'),
(3062, '2022-04-10 00:00:00', '172.70.126.254', '2022-04-10 20:59:17', '2022-04-10 20:59:17'),
(3063, '2022-04-10 00:00:00', '172.70.174.140', '2022-04-10 21:13:23', '2022-04-10 21:13:23'),
(3064, '2022-04-10 00:00:00', '172.70.178.14', '2022-04-10 21:44:03', '2022-04-10 21:44:03'),
(3065, '2022-04-10 00:00:00', '172.70.34.202', '2022-04-10 21:49:58', '2022-04-10 21:49:58'),
(3066, '2022-04-10 00:00:00', '162.158.79.45', '2022-04-10 21:50:00', '2022-04-10 21:50:00'),
(3067, '2022-04-10 00:00:00', '172.70.38.72', '2022-04-10 21:50:01', '2022-04-10 21:50:01'),
(3068, '2022-04-10 00:00:00', '172.70.135.81', '2022-04-10 21:50:03', '2022-04-10 21:50:03'),
(3069, '2022-04-10 00:00:00', '172.70.135.15', '2022-04-10 21:50:17', '2022-04-10 21:50:17'),
(3070, '2022-04-10 00:00:00', '172.70.134.154', '2022-04-10 21:50:21', '2022-04-10 21:50:21'),
(3071, '2022-04-10 00:00:00', '172.70.38.82', '2022-04-10 21:50:23', '2022-04-10 21:50:23'),
(3072, '2022-04-10 00:00:00', '172.70.38.26', '2022-04-10 21:50:25', '2022-04-10 21:50:25'),
(3073, '2022-04-10 00:00:00', '172.70.35.53', '2022-04-10 21:50:26', '2022-04-10 21:50:26'),
(3074, '2022-04-10 00:00:00', '172.70.175.213', '2022-04-10 21:50:28', '2022-04-10 21:50:28'),
(3075, '2022-04-10 00:00:00', '172.70.174.140', '2022-04-10 21:50:30', '2022-04-10 21:50:30'),
(3076, '2022-04-10 00:00:00', '172.70.34.2', '2022-04-10 21:50:31', '2022-04-10 21:50:31'),
(3077, '2022-04-10 00:00:00', '172.70.126.158', '2022-04-10 22:51:13', '2022-04-10 22:51:13'),
(3078, '2022-04-11 00:00:00', '172.70.130.118', '2022-04-11 00:47:26', '2022-04-11 00:47:26'),
(3079, '2022-04-11 00:00:00', '172.70.178.172', '2022-04-11 01:09:43', '2022-04-11 01:09:43'),
(3080, '2022-04-11 00:00:00', '172.70.242.100', '2022-04-11 01:42:10', '2022-04-11 01:42:10'),
(3081, '2022-04-11 00:00:00', '162.158.162.252', '2022-04-11 01:44:23', '2022-04-11 01:44:23'),
(3082, '2022-04-11 00:00:00', '172.70.246.74', '2022-04-11 02:12:42', '2022-04-11 02:12:42'),
(3083, '2022-04-11 00:00:00', '172.70.189.117', '2022-04-11 02:13:51', '2022-04-11 02:13:51'),
(3084, '2022-04-11 00:00:00', '172.70.189.59', '2022-04-11 02:22:52', '2022-04-11 02:22:52'),
(3085, '2022-04-11 00:00:00', '172.70.189.59', '2022-04-11 02:56:15', '2022-04-11 02:56:15'),
(3086, '2022-04-11 00:00:00', '172.70.92.150', '2022-04-11 02:57:14', '2022-04-11 02:57:14'),
(3087, '2022-04-11 00:00:00', '172.70.130.140', '2022-04-11 04:09:31', '2022-04-11 04:09:31'),
(3088, '2022-04-11 00:00:00', '172.70.178.94', '2022-04-11 04:09:32', '2022-04-11 04:09:32'),
(3089, '2022-04-11 00:00:00', '172.70.130.176', '2022-04-11 04:57:26', '2022-04-11 04:57:26'),
(3090, '2022-04-11 00:00:00', '141.101.104.177', '2022-04-11 07:46:35', '2022-04-11 07:46:35'),
(3091, '2022-04-11 00:00:00', '162.158.159.126', '2022-04-11 09:22:23', '2022-04-11 09:22:23'),
(3092, '2022-04-11 00:00:00', '172.70.142.34', '2022-04-11 09:52:45', '2022-04-11 09:52:45'),
(3093, '2022-04-11 00:00:00', '108.162.245.92', '2022-04-11 10:04:34', '2022-04-11 10:04:34'),
(3094, '2022-04-11 00:00:00', '162.158.107.151', '2022-04-11 10:04:35', '2022-04-11 10:04:35'),
(3095, '2022-04-11 00:00:00', '108.162.245.126', '2022-04-11 10:04:35', '2022-04-11 10:04:35'),
(3096, '2022-04-11 00:00:00', '162.158.166.100', '2022-04-11 10:04:36', '2022-04-11 10:04:36'),
(3097, '2022-04-11 00:00:00', '108.162.246.49', '2022-04-11 10:04:36', '2022-04-11 10:04:36'),
(3098, '2022-04-11 00:00:00', '108.162.246.7', '2022-04-11 10:04:37', '2022-04-11 10:04:37'),
(3099, '2022-04-11 00:00:00', '162.158.107.163', '2022-04-11 10:04:37', '2022-04-11 10:04:37'),
(3100, '2022-04-11 00:00:00', '172.69.134.94', '2022-04-11 10:04:38', '2022-04-11 10:04:38'),
(3101, '2022-04-11 00:00:00', '108.162.246.105', '2022-04-11 10:04:40', '2022-04-11 10:04:40'),
(3102, '2022-04-11 00:00:00', '172.69.134.70', '2022-04-11 10:04:41', '2022-04-11 10:04:41'),
(3103, '2022-04-11 00:00:00', '162.158.166.118', '2022-04-11 10:04:41', '2022-04-11 10:04:41'),
(3104, '2022-04-11 00:00:00', '162.158.106.124', '2022-04-11 10:04:42', '2022-04-11 10:04:42'),
(3105, '2022-04-11 00:00:00', '108.162.245.204', '2022-04-11 10:04:42', '2022-04-11 10:04:42'),
(3106, '2022-04-11 00:00:00', '162.158.107.167', '2022-04-11 10:04:42', '2022-04-11 10:04:42'),
(3107, '2022-04-11 00:00:00', '162.158.107.105', '2022-04-11 10:04:43', '2022-04-11 10:04:43'),
(3108, '2022-04-11 00:00:00', '172.69.134.52', '2022-04-11 10:04:43', '2022-04-11 10:04:43'),
(3109, '2022-04-11 00:00:00', '172.69.134.36', '2022-04-11 10:04:43', '2022-04-11 10:04:43'),
(3110, '2022-04-11 00:00:00', '162.158.106.212', '2022-04-11 10:04:44', '2022-04-11 10:04:44'),
(3111, '2022-04-11 00:00:00', '172.70.211.121', '2022-04-11 10:04:44', '2022-04-11 10:04:44'),
(3112, '2022-04-11 00:00:00', '172.70.206.170', '2022-04-11 10:04:47', '2022-04-11 10:04:47'),
(3113, '2022-04-11 00:00:00', '172.68.133.84', '2022-04-11 10:04:47', '2022-04-11 10:04:47'),
(3114, '2022-04-11 00:00:00', '162.158.107.133', '2022-04-11 10:04:48', '2022-04-11 10:04:48'),
(3115, '2022-04-11 00:00:00', '172.68.143.228', '2022-04-11 10:04:48', '2022-04-11 10:04:48'),
(3116, '2022-04-11 00:00:00', '162.158.107.127', '2022-04-11 10:04:49', '2022-04-11 10:04:49'),
(3117, '2022-04-11 00:00:00', '172.69.33.196', '2022-04-11 10:04:50', '2022-04-11 10:04:50'),
(3118, '2022-04-11 00:00:00', '162.158.166.214', '2022-04-11 10:04:50', '2022-04-11 10:04:50'),
(3119, '2022-04-11 00:00:00', '172.69.134.28', '2022-04-11 10:04:51', '2022-04-11 10:04:51'),
(3120, '2022-04-11 00:00:00', '172.70.210.54', '2022-04-11 10:04:52', '2022-04-11 10:04:52'),
(3121, '2022-04-11 00:00:00', '172.70.214.32', '2022-04-11 10:04:53', '2022-04-11 10:04:53'),
(3122, '2022-04-11 00:00:00', '172.70.142.218', '2022-04-11 10:23:57', '2022-04-11 10:23:57'),
(3123, '2022-04-11 00:00:00', '172.70.126.202', '2022-04-11 10:42:53', '2022-04-11 10:42:53'),
(3124, '2022-04-11 00:00:00', '172.70.92.156', '2022-04-11 11:31:17', '2022-04-11 11:31:17'),
(3125, '2022-04-11 00:00:00', '172.70.147.34', '2022-04-11 11:36:40', '2022-04-11 11:36:40'),
(3126, '2022-04-11 00:00:00', '162.158.222.160', '2022-04-11 11:47:54', '2022-04-11 11:47:54'),
(3127, '2022-04-11 00:00:00', '172.70.131.127', '2022-04-11 11:54:18', '2022-04-11 11:54:18'),
(3128, '2022-04-11 00:00:00', '172.70.85.154', '2022-04-11 12:03:10', '2022-04-11 12:03:10'),
(3129, '2022-04-11 00:00:00', '172.70.178.120', '2022-04-11 12:30:01', '2022-04-11 12:30:01'),
(3130, '2022-04-11 00:00:00', '172.70.178.88', '2022-04-11 13:05:44', '2022-04-11 13:05:44'),
(3131, '2022-04-11 00:00:00', '172.69.170.148', '2022-04-11 15:35:30', '2022-04-11 15:35:30'),
(3132, '2022-04-11 00:00:00', '108.162.216.236', '2022-04-11 17:54:06', '2022-04-11 17:54:06'),
(3133, '2022-04-11 00:00:00', '162.158.203.15', '2022-04-11 18:07:42', '2022-04-11 18:07:42'),
(3134, '2022-04-11 00:00:00', '141.101.76.14', '2022-04-11 18:58:35', '2022-04-11 18:58:35'),
(3135, '2022-04-11 00:00:00', '172.70.188.36', '2022-04-11 19:16:28', '2022-04-11 19:16:28'),
(3136, '2022-04-11 00:00:00', '172.70.126.38', '2022-04-11 19:50:37', '2022-04-11 19:50:37'),
(3137, '2022-04-11 00:00:00', '172.70.126.46', '2022-04-11 21:00:21', '2022-04-11 21:00:21'),
(3138, '2022-04-11 00:00:00', '172.70.131.167', '2022-04-11 21:27:31', '2022-04-11 21:27:31'),
(3139, '2022-04-11 00:00:00', '172.70.130.224', '2022-04-11 21:28:54', '2022-04-11 21:28:54'),
(3140, '2022-04-11 00:00:00', '172.70.82.118', '2022-04-11 21:48:57', '2022-04-11 21:48:57'),
(3141, '2022-04-11 00:00:00', '172.70.254.36', '2022-04-11 21:48:58', '2022-04-11 21:48:58'),
(3142, '2022-04-11 00:00:00', '172.70.189.137', '2022-04-11 22:45:47', '2022-04-11 22:45:47'),
(3143, '2022-04-11 00:00:00', '172.70.178.168', '2022-04-11 23:00:22', '2022-04-11 23:00:22'),
(3144, '2022-04-12 00:00:00', '172.70.178.106', '2022-04-12 00:00:21', '2022-04-12 00:00:21'),
(3145, '2022-04-12 00:00:00', '172.70.147.42', '2022-04-12 00:24:06', '2022-04-12 00:24:06'),
(3146, '2022-04-12 00:00:00', '172.70.127.35', '2022-04-12 00:36:48', '2022-04-12 00:36:48'),
(3147, '2022-04-12 00:00:00', '172.70.126.14', '2022-04-12 03:18:05', '2022-04-12 03:18:05'),
(3148, '2022-04-12 00:00:00', '172.70.131.39', '2022-04-12 03:18:05', '2022-04-12 03:18:05'),
(3149, '2022-04-12 00:00:00', '172.70.126.236', '2022-04-12 04:18:05', '2022-04-12 04:18:05'),
(3150, '2022-04-12 00:00:00', '172.70.178.32', '2022-04-12 05:18:05', '2022-04-12 05:18:05'),
(3151, '2022-04-12 00:00:00', '172.69.33.68', '2022-04-12 05:20:26', '2022-04-12 05:20:26'),
(3152, '2022-04-12 00:00:00', '172.70.189.59', '2022-04-12 05:47:18', '2022-04-12 05:47:18'),
(3153, '2022-04-12 00:00:00', '172.70.147.34', '2022-04-12 06:50:54', '2022-04-12 06:50:54'),
(3154, '2022-04-12 00:00:00', '141.101.69.249', '2022-04-12 07:49:45', '2022-04-12 07:49:45'),
(3155, '2022-04-12 00:00:00', '172.70.92.156', '2022-04-12 08:42:17', '2022-04-12 08:42:17'),
(3156, '2022-04-12 00:00:00', '172.70.178.106', '2022-04-12 10:18:05', '2022-04-12 10:18:05'),
(3157, '2022-04-12 00:00:00', '172.70.188.36', '2022-04-12 10:48:04', '2022-04-12 10:48:04'),
(3158, '2022-04-12 00:00:00', '172.70.131.119', '2022-04-12 11:18:05', '2022-04-12 11:18:05'),
(3159, '2022-04-12 00:00:00', '172.70.214.188', '2022-04-12 11:19:36', '2022-04-12 11:19:36'),
(3160, '2022-04-12 00:00:00', '172.69.68.7', '2022-04-12 11:20:47', '2022-04-12 11:20:47'),
(3161, '2022-04-12 00:00:00', '172.69.70.116', '2022-04-12 11:21:01', '2022-04-12 11:21:01'),
(3162, '2022-04-12 00:00:00', '172.70.142.224', '2022-04-12 13:58:33', '2022-04-12 13:58:33'),
(3163, '2022-04-12 00:00:00', '172.70.174.190', '2022-04-12 14:28:10', '2022-04-12 14:28:10'),
(3164, '2022-04-12 00:00:00', '162.158.222.148', '2022-04-12 15:26:18', '2022-04-12 15:26:18'),
(3165, '2022-04-12 00:00:00', '172.70.134.196', '2022-04-12 16:29:21', '2022-04-12 16:29:21'),
(3166, '2022-04-12 00:00:00', '172.70.92.156', '2022-04-12 18:03:42', '2022-04-12 18:03:42'),
(3167, '2022-04-12 00:00:00', '172.70.175.67', '2022-04-12 18:27:23', '2022-04-12 18:27:23'),
(3168, '2022-04-12 00:00:00', '172.70.135.127', '2022-04-12 18:27:23', '2022-04-12 18:27:23'),
(3169, '2022-04-12 00:00:00', '141.101.77.44', '2022-04-12 19:38:03', '2022-04-12 19:38:03'),
(3170, '2022-04-12 00:00:00', '141.101.77.44', '2022-04-12 23:32:45', '2022-04-12 23:32:45'),
(3171, '2022-04-13 00:00:00', '172.70.142.224', '2022-04-13 01:51:58', '2022-04-13 01:51:58'),
(3172, '2022-04-13 00:00:00', '172.70.162.14', '2022-04-13 02:18:49', '2022-04-13 02:18:49'),
(3173, '2022-04-13 00:00:00', '162.158.162.8', '2022-04-13 02:46:55', '2022-04-13 02:46:55'),
(3174, '2022-04-13 00:00:00', '172.70.135.111', '2022-04-13 03:09:44', '2022-04-13 03:09:44'),
(3175, '2022-04-13 00:00:00', '162.158.79.35', '2022-04-13 03:13:45', '2022-04-13 03:13:45'),
(3176, '2022-04-13 00:00:00', '172.70.135.99', '2022-04-13 05:13:46', '2022-04-13 05:13:46'),
(3177, '2022-04-13 00:00:00', '172.69.170.92', '2022-04-13 06:01:52', '2022-04-13 06:01:52'),
(3178, '2022-04-13 00:00:00', '172.70.110.62', '2022-04-13 06:09:25', '2022-04-13 06:09:25'),
(3179, '2022-04-13 00:00:00', '172.70.230.172', '2022-04-13 06:09:26', '2022-04-13 06:09:26'),
(3180, '2022-04-13 00:00:00', '162.158.79.43', '2022-04-13 06:09:31', '2022-04-13 06:09:31'),
(3181, '2022-04-13 00:00:00', '172.70.251.153', '2022-04-13 08:27:44', '2022-04-13 08:27:44'),
(3182, '2022-04-13 00:00:00', '162.158.89.169', '2022-04-13 08:27:44', '2022-04-13 08:27:44'),
(3183, '2022-04-13 00:00:00', '172.70.188.36', '2022-04-13 09:22:59', '2022-04-13 09:22:59'),
(3184, '2022-04-13 00:00:00', '172.70.92.150', '2022-04-13 10:31:13', '2022-04-13 10:31:13'),
(3185, '2022-04-13 00:00:00', '172.70.142.224', '2022-04-13 11:51:04', '2022-04-13 11:51:04'),
(3186, '2022-04-13 00:00:00', '172.70.131.185', '2022-04-13 12:24:02', '2022-04-13 12:24:02'),
(3187, '2022-04-13 00:00:00', '172.69.69.87', '2022-04-13 13:24:01', '2022-04-13 13:24:01'),
(3188, '2022-04-13 00:00:00', '162.158.107.163', '2022-04-13 14:18:25', '2022-04-13 14:18:25'),
(3189, '2022-04-13 00:00:00', '172.69.70.58', '2022-04-13 14:24:01', '2022-04-13 14:24:01'),
(3190, '2022-04-13 00:00:00', '172.69.69.189', '2022-04-13 14:54:01', '2022-04-13 14:54:01'),
(3191, '2022-04-13 00:00:00', '172.69.71.174', '2022-04-13 15:24:03', '2022-04-13 15:24:03'),
(3192, '2022-04-13 00:00:00', '172.70.92.150', '2022-04-13 18:51:21', '2022-04-13 18:51:21'),
(3193, '2022-04-13 00:00:00', '172.70.142.218', '2022-04-13 18:51:22', '2022-04-13 18:51:22'),
(3194, '2022-04-13 00:00:00', '172.70.142.34', '2022-04-13 18:51:27', '2022-04-13 18:51:27'),
(3195, '2022-04-13 00:00:00', '172.70.142.34', '2022-04-13 19:16:25', '2022-04-13 19:16:25'),
(3196, '2022-04-13 00:00:00', '141.101.98.222', '2022-04-13 21:05:00', '2022-04-13 21:05:00'),
(3197, '2022-04-13 00:00:00', '172.70.162.240', '2022-04-13 21:05:00', '2022-04-13 21:05:00'),
(3198, '2022-04-13 00:00:00', '172.70.90.6', '2022-04-13 21:05:01', '2022-04-13 21:05:01'),
(3199, '2022-04-13 00:00:00', '172.70.189.137', '2022-04-13 21:30:31', '2022-04-13 21:30:31'),
(3200, '2022-04-13 00:00:00', '172.70.110.184', '2022-04-13 23:37:46', '2022-04-13 23:37:46'),
(3201, '2022-04-13 00:00:00', '172.70.189.59', '2022-04-13 23:45:40', '2022-04-13 23:45:40'),
(3202, '2022-04-13 00:00:00', '172.70.189.117', '2022-04-13 23:54:53', '2022-04-13 23:54:53'),
(3203, '2022-04-14 00:00:00', '172.70.142.224', '2022-04-14 01:14:19', '2022-04-14 01:14:19'),
(3204, '2022-04-14 00:00:00', '172.70.189.59', '2022-04-14 01:35:33', '2022-04-14 01:35:33'),
(3205, '2022-04-14 00:00:00', '172.69.71.174', '2022-04-14 01:59:06', '2022-04-14 01:59:06'),
(3206, '2022-04-14 00:00:00', '172.70.189.117', '2022-04-14 03:15:50', '2022-04-14 03:15:50'),
(3207, '2022-04-14 00:00:00', '141.101.76.130', '2022-04-14 03:55:21', '2022-04-14 03:55:21'),
(3208, '2022-04-14 00:00:00', '172.70.130.244', '2022-04-14 04:15:57', '2022-04-14 04:15:57'),
(3209, '2022-04-14 00:00:00', '172.70.142.42', '2022-04-14 05:30:12', '2022-04-14 05:30:12'),
(3210, '2022-04-14 00:00:00', '172.70.131.53', '2022-04-14 05:44:11', '2022-04-14 05:44:11'),
(3211, '2022-04-14 00:00:00', '172.70.142.218', '2022-04-14 09:30:13', '2022-04-14 09:30:13'),
(3212, '2022-04-14 00:00:00', '172.70.85.154', '2022-04-14 09:47:31', '2022-04-14 09:47:31'),
(3213, '2022-04-14 00:00:00', '172.70.90.172', '2022-04-14 09:47:31', '2022-04-14 09:47:31'),
(3214, '2022-04-14 00:00:00', '172.70.162.240', '2022-04-14 09:47:31', '2022-04-14 09:47:31'),
(3215, '2022-04-14 00:00:00', '141.101.99.253', '2022-04-14 09:47:32', '2022-04-14 09:47:32'),
(3216, '2022-04-14 00:00:00', '172.70.189.117', '2022-04-14 12:31:12', '2022-04-14 12:31:12'),
(3217, '2022-04-14 00:00:00', '172.70.206.34', '2022-04-14 13:25:15', '2022-04-14 13:25:15'),
(3218, '2022-04-14 00:00:00', '172.70.38.118', '2022-04-14 14:34:51', '2022-04-14 14:34:51'),
(3219, '2022-04-14 00:00:00', '172.70.134.44', '2022-04-14 15:05:04', '2022-04-14 15:05:04'),
(3220, '2022-04-14 00:00:00', '162.158.78.114', '2022-04-14 15:49:11', '2022-04-14 15:49:11'),
(3221, '2022-04-14 00:00:00', '172.70.174.168', '2022-04-14 16:35:04', '2022-04-14 16:35:04'),
(3222, '2022-04-14 00:00:00', '172.70.38.172', '2022-04-14 17:17:25', '2022-04-14 17:17:25'),
(3223, '2022-04-14 00:00:00', '172.70.92.150', '2022-04-14 17:40:13', '2022-04-14 17:40:13'),
(3224, '2022-04-14 00:00:00', '172.70.189.117', '2022-04-14 17:45:26', '2022-04-14 17:45:26'),
(3225, '2022-04-14 00:00:00', '172.70.211.157', '2022-04-14 21:16:40', '2022-04-14 21:16:40'),
(3226, '2022-04-14 00:00:00', '172.70.34.104', '2022-04-14 23:08:24', '2022-04-14 23:08:24'),
(3227, '2022-04-15 00:00:00', '172.69.33.102', '2022-04-15 02:55:51', '2022-04-15 02:55:51'),
(3228, '2022-04-15 00:00:00', '172.70.142.218', '2022-04-15 03:11:25', '2022-04-15 03:11:25'),
(3229, '2022-04-15 00:00:00', '172.70.189.59', '2022-04-15 03:35:03', '2022-04-15 03:35:03'),
(3230, '2022-04-15 00:00:00', '172.70.114.246', '2022-04-15 04:05:15', '2022-04-15 04:05:15'),
(3231, '2022-04-15 00:00:00', '172.68.94.254', '2022-04-15 04:05:17', '2022-04-15 04:05:17'),
(3232, '2022-04-15 00:00:00', '172.70.142.218', '2022-04-15 04:07:25', '2022-04-15 04:07:25'),
(3233, '2022-04-15 00:00:00', '172.70.218.198', '2022-04-15 04:42:07', '2022-04-15 04:42:07'),
(3234, '2022-04-15 00:00:00', '172.70.175.147', '2022-04-15 05:28:07', '2022-04-15 05:28:07'),
(3235, '2022-04-15 00:00:00', '172.70.142.34', '2022-04-15 05:29:24', '2022-04-15 05:29:24'),
(3236, '2022-04-15 00:00:00', '172.70.175.69', '2022-04-15 05:58:06', '2022-04-15 05:58:06'),
(3237, '2022-04-15 00:00:00', '172.70.135.155', '2022-04-15 06:58:06', '2022-04-15 06:58:06'),
(3238, '2022-04-15 00:00:00', '172.70.242.210', '2022-04-15 07:27:10', '2022-04-15 07:27:10'),
(3239, '2022-04-15 00:00:00', '172.70.135.17', '2022-04-15 07:28:06', '2022-04-15 07:28:06'),
(3240, '2022-04-15 00:00:00', '172.70.134.206', '2022-04-15 07:58:06', '2022-04-15 07:58:06'),
(3241, '2022-04-15 00:00:00', '172.70.174.66', '2022-04-15 08:06:54', '2022-04-15 08:06:54'),
(3242, '2022-04-15 00:00:00', '162.158.78.254', '2022-04-15 08:28:06', '2022-04-15 08:28:06'),
(3243, '2022-04-15 00:00:00', '172.70.134.114', '2022-04-15 08:51:01', '2022-04-15 08:51:01'),
(3244, '2022-04-15 00:00:00', '172.70.175.199', '2022-04-15 08:58:06', '2022-04-15 08:58:06'),
(3245, '2022-04-15 00:00:00', '172.70.175.195', '2022-04-15 09:35:08', '2022-04-15 09:35:08'),
(3246, '2022-04-15 00:00:00', '172.70.35.37', '2022-04-15 09:36:53', '2022-04-15 09:36:53'),
(3247, '2022-04-15 00:00:00', '172.68.10.226', '2022-04-15 09:59:35', '2022-04-15 09:59:35'),
(3248, '2022-04-15 00:00:00', '172.68.244.66', '2022-04-15 09:59:42', '2022-04-15 09:59:42'),
(3249, '2022-04-15 00:00:00', '172.68.11.155', '2022-04-15 09:59:43', '2022-04-15 09:59:43'),
(3250, '2022-04-15 00:00:00', '172.70.174.214', '2022-04-15 10:19:15', '2022-04-15 10:19:15'),
(3251, '2022-04-15 00:00:00', '172.70.174.78', '2022-04-15 10:28:06', '2022-04-15 10:28:06'),
(3252, '2022-04-15 00:00:00', '172.70.134.94', '2022-04-15 11:28:06', '2022-04-15 11:28:06'),
(3253, '2022-04-15 00:00:00', '172.70.134.146', '2022-04-15 11:47:29', '2022-04-15 11:47:29'),
(3254, '2022-04-15 00:00:00', '172.70.122.200', '2022-04-15 11:47:50', '2022-04-15 11:47:50'),
(3255, '2022-04-15 00:00:00', '108.162.246.207', '2022-04-15 11:58:07', '2022-04-15 11:58:07'),
(3256, '2022-04-15 00:00:00', '108.162.245.88', '2022-04-15 12:28:06', '2022-04-15 12:28:06'),
(3257, '2022-04-15 00:00:00', '108.162.246.25', '2022-04-15 12:31:37', '2022-04-15 12:31:37'),
(3258, '2022-04-15 00:00:00', '162.158.106.212', '2022-04-15 12:58:06', '2022-04-15 12:58:06'),
(3259, '2022-04-15 00:00:00', '162.158.106.164', '2022-04-15 13:28:06', '2022-04-15 13:28:06'),
(3260, '2022-04-15 00:00:00', '108.162.246.25', '2022-04-15 13:59:50', '2022-04-15 13:59:50');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(3261, '2022-04-15 00:00:00', '172.70.92.156', '2022-04-15 14:01:48', '2022-04-15 14:01:48'),
(3262, '2022-04-15 00:00:00', '162.158.107.21', '2022-04-15 14:15:07', '2022-04-15 14:15:07'),
(3263, '2022-04-15 00:00:00', '162.158.106.146', '2022-04-15 14:25:44', '2022-04-15 14:25:44'),
(3264, '2022-04-15 00:00:00', '172.70.251.51', '2022-04-15 14:49:07', '2022-04-15 14:49:07'),
(3265, '2022-04-15 00:00:00', '162.158.91.187', '2022-04-15 14:49:07', '2022-04-15 14:49:07'),
(3266, '2022-04-15 00:00:00', '172.70.82.182', '2022-04-15 16:45:07', '2022-04-15 16:45:07'),
(3267, '2022-04-15 00:00:00', '172.70.82.10', '2022-04-15 16:45:09', '2022-04-15 16:45:09'),
(3268, '2022-04-15 00:00:00', '162.158.107.189', '2022-04-15 19:29:22', '2022-04-15 19:29:22'),
(3269, '2022-04-15 00:00:00', '162.158.106.178', '2022-04-15 21:27:22', '2022-04-15 21:27:22'),
(3270, '2022-04-15 00:00:00', '108.162.246.49', '2022-04-15 21:27:23', '2022-04-15 21:27:23'),
(3271, '2022-04-15 00:00:00', '108.162.246.151', '2022-04-15 21:27:23', '2022-04-15 21:27:23'),
(3272, '2022-04-15 00:00:00', '172.70.92.156', '2022-04-15 22:30:19', '2022-04-15 22:30:19'),
(3273, '2022-04-15 00:00:00', '172.70.162.188', '2022-04-15 22:51:39', '2022-04-15 22:51:39'),
(3274, '2022-04-15 00:00:00', '172.70.251.125', '2022-04-15 23:09:55', '2022-04-15 23:09:55'),
(3275, '2022-04-15 00:00:00', '108.162.245.108', '2022-04-15 23:21:09', '2022-04-15 23:21:09'),
(3276, '2022-04-16 00:00:00', '172.70.250.206', '2022-04-16 00:56:58', '2022-04-16 00:56:58'),
(3277, '2022-04-16 00:00:00', '172.70.85.204', '2022-04-16 01:14:58', '2022-04-16 01:14:58'),
(3278, '2022-04-16 00:00:00', '172.70.251.125', '2022-04-16 02:43:32', '2022-04-16 02:43:32'),
(3279, '2022-04-16 00:00:00', '172.70.251.125', '2022-04-16 05:20:25', '2022-04-16 05:20:25'),
(3280, '2022-04-16 00:00:00', '141.101.99.253', '2022-04-16 08:15:43', '2022-04-16 08:15:43'),
(3281, '2022-04-16 00:00:00', '162.158.107.231', '2022-04-16 09:32:29', '2022-04-16 09:32:29'),
(3282, '2022-04-16 00:00:00', '162.158.106.164', '2022-04-16 10:06:04', '2022-04-16 10:06:04'),
(3283, '2022-04-16 00:00:00', '172.70.189.117', '2022-04-16 10:14:54', '2022-04-16 10:14:54'),
(3284, '2022-04-16 00:00:00', '108.162.245.46', '2022-04-16 10:53:52', '2022-04-16 10:53:52'),
(3285, '2022-04-16 00:00:00', '172.70.135.7', '2022-04-16 12:23:51', '2022-04-16 12:23:51'),
(3286, '2022-04-16 00:00:00', '172.70.174.224', '2022-04-16 12:53:59', '2022-04-16 12:53:59'),
(3287, '2022-04-16 00:00:00', '172.70.211.157', '2022-04-16 12:57:01', '2022-04-16 12:57:01'),
(3288, '2022-04-16 00:00:00', '172.70.175.199', '2022-04-16 13:27:33', '2022-04-16 13:27:33'),
(3289, '2022-04-16 00:00:00', '172.70.38.30', '2022-04-16 13:53:52', '2022-04-16 13:53:52'),
(3290, '2022-04-16 00:00:00', '172.70.142.42', '2022-04-16 14:44:26', '2022-04-16 14:44:26'),
(3291, '2022-04-16 00:00:00', '172.68.146.32', '2022-04-16 14:54:18', '2022-04-16 14:54:18'),
(3292, '2022-04-16 00:00:00', '172.68.210.32', '2022-04-16 14:54:18', '2022-04-16 14:54:18'),
(3293, '2022-04-16 00:00:00', '172.70.251.125', '2022-04-16 14:54:20', '2022-04-16 14:54:20'),
(3294, '2022-04-16 00:00:00', '172.70.189.117', '2022-04-16 15:47:43', '2022-04-16 15:47:43'),
(3295, '2022-04-16 00:00:00', '172.70.35.51', '2022-04-16 16:27:33', '2022-04-16 16:27:33'),
(3296, '2022-04-16 00:00:00', '172.70.142.224', '2022-04-16 17:59:11', '2022-04-16 17:59:11'),
(3297, '2022-04-16 00:00:00', '172.70.188.36', '2022-04-16 18:04:18', '2022-04-16 18:04:18'),
(3298, '2022-04-16 00:00:00', '172.70.147.42', '2022-04-16 18:09:26', '2022-04-16 18:09:26'),
(3299, '2022-04-17 00:00:00', '172.70.134.44', '2022-04-17 00:44:12', '2022-04-17 00:44:12'),
(3300, '2022-04-17 00:00:00', '172.70.175.107', '2022-04-17 01:29:11', '2022-04-17 01:29:11'),
(3301, '2022-04-17 00:00:00', '172.70.135.67', '2022-04-17 01:44:11', '2022-04-17 01:44:11'),
(3302, '2022-04-17 00:00:00', '172.70.142.224', '2022-04-17 01:53:48', '2022-04-17 01:53:48'),
(3303, '2022-04-17 00:00:00', '172.70.135.29', '2022-04-17 01:59:11', '2022-04-17 01:59:11'),
(3304, '2022-04-17 00:00:00', '162.158.162.6', '2022-04-17 02:13:11', '2022-04-17 02:13:11'),
(3305, '2022-04-17 00:00:00', '172.70.92.150', '2022-04-17 02:21:18', '2022-04-17 02:21:18'),
(3306, '2022-04-17 00:00:00', '172.70.85.150', '2022-04-17 02:40:04', '2022-04-17 02:40:04'),
(3307, '2022-04-17 00:00:00', '172.70.135.45', '2022-04-17 02:44:12', '2022-04-17 02:44:12'),
(3308, '2022-04-17 00:00:00', '162.158.163.165', '2022-04-17 03:16:49', '2022-04-17 03:16:49'),
(3309, '2022-04-17 00:00:00', '172.70.174.198', '2022-04-17 03:33:06', '2022-04-17 03:33:06'),
(3310, '2022-04-17 00:00:00', '172.70.174.70', '2022-04-17 03:57:33', '2022-04-17 03:57:33'),
(3311, '2022-04-17 00:00:00', '172.70.38.8', '2022-04-17 04:22:01', '2022-04-17 04:22:01'),
(3312, '2022-04-17 00:00:00', '172.70.162.188', '2022-04-17 04:29:53', '2022-04-17 04:29:53'),
(3313, '2022-04-17 00:00:00', '172.70.175.199', '2022-04-17 05:15:41', '2022-04-17 05:15:41'),
(3314, '2022-04-17 00:00:00', '172.70.142.42', '2022-04-17 06:09:10', '2022-04-17 06:09:10'),
(3315, '2022-04-17 00:00:00', '141.101.77.200', '2022-04-17 09:28:58', '2022-04-17 09:28:58'),
(3316, '2022-04-17 00:00:00', '172.70.147.42', '2022-04-17 12:30:26', '2022-04-17 12:30:26'),
(3317, '2022-04-17 00:00:00', '172.70.188.36', '2022-04-17 13:39:53', '2022-04-17 13:39:53'),
(3318, '2022-04-17 00:00:00', '172.70.189.137', '2022-04-17 13:45:23', '2022-04-17 13:45:23'),
(3319, '2022-04-17 00:00:00', '172.69.70.32', '2022-04-17 13:56:12', '2022-04-17 13:56:12'),
(3320, '2022-04-17 00:00:00', '172.69.68.26', '2022-04-17 13:56:26', '2022-04-17 13:56:26'),
(3321, '2022-04-17 00:00:00', '172.70.175.199', '2022-04-17 14:14:02', '2022-04-17 14:14:02'),
(3322, '2022-04-17 00:00:00', '172.70.147.34', '2022-04-17 15:19:54', '2022-04-17 15:19:54'),
(3323, '2022-04-17 00:00:00', '172.70.175.69', '2022-04-17 16:20:53', '2022-04-17 16:20:53'),
(3324, '2022-04-17 00:00:00', '172.68.26.82', '2022-04-17 17:30:04', '2022-04-17 17:30:04'),
(3325, '2022-04-17 00:00:00', '162.158.162.66', '2022-04-17 17:34:12', '2022-04-17 17:34:12'),
(3326, '2022-04-17 00:00:00', '172.70.250.206', '2022-04-17 19:41:16', '2022-04-17 19:41:16'),
(3327, '2022-04-17 00:00:00', '172.70.90.164', '2022-04-17 21:01:16', '2022-04-17 21:01:16'),
(3328, '2022-04-17 00:00:00', '172.70.85.34', '2022-04-17 21:01:17', '2022-04-17 21:01:17'),
(3329, '2022-04-17 00:00:00', '162.158.159.110', '2022-04-17 21:57:45', '2022-04-17 21:57:45'),
(3330, '2022-04-17 00:00:00', '172.70.91.95', '2022-04-17 21:57:46', '2022-04-17 21:57:46'),
(3331, '2022-04-17 00:00:00', '141.101.98.244', '2022-04-17 23:01:25', '2022-04-17 23:01:25'),
(3332, '2022-04-17 00:00:00', '141.101.99.151', '2022-04-17 23:35:29', '2022-04-17 23:35:29'),
(3333, '2022-04-17 00:00:00', '172.70.90.6', '2022-04-17 23:35:31', '2022-04-17 23:35:31'),
(3334, '2022-04-17 00:00:00', '141.101.99.253', '2022-04-17 23:35:32', '2022-04-17 23:35:32'),
(3335, '2022-04-17 00:00:00', '172.70.85.34', '2022-04-17 23:38:05', '2022-04-17 23:38:05'),
(3336, '2022-04-17 00:00:00', '172.70.90.82', '2022-04-17 23:38:06', '2022-04-17 23:38:06'),
(3337, '2022-04-17 00:00:00', '162.158.159.126', '2022-04-17 23:38:07', '2022-04-17 23:38:07'),
(3338, '2022-04-17 00:00:00', '141.101.107.142', '2022-04-17 23:38:07', '2022-04-17 23:38:07'),
(3339, '2022-04-17 00:00:00', '172.70.162.188', '2022-04-17 23:44:51', '2022-04-17 23:44:51'),
(3340, '2022-04-17 00:00:00', '172.70.85.204', '2022-04-17 23:44:52', '2022-04-17 23:44:52'),
(3341, '2022-04-17 00:00:00', '172.70.162.22', '2022-04-17 23:44:52', '2022-04-17 23:44:52'),
(3342, '2022-04-17 00:00:00', '172.70.90.212', '2022-04-17 23:46:19', '2022-04-17 23:46:19'),
(3343, '2022-04-18 00:00:00', '172.70.92.156', '2022-04-18 00:17:04', '2022-04-18 00:17:04'),
(3344, '2022-04-18 00:00:00', '172.70.210.180', '2022-04-18 00:32:59', '2022-04-18 00:32:59'),
(3345, '2022-04-18 00:00:00', '172.70.92.156', '2022-04-18 01:13:46', '2022-04-18 01:13:46'),
(3346, '2022-04-18 00:00:00', '172.70.178.182', '2022-04-18 03:03:02', '2022-04-18 03:03:02'),
(3347, '2022-04-18 00:00:00', '172.70.92.150', '2022-04-18 03:16:23', '2022-04-18 03:16:23'),
(3348, '2022-04-18 00:00:00', '172.70.142.34', '2022-04-18 03:21:33', '2022-04-18 03:21:33'),
(3349, '2022-04-18 00:00:00', '172.69.170.142', '2022-04-18 03:25:58', '2022-04-18 03:25:58'),
(3350, '2022-04-18 00:00:00', '162.158.163.67', '2022-04-18 03:26:41', '2022-04-18 03:26:41'),
(3351, '2022-04-18 00:00:00', '108.162.246.223', '2022-04-18 03:50:53', '2022-04-18 03:50:53'),
(3352, '2022-04-18 00:00:00', '108.162.246.147', '2022-04-18 05:18:11', '2022-04-18 05:18:11'),
(3353, '2022-04-18 00:00:00', '162.158.162.66', '2022-04-18 05:47:37', '2022-04-18 05:47:37'),
(3354, '2022-04-18 00:00:00', '172.68.110.162', '2022-04-18 06:40:47', '2022-04-18 06:40:47'),
(3355, '2022-04-18 00:00:00', '172.70.189.137', '2022-04-18 07:23:09', '2022-04-18 07:23:09'),
(3356, '2022-04-18 00:00:00', '172.70.189.59', '2022-04-18 07:33:20', '2022-04-18 07:33:20'),
(3357, '2022-04-18 00:00:00', '141.101.69.143', '2022-04-18 09:53:28', '2022-04-18 09:53:28'),
(3358, '2022-04-18 00:00:00', '141.101.68.206', '2022-04-18 10:48:32', '2022-04-18 10:48:32'),
(3359, '2022-04-18 00:00:00', '141.101.68.198', '2022-04-18 10:48:32', '2022-04-18 10:48:32'),
(3360, '2022-04-18 00:00:00', '172.70.90.96', '2022-04-18 10:56:43', '2022-04-18 10:56:43'),
(3361, '2022-04-18 00:00:00', '108.162.242.11', '2022-04-18 11:07:57', '2022-04-18 11:07:57'),
(3362, '2022-04-18 00:00:00', '162.158.178.13', '2022-04-18 12:42:05', '2022-04-18 12:42:05'),
(3363, '2022-04-18 00:00:00', '162.158.62.137', '2022-04-18 13:04:17', '2022-04-18 13:04:17'),
(3364, '2022-04-18 00:00:00', '162.158.107.181', '2022-04-18 14:39:05', '2022-04-18 14:39:05'),
(3365, '2022-04-18 00:00:00', '162.158.107.105', '2022-04-18 14:40:02', '2022-04-18 14:40:02'),
(3366, '2022-04-18 00:00:00', '172.70.175.25', '2022-04-18 15:18:16', '2022-04-18 15:18:16'),
(3367, '2022-04-18 00:00:00', '172.70.174.66', '2022-04-18 16:03:16', '2022-04-18 16:03:16'),
(3368, '2022-04-18 00:00:00', '172.70.251.125', '2022-04-18 16:08:32', '2022-04-18 16:08:32'),
(3369, '2022-04-18 00:00:00', '172.70.250.206', '2022-04-18 16:08:32', '2022-04-18 16:08:32'),
(3370, '2022-04-18 00:00:00', '172.70.147.42', '2022-04-18 21:21:16', '2022-04-18 21:21:16'),
(3371, '2022-04-18 00:00:00', '172.70.147.48', '2022-04-18 21:21:16', '2022-04-18 21:21:16'),
(3372, '2022-04-18 00:00:00', '172.70.142.218', '2022-04-18 21:49:57', '2022-04-18 21:49:57'),
(3373, '2022-04-18 00:00:00', '172.68.10.40', '2022-04-18 21:59:20', '2022-04-18 21:59:20'),
(3374, '2022-04-18 00:00:00', '172.70.135.147', '2022-04-18 22:41:08', '2022-04-18 22:41:08'),
(3375, '2022-04-18 00:00:00', '172.70.175.199', '2022-04-18 23:26:07', '2022-04-18 23:26:07'),
(3376, '2022-04-18 00:00:00', '108.162.245.48', '2022-04-18 23:52:50', '2022-04-18 23:52:50'),
(3377, '2022-04-19 00:00:00', '162.158.79.77', '2022-04-19 00:09:21', '2022-04-19 00:09:21'),
(3378, '2022-04-19 00:00:00', '172.70.142.42', '2022-04-19 00:43:54', '2022-04-19 00:43:54'),
(3379, '2022-04-19 00:00:00', '172.70.142.218', '2022-04-19 01:12:36', '2022-04-19 01:12:36'),
(3380, '2022-04-19 00:00:00', '162.158.92.200', '2022-04-19 01:13:41', '2022-04-19 01:13:41'),
(3381, '2022-04-19 00:00:00', '172.70.135.187', '2022-04-19 01:37:35', '2022-04-19 01:37:35'),
(3382, '2022-04-19 00:00:00', '172.70.147.42', '2022-04-19 02:50:04', '2022-04-19 02:50:04'),
(3383, '2022-04-19 00:00:00', '172.70.242.210', '2022-04-19 03:02:46', '2022-04-19 03:02:46'),
(3384, '2022-04-19 00:00:00', '172.70.142.224', '2022-04-19 03:15:37', '2022-04-19 03:15:37'),
(3385, '2022-04-19 00:00:00', '141.101.77.200', '2022-04-19 03:22:50', '2022-04-19 03:22:50'),
(3386, '2022-04-19 00:00:00', '172.70.147.42', '2022-04-19 06:20:49', '2022-04-19 06:20:49'),
(3387, '2022-04-19 00:00:00', '108.162.246.241', '2022-04-19 06:58:55', '2022-04-19 06:58:55'),
(3388, '2022-04-19 00:00:00', '172.70.90.150', '2022-04-19 07:06:52', '2022-04-19 07:06:52'),
(3389, '2022-04-19 00:00:00', '108.162.241.234', '2022-04-19 07:34:39', '2022-04-19 07:34:39'),
(3390, '2022-04-19 00:00:00', '162.158.222.124', '2022-04-19 07:37:26', '2022-04-19 07:37:26'),
(3391, '2022-04-19 00:00:00', '172.70.147.42', '2022-04-19 08:35:52', '2022-04-19 08:35:52'),
(3392, '2022-04-19 00:00:00', '172.70.189.117', '2022-04-19 08:43:12', '2022-04-19 08:43:12'),
(3393, '2022-04-19 00:00:00', '172.70.142.218', '2022-04-19 09:00:38', '2022-04-19 09:00:38'),
(3394, '2022-04-19 00:00:00', '172.70.135.217', '2022-04-19 09:08:19', '2022-04-19 09:08:19'),
(3395, '2022-04-19 00:00:00', '172.70.147.34', '2022-04-19 09:09:28', '2022-04-19 09:09:28'),
(3396, '2022-04-19 00:00:00', '162.158.162.236', '2022-04-19 09:17:00', '2022-04-19 09:17:00'),
(3397, '2022-04-19 00:00:00', '162.158.162.116', '2022-04-19 09:31:28', '2022-04-19 09:31:28'),
(3398, '2022-04-19 00:00:00', '172.70.189.137', '2022-04-19 09:39:57', '2022-04-19 09:39:57'),
(3399, '2022-04-19 00:00:00', '172.70.174.82', '2022-04-19 09:53:19', '2022-04-19 09:53:19'),
(3400, '2022-04-19 00:00:00', '172.70.147.34', '2022-04-19 10:02:32', '2022-04-19 10:02:32'),
(3401, '2022-04-19 00:00:00', '172.70.142.218', '2022-04-19 10:13:21', '2022-04-19 10:13:21'),
(3402, '2022-04-19 00:00:00', '172.70.142.42', '2022-04-19 10:25:21', '2022-04-19 10:25:21'),
(3403, '2022-04-19 00:00:00', '172.68.50.192', '2022-04-19 10:51:29', '2022-04-19 10:51:29'),
(3404, '2022-04-19 00:00:00', '172.70.135.87', '2022-04-19 10:53:52', '2022-04-19 10:53:52'),
(3405, '2022-04-19 00:00:00', '172.70.142.42', '2022-04-19 13:53:11', '2022-04-19 13:53:11'),
(3406, '2022-04-19 00:00:00', '172.70.142.34', '2022-04-19 13:59:28', '2022-04-19 13:59:28'),
(3407, '2022-04-19 00:00:00', '172.70.92.156', '2022-04-19 14:13:21', '2022-04-19 14:13:21'),
(3408, '2022-04-19 00:00:00', '162.158.162.116', '2022-04-19 14:21:38', '2022-04-19 14:21:38'),
(3409, '2022-04-19 00:00:00', '172.70.142.218', '2022-04-19 14:30:00', '2022-04-19 14:30:00'),
(3410, '2022-04-19 00:00:00', '172.70.92.156', '2022-04-19 14:38:13', '2022-04-19 14:38:13'),
(3411, '2022-04-19 00:00:00', '172.70.142.224', '2022-04-19 15:07:41', '2022-04-19 15:07:41'),
(3412, '2022-04-19 00:00:00', '172.70.189.137', '2022-04-19 15:20:55', '2022-04-19 15:20:55'),
(3413, '2022-04-19 00:00:00', '172.70.147.34', '2022-04-19 15:42:04', '2022-04-19 15:42:04'),
(3414, '2022-04-19 00:00:00', '172.70.189.117', '2022-04-19 15:58:05', '2022-04-19 15:58:05'),
(3415, '2022-04-19 00:00:00', '162.158.163.155', '2022-04-19 16:07:59', '2022-04-19 16:07:59'),
(3416, '2022-04-19 00:00:00', '162.158.162.64', '2022-04-19 16:31:31', '2022-04-19 16:31:31'),
(3417, '2022-04-19 00:00:00', '172.70.175.47', '2022-04-19 16:34:34', '2022-04-19 16:34:34'),
(3418, '2022-04-19 00:00:00', '162.158.163.181', '2022-04-19 16:51:19', '2022-04-19 16:51:19'),
(3419, '2022-04-19 00:00:00', '162.158.162.80', '2022-04-19 17:04:06', '2022-04-19 17:04:06'),
(3420, '2022-04-19 00:00:00', '172.70.206.34', '2022-04-19 17:05:02', '2022-04-19 17:05:02'),
(3421, '2022-04-19 00:00:00', '172.70.142.34', '2022-04-19 17:17:22', '2022-04-19 17:17:22'),
(3422, '2022-04-19 00:00:00', '172.70.142.224', '2022-04-19 17:29:23', '2022-04-19 17:29:23'),
(3423, '2022-04-19 00:00:00', '172.70.142.224', '2022-04-19 17:41:47', '2022-04-19 17:41:47'),
(3424, '2022-04-19 00:00:00', '172.70.189.137', '2022-04-19 17:54:21', '2022-04-19 17:54:21'),
(3425, '2022-04-19 00:00:00', '162.158.163.157', '2022-04-19 18:06:23', '2022-04-19 18:06:23'),
(3426, '2022-04-19 00:00:00', '172.70.92.150', '2022-04-19 18:53:42', '2022-04-19 18:53:42'),
(3427, '2022-04-19 00:00:00', '172.70.142.34', '2022-04-19 19:05:16', '2022-04-19 19:05:16'),
(3428, '2022-04-19 00:00:00', '108.162.221.56', '2022-04-19 19:08:01', '2022-04-19 19:08:01'),
(3429, '2022-04-19 00:00:00', '172.69.70.140', '2022-04-19 19:08:13', '2022-04-19 19:08:13'),
(3430, '2022-04-19 00:00:00', '172.70.142.218', '2022-04-19 19:17:42', '2022-04-19 19:17:42'),
(3431, '2022-04-19 00:00:00', '108.162.246.241', '2022-04-19 19:37:53', '2022-04-19 19:37:53'),
(3432, '2022-04-19 00:00:00', '172.70.142.218', '2022-04-19 19:50:35', '2022-04-19 19:50:35'),
(3433, '2022-04-19 00:00:00', '172.70.142.42', '2022-04-19 20:12:52', '2022-04-19 20:12:52'),
(3434, '2022-04-19 00:00:00', '162.158.163.157', '2022-04-19 20:35:11', '2022-04-19 20:35:11'),
(3435, '2022-04-19 00:00:00', '162.158.162.80', '2022-04-19 20:46:06', '2022-04-19 20:46:06'),
(3436, '2022-04-19 00:00:00', '172.70.147.42', '2022-04-19 20:57:03', '2022-04-19 20:57:03'),
(3437, '2022-04-19 00:00:00', '172.70.189.137', '2022-04-19 21:08:11', '2022-04-19 21:08:11'),
(3438, '2022-04-19 00:00:00', '172.70.147.34', '2022-04-19 21:19:02', '2022-04-19 21:19:02'),
(3439, '2022-04-19 00:00:00', '172.70.189.137', '2022-04-19 21:30:02', '2022-04-19 21:30:02'),
(3440, '2022-04-19 00:00:00', '172.70.142.34', '2022-04-19 21:51:38', '2022-04-19 21:51:38'),
(3441, '2022-04-19 00:00:00', '172.70.189.137', '2022-04-19 22:13:10', '2022-04-19 22:13:10'),
(3442, '2022-04-19 00:00:00', '108.162.246.223', '2022-04-19 22:29:37', '2022-04-19 22:29:37'),
(3443, '2022-04-19 00:00:00', '172.70.142.224', '2022-04-19 22:34:48', '2022-04-19 22:34:48'),
(3444, '2022-04-19 00:00:00', '172.70.189.117', '2022-04-19 23:16:31', '2022-04-19 23:16:31'),
(3445, '2022-04-19 00:00:00', '162.158.163.213', '2022-04-19 23:27:07', '2022-04-19 23:27:07'),
(3446, '2022-04-20 00:00:00', '162.158.92.148', '2022-04-20 03:31:06', '2022-04-20 03:31:06'),
(3447, '2022-04-20 00:00:00', '162.158.92.76', '2022-04-20 06:17:20', '2022-04-20 06:17:20'),
(3448, '2022-04-20 00:00:00', '162.158.166.188', '2022-04-20 06:30:18', '2022-04-20 06:30:18'),
(3449, '2022-04-20 00:00:00', '162.158.48.128', '2022-04-20 07:11:43', '2022-04-20 07:11:43'),
(3450, '2022-04-20 00:00:00', '162.158.235.81', '2022-04-20 07:11:57', '2022-04-20 07:11:57'),
(3451, '2022-04-20 00:00:00', '172.70.142.34', '2022-04-20 07:25:02', '2022-04-20 07:25:02'),
(3452, '2022-04-20 00:00:00', '162.158.235.25', '2022-04-20 07:28:19', '2022-04-20 07:28:19'),
(3453, '2022-04-20 00:00:00', '172.70.222.28', '2022-04-20 07:29:58', '2022-04-20 07:29:58'),
(3454, '2022-04-20 00:00:00', '172.68.39.134', '2022-04-20 07:40:34', '2022-04-20 07:40:34'),
(3455, '2022-04-20 00:00:00', '172.68.39.150', '2022-04-20 07:41:04', '2022-04-20 07:41:04'),
(3456, '2022-04-20 00:00:00', '162.158.227.224', '2022-04-20 08:04:30', '2022-04-20 08:04:30'),
(3457, '2022-04-20 00:00:00', '172.70.218.198', '2022-04-20 08:11:13', '2022-04-20 08:11:13'),
(3458, '2022-04-20 00:00:00', '162.158.178.178', '2022-04-20 08:12:46', '2022-04-20 08:12:46'),
(3459, '2022-04-20 00:00:00', '162.158.227.234', '2022-04-20 08:20:01', '2022-04-20 08:20:01'),
(3460, '2022-04-20 00:00:00', '172.68.39.216', '2022-04-20 08:31:54', '2022-04-20 08:31:54'),
(3461, '2022-04-20 00:00:00', '172.70.218.174', '2022-04-20 08:57:09', '2022-04-20 08:57:09'),
(3462, '2022-04-20 00:00:00', '172.70.251.125', '2022-04-20 09:02:15', '2022-04-20 09:02:15'),
(3463, '2022-04-20 00:00:00', '162.158.90.130', '2022-04-20 09:05:10', '2022-04-20 09:05:10'),
(3464, '2022-04-20 00:00:00', '162.158.235.161', '2022-04-20 09:08:32', '2022-04-20 09:08:32'),
(3465, '2022-04-20 00:00:00', '172.70.218.28', '2022-04-20 09:09:57', '2022-04-20 09:09:57'),
(3466, '2022-04-20 00:00:00', '162.158.92.76', '2022-04-20 09:11:39', '2022-04-20 09:11:39'),
(3467, '2022-04-20 00:00:00', '172.70.242.72', '2022-04-20 09:13:07', '2022-04-20 09:13:07'),
(3468, '2022-04-20 00:00:00', '172.70.250.64', '2022-04-20 09:14:07', '2022-04-20 09:14:07'),
(3469, '2022-04-20 00:00:00', '172.70.251.51', '2022-04-20 09:16:05', '2022-04-20 09:16:05'),
(3470, '2022-04-20 00:00:00', '172.70.242.210', '2022-04-20 09:34:33', '2022-04-20 09:34:33'),
(3471, '2022-04-20 00:00:00', '172.70.242.68', '2022-04-20 09:49:05', '2022-04-20 09:49:05'),
(3472, '2022-04-20 00:00:00', '172.70.242.182', '2022-04-20 09:49:05', '2022-04-20 09:49:05'),
(3473, '2022-04-20 00:00:00', '162.158.227.234', '2022-04-20 11:07:13', '2022-04-20 11:07:13'),
(3474, '2022-04-20 00:00:00', '162.158.227.236', '2022-04-20 11:15:58', '2022-04-20 11:15:58'),
(3475, '2022-04-20 00:00:00', '162.158.227.238', '2022-04-20 11:19:08', '2022-04-20 11:19:08'),
(3476, '2022-04-20 00:00:00', '162.158.89.235', '2022-04-20 11:22:31', '2022-04-20 11:22:31'),
(3477, '2022-04-20 00:00:00', '108.162.246.7', '2022-04-20 11:46:36', '2022-04-20 11:46:36'),
(3478, '2022-04-20 00:00:00', '108.162.246.49', '2022-04-20 11:46:37', '2022-04-20 11:46:37'),
(3479, '2022-04-20 00:00:00', '172.70.211.157', '2022-04-20 11:46:37', '2022-04-20 11:46:37'),
(3480, '2022-04-20 00:00:00', '172.69.134.94', '2022-04-20 11:46:38', '2022-04-20 11:46:38'),
(3481, '2022-04-20 00:00:00', '162.158.107.151', '2022-04-20 11:46:38', '2022-04-20 11:46:38'),
(3482, '2022-04-20 00:00:00', '108.162.245.92', '2022-04-20 11:46:38', '2022-04-20 11:46:38'),
(3483, '2022-04-20 00:00:00', '162.158.166.188', '2022-04-20 11:46:39', '2022-04-20 11:46:39'),
(3484, '2022-04-20 00:00:00', '162.158.107.69', '2022-04-20 11:46:40', '2022-04-20 11:46:40'),
(3485, '2022-04-20 00:00:00', '108.162.246.105', '2022-04-20 11:46:40', '2022-04-20 11:46:40'),
(3486, '2022-04-20 00:00:00', '162.158.107.133', '2022-04-20 11:46:40', '2022-04-20 11:46:40'),
(3487, '2022-04-20 00:00:00', '172.70.206.32', '2022-04-20 11:46:41', '2022-04-20 11:46:41'),
(3488, '2022-04-20 00:00:00', '172.69.134.70', '2022-04-20 11:46:41', '2022-04-20 11:46:41'),
(3489, '2022-04-20 00:00:00', '108.162.245.214', '2022-04-20 11:46:41', '2022-04-20 11:46:41'),
(3490, '2022-04-20 00:00:00', '162.158.107.167', '2022-04-20 11:46:41', '2022-04-20 11:46:41'),
(3491, '2022-04-20 00:00:00', '108.162.246.45', '2022-04-20 11:46:42', '2022-04-20 11:46:42'),
(3492, '2022-04-20 00:00:00', '162.158.106.212', '2022-04-20 11:46:43', '2022-04-20 11:46:43'),
(3493, '2022-04-20 00:00:00', '172.68.133.64', '2022-04-20 11:46:43', '2022-04-20 11:46:43'),
(3494, '2022-04-20 00:00:00', '108.162.245.36', '2022-04-20 11:46:43', '2022-04-20 11:46:43'),
(3495, '2022-04-20 00:00:00', '108.162.245.204', '2022-04-20 11:46:44', '2022-04-20 11:46:44'),
(3496, '2022-04-20 00:00:00', '108.162.246.117', '2022-04-20 11:46:44', '2022-04-20 11:46:44'),
(3497, '2022-04-20 00:00:00', '162.158.166.56', '2022-04-20 11:46:45', '2022-04-20 11:46:45'),
(3498, '2022-04-20 00:00:00', '108.162.245.126', '2022-04-20 11:46:45', '2022-04-20 11:46:45'),
(3499, '2022-04-20 00:00:00', '162.158.106.146', '2022-04-20 11:46:46', '2022-04-20 11:46:46'),
(3500, '2022-04-20 00:00:00', '162.158.106.124', '2022-04-20 11:46:46', '2022-04-20 11:46:46'),
(3501, '2022-04-20 00:00:00', '162.158.107.21', '2022-04-20 11:46:47', '2022-04-20 11:46:47'),
(3502, '2022-04-20 00:00:00', '172.70.214.32', '2022-04-20 11:46:48', '2022-04-20 11:46:48'),
(3503, '2022-04-20 00:00:00', '162.158.107.209', '2022-04-20 11:46:48', '2022-04-20 11:46:48'),
(3504, '2022-04-20 00:00:00', '172.68.143.252', '2022-04-20 11:46:49', '2022-04-20 11:46:49'),
(3505, '2022-04-20 00:00:00', '172.70.211.121', '2022-04-20 11:46:50', '2022-04-20 11:46:50'),
(3506, '2022-04-20 00:00:00', '172.70.210.180', '2022-04-20 11:46:50', '2022-04-20 11:46:50'),
(3507, '2022-04-20 00:00:00', '172.69.33.242', '2022-04-20 11:46:52', '2022-04-20 11:46:52'),
(3508, '2022-04-20 00:00:00', '162.158.166.186', '2022-04-20 11:46:54', '2022-04-20 11:46:54'),
(3509, '2022-04-20 00:00:00', '172.70.206.170', '2022-04-20 11:46:57', '2022-04-20 11:46:57'),
(3510, '2022-04-20 00:00:00', '162.158.235.81', '2022-04-20 12:07:31', '2022-04-20 12:07:31'),
(3511, '2022-04-20 00:00:00', '172.70.189.117', '2022-04-20 12:15:54', '2022-04-20 12:15:54'),
(3512, '2022-04-20 00:00:00', '172.70.218.236', '2022-04-20 12:16:12', '2022-04-20 12:16:12'),
(3513, '2022-04-20 00:00:00', '172.70.251.51', '2022-04-20 12:35:23', '2022-04-20 12:35:23'),
(3514, '2022-04-20 00:00:00', '172.70.246.52', '2022-04-20 12:36:52', '2022-04-20 12:36:52'),
(3515, '2022-04-20 00:00:00', '172.70.242.72', '2022-04-20 12:43:54', '2022-04-20 12:43:54'),
(3516, '2022-04-20 00:00:00', '172.70.218.174', '2022-04-20 12:53:35', '2022-04-20 12:53:35'),
(3517, '2022-04-20 00:00:00', '162.158.227.234', '2022-04-20 12:55:08', '2022-04-20 12:55:08'),
(3518, '2022-04-20 00:00:00', '172.70.218.236', '2022-04-20 12:58:29', '2022-04-20 12:58:29'),
(3519, '2022-04-20 00:00:00', '162.158.227.224', '2022-04-20 13:16:08', '2022-04-20 13:16:08'),
(3520, '2022-04-20 00:00:00', '172.70.210.166', '2022-04-20 13:34:20', '2022-04-20 13:34:20'),
(3521, '2022-04-20 00:00:00', '172.70.142.218', '2022-04-20 13:38:13', '2022-04-20 13:38:13'),
(3522, '2022-04-20 00:00:00', '172.70.135.15', '2022-04-20 14:01:36', '2022-04-20 14:01:36'),
(3523, '2022-04-20 00:00:00', '172.70.34.2', '2022-04-20 14:07:54', '2022-04-20 14:07:54'),
(3524, '2022-04-20 00:00:00', '172.70.142.218', '2022-04-20 14:08:42', '2022-04-20 14:08:42'),
(3525, '2022-04-20 00:00:00', '172.70.92.150', '2022-04-20 14:22:43', '2022-04-20 14:22:43'),
(3526, '2022-04-20 00:00:00', '172.70.92.150', '2022-04-20 14:36:00', '2022-04-20 14:36:00'),
(3527, '2022-04-20 00:00:00', '172.70.142.34', '2022-04-20 14:48:46', '2022-04-20 14:48:46'),
(3528, '2022-04-20 00:00:00', '141.101.77.67', '2022-04-20 14:57:26', '2022-04-20 14:57:26'),
(3529, '2022-04-20 00:00:00', '172.70.92.156', '2022-04-20 15:01:41', '2022-04-20 15:01:41'),
(3530, '2022-04-20 00:00:00', '172.70.135.81', '2022-04-20 15:08:44', '2022-04-20 15:08:44'),
(3531, '2022-04-20 00:00:00', '172.70.147.42', '2022-04-20 15:15:54', '2022-04-20 15:15:54'),
(3532, '2022-04-20 00:00:00', '172.70.142.218', '2022-04-20 15:29:02', '2022-04-20 15:29:02'),
(3533, '2022-04-20 00:00:00', '172.70.142.218', '2022-04-20 15:41:14', '2022-04-20 15:41:14'),
(3534, '2022-04-20 00:00:00', '172.70.92.150', '2022-04-20 15:52:57', '2022-04-20 15:52:57'),
(3535, '2022-04-20 00:00:00', '172.70.188.36', '2022-04-20 16:04:27', '2022-04-20 16:04:27'),
(3536, '2022-04-20 00:00:00', '172.70.92.156', '2022-04-20 16:15:54', '2022-04-20 16:15:54'),
(3537, '2022-04-20 00:00:00', '162.158.162.112', '2022-04-20 16:18:19', '2022-04-20 16:18:19'),
(3538, '2022-04-20 00:00:00', '162.158.162.96', '2022-04-20 16:18:19', '2022-04-20 16:18:19'),
(3539, '2022-04-20 00:00:00', '172.70.142.224', '2022-04-20 16:27:16', '2022-04-20 16:27:16'),
(3540, '2022-04-20 00:00:00', '172.70.92.156', '2022-04-20 16:38:38', '2022-04-20 16:38:38'),
(3541, '2022-04-20 00:00:00', '172.70.142.218', '2022-04-20 16:59:53', '2022-04-20 16:59:53'),
(3542, '2022-04-20 00:00:00', '172.70.142.34', '2022-04-20 17:10:35', '2022-04-20 17:10:35'),
(3543, '2022-04-20 00:00:00', '172.70.189.59', '2022-04-20 17:31:51', '2022-04-20 17:31:51'),
(3544, '2022-04-20 00:00:00', '172.70.188.36', '2022-04-20 17:42:18', '2022-04-20 17:42:18'),
(3545, '2022-04-20 00:00:00', '172.70.147.34', '2022-04-20 17:52:39', '2022-04-20 17:52:39'),
(3546, '2022-04-20 00:00:00', '172.70.189.117', '2022-04-20 18:13:17', '2022-04-20 18:13:17'),
(3547, '2022-04-20 00:00:00', '172.70.189.117', '2022-04-20 18:33:56', '2022-04-20 18:33:56'),
(3548, '2022-04-20 00:00:00', '108.162.246.25', '2022-04-20 18:35:29', '2022-04-20 18:35:29'),
(3549, '2022-04-20 00:00:00', '172.70.142.42', '2022-04-20 19:04:39', '2022-04-20 19:04:39'),
(3550, '2022-04-20 00:00:00', '172.70.147.34', '2022-04-20 19:14:39', '2022-04-20 19:14:39'),
(3551, '2022-04-20 00:00:00', '108.162.245.68', '2022-04-20 19:19:44', '2022-04-20 19:19:44'),
(3552, '2022-04-20 00:00:00', '172.70.147.42', '2022-04-20 19:34:42', '2022-04-20 19:34:42'),
(3553, '2022-04-20 00:00:00', '162.158.107.165', '2022-04-20 23:38:22', '2022-04-20 23:38:22'),
(3554, '2022-04-20 00:00:00', '172.70.175.175', '2022-04-20 23:52:56', '2022-04-20 23:52:56'),
(3555, '2022-04-21 00:00:00', '162.158.106.130', '2022-04-21 00:00:53', '2022-04-21 00:00:53'),
(3556, '2022-04-21 00:00:00', '162.158.106.212', '2022-04-21 00:23:21', '2022-04-21 00:23:21'),
(3557, '2022-04-21 00:00:00', '172.70.189.59', '2022-04-21 00:55:00', '2022-04-21 00:55:00'),
(3558, '2022-04-21 00:00:00', '108.162.246.241', '2022-04-21 01:08:21', '2022-04-21 01:08:21'),
(3559, '2022-04-21 00:00:00', '108.162.246.25', '2022-04-21 01:15:24', '2022-04-21 01:15:24'),
(3560, '2022-04-21 00:00:00', '172.70.188.36', '2022-04-21 01:23:47', '2022-04-21 01:23:47'),
(3561, '2022-04-21 00:00:00', '172.68.254.18', '2022-04-21 01:36:07', '2022-04-21 01:36:07'),
(3562, '2022-04-21 00:00:00', '172.70.92.156', '2022-04-21 02:07:27', '2022-04-21 02:07:27'),
(3563, '2022-04-21 00:00:00', '172.70.175.175', '2022-04-21 02:14:26', '2022-04-21 02:14:26'),
(3564, '2022-04-21 00:00:00', '162.158.106.212', '2022-04-21 03:06:55', '2022-04-21 03:06:55'),
(3565, '2022-04-21 00:00:00', '162.158.106.128', '2022-04-21 04:06:55', '2022-04-21 04:06:55'),
(3566, '2022-04-21 00:00:00', '108.162.216.68', '2022-04-21 05:06:48', '2022-04-21 05:06:48'),
(3567, '2022-04-21 00:00:00', '172.70.135.81', '2022-04-21 05:48:41', '2022-04-21 05:48:41'),
(3568, '2022-04-21 00:00:00', '172.70.218.174', '2022-04-21 08:40:29', '2022-04-21 08:40:29'),
(3569, '2022-04-21 00:00:00', '162.158.235.161', '2022-04-21 08:45:48', '2022-04-21 08:45:48'),
(3570, '2022-04-21 00:00:00', '172.70.218.198', '2022-04-21 08:50:44', '2022-04-21 08:50:44'),
(3571, '2022-04-21 00:00:00', '172.70.218.236', '2022-04-21 08:52:27', '2022-04-21 08:52:27'),
(3572, '2022-04-21 00:00:00', '162.158.235.43', '2022-04-21 08:55:14', '2022-04-21 08:55:14'),
(3573, '2022-04-21 00:00:00', '172.70.218.28', '2022-04-21 08:56:11', '2022-04-21 08:56:11'),
(3574, '2022-04-21 00:00:00', '162.158.227.236', '2022-04-21 08:58:13', '2022-04-21 08:58:13'),
(3575, '2022-04-21 00:00:00', '162.158.235.161', '2022-04-21 09:23:01', '2022-04-21 09:23:01'),
(3576, '2022-04-21 00:00:00', '172.70.246.180', '2022-04-21 09:29:06', '2022-04-21 09:29:06'),
(3577, '2022-04-21 00:00:00', '172.70.218.174', '2022-04-21 09:40:07', '2022-04-21 09:40:07'),
(3578, '2022-04-21 00:00:00', '172.70.242.210', '2022-04-21 09:45:59', '2022-04-21 09:45:59'),
(3579, '2022-04-21 00:00:00', '172.70.250.206', '2022-04-21 09:58:53', '2022-04-21 09:58:53'),
(3580, '2022-04-21 00:00:00', '162.158.118.94', '2022-04-21 09:59:34', '2022-04-21 09:59:34'),
(3581, '2022-04-21 00:00:00', '172.70.85.150', '2022-04-21 09:59:40', '2022-04-21 09:59:40'),
(3582, '2022-04-21 00:00:00', '172.70.188.36', '2022-04-21 10:01:54', '2022-04-21 10:01:54'),
(3583, '2022-04-21 00:00:00', '172.70.142.34', '2022-04-21 10:45:54', '2022-04-21 10:45:54'),
(3584, '2022-04-21 00:00:00', '172.70.142.34', '2022-04-21 11:04:47', '2022-04-21 11:04:47'),
(3585, '2022-04-21 00:00:00', '172.70.218.236', '2022-04-21 11:09:12', '2022-04-21 11:09:12'),
(3586, '2022-04-21 00:00:00', '172.70.251.51', '2022-04-21 11:12:37', '2022-04-21 11:12:37'),
(3587, '2022-04-21 00:00:00', '172.70.130.82', '2022-04-21 11:42:40', '2022-04-21 11:42:40'),
(3588, '2022-04-21 00:00:00', '162.158.235.161', '2022-04-21 12:04:13', '2022-04-21 12:04:13'),
(3589, '2022-04-21 00:00:00', '172.70.90.212', '2022-04-21 12:17:36', '2022-04-21 12:17:36'),
(3590, '2022-04-21 00:00:00', '172.68.39.236', '2022-04-21 12:25:22', '2022-04-21 12:25:22'),
(3591, '2022-04-21 00:00:00', '172.68.39.216', '2022-04-21 12:26:35', '2022-04-21 12:26:35'),
(3592, '2022-04-21 00:00:00', '172.70.246.180', '2022-04-21 12:27:10', '2022-04-21 12:27:10'),
(3593, '2022-04-21 00:00:00', '172.70.175.117', '2022-04-21 12:51:46', '2022-04-21 12:51:46'),
(3594, '2022-04-21 00:00:00', '172.70.85.40', '2022-04-21 13:12:36', '2022-04-21 13:12:36'),
(3595, '2022-04-21 00:00:00', '162.158.78.48', '2022-04-21 13:36:46', '2022-04-21 13:36:46'),
(3596, '2022-04-21 00:00:00', '172.70.246.52', '2022-04-21 13:56:08', '2022-04-21 13:56:08'),
(3597, '2022-04-21 00:00:00', '162.158.235.81', '2022-04-21 14:00:08', '2022-04-21 14:00:08'),
(3598, '2022-04-21 00:00:00', '172.68.39.216', '2022-04-21 14:02:17', '2022-04-21 14:02:17'),
(3599, '2022-04-21 00:00:00', '172.70.175.47', '2022-04-21 14:03:00', '2022-04-21 14:03:00'),
(3600, '2022-04-21 00:00:00', '172.70.218.174', '2022-04-21 14:08:11', '2022-04-21 14:08:11'),
(3601, '2022-04-21 00:00:00', '162.158.235.181', '2022-04-21 14:17:38', '2022-04-21 14:17:38'),
(3602, '2022-04-21 00:00:00', '172.68.39.236', '2022-04-21 14:18:22', '2022-04-21 14:18:22'),
(3603, '2022-04-21 00:00:00', '172.70.218.28', '2022-04-21 14:21:48', '2022-04-21 14:21:48'),
(3604, '2022-04-21 00:00:00', '172.68.11.113', '2022-04-21 14:22:58', '2022-04-21 14:22:58'),
(3605, '2022-04-21 00:00:00', '172.70.250.206', '2022-04-21 14:24:37', '2022-04-21 14:24:37'),
(3606, '2022-04-21 00:00:00', '172.70.242.210', '2022-04-21 14:25:43', '2022-04-21 14:25:43'),
(3607, '2022-04-21 00:00:00', '172.70.246.52', '2022-04-21 14:27:08', '2022-04-21 14:27:08'),
(3608, '2022-04-21 00:00:00', '172.70.218.28', '2022-04-21 14:34:18', '2022-04-21 14:34:18'),
(3609, '2022-04-21 00:00:00', '172.70.206.94', '2022-04-21 17:10:25', '2022-04-21 17:10:25'),
(3610, '2022-04-21 00:00:00', '172.70.211.121', '2022-04-21 17:10:30', '2022-04-21 17:10:30'),
(3611, '2022-04-21 00:00:00', '172.70.210.226', '2022-04-21 17:10:37', '2022-04-21 17:10:37'),
(3612, '2022-04-21 00:00:00', '172.70.85.150', '2022-04-21 17:21:03', '2022-04-21 17:21:03'),
(3613, '2022-04-21 00:00:00', '172.70.174.198', '2022-04-21 22:11:47', '2022-04-21 22:11:47'),
(3614, '2022-04-21 00:00:00', '172.70.189.137', '2022-04-21 22:54:52', '2022-04-21 22:54:52'),
(3615, '2022-04-21 00:00:00', '172.70.38.218', '2022-04-21 23:41:47', '2022-04-21 23:41:47'),
(3616, '2022-04-21 00:00:00', '172.70.142.218', '2022-04-21 23:49:01', '2022-04-21 23:49:01'),
(3617, '2022-04-22 00:00:00', '172.70.142.32', '2022-04-22 00:28:11', '2022-04-22 00:28:11'),
(3618, '2022-04-22 00:00:00', '172.70.189.137', '2022-04-22 00:50:19', '2022-04-22 00:50:19'),
(3619, '2022-04-22 00:00:00', '172.70.242.210', '2022-04-22 00:51:18', '2022-04-22 00:51:18'),
(3620, '2022-04-22 00:00:00', '162.158.78.28', '2022-04-22 00:52:47', '2022-04-22 00:52:47'),
(3621, '2022-04-22 00:00:00', '172.70.174.76', '2022-04-22 01:11:47', '2022-04-22 01:11:47'),
(3622, '2022-04-22 00:00:00', '172.69.69.140', '2022-04-22 01:45:35', '2022-04-22 01:45:35'),
(3623, '2022-04-22 00:00:00', '172.70.178.86', '2022-04-22 01:45:45', '2022-04-22 01:45:45'),
(3624, '2022-04-22 00:00:00', '162.158.78.16', '2022-04-22 01:47:24', '2022-04-22 01:47:24'),
(3625, '2022-04-22 00:00:00', '172.70.135.77', '2022-04-22 02:24:50', '2022-04-22 02:24:50'),
(3626, '2022-04-22 00:00:00', '172.70.174.190', '2022-04-22 02:39:32', '2022-04-22 02:39:32'),
(3627, '2022-04-22 00:00:00', '162.158.134.22', '2022-04-22 02:44:41', '2022-04-22 02:44:41'),
(3628, '2022-04-22 00:00:00', '172.70.175.17', '2022-04-22 03:07:07', '2022-04-22 03:07:07'),
(3629, '2022-04-22 00:00:00', '172.70.174.78', '2022-04-22 03:40:42', '2022-04-22 03:40:42'),
(3630, '2022-04-22 00:00:00', '172.70.134.154', '2022-04-22 04:11:06', '2022-04-22 04:11:06'),
(3631, '2022-04-22 00:00:00', '172.70.174.58', '2022-04-22 04:14:24', '2022-04-22 04:14:24'),
(3632, '2022-04-22 00:00:00', '172.68.146.46', '2022-04-22 04:36:15', '2022-04-22 04:36:15'),
(3633, '2022-04-22 00:00:00', '162.158.78.152', '2022-04-22 04:47:51', '2022-04-22 04:47:51'),
(3634, '2022-04-22 00:00:00', '172.70.134.140', '2022-04-22 05:21:26', '2022-04-22 05:21:26'),
(3635, '2022-04-22 00:00:00', '172.70.218.198', '2022-04-22 05:31:56', '2022-04-22 05:31:56'),
(3636, '2022-04-22 00:00:00', '172.70.251.51', '2022-04-22 05:43:13', '2022-04-22 05:43:13'),
(3637, '2022-04-22 00:00:00', '162.158.227.236', '2022-04-22 05:48:31', '2022-04-22 05:48:31'),
(3638, '2022-04-22 00:00:00', '172.70.218.174', '2022-04-22 05:50:26', '2022-04-22 05:50:26'),
(3639, '2022-04-22 00:00:00', '172.70.218.236', '2022-04-22 05:55:00', '2022-04-22 05:55:00'),
(3640, '2022-04-22 00:00:00', '172.70.175.69', '2022-04-22 05:55:01', '2022-04-22 05:55:01'),
(3641, '2022-04-22 00:00:00', '162.158.227.240', '2022-04-22 06:00:33', '2022-04-22 06:00:33'),
(3642, '2022-04-22 00:00:00', '172.70.218.198', '2022-04-22 06:02:09', '2022-04-22 06:02:09'),
(3643, '2022-04-22 00:00:00', '172.70.218.236', '2022-04-22 06:14:45', '2022-04-22 06:14:45'),
(3644, '2022-04-22 00:00:00', '162.158.235.43', '2022-04-22 06:16:28', '2022-04-22 06:16:28'),
(3645, '2022-04-22 00:00:00', '172.70.251.51', '2022-04-22 06:18:58', '2022-04-22 06:18:58'),
(3646, '2022-04-22 00:00:00', '172.70.250.206', '2022-04-22 06:20:56', '2022-04-22 06:20:56'),
(3647, '2022-04-22 00:00:00', '172.70.251.125', '2022-04-22 06:24:06', '2022-04-22 06:24:06'),
(3648, '2022-04-22 00:00:00', '172.70.242.72', '2022-04-22 06:26:28', '2022-04-22 06:26:28'),
(3649, '2022-04-22 00:00:00', '172.70.34.172', '2022-04-22 06:28:36', '2022-04-22 06:28:36'),
(3650, '2022-04-22 00:00:00', '172.70.135.35', '2022-04-22 07:03:56', '2022-04-22 07:03:56'),
(3651, '2022-04-22 00:00:00', '172.70.134.28', '2022-04-22 07:22:11', '2022-04-22 07:22:11'),
(3652, '2022-04-22 00:00:00', '162.158.90.86', '2022-04-22 07:50:14', '2022-04-22 07:50:14'),
(3653, '2022-04-22 00:00:00', '172.70.174.196', '2022-04-22 08:15:45', '2022-04-22 08:15:45'),
(3654, '2022-04-22 00:00:00', '162.158.227.234', '2022-04-22 08:18:15', '2022-04-22 08:18:15'),
(3655, '2022-04-22 00:00:00', '172.70.242.210', '2022-04-22 08:40:56', '2022-04-22 08:40:56'),
(3656, '2022-04-22 00:00:00', '172.70.142.42', '2022-04-22 08:51:37', '2022-04-22 08:51:37'),
(3657, '2022-04-22 00:00:00', '162.158.162.236', '2022-04-22 08:53:44', '2022-04-22 08:53:44'),
(3658, '2022-04-22 00:00:00', '172.70.250.64', '2022-04-22 08:54:39', '2022-04-22 08:54:39'),
(3659, '2022-04-22 00:00:00', '172.70.250.206', '2022-04-22 08:59:12', '2022-04-22 08:59:12'),
(3660, '2022-04-22 00:00:00', '162.158.227.224', '2022-04-22 09:00:54', '2022-04-22 09:00:54'),
(3661, '2022-04-22 00:00:00', '172.70.218.198', '2022-04-22 09:01:44', '2022-04-22 09:01:44'),
(3662, '2022-04-22 00:00:00', '172.70.251.51', '2022-04-22 09:30:12', '2022-04-22 09:30:12'),
(3663, '2022-04-22 00:00:00', '172.70.250.206', '2022-04-22 09:32:35', '2022-04-22 09:32:35'),
(3664, '2022-04-22 00:00:00', '172.70.250.64', '2022-04-22 09:33:12', '2022-04-22 09:33:12'),
(3665, '2022-04-22 00:00:00', '172.70.218.236', '2022-04-22 09:35:53', '2022-04-22 09:35:53'),
(3666, '2022-04-22 00:00:00', '162.158.235.43', '2022-04-22 09:42:11', '2022-04-22 09:42:11'),
(3667, '2022-04-22 00:00:00', '172.70.251.125', '2022-04-22 09:45:12', '2022-04-22 09:45:12'),
(3668, '2022-04-22 00:00:00', '162.158.90.58', '2022-04-22 10:02:28', '2022-04-22 10:02:28'),
(3669, '2022-04-22 00:00:00', '172.70.174.128', '2022-04-22 10:02:54', '2022-04-22 10:02:54'),
(3670, '2022-04-22 00:00:00', '172.70.251.51', '2022-04-22 10:13:12', '2022-04-22 10:13:12'),
(3671, '2022-04-22 00:00:00', '172.70.242.128', '2022-04-22 10:14:14', '2022-04-22 10:14:14'),
(3672, '2022-04-22 00:00:00', '172.70.250.206', '2022-04-22 10:21:28', '2022-04-22 10:21:28'),
(3673, '2022-04-22 00:00:00', '172.70.175.47', '2022-04-22 10:25:11', '2022-04-22 10:25:11'),
(3674, '2022-04-22 00:00:00', '172.70.189.59', '2022-04-22 10:25:39', '2022-04-22 10:25:39'),
(3675, '2022-04-22 00:00:00', '172.70.242.128', '2022-04-22 10:31:22', '2022-04-22 10:31:22'),
(3676, '2022-04-22 00:00:00', '172.70.250.206', '2022-04-22 10:35:19', '2022-04-22 10:35:19'),
(3677, '2022-04-22 00:00:00', '172.70.218.236', '2022-04-22 10:56:03', '2022-04-22 10:56:03'),
(3678, '2022-04-22 00:00:00', '172.70.35.31', '2022-04-22 10:56:27', '2022-04-22 10:56:27'),
(3679, '2022-04-22 00:00:00', '172.70.211.157', '2022-04-22 11:08:24', '2022-04-22 11:08:24'),
(3680, '2022-04-22 00:00:00', '172.71.6.48', '2022-04-22 11:20:02', '2022-04-22 11:20:02'),
(3681, '2022-04-22 00:00:00', '172.70.218.236', '2022-04-22 11:30:22', '2022-04-22 11:30:22'),
(3682, '2022-04-22 00:00:00', '172.70.218.174', '2022-04-22 11:39:47', '2022-04-22 11:39:47'),
(3683, '2022-04-22 00:00:00', '162.158.227.236', '2022-04-22 11:40:24', '2022-04-22 11:40:24'),
(3684, '2022-04-22 00:00:00', '172.68.39.216', '2022-04-22 11:40:52', '2022-04-22 11:40:52'),
(3685, '2022-04-22 00:00:00', '162.158.78.212', '2022-04-22 11:50:02', '2022-04-22 11:50:02'),
(3686, '2022-04-22 00:00:00', '172.70.251.51', '2022-04-22 12:12:13', '2022-04-22 12:12:13'),
(3687, '2022-04-22 00:00:00', '162.158.48.62', '2022-04-22 12:30:22', '2022-04-22 12:30:22'),
(3688, '2022-04-22 00:00:00', '162.158.227.234', '2022-04-22 12:35:01', '2022-04-22 12:35:01'),
(3689, '2022-04-22 00:00:00', '172.70.34.6', '2022-04-22 12:43:36', '2022-04-22 12:43:36'),
(3690, '2022-04-22 00:00:00', '172.70.242.72', '2022-04-22 13:04:37', '2022-04-22 13:04:37'),
(3691, '2022-04-22 00:00:00', '172.70.218.28', '2022-04-22 13:08:04', '2022-04-22 13:08:04'),
(3692, '2022-04-22 00:00:00', '172.70.38.138', '2022-04-22 13:17:11', '2022-04-22 13:17:11'),
(3693, '2022-04-22 00:00:00', '172.70.134.186', '2022-04-22 14:43:35', '2022-04-22 14:43:35'),
(3694, '2022-04-22 00:00:00', '172.70.134.98', '2022-04-22 15:02:49', '2022-04-22 15:02:49'),
(3695, '2022-04-22 00:00:00', '172.70.142.228', '2022-04-22 15:14:09', '2022-04-22 15:14:09'),
(3696, '2022-04-22 00:00:00', '172.70.134.64', '2022-04-22 15:22:02', '2022-04-22 15:22:02'),
(3697, '2022-04-22 00:00:00', '172.70.147.32', '2022-04-22 15:37:24', '2022-04-22 15:37:24'),
(3698, '2022-04-22 00:00:00', '162.158.78.12', '2022-04-22 15:41:16', '2022-04-22 15:41:16'),
(3699, '2022-04-22 00:00:00', '172.70.142.228', '2022-04-22 15:44:30', '2022-04-22 15:44:30'),
(3700, '2022-04-22 00:00:00', '172.70.142.224', '2022-04-22 15:51:28', '2022-04-22 15:51:28'),
(3701, '2022-04-22 00:00:00', '162.158.162.240', '2022-04-22 15:58:27', '2022-04-22 15:58:27'),
(3702, '2022-04-22 00:00:00', '172.70.174.190', '2022-04-22 16:00:30', '2022-04-22 16:00:30'),
(3703, '2022-04-22 00:00:00', '172.70.34.162', '2022-04-22 16:19:44', '2022-04-22 16:19:44'),
(3704, '2022-04-22 00:00:00', '162.158.178.178', '2022-04-22 17:01:36', '2022-04-22 17:01:36'),
(3705, '2022-04-22 00:00:00', '172.70.174.54', '2022-04-22 17:32:32', '2022-04-22 17:32:32'),
(3706, '2022-04-22 00:00:00', '172.68.10.182', '2022-04-22 17:54:01', '2022-04-22 17:54:01'),
(3707, '2022-04-22 00:00:00', '172.70.214.182', '2022-04-22 17:54:51', '2022-04-22 17:54:51'),
(3708, '2022-04-22 00:00:00', '172.70.34.110', '2022-04-22 18:26:06', '2022-04-22 18:26:06'),
(3709, '2022-04-22 00:00:00', '172.70.189.59', '2022-04-22 19:07:49', '2022-04-22 19:07:49'),
(3710, '2022-04-22 00:00:00', '162.158.162.208', '2022-04-22 19:13:13', '2022-04-22 19:13:13'),
(3711, '2022-04-22 00:00:00', '172.70.147.48', '2022-04-22 19:18:37', '2022-04-22 19:18:37'),
(3712, '2022-04-22 00:00:00', '172.70.147.32', '2022-04-22 19:25:40', '2022-04-22 19:25:40'),
(3713, '2022-04-22 00:00:00', '172.70.135.5', '2022-04-22 21:06:49', '2022-04-22 21:06:49'),
(3714, '2022-04-22 00:00:00', '172.70.175.89', '2022-04-22 22:36:49', '2022-04-22 22:36:49'),
(3715, '2022-04-23 00:00:00', '172.70.34.254', '2022-04-23 01:39:12', '2022-04-23 01:39:12'),
(3716, '2022-04-23 00:00:00', '172.70.178.106', '2022-04-23 01:43:33', '2022-04-23 01:43:33'),
(3717, '2022-04-23 00:00:00', '172.70.178.36', '2022-04-23 02:31:18', '2022-04-23 02:31:18'),
(3718, '2022-04-23 00:00:00', '162.158.50.73', '2022-04-23 02:41:36', '2022-04-23 02:41:36'),
(3719, '2022-04-23 00:00:00', '172.70.38.8', '2022-04-23 03:09:12', '2022-04-23 03:09:12'),
(3720, '2022-04-23 00:00:00', '172.70.188.150', '2022-04-23 04:30:38', '2022-04-23 04:30:38'),
(3721, '2022-04-23 00:00:00', '172.70.188.90', '2022-04-23 04:35:50', '2022-04-23 04:35:50'),
(3722, '2022-04-23 00:00:00', '172.70.174.138', '2022-04-23 04:36:49', '2022-04-23 04:36:49'),
(3723, '2022-04-23 00:00:00', '172.70.92.150', '2022-04-23 04:41:02', '2022-04-23 04:41:02'),
(3724, '2022-04-23 00:00:00', '172.70.142.32', '2022-04-23 04:46:16', '2022-04-23 04:46:16'),
(3725, '2022-04-23 00:00:00', '108.162.229.70', '2022-04-23 05:07:31', '2022-04-23 05:07:31'),
(3726, '2022-04-23 00:00:00', '172.68.10.182', '2022-04-23 06:24:55', '2022-04-23 06:24:55'),
(3727, '2022-04-23 00:00:00', '172.70.246.142', '2022-04-23 08:41:44', '2022-04-23 08:41:44'),
(3728, '2022-04-23 00:00:00', '172.70.126.34', '2022-04-23 09:36:11', '2022-04-23 09:36:11'),
(3729, '2022-04-23 00:00:00', '141.101.104.71', '2022-04-23 09:44:17', '2022-04-23 09:44:17'),
(3730, '2022-04-23 00:00:00', '141.101.105.145', '2022-04-23 09:44:19', '2022-04-23 09:44:19'),
(3731, '2022-04-23 00:00:00', '172.70.189.95', '2022-04-23 10:10:20', '2022-04-23 10:10:20'),
(3732, '2022-04-23 00:00:00', '172.70.92.156', '2022-04-23 10:30:06', '2022-04-23 10:30:06'),
(3733, '2022-04-23 00:00:00', '141.101.77.11', '2022-04-23 11:23:44', '2022-04-23 11:23:44'),
(3734, '2022-04-23 00:00:00', '141.101.104.73', '2022-04-23 11:23:45', '2022-04-23 11:23:45'),
(3735, '2022-04-23 00:00:00', '162.158.162.80', '2022-04-23 12:13:47', '2022-04-23 12:13:47'),
(3736, '2022-04-23 00:00:00', '172.70.126.202', '2022-04-23 14:28:45', '2022-04-23 14:28:45'),
(3737, '2022-04-23 00:00:00', '172.68.39.134', '2022-04-23 14:52:06', '2022-04-23 14:52:06'),
(3738, '2022-04-23 00:00:00', '172.70.92.160', '2022-04-23 15:03:39', '2022-04-23 15:03:39'),
(3739, '2022-04-23 00:00:00', '172.70.178.106', '2022-04-23 15:08:45', '2022-04-23 15:08:45'),
(3740, '2022-04-23 00:00:00', '172.70.127.39', '2022-04-23 15:48:47', '2022-04-23 15:48:47'),
(3741, '2022-04-23 00:00:00', '172.70.179.9', '2022-04-23 16:09:44', '2022-04-23 16:09:44'),
(3742, '2022-04-23 00:00:00', '162.158.170.178', '2022-04-23 16:28:03', '2022-04-23 16:28:03'),
(3743, '2022-04-23 00:00:00', '172.70.126.186', '2022-04-23 16:30:38', '2022-04-23 16:30:38'),
(3744, '2022-04-23 00:00:00', '173.245.52.195', '2022-04-23 17:02:26', '2022-04-23 17:02:26'),
(3745, '2022-04-23 00:00:00', '172.69.33.44', '2022-04-23 17:05:19', '2022-04-23 17:05:19'),
(3746, '2022-04-23 00:00:00', '172.70.131.45', '2022-04-23 17:59:30', '2022-04-23 17:59:30'),
(3747, '2022-04-23 00:00:00', '172.70.178.196', '2022-04-23 18:41:18', '2022-04-23 18:41:18'),
(3748, '2022-04-23 00:00:00', '172.70.147.42', '2022-04-23 19:24:05', '2022-04-23 19:24:05'),
(3749, '2022-04-23 00:00:00', '172.71.10.88', '2022-04-23 20:11:27', '2022-04-23 20:11:27'),
(3750, '2022-04-23 00:00:00', '162.158.170.60', '2022-04-23 20:55:06', '2022-04-23 20:55:06'),
(3751, '2022-04-23 00:00:00', '172.70.90.172', '2022-04-23 23:19:40', '2022-04-23 23:19:40'),
(3752, '2022-04-23 00:00:00', '162.158.170.188', '2022-04-23 23:26:56', '2022-04-23 23:26:56'),
(3753, '2022-04-23 00:00:00', '162.158.106.212', '2022-04-23 23:28:17', '2022-04-23 23:28:17'),
(3754, '2022-04-23 00:00:00', '162.158.92.200', '2022-04-23 23:38:46', '2022-04-23 23:38:46'),
(3755, '2022-04-24 00:00:00', '172.70.130.186', '2022-04-24 00:14:16', '2022-04-24 00:14:16'),
(3756, '2022-04-24 00:00:00', '172.70.131.33', '2022-04-24 00:42:33', '2022-04-24 00:42:33'),
(3757, '2022-04-24 00:00:00', '108.162.246.105', '2022-04-24 01:07:23', '2022-04-24 01:07:23'),
(3758, '2022-04-24 00:00:00', '172.70.126.22', '2022-04-24 01:27:34', '2022-04-24 01:27:34'),
(3759, '2022-04-24 00:00:00', '162.158.163.201', '2022-04-24 01:30:45', '2022-04-24 01:30:45'),
(3760, '2022-04-24 00:00:00', '172.70.130.254', '2022-04-24 02:11:17', '2022-04-24 02:11:17'),
(3761, '2022-04-24 00:00:00', '172.70.142.228', '2022-04-24 02:24:08', '2022-04-24 02:24:08'),
(3762, '2022-04-24 00:00:00', '172.70.126.44', '2022-04-24 02:32:34', '2022-04-24 02:32:34'),
(3763, '2022-04-24 00:00:00', '172.70.147.48', '2022-04-24 02:47:56', '2022-04-24 02:47:56'),
(3764, '2022-04-24 00:00:00', '172.70.189.95', '2022-04-24 03:00:52', '2022-04-24 03:00:52'),
(3765, '2022-04-24 00:00:00', '172.70.131.45', '2022-04-24 03:11:17', '2022-04-24 03:11:17'),
(3766, '2022-04-24 00:00:00', '172.70.126.90', '2022-04-24 03:41:17', '2022-04-24 03:41:17'),
(3767, '2022-04-24 00:00:00', '172.70.131.43', '2022-04-24 04:10:22', '2022-04-24 04:10:22'),
(3768, '2022-04-24 00:00:00', '172.70.131.153', '2022-04-24 04:41:18', '2022-04-24 04:41:18'),
(3769, '2022-04-24 00:00:00', '172.70.92.150', '2022-04-24 04:58:39', '2022-04-24 04:58:39'),
(3770, '2022-04-24 00:00:00', '108.162.221.56', '2022-04-24 07:19:29', '2022-04-24 07:19:29'),
(3771, '2022-04-24 00:00:00', '172.68.254.28', '2022-04-24 07:51:19', '2022-04-24 07:51:19'),
(3772, '2022-04-24 00:00:00', '172.70.211.157', '2022-04-24 08:21:44', '2022-04-24 08:21:44'),
(3773, '2022-04-24 00:00:00', '172.70.206.154', '2022-04-24 08:21:45', '2022-04-24 08:21:45'),
(3774, '2022-04-24 00:00:00', '162.158.107.127', '2022-04-24 11:33:37', '2022-04-24 11:33:37'),
(3775, '2022-04-24 00:00:00', '172.70.130.156', '2022-04-24 12:02:04', '2022-04-24 12:02:04'),
(3776, '2022-04-24 00:00:00', '172.70.189.59', '2022-04-24 12:55:48', '2022-04-24 12:55:48'),
(3777, '2022-04-24 00:00:00', '172.70.92.150', '2022-04-24 13:15:04', '2022-04-24 13:15:04'),
(3778, '2022-04-24 00:00:00', '172.70.135.93', '2022-04-24 13:43:30', '2022-04-24 13:43:30'),
(3779, '2022-04-24 00:00:00', '172.70.189.117', '2022-04-24 13:46:49', '2022-04-24 13:46:49'),
(3780, '2022-04-24 00:00:00', '172.70.162.12', '2022-04-24 15:11:43', '2022-04-24 15:11:43'),
(3781, '2022-04-24 00:00:00', '172.70.175.27', '2022-04-24 15:13:30', '2022-04-24 15:13:30'),
(3782, '2022-04-24 00:00:00', '172.70.34.214', '2022-04-24 15:43:29', '2022-04-24 15:43:29'),
(3783, '2022-04-24 00:00:00', '172.70.135.51', '2022-04-24 16:05:59', '2022-04-24 16:05:59'),
(3784, '2022-04-24 00:00:00', '172.70.175.89', '2022-04-24 16:28:29', '2022-04-24 16:28:29'),
(3785, '2022-04-24 00:00:00', '172.70.35.51', '2022-04-24 16:50:59', '2022-04-24 16:50:59'),
(3786, '2022-04-24 00:00:00', '162.158.178.8', '2022-04-24 17:17:59', '2022-04-24 17:17:59'),
(3787, '2022-04-24 00:00:00', '172.70.142.218', '2022-04-24 17:24:41', '2022-04-24 17:24:41'),
(3788, '2022-04-24 00:00:00', '172.70.147.42', '2022-04-24 17:35:16', '2022-04-24 17:35:16'),
(3789, '2022-04-24 00:00:00', '172.70.34.6', '2022-04-24 17:35:59', '2022-04-24 17:35:59'),
(3790, '2022-04-24 00:00:00', '172.70.142.34', '2022-04-24 17:45:43', '2022-04-24 17:45:43'),
(3791, '2022-04-24 00:00:00', '162.158.163.217', '2022-04-24 18:06:58', '2022-04-24 18:06:58'),
(3792, '2022-04-24 00:00:00', '172.70.147.42', '2022-04-24 18:17:33', '2022-04-24 18:17:33'),
(3793, '2022-04-24 00:00:00', '172.70.189.59', '2022-04-24 18:28:14', '2022-04-24 18:28:14'),
(3794, '2022-04-24 00:00:00', '162.158.163.177', '2022-04-24 18:49:27', '2022-04-24 18:49:27'),
(3795, '2022-04-24 00:00:00', '172.70.110.202', '2022-04-24 19:10:40', '2022-04-24 19:10:40'),
(3796, '2022-04-24 00:00:00', '172.70.115.15', '2022-04-24 19:10:47', '2022-04-24 19:10:47'),
(3797, '2022-04-24 00:00:00', '172.70.110.72', '2022-04-24 19:10:47', '2022-04-24 19:10:47'),
(3798, '2022-04-24 00:00:00', '162.158.62.73', '2022-04-24 19:10:51', '2022-04-24 19:10:51');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(3799, '2022-04-24 00:00:00', '172.70.114.50', '2022-04-24 19:11:04', '2022-04-24 19:11:04'),
(3800, '2022-04-24 00:00:00', '172.70.110.220', '2022-04-24 19:11:08', '2022-04-24 19:11:08'),
(3801, '2022-04-24 00:00:00', '162.158.162.96', '2022-04-24 19:11:09', '2022-04-24 19:11:09'),
(3802, '2022-04-24 00:00:00', '172.70.230.136', '2022-04-24 19:11:14', '2022-04-24 19:11:14'),
(3803, '2022-04-24 00:00:00', '172.70.114.166', '2022-04-24 19:11:15', '2022-04-24 19:11:15'),
(3804, '2022-04-24 00:00:00', '172.70.188.36', '2022-04-24 19:37:31', '2022-04-24 19:37:31'),
(3805, '2022-04-24 00:00:00', '172.70.175.213', '2022-04-24 20:04:55', '2022-04-24 20:04:55'),
(3806, '2022-04-24 00:00:00', '172.70.174.12', '2022-04-24 20:04:55', '2022-04-24 20:04:55'),
(3807, '2022-04-24 00:00:00', '172.70.34.188', '2022-04-24 20:05:05', '2022-04-24 20:05:05'),
(3808, '2022-04-24 00:00:00', '162.158.78.24', '2022-04-24 22:05:25', '2022-04-24 22:05:25'),
(3809, '2022-04-24 00:00:00', '162.158.78.154', '2022-04-24 22:05:26', '2022-04-24 22:05:26'),
(3810, '2022-04-24 00:00:00', '172.70.34.118', '2022-04-24 22:05:29', '2022-04-24 22:05:29'),
(3811, '2022-04-24 00:00:00', '172.70.35.61', '2022-04-24 22:05:29', '2022-04-24 22:05:29'),
(3812, '2022-04-24 00:00:00', '172.70.34.40', '2022-04-24 22:05:29', '2022-04-24 22:05:29'),
(3813, '2022-04-25 00:00:00', '172.70.188.150', '2022-04-25 00:01:04', '2022-04-25 00:01:04'),
(3814, '2022-04-25 00:00:00', '162.158.170.60', '2022-04-25 01:07:58', '2022-04-25 01:07:58'),
(3815, '2022-04-25 00:00:00', '172.70.142.224', '2022-04-25 01:30:57', '2022-04-25 01:30:57'),
(3816, '2022-04-25 00:00:00', '172.70.135.15', '2022-04-25 01:41:34', '2022-04-25 01:41:34'),
(3817, '2022-04-25 00:00:00', '172.70.142.228', '2022-04-25 01:42:29', '2022-04-25 01:42:29'),
(3818, '2022-04-25 00:00:00', '172.69.70.240', '2022-04-25 01:51:30', '2022-04-25 01:51:30'),
(3819, '2022-04-25 00:00:00', '172.69.70.22', '2022-04-25 01:51:31', '2022-04-25 01:51:31'),
(3820, '2022-04-25 00:00:00', '108.162.221.56', '2022-04-25 01:51:33', '2022-04-25 01:51:33'),
(3821, '2022-04-25 00:00:00', '172.70.130.146', '2022-04-25 01:51:37', '2022-04-25 01:51:37'),
(3822, '2022-04-25 00:00:00', '172.70.130.156', '2022-04-25 01:51:39', '2022-04-25 01:51:39'),
(3823, '2022-04-25 00:00:00', '172.70.131.167', '2022-04-25 01:51:43', '2022-04-25 01:51:43'),
(3824, '2022-04-25 00:00:00', '172.70.178.140', '2022-04-25 01:51:45', '2022-04-25 01:51:45'),
(3825, '2022-04-25 00:00:00', '172.70.178.104', '2022-04-25 01:51:46', '2022-04-25 01:51:46'),
(3826, '2022-04-25 00:00:00', '108.162.216.208', '2022-04-25 01:51:48', '2022-04-25 01:51:48'),
(3827, '2022-04-25 00:00:00', '162.158.170.58', '2022-04-25 01:53:29', '2022-04-25 01:53:29'),
(3828, '2022-04-25 00:00:00', '172.70.174.142', '2022-04-25 02:26:34', '2022-04-25 02:26:34'),
(3829, '2022-04-25 00:00:00', '172.70.142.48', '2022-04-25 02:32:14', '2022-04-25 02:32:14'),
(3830, '2022-04-25 00:00:00', '172.70.34.50', '2022-04-25 03:11:34', '2022-04-25 03:11:34'),
(3831, '2022-04-25 00:00:00', '172.70.250.206', '2022-04-25 04:04:15', '2022-04-25 04:04:15'),
(3832, '2022-04-25 00:00:00', '172.70.147.48', '2022-04-25 05:42:14', '2022-04-25 05:42:14'),
(3833, '2022-04-25 00:00:00', '172.70.142.218', '2022-04-25 05:52:57', '2022-04-25 05:52:57'),
(3834, '2022-04-25 00:00:00', '162.158.162.156', '2022-04-25 06:18:36', '2022-04-25 06:18:36'),
(3835, '2022-04-25 00:00:00', '172.68.39.216', '2022-04-25 06:25:17', '2022-04-25 06:25:17'),
(3836, '2022-04-25 00:00:00', '162.158.170.228', '2022-04-25 06:41:38', '2022-04-25 06:41:38'),
(3837, '2022-04-25 00:00:00', '162.158.48.128', '2022-04-25 06:43:27', '2022-04-25 06:43:27'),
(3838, '2022-04-25 00:00:00', '172.70.175.9', '2022-04-25 06:44:40', '2022-04-25 06:44:40'),
(3839, '2022-04-25 00:00:00', '172.70.135.201', '2022-04-25 06:44:41', '2022-04-25 06:44:41'),
(3840, '2022-04-25 00:00:00', '172.70.187.138', '2022-04-25 06:51:12', '2022-04-25 06:51:12'),
(3841, '2022-04-25 00:00:00', '162.158.163.233', '2022-04-25 06:51:33', '2022-04-25 06:51:33'),
(3842, '2022-04-25 00:00:00', '162.158.159.122', '2022-04-25 06:52:27', '2022-04-25 06:52:27'),
(3843, '2022-04-25 00:00:00', '172.70.85.40', '2022-04-25 06:57:57', '2022-04-25 06:57:57'),
(3844, '2022-04-25 00:00:00', '172.70.90.150', '2022-04-25 06:59:51', '2022-04-25 06:59:51'),
(3845, '2022-04-25 00:00:00', '172.70.85.34', '2022-04-25 07:06:29', '2022-04-25 07:06:29'),
(3846, '2022-04-25 00:00:00', '172.70.38.224', '2022-04-25 07:07:10', '2022-04-25 07:07:10'),
(3847, '2022-04-25 00:00:00', '172.70.90.150', '2022-04-25 07:11:58', '2022-04-25 07:11:58'),
(3848, '2022-04-25 00:00:00', '162.158.163.203', '2022-04-25 07:19:00', '2022-04-25 07:19:00'),
(3849, '2022-04-25 00:00:00', '141.101.98.240', '2022-04-25 07:24:10', '2022-04-25 07:24:10'),
(3850, '2022-04-25 00:00:00', '172.69.70.240', '2022-04-25 07:25:13', '2022-04-25 07:25:13'),
(3851, '2022-04-25 00:00:00', '172.70.85.154', '2022-04-25 07:25:39', '2022-04-25 07:25:39'),
(3852, '2022-04-25 00:00:00', '172.70.142.34', '2022-04-25 07:29:40', '2022-04-25 07:29:40'),
(3853, '2022-04-25 00:00:00', '172.70.85.40', '2022-04-25 07:38:24', '2022-04-25 07:38:24'),
(3854, '2022-04-25 00:00:00', '162.158.178.47', '2022-04-25 07:40:13', '2022-04-25 07:40:13'),
(3855, '2022-04-25 00:00:00', '162.158.163.53', '2022-04-25 07:40:22', '2022-04-25 07:40:22'),
(3856, '2022-04-25 00:00:00', '141.101.99.239', '2022-04-25 07:41:29', '2022-04-25 07:41:29'),
(3857, '2022-04-25 00:00:00', '141.101.68.198', '2022-04-25 08:10:05', '2022-04-25 08:10:05'),
(3858, '2022-04-25 00:00:00', '172.70.142.34', '2022-04-25 08:12:21', '2022-04-25 08:12:21'),
(3859, '2022-04-25 00:00:00', '172.70.188.36', '2022-04-25 08:23:02', '2022-04-25 08:23:02'),
(3860, '2022-04-25 00:00:00', '172.70.142.218', '2022-04-25 08:33:42', '2022-04-25 08:33:42'),
(3861, '2022-04-25 00:00:00', '172.69.33.60', '2022-04-25 08:55:35', '2022-04-25 08:55:35'),
(3862, '2022-04-25 00:00:00', '172.70.242.210', '2022-04-25 09:03:58', '2022-04-25 09:03:58'),
(3863, '2022-04-25 00:00:00', '162.158.170.60', '2022-04-25 09:20:12', '2022-04-25 09:20:12'),
(3864, '2022-04-25 00:00:00', '172.70.162.12', '2022-04-25 09:50:07', '2022-04-25 09:50:07'),
(3865, '2022-04-25 00:00:00', '172.70.85.34', '2022-04-25 09:52:43', '2022-04-25 09:52:43'),
(3866, '2022-04-25 00:00:00', '172.70.142.228', '2022-04-25 10:14:00', '2022-04-25 10:14:00'),
(3867, '2022-04-25 00:00:00', '141.101.99.253', '2022-04-25 10:24:09', '2022-04-25 10:24:09'),
(3868, '2022-04-25 00:00:00', '172.68.11.49', '2022-04-25 10:34:49', '2022-04-25 10:34:49'),
(3869, '2022-04-25 00:00:00', '172.70.162.240', '2022-04-25 10:38:06', '2022-04-25 10:38:06'),
(3870, '2022-04-25 00:00:00', '172.70.90.150', '2022-04-25 10:39:40', '2022-04-25 10:39:40'),
(3871, '2022-04-25 00:00:00', '172.70.162.22', '2022-04-25 10:43:58', '2022-04-25 10:43:58'),
(3872, '2022-04-25 00:00:00', '172.70.85.154', '2022-04-25 10:49:34', '2022-04-25 10:49:34'),
(3873, '2022-04-25 00:00:00', '162.158.159.110', '2022-04-25 10:51:39', '2022-04-25 10:51:39'),
(3874, '2022-04-25 00:00:00', '162.158.163.203', '2022-04-25 10:51:55', '2022-04-25 10:51:55'),
(3875, '2022-04-25 00:00:00', '172.70.90.212', '2022-04-25 10:53:05', '2022-04-25 10:53:05'),
(3876, '2022-04-25 00:00:00', '172.70.90.6', '2022-04-25 10:56:35', '2022-04-25 10:56:35'),
(3877, '2022-04-25 00:00:00', '172.70.130.206', '2022-04-25 11:05:59', '2022-04-25 11:05:59'),
(3878, '2022-04-25 00:00:00', '162.158.159.88', '2022-04-25 11:15:07', '2022-04-25 11:15:07'),
(3879, '2022-04-25 00:00:00', '172.70.85.34', '2022-04-25 11:15:39', '2022-04-25 11:15:39'),
(3880, '2022-04-25 00:00:00', '141.101.99.17', '2022-04-25 11:16:26', '2022-04-25 11:16:26'),
(3881, '2022-04-25 00:00:00', '172.70.85.150', '2022-04-25 11:19:55', '2022-04-25 11:19:55'),
(3882, '2022-04-25 00:00:00', '172.70.90.150', '2022-04-25 11:35:08', '2022-04-25 11:35:08'),
(3883, '2022-04-25 00:00:00', '141.101.99.253', '2022-04-25 13:13:11', '2022-04-25 13:13:11'),
(3884, '2022-04-25 00:00:00', '172.70.162.240', '2022-04-25 13:14:58', '2022-04-25 13:14:58'),
(3885, '2022-04-25 00:00:00', '172.70.85.150', '2022-04-25 13:36:01', '2022-04-25 13:36:01'),
(3886, '2022-04-25 00:00:00', '172.69.68.179', '2022-04-25 13:54:44', '2022-04-25 13:54:44'),
(3887, '2022-04-25 00:00:00', '172.70.135.15', '2022-04-25 14:19:46', '2022-04-25 14:19:46'),
(3888, '2022-04-25 00:00:00', '172.70.85.40', '2022-04-25 14:19:58', '2022-04-25 14:19:58'),
(3889, '2022-04-25 00:00:00', '172.70.175.213', '2022-04-25 14:20:04', '2022-04-25 14:20:04'),
(3890, '2022-04-25 00:00:00', '172.68.253.249', '2022-04-25 14:20:46', '2022-04-25 14:20:46'),
(3891, '2022-04-25 00:00:00', '172.68.39.150', '2022-04-25 14:24:09', '2022-04-25 14:24:09'),
(3892, '2022-04-25 00:00:00', '172.70.131.63', '2022-04-25 14:39:43', '2022-04-25 14:39:43'),
(3893, '2022-04-25 00:00:00', '172.68.39.150', '2022-04-25 14:43:54', '2022-04-25 14:43:54'),
(3894, '2022-04-25 00:00:00', '108.162.221.32', '2022-04-25 15:02:14', '2022-04-25 15:02:14'),
(3895, '2022-04-25 00:00:00', '141.101.68.198', '2022-04-25 15:06:03', '2022-04-25 15:06:03'),
(3896, '2022-04-25 00:00:00', '108.162.221.120', '2022-04-25 15:46:27', '2022-04-25 15:46:27'),
(3897, '2022-04-25 00:00:00', '162.158.170.228', '2022-04-25 16:23:54', '2022-04-25 16:23:54'),
(3898, '2022-04-25 00:00:00', '172.70.189.59', '2022-04-25 17:03:34', '2022-04-25 17:03:34'),
(3899, '2022-04-25 00:00:00', '172.69.70.116', '2022-04-25 18:18:30', '2022-04-25 18:18:30'),
(3900, '2022-04-25 00:00:00', '162.158.162.250', '2022-04-25 18:25:17', '2022-04-25 18:25:17'),
(3901, '2022-04-25 00:00:00', '172.70.126.38', '2022-04-25 19:03:30', '2022-04-25 19:03:30'),
(3902, '2022-04-25 00:00:00', '172.69.70.110', '2022-04-25 20:33:30', '2022-04-25 20:33:30'),
(3903, '2022-04-25 00:00:00', '172.68.254.174', '2022-04-25 20:47:25', '2022-04-25 20:47:25'),
(3904, '2022-04-25 00:00:00', '108.162.246.223', '2022-04-25 21:13:18', '2022-04-25 21:13:18'),
(3905, '2022-04-25 00:00:00', '172.69.69.189', '2022-04-25 21:18:30', '2022-04-25 21:18:30'),
(3906, '2022-04-25 00:00:00', '108.162.216.208', '2022-04-25 22:03:30', '2022-04-25 22:03:30'),
(3907, '2022-04-25 00:00:00', '108.162.245.204', '2022-04-25 22:28:33', '2022-04-25 22:28:33'),
(3908, '2022-04-25 00:00:00', '108.162.246.105', '2022-04-25 22:35:59', '2022-04-25 22:35:59'),
(3909, '2022-04-25 00:00:00', '108.162.216.178', '2022-04-25 22:48:30', '2022-04-25 22:48:30'),
(3910, '2022-04-25 00:00:00', '172.70.130.190', '2022-04-25 23:33:30', '2022-04-25 23:33:30'),
(3911, '2022-04-26 00:00:00', '172.71.10.144', '2022-04-26 00:00:11', '2022-04-26 00:00:11'),
(3912, '2022-04-26 00:00:00', '108.162.221.160', '2022-04-26 02:50:30', '2022-04-26 02:50:30'),
(3913, '2022-04-26 00:00:00', '172.69.70.50', '2022-04-26 03:14:58', '2022-04-26 03:14:58'),
(3914, '2022-04-26 00:00:00', '172.69.69.103', '2022-04-26 03:30:14', '2022-04-26 03:30:14'),
(3915, '2022-04-26 00:00:00', '172.70.35.11', '2022-04-26 03:35:39', '2022-04-26 03:35:39'),
(3916, '2022-04-26 00:00:00', '172.69.68.26', '2022-04-26 03:54:42', '2022-04-26 03:54:42'),
(3917, '2022-04-26 00:00:00', '162.158.222.118', '2022-04-26 03:57:13', '2022-04-26 03:57:13'),
(3918, '2022-04-26 00:00:00', '172.70.130.186', '2022-04-26 05:37:12', '2022-04-26 05:37:12'),
(3919, '2022-04-26 00:00:00', '162.158.163.225', '2022-04-26 06:19:19', '2022-04-26 06:19:19'),
(3920, '2022-04-26 00:00:00', '162.158.162.70', '2022-04-26 06:25:42', '2022-04-26 06:25:42'),
(3921, '2022-04-26 00:00:00', '172.69.69.222', '2022-04-26 06:30:45', '2022-04-26 06:30:45'),
(3922, '2022-04-26 00:00:00', '162.158.170.60', '2022-04-26 06:38:44', '2022-04-26 06:38:44'),
(3923, '2022-04-26 00:00:00', '162.158.170.126', '2022-04-26 06:45:29', '2022-04-26 06:45:29'),
(3924, '2022-04-26 00:00:00', '172.70.189.95', '2022-04-26 06:52:11', '2022-04-26 06:52:11'),
(3925, '2022-04-26 00:00:00', '172.70.147.48', '2022-04-26 06:59:05', '2022-04-26 06:59:05'),
(3926, '2022-04-26 00:00:00', '172.70.142.42', '2022-04-26 07:12:59', '2022-04-26 07:12:59'),
(3927, '2022-04-26 00:00:00', '162.158.162.156', '2022-04-26 07:41:30', '2022-04-26 07:41:30'),
(3928, '2022-04-26 00:00:00', '162.158.163.175', '2022-04-26 07:48:56', '2022-04-26 07:48:56'),
(3929, '2022-04-26 00:00:00', '172.70.147.32', '2022-04-26 07:56:07', '2022-04-26 07:56:07'),
(3930, '2022-04-26 00:00:00', '172.70.142.34', '2022-04-26 08:02:42', '2022-04-26 08:02:42'),
(3931, '2022-04-26 00:00:00', '162.158.162.168', '2022-04-26 08:09:05', '2022-04-26 08:09:05'),
(3932, '2022-04-26 00:00:00', '162.158.38.12', '2022-04-26 08:54:23', '2022-04-26 08:54:23'),
(3933, '2022-04-26 00:00:00', '172.70.142.218', '2022-04-26 09:17:17', '2022-04-26 09:17:17'),
(3934, '2022-04-26 00:00:00', '172.70.142.224', '2022-04-26 09:30:09', '2022-04-26 09:30:09'),
(3935, '2022-04-26 00:00:00', '172.70.142.224', '2022-04-26 09:43:04', '2022-04-26 09:43:04'),
(3936, '2022-04-26 00:00:00', '172.70.211.157', '2022-04-26 09:44:17', '2022-04-26 09:44:17'),
(3937, '2022-04-26 00:00:00', '172.69.69.159', '2022-04-26 09:47:05', '2022-04-26 09:47:05'),
(3938, '2022-04-26 00:00:00', '172.70.147.48', '2022-04-26 09:49:35', '2022-04-26 09:49:35'),
(3939, '2022-04-26 00:00:00', '108.162.216.142', '2022-04-26 10:02:05', '2022-04-26 10:02:05'),
(3940, '2022-04-26 00:00:00', '172.70.189.59', '2022-04-26 10:02:46', '2022-04-26 10:02:46'),
(3941, '2022-04-26 00:00:00', '172.70.142.224', '2022-04-26 10:09:20', '2022-04-26 10:09:20'),
(3942, '2022-04-26 00:00:00', '162.158.163.181', '2022-04-26 10:16:15', '2022-04-26 10:16:15'),
(3943, '2022-04-26 00:00:00', '172.69.68.40', '2022-04-26 10:17:05', '2022-04-26 10:17:05'),
(3944, '2022-04-26 00:00:00', '172.69.70.230', '2022-04-26 10:32:05', '2022-04-26 10:32:05'),
(3945, '2022-04-26 00:00:00', '172.69.69.17', '2022-04-26 10:47:05', '2022-04-26 10:47:05'),
(3946, '2022-04-26 00:00:00', '172.70.38.64', '2022-04-26 11:32:05', '2022-04-26 11:32:05'),
(3947, '2022-04-26 00:00:00', '162.158.79.47', '2022-04-26 12:17:05', '2022-04-26 12:17:05'),
(3948, '2022-04-26 00:00:00', '172.68.26.118', '2022-04-26 12:18:09', '2022-04-26 12:18:09'),
(3949, '2022-04-26 00:00:00', '172.70.174.58', '2022-04-26 13:02:05', '2022-04-26 13:02:05'),
(3950, '2022-04-26 00:00:00', '172.70.174.190', '2022-04-26 13:47:06', '2022-04-26 13:47:06'),
(3951, '2022-04-26 00:00:00', '172.70.38.26', '2022-04-26 14:07:04', '2022-04-26 14:07:04'),
(3952, '2022-04-26 00:00:00', '172.70.175.47', '2022-04-26 15:07:18', '2022-04-26 15:07:18'),
(3953, '2022-04-26 00:00:00', '172.70.189.117', '2022-04-26 15:34:32', '2022-04-26 15:34:32'),
(3954, '2022-04-26 00:00:00', '162.158.170.88', '2022-04-26 16:01:05', '2022-04-26 16:01:05'),
(3955, '2022-04-26 00:00:00', '172.70.175.173', '2022-04-26 16:02:05', '2022-04-26 16:02:05'),
(3956, '2022-04-26 00:00:00', '172.70.189.117', '2022-04-26 16:18:01', '2022-04-26 16:18:01'),
(3957, '2022-04-26 00:00:00', '172.70.142.34', '2022-04-26 16:33:29', '2022-04-26 16:33:29'),
(3958, '2022-04-26 00:00:00', '162.158.170.78', '2022-04-26 17:23:42', '2022-04-26 17:23:42'),
(3959, '2022-04-26 00:00:00', '172.70.92.160', '2022-04-26 17:39:24', '2022-04-26 17:39:24'),
(3960, '2022-04-26 00:00:00', '172.70.147.48', '2022-04-26 17:47:15', '2022-04-26 17:47:15'),
(3961, '2022-04-26 00:00:00', '172.70.92.160', '2022-04-26 17:55:11', '2022-04-26 17:55:11'),
(3962, '2022-04-26 00:00:00', '172.70.147.48', '2022-04-26 18:18:54', '2022-04-26 18:18:54'),
(3963, '2022-04-26 00:00:00', '172.70.188.192', '2022-04-26 18:34:47', '2022-04-26 18:34:47'),
(3964, '2022-04-26 00:00:00', '172.70.175.11', '2022-04-26 19:02:05', '2022-04-26 19:02:05'),
(3965, '2022-04-26 00:00:00', '172.70.142.218', '2022-04-26 19:06:31', '2022-04-26 19:06:31'),
(3966, '2022-04-26 00:00:00', '172.70.147.34', '2022-04-26 19:30:44', '2022-04-26 19:30:44'),
(3967, '2022-04-26 00:00:00', '162.158.162.44', '2022-04-26 19:38:41', '2022-04-26 19:38:41'),
(3968, '2022-04-26 00:00:00', '172.70.178.104', '2022-04-26 20:24:10', '2022-04-26 20:24:10'),
(3969, '2022-04-26 00:00:00', '172.70.35.51', '2022-04-26 20:32:05', '2022-04-26 20:32:05'),
(3970, '2022-04-26 00:00:00', '172.68.238.56', '2022-04-26 21:13:49', '2022-04-26 21:13:49'),
(3971, '2022-04-26 00:00:00', '172.70.175.3', '2022-04-26 22:02:05', '2022-04-26 22:02:05'),
(3972, '2022-04-26 00:00:00', '172.70.35.39', '2022-04-26 22:16:05', '2022-04-26 22:16:05'),
(3973, '2022-04-26 00:00:00', '162.158.163.229', '2022-04-26 23:04:14', '2022-04-26 23:04:14'),
(3974, '2022-04-27 00:00:00', '172.70.188.90', '2022-04-27 01:07:09', '2022-04-27 01:07:09'),
(3975, '2022-04-27 00:00:00', '162.158.163.21', '2022-04-27 01:14:46', '2022-04-27 01:14:46'),
(3976, '2022-04-27 00:00:00', '172.70.131.167', '2022-04-27 01:16:06', '2022-04-27 01:16:06'),
(3977, '2022-04-27 00:00:00', '172.69.71.182', '2022-04-27 01:17:03', '2022-04-27 01:17:03'),
(3978, '2022-04-27 00:00:00', '162.158.163.155', '2022-04-27 01:22:29', '2022-04-27 01:22:29'),
(3979, '2022-04-27 00:00:00', '172.70.147.32', '2022-04-27 01:37:33', '2022-04-27 01:37:33'),
(3980, '2022-04-27 00:00:00', '172.70.34.10', '2022-04-27 01:51:42', '2022-04-27 01:51:42'),
(3981, '2022-04-27 00:00:00', '172.70.147.42', '2022-04-27 01:52:08', '2022-04-27 01:52:08'),
(3982, '2022-04-27 00:00:00', '172.70.135.141', '2022-04-27 02:14:12', '2022-04-27 02:14:12'),
(3983, '2022-04-27 00:00:00', '172.70.175.9', '2022-04-27 02:25:27', '2022-04-27 02:25:27'),
(3984, '2022-04-27 00:00:00', '172.70.135.191', '2022-04-27 02:36:42', '2022-04-27 02:36:42'),
(3985, '2022-04-27 00:00:00', '162.158.79.103', '2022-04-27 02:59:12', '2022-04-27 02:59:12'),
(3986, '2022-04-27 00:00:00', '162.158.222.148', '2022-04-27 03:56:03', '2022-04-27 03:56:03'),
(3987, '2022-04-27 00:00:00', '172.70.174.128', '2022-04-27 04:21:41', '2022-04-27 04:21:41'),
(3988, '2022-04-27 00:00:00', '172.70.175.27', '2022-04-27 04:51:42', '2022-04-27 04:51:42'),
(3989, '2022-04-27 00:00:00', '172.70.135.225', '2022-04-27 05:21:41', '2022-04-27 05:21:41'),
(3990, '2022-04-27 00:00:00', '172.70.142.218', '2022-04-27 06:29:32', '2022-04-27 06:29:32'),
(3991, '2022-04-27 00:00:00', '162.158.171.9', '2022-04-27 06:47:15', '2022-04-27 06:47:15'),
(3992, '2022-04-27 00:00:00', '172.70.92.156', '2022-04-27 07:13:42', '2022-04-27 07:13:42'),
(3993, '2022-04-27 00:00:00', '172.70.147.42', '2022-04-27 07:21:48', '2022-04-27 07:21:48'),
(3994, '2022-04-27 00:00:00', '172.70.142.228', '2022-04-27 07:37:31', '2022-04-27 07:37:31'),
(3995, '2022-04-27 00:00:00', '172.70.250.186', '2022-04-27 08:20:57', '2022-04-27 08:20:57'),
(3996, '2022-04-27 00:00:00', '172.68.50.76', '2022-04-27 08:26:35', '2022-04-27 08:26:35'),
(3997, '2022-04-27 00:00:00', '172.70.162.12', '2022-04-27 09:11:49', '2022-04-27 09:11:49'),
(3998, '2022-04-27 00:00:00', '108.162.245.204', '2022-04-27 09:15:01', '2022-04-27 09:15:01'),
(3999, '2022-04-27 00:00:00', '172.70.39.141', '2022-04-27 09:59:54', '2022-04-27 09:59:54'),
(4000, '2022-04-27 00:00:00', '172.70.126.46', '2022-04-27 10:59:32', '2022-04-27 10:59:32'),
(4001, '2022-04-27 00:00:00', '172.70.135.201', '2022-04-27 11:05:54', '2022-04-27 11:05:54'),
(4002, '2022-04-27 00:00:00', '162.158.106.212', '2022-04-27 11:17:36', '2022-04-27 11:17:36'),
(4003, '2022-04-27 00:00:00', '172.70.174.154', '2022-04-27 11:50:54', '2022-04-27 11:50:54'),
(4004, '2022-04-27 00:00:00', '162.158.170.254', '2022-04-27 12:45:33', '2022-04-27 12:45:33'),
(4005, '2022-04-27 00:00:00', '172.70.142.42', '2022-04-27 13:12:38', '2022-04-27 13:12:38'),
(4006, '2022-04-27 00:00:00', '162.158.170.254', '2022-04-27 13:21:18', '2022-04-27 13:21:18'),
(4007, '2022-04-27 00:00:00', '162.158.162.112', '2022-04-27 13:30:06', '2022-04-27 13:30:06'),
(4008, '2022-04-27 00:00:00', '172.70.147.48', '2022-04-27 13:44:20', '2022-04-27 13:44:20'),
(4009, '2022-04-27 00:00:00', '172.70.39.7', '2022-04-27 14:22:54', '2022-04-27 14:22:54'),
(4010, '2022-04-27 00:00:00', '172.70.189.95', '2022-04-27 14:23:01', '2022-04-27 14:23:01'),
(4011, '2022-04-27 00:00:00', '172.70.135.81', '2022-04-27 14:31:20', '2022-04-27 14:31:20'),
(4012, '2022-04-27 00:00:00', '162.158.163.155', '2022-04-27 15:36:04', '2022-04-27 15:36:04'),
(4013, '2022-04-27 00:00:00', '172.70.92.156', '2022-04-27 19:32:20', '2022-04-27 19:32:20'),
(4014, '2022-04-27 00:00:00', '172.70.135.57', '2022-04-27 20:17:59', '2022-04-27 20:17:59'),
(4015, '2022-04-27 00:00:00', '162.158.170.214', '2022-04-27 20:18:29', '2022-04-27 20:18:29'),
(4016, '2022-04-27 00:00:00', '162.158.163.53', '2022-04-27 20:34:51', '2022-04-27 20:34:51'),
(4017, '2022-04-27 00:00:00', '172.70.147.32', '2022-04-27 21:20:23', '2022-04-27 21:20:23'),
(4018, '2022-04-27 00:00:00', '172.70.135.35', '2022-04-27 21:25:26', '2022-04-27 21:25:26'),
(4019, '2022-04-27 00:00:00', '172.70.174.140', '2022-04-27 23:16:34', '2022-04-27 23:16:34'),
(4020, '2022-04-28 00:00:00', '162.158.79.35', '2022-04-28 00:02:56', '2022-04-28 00:02:56'),
(4021, '2022-04-28 00:00:00', '172.70.175.67', '2022-04-28 01:05:29', '2022-04-28 01:05:29'),
(4022, '2022-04-28 00:00:00', '172.70.34.110', '2022-04-28 01:05:30', '2022-04-28 01:05:30'),
(4023, '2022-04-28 00:00:00', '172.70.134.52', '2022-04-28 01:05:30', '2022-04-28 01:05:30'),
(4024, '2022-04-28 00:00:00', '172.70.189.137', '2022-04-28 01:18:42', '2022-04-28 01:18:42'),
(4025, '2022-04-28 00:00:00', '172.70.230.50', '2022-04-28 06:02:23', '2022-04-28 06:02:23'),
(4026, '2022-04-28 00:00:00', '172.70.92.156', '2022-04-28 07:45:35', '2022-04-28 07:45:35'),
(4027, '2022-04-28 00:00:00', '162.158.170.196', '2022-04-28 07:51:32', '2022-04-28 07:51:32'),
(4028, '2022-04-28 00:00:00', '172.70.188.150', '2022-04-28 07:57:29', '2022-04-28 07:57:29'),
(4029, '2022-04-28 00:00:00', '172.70.147.42', '2022-04-28 08:03:23', '2022-04-28 08:03:23'),
(4030, '2022-04-28 00:00:00', '172.70.175.175', '2022-04-28 11:09:27', '2022-04-28 11:09:27'),
(4031, '2022-04-28 00:00:00', '172.70.242.128', '2022-04-28 11:24:19', '2022-04-28 11:24:19'),
(4032, '2022-04-28 00:00:00', '172.70.147.34', '2022-04-28 13:44:25', '2022-04-28 13:44:25'),
(4033, '2022-04-28 00:00:00', '162.158.170.56', '2022-04-28 14:02:11', '2022-04-28 14:02:11'),
(4034, '2022-04-28 00:00:00', '162.158.162.78', '2022-04-28 14:31:55', '2022-04-28 14:31:55'),
(4035, '2022-04-28 00:00:00', '172.70.189.117', '2022-04-28 14:38:33', '2022-04-28 14:38:33'),
(4036, '2022-04-28 00:00:00', '172.70.175.175', '2022-04-28 14:38:48', '2022-04-28 14:38:48'),
(4037, '2022-04-28 00:00:00', '162.158.162.94', '2022-04-28 17:17:21', '2022-04-28 17:17:21'),
(4038, '2022-04-28 00:00:00', '172.70.92.156', '2022-04-28 17:39:44', '2022-04-28 17:39:44'),
(4039, '2022-04-28 00:00:00', '172.70.92.156', '2022-04-28 18:01:53', '2022-04-28 18:01:53'),
(4040, '2022-04-28 00:00:00', '172.70.92.156', '2022-04-28 18:58:27', '2022-04-28 18:58:27'),
(4041, '2022-04-28 00:00:00', '172.70.92.150', '2022-04-28 18:58:31', '2022-04-28 18:58:31'),
(4042, '2022-04-28 00:00:00', '162.158.163.41', '2022-04-28 18:58:40', '2022-04-28 18:58:40'),
(4043, '2022-04-28 00:00:00', '172.70.142.42', '2022-04-28 21:41:54', '2022-04-28 21:41:54'),
(4044, '2022-04-28 00:00:00', '172.70.38.4', '2022-04-28 21:47:44', '2022-04-28 21:47:44'),
(4045, '2022-04-28 00:00:00', '162.158.79.59', '2022-04-28 22:32:44', '2022-04-28 22:32:44'),
(4046, '2022-04-28 00:00:00', '172.70.35.9', '2022-04-28 22:55:14', '2022-04-28 22:55:14'),
(4047, '2022-04-28 00:00:00', '108.162.216.122', '2022-04-28 23:33:10', '2022-04-28 23:33:10'),
(4048, '2022-04-28 00:00:00', '108.162.216.66', '2022-04-28 23:33:59', '2022-04-28 23:33:59'),
(4049, '2022-04-28 00:00:00', '172.70.142.34', '2022-04-28 23:48:15', '2022-04-28 23:48:15'),
(4050, '2022-04-28 00:00:00', '172.70.142.218', '2022-04-28 23:54:12', '2022-04-28 23:54:12'),
(4051, '2022-04-29 00:00:00', '162.158.162.156', '2022-04-29 00:13:06', '2022-04-29 00:13:06'),
(4052, '2022-04-29 00:00:00', '172.70.188.36', '2022-04-29 00:19:06', '2022-04-29 00:19:06'),
(4053, '2022-04-29 00:00:00', '162.158.163.3', '2022-04-29 00:42:30', '2022-04-29 00:42:30'),
(4054, '2022-04-29 00:00:00', '172.70.134.94', '2022-04-29 00:47:44', '2022-04-29 00:47:44'),
(4055, '2022-04-29 00:00:00', '162.158.179.97', '2022-04-29 01:54:35', '2022-04-29 01:54:35'),
(4056, '2022-04-29 00:00:00', '172.70.189.95', '2022-04-29 04:50:36', '2022-04-29 04:50:36'),
(4057, '2022-04-29 00:00:00', '172.70.85.150', '2022-04-29 06:19:17', '2022-04-29 06:19:17'),
(4058, '2022-04-29 00:00:00', '188.114.111.151', '2022-04-29 06:25:59', '2022-04-29 06:25:59'),
(4059, '2022-04-29 00:00:00', '172.70.90.6', '2022-04-29 06:27:18', '2022-04-29 06:27:18'),
(4060, '2022-04-29 00:00:00', '172.70.90.6', '2022-04-29 06:42:42', '2022-04-29 06:42:42'),
(4061, '2022-04-29 00:00:00', '162.158.159.34', '2022-04-29 06:46:08', '2022-04-29 06:46:08'),
(4062, '2022-04-29 00:00:00', '172.70.90.212', '2022-04-29 06:50:09', '2022-04-29 06:50:09'),
(4063, '2022-04-29 00:00:00', '141.101.107.160', '2022-04-29 06:50:58', '2022-04-29 06:50:58'),
(4064, '2022-04-29 00:00:00', '172.70.188.192', '2022-04-29 07:02:00', '2022-04-29 07:02:00'),
(4065, '2022-04-29 00:00:00', '172.70.85.40', '2022-04-29 07:07:49', '2022-04-29 07:07:49'),
(4066, '2022-04-29 00:00:00', '162.158.159.120', '2022-04-29 07:09:29', '2022-04-29 07:09:29'),
(4067, '2022-04-29 00:00:00', '162.158.159.34', '2022-04-29 07:15:41', '2022-04-29 07:15:41'),
(4068, '2022-04-29 00:00:00', '141.101.99.149', '2022-04-29 07:18:46', '2022-04-29 07:18:46'),
(4069, '2022-04-29 00:00:00', '172.70.85.154', '2022-04-29 07:21:38', '2022-04-29 07:21:38'),
(4070, '2022-04-29 00:00:00', '172.70.162.12', '2022-04-29 07:24:56', '2022-04-29 07:24:56'),
(4071, '2022-04-29 00:00:00', '172.70.142.218', '2022-04-29 07:27:56', '2022-04-29 07:27:56'),
(4072, '2022-04-29 00:00:00', '172.70.90.212', '2022-04-29 07:31:25', '2022-04-29 07:31:25'),
(4073, '2022-04-29 00:00:00', '162.158.159.24', '2022-04-29 07:37:48', '2022-04-29 07:37:48'),
(4074, '2022-04-29 00:00:00', '172.70.85.34', '2022-04-29 07:44:04', '2022-04-29 07:44:04'),
(4075, '2022-04-29 00:00:00', '141.101.107.138', '2022-04-29 07:45:09', '2022-04-29 07:45:09'),
(4076, '2022-04-29 00:00:00', '172.70.90.150', '2022-04-29 07:49:17', '2022-04-29 07:49:17'),
(4077, '2022-04-29 00:00:00', '162.158.159.82', '2022-04-29 07:51:19', '2022-04-29 07:51:19'),
(4078, '2022-04-29 00:00:00', '172.70.188.36', '2022-04-29 07:51:53', '2022-04-29 07:51:53'),
(4079, '2022-04-29 00:00:00', '172.70.189.117', '2022-04-29 08:11:20', '2022-04-29 08:11:20'),
(4080, '2022-04-29 00:00:00', '172.70.162.240', '2022-04-29 08:13:48', '2022-04-29 08:13:48'),
(4081, '2022-04-29 00:00:00', '172.70.85.40', '2022-04-29 08:28:50', '2022-04-29 08:28:50'),
(4082, '2022-04-29 00:00:00', '172.70.162.240', '2022-04-29 08:34:53', '2022-04-29 08:34:53'),
(4083, '2022-04-29 00:00:00', '172.70.85.40', '2022-04-29 08:51:31', '2022-04-29 08:51:31'),
(4084, '2022-04-29 00:00:00', '172.70.90.172', '2022-04-29 08:57:05', '2022-04-29 08:57:05'),
(4085, '2022-04-29 00:00:00', '172.68.39.150', '2022-04-29 09:55:52', '2022-04-29 09:55:52'),
(4086, '2022-04-29 00:00:00', '162.158.159.82', '2022-04-29 10:23:32', '2022-04-29 10:23:32'),
(4087, '2022-04-29 00:00:00', '172.70.135.5', '2022-04-29 11:56:26', '2022-04-29 11:56:26'),
(4088, '2022-04-29 00:00:00', '162.158.159.82', '2022-04-29 12:35:41', '2022-04-29 12:35:41'),
(4089, '2022-04-29 00:00:00', '172.70.135.15', '2022-04-29 12:41:26', '2022-04-29 12:41:26'),
(4090, '2022-04-29 00:00:00', '162.158.159.44', '2022-04-29 12:49:58', '2022-04-29 12:49:58'),
(4091, '2022-04-29 00:00:00', '172.70.188.150', '2022-04-29 12:54:44', '2022-04-29 12:54:44'),
(4092, '2022-04-29 00:00:00', '172.70.34.122', '2022-04-29 13:26:26', '2022-04-29 13:26:26'),
(4093, '2022-04-29 00:00:00', '162.158.170.194', '2022-04-29 13:28:35', '2022-04-29 13:28:35'),
(4094, '2022-04-29 00:00:00', '172.70.85.34', '2022-04-29 13:35:49', '2022-04-29 13:35:49'),
(4095, '2022-04-29 00:00:00', '172.70.85.150', '2022-04-29 13:38:57', '2022-04-29 13:38:57'),
(4096, '2022-04-29 00:00:00', '172.70.162.12', '2022-04-29 13:40:00', '2022-04-29 13:40:00'),
(4097, '2022-04-29 00:00:00', '172.70.90.150', '2022-04-29 13:42:10', '2022-04-29 13:42:10'),
(4098, '2022-04-29 00:00:00', '172.70.90.172', '2022-04-29 13:44:49', '2022-04-29 13:44:49'),
(4099, '2022-04-29 00:00:00', '162.158.159.120', '2022-04-29 13:46:10', '2022-04-29 13:46:10'),
(4100, '2022-04-29 00:00:00', '141.101.107.160', '2022-04-29 13:47:12', '2022-04-29 13:47:12'),
(4101, '2022-04-29 00:00:00', '172.70.85.34', '2022-04-29 13:48:16', '2022-04-29 13:48:16'),
(4102, '2022-04-29 00:00:00', '162.158.159.82', '2022-04-29 13:49:27', '2022-04-29 13:49:27'),
(4103, '2022-04-29 00:00:00', '172.70.85.154', '2022-04-29 13:52:15', '2022-04-29 13:52:15'),
(4104, '2022-04-29 00:00:00', '172.70.90.212', '2022-04-29 13:53:02', '2022-04-29 13:53:02'),
(4105, '2022-04-29 00:00:00', '172.70.188.192', '2022-04-29 14:00:27', '2022-04-29 14:00:27'),
(4106, '2022-04-29 00:00:00', '172.68.39.216', '2022-04-29 14:08:35', '2022-04-29 14:08:35'),
(4107, '2022-04-29 00:00:00', '172.70.135.81', '2022-04-29 14:17:35', '2022-04-29 14:17:35'),
(4108, '2022-04-29 00:00:00', '172.68.39.236', '2022-04-29 14:18:08', '2022-04-29 14:18:08'),
(4109, '2022-04-29 00:00:00', '162.158.106.146', '2022-04-29 14:38:41', '2022-04-29 14:38:41'),
(4110, '2022-04-29 00:00:00', '162.158.170.56', '2022-04-29 14:51:22', '2022-04-29 14:51:22'),
(4111, '2022-04-29 00:00:00', '172.70.142.48', '2022-04-29 14:57:40', '2022-04-29 14:57:40'),
(4112, '2022-04-29 00:00:00', '172.70.188.150', '2022-04-29 15:15:18', '2022-04-29 15:15:18'),
(4113, '2022-04-29 00:00:00', '162.158.163.225', '2022-04-29 16:48:44', '2022-04-29 16:48:44'),
(4114, '2022-04-29 00:00:00', '162.158.106.146', '2022-04-29 22:18:34', '2022-04-29 22:18:34'),
(4115, '2022-04-29 00:00:00', '108.162.245.214', '2022-04-29 22:18:35', '2022-04-29 22:18:35'),
(4116, '2022-04-29 00:00:00', '108.162.246.117', '2022-04-29 22:18:35', '2022-04-29 22:18:35'),
(4117, '2022-04-29 00:00:00', '162.158.107.181', '2022-04-29 22:18:36', '2022-04-29 22:18:36'),
(4118, '2022-04-29 00:00:00', '162.158.107.167', '2022-04-29 22:18:36', '2022-04-29 22:18:36'),
(4119, '2022-04-29 00:00:00', '172.69.134.36', '2022-04-29 22:18:36', '2022-04-29 22:18:36'),
(4120, '2022-04-29 00:00:00', '162.158.107.21', '2022-04-29 22:18:37', '2022-04-29 22:18:37'),
(4121, '2022-04-29 00:00:00', '162.158.107.209', '2022-04-29 22:18:37', '2022-04-29 22:18:37'),
(4122, '2022-04-29 00:00:00', '172.69.134.94', '2022-04-29 22:18:37', '2022-04-29 22:18:37'),
(4123, '2022-04-29 00:00:00', '108.162.245.126', '2022-04-29 22:18:37', '2022-04-29 22:18:37'),
(4124, '2022-04-29 00:00:00', '108.162.245.36', '2022-04-29 22:18:37', '2022-04-29 22:18:37'),
(4125, '2022-04-29 00:00:00', '162.158.107.69', '2022-04-29 22:18:38', '2022-04-29 22:18:38'),
(4126, '2022-04-29 00:00:00', '108.162.246.7', '2022-04-29 22:18:38', '2022-04-29 22:18:38'),
(4127, '2022-04-29 00:00:00', '108.162.245.92', '2022-04-29 22:18:39', '2022-04-29 22:18:39'),
(4128, '2022-04-29 00:00:00', '172.68.132.211', '2022-04-29 22:18:40', '2022-04-29 22:18:40'),
(4129, '2022-04-29 00:00:00', '162.158.107.151', '2022-04-29 22:18:40', '2022-04-29 22:18:40'),
(4130, '2022-04-29 00:00:00', '162.158.106.94', '2022-04-29 22:18:41', '2022-04-29 22:18:41'),
(4131, '2022-04-29 00:00:00', '162.158.107.105', '2022-04-29 22:18:42', '2022-04-29 22:18:42'),
(4132, '2022-04-29 00:00:00', '162.158.106.124', '2022-04-29 22:18:43', '2022-04-29 22:18:43'),
(4133, '2022-04-29 00:00:00', '172.70.211.23', '2022-04-29 22:18:43', '2022-04-29 22:18:43'),
(4134, '2022-04-29 00:00:00', '172.69.33.154', '2022-04-29 22:18:43', '2022-04-29 22:18:43'),
(4135, '2022-04-29 00:00:00', '162.158.166.216', '2022-04-29 22:18:44', '2022-04-29 22:18:44'),
(4136, '2022-04-29 00:00:00', '108.162.246.45', '2022-04-29 22:18:47', '2022-04-29 22:18:47'),
(4137, '2022-04-29 00:00:00', '172.68.132.127', '2022-04-29 22:18:48', '2022-04-29 22:18:48'),
(4138, '2022-04-29 00:00:00', '162.158.107.133', '2022-04-29 22:18:48', '2022-04-29 22:18:48'),
(4139, '2022-04-29 00:00:00', '172.68.143.212', '2022-04-29 22:18:50', '2022-04-29 22:18:50'),
(4140, '2022-04-29 00:00:00', '172.68.133.216', '2022-04-29 22:18:50', '2022-04-29 22:18:50'),
(4141, '2022-04-29 00:00:00', '172.70.211.157', '2022-04-29 22:18:51', '2022-04-29 22:18:51'),
(4142, '2022-04-29 00:00:00', '172.70.211.39', '2022-04-29 22:18:52', '2022-04-29 22:18:52'),
(4143, '2022-04-29 00:00:00', '172.69.34.45', '2022-04-29 22:18:52', '2022-04-29 22:18:52'),
(4144, '2022-04-29 00:00:00', '172.71.10.144', '2022-04-29 23:53:42', '2022-04-29 23:53:42'),
(4145, '2022-04-30 00:00:00', '162.158.78.28', '2022-04-30 00:12:11', '2022-04-30 00:12:11'),
(4146, '2022-04-30 00:00:00', '172.70.85.40', '2022-04-30 00:36:39', '2022-04-30 00:36:39'),
(4147, '2022-04-30 00:00:00', '172.70.188.150', '2022-04-30 01:02:57', '2022-04-30 01:02:57'),
(4148, '2022-04-30 00:00:00', '162.158.92.130', '2022-04-30 01:20:48', '2022-04-30 01:20:48'),
(4149, '2022-04-30 00:00:00', '172.70.188.192', '2022-04-30 01:26:47', '2022-04-30 01:26:47'),
(4150, '2022-04-30 00:00:00', '172.70.92.156', '2022-04-30 01:32:16', '2022-04-30 01:32:16'),
(4151, '2022-04-30 00:00:00', '162.158.162.40', '2022-04-30 01:43:09', '2022-04-30 01:43:09'),
(4152, '2022-04-30 00:00:00', '172.70.92.160', '2022-04-30 01:54:03', '2022-04-30 01:54:03'),
(4153, '2022-04-30 00:00:00', '141.101.69.69', '2022-04-30 03:15:46', '2022-04-30 03:15:46'),
(4154, '2022-04-30 00:00:00', '172.70.206.172', '2022-04-30 06:14:43', '2022-04-30 06:14:43'),
(4155, '2022-04-30 00:00:00', '172.70.131.63', '2022-04-30 07:15:53', '2022-04-30 07:15:53'),
(4156, '2022-04-30 00:00:00', '162.158.162.40', '2022-04-30 07:26:11', '2022-04-30 07:26:11'),
(4157, '2022-04-30 00:00:00', '162.158.170.196', '2022-04-30 07:31:43', '2022-04-30 07:31:43'),
(4158, '2022-04-30 00:00:00', '162.158.162.240', '2022-04-30 07:37:07', '2022-04-30 07:37:07'),
(4159, '2022-04-30 00:00:00', '172.70.92.156', '2022-04-30 07:59:09', '2022-04-30 07:59:09'),
(4160, '2022-04-30 00:00:00', '162.158.163.207', '2022-04-30 08:20:29', '2022-04-30 08:20:29'),
(4161, '2022-04-30 00:00:00', '172.70.188.150', '2022-04-30 09:24:38', '2022-04-30 09:24:38'),
(4162, '2022-04-30 00:00:00', '172.68.50.220', '2022-04-30 11:08:21', '2022-04-30 11:08:21'),
(4163, '2022-04-30 00:00:00', '172.68.50.76', '2022-04-30 11:08:23', '2022-04-30 11:08:23'),
(4164, '2022-04-30 00:00:00', '172.68.50.104', '2022-04-30 11:08:27', '2022-04-30 11:08:27'),
(4165, '2022-04-30 00:00:00', '172.70.147.32', '2022-04-30 11:42:12', '2022-04-30 11:42:12'),
(4166, '2022-04-30 00:00:00', '162.158.163.21', '2022-04-30 12:51:41', '2022-04-30 12:51:41'),
(4167, '2022-04-30 00:00:00', '162.158.170.214', '2022-04-30 12:51:45', '2022-04-30 12:51:45'),
(4168, '2022-04-30 00:00:00', '172.70.92.156', '2022-04-30 12:51:46', '2022-04-30 12:51:46'),
(4169, '2022-04-30 00:00:00', '162.158.79.99', '2022-04-30 14:07:46', '2022-04-30 14:07:46'),
(4170, '2022-04-30 00:00:00', '172.70.188.150', '2022-04-30 14:16:37', '2022-04-30 14:16:37'),
(4171, '2022-04-30 00:00:00', '162.158.162.172', '2022-04-30 14:22:16', '2022-04-30 14:22:16'),
(4172, '2022-04-30 00:00:00', '172.68.50.192', '2022-04-30 16:16:19', '2022-04-30 16:16:19'),
(4173, '2022-04-30 00:00:00', '172.70.126.190', '2022-04-30 16:59:43', '2022-04-30 16:59:43'),
(4174, '2022-04-30 00:00:00', '162.158.170.214', '2022-04-30 18:04:16', '2022-04-30 18:04:16'),
(4175, '2022-04-30 00:00:00', '108.162.219.10', '2022-04-30 18:17:42', '2022-04-30 18:17:42'),
(4176, '2022-04-30 00:00:00', '162.158.62.105', '2022-04-30 18:17:43', '2022-04-30 18:17:43'),
(4177, '2022-04-30 00:00:00', '108.162.219.192', '2022-04-30 18:17:43', '2022-04-30 18:17:43'),
(4178, '2022-04-30 00:00:00', '172.70.142.224', '2022-04-30 18:35:04', '2022-04-30 18:35:04'),
(4179, '2022-04-30 00:00:00', '172.70.142.218', '2022-04-30 18:50:26', '2022-04-30 18:50:26'),
(4180, '2022-04-30 00:00:00', '172.70.142.48', '2022-04-30 19:26:47', '2022-04-30 19:26:47'),
(4181, '2022-04-30 00:00:00', '172.70.251.125', '2022-04-30 20:09:49', '2022-04-30 20:09:49'),
(4182, '2022-04-30 00:00:00', '172.70.250.64', '2022-04-30 20:09:51', '2022-04-30 20:09:51'),
(4183, '2022-05-01 00:00:00', '172.70.142.224', '2022-05-01 00:22:02', '2022-05-01 00:22:02'),
(4184, '2022-05-01 00:00:00', '172.70.142.218', '2022-05-01 00:40:06', '2022-05-01 00:40:06'),
(4185, '2022-05-01 00:00:00', '172.70.92.150', '2022-05-01 01:11:05', '2022-05-01 01:11:05'),
(4186, '2022-05-01 00:00:00', '162.158.170.56', '2022-05-01 01:28:48', '2022-05-01 01:28:48'),
(4187, '2022-05-01 00:00:00', '162.158.163.195', '2022-05-01 01:34:12', '2022-05-01 01:34:12'),
(4188, '2022-05-01 00:00:00', '172.70.34.142', '2022-05-01 01:57:18', '2022-05-01 01:57:18'),
(4189, '2022-05-01 00:00:00', '172.70.147.48', '2022-05-01 02:06:52', '2022-05-01 02:06:52'),
(4190, '2022-05-01 00:00:00', '141.101.68.198', '2022-05-01 02:21:49', '2022-05-01 02:21:49'),
(4191, '2022-05-01 00:00:00', '141.101.69.47', '2022-05-01 02:21:50', '2022-05-01 02:21:50'),
(4192, '2022-05-01 00:00:00', '172.70.174.224', '2022-05-01 03:27:18', '2022-05-01 03:27:18'),
(4193, '2022-05-01 00:00:00', '172.70.142.32', '2022-05-01 05:20:37', '2022-05-01 05:20:37'),
(4194, '2022-05-01 00:00:00', '172.70.35.9', '2022-05-01 05:42:19', '2022-05-01 05:42:19'),
(4195, '2022-05-01 00:00:00', '172.70.134.218', '2022-05-01 06:27:18', '2022-05-01 06:27:18'),
(4196, '2022-05-01 00:00:00', '108.162.246.223', '2022-05-01 06:34:07', '2022-05-01 06:34:07'),
(4197, '2022-05-01 00:00:00', '172.70.142.34', '2022-05-01 06:36:52', '2022-05-01 06:36:52'),
(4198, '2022-05-01 00:00:00', '172.70.142.48', '2022-05-01 06:44:48', '2022-05-01 06:44:48'),
(4199, '2022-05-01 00:00:00', '172.70.92.150', '2022-05-01 06:56:24', '2022-05-01 06:56:24'),
(4200, '2022-05-01 00:00:00', '172.70.142.34', '2022-05-01 07:09:44', '2022-05-01 07:09:44'),
(4201, '2022-05-01 00:00:00', '162.158.162.252', '2022-05-01 07:35:11', '2022-05-01 07:35:11'),
(4202, '2022-05-01 00:00:00', '162.158.78.200', '2022-05-01 08:06:28', '2022-05-01 08:06:28'),
(4203, '2022-05-01 00:00:00', '172.70.92.150', '2022-05-01 08:32:57', '2022-05-01 08:32:57'),
(4204, '2022-05-01 00:00:00', '172.70.134.54', '2022-05-01 10:04:28', '2022-05-01 10:04:28'),
(4205, '2022-05-01 00:00:00', '108.162.229.10', '2022-05-01 10:06:11', '2022-05-01 10:06:11'),
(4206, '2022-05-01 00:00:00', '141.101.99.235', '2022-05-01 10:25:06', '2022-05-01 10:25:06'),
(4207, '2022-05-01 00:00:00', '172.70.134.4', '2022-05-01 10:49:28', '2022-05-01 10:49:28'),
(4208, '2022-05-01 00:00:00', '172.70.142.224', '2022-05-01 11:41:09', '2022-05-01 11:41:09'),
(4209, '2022-05-01 00:00:00', '162.158.170.78', '2022-05-01 12:23:32', '2022-05-01 12:23:32'),
(4210, '2022-05-01 00:00:00', '162.158.162.164', '2022-05-01 12:35:30', '2022-05-01 12:35:30'),
(4211, '2022-05-01 00:00:00', '172.70.130.156', '2022-05-01 12:38:43', '2022-05-01 12:38:43'),
(4212, '2022-05-01 00:00:00', '172.70.188.150', '2022-05-01 12:45:03', '2022-05-01 12:45:03'),
(4213, '2022-05-01 00:00:00', '172.70.34.110', '2022-05-01 12:57:18', '2022-05-01 12:57:18'),
(4214, '2022-05-01 00:00:00', '172.70.38.64', '2022-05-01 13:27:18', '2022-05-01 13:27:18'),
(4215, '2022-05-01 00:00:00', '162.158.78.44', '2022-05-01 13:57:18', '2022-05-01 13:57:18'),
(4216, '2022-05-01 00:00:00', '172.70.189.137', '2022-05-01 14:16:17', '2022-05-01 14:16:17'),
(4217, '2022-05-01 00:00:00', '162.158.163.175', '2022-05-01 14:22:12', '2022-05-01 14:22:12'),
(4218, '2022-05-01 00:00:00', '172.70.175.3', '2022-05-01 14:27:18', '2022-05-01 14:27:18'),
(4219, '2022-05-01 00:00:00', '172.70.189.117', '2022-05-01 15:10:28', '2022-05-01 15:10:28'),
(4220, '2022-05-01 00:00:00', '172.70.35.11', '2022-05-01 15:19:49', '2022-05-01 15:19:49'),
(4221, '2022-05-01 00:00:00', '172.70.142.224', '2022-05-01 15:36:37', '2022-05-01 15:36:37'),
(4222, '2022-05-01 00:00:00', '172.70.35.35', '2022-05-01 15:42:18', '2022-05-01 15:42:18'),
(4223, '2022-05-01 00:00:00', '172.70.175.173', '2022-05-01 16:27:18', '2022-05-01 16:27:18'),
(4224, '2022-05-01 00:00:00', '172.70.142.224', '2022-05-01 16:34:37', '2022-05-01 16:34:37'),
(4225, '2022-05-01 00:00:00', '172.70.135.29', '2022-05-01 16:49:48', '2022-05-01 16:49:48'),
(4226, '2022-05-01 00:00:00', '172.70.174.190', '2022-05-01 17:12:18', '2022-05-01 17:12:18'),
(4227, '2022-05-01 00:00:00', '172.70.174.254', '2022-05-01 17:34:48', '2022-05-01 17:34:48'),
(4228, '2022-05-01 00:00:00', '172.70.175.27', '2022-05-01 19:04:48', '2022-05-01 19:04:48'),
(4229, '2022-05-01 00:00:00', '172.70.147.34', '2022-05-01 19:19:52', '2022-05-01 19:19:52'),
(4230, '2022-05-01 00:00:00', '172.70.34.38', '2022-05-01 19:49:49', '2022-05-01 19:49:49'),
(4231, '2022-05-01 00:00:00', '172.71.16.210', '2022-05-01 23:27:23', '2022-05-01 23:27:23'),
(4232, '2022-05-02 00:00:00', '141.101.69.69', '2022-05-02 00:05:10', '2022-05-02 00:05:10'),
(4233, '2022-05-02 00:00:00', '172.70.142.34', '2022-05-02 00:45:25', '2022-05-02 00:45:25'),
(4234, '2022-05-02 00:00:00', '162.158.162.94', '2022-05-02 01:00:43', '2022-05-02 01:00:43'),
(4235, '2022-05-02 00:00:00', '172.70.147.42', '2022-05-02 01:16:00', '2022-05-02 01:16:00'),
(4236, '2022-05-02 00:00:00', '172.70.188.192', '2022-05-02 01:21:05', '2022-05-02 01:21:05'),
(4237, '2022-05-02 00:00:00', '172.70.142.224', '2022-05-02 01:26:14', '2022-05-02 01:26:14'),
(4238, '2022-05-02 00:00:00', '162.158.163.53', '2022-05-02 01:31:22', '2022-05-02 01:31:22'),
(4239, '2022-05-02 00:00:00', '172.70.147.48', '2022-05-02 01:36:28', '2022-05-02 01:36:28'),
(4240, '2022-05-02 00:00:00', '172.70.92.156', '2022-05-02 01:41:39', '2022-05-02 01:41:39'),
(4241, '2022-05-02 00:00:00', '172.70.142.224', '2022-05-02 01:46:47', '2022-05-02 01:46:47'),
(4242, '2022-05-02 00:00:00', '172.70.92.150', '2022-05-02 01:52:02', '2022-05-02 01:52:02'),
(4243, '2022-05-02 00:00:00', '162.158.92.160', '2022-05-02 04:48:04', '2022-05-02 04:48:04'),
(4244, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 05:36:08', '2022-05-02 05:36:08'),
(4245, '2022-05-02 00:00:00', '141.101.107.82', '2022-05-02 06:32:48', '2022-05-02 06:32:48'),
(4246, '2022-05-02 00:00:00', '162.158.159.120', '2022-05-02 06:35:13', '2022-05-02 06:35:13'),
(4247, '2022-05-02 00:00:00', '172.70.90.172', '2022-05-02 06:38:04', '2022-05-02 06:38:04'),
(4248, '2022-05-02 00:00:00', '172.70.90.6', '2022-05-02 06:40:17', '2022-05-02 06:40:17'),
(4249, '2022-05-02 00:00:00', '162.158.170.194', '2022-05-02 06:42:13', '2022-05-02 06:42:13'),
(4250, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 06:51:44', '2022-05-02 06:51:44'),
(4251, '2022-05-02 00:00:00', '172.70.188.192', '2022-05-02 06:54:50', '2022-05-02 06:54:50'),
(4252, '2022-05-02 00:00:00', '162.158.163.7', '2022-05-02 07:01:35', '2022-05-02 07:01:35'),
(4253, '2022-05-02 00:00:00', '141.101.107.138', '2022-05-02 07:02:30', '2022-05-02 07:02:30'),
(4254, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 07:11:10', '2022-05-02 07:11:10'),
(4255, '2022-05-02 00:00:00', '141.101.98.242', '2022-05-02 07:21:08', '2022-05-02 07:21:08'),
(4256, '2022-05-02 00:00:00', '172.70.34.90', '2022-05-02 07:45:45', '2022-05-02 07:45:45'),
(4257, '2022-05-02 00:00:00', '141.101.77.43', '2022-05-02 07:49:40', '2022-05-02 07:49:40'),
(4258, '2022-05-02 00:00:00', '172.68.39.236', '2022-05-02 07:51:48', '2022-05-02 07:51:48'),
(4259, '2022-05-02 00:00:00', '172.70.142.34', '2022-05-02 07:59:28', '2022-05-02 07:59:28'),
(4260, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 08:00:19', '2022-05-02 08:00:19'),
(4261, '2022-05-02 00:00:00', '172.70.142.224', '2022-05-02 08:01:28', '2022-05-02 08:01:28'),
(4262, '2022-05-02 00:00:00', '172.70.90.6', '2022-05-02 08:09:41', '2022-05-02 08:09:41'),
(4263, '2022-05-02 00:00:00', '172.70.85.34', '2022-05-02 08:11:23', '2022-05-02 08:11:23'),
(4264, '2022-05-02 00:00:00', '172.70.85.150', '2022-05-02 08:12:14', '2022-05-02 08:12:14'),
(4265, '2022-05-02 00:00:00', '162.158.159.82', '2022-05-02 08:20:56', '2022-05-02 08:20:56'),
(4266, '2022-05-02 00:00:00', '172.70.90.150', '2022-05-02 08:22:21', '2022-05-02 08:22:21'),
(4267, '2022-05-02 00:00:00', '172.70.90.6', '2022-05-02 08:23:28', '2022-05-02 08:23:28'),
(4268, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 08:24:16', '2022-05-02 08:24:16'),
(4269, '2022-05-02 00:00:00', '162.158.159.112', '2022-05-02 08:25:38', '2022-05-02 08:25:38'),
(4270, '2022-05-02 00:00:00', '172.68.39.216', '2022-05-02 08:39:25', '2022-05-02 08:39:25'),
(4271, '2022-05-02 00:00:00', '172.70.131.167', '2022-05-02 08:42:43', '2022-05-02 08:42:43'),
(4272, '2022-05-02 00:00:00', '172.70.90.6', '2022-05-02 09:18:26', '2022-05-02 09:18:26'),
(4273, '2022-05-02 00:00:00', '172.70.90.172', '2022-05-02 09:23:30', '2022-05-02 09:23:30'),
(4274, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 09:44:42', '2022-05-02 09:44:42'),
(4275, '2022-05-02 00:00:00', '141.101.99.85', '2022-05-02 09:45:42', '2022-05-02 09:45:42'),
(4276, '2022-05-02 00:00:00', '162.158.159.112', '2022-05-02 09:50:28', '2022-05-02 09:50:28'),
(4277, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 09:52:00', '2022-05-02 09:52:00'),
(4278, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 09:54:35', '2022-05-02 09:54:35'),
(4279, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 10:05:23', '2022-05-02 10:05:23'),
(4280, '2022-05-02 00:00:00', '172.68.39.134', '2022-05-02 10:13:17', '2022-05-02 10:13:17'),
(4281, '2022-05-02 00:00:00', '172.70.90.6', '2022-05-02 10:16:54', '2022-05-02 10:16:54'),
(4282, '2022-05-02 00:00:00', '141.101.99.151', '2022-05-02 10:17:52', '2022-05-02 10:17:52'),
(4283, '2022-05-02 00:00:00', '141.101.107.160', '2022-05-02 10:19:06', '2022-05-02 10:19:06'),
(4284, '2022-05-02 00:00:00', '172.70.90.172', '2022-05-02 10:21:51', '2022-05-02 10:21:51'),
(4285, '2022-05-02 00:00:00', '172.68.39.150', '2022-05-02 10:36:06', '2022-05-02 10:36:06'),
(4286, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 10:38:45', '2022-05-02 10:38:45'),
(4287, '2022-05-02 00:00:00', '172.70.90.172', '2022-05-02 10:39:22', '2022-05-02 10:39:22'),
(4288, '2022-05-02 00:00:00', '162.158.227.240', '2022-05-02 10:39:36', '2022-05-02 10:39:36'),
(4289, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 10:40:00', '2022-05-02 10:40:00'),
(4290, '2022-05-02 00:00:00', '162.158.235.81', '2022-05-02 10:42:56', '2022-05-02 10:42:56'),
(4291, '2022-05-02 00:00:00', '141.101.107.138', '2022-05-02 10:43:36', '2022-05-02 10:43:36'),
(4292, '2022-05-02 00:00:00', '172.70.85.150', '2022-05-02 10:53:59', '2022-05-02 10:53:59'),
(4293, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 10:55:27', '2022-05-02 10:55:27'),
(4294, '2022-05-02 00:00:00', '172.70.162.240', '2022-05-02 10:59:40', '2022-05-02 10:59:40'),
(4295, '2022-05-02 00:00:00', '172.70.162.22', '2022-05-02 11:00:46', '2022-05-02 11:00:46'),
(4296, '2022-05-02 00:00:00', '172.70.162.188', '2022-05-02 11:01:45', '2022-05-02 11:01:45'),
(4297, '2022-05-02 00:00:00', '172.70.90.150', '2022-05-02 11:03:25', '2022-05-02 11:03:25'),
(4298, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 11:04:07', '2022-05-02 11:04:07'),
(4299, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 11:06:46', '2022-05-02 11:06:46'),
(4300, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 11:22:09', '2022-05-02 11:22:09'),
(4301, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 11:22:52', '2022-05-02 11:22:52'),
(4302, '2022-05-02 00:00:00', '172.70.85.150', '2022-05-02 11:24:58', '2022-05-02 11:24:58'),
(4303, '2022-05-02 00:00:00', '172.70.90.150', '2022-05-02 11:26:25', '2022-05-02 11:26:25'),
(4304, '2022-05-02 00:00:00', '172.70.85.34', '2022-05-02 11:28:01', '2022-05-02 11:28:01'),
(4305, '2022-05-02 00:00:00', '172.70.90.172', '2022-05-02 11:29:16', '2022-05-02 11:29:16'),
(4306, '2022-05-02 00:00:00', '172.68.39.134', '2022-05-02 11:30:01', '2022-05-02 11:30:01'),
(4307, '2022-05-02 00:00:00', '172.70.162.22', '2022-05-02 11:32:35', '2022-05-02 11:32:35'),
(4308, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 11:36:32', '2022-05-02 11:36:32'),
(4309, '2022-05-02 00:00:00', '141.101.99.151', '2022-05-02 11:38:08', '2022-05-02 11:38:08'),
(4310, '2022-05-02 00:00:00', '141.101.107.160', '2022-05-02 11:40:39', '2022-05-02 11:40:39'),
(4311, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 11:41:57', '2022-05-02 11:41:57'),
(4312, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 11:44:36', '2022-05-02 11:44:36'),
(4313, '2022-05-02 00:00:00', '172.70.85.154', '2022-05-02 11:47:40', '2022-05-02 11:47:40'),
(4314, '2022-05-02 00:00:00', '141.101.99.235', '2022-05-02 11:48:48', '2022-05-02 11:48:48'),
(4315, '2022-05-02 00:00:00', '172.70.85.150', '2022-05-02 11:49:54', '2022-05-02 11:49:54'),
(4316, '2022-05-02 00:00:00', '172.70.90.150', '2022-05-02 11:53:00', '2022-05-02 11:53:00'),
(4317, '2022-05-02 00:00:00', '172.68.39.150', '2022-05-02 12:05:58', '2022-05-02 12:05:58'),
(4318, '2022-05-02 00:00:00', '141.101.98.242', '2022-05-02 12:07:24', '2022-05-02 12:07:24'),
(4319, '2022-05-02 00:00:00', '172.70.218.236', '2022-05-02 12:08:10', '2022-05-02 12:08:10'),
(4320, '2022-05-02 00:00:00', '172.70.218.28', '2022-05-02 12:19:06', '2022-05-02 12:19:06'),
(4321, '2022-05-02 00:00:00', '172.70.218.198', '2022-05-02 12:24:00', '2022-05-02 12:24:00'),
(4322, '2022-05-02 00:00:00', '172.70.90.150', '2022-05-02 12:24:11', '2022-05-02 12:24:11'),
(4323, '2022-05-02 00:00:00', '172.70.85.40', '2022-05-02 12:24:58', '2022-05-02 12:24:58'),
(4324, '2022-05-02 00:00:00', '172.70.85.150', '2022-05-02 12:25:30', '2022-05-02 12:25:30'),
(4325, '2022-05-02 00:00:00', '162.158.159.120', '2022-05-02 12:26:32', '2022-05-02 12:26:32'),
(4326, '2022-05-02 00:00:00', '162.158.48.128', '2022-05-02 13:05:58', '2022-05-02 13:05:58'),
(4327, '2022-05-02 00:00:00', '162.158.235.43', '2022-05-02 13:08:47', '2022-05-02 13:08:47'),
(4328, '2022-05-02 00:00:00', '172.70.85.34', '2022-05-02 13:10:04', '2022-05-02 13:10:04'),
(4329, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 13:12:10', '2022-05-02 13:12:10'),
(4330, '2022-05-02 00:00:00', '172.70.142.224', '2022-05-02 13:14:35', '2022-05-02 13:14:35'),
(4331, '2022-05-02 00:00:00', '162.158.235.25', '2022-05-02 13:28:04', '2022-05-02 13:28:04'),
(4332, '2022-05-02 00:00:00', '172.70.90.212', '2022-05-02 13:39:40', '2022-05-02 13:39:40'),
(4333, '2022-05-02 00:00:00', '172.70.85.34', '2022-05-02 13:40:29', '2022-05-02 13:40:29'),
(4334, '2022-05-02 00:00:00', '172.70.162.240', '2022-05-02 13:41:42', '2022-05-02 13:41:42'),
(4335, '2022-05-02 00:00:00', '162.158.159.120', '2022-05-02 13:49:14', '2022-05-02 13:49:14'),
(4336, '2022-05-02 00:00:00', '141.101.107.82', '2022-05-02 14:03:55', '2022-05-02 14:03:55'),
(4337, '2022-05-02 00:00:00', '172.68.39.236', '2022-05-02 14:04:35', '2022-05-02 14:04:35');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(4338, '2022-05-02 00:00:00', '141.101.107.160', '2022-05-02 14:04:37', '2022-05-02 14:04:37'),
(4339, '2022-05-02 00:00:00', '172.68.39.134', '2022-05-02 14:09:55', '2022-05-02 14:09:55'),
(4340, '2022-05-02 00:00:00', '162.158.163.203', '2022-05-02 14:15:14', '2022-05-02 14:15:14'),
(4341, '2022-05-02 00:00:00', '172.70.142.48', '2022-05-02 14:33:48', '2022-05-02 14:33:48'),
(4342, '2022-05-02 00:00:00', '172.70.189.95', '2022-05-02 14:48:20', '2022-05-02 14:48:20'),
(4343, '2022-05-02 00:00:00', '172.70.142.42', '2022-05-02 15:09:39', '2022-05-02 15:09:39'),
(4344, '2022-05-02 00:00:00', '162.158.159.24', '2022-05-02 15:24:11', '2022-05-02 15:24:11'),
(4345, '2022-05-02 00:00:00', '162.158.227.236', '2022-05-02 15:35:16', '2022-05-02 15:35:16'),
(4346, '2022-05-02 00:00:00', '172.68.10.182', '2022-05-02 15:39:37', '2022-05-02 15:39:37'),
(4347, '2022-05-02 00:00:00', '172.68.245.221', '2022-05-02 15:40:58', '2022-05-02 15:40:58'),
(4348, '2022-05-02 00:00:00', '172.68.244.190', '2022-05-02 15:40:59', '2022-05-02 15:40:59'),
(4349, '2022-05-02 00:00:00', '172.68.11.113', '2022-05-02 15:41:02', '2022-05-02 15:41:02'),
(4350, '2022-05-02 00:00:00', '172.68.11.151', '2022-05-02 15:41:05', '2022-05-02 15:41:05'),
(4351, '2022-05-02 00:00:00', '172.68.11.155', '2022-05-02 15:41:07', '2022-05-02 15:41:07'),
(4352, '2022-05-02 00:00:00', '172.68.11.51', '2022-05-02 15:41:08', '2022-05-02 15:41:08'),
(4353, '2022-05-02 00:00:00', '172.68.11.101', '2022-05-02 15:41:09', '2022-05-02 15:41:09'),
(4354, '2022-05-02 00:00:00', '172.68.11.11', '2022-05-02 15:41:13', '2022-05-02 15:41:13'),
(4355, '2022-05-02 00:00:00', '172.68.245.5', '2022-05-02 15:41:13', '2022-05-02 15:41:13'),
(4356, '2022-05-02 00:00:00', '172.68.11.5', '2022-05-02 15:41:15', '2022-05-02 15:41:15'),
(4357, '2022-05-02 00:00:00', '172.68.10.58', '2022-05-02 15:41:17', '2022-05-02 15:41:17'),
(4358, '2022-05-02 00:00:00', '172.68.246.30', '2022-05-02 15:41:19', '2022-05-02 15:41:19'),
(4359, '2022-05-02 00:00:00', '172.68.11.129', '2022-05-02 15:41:20', '2022-05-02 15:41:20'),
(4360, '2022-05-02 00:00:00', '172.68.10.148', '2022-05-02 15:41:22', '2022-05-02 15:41:22'),
(4361, '2022-05-02 00:00:00', '162.158.78.178', '2022-05-02 15:41:23', '2022-05-02 15:41:23'),
(4362, '2022-05-02 00:00:00', '172.68.246.66', '2022-05-02 15:41:24', '2022-05-02 15:41:24'),
(4363, '2022-05-02 00:00:00', '172.68.244.76', '2022-05-02 15:41:26', '2022-05-02 15:41:26'),
(4364, '2022-05-02 00:00:00', '172.68.244.142', '2022-05-02 15:41:28', '2022-05-02 15:41:28'),
(4365, '2022-05-02 00:00:00', '172.68.11.73', '2022-05-02 15:41:32', '2022-05-02 15:41:32'),
(4366, '2022-05-02 00:00:00', '172.68.10.52', '2022-05-02 15:41:33', '2022-05-02 15:41:33'),
(4367, '2022-05-02 00:00:00', '172.68.11.135', '2022-05-02 15:41:35', '2022-05-02 15:41:35'),
(4368, '2022-05-02 00:00:00', '172.68.11.23', '2022-05-02 15:41:37', '2022-05-02 15:41:37'),
(4369, '2022-05-02 00:00:00', '172.68.10.120', '2022-05-02 15:41:40', '2022-05-02 15:41:40'),
(4370, '2022-05-02 00:00:00', '172.68.244.64', '2022-05-02 15:41:42', '2022-05-02 15:41:42'),
(4371, '2022-05-02 00:00:00', '172.68.10.54', '2022-05-02 15:41:44', '2022-05-02 15:41:44'),
(4372, '2022-05-02 00:00:00', '172.68.11.21', '2022-05-02 15:42:04', '2022-05-02 15:42:04'),
(4373, '2022-05-02 00:00:00', '172.68.10.4', '2022-05-02 15:42:06', '2022-05-02 15:42:06'),
(4374, '2022-05-02 00:00:00', '172.68.246.96', '2022-05-02 15:42:08', '2022-05-02 15:42:08'),
(4375, '2022-05-02 00:00:00', '172.68.245.137', '2022-05-02 15:42:17', '2022-05-02 15:42:17'),
(4376, '2022-05-02 00:00:00', '172.68.245.23', '2022-05-02 15:42:19', '2022-05-02 15:42:19'),
(4377, '2022-05-02 00:00:00', '172.68.11.77', '2022-05-02 15:42:19', '2022-05-02 15:42:19'),
(4378, '2022-05-02 00:00:00', '172.68.244.160', '2022-05-02 15:42:20', '2022-05-02 15:42:20'),
(4379, '2022-05-02 00:00:00', '172.68.245.113', '2022-05-02 15:42:22', '2022-05-02 15:42:22'),
(4380, '2022-05-02 00:00:00', '172.68.10.102', '2022-05-02 15:42:22', '2022-05-02 15:42:22'),
(4381, '2022-05-02 00:00:00', '172.68.244.72', '2022-05-02 15:42:23', '2022-05-02 15:42:23'),
(4382, '2022-05-02 00:00:00', '172.68.10.208', '2022-05-02 15:42:24', '2022-05-02 15:42:24'),
(4383, '2022-05-02 00:00:00', '172.68.10.62', '2022-05-02 15:42:26', '2022-05-02 15:42:26'),
(4384, '2022-05-02 00:00:00', '172.68.11.41', '2022-05-02 15:42:26', '2022-05-02 15:42:26'),
(4385, '2022-05-02 00:00:00', '172.68.246.222', '2022-05-02 15:42:27', '2022-05-02 15:42:27'),
(4386, '2022-05-02 00:00:00', '172.68.11.175', '2022-05-02 15:42:28', '2022-05-02 15:42:28'),
(4387, '2022-05-02 00:00:00', '172.68.10.248', '2022-05-02 15:42:37', '2022-05-02 15:42:37'),
(4388, '2022-05-02 00:00:00', '172.68.246.198', '2022-05-02 15:42:39', '2022-05-02 15:42:39'),
(4389, '2022-05-02 00:00:00', '172.68.246.60', '2022-05-02 15:42:40', '2022-05-02 15:42:40'),
(4390, '2022-05-02 00:00:00', '172.68.11.31', '2022-05-02 15:43:06', '2022-05-02 15:43:06'),
(4391, '2022-05-02 00:00:00', '172.68.11.187', '2022-05-02 15:44:09', '2022-05-02 15:44:09'),
(4392, '2022-05-02 00:00:00', '172.68.245.71', '2022-05-02 15:44:11', '2022-05-02 15:44:11'),
(4393, '2022-05-02 00:00:00', '172.68.11.115', '2022-05-02 15:44:29', '2022-05-02 15:44:29'),
(4394, '2022-05-02 00:00:00', '172.68.246.210', '2022-05-02 15:44:30', '2022-05-02 15:44:30'),
(4395, '2022-05-02 00:00:00', '172.68.10.68', '2022-05-02 15:45:16', '2022-05-02 15:45:16'),
(4396, '2022-05-02 00:00:00', '172.68.11.185', '2022-05-02 15:45:34', '2022-05-02 15:45:34'),
(4397, '2022-05-02 00:00:00', '172.68.11.139', '2022-05-02 15:45:52', '2022-05-02 15:45:52'),
(4398, '2022-05-02 00:00:00', '172.68.10.14', '2022-05-02 15:45:52', '2022-05-02 15:45:52'),
(4399, '2022-05-02 00:00:00', '172.68.244.56', '2022-05-02 15:46:05', '2022-05-02 15:46:05'),
(4400, '2022-05-02 00:00:00', '172.68.10.252', '2022-05-02 15:46:07', '2022-05-02 15:46:07'),
(4401, '2022-05-02 00:00:00', '172.68.10.118', '2022-05-02 15:46:08', '2022-05-02 15:46:08'),
(4402, '2022-05-02 00:00:00', '172.68.244.106', '2022-05-02 15:46:08', '2022-05-02 15:46:08'),
(4403, '2022-05-02 00:00:00', '172.68.10.50', '2022-05-02 15:46:09', '2022-05-02 15:46:09'),
(4404, '2022-05-02 00:00:00', '172.68.11.95', '2022-05-02 15:46:40', '2022-05-02 15:46:40'),
(4405, '2022-05-02 00:00:00', '172.68.10.36', '2022-05-02 15:47:20', '2022-05-02 15:47:20'),
(4406, '2022-05-02 00:00:00', '172.68.11.191', '2022-05-02 15:50:06', '2022-05-02 15:50:06'),
(4407, '2022-05-02 00:00:00', '172.70.246.74', '2022-05-02 16:12:23', '2022-05-02 16:12:23'),
(4408, '2022-05-02 00:00:00', '172.70.85.34', '2022-05-02 16:12:35', '2022-05-02 16:12:35'),
(4409, '2022-05-02 00:00:00', '162.158.163.155', '2022-05-02 17:17:41', '2022-05-02 17:17:41'),
(4410, '2022-05-02 00:00:00', '172.70.189.117', '2022-05-02 17:32:46', '2022-05-02 17:32:46'),
(4411, '2022-05-02 00:00:00', '162.158.162.40', '2022-05-02 17:37:50', '2022-05-02 17:37:50'),
(4412, '2022-05-02 00:00:00', '172.70.92.150', '2022-05-02 17:48:11', '2022-05-02 17:48:11'),
(4413, '2022-05-02 00:00:00', '162.158.162.218', '2022-05-02 17:53:03', '2022-05-02 17:53:03'),
(4414, '2022-05-02 00:00:00', '108.162.246.241', '2022-05-02 18:40:50', '2022-05-02 18:40:50'),
(4415, '2022-05-02 00:00:00', '108.162.245.70', '2022-05-02 19:03:27', '2022-05-02 19:03:27'),
(4416, '2022-05-02 00:00:00', '162.158.107.219', '2022-05-02 19:25:57', '2022-05-02 19:25:57'),
(4417, '2022-05-02 00:00:00', '162.158.106.254', '2022-05-02 19:48:27', '2022-05-02 19:48:27'),
(4418, '2022-05-02 00:00:00', '108.162.245.222', '2022-05-02 22:03:21', '2022-05-02 22:03:21'),
(4419, '2022-05-02 00:00:00', '162.158.179.77', '2022-05-02 23:28:40', '2022-05-02 23:28:40'),
(4420, '2022-05-03 00:00:00', '172.70.142.224', '2022-05-03 00:02:07', '2022-05-03 00:02:07'),
(4421, '2022-05-03 00:00:00', '162.158.50.73', '2022-05-03 00:08:08', '2022-05-03 00:08:08'),
(4422, '2022-05-03 00:00:00', '162.158.170.254', '2022-05-03 00:47:45', '2022-05-03 00:47:45'),
(4423, '2022-05-03 00:00:00', '141.101.69.47', '2022-05-03 00:54:41', '2022-05-03 00:54:41'),
(4424, '2022-05-03 00:00:00', '162.158.50.73', '2022-05-03 01:03:22', '2022-05-03 01:03:22'),
(4425, '2022-05-03 00:00:00', '162.158.50.19', '2022-05-03 01:17:44', '2022-05-03 01:17:44'),
(4426, '2022-05-03 00:00:00', '108.162.245.70', '2022-05-03 01:37:11', '2022-05-03 01:37:11'),
(4427, '2022-05-03 00:00:00', '162.158.107.29', '2022-05-03 03:07:11', '2022-05-03 03:07:11'),
(4428, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 04:05:12', '2022-05-03 04:05:12'),
(4429, '2022-05-03 00:00:00', '108.162.246.19', '2022-05-03 04:37:11', '2022-05-03 04:37:11'),
(4430, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 05:11:00', '2022-05-03 05:11:00'),
(4431, '2022-05-03 00:00:00', '162.158.227.234', '2022-05-03 05:12:38', '2022-05-03 05:12:38'),
(4432, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 05:16:04', '2022-05-03 05:16:04'),
(4433, '2022-05-03 00:00:00', '141.101.99.149', '2022-05-03 05:17:39', '2022-05-03 05:17:39'),
(4434, '2022-05-03 00:00:00', '172.70.218.28', '2022-05-03 05:18:13', '2022-05-03 05:18:13'),
(4435, '2022-05-03 00:00:00', '141.101.99.11', '2022-05-03 05:19:24', '2022-05-03 05:19:24'),
(4436, '2022-05-03 00:00:00', '172.70.90.150', '2022-05-03 05:21:38', '2022-05-03 05:21:38'),
(4437, '2022-05-03 00:00:00', '172.70.218.174', '2022-05-03 05:27:18', '2022-05-03 05:27:18'),
(4438, '2022-05-03 00:00:00', '172.70.218.28', '2022-05-03 05:39:25', '2022-05-03 05:39:25'),
(4439, '2022-05-03 00:00:00', '162.158.235.181', '2022-05-03 05:41:16', '2022-05-03 05:41:16'),
(4440, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 05:41:28', '2022-05-03 05:41:28'),
(4441, '2022-05-03 00:00:00', '172.70.218.174', '2022-05-03 05:42:08', '2022-05-03 05:42:08'),
(4442, '2022-05-03 00:00:00', '162.158.159.34', '2022-05-03 05:42:58', '2022-05-03 05:42:58'),
(4443, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 05:45:18', '2022-05-03 05:45:18'),
(4444, '2022-05-03 00:00:00', '172.70.218.236', '2022-05-03 05:45:50', '2022-05-03 05:45:50'),
(4445, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 05:47:33', '2022-05-03 05:47:33'),
(4446, '2022-05-03 00:00:00', '172.68.39.236', '2022-05-03 05:52:27', '2022-05-03 05:52:27'),
(4447, '2022-05-03 00:00:00', '172.70.162.240', '2022-05-03 06:14:27', '2022-05-03 06:14:27'),
(4448, '2022-05-03 00:00:00', '172.70.189.95', '2022-05-03 06:24:41', '2022-05-03 06:24:41'),
(4449, '2022-05-03 00:00:00', '172.70.162.188', '2022-05-03 06:40:35', '2022-05-03 06:40:35'),
(4450, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 06:43:34', '2022-05-03 06:43:34'),
(4451, '2022-05-03 00:00:00', '172.70.251.7', '2022-05-03 06:46:02', '2022-05-03 06:46:02'),
(4452, '2022-05-03 00:00:00', '172.70.162.12', '2022-05-03 06:52:26', '2022-05-03 06:52:26'),
(4453, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 06:56:57', '2022-05-03 06:56:57'),
(4454, '2022-05-03 00:00:00', '141.101.99.149', '2022-05-03 06:58:29', '2022-05-03 06:58:29'),
(4455, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 07:16:53', '2022-05-03 07:16:53'),
(4456, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 07:16:56', '2022-05-03 07:16:56'),
(4457, '2022-05-03 00:00:00', '162.158.159.54', '2022-05-03 07:22:15', '2022-05-03 07:22:15'),
(4458, '2022-05-03 00:00:00', '172.70.162.12', '2022-05-03 07:23:30', '2022-05-03 07:23:30'),
(4459, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 07:27:09', '2022-05-03 07:27:09'),
(4460, '2022-05-03 00:00:00', '162.158.170.214', '2022-05-03 07:32:33', '2022-05-03 07:32:33'),
(4461, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 07:34:11', '2022-05-03 07:34:11'),
(4462, '2022-05-03 00:00:00', '172.70.92.150', '2022-05-03 07:36:25', '2022-05-03 07:36:25'),
(4463, '2022-05-03 00:00:00', '172.70.188.36', '2022-05-03 07:41:46', '2022-05-03 07:41:46'),
(4464, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 07:43:01', '2022-05-03 07:43:01'),
(4465, '2022-05-03 00:00:00', '172.70.90.150', '2022-05-03 07:45:11', '2022-05-03 07:45:11'),
(4466, '2022-05-03 00:00:00', '172.70.162.22', '2022-05-03 07:46:06', '2022-05-03 07:46:06'),
(4467, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 07:51:00', '2022-05-03 07:51:00'),
(4468, '2022-05-03 00:00:00', '162.158.162.30', '2022-05-03 07:52:50', '2022-05-03 07:52:50'),
(4469, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 07:57:23', '2022-05-03 07:57:23'),
(4470, '2022-05-03 00:00:00', '172.70.92.156', '2022-05-03 07:58:24', '2022-05-03 07:58:24'),
(4471, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 08:06:41', '2022-05-03 08:06:41'),
(4472, '2022-05-03 00:00:00', '141.101.99.85', '2022-05-03 08:08:22', '2022-05-03 08:08:22'),
(4473, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 08:11:44', '2022-05-03 08:11:44'),
(4474, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 08:12:57', '2022-05-03 08:12:57'),
(4475, '2022-05-03 00:00:00', '172.70.162.22', '2022-05-03 08:14:46', '2022-05-03 08:14:46'),
(4476, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 08:22:04', '2022-05-03 08:22:04'),
(4477, '2022-05-03 00:00:00', '172.70.90.212', '2022-05-03 08:30:08', '2022-05-03 08:30:08'),
(4478, '2022-05-03 00:00:00', '172.70.162.240', '2022-05-03 08:32:47', '2022-05-03 08:32:47'),
(4479, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 08:41:09', '2022-05-03 08:41:09'),
(4480, '2022-05-03 00:00:00', '172.70.162.22', '2022-05-03 08:52:35', '2022-05-03 08:52:35'),
(4481, '2022-05-03 00:00:00', '162.158.227.236', '2022-05-03 08:57:22', '2022-05-03 08:57:22'),
(4482, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 09:06:02', '2022-05-03 09:06:02'),
(4483, '2022-05-03 00:00:00', '172.70.162.12', '2022-05-03 09:07:16', '2022-05-03 09:07:16'),
(4484, '2022-05-03 00:00:00', '172.70.162.188', '2022-05-03 09:29:11', '2022-05-03 09:29:11'),
(4485, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 09:41:12', '2022-05-03 09:41:12'),
(4486, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 09:52:25', '2022-05-03 09:52:25'),
(4487, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 09:57:20', '2022-05-03 09:57:20'),
(4488, '2022-05-03 00:00:00', '172.70.162.188', '2022-05-03 09:59:46', '2022-05-03 09:59:46'),
(4489, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 10:02:29', '2022-05-03 10:02:29'),
(4490, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 10:04:35', '2022-05-03 10:04:35'),
(4491, '2022-05-03 00:00:00', '162.158.162.112', '2022-05-03 10:11:47', '2022-05-03 10:11:47'),
(4492, '2022-05-03 00:00:00', '172.70.189.117', '2022-05-03 10:20:21', '2022-05-03 10:20:21'),
(4493, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 10:20:35', '2022-05-03 10:20:35'),
(4494, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 10:21:25', '2022-05-03 10:21:25'),
(4495, '2022-05-03 00:00:00', '172.69.239.146', '2022-05-03 10:21:49', '2022-05-03 10:21:49'),
(4496, '2022-05-03 00:00:00', '172.68.39.134', '2022-05-03 10:23:19', '2022-05-03 10:23:19'),
(4497, '2022-05-03 00:00:00', '172.70.162.240', '2022-05-03 10:24:11', '2022-05-03 10:24:11'),
(4498, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 10:24:52', '2022-05-03 10:24:52'),
(4499, '2022-05-03 00:00:00', '172.70.90.212', '2022-05-03 10:26:09', '2022-05-03 10:26:09'),
(4500, '2022-05-03 00:00:00', '172.70.218.28', '2022-05-03 10:27:04', '2022-05-03 10:27:04'),
(4501, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 10:27:58', '2022-05-03 10:27:58'),
(4502, '2022-05-03 00:00:00', '162.158.235.161', '2022-05-03 10:29:11', '2022-05-03 10:29:11'),
(4503, '2022-05-03 00:00:00', '141.101.107.138', '2022-05-03 10:35:55', '2022-05-03 10:35:55'),
(4504, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 10:39:43', '2022-05-03 10:39:43'),
(4505, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 10:39:57', '2022-05-03 10:39:57'),
(4506, '2022-05-03 00:00:00', '172.68.39.150', '2022-05-03 10:40:35', '2022-05-03 10:40:35'),
(4507, '2022-05-03 00:00:00', '162.158.227.224', '2022-05-03 10:40:52', '2022-05-03 10:40:52'),
(4508, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 10:41:46', '2022-05-03 10:41:46'),
(4509, '2022-05-03 00:00:00', '172.70.218.236', '2022-05-03 10:44:19', '2022-05-03 10:44:19'),
(4510, '2022-05-03 00:00:00', '172.70.189.59', '2022-05-03 10:47:12', '2022-05-03 10:47:12'),
(4511, '2022-05-03 00:00:00', '172.70.218.198', '2022-05-03 10:48:14', '2022-05-03 10:48:14'),
(4512, '2022-05-03 00:00:00', '172.70.90.212', '2022-05-03 10:48:38', '2022-05-03 10:48:38'),
(4513, '2022-05-03 00:00:00', '141.101.99.151', '2022-05-03 10:52:22', '2022-05-03 10:52:22'),
(4514, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 10:53:20', '2022-05-03 10:53:20'),
(4515, '2022-05-03 00:00:00', '172.70.188.36', '2022-05-03 10:56:13', '2022-05-03 10:56:13'),
(4516, '2022-05-03 00:00:00', '172.70.90.150', '2022-05-03 10:59:22', '2022-05-03 10:59:22'),
(4517, '2022-05-03 00:00:00', '162.158.235.43', '2022-05-03 11:04:17', '2022-05-03 11:04:17'),
(4518, '2022-05-03 00:00:00', '172.68.39.236', '2022-05-03 11:05:40', '2022-05-03 11:05:40'),
(4519, '2022-05-03 00:00:00', '141.101.99.151', '2022-05-03 11:05:40', '2022-05-03 11:05:40'),
(4520, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 11:06:50', '2022-05-03 11:06:50'),
(4521, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 11:07:30', '2022-05-03 11:07:30'),
(4522, '2022-05-03 00:00:00', '172.70.92.150', '2022-05-03 11:08:24', '2022-05-03 11:08:24'),
(4523, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 11:12:57', '2022-05-03 11:12:57'),
(4524, '2022-05-03 00:00:00', '172.70.162.240', '2022-05-03 11:18:27', '2022-05-03 11:18:27'),
(4525, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 11:20:19', '2022-05-03 11:20:19'),
(4526, '2022-05-03 00:00:00', '172.69.239.160', '2022-05-03 11:21:29', '2022-05-03 11:21:29'),
(4527, '2022-05-03 00:00:00', '172.68.39.150', '2022-05-03 11:24:01', '2022-05-03 11:24:01'),
(4528, '2022-05-03 00:00:00', '172.70.162.188', '2022-05-03 11:24:12', '2022-05-03 11:24:12'),
(4529, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 11:25:32', '2022-05-03 11:25:32'),
(4530, '2022-05-03 00:00:00', '172.70.92.150', '2022-05-03 11:26:50', '2022-05-03 11:26:50'),
(4531, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 11:27:05', '2022-05-03 11:27:05'),
(4532, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 11:29:58', '2022-05-03 11:29:58'),
(4533, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 11:30:01', '2022-05-03 11:30:01'),
(4534, '2022-05-03 00:00:00', '162.158.48.128', '2022-05-03 11:32:32', '2022-05-03 11:32:32'),
(4535, '2022-05-03 00:00:00', '162.158.227.240', '2022-05-03 11:33:22', '2022-05-03 11:33:22'),
(4536, '2022-05-03 00:00:00', '162.158.235.81', '2022-05-03 11:33:44', '2022-05-03 11:33:44'),
(4537, '2022-05-03 00:00:00', '162.158.159.82', '2022-05-03 11:34:28', '2022-05-03 11:34:28'),
(4538, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 11:37:43', '2022-05-03 11:37:43'),
(4539, '2022-05-03 00:00:00', '172.70.162.12', '2022-05-03 11:39:46', '2022-05-03 11:39:46'),
(4540, '2022-05-03 00:00:00', '172.70.147.42', '2022-05-03 11:42:52', '2022-05-03 11:42:52'),
(4541, '2022-05-03 00:00:00', '141.101.99.85', '2022-05-03 11:48:10', '2022-05-03 11:48:10'),
(4542, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 11:49:50', '2022-05-03 11:49:50'),
(4543, '2022-05-03 00:00:00', '162.158.163.43', '2022-05-03 11:49:54', '2022-05-03 11:49:54'),
(4544, '2022-05-03 00:00:00', '162.158.159.82', '2022-05-03 11:53:05', '2022-05-03 11:53:05'),
(4545, '2022-05-03 00:00:00', '172.68.39.236', '2022-05-03 11:58:53', '2022-05-03 11:58:53'),
(4546, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 12:00:55', '2022-05-03 12:00:55'),
(4547, '2022-05-03 00:00:00', '172.68.39.150', '2022-05-03 12:02:39', '2022-05-03 12:02:39'),
(4548, '2022-05-03 00:00:00', '172.68.39.134', '2022-05-03 12:04:01', '2022-05-03 12:04:01'),
(4549, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 12:04:57', '2022-05-03 12:04:57'),
(4550, '2022-05-03 00:00:00', '172.70.237.164', '2022-05-03 12:05:23', '2022-05-03 12:05:23'),
(4551, '2022-05-03 00:00:00', '172.70.237.156', '2022-05-03 12:07:43', '2022-05-03 12:07:43'),
(4552, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 12:07:53', '2022-05-03 12:07:53'),
(4553, '2022-05-03 00:00:00', '141.101.99.149', '2022-05-03 12:12:22', '2022-05-03 12:12:22'),
(4554, '2022-05-03 00:00:00', '172.70.162.188', '2022-05-03 12:14:41', '2022-05-03 12:14:41'),
(4555, '2022-05-03 00:00:00', '172.68.39.236', '2022-05-03 12:15:47', '2022-05-03 12:15:47'),
(4556, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 12:15:51', '2022-05-03 12:15:51'),
(4557, '2022-05-03 00:00:00', '172.70.90.150', '2022-05-03 12:18:07', '2022-05-03 12:18:07'),
(4558, '2022-05-03 00:00:00', '141.101.99.235', '2022-05-03 12:19:11', '2022-05-03 12:19:11'),
(4559, '2022-05-03 00:00:00', '162.158.159.44', '2022-05-03 12:21:55', '2022-05-03 12:21:55'),
(4560, '2022-05-03 00:00:00', '172.70.92.150', '2022-05-03 12:23:11', '2022-05-03 12:23:11'),
(4561, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 12:23:16', '2022-05-03 12:23:16'),
(4562, '2022-05-03 00:00:00', '172.70.147.42', '2022-05-03 12:24:39', '2022-05-03 12:24:39'),
(4563, '2022-05-03 00:00:00', '172.70.147.42', '2022-05-03 12:24:39', '2022-05-03 12:24:39'),
(4564, '2022-05-03 00:00:00', '172.70.162.188', '2022-05-03 12:27:40', '2022-05-03 12:27:40'),
(4565, '2022-05-03 00:00:00', '162.158.235.25', '2022-05-03 12:27:42', '2022-05-03 12:27:42'),
(4566, '2022-05-03 00:00:00', '141.101.107.138', '2022-05-03 12:28:52', '2022-05-03 12:28:52'),
(4567, '2022-05-03 00:00:00', '172.70.162.22', '2022-05-03 12:30:37', '2022-05-03 12:30:37'),
(4568, '2022-05-03 00:00:00', '172.68.39.236', '2022-05-03 12:31:47', '2022-05-03 12:31:47'),
(4569, '2022-05-03 00:00:00', '162.158.227.234', '2022-05-03 12:34:08', '2022-05-03 12:34:08'),
(4570, '2022-05-03 00:00:00', '172.70.162.12', '2022-05-03 12:35:20', '2022-05-03 12:35:20'),
(4571, '2022-05-03 00:00:00', '172.68.39.150', '2022-05-03 12:35:29', '2022-05-03 12:35:29'),
(4572, '2022-05-03 00:00:00', '172.70.237.158', '2022-05-03 12:35:35', '2022-05-03 12:35:35'),
(4573, '2022-05-03 00:00:00', '172.70.218.198', '2022-05-03 12:36:11', '2022-05-03 12:36:11'),
(4574, '2022-05-03 00:00:00', '162.158.227.236', '2022-05-03 12:36:21', '2022-05-03 12:36:21'),
(4575, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 12:36:45', '2022-05-03 12:36:45'),
(4576, '2022-05-03 00:00:00', '172.70.142.228', '2022-05-03 12:48:58', '2022-05-03 12:48:58'),
(4577, '2022-05-03 00:00:00', '141.101.107.160', '2022-05-03 12:54:34', '2022-05-03 12:54:34'),
(4578, '2022-05-03 00:00:00', '172.70.188.192', '2022-05-03 12:54:47', '2022-05-03 12:54:47'),
(4579, '2022-05-03 00:00:00', '172.70.237.158', '2022-05-03 12:55:34', '2022-05-03 12:55:34'),
(4580, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 12:57:41', '2022-05-03 12:57:41'),
(4581, '2022-05-03 00:00:00', '172.70.142.228', '2022-05-03 13:00:32', '2022-05-03 13:00:32'),
(4582, '2022-05-03 00:00:00', '162.158.159.44', '2022-05-03 13:01:28', '2022-05-03 13:01:28'),
(4583, '2022-05-03 00:00:00', '172.70.218.236', '2022-05-03 13:17:26', '2022-05-03 13:17:26'),
(4584, '2022-05-03 00:00:00', '172.70.218.198', '2022-05-03 13:18:32', '2022-05-03 13:18:32'),
(4585, '2022-05-03 00:00:00', '172.70.135.201', '2022-05-03 13:36:24', '2022-05-03 13:36:24'),
(4586, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 13:48:46', '2022-05-03 13:48:46'),
(4587, '2022-05-03 00:00:00', '172.70.147.34', '2022-05-03 13:58:27', '2022-05-03 13:58:27'),
(4588, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 14:02:20', '2022-05-03 14:02:20'),
(4589, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 14:02:54', '2022-05-03 14:02:54'),
(4590, '2022-05-03 00:00:00', '172.70.92.150', '2022-05-03 14:03:17', '2022-05-03 14:03:17'),
(4591, '2022-05-03 00:00:00', '172.70.90.212', '2022-05-03 14:10:32', '2022-05-03 14:10:32'),
(4592, '2022-05-03 00:00:00', '172.70.134.140', '2022-05-03 14:10:41', '2022-05-03 14:10:41'),
(4593, '2022-05-03 00:00:00', '172.70.85.154', '2022-05-03 14:11:17', '2022-05-03 14:11:17'),
(4594, '2022-05-03 00:00:00', '172.70.90.6', '2022-05-03 14:16:00', '2022-05-03 14:16:00'),
(4595, '2022-05-03 00:00:00', '172.70.85.150', '2022-05-03 14:16:34', '2022-05-03 14:16:34'),
(4596, '2022-05-03 00:00:00', '172.70.162.12', '2022-05-03 14:17:48', '2022-05-03 14:17:48'),
(4597, '2022-05-03 00:00:00', '172.68.39.236', '2022-05-03 14:18:30', '2022-05-03 14:18:30'),
(4598, '2022-05-03 00:00:00', '141.101.99.151', '2022-05-03 14:18:31', '2022-05-03 14:18:31'),
(4599, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 14:19:04', '2022-05-03 14:19:04'),
(4600, '2022-05-03 00:00:00', '172.68.39.216', '2022-05-03 14:20:32', '2022-05-03 14:20:32'),
(4601, '2022-05-03 00:00:00', '141.101.107.138', '2022-05-03 14:26:14', '2022-05-03 14:26:14'),
(4602, '2022-05-03 00:00:00', '162.158.159.120', '2022-05-03 14:27:53', '2022-05-03 14:27:53'),
(4603, '2022-05-03 00:00:00', '141.101.99.85', '2022-05-03 14:29:27', '2022-05-03 14:29:27'),
(4604, '2022-05-03 00:00:00', '172.70.90.150', '2022-05-03 14:31:22', '2022-05-03 14:31:22'),
(4605, '2022-05-03 00:00:00', '172.70.85.40', '2022-05-03 14:32:18', '2022-05-03 14:32:18'),
(4606, '2022-05-03 00:00:00', '172.70.90.172', '2022-05-03 14:35:25', '2022-05-03 14:35:25'),
(4607, '2022-05-03 00:00:00', '162.158.162.96', '2022-05-03 14:39:39', '2022-05-03 14:39:39'),
(4608, '2022-05-03 00:00:00', '141.101.98.242', '2022-05-03 14:39:43', '2022-05-03 14:39:43'),
(4609, '2022-05-03 00:00:00', '172.70.162.240', '2022-05-03 14:40:18', '2022-05-03 14:40:18'),
(4610, '2022-05-03 00:00:00', '172.70.189.117', '2022-05-03 14:45:27', '2022-05-03 14:45:27'),
(4611, '2022-05-03 00:00:00', '172.70.188.192', '2022-05-03 14:51:05', '2022-05-03 14:51:05'),
(4612, '2022-05-03 00:00:00', '172.70.135.81', '2022-05-03 15:16:04', '2022-05-03 15:16:04'),
(4613, '2022-05-03 00:00:00', '141.101.99.11', '2022-05-03 15:33:50', '2022-05-03 15:33:50'),
(4614, '2022-05-03 00:00:00', '172.70.206.204', '2022-05-03 16:58:14', '2022-05-03 16:58:14'),
(4615, '2022-05-03 00:00:00', '172.70.142.34', '2022-05-03 17:15:50', '2022-05-03 17:15:50'),
(4616, '2022-05-03 00:00:00', '172.70.85.34', '2022-05-03 20:51:24', '2022-05-03 20:51:24'),
(4617, '2022-05-03 00:00:00', '141.101.105.92', '2022-05-03 21:14:54', '2022-05-03 21:14:54'),
(4618, '2022-05-03 00:00:00', '162.158.78.174', '2022-05-03 22:41:58', '2022-05-03 22:41:58'),
(4619, '2022-05-03 00:00:00', '172.70.134.8', '2022-05-03 22:47:25', '2022-05-03 22:47:25'),
(4620, '2022-05-03 00:00:00', '172.70.135.131', '2022-05-03 22:52:52', '2022-05-03 22:52:52'),
(4621, '2022-05-03 00:00:00', '172.70.134.176', '2022-05-03 23:03:47', '2022-05-03 23:03:47'),
(4622, '2022-05-03 00:00:00', '172.70.175.117', '2022-05-03 23:09:14', '2022-05-03 23:09:14'),
(4623, '2022-05-03 00:00:00', '172.70.34.214', '2022-05-03 23:14:41', '2022-05-03 23:14:41'),
(4624, '2022-05-03 00:00:00', '172.70.35.17', '2022-05-03 23:31:24', '2022-05-03 23:31:24'),
(4625, '2022-05-03 00:00:00', '172.70.34.108', '2022-05-03 23:42:19', '2022-05-03 23:42:19'),
(4626, '2022-05-03 00:00:00', '172.70.135.29', '2022-05-03 23:47:46', '2022-05-03 23:47:46'),
(4627, '2022-05-04 00:00:00', '172.70.246.180', '2022-05-04 00:38:47', '2022-05-04 00:38:47'),
(4628, '2022-05-04 00:00:00', '172.70.142.32', '2022-05-04 02:02:02', '2022-05-04 02:02:02'),
(4629, '2022-05-04 00:00:00', '162.158.163.155', '2022-05-04 02:16:34', '2022-05-04 02:16:34'),
(4630, '2022-05-04 00:00:00', '172.70.142.224', '2022-05-04 02:31:47', '2022-05-04 02:31:47'),
(4631, '2022-05-04 00:00:00', '162.158.170.194', '2022-05-04 02:36:53', '2022-05-04 02:36:53'),
(4632, '2022-05-04 00:00:00', '172.70.38.72', '2022-05-04 02:41:53', '2022-05-04 02:41:53'),
(4633, '2022-05-04 00:00:00', '172.70.142.34', '2022-05-04 02:42:02', '2022-05-04 02:42:02'),
(4634, '2022-05-04 00:00:00', '172.70.189.59', '2022-05-04 03:07:16', '2022-05-04 03:07:16'),
(4635, '2022-05-04 00:00:00', '141.101.105.78', '2022-05-04 06:28:38', '2022-05-04 06:28:38'),
(4636, '2022-05-04 00:00:00', '141.101.104.71', '2022-05-04 06:28:38', '2022-05-04 06:28:38'),
(4637, '2022-05-04 00:00:00', '172.70.100.17', '2022-05-04 06:41:47', '2022-05-04 06:41:47'),
(4638, '2022-05-04 00:00:00', '172.70.178.182', '2022-05-04 06:42:06', '2022-05-04 06:42:06'),
(4639, '2022-05-04 00:00:00', '172.70.130.146', '2022-05-04 06:42:07', '2022-05-04 06:42:07'),
(4640, '2022-05-04 00:00:00', '172.70.178.104', '2022-05-04 06:42:09', '2022-05-04 06:42:09'),
(4641, '2022-05-04 00:00:00', '172.70.126.238', '2022-05-04 06:42:10', '2022-05-04 06:42:10'),
(4642, '2022-05-04 00:00:00', '172.70.126.190', '2022-05-04 06:42:11', '2022-05-04 06:42:11'),
(4643, '2022-05-04 00:00:00', '172.70.85.34', '2022-05-04 07:49:56', '2022-05-04 07:49:56'),
(4644, '2022-05-04 00:00:00', '172.70.85.150', '2022-05-04 08:03:41', '2022-05-04 08:03:41'),
(4645, '2022-05-04 00:00:00', '172.70.92.160', '2022-05-04 08:10:07', '2022-05-04 08:10:07'),
(4646, '2022-05-04 00:00:00', '162.158.90.166', '2022-05-04 08:26:39', '2022-05-04 08:26:39'),
(4647, '2022-05-04 00:00:00', '172.70.85.150', '2022-05-04 08:31:08', '2022-05-04 08:31:08'),
(4648, '2022-05-04 00:00:00', '172.70.188.36', '2022-05-04 08:43:31', '2022-05-04 08:43:31'),
(4649, '2022-05-04 00:00:00', '141.101.77.200', '2022-05-04 09:15:35', '2022-05-04 09:15:35'),
(4650, '2022-05-04 00:00:00', '172.68.10.182', '2022-05-04 09:15:44', '2022-05-04 09:15:44'),
(4651, '2022-05-04 00:00:00', '172.70.162.188', '2022-05-04 10:26:43', '2022-05-04 10:26:43'),
(4652, '2022-05-04 00:00:00', '172.70.135.15', '2022-05-04 10:32:50', '2022-05-04 10:32:50'),
(4653, '2022-05-04 00:00:00', '162.158.163.21', '2022-05-04 10:45:04', '2022-05-04 10:45:04'),
(4654, '2022-05-04 00:00:00', '172.70.162.240', '2022-05-04 11:00:18', '2022-05-04 11:00:18'),
(4655, '2022-05-04 00:00:00', '172.70.85.150', '2022-05-04 11:01:08', '2022-05-04 11:01:08'),
(4656, '2022-05-04 00:00:00', '172.70.175.199', '2022-05-04 12:48:10', '2022-05-04 12:48:10'),
(4657, '2022-05-04 00:00:00', '172.70.189.59', '2022-05-04 13:44:30', '2022-05-04 13:44:30'),
(4658, '2022-05-04 00:00:00', '172.70.38.26', '2022-05-04 13:52:24', '2022-05-04 13:52:24'),
(4659, '2022-05-04 00:00:00', '172.70.188.150', '2022-05-04 14:01:57', '2022-05-04 14:01:57'),
(4660, '2022-05-04 00:00:00', '108.162.246.219', '2022-05-04 14:17:42', '2022-05-04 14:17:42'),
(4661, '2022-05-04 00:00:00', '172.70.90.212', '2022-05-04 14:46:11', '2022-05-04 14:46:11'),
(4662, '2022-05-04 00:00:00', '172.70.242.128', '2022-05-04 16:07:38', '2022-05-04 16:07:38'),
(4663, '2022-05-04 00:00:00', '172.70.189.95', '2022-05-04 17:12:46', '2022-05-04 17:12:46'),
(4664, '2022-05-04 00:00:00', '172.70.142.228', '2022-05-04 17:28:18', '2022-05-04 17:28:18'),
(4665, '2022-05-04 00:00:00', '172.70.147.48', '2022-05-04 17:33:26', '2022-05-04 17:33:26'),
(4666, '2022-05-04 00:00:00', '162.158.163.41', '2022-05-04 17:38:37', '2022-05-04 17:38:37'),
(4667, '2022-05-04 00:00:00', '162.158.162.218', '2022-05-04 23:20:51', '2022-05-04 23:20:51'),
(4668, '2022-05-05 00:00:00', '172.70.189.117', '2022-05-05 00:19:41', '2022-05-05 00:19:41'),
(4669, '2022-05-05 00:00:00', '162.158.170.194', '2022-05-05 00:52:47', '2022-05-05 00:52:47'),
(4670, '2022-05-05 00:00:00', '162.158.162.172', '2022-05-05 01:57:51', '2022-05-05 01:57:51'),
(4671, '2022-05-05 00:00:00', '162.158.222.118', '2022-05-05 02:49:36', '2022-05-05 02:49:36'),
(4672, '2022-05-05 00:00:00', '172.70.92.150', '2022-05-05 03:26:31', '2022-05-05 03:26:31'),
(4673, '2022-05-05 00:00:00', '162.158.162.156', '2022-05-05 05:00:43', '2022-05-05 05:00:43'),
(4674, '2022-05-05 00:00:00', '172.70.142.34', '2022-05-05 05:11:01', '2022-05-05 05:11:01'),
(4675, '2022-05-05 00:00:00', '172.70.188.150', '2022-05-05 05:16:12', '2022-05-05 05:16:12'),
(4676, '2022-05-05 00:00:00', '172.69.34.21', '2022-05-05 05:20:50', '2022-05-05 05:20:50'),
(4677, '2022-05-05 00:00:00', '172.70.92.160', '2022-05-05 05:52:35', '2022-05-05 05:52:35'),
(4678, '2022-05-05 00:00:00', '141.101.99.85', '2022-05-05 06:48:52', '2022-05-05 06:48:52'),
(4679, '2022-05-05 00:00:00', '172.70.211.157', '2022-05-05 07:30:29', '2022-05-05 07:30:29'),
(4680, '2022-05-05 00:00:00', '172.70.206.204', '2022-05-05 07:30:47', '2022-05-05 07:30:47'),
(4681, '2022-05-05 00:00:00', '172.70.85.40', '2022-05-05 09:30:31', '2022-05-05 09:30:31'),
(4682, '2022-05-05 00:00:00', '172.70.90.6', '2022-05-05 09:37:52', '2022-05-05 09:37:52'),
(4683, '2022-05-05 00:00:00', '162.158.107.51', '2022-05-05 10:21:05', '2022-05-05 10:21:05'),
(4684, '2022-05-05 00:00:00', '172.70.162.12', '2022-05-05 10:29:13', '2022-05-05 10:29:13'),
(4685, '2022-05-05 00:00:00', '162.158.235.181', '2022-05-05 10:38:08', '2022-05-05 10:38:08'),
(4686, '2022-05-05 00:00:00', '172.70.147.42', '2022-05-05 10:43:12', '2022-05-05 10:43:12'),
(4687, '2022-05-05 00:00:00', '172.70.188.90', '2022-05-05 10:53:30', '2022-05-05 10:53:30'),
(4688, '2022-05-05 00:00:00', '172.70.147.42', '2022-05-05 11:03:59', '2022-05-05 11:03:59'),
(4689, '2022-05-05 00:00:00', '172.70.92.156', '2022-05-05 11:40:15', '2022-05-05 11:40:15'),
(4690, '2022-05-05 00:00:00', '162.158.162.88', '2022-05-05 11:50:52', '2022-05-05 11:50:52'),
(4691, '2022-05-05 00:00:00', '172.70.162.188', '2022-05-05 12:54:53', '2022-05-05 12:54:53'),
(4692, '2022-05-05 00:00:00', '172.70.162.240', '2022-05-05 12:57:57', '2022-05-05 12:57:57'),
(4693, '2022-05-05 00:00:00', '172.70.85.150', '2022-05-05 12:58:52', '2022-05-05 12:58:52'),
(4694, '2022-05-05 00:00:00', '172.70.162.12', '2022-05-05 13:01:33', '2022-05-05 13:01:33'),
(4695, '2022-05-05 00:00:00', '172.70.147.48', '2022-05-05 13:04:04', '2022-05-05 13:04:04'),
(4696, '2022-05-05 00:00:00', '172.70.85.154', '2022-05-05 13:04:18', '2022-05-05 13:04:18'),
(4697, '2022-05-05 00:00:00', '172.70.90.150', '2022-05-05 13:22:26', '2022-05-05 13:22:26'),
(4698, '2022-05-05 00:00:00', '172.70.175.175', '2022-05-05 14:51:18', '2022-05-05 14:51:18'),
(4699, '2022-05-05 00:00:00', '172.70.142.42', '2022-05-05 15:52:15', '2022-05-05 15:52:15'),
(4700, '2022-05-05 00:00:00', '172.70.251.51', '2022-05-05 16:52:57', '2022-05-05 16:52:57'),
(4701, '2022-05-05 00:00:00', '172.70.189.95', '2022-05-05 16:53:21', '2022-05-05 16:53:21'),
(4702, '2022-05-05 00:00:00', '172.70.214.118', '2022-05-05 17:09:59', '2022-05-05 17:09:59'),
(4703, '2022-05-05 00:00:00', '172.70.142.218', '2022-05-05 18:27:25', '2022-05-05 18:27:25'),
(4704, '2022-05-05 00:00:00', '172.70.189.95', '2022-05-05 18:38:39', '2022-05-05 18:38:39'),
(4705, '2022-05-05 00:00:00', '172.70.142.228', '2022-05-05 18:49:17', '2022-05-05 18:49:17'),
(4706, '2022-05-05 00:00:00', '172.71.10.6', '2022-05-05 19:05:52', '2022-05-05 19:05:52'),
(4707, '2022-05-05 00:00:00', '162.158.50.73', '2022-05-05 20:19:19', '2022-05-05 20:19:19'),
(4708, '2022-05-05 00:00:00', '172.70.92.160', '2022-05-05 21:29:48', '2022-05-05 21:29:48'),
(4709, '2022-05-05 00:00:00', '172.70.142.34', '2022-05-05 21:53:03', '2022-05-05 21:53:03'),
(4710, '2022-05-05 00:00:00', '172.70.92.150', '2022-05-05 22:14:12', '2022-05-05 22:14:12'),
(4711, '2022-05-05 00:00:00', '162.158.129.144', '2022-05-05 22:37:05', '2022-05-05 22:37:05'),
(4712, '2022-05-05 00:00:00', '162.158.170.78', '2022-05-05 23:37:42', '2022-05-05 23:37:42'),
(4713, '2022-05-06 00:00:00', '141.101.105.56', '2022-05-06 00:09:20', '2022-05-06 00:09:20'),
(4714, '2022-05-06 00:00:00', '141.101.105.160', '2022-05-06 00:09:21', '2022-05-06 00:09:21'),
(4715, '2022-05-06 00:00:00', '141.101.76.74', '2022-05-06 00:09:21', '2022-05-06 00:09:21'),
(4716, '2022-05-06 00:00:00', '141.101.104.15', '2022-05-06 00:09:22', '2022-05-06 00:09:22'),
(4717, '2022-05-06 00:00:00', '162.158.170.196', '2022-05-06 00:44:24', '2022-05-06 00:44:24'),
(4718, '2022-05-06 00:00:00', '172.70.142.34', '2022-05-06 01:28:37', '2022-05-06 01:28:37'),
(4719, '2022-05-06 00:00:00', '162.158.134.78', '2022-05-06 02:00:04', '2022-05-06 02:00:04'),
(4720, '2022-05-06 00:00:00', '172.70.142.32', '2022-05-06 02:47:15', '2022-05-06 02:47:15'),
(4721, '2022-05-06 00:00:00', '108.162.229.70', '2022-05-06 05:22:20', '2022-05-06 05:22:20'),
(4722, '2022-05-06 00:00:00', '108.162.229.70', '2022-05-06 05:39:21', '2022-05-06 05:39:21'),
(4723, '2022-05-06 00:00:00', '172.70.174.140', '2022-05-06 05:41:13', '2022-05-06 05:41:13'),
(4724, '2022-05-06 00:00:00', '108.162.229.70', '2022-05-06 05:56:22', '2022-05-06 05:56:22'),
(4725, '2022-05-06 00:00:00', '172.70.90.6', '2022-05-06 06:10:12', '2022-05-06 06:10:12'),
(4726, '2022-05-06 00:00:00', '162.158.159.54', '2022-05-06 06:10:50', '2022-05-06 06:10:50'),
(4727, '2022-05-06 00:00:00', '141.101.107.160', '2022-05-06 06:11:33', '2022-05-06 06:11:33'),
(4728, '2022-05-06 00:00:00', '172.70.90.172', '2022-05-06 06:12:18', '2022-05-06 06:12:18'),
(4729, '2022-05-06 00:00:00', '108.162.229.64', '2022-05-06 06:13:57', '2022-05-06 06:13:57'),
(4730, '2022-05-06 00:00:00', '172.70.90.150', '2022-05-06 06:14:43', '2022-05-06 06:14:43'),
(4731, '2022-05-06 00:00:00', '172.70.85.40', '2022-05-06 06:19:07', '2022-05-06 06:19:07'),
(4732, '2022-05-06 00:00:00', '172.70.90.6', '2022-05-06 06:38:47', '2022-05-06 06:38:47'),
(4733, '2022-05-06 00:00:00', '162.158.159.34', '2022-05-06 10:27:08', '2022-05-06 10:27:08'),
(4734, '2022-05-06 00:00:00', '172.70.162.12', '2022-05-06 10:39:04', '2022-05-06 10:39:04'),
(4735, '2022-05-06 00:00:00', '141.101.99.235', '2022-05-06 10:45:29', '2022-05-06 10:45:29'),
(4736, '2022-05-06 00:00:00', '172.70.90.212', '2022-05-06 11:09:35', '2022-05-06 11:09:35'),
(4737, '2022-05-06 00:00:00', '172.69.33.10', '2022-05-06 11:44:26', '2022-05-06 11:44:26'),
(4738, '2022-05-06 00:00:00', '172.70.210.54', '2022-05-06 11:44:28', '2022-05-06 11:44:28'),
(4739, '2022-05-06 00:00:00', '172.70.188.192', '2022-05-06 12:11:12', '2022-05-06 12:11:12'),
(4740, '2022-05-06 00:00:00', '172.70.147.32', '2022-05-06 12:16:54', '2022-05-06 12:16:54'),
(4741, '2022-05-06 00:00:00', '172.70.142.48', '2022-05-06 12:22:47', '2022-05-06 12:22:47'),
(4742, '2022-05-06 00:00:00', '172.70.147.34', '2022-05-06 12:28:42', '2022-05-06 12:28:42'),
(4743, '2022-05-06 00:00:00', '162.158.170.56', '2022-05-06 12:40:40', '2022-05-06 12:40:40'),
(4744, '2022-05-06 00:00:00', '162.158.162.106', '2022-05-06 12:58:59', '2022-05-06 12:58:59'),
(4745, '2022-05-06 00:00:00', '172.70.189.95', '2022-05-06 13:05:11', '2022-05-06 13:05:11'),
(4746, '2022-05-06 00:00:00', '172.70.162.188', '2022-05-06 14:24:00', '2022-05-06 14:24:00'),
(4747, '2022-05-06 00:00:00', '172.70.134.140', '2022-05-06 14:50:10', '2022-05-06 14:50:10'),
(4748, '2022-05-06 00:00:00', '172.70.35.39', '2022-05-06 14:50:19', '2022-05-06 14:50:19'),
(4749, '2022-05-06 00:00:00', '172.70.162.12', '2022-05-06 15:04:52', '2022-05-06 15:04:52'),
(4750, '2022-05-06 00:00:00', '172.70.85.34', '2022-05-06 15:05:45', '2022-05-06 15:05:45'),
(4751, '2022-05-06 00:00:00', '172.70.90.6', '2022-05-06 15:09:27', '2022-05-06 15:09:27'),
(4752, '2022-05-06 00:00:00', '172.70.90.172', '2022-05-06 15:10:32', '2022-05-06 15:10:32'),
(4753, '2022-05-06 00:00:00', '172.70.162.22', '2022-05-06 15:12:18', '2022-05-06 15:12:18'),
(4754, '2022-05-06 00:00:00', '172.70.85.34', '2022-05-06 15:17:53', '2022-05-06 15:17:53'),
(4755, '2022-05-06 00:00:00', '172.70.162.12', '2022-05-06 15:20:01', '2022-05-06 15:20:01'),
(4756, '2022-05-06 00:00:00', '172.70.90.6', '2022-05-06 15:23:11', '2022-05-06 15:23:11'),
(4757, '2022-05-06 00:00:00', '172.70.90.172', '2022-05-06 15:33:13', '2022-05-06 15:33:13'),
(4758, '2022-05-06 00:00:00', '172.70.85.150', '2022-05-06 15:36:11', '2022-05-06 15:36:11'),
(4759, '2022-05-06 00:00:00', '172.70.85.34', '2022-05-06 15:37:19', '2022-05-06 15:37:19'),
(4760, '2022-05-06 00:00:00', '172.70.162.240', '2022-05-06 15:41:27', '2022-05-06 15:41:27'),
(4761, '2022-05-06 00:00:00', '172.70.162.188', '2022-05-06 15:45:03', '2022-05-06 15:45:03'),
(4762, '2022-05-06 00:00:00', '172.70.90.212', '2022-05-06 15:46:42', '2022-05-06 15:46:42'),
(4763, '2022-05-06 00:00:00', '172.70.162.12', '2022-05-06 15:56:17', '2022-05-06 15:56:17'),
(4764, '2022-05-06 00:00:00', '172.70.90.150', '2022-05-06 15:58:46', '2022-05-06 15:58:46'),
(4765, '2022-05-06 00:00:00', '172.70.90.172', '2022-05-06 16:05:34', '2022-05-06 16:05:34'),
(4766, '2022-05-06 00:00:00', '172.70.90.6', '2022-05-06 16:08:24', '2022-05-06 16:08:24'),
(4767, '2022-05-06 00:00:00', '141.101.99.151', '2022-05-06 16:09:36', '2022-05-06 16:09:36'),
(4768, '2022-05-06 00:00:00', '172.70.174.140', '2022-05-06 16:12:05', '2022-05-06 16:12:05'),
(4769, '2022-05-06 00:00:00', '162.158.162.94', '2022-05-06 17:15:57', '2022-05-06 17:15:57'),
(4770, '2022-05-06 00:00:00', '172.70.142.224', '2022-05-06 17:22:08', '2022-05-06 17:22:08'),
(4771, '2022-05-06 00:00:00', '172.70.178.88', '2022-05-06 17:59:55', '2022-05-06 17:59:55'),
(4772, '2022-05-06 00:00:00', '172.69.33.198', '2022-05-06 18:15:51', '2022-05-06 18:15:51'),
(4773, '2022-05-06 00:00:00', '172.70.130.116', '2022-05-06 18:22:26', '2022-05-06 18:22:26'),
(4774, '2022-05-06 00:00:00', '172.70.142.228', '2022-05-06 18:25:33', '2022-05-06 18:25:33'),
(4775, '2022-05-06 00:00:00', '172.70.189.117', '2022-05-06 18:31:51', '2022-05-06 18:31:51'),
(4776, '2022-05-06 00:00:00', '162.158.170.56', '2022-05-06 18:38:05', '2022-05-06 18:38:05'),
(4777, '2022-05-06 00:00:00', '172.70.178.206', '2022-05-06 18:44:57', '2022-05-06 18:44:57'),
(4778, '2022-05-06 00:00:00', '162.158.163.217', '2022-05-06 18:50:32', '2022-05-06 18:50:32'),
(4779, '2022-05-06 00:00:00', '172.70.179.9', '2022-05-06 19:41:10', '2022-05-06 19:41:10'),
(4780, '2022-05-06 00:00:00', '108.162.216.16', '2022-05-06 21:11:11', '2022-05-06 21:11:11'),
(4781, '2022-05-06 00:00:00', '162.158.159.44', '2022-05-06 22:39:20', '2022-05-06 22:39:20'),
(4782, '2022-05-06 00:00:00', '172.70.142.42', '2022-05-06 23:18:04', '2022-05-06 23:18:04'),
(4783, '2022-05-06 00:00:00', '172.70.147.48', '2022-05-06 23:38:06', '2022-05-06 23:38:06'),
(4784, '2022-05-07 00:00:00', '162.158.170.88', '2022-05-07 00:21:35', '2022-05-07 00:21:35'),
(4785, '2022-05-07 00:00:00', '172.70.92.150', '2022-05-07 00:49:17', '2022-05-07 00:49:17'),
(4786, '2022-05-07 00:00:00', '172.70.189.59', '2022-05-07 00:58:37', '2022-05-07 00:58:37'),
(4787, '2022-05-07 00:00:00', '162.158.163.155', '2022-05-07 01:56:03', '2022-05-07 01:56:03'),
(4788, '2022-05-07 00:00:00', '162.158.162.206', '2022-05-07 05:40:13', '2022-05-07 05:40:13'),
(4789, '2022-05-07 00:00:00', '172.70.142.32', '2022-05-07 06:26:10', '2022-05-07 06:26:10'),
(4790, '2022-05-07 00:00:00', '172.70.92.150', '2022-05-07 06:36:13', '2022-05-07 06:36:13'),
(4791, '2022-05-07 00:00:00', '162.158.163.155', '2022-05-07 07:10:29', '2022-05-07 07:10:29'),
(4792, '2022-05-07 00:00:00', '172.70.92.160', '2022-05-07 07:20:29', '2022-05-07 07:20:29'),
(4793, '2022-05-07 00:00:00', '172.70.189.95', '2022-05-07 08:01:47', '2022-05-07 08:01:47'),
(4794, '2022-05-07 00:00:00', '172.70.189.59', '2022-05-07 08:18:51', '2022-05-07 08:18:51'),
(4795, '2022-05-07 00:00:00', '172.70.188.36', '2022-05-07 08:26:50', '2022-05-07 08:26:50'),
(4796, '2022-05-07 00:00:00', '172.70.92.150', '2022-05-07 09:08:17', '2022-05-07 09:08:17'),
(4797, '2022-05-07 00:00:00', '172.70.142.42', '2022-05-07 09:16:03', '2022-05-07 09:16:03'),
(4798, '2022-05-07 00:00:00', '162.158.163.41', '2022-05-07 09:33:14', '2022-05-07 09:33:14'),
(4799, '2022-05-07 00:00:00', '172.70.188.192', '2022-05-07 13:52:47', '2022-05-07 13:52:47'),
(4800, '2022-05-07 00:00:00', '108.162.246.219', '2022-05-07 14:35:30', '2022-05-07 14:35:30'),
(4801, '2022-05-07 00:00:00', '172.70.142.224', '2022-05-07 15:09:48', '2022-05-07 15:09:48'),
(4802, '2022-05-07 00:00:00', '172.70.142.218', '2022-05-07 15:49:58', '2022-05-07 15:49:58'),
(4803, '2022-05-07 00:00:00', '162.158.163.227', '2022-05-07 16:30:46', '2022-05-07 16:30:46'),
(4804, '2022-05-07 00:00:00', '162.158.163.175', '2022-05-07 16:38:17', '2022-05-07 16:38:17'),
(4805, '2022-05-07 00:00:00', '162.158.22.196', '2022-05-07 17:15:32', '2022-05-07 17:15:32'),
(4806, '2022-05-07 00:00:00', '141.101.77.200', '2022-05-07 21:39:34', '2022-05-07 21:39:34'),
(4807, '2022-05-08 00:00:00', '172.70.189.137', '2022-05-08 00:42:43', '2022-05-08 00:42:43'),
(4808, '2022-05-08 00:00:00', '172.70.147.48', '2022-05-08 00:58:32', '2022-05-08 00:58:32'),
(4809, '2022-05-08 00:00:00', '162.158.163.217', '2022-05-08 01:04:03', '2022-05-08 01:04:03'),
(4810, '2022-05-08 00:00:00', '172.70.142.34', '2022-05-08 01:14:40', '2022-05-08 01:14:40'),
(4811, '2022-05-08 00:00:00', '172.70.188.36', '2022-05-08 01:20:13', '2022-05-08 01:20:13'),
(4812, '2022-05-08 00:00:00', '172.70.189.59', '2022-05-08 01:36:13', '2022-05-08 01:36:13'),
(4813, '2022-05-08 00:00:00', '172.68.254.174', '2022-05-08 02:02:40', '2022-05-08 02:02:40'),
(4814, '2022-05-08 00:00:00', '162.158.170.56', '2022-05-08 02:09:05', '2022-05-08 02:09:05'),
(4815, '2022-05-08 00:00:00', '172.70.242.210', '2022-05-08 02:22:19', '2022-05-08 02:22:19'),
(4816, '2022-05-08 00:00:00', '172.70.251.51', '2022-05-08 02:22:20', '2022-05-08 02:22:20'),
(4817, '2022-05-08 00:00:00', '172.69.33.112', '2022-05-08 06:36:59', '2022-05-08 06:36:59'),
(4818, '2022-05-08 00:00:00', '172.70.142.224', '2022-05-08 07:00:44', '2022-05-08 07:00:44'),
(4819, '2022-05-08 00:00:00', '172.70.92.156', '2022-05-08 07:08:24', '2022-05-08 07:08:24'),
(4820, '2022-05-08 00:00:00', '162.158.170.214', '2022-05-08 07:16:12', '2022-05-08 07:16:12'),
(4821, '2022-05-08 00:00:00', '162.158.170.254', '2022-05-08 07:40:15', '2022-05-08 07:40:15'),
(4822, '2022-05-08 00:00:00', '162.158.163.217', '2022-05-08 08:39:31', '2022-05-08 08:39:31'),
(4823, '2022-05-08 00:00:00', '141.101.69.69', '2022-05-08 08:46:51', '2022-05-08 08:46:51'),
(4824, '2022-05-08 00:00:00', '172.70.126.144', '2022-05-08 10:58:10', '2022-05-08 10:58:10'),
(4825, '2022-05-08 00:00:00', '162.158.163.11', '2022-05-08 12:42:14', '2022-05-08 12:42:14'),
(4826, '2022-05-08 00:00:00', '172.70.142.218', '2022-05-08 14:37:28', '2022-05-08 14:37:28'),
(4827, '2022-05-08 00:00:00', '172.70.92.150', '2022-05-08 14:47:24', '2022-05-08 14:47:24'),
(4828, '2022-05-08 00:00:00', '172.70.142.224', '2022-05-08 15:17:26', '2022-05-08 15:17:26'),
(4829, '2022-05-08 00:00:00', '172.70.142.48', '2022-05-08 15:37:15', '2022-05-08 15:37:15'),
(4830, '2022-05-08 00:00:00', '172.70.147.34', '2022-05-08 15:47:15', '2022-05-08 15:47:15'),
(4831, '2022-05-08 00:00:00', '172.70.147.42', '2022-05-08 16:15:40', '2022-05-08 16:15:40'),
(4832, '2022-05-08 00:00:00', '172.70.142.34', '2022-05-08 17:37:22', '2022-05-08 17:37:22'),
(4833, '2022-05-08 00:00:00', '172.70.147.32', '2022-05-08 17:43:21', '2022-05-08 17:43:21'),
(4834, '2022-05-08 00:00:00', '162.158.163.9', '2022-05-08 17:49:37', '2022-05-08 17:49:37'),
(4835, '2022-05-08 00:00:00', '162.158.170.196', '2022-05-08 18:13:52', '2022-05-08 18:13:52'),
(4836, '2022-05-08 00:00:00', '172.69.70.98', '2022-05-08 19:57:42', '2022-05-08 19:57:42'),
(4837, '2022-05-08 00:00:00', '108.162.245.128', '2022-05-08 23:18:57', '2022-05-08 23:18:57'),
(4838, '2022-05-08 00:00:00', '162.158.107.191', '2022-05-08 23:18:59', '2022-05-08 23:18:59'),
(4839, '2022-05-08 00:00:00', '172.70.211.157', '2022-05-08 23:19:00', '2022-05-08 23:19:00'),
(4840, '2022-05-08 00:00:00', '108.162.246.219', '2022-05-08 23:19:00', '2022-05-08 23:19:00'),
(4841, '2022-05-08 00:00:00', '162.158.107.189', '2022-05-08 23:19:01', '2022-05-08 23:19:01'),
(4842, '2022-05-08 00:00:00', '108.162.245.252', '2022-05-08 23:19:01', '2022-05-08 23:19:01'),
(4843, '2022-05-08 00:00:00', '108.162.245.8', '2022-05-08 23:19:02', '2022-05-08 23:19:02'),
(4844, '2022-05-08 00:00:00', '172.70.207.19', '2022-05-08 23:19:02', '2022-05-08 23:19:02'),
(4845, '2022-05-08 00:00:00', '172.70.206.154', '2022-05-08 23:19:03', '2022-05-08 23:19:03'),
(4846, '2022-05-08 00:00:00', '162.158.107.165', '2022-05-08 23:19:03', '2022-05-08 23:19:03'),
(4847, '2022-05-08 00:00:00', '172.68.143.230', '2022-05-08 23:19:04', '2022-05-08 23:19:04'),
(4848, '2022-05-08 00:00:00', '172.70.214.66', '2022-05-08 23:19:04', '2022-05-08 23:19:04'),
(4849, '2022-05-08 00:00:00', '162.158.106.68', '2022-05-08 23:19:05', '2022-05-08 23:19:05'),
(4850, '2022-05-08 00:00:00', '162.158.107.237', '2022-05-08 23:19:05', '2022-05-08 23:19:05'),
(4851, '2022-05-08 00:00:00', '108.162.245.28', '2022-05-08 23:19:05', '2022-05-08 23:19:05'),
(4852, '2022-05-08 00:00:00', '162.158.166.116', '2022-05-08 23:19:06', '2022-05-08 23:19:06'),
(4853, '2022-05-08 00:00:00', '162.158.107.35', '2022-05-08 23:19:06', '2022-05-08 23:19:06'),
(4854, '2022-05-08 00:00:00', '108.162.245.116', '2022-05-08 23:19:07', '2022-05-08 23:19:07'),
(4855, '2022-05-08 00:00:00', '162.158.106.206', '2022-05-08 23:19:07', '2022-05-08 23:19:07'),
(4856, '2022-05-08 00:00:00', '162.158.106.16', '2022-05-08 23:19:08', '2022-05-08 23:19:08'),
(4857, '2022-05-08 00:00:00', '108.162.246.83', '2022-05-08 23:19:08', '2022-05-08 23:19:08'),
(4858, '2022-05-08 00:00:00', '162.158.107.39', '2022-05-08 23:19:09', '2022-05-08 23:19:09'),
(4859, '2022-05-08 00:00:00', '162.158.166.220', '2022-05-08 23:19:09', '2022-05-08 23:19:09'),
(4860, '2022-05-08 00:00:00', '108.162.246.51', '2022-05-08 23:19:09', '2022-05-08 23:19:09'),
(4861, '2022-05-08 00:00:00', '162.158.166.60', '2022-05-08 23:19:15', '2022-05-08 23:19:15'),
(4862, '2022-05-08 00:00:00', '162.158.107.95', '2022-05-08 23:19:15', '2022-05-08 23:19:15'),
(4863, '2022-05-08 00:00:00', '172.69.33.124', '2022-05-08 23:19:16', '2022-05-08 23:19:16'),
(4864, '2022-05-08 00:00:00', '108.162.245.166', '2022-05-08 23:19:16', '2022-05-08 23:19:16'),
(4865, '2022-05-08 00:00:00', '172.70.214.32', '2022-05-08 23:19:17', '2022-05-08 23:19:17'),
(4866, '2022-05-08 00:00:00', '172.68.133.104', '2022-05-08 23:19:18', '2022-05-08 23:19:18'),
(4867, '2022-05-08 00:00:00', '172.70.214.146', '2022-05-08 23:19:21', '2022-05-08 23:19:21'),
(4868, '2022-05-08 00:00:00', '172.70.142.34', '2022-05-08 23:36:36', '2022-05-08 23:36:36'),
(4869, '2022-05-09 00:00:00', '172.70.142.34', '2022-05-09 00:02:31', '2022-05-09 00:02:31'),
(4870, '2022-05-09 00:00:00', '162.158.163.3', '2022-05-09 00:24:13', '2022-05-09 00:24:13'),
(4871, '2022-05-09 00:00:00', '172.70.178.230', '2022-05-09 00:27:40', '2022-05-09 00:27:40'),
(4872, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 01:18:51', '2022-05-09 01:18:51'),
(4873, '2022-05-09 00:00:00', '172.70.142.34', '2022-05-09 01:49:06', '2022-05-09 01:49:06'),
(4874, '2022-05-09 00:00:00', '172.70.188.150', '2022-05-09 02:05:57', '2022-05-09 02:05:57'),
(4875, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 04:38:29', '2022-05-09 04:38:29'),
(4876, '2022-05-09 00:00:00', '162.158.103.187', '2022-05-09 04:41:05', '2022-05-09 04:41:05'),
(4877, '2022-05-09 00:00:00', '172.70.142.224', '2022-05-09 04:43:15', '2022-05-09 04:43:15');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(4878, '2022-05-09 00:00:00', '108.162.245.222', '2022-05-09 04:59:28', '2022-05-09 04:59:28'),
(4879, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 05:00:48', '2022-05-09 05:00:48'),
(4880, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 05:03:33', '2022-05-09 05:03:33'),
(4881, '2022-05-09 00:00:00', '172.70.218.236', '2022-05-09 05:38:57', '2022-05-09 05:38:57'),
(4882, '2022-05-09 00:00:00', '172.70.218.28', '2022-05-09 05:44:15', '2022-05-09 05:44:15'),
(4883, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 05:51:19', '2022-05-09 05:51:19'),
(4884, '2022-05-09 00:00:00', '172.70.85.34', '2022-05-09 05:52:24', '2022-05-09 05:52:24'),
(4885, '2022-05-09 00:00:00', '172.70.127.39', '2022-05-09 07:00:43', '2022-05-09 07:00:43'),
(4886, '2022-05-09 00:00:00', '172.70.90.172', '2022-05-09 07:26:31', '2022-05-09 07:26:31'),
(4887, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 07:27:56', '2022-05-09 07:27:56'),
(4888, '2022-05-09 00:00:00', '172.70.147.32', '2022-05-09 08:26:31', '2022-05-09 08:26:31'),
(4889, '2022-05-09 00:00:00', '172.70.142.32', '2022-05-09 08:41:12', '2022-05-09 08:41:12'),
(4890, '2022-05-09 00:00:00', '172.70.142.48', '2022-05-09 08:54:57', '2022-05-09 08:54:57'),
(4891, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 09:05:38', '2022-05-09 09:05:38'),
(4892, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 09:06:24', '2022-05-09 09:06:24'),
(4893, '2022-05-09 00:00:00', '172.70.90.172', '2022-05-09 09:08:02', '2022-05-09 09:08:02'),
(4894, '2022-05-09 00:00:00', '162.158.227.240', '2022-05-09 09:15:19', '2022-05-09 09:15:19'),
(4895, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 09:16:48', '2022-05-09 09:16:48'),
(4896, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 09:20:00', '2022-05-09 09:20:00'),
(4897, '2022-05-09 00:00:00', '172.70.90.172', '2022-05-09 09:23:09', '2022-05-09 09:23:09'),
(4898, '2022-05-09 00:00:00', '172.70.85.34', '2022-05-09 09:24:39', '2022-05-09 09:24:39'),
(4899, '2022-05-09 00:00:00', '141.101.99.85', '2022-05-09 09:27:29', '2022-05-09 09:27:29'),
(4900, '2022-05-09 00:00:00', '172.70.85.150', '2022-05-09 09:29:30', '2022-05-09 09:29:30'),
(4901, '2022-05-09 00:00:00', '172.70.90.212', '2022-05-09 09:31:22', '2022-05-09 09:31:22'),
(4902, '2022-05-09 00:00:00', '172.70.162.22', '2022-05-09 09:32:06', '2022-05-09 09:32:06'),
(4903, '2022-05-09 00:00:00', '172.70.90.6', '2022-05-09 09:33:43', '2022-05-09 09:33:43'),
(4904, '2022-05-09 00:00:00', '162.158.159.44', '2022-05-09 09:34:28', '2022-05-09 09:34:28'),
(4905, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 09:37:51', '2022-05-09 09:37:51'),
(4906, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 09:42:44', '2022-05-09 09:42:44'),
(4907, '2022-05-09 00:00:00', '172.70.162.188', '2022-05-09 09:43:53', '2022-05-09 09:43:53'),
(4908, '2022-05-09 00:00:00', '172.70.85.34', '2022-05-09 09:46:04', '2022-05-09 09:46:04'),
(4909, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 09:47:40', '2022-05-09 09:47:40'),
(4910, '2022-05-09 00:00:00', '172.70.162.188', '2022-05-09 09:57:31', '2022-05-09 09:57:31'),
(4911, '2022-05-09 00:00:00', '141.101.99.151', '2022-05-09 10:00:14', '2022-05-09 10:00:14'),
(4912, '2022-05-09 00:00:00', '172.70.131.43', '2022-05-09 10:00:43', '2022-05-09 10:00:43'),
(4913, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 10:01:07', '2022-05-09 10:01:07'),
(4914, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 10:01:22', '2022-05-09 10:01:22'),
(4915, '2022-05-09 00:00:00', '172.70.218.236', '2022-05-09 10:03:58', '2022-05-09 10:03:58'),
(4916, '2022-05-09 00:00:00', '172.70.90.212', '2022-05-09 10:11:10', '2022-05-09 10:11:10'),
(4917, '2022-05-09 00:00:00', '172.70.162.188', '2022-05-09 10:12:19', '2022-05-09 10:12:19'),
(4918, '2022-05-09 00:00:00', '172.70.90.6', '2022-05-09 10:15:45', '2022-05-09 10:15:45'),
(4919, '2022-05-09 00:00:00', '162.158.159.120', '2022-05-09 11:04:49', '2022-05-09 11:04:49'),
(4920, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 11:11:43', '2022-05-09 11:11:43'),
(4921, '2022-05-09 00:00:00', '172.70.162.240', '2022-05-09 11:15:29', '2022-05-09 11:15:29'),
(4922, '2022-05-09 00:00:00', '172.70.38.210', '2022-05-09 11:30:43', '2022-05-09 11:30:43'),
(4923, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 11:41:21', '2022-05-09 11:41:21'),
(4924, '2022-05-09 00:00:00', '172.70.90.6', '2022-05-09 11:45:53', '2022-05-09 11:45:53'),
(4925, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 11:47:09', '2022-05-09 11:47:09'),
(4926, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 11:47:51', '2022-05-09 11:47:51'),
(4927, '2022-05-09 00:00:00', '162.158.159.54', '2022-05-09 11:49:28', '2022-05-09 11:49:28'),
(4928, '2022-05-09 00:00:00', '172.70.85.150', '2022-05-09 11:52:15', '2022-05-09 11:52:15'),
(4929, '2022-05-09 00:00:00', '141.101.99.11', '2022-05-09 11:55:10', '2022-05-09 11:55:10'),
(4930, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 11:58:54', '2022-05-09 11:58:54'),
(4931, '2022-05-09 00:00:00', '172.70.162.12', '2022-05-09 12:08:44', '2022-05-09 12:08:44'),
(4932, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 12:10:10', '2022-05-09 12:10:10'),
(4933, '2022-05-09 00:00:00', '162.158.159.54', '2022-05-09 12:12:23', '2022-05-09 12:12:23'),
(4934, '2022-05-09 00:00:00', '172.70.90.6', '2022-05-09 12:14:03', '2022-05-09 12:14:03'),
(4935, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 12:15:07', '2022-05-09 12:15:07'),
(4936, '2022-05-09 00:00:00', '141.101.107.82', '2022-05-09 12:17:28', '2022-05-09 12:17:28'),
(4937, '2022-05-09 00:00:00', '162.158.163.217', '2022-05-09 12:17:44', '2022-05-09 12:17:44'),
(4938, '2022-05-09 00:00:00', '172.70.85.150', '2022-05-09 12:20:37', '2022-05-09 12:20:37'),
(4939, '2022-05-09 00:00:00', '172.70.90.172', '2022-05-09 12:21:51', '2022-05-09 12:21:51'),
(4940, '2022-05-09 00:00:00', '172.70.90.150', '2022-05-09 12:23:17', '2022-05-09 12:23:17'),
(4941, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 12:30:11', '2022-05-09 12:30:11'),
(4942, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 12:32:04', '2022-05-09 12:32:04'),
(4943, '2022-05-09 00:00:00', '162.158.159.24', '2022-05-09 12:36:04', '2022-05-09 12:36:04'),
(4944, '2022-05-09 00:00:00', '172.70.90.212', '2022-05-09 12:37:21', '2022-05-09 12:37:21'),
(4945, '2022-05-09 00:00:00', '162.158.159.82', '2022-05-09 12:39:26', '2022-05-09 12:39:26'),
(4946, '2022-05-09 00:00:00', '172.70.162.12', '2022-05-09 12:42:56', '2022-05-09 12:42:56'),
(4947, '2022-05-09 00:00:00', '172.68.118.112', '2022-05-09 12:51:30', '2022-05-09 12:51:30'),
(4948, '2022-05-09 00:00:00', '172.68.39.150', '2022-05-09 12:51:50', '2022-05-09 12:51:50'),
(4949, '2022-05-09 00:00:00', '172.68.39.216', '2022-05-09 12:52:33', '2022-05-09 12:52:33'),
(4950, '2022-05-09 00:00:00', '172.68.39.134', '2022-05-09 12:53:18', '2022-05-09 12:53:18'),
(4951, '2022-05-09 00:00:00', '172.70.85.40', '2022-05-09 12:54:06', '2022-05-09 12:54:06'),
(4952, '2022-05-09 00:00:00', '172.68.39.236', '2022-05-09 12:59:32', '2022-05-09 12:59:32'),
(4953, '2022-05-09 00:00:00', '172.68.39.216', '2022-05-09 13:03:34', '2022-05-09 13:03:34'),
(4954, '2022-05-09 00:00:00', '172.70.162.188', '2022-05-09 13:07:15', '2022-05-09 13:07:15'),
(4955, '2022-05-09 00:00:00', '172.70.162.22', '2022-05-09 13:08:02', '2022-05-09 13:08:02'),
(4956, '2022-05-09 00:00:00', '172.68.39.150', '2022-05-09 13:09:15', '2022-05-09 13:09:15'),
(4957, '2022-05-09 00:00:00', '172.68.39.134', '2022-05-09 13:17:27', '2022-05-09 13:17:27'),
(4958, '2022-05-09 00:00:00', '172.68.39.236', '2022-05-09 13:18:52', '2022-05-09 13:18:52'),
(4959, '2022-05-09 00:00:00', '141.101.107.138', '2022-05-09 13:19:16', '2022-05-09 13:19:16'),
(4960, '2022-05-09 00:00:00', '172.68.39.216', '2022-05-09 13:19:38', '2022-05-09 13:19:38'),
(4961, '2022-05-09 00:00:00', '172.70.162.22', '2022-05-09 13:27:31', '2022-05-09 13:27:31'),
(4962, '2022-05-09 00:00:00', '172.70.188.192', '2022-05-09 13:43:45', '2022-05-09 13:43:45'),
(4963, '2022-05-09 00:00:00', '172.70.85.34', '2022-05-09 13:45:41', '2022-05-09 13:45:41'),
(4964, '2022-05-09 00:00:00', '172.70.85.154', '2022-05-09 14:04:17', '2022-05-09 14:04:17'),
(4965, '2022-05-09 00:00:00', '172.70.90.212', '2022-05-09 14:09:26', '2022-05-09 14:09:26'),
(4966, '2022-05-09 00:00:00', '172.70.85.34', '2022-05-09 14:18:12', '2022-05-09 14:18:12'),
(4967, '2022-05-09 00:00:00', '172.70.162.22', '2022-05-09 14:21:55', '2022-05-09 14:21:55'),
(4968, '2022-05-09 00:00:00', '172.70.90.212', '2022-05-09 14:24:31', '2022-05-09 14:24:31'),
(4969, '2022-05-09 00:00:00', '172.70.85.150', '2022-05-09 14:38:42', '2022-05-09 14:38:42'),
(4970, '2022-05-09 00:00:00', '172.70.135.81', '2022-05-09 14:39:23', '2022-05-09 14:39:23'),
(4971, '2022-05-09 00:00:00', '172.70.175.213', '2022-05-09 14:39:26', '2022-05-09 14:39:26'),
(4972, '2022-05-09 00:00:00', '172.70.206.94', '2022-05-09 14:43:58', '2022-05-09 14:43:58'),
(4973, '2022-05-09 00:00:00', '172.68.39.236', '2022-05-09 15:08:40', '2022-05-09 15:08:40'),
(4974, '2022-05-09 00:00:00', '141.101.99.85', '2022-05-09 15:24:32', '2022-05-09 15:24:32'),
(4975, '2022-05-09 00:00:00', '162.158.178.13', '2022-05-09 15:24:35', '2022-05-09 15:24:35'),
(4976, '2022-05-09 00:00:00', '172.70.175.9', '2022-05-09 15:50:45', '2022-05-09 15:50:45'),
(4977, '2022-05-09 00:00:00', '172.70.174.220', '2022-05-09 15:50:45', '2022-05-09 15:50:45'),
(4978, '2022-05-09 00:00:00', '172.70.214.82', '2022-05-09 15:51:51', '2022-05-09 15:51:51'),
(4979, '2022-05-09 00:00:00', '172.70.189.59', '2022-05-09 16:20:12', '2022-05-09 16:20:12'),
(4980, '2022-05-09 00:00:00', '172.70.35.73', '2022-05-09 16:35:46', '2022-05-09 16:35:46'),
(4981, '2022-05-09 00:00:00', '172.70.147.42', '2022-05-09 16:45:50', '2022-05-09 16:45:50'),
(4982, '2022-05-09 00:00:00', '162.158.170.56', '2022-05-09 16:52:01', '2022-05-09 16:52:01'),
(4983, '2022-05-09 00:00:00', '172.70.34.16', '2022-05-09 17:20:45', '2022-05-09 17:20:45'),
(4984, '2022-05-09 00:00:00', '172.70.174.220', '2022-05-09 17:20:46', '2022-05-09 17:20:46'),
(4985, '2022-05-09 00:00:00', '172.70.142.34', '2022-05-09 18:07:36', '2022-05-09 18:07:36'),
(4986, '2022-05-09 00:00:00', '162.158.162.236', '2022-05-09 18:35:44', '2022-05-09 18:35:44'),
(4987, '2022-05-09 00:00:00', '172.70.35.89', '2022-05-09 19:44:35', '2022-05-09 19:44:35'),
(4988, '2022-05-09 00:00:00', '172.69.69.169', '2022-05-09 22:52:29', '2022-05-09 22:52:29'),
(4989, '2022-05-09 00:00:00', '172.69.68.216', '2022-05-09 22:53:20', '2022-05-09 22:53:20'),
(4990, '2022-05-09 00:00:00', '108.162.221.50', '2022-05-09 22:53:48', '2022-05-09 22:53:48'),
(4991, '2022-05-09 00:00:00', '172.69.70.140', '2022-05-09 22:54:56', '2022-05-09 22:54:56'),
(4992, '2022-05-09 00:00:00', '172.69.71.110', '2022-05-09 22:57:37', '2022-05-09 22:57:37'),
(4993, '2022-05-09 00:00:00', '108.162.221.154', '2022-05-09 22:59:46', '2022-05-09 22:59:46'),
(4994, '2022-05-09 00:00:00', '172.70.126.44', '2022-05-09 23:02:32', '2022-05-09 23:02:32'),
(4995, '2022-05-09 00:00:00', '172.70.126.238', '2022-05-09 23:03:21', '2022-05-09 23:03:21'),
(4996, '2022-05-10 00:00:00', '172.70.162.22', '2022-05-10 00:19:41', '2022-05-10 00:19:41'),
(4997, '2022-05-10 00:00:00', '172.69.34.25', '2022-05-10 00:41:25', '2022-05-10 00:41:25'),
(4998, '2022-05-10 00:00:00', '172.69.33.14', '2022-05-10 00:41:54', '2022-05-10 00:41:54'),
(4999, '2022-05-10 00:00:00', '172.70.178.32', '2022-05-10 00:48:01', '2022-05-10 00:48:01'),
(5000, '2022-05-10 00:00:00', '172.70.126.238', '2022-05-10 00:48:35', '2022-05-10 00:48:35'),
(5001, '2022-05-10 00:00:00', '172.68.254.28', '2022-05-10 01:33:40', '2022-05-10 01:33:40'),
(5002, '2022-05-10 00:00:00', '172.70.142.218', '2022-05-10 02:01:48', '2022-05-10 02:01:48'),
(5003, '2022-05-10 00:00:00', '172.70.142.224', '2022-05-10 02:18:16', '2022-05-10 02:18:16'),
(5004, '2022-05-10 00:00:00', '172.70.147.34', '2022-05-10 03:05:22', '2022-05-10 03:05:22'),
(5005, '2022-05-10 00:00:00', '172.70.142.42', '2022-05-10 03:37:37', '2022-05-10 03:37:37'),
(5006, '2022-05-10 00:00:00', '172.70.142.48', '2022-05-10 04:09:38', '2022-05-10 04:09:38'),
(5007, '2022-05-10 00:00:00', '172.70.188.192', '2022-05-10 04:28:53', '2022-05-10 04:28:53'),
(5008, '2022-05-10 00:00:00', '172.70.92.156', '2022-05-10 04:41:03', '2022-05-10 04:41:03'),
(5009, '2022-05-10 00:00:00', '172.70.90.172', '2022-05-10 04:59:41', '2022-05-10 04:59:41'),
(5010, '2022-05-10 00:00:00', '162.158.79.49', '2022-05-10 05:06:01', '2022-05-10 05:06:01'),
(5011, '2022-05-10 00:00:00', '172.70.134.80', '2022-05-10 05:50:58', '2022-05-10 05:50:58'),
(5012, '2022-05-10 00:00:00', '172.70.135.51', '2022-05-10 06:35:58', '2022-05-10 06:35:58'),
(5013, '2022-05-10 00:00:00', '172.70.131.199', '2022-05-10 07:45:35', '2022-05-10 07:45:35'),
(5014, '2022-05-10 00:00:00', '172.70.126.20', '2022-05-10 07:45:35', '2022-05-10 07:45:35'),
(5015, '2022-05-10 00:00:00', '172.70.131.157', '2022-05-10 07:45:36', '2022-05-10 07:45:36'),
(5016, '2022-05-10 00:00:00', '172.70.142.218', '2022-05-10 08:49:40', '2022-05-10 08:49:40'),
(5017, '2022-05-10 00:00:00', '172.70.147.34', '2022-05-10 08:57:05', '2022-05-10 08:57:05'),
(5018, '2022-05-10 00:00:00', '162.158.170.88', '2022-05-10 09:04:44', '2022-05-10 09:04:44'),
(5019, '2022-05-10 00:00:00', '172.70.147.34', '2022-05-10 09:21:11', '2022-05-10 09:21:11'),
(5020, '2022-05-10 00:00:00', '172.70.189.95', '2022-05-10 09:29:10', '2022-05-10 09:29:10'),
(5021, '2022-05-10 00:00:00', '172.70.188.90', '2022-05-10 09:36:48', '2022-05-10 09:36:48'),
(5022, '2022-05-10 00:00:00', '108.162.241.16', '2022-05-10 09:42:20', '2022-05-10 09:42:20'),
(5023, '2022-05-10 00:00:00', '172.68.39.216', '2022-05-10 10:01:00', '2022-05-10 10:01:00'),
(5024, '2022-05-10 00:00:00', '172.70.189.95', '2022-05-10 10:13:34', '2022-05-10 10:13:34'),
(5025, '2022-05-10 00:00:00', '172.70.188.150', '2022-05-10 10:27:05', '2022-05-10 10:27:05'),
(5026, '2022-05-10 00:00:00', '172.70.134.140', '2022-05-10 14:47:01', '2022-05-10 14:47:01'),
(5027, '2022-05-10 00:00:00', '162.158.163.227', '2022-05-10 15:29:56', '2022-05-10 15:29:56'),
(5028, '2022-05-10 00:00:00', '172.70.92.160', '2022-05-10 15:49:04', '2022-05-10 15:49:04'),
(5029, '2022-05-10 00:00:00', '162.158.162.220', '2022-05-10 15:57:05', '2022-05-10 15:57:05'),
(5030, '2022-05-10 00:00:00', '172.70.142.218', '2022-05-10 16:05:20', '2022-05-10 16:05:20'),
(5031, '2022-05-10 00:00:00', '172.70.147.34', '2022-05-10 16:11:34', '2022-05-10 16:11:34'),
(5032, '2022-05-10 00:00:00', '172.70.147.48', '2022-05-10 16:18:34', '2022-05-10 16:18:34'),
(5033, '2022-05-10 00:00:00', '172.70.34.82', '2022-05-10 16:45:24', '2022-05-10 16:45:24'),
(5034, '2022-05-10 00:00:00', '162.158.163.233', '2022-05-10 17:26:14', '2022-05-10 17:26:14'),
(5035, '2022-05-10 00:00:00', '162.158.162.156', '2022-05-10 17:31:12', '2022-05-10 17:31:12'),
(5036, '2022-05-10 00:00:00', '172.70.92.160', '2022-05-10 17:36:14', '2022-05-10 17:36:14'),
(5037, '2022-05-10 00:00:00', '162.158.170.56', '2022-05-10 17:41:04', '2022-05-10 17:41:04'),
(5038, '2022-05-10 00:00:00', '162.158.162.94', '2022-05-10 17:57:16', '2022-05-10 17:57:16'),
(5039, '2022-05-10 00:00:00', '141.101.99.235', '2022-05-10 18:07:55', '2022-05-10 18:07:55'),
(5040, '2022-05-10 00:00:00', '172.70.142.42', '2022-05-10 18:08:07', '2022-05-10 18:08:07'),
(5041, '2022-05-10 00:00:00', '172.70.134.80', '2022-05-10 18:15:23', '2022-05-10 18:15:23'),
(5042, '2022-05-10 00:00:00', '162.158.78.182', '2022-05-10 18:48:26', '2022-05-10 18:48:26'),
(5043, '2022-05-10 00:00:00', '172.70.175.17', '2022-05-10 19:33:26', '2022-05-10 19:33:26'),
(5044, '2022-05-10 00:00:00', '172.70.134.4', '2022-05-10 20:18:26', '2022-05-10 20:18:26'),
(5045, '2022-05-10 00:00:00', '162.158.79.47', '2022-05-10 20:40:57', '2022-05-10 20:40:57'),
(5046, '2022-05-10 00:00:00', '172.70.34.30', '2022-05-10 23:06:27', '2022-05-10 23:06:27'),
(5047, '2022-05-10 00:00:00', '172.70.246.180', '2022-05-10 23:08:13', '2022-05-10 23:08:13'),
(5048, '2022-05-11 00:00:00', '172.70.174.166', '2022-05-11 00:25:06', '2022-05-11 00:25:06'),
(5049, '2022-05-11 00:00:00', '172.70.92.150', '2022-05-11 00:29:17', '2022-05-11 00:29:17'),
(5050, '2022-05-11 00:00:00', '20.108.23.30', '2022-05-11 00:36:48', '2022-05-11 00:36:48'),
(5051, '2022-05-11 00:00:00', '172.70.189.59', '2022-05-11 00:44:16', '2022-05-11 00:44:16'),
(5052, '2022-05-11 00:00:00', '141.101.76.32', '2022-05-11 01:21:54', '2022-05-11 01:21:54'),
(5053, '2022-05-11 00:00:00', '172.70.34.146', '2022-05-11 01:55:06', '2022-05-11 01:55:06'),
(5054, '2022-05-11 00:00:00', '172.70.92.150', '2022-05-11 02:07:24', '2022-05-11 02:07:24'),
(5055, '2022-05-11 00:00:00', '172.70.135.131', '2022-05-11 02:25:06', '2022-05-11 02:25:06'),
(5056, '2022-05-11 00:00:00', '162.158.162.206', '2022-05-11 02:35:13', '2022-05-11 02:35:13'),
(5057, '2022-05-11 00:00:00', '172.70.142.224', '2022-05-11 02:52:04', '2022-05-11 02:52:04'),
(5058, '2022-05-11 00:00:00', '172.70.34.140', '2022-05-11 02:55:06', '2022-05-11 02:55:06'),
(5059, '2022-05-11 00:00:00', '172.70.189.137', '2022-05-11 03:22:53', '2022-05-11 03:22:53'),
(5060, '2022-05-11 00:00:00', '172.70.34.198', '2022-05-11 03:25:06', '2022-05-11 03:25:06'),
(5061, '2022-05-11 00:00:00', '162.158.163.175', '2022-05-11 03:30:42', '2022-05-11 03:30:42'),
(5062, '2022-05-11 00:00:00', '172.70.38.28', '2022-05-11 03:55:06', '2022-05-11 03:55:06'),
(5063, '2022-05-11 00:00:00', '162.158.78.86', '2022-05-11 04:19:46', '2022-05-11 04:19:46'),
(5064, '2022-05-11 00:00:00', '172.70.35.89', '2022-05-11 04:19:47', '2022-05-11 04:19:47'),
(5065, '2022-05-11 00:00:00', '172.70.135.5', '2022-05-11 05:43:27', '2022-05-11 05:43:27'),
(5066, '2022-05-11 00:00:00', '172.70.90.212', '2022-05-11 06:12:13', '2022-05-11 06:12:13'),
(5067, '2022-05-11 00:00:00', '172.70.162.12', '2022-05-11 06:32:34', '2022-05-11 06:32:34'),
(5068, '2022-05-11 00:00:00', '172.70.189.59', '2022-05-11 06:41:37', '2022-05-11 06:41:37'),
(5069, '2022-05-11 00:00:00', '172.70.162.12', '2022-05-11 06:43:54', '2022-05-11 06:43:54'),
(5070, '2022-05-11 00:00:00', '172.70.162.12', '2022-05-11 06:56:23', '2022-05-11 06:56:23'),
(5071, '2022-05-11 00:00:00', '108.162.246.223', '2022-05-11 07:07:51', '2022-05-11 07:07:51'),
(5072, '2022-05-11 00:00:00', '172.70.134.4', '2022-05-11 07:13:26', '2022-05-11 07:13:26'),
(5073, '2022-05-11 00:00:00', '172.70.142.42', '2022-05-11 07:39:43', '2022-05-11 07:39:43'),
(5074, '2022-05-11 00:00:00', '172.70.251.7', '2022-05-11 08:24:34', '2022-05-11 08:24:34'),
(5075, '2022-05-11 00:00:00', '172.70.246.10', '2022-05-11 08:24:34', '2022-05-11 08:24:34'),
(5076, '2022-05-11 00:00:00', '172.70.92.150', '2022-05-11 08:27:52', '2022-05-11 08:27:52'),
(5077, '2022-05-11 00:00:00', '162.158.78.114', '2022-05-11 08:43:26', '2022-05-11 08:43:26'),
(5078, '2022-05-11 00:00:00', '172.70.189.137', '2022-05-11 09:21:03', '2022-05-11 09:21:03'),
(5079, '2022-05-11 00:00:00', '162.158.163.225', '2022-05-11 09:26:43', '2022-05-11 09:26:43'),
(5080, '2022-05-11 00:00:00', '162.158.163.213', '2022-05-11 10:13:39', '2022-05-11 10:13:39'),
(5081, '2022-05-11 00:00:00', '172.68.10.108', '2022-05-11 10:29:23', '2022-05-11 10:29:23'),
(5082, '2022-05-11 00:00:00', '172.70.147.42', '2022-05-11 10:32:10', '2022-05-11 10:32:10'),
(5083, '2022-05-11 00:00:00', '172.70.92.156', '2022-05-11 11:01:04', '2022-05-11 11:01:04'),
(5084, '2022-05-11 00:00:00', '172.69.70.140', '2022-05-11 11:14:51', '2022-05-11 11:14:51'),
(5085, '2022-05-11 00:00:00', '108.162.221.56', '2022-05-11 11:15:11', '2022-05-11 11:15:11'),
(5086, '2022-05-11 00:00:00', '162.158.162.78', '2022-05-11 11:17:24', '2022-05-11 11:17:24'),
(5087, '2022-05-11 00:00:00', '162.158.178.178', '2022-05-11 11:48:27', '2022-05-11 11:48:27'),
(5088, '2022-05-11 00:00:00', '162.158.159.54', '2022-05-11 11:54:08', '2022-05-11 11:54:08'),
(5089, '2022-05-11 00:00:00', '172.70.85.34', '2022-05-11 11:58:21', '2022-05-11 11:58:21'),
(5090, '2022-05-11 00:00:00', '108.162.245.28', '2022-05-11 12:10:41', '2022-05-11 12:10:41'),
(5091, '2022-05-11 00:00:00', '172.70.85.40', '2022-05-11 12:23:10', '2022-05-11 12:23:10'),
(5092, '2022-05-11 00:00:00', '141.101.99.235', '2022-05-11 12:33:18', '2022-05-11 12:33:18'),
(5093, '2022-05-11 00:00:00', '162.158.162.252', '2022-05-11 12:33:52', '2022-05-11 12:33:52'),
(5094, '2022-05-11 00:00:00', '172.70.218.236', '2022-05-11 12:33:58', '2022-05-11 12:33:58'),
(5095, '2022-05-11 00:00:00', '162.158.227.238', '2022-05-11 12:34:04', '2022-05-11 12:34:04'),
(5096, '2022-05-11 00:00:00', '172.70.162.188', '2022-05-11 12:34:25', '2022-05-11 12:34:25'),
(5097, '2022-05-11 00:00:00', '172.70.85.150', '2022-05-11 12:35:42', '2022-05-11 12:35:42'),
(5098, '2022-05-11 00:00:00', '172.70.218.28', '2022-05-11 12:35:48', '2022-05-11 12:35:48'),
(5099, '2022-05-11 00:00:00', '172.70.90.6', '2022-05-11 12:49:36', '2022-05-11 12:49:36'),
(5100, '2022-05-11 00:00:00', '172.70.162.12', '2022-05-11 13:13:28', '2022-05-11 13:13:28'),
(5101, '2022-05-11 00:00:00', '172.70.90.172', '2022-05-11 13:14:01', '2022-05-11 13:14:01'),
(5102, '2022-05-11 00:00:00', '172.70.162.22', '2022-05-11 13:15:57', '2022-05-11 13:15:57'),
(5103, '2022-05-11 00:00:00', '172.70.85.34', '2022-05-11 13:18:16', '2022-05-11 13:18:16'),
(5104, '2022-05-11 00:00:00', '172.70.90.150', '2022-05-11 13:20:30', '2022-05-11 13:20:30'),
(5105, '2022-05-11 00:00:00', '141.101.99.235', '2022-05-11 13:21:02', '2022-05-11 13:21:02'),
(5106, '2022-05-11 00:00:00', '172.70.162.188', '2022-05-11 13:22:40', '2022-05-11 13:22:40'),
(5107, '2022-05-11 00:00:00', '172.70.123.55', '2022-05-11 13:25:46', '2022-05-11 13:25:46'),
(5108, '2022-05-11 00:00:00', '172.70.85.150', '2022-05-11 13:27:52', '2022-05-11 13:27:52'),
(5109, '2022-05-11 00:00:00', '172.70.90.212', '2022-05-11 13:31:35', '2022-05-11 13:31:35'),
(5110, '2022-05-11 00:00:00', '162.158.159.54', '2022-05-11 13:32:12', '2022-05-11 13:32:12'),
(5111, '2022-05-11 00:00:00', '172.70.162.22', '2022-05-11 13:33:07', '2022-05-11 13:33:07'),
(5112, '2022-05-11 00:00:00', '141.101.99.149', '2022-05-11 13:34:44', '2022-05-11 13:34:44'),
(5113, '2022-05-11 00:00:00', '172.70.162.188', '2022-05-11 13:36:29', '2022-05-11 13:36:29'),
(5114, '2022-05-11 00:00:00', '172.70.90.6', '2022-05-11 13:39:50', '2022-05-11 13:39:50'),
(5115, '2022-05-11 00:00:00', '172.70.85.34', '2022-05-11 13:41:46', '2022-05-11 13:41:46'),
(5116, '2022-05-11 00:00:00', '172.70.90.212', '2022-05-11 13:45:43', '2022-05-11 13:45:43'),
(5117, '2022-05-11 00:00:00', '172.70.85.40', '2022-05-11 13:46:19', '2022-05-11 13:46:19'),
(5118, '2022-05-11 00:00:00', '172.70.85.150', '2022-05-11 13:47:37', '2022-05-11 13:47:37'),
(5119, '2022-05-11 00:00:00', '172.70.90.172', '2022-05-11 13:48:24', '2022-05-11 13:48:24'),
(5120, '2022-05-11 00:00:00', '172.70.85.154', '2022-05-11 13:49:39', '2022-05-11 13:49:39'),
(5121, '2022-05-11 00:00:00', '172.70.90.6', '2022-05-11 13:50:50', '2022-05-11 13:50:50'),
(5122, '2022-05-11 00:00:00', '172.70.85.34', '2022-05-11 13:53:01', '2022-05-11 13:53:01'),
(5123, '2022-05-11 00:00:00', '172.70.85.150', '2022-05-11 14:02:55', '2022-05-11 14:02:55'),
(5124, '2022-05-11 00:00:00', '172.68.244.172', '2022-05-11 14:07:05', '2022-05-11 14:07:05'),
(5125, '2022-05-11 00:00:00', '172.68.246.54', '2022-05-11 14:07:06', '2022-05-11 14:07:06'),
(5126, '2022-05-11 00:00:00', '172.68.11.135', '2022-05-11 14:07:06', '2022-05-11 14:07:06'),
(5127, '2022-05-11 00:00:00', '172.68.10.54', '2022-05-11 14:07:08', '2022-05-11 14:07:08'),
(5128, '2022-05-11 00:00:00', '172.70.162.12', '2022-05-11 14:10:26', '2022-05-11 14:10:26'),
(5129, '2022-05-11 00:00:00', '172.70.90.6', '2022-05-11 14:11:42', '2022-05-11 14:11:42'),
(5130, '2022-05-11 00:00:00', '172.70.218.28', '2022-05-11 14:13:27', '2022-05-11 14:13:27'),
(5131, '2022-05-11 00:00:00', '172.70.162.22', '2022-05-11 14:18:50', '2022-05-11 14:18:50'),
(5132, '2022-05-11 00:00:00', '172.70.142.228', '2022-05-11 14:21:08', '2022-05-11 14:21:08'),
(5133, '2022-05-11 00:00:00', '172.68.39.150', '2022-05-11 14:37:00', '2022-05-11 14:37:00'),
(5134, '2022-05-11 00:00:00', '172.70.162.22', '2022-05-11 14:38:48', '2022-05-11 14:38:48'),
(5135, '2022-05-11 00:00:00', '172.70.90.212', '2022-05-11 14:39:34', '2022-05-11 14:39:34'),
(5136, '2022-05-11 00:00:00', '172.70.174.154', '2022-05-11 14:39:35', '2022-05-11 14:39:35'),
(5137, '2022-05-11 00:00:00', '141.101.107.138', '2022-05-11 14:42:38', '2022-05-11 14:42:38'),
(5138, '2022-05-11 00:00:00', '172.68.39.134', '2022-05-11 14:48:14', '2022-05-11 14:48:14'),
(5139, '2022-05-11 00:00:00', '172.70.162.22', '2022-05-11 14:52:36', '2022-05-11 14:52:36'),
(5140, '2022-05-11 00:00:00', '172.70.162.188', '2022-05-11 14:55:37', '2022-05-11 14:55:37'),
(5141, '2022-05-11 00:00:00', '172.70.142.34', '2022-05-11 14:56:18', '2022-05-11 14:56:18'),
(5142, '2022-05-11 00:00:00', '172.70.90.6', '2022-05-11 15:00:35', '2022-05-11 15:00:35'),
(5143, '2022-05-11 00:00:00', '172.68.39.216', '2022-05-11 15:00:38', '2022-05-11 15:00:38'),
(5144, '2022-05-11 00:00:00', '172.68.39.150', '2022-05-11 15:01:20', '2022-05-11 15:01:20'),
(5145, '2022-05-11 00:00:00', '172.70.175.213', '2022-05-11 15:11:52', '2022-05-11 15:11:52'),
(5146, '2022-05-11 00:00:00', '141.101.98.242', '2022-05-11 15:12:58', '2022-05-11 15:12:58'),
(5147, '2022-05-11 00:00:00', '172.70.90.172', '2022-05-11 15:15:25', '2022-05-11 15:15:25'),
(5148, '2022-05-11 00:00:00', '162.158.159.120', '2022-05-11 15:20:42', '2022-05-11 15:20:42'),
(5149, '2022-05-11 00:00:00', '172.70.135.81', '2022-05-11 15:20:47', '2022-05-11 15:20:47'),
(5150, '2022-05-11 00:00:00', '172.70.135.201', '2022-05-11 15:24:38', '2022-05-11 15:24:38'),
(5151, '2022-05-11 00:00:00', '172.70.38.246', '2022-05-11 17:47:54', '2022-05-11 17:47:54'),
(5152, '2022-05-11 00:00:00', '172.70.135.17', '2022-05-11 21:19:13', '2022-05-11 21:19:13'),
(5153, '2022-05-11 00:00:00', '172.70.134.224', '2022-05-11 22:19:14', '2022-05-11 22:19:14'),
(5154, '2022-05-11 00:00:00', '172.70.175.199', '2022-05-11 22:21:35', '2022-05-11 22:21:35'),
(5155, '2022-05-11 00:00:00', '172.70.174.128', '2022-05-11 23:06:40', '2022-05-11 23:06:40'),
(5156, '2022-05-12 00:00:00', '162.158.162.112', '2022-05-12 02:10:56', '2022-05-12 02:10:56'),
(5157, '2022-05-12 00:00:00', '172.70.147.34', '2022-05-12 02:44:02', '2022-05-12 02:44:02'),
(5158, '2022-05-12 00:00:00', '172.70.188.90', '2022-05-12 02:56:22', '2022-05-12 02:56:22'),
(5159, '2022-05-12 00:00:00', '162.158.171.9', '2022-05-12 03:03:16', '2022-05-12 03:03:16'),
(5160, '2022-05-12 00:00:00', '172.70.175.47', '2022-05-12 04:59:07', '2022-05-12 04:59:07'),
(5161, '2022-05-12 00:00:00', '172.70.135.175', '2022-05-12 06:42:41', '2022-05-12 06:42:41'),
(5162, '2022-05-12 00:00:00', '162.158.159.54', '2022-05-12 06:46:21', '2022-05-12 06:46:21'),
(5163, '2022-05-12 00:00:00', '172.70.85.150', '2022-05-12 06:49:40', '2022-05-12 06:49:40'),
(5164, '2022-05-12 00:00:00', '162.158.159.112', '2022-05-12 06:50:25', '2022-05-12 06:50:25'),
(5165, '2022-05-12 00:00:00', '172.70.162.22', '2022-05-12 07:16:39', '2022-05-12 07:16:39'),
(5166, '2022-05-12 00:00:00', '172.70.135.191', '2022-05-12 07:27:41', '2022-05-12 07:27:41'),
(5167, '2022-05-12 00:00:00', '172.69.34.43', '2022-05-12 07:49:26', '2022-05-12 07:49:26'),
(5168, '2022-05-12 00:00:00', '162.158.163.177', '2022-05-12 08:09:37', '2022-05-12 08:09:37'),
(5169, '2022-05-12 00:00:00', '162.158.170.88', '2022-05-12 08:30:31', '2022-05-12 08:30:31'),
(5170, '2022-05-12 00:00:00', '172.70.162.188', '2022-05-12 08:32:00', '2022-05-12 08:32:00'),
(5171, '2022-05-12 00:00:00', '172.70.90.150', '2022-05-12 08:36:32', '2022-05-12 08:36:32'),
(5172, '2022-05-12 00:00:00', '172.70.189.95', '2022-05-12 08:40:07', '2022-05-12 08:40:07'),
(5173, '2022-05-12 00:00:00', '172.70.147.48', '2022-05-12 08:58:43', '2022-05-12 08:58:43'),
(5174, '2022-05-12 00:00:00', '172.70.189.95', '2022-05-12 09:08:11', '2022-05-12 09:08:11'),
(5175, '2022-05-12 00:00:00', '172.70.90.6', '2022-05-12 09:44:32', '2022-05-12 09:44:32'),
(5176, '2022-05-12 00:00:00', '172.70.85.34', '2022-05-12 09:48:06', '2022-05-12 09:48:06'),
(5177, '2022-05-12 00:00:00', '172.70.162.22', '2022-05-12 09:50:34', '2022-05-12 09:50:34'),
(5178, '2022-05-12 00:00:00', '172.70.162.12', '2022-05-12 09:51:19', '2022-05-12 09:51:19'),
(5179, '2022-05-12 00:00:00', '162.158.159.24', '2022-05-12 09:52:47', '2022-05-12 09:52:47'),
(5180, '2022-05-12 00:00:00', '172.70.92.150', '2022-05-12 09:54:14', '2022-05-12 09:54:14'),
(5181, '2022-05-12 00:00:00', '141.101.99.149', '2022-05-12 10:01:16', '2022-05-12 10:01:16'),
(5182, '2022-05-12 00:00:00', '172.70.188.192', '2022-05-12 10:03:10', '2022-05-12 10:03:10'),
(5183, '2022-05-12 00:00:00', '172.70.85.40', '2022-05-12 10:06:46', '2022-05-12 10:06:46'),
(5184, '2022-05-12 00:00:00', '172.70.162.12', '2022-05-12 10:07:07', '2022-05-12 10:07:07'),
(5185, '2022-05-12 00:00:00', '172.70.162.188', '2022-05-12 10:07:47', '2022-05-12 10:07:47'),
(5186, '2022-05-12 00:00:00', '141.101.99.235', '2022-05-12 10:09:49', '2022-05-12 10:09:49'),
(5187, '2022-05-12 00:00:00', '172.70.85.150', '2022-05-12 10:11:06', '2022-05-12 10:11:06'),
(5188, '2022-05-12 00:00:00', '141.101.99.149', '2022-05-12 10:15:06', '2022-05-12 10:15:06'),
(5189, '2022-05-12 00:00:00', '172.68.39.134', '2022-05-12 10:23:22', '2022-05-12 10:23:22'),
(5190, '2022-05-12 00:00:00', '172.68.39.236', '2022-05-12 10:24:33', '2022-05-12 10:24:33'),
(5191, '2022-05-12 00:00:00', '162.158.162.236', '2022-05-12 10:27:56', '2022-05-12 10:27:56'),
(5192, '2022-05-12 00:00:00', '172.70.34.42', '2022-05-12 11:21:59', '2022-05-12 11:21:59'),
(5193, '2022-05-12 00:00:00', '172.70.90.150', '2022-05-12 11:29:58', '2022-05-12 11:29:58'),
(5194, '2022-05-12 00:00:00', '172.70.90.212', '2022-05-12 12:53:19', '2022-05-12 12:53:19'),
(5195, '2022-05-12 00:00:00', '141.101.99.151', '2022-05-12 12:56:14', '2022-05-12 12:56:14'),
(5196, '2022-05-12 00:00:00', '172.70.162.240', '2022-05-12 12:57:43', '2022-05-12 12:57:43'),
(5197, '2022-05-12 00:00:00', '172.70.162.12', '2022-05-12 12:58:58', '2022-05-12 12:58:58'),
(5198, '2022-05-12 00:00:00', '162.158.163.191', '2022-05-12 13:20:28', '2022-05-12 13:20:28'),
(5199, '2022-05-12 00:00:00', '172.70.135.213', '2022-05-12 14:03:45', '2022-05-12 14:03:45'),
(5200, '2022-05-12 00:00:00', '172.70.90.172', '2022-05-12 14:08:08', '2022-05-12 14:08:08'),
(5201, '2022-05-12 00:00:00', '108.162.246.83', '2022-05-12 14:09:01', '2022-05-12 14:09:01'),
(5202, '2022-05-12 00:00:00', '162.158.170.246', '2022-05-12 14:09:38', '2022-05-12 14:09:38'),
(5203, '2022-05-12 00:00:00', '172.70.85.150', '2022-05-12 14:10:20', '2022-05-12 14:10:20'),
(5204, '2022-05-12 00:00:00', '172.68.39.150', '2022-05-12 14:29:47', '2022-05-12 14:29:47'),
(5205, '2022-05-12 00:00:00', '162.158.163.181', '2022-05-12 14:30:55', '2022-05-12 14:30:55'),
(5206, '2022-05-12 00:00:00', '172.70.147.34', '2022-05-12 15:03:00', '2022-05-12 15:03:00'),
(5207, '2022-05-12 00:00:00', '162.158.170.184', '2022-05-12 15:13:32', '2022-05-12 15:13:32'),
(5208, '2022-05-12 00:00:00', '172.70.188.192', '2022-05-12 15:55:51', '2022-05-12 15:55:51'),
(5209, '2022-05-12 00:00:00', '172.70.92.150', '2022-05-12 17:27:54', '2022-05-12 17:27:54'),
(5210, '2022-05-12 00:00:00', '162.158.170.236', '2022-05-12 17:42:13', '2022-05-12 17:42:13'),
(5211, '2022-05-12 00:00:00', '172.70.92.160', '2022-05-12 17:55:56', '2022-05-12 17:55:56'),
(5212, '2022-05-12 00:00:00', '172.68.39.236', '2022-05-12 17:56:03', '2022-05-12 17:56:03'),
(5213, '2022-05-12 00:00:00', '172.70.92.156', '2022-05-12 18:22:24', '2022-05-12 18:22:24'),
(5214, '2022-05-12 00:00:00', '162.158.222.160', '2022-05-12 22:11:51', '2022-05-12 22:11:51'),
(5215, '2022-05-12 00:00:00', '20.108.23.30', '2022-05-12 22:48:59', '2022-05-12 22:48:59'),
(5216, '2022-05-12 00:00:00', '162.158.162.90', '2022-05-12 23:20:00', '2022-05-12 23:20:00'),
(5217, '2022-05-12 00:00:00', '172.70.206.94', '2022-05-12 23:40:14', '2022-05-12 23:40:14'),
(5218, '2022-05-13 00:00:00', '172.70.142.218', '2022-05-13 00:31:48', '2022-05-13 00:31:48'),
(5219, '2022-05-13 00:00:00', '172.70.188.192', '2022-05-13 01:07:00', '2022-05-13 01:07:00'),
(5220, '2022-05-13 00:00:00', '172.70.142.228', '2022-05-13 01:18:16', '2022-05-13 01:18:16'),
(5221, '2022-05-13 00:00:00', '172.70.142.224', '2022-05-13 01:29:42', '2022-05-13 01:29:42'),
(5222, '2022-05-13 00:00:00', '172.70.147.48', '2022-05-13 01:41:01', '2022-05-13 01:41:01'),
(5223, '2022-05-13 00:00:00', '172.70.175.107', '2022-05-13 02:04:14', '2022-05-13 02:04:14'),
(5224, '2022-05-13 00:00:00', '172.70.135.177', '2022-05-13 03:34:14', '2022-05-13 03:34:14'),
(5225, '2022-05-13 00:00:00', '172.70.188.36', '2022-05-13 03:41:25', '2022-05-13 03:41:25'),
(5226, '2022-05-13 00:00:00', '172.70.134.166', '2022-05-13 04:15:31', '2022-05-13 04:15:31'),
(5227, '2022-05-13 00:00:00', '172.70.34.108', '2022-05-13 04:15:33', '2022-05-13 04:15:33'),
(5228, '2022-05-13 00:00:00', '172.70.147.48', '2022-05-13 04:58:25', '2022-05-13 04:58:25'),
(5229, '2022-05-13 00:00:00', '172.70.147.42', '2022-05-13 06:31:29', '2022-05-13 06:31:29'),
(5230, '2022-05-13 00:00:00', '172.70.38.26', '2022-05-13 06:43:05', '2022-05-13 06:43:05'),
(5231, '2022-05-13 00:00:00', '172.70.189.59', '2022-05-13 06:55:14', '2022-05-13 06:55:14'),
(5232, '2022-05-13 00:00:00', '172.70.92.150', '2022-05-13 07:48:13', '2022-05-13 07:48:13'),
(5233, '2022-05-13 00:00:00', '172.70.189.117', '2022-05-13 08:18:47', '2022-05-13 08:18:47'),
(5234, '2022-05-13 00:00:00', '172.70.92.156', '2022-05-13 09:14:16', '2022-05-13 09:14:16'),
(5235, '2022-05-13 00:00:00', '162.158.162.70', '2022-05-13 11:41:52', '2022-05-13 11:41:52'),
(5236, '2022-05-13 00:00:00', '108.162.245.232', '2022-05-13 12:17:29', '2022-05-13 12:17:29'),
(5237, '2022-05-13 00:00:00', '172.70.189.117', '2022-05-13 13:57:47', '2022-05-13 13:57:47'),
(5238, '2022-05-13 00:00:00', '172.70.174.140', '2022-05-13 15:03:11', '2022-05-13 15:03:11'),
(5239, '2022-05-13 00:00:00', '172.70.175.213', '2022-05-13 15:03:14', '2022-05-13 15:03:14'),
(5240, '2022-05-13 00:00:00', '172.70.92.156', '2022-05-13 15:50:11', '2022-05-13 15:50:11'),
(5241, '2022-05-13 00:00:00', '172.70.122.100', '2022-05-13 16:45:57', '2022-05-13 16:45:57'),
(5242, '2022-05-13 00:00:00', '172.70.92.156', '2022-05-13 16:49:16', '2022-05-13 16:49:16'),
(5243, '2022-05-13 00:00:00', '172.70.114.92', '2022-05-13 17:16:23', '2022-05-13 17:16:23'),
(5244, '2022-05-13 00:00:00', '172.70.114.78', '2022-05-13 17:16:23', '2022-05-13 17:16:23'),
(5245, '2022-05-13 00:00:00', '172.70.114.72', '2022-05-13 17:16:25', '2022-05-13 17:16:25'),
(5246, '2022-05-13 00:00:00', '172.70.142.224', '2022-05-13 17:24:27', '2022-05-13 17:24:27'),
(5247, '2022-05-13 00:00:00', '172.70.210.112', '2022-05-13 19:30:58', '2022-05-13 19:30:58'),
(5248, '2022-05-13 00:00:00', '172.69.34.5', '2022-05-13 21:55:40', '2022-05-13 21:55:40'),
(5249, '2022-05-14 00:00:00', '108.162.246.241', '2022-05-14 01:10:45', '2022-05-14 01:10:45'),
(5250, '2022-05-14 00:00:00', '162.158.107.133', '2022-05-14 01:33:16', '2022-05-14 01:33:16'),
(5251, '2022-05-14 00:00:00', '162.158.107.29', '2022-05-14 01:55:45', '2022-05-14 01:55:45'),
(5252, '2022-05-14 00:00:00', '172.70.85.150', '2022-05-14 02:29:22', '2022-05-14 02:29:22'),
(5253, '2022-05-14 00:00:00', '172.70.147.34', '2022-05-14 02:56:57', '2022-05-14 02:56:57'),
(5254, '2022-05-14 00:00:00', '162.158.170.236', '2022-05-14 03:02:25', '2022-05-14 03:02:25'),
(5255, '2022-05-14 00:00:00', '162.158.106.206', '2022-05-14 03:03:15', '2022-05-14 03:03:15'),
(5256, '2022-05-14 00:00:00', '162.158.163.199', '2022-05-14 03:07:59', '2022-05-14 03:07:59'),
(5257, '2022-05-14 00:00:00', '162.158.170.184', '2022-05-14 03:13:25', '2022-05-14 03:13:25'),
(5258, '2022-05-14 00:00:00', '172.70.142.42', '2022-05-14 03:19:00', '2022-05-14 03:19:00'),
(5259, '2022-05-14 00:00:00', '162.158.170.128', '2022-05-14 03:24:36', '2022-05-14 03:24:36'),
(5260, '2022-05-14 00:00:00', '172.70.92.156', '2022-05-14 03:30:09', '2022-05-14 03:30:09'),
(5261, '2022-05-14 00:00:00', '162.158.162.40', '2022-05-14 03:35:38', '2022-05-14 03:35:38'),
(5262, '2022-05-14 00:00:00', '172.70.142.224', '2022-05-14 03:46:47', '2022-05-14 03:46:47'),
(5263, '2022-05-14 00:00:00', '172.70.147.34', '2022-05-14 03:52:20', '2022-05-14 03:52:20'),
(5264, '2022-05-14 00:00:00', '172.70.142.224', '2022-05-14 04:09:14', '2022-05-14 04:09:14'),
(5265, '2022-05-14 00:00:00', '172.70.142.224', '2022-05-14 04:20:38', '2022-05-14 04:20:38'),
(5266, '2022-05-14 00:00:00', '162.158.170.236', '2022-05-14 04:32:05', '2022-05-14 04:32:05'),
(5267, '2022-05-14 00:00:00', '108.162.246.241', '2022-05-14 04:33:15', '2022-05-14 04:33:15'),
(5268, '2022-05-14 00:00:00', '172.70.126.44', '2022-05-14 04:37:18', '2022-05-14 04:37:18'),
(5269, '2022-05-14 00:00:00', '172.70.189.95', '2022-05-14 05:07:53', '2022-05-14 05:07:53'),
(5270, '2022-05-14 00:00:00', '162.158.170.184', '2022-05-14 05:07:53', '2022-05-14 05:07:53'),
(5271, '2022-05-14 00:00:00', '172.69.70.140', '2022-05-14 07:32:58', '2022-05-14 07:32:58'),
(5272, '2022-05-14 00:00:00', '172.70.142.218', '2022-05-14 08:26:02', '2022-05-14 08:26:02'),
(5273, '2022-05-14 00:00:00', '172.70.142.34', '2022-05-14 08:47:55', '2022-05-14 08:47:55'),
(5274, '2022-05-14 00:00:00', '162.158.162.250', '2022-05-14 09:09:48', '2022-05-14 09:09:48'),
(5275, '2022-05-14 00:00:00', '172.70.92.156', '2022-05-14 09:19:27', '2022-05-14 09:19:27'),
(5276, '2022-05-14 00:00:00', '172.68.253.249', '2022-05-14 09:32:30', '2022-05-14 09:32:30'),
(5277, '2022-05-14 00:00:00', '141.101.104.87', '2022-05-14 09:54:59', '2022-05-14 09:54:59'),
(5278, '2022-05-14 00:00:00', '172.70.142.48', '2022-05-14 09:58:30', '2022-05-14 09:58:30'),
(5279, '2022-05-14 00:00:00', '162.158.50.73', '2022-05-14 11:55:32', '2022-05-14 11:55:32'),
(5280, '2022-05-14 00:00:00', '172.70.218.236', '2022-05-14 13:13:57', '2022-05-14 13:13:57'),
(5281, '2022-05-14 00:00:00', '172.70.218.174', '2022-05-14 13:27:49', '2022-05-14 13:27:49'),
(5282, '2022-05-14 00:00:00', '172.70.218.236', '2022-05-14 13:42:38', '2022-05-14 13:42:38'),
(5283, '2022-05-14 00:00:00', '172.70.142.42', '2022-05-14 14:47:46', '2022-05-14 14:47:46'),
(5284, '2022-05-14 00:00:00', '162.158.170.236', '2022-05-14 15:31:15', '2022-05-14 15:31:15'),
(5285, '2022-05-14 00:00:00', '172.70.211.63', '2022-05-14 15:37:31', '2022-05-14 15:37:31'),
(5286, '2022-05-14 00:00:00', '172.70.142.224', '2022-05-14 15:42:02', '2022-05-14 15:42:02'),
(5287, '2022-05-14 00:00:00', '172.70.189.137', '2022-05-14 15:52:48', '2022-05-14 15:52:48'),
(5288, '2022-05-14 00:00:00', '172.70.92.150', '2022-05-14 16:03:46', '2022-05-14 16:03:46'),
(5289, '2022-05-14 00:00:00', '108.162.229.70', '2022-05-14 16:16:10', '2022-05-14 16:16:10'),
(5290, '2022-05-14 00:00:00', '141.101.68.250', '2022-05-14 16:16:10', '2022-05-14 16:16:10'),
(5291, '2022-05-14 00:00:00', '141.101.69.143', '2022-05-14 16:16:13', '2022-05-14 16:16:13'),
(5292, '2022-05-14 00:00:00', '141.101.69.69', '2022-05-14 16:17:21', '2022-05-14 16:17:21'),
(5293, '2022-05-14 00:00:00', '141.101.69.73', '2022-05-14 16:17:31', '2022-05-14 16:17:31'),
(5294, '2022-05-14 00:00:00', '141.101.69.173', '2022-05-14 16:17:32', '2022-05-14 16:17:32'),
(5295, '2022-05-14 00:00:00', '141.101.68.146', '2022-05-14 16:17:32', '2022-05-14 16:17:32'),
(5296, '2022-05-14 00:00:00', '141.101.68.206', '2022-05-14 16:18:00', '2022-05-14 16:18:00'),
(5297, '2022-05-14 00:00:00', '108.162.229.6', '2022-05-14 16:18:34', '2022-05-14 16:18:34'),
(5298, '2022-05-14 00:00:00', '141.101.68.224', '2022-05-14 16:18:44', '2022-05-14 16:18:44'),
(5299, '2022-05-14 00:00:00', '141.101.69.47', '2022-05-14 16:18:45', '2022-05-14 16:18:45'),
(5300, '2022-05-14 00:00:00', '162.158.170.246', '2022-05-14 16:24:06', '2022-05-14 16:24:06'),
(5301, '2022-05-14 00:00:00', '162.158.162.78', '2022-05-14 16:43:21', '2022-05-14 16:43:21'),
(5302, '2022-05-14 00:00:00', '162.158.163.175', '2022-05-14 17:25:50', '2022-05-14 17:25:50'),
(5303, '2022-05-14 00:00:00', '162.158.235.161', '2022-05-14 17:46:57', '2022-05-14 17:46:57'),
(5304, '2022-05-14 00:00:00', '162.158.162.208', '2022-05-14 17:56:07', '2022-05-14 17:56:07'),
(5305, '2022-05-14 00:00:00', '162.158.170.82', '2022-05-14 18:16:14', '2022-05-14 18:16:14'),
(5306, '2022-05-14 00:00:00', '172.70.142.228', '2022-05-14 18:57:00', '2022-05-14 18:57:00'),
(5307, '2022-05-14 00:00:00', '172.70.178.156', '2022-05-14 22:36:57', '2022-05-14 22:36:57'),
(5308, '2022-05-15 00:00:00', '172.70.142.32', '2022-05-15 00:02:49', '2022-05-15 00:02:49'),
(5309, '2022-05-15 00:00:00', '172.70.142.48', '2022-05-15 00:24:37', '2022-05-15 00:24:37'),
(5310, '2022-05-15 00:00:00', '172.70.147.34', '2022-05-15 00:35:40', '2022-05-15 00:35:40'),
(5311, '2022-05-15 00:00:00', '162.158.162.240', '2022-05-15 01:19:36', '2022-05-15 01:19:36'),
(5312, '2022-05-15 00:00:00', '172.70.142.42', '2022-05-15 01:30:23', '2022-05-15 01:30:23'),
(5313, '2022-05-15 00:00:00', '172.70.142.32', '2022-05-15 01:52:20', '2022-05-15 01:52:20'),
(5314, '2022-05-15 00:00:00', '172.70.142.224', '2022-05-15 05:09:51', '2022-05-15 05:09:51'),
(5315, '2022-05-15 00:00:00', '172.70.162.14', '2022-05-15 06:51:18', '2022-05-15 06:51:18'),
(5316, '2022-05-15 00:00:00', '172.70.147.48', '2022-05-15 07:23:02', '2022-05-15 07:23:02'),
(5317, '2022-05-15 00:00:00', '172.70.54.188', '2022-05-15 07:54:48', '2022-05-15 07:54:48'),
(5318, '2022-05-15 00:00:00', '162.158.62.150', '2022-05-15 08:41:46', '2022-05-15 08:41:46'),
(5319, '2022-05-15 00:00:00', '172.70.92.156', '2022-05-15 10:15:54', '2022-05-15 10:15:54'),
(5320, '2022-05-15 00:00:00', '162.158.170.236', '2022-05-15 11:13:26', '2022-05-15 11:13:26'),
(5321, '2022-05-15 00:00:00', '172.70.188.150', '2022-05-15 13:04:57', '2022-05-15 13:04:57'),
(5322, '2022-05-15 00:00:00', '172.70.142.34', '2022-05-15 14:26:16', '2022-05-15 14:26:16'),
(5323, '2022-05-15 00:00:00', '162.158.170.246', '2022-05-15 15:20:52', '2022-05-15 15:20:52'),
(5324, '2022-05-15 00:00:00', '172.70.92.150', '2022-05-15 15:31:28', '2022-05-15 15:31:28'),
(5325, '2022-05-15 00:00:00', '172.70.142.224', '2022-05-15 15:42:10', '2022-05-15 15:42:10'),
(5326, '2022-05-15 00:00:00', '162.158.179.239', '2022-05-15 15:51:38', '2022-05-15 15:51:38'),
(5327, '2022-05-15 00:00:00', '172.70.142.218', '2022-05-15 16:38:59', '2022-05-15 16:38:59'),
(5328, '2022-05-15 00:00:00', '162.158.162.112', '2022-05-15 17:04:35', '2022-05-15 17:04:35'),
(5329, '2022-05-15 00:00:00', '172.70.175.11', '2022-05-15 19:45:03', '2022-05-15 19:45:03'),
(5330, '2022-05-15 00:00:00', '172.68.18.166', '2022-05-15 21:09:59', '2022-05-15 21:09:59'),
(5331, '2022-05-15 00:00:00', '172.70.134.20', '2022-05-15 21:15:02', '2022-05-15 21:15:02'),
(5332, '2022-05-15 00:00:00', '162.158.90.24', '2022-05-15 21:39:10', '2022-05-15 21:39:10'),
(5333, '2022-05-15 00:00:00', '172.70.250.64', '2022-05-15 21:39:10', '2022-05-15 21:39:10'),
(5334, '2022-05-15 00:00:00', '172.70.250.72', '2022-05-15 21:39:34', '2022-05-15 21:39:34'),
(5335, '2022-05-15 00:00:00', '162.158.90.50', '2022-05-15 21:39:35', '2022-05-15 21:39:35'),
(5336, '2022-05-15 00:00:00', '172.70.90.172', '2022-05-15 22:00:31', '2022-05-15 22:00:31'),
(5337, '2022-05-15 00:00:00', '172.70.174.190', '2022-05-15 22:45:03', '2022-05-15 22:45:03'),
(5338, '2022-05-16 00:00:00', '172.70.142.224', '2022-05-16 02:56:03', '2022-05-16 02:56:03'),
(5339, '2022-05-16 00:00:00', '172.70.135.79', '2022-05-16 03:10:50', '2022-05-16 03:10:50'),
(5340, '2022-05-16 00:00:00', '172.70.142.34', '2022-05-16 03:19:43', '2022-05-16 03:19:43'),
(5341, '2022-05-16 00:00:00', '162.158.163.229', '2022-05-16 03:38:42', '2022-05-16 03:38:42'),
(5342, '2022-05-16 00:00:00', '172.70.38.72', '2022-05-16 04:14:40', '2022-05-16 04:14:40'),
(5343, '2022-05-16 00:00:00', '162.158.78.154', '2022-05-16 04:40:50', '2022-05-16 04:40:50'),
(5344, '2022-05-16 00:00:00', '172.70.174.116', '2022-05-16 06:24:50', '2022-05-16 06:24:50'),
(5345, '2022-05-16 00:00:00', '108.162.229.64', '2022-05-16 06:25:24', '2022-05-16 06:25:24'),
(5346, '2022-05-16 00:00:00', '172.70.38.184', '2022-05-16 07:40:50', '2022-05-16 07:40:50'),
(5347, '2022-05-16 00:00:00', '172.70.35.51', '2022-05-16 07:54:50', '2022-05-16 07:54:50'),
(5348, '2022-05-16 00:00:00', '172.70.38.224', '2022-05-16 08:39:50', '2022-05-16 08:39:50'),
(5349, '2022-05-16 00:00:00', '162.158.163.155', '2022-05-16 09:27:33', '2022-05-16 09:27:33'),
(5350, '2022-05-16 00:00:00', '172.70.135.45', '2022-05-16 09:32:51', '2022-05-16 09:32:51'),
(5351, '2022-05-16 00:00:00', '162.158.162.236', '2022-05-16 09:41:33', '2022-05-16 09:41:33'),
(5352, '2022-05-16 00:00:00', '172.70.147.34', '2022-05-16 09:50:27', '2022-05-16 09:50:27'),
(5353, '2022-05-16 00:00:00', '172.70.135.45', '2022-05-16 10:09:51', '2022-05-16 10:09:51'),
(5354, '2022-05-16 00:00:00', '172.70.134.112', '2022-05-16 11:03:58', '2022-05-16 11:03:58'),
(5355, '2022-05-16 00:00:00', '172.70.142.218', '2022-05-16 11:24:10', '2022-05-16 11:24:10'),
(5356, '2022-05-16 00:00:00', '172.70.142.32', '2022-05-16 11:36:41', '2022-05-16 11:36:41'),
(5357, '2022-05-16 00:00:00', '172.70.175.27', '2022-05-16 11:39:50', '2022-05-16 11:39:50'),
(5358, '2022-05-16 00:00:00', '172.68.10.182', '2022-05-16 12:59:00', '2022-05-16 12:59:00'),
(5359, '2022-05-16 00:00:00', '172.70.142.34', '2022-05-16 13:43:01', '2022-05-16 13:43:01'),
(5360, '2022-05-16 00:00:00', '172.70.188.150', '2022-05-16 14:33:18', '2022-05-16 14:33:18'),
(5361, '2022-05-16 00:00:00', '172.70.92.156', '2022-05-16 14:44:03', '2022-05-16 14:44:03'),
(5362, '2022-05-16 00:00:00', '162.158.170.232', '2022-05-16 14:54:55', '2022-05-16 14:54:55'),
(5363, '2022-05-16 00:00:00', '172.70.34.108', '2022-05-16 15:43:02', '2022-05-16 15:43:02'),
(5364, '2022-05-16 00:00:00', '172.70.38.82', '2022-05-16 15:43:03', '2022-05-16 15:43:03'),
(5365, '2022-05-16 00:00:00', '172.70.135.167', '2022-05-16 15:43:03', '2022-05-16 15:43:03'),
(5366, '2022-05-16 00:00:00', '172.70.188.150', '2022-05-16 15:48:41', '2022-05-16 15:48:41'),
(5367, '2022-05-16 00:00:00', '172.70.92.150', '2022-05-16 15:59:23', '2022-05-16 15:59:23'),
(5368, '2022-05-16 00:00:00', '162.158.170.82', '2022-05-16 17:31:34', '2022-05-16 17:31:34'),
(5369, '2022-05-16 00:00:00', '162.158.163.227', '2022-05-16 17:46:20', '2022-05-16 17:46:20'),
(5370, '2022-05-16 00:00:00', '172.70.142.34', '2022-05-16 18:07:15', '2022-05-16 18:07:15'),
(5371, '2022-05-16 00:00:00', '172.70.250.64', '2022-05-16 19:05:05', '2022-05-16 19:05:05'),
(5372, '2022-05-16 00:00:00', '172.70.251.125', '2022-05-16 19:05:06', '2022-05-16 19:05:06'),
(5373, '2022-05-16 00:00:00', '162.158.107.35', '2022-05-16 19:12:40', '2022-05-16 19:12:40'),
(5374, '2022-05-16 00:00:00', '172.70.135.35', '2022-05-16 20:10:57', '2022-05-16 20:10:57'),
(5375, '2022-05-16 00:00:00', '108.162.246.183', '2022-05-16 20:22:40', '2022-05-16 20:22:40'),
(5376, '2022-05-16 00:00:00', '172.70.134.90', '2022-05-16 20:55:57', '2022-05-16 20:55:57'),
(5377, '2022-05-16 00:00:00', '162.158.79.67', '2022-05-16 22:09:51', '2022-05-16 22:09:51'),
(5378, '2022-05-16 00:00:00', '162.158.163.181', '2022-05-16 22:13:17', '2022-05-16 22:13:17'),
(5379, '2022-05-16 00:00:00', '172.70.142.218', '2022-05-16 23:36:18', '2022-05-16 23:36:18'),
(5380, '2022-05-17 00:00:00', '172.70.147.42', '2022-05-17 01:07:08', '2022-05-17 01:07:08'),
(5381, '2022-05-17 00:00:00', '162.158.162.250', '2022-05-17 02:01:08', '2022-05-17 02:01:08'),
(5382, '2022-05-17 00:00:00', '172.70.82.46', '2022-05-17 02:34:02', '2022-05-17 02:34:02'),
(5383, '2022-05-17 00:00:00', '172.70.92.156', '2022-05-17 04:11:46', '2022-05-17 04:11:46'),
(5384, '2022-05-17 00:00:00', '172.70.142.42', '2022-05-17 04:53:46', '2022-05-17 04:53:46'),
(5385, '2022-05-17 00:00:00', '162.158.170.236', '2022-05-17 05:35:54', '2022-05-17 05:35:54'),
(5386, '2022-05-17 00:00:00', '172.70.174.96', '2022-05-17 06:12:58', '2022-05-17 06:12:58'),
(5387, '2022-05-17 00:00:00', '172.70.147.42', '2022-05-17 06:31:24', '2022-05-17 06:31:24'),
(5388, '2022-05-17 00:00:00', '172.70.142.48', '2022-05-17 06:40:46', '2022-05-17 06:40:46'),
(5389, '2022-05-17 00:00:00', '172.70.147.42', '2022-05-17 06:49:59', '2022-05-17 06:49:59'),
(5390, '2022-05-17 00:00:00', '162.158.162.70', '2022-05-17 06:59:41', '2022-05-17 06:59:41'),
(5391, '2022-05-17 00:00:00', '172.70.175.107', '2022-05-17 07:20:29', '2022-05-17 07:20:29'),
(5392, '2022-05-17 00:00:00', '162.158.170.184', '2022-05-17 07:31:40', '2022-05-17 07:31:40'),
(5393, '2022-05-17 00:00:00', '172.70.38.16', '2022-05-17 07:42:59', '2022-05-17 07:42:59'),
(5394, '2022-05-17 00:00:00', '172.70.147.42', '2022-05-17 08:02:23', '2022-05-17 08:02:23'),
(5395, '2022-05-17 00:00:00', '172.70.188.36', '2022-05-17 08:24:36', '2022-05-17 08:24:36'),
(5396, '2022-05-17 00:00:00', '162.158.78.114', '2022-05-17 08:27:58', '2022-05-17 08:27:58'),
(5397, '2022-05-17 00:00:00', '162.158.78.214', '2022-05-17 09:12:59', '2022-05-17 09:12:59'),
(5398, '2022-05-17 00:00:00', '162.158.78.174', '2022-05-17 09:14:59', '2022-05-17 09:14:59'),
(5399, '2022-05-17 00:00:00', '172.70.51.143', '2022-05-17 10:10:58', '2022-05-17 10:10:58'),
(5400, '2022-05-17 00:00:00', '162.158.78.112', '2022-05-17 10:43:00', '2022-05-17 10:43:00'),
(5401, '2022-05-17 00:00:00', '108.162.210.152', '2022-05-17 11:11:01', '2022-05-17 11:11:01'),
(5402, '2022-05-17 00:00:00', '172.70.54.86', '2022-05-17 11:20:29', '2022-05-17 11:20:29'),
(5403, '2022-05-17 00:00:00', '162.158.170.170', '2022-05-17 13:51:43', '2022-05-17 13:51:43'),
(5404, '2022-05-17 00:00:00', '162.158.163.175', '2022-05-17 14:02:02', '2022-05-17 14:02:02'),
(5405, '2022-05-17 00:00:00', '172.68.246.60', '2022-05-17 14:13:34', '2022-05-17 14:13:34'),
(5406, '2022-05-17 00:00:00', '172.68.10.200', '2022-05-17 14:18:33', '2022-05-17 14:18:33'),
(5407, '2022-05-17 00:00:00', '108.162.246.51', '2022-05-17 14:44:33', '2022-05-17 14:44:33'),
(5408, '2022-05-17 00:00:00', '162.158.170.184', '2022-05-17 14:54:01', '2022-05-17 14:54:01'),
(5409, '2022-05-17 00:00:00', '172.70.92.160', '2022-05-17 15:42:36', '2022-05-17 15:42:36'),
(5410, '2022-05-17 00:00:00', '172.70.92.160', '2022-05-17 16:24:47', '2022-05-17 16:24:47'),
(5411, '2022-05-17 00:00:00', '172.70.142.42', '2022-05-17 16:48:26', '2022-05-17 16:48:26'),
(5412, '2022-05-17 00:00:00', '162.158.92.190', '2022-05-17 18:09:37', '2022-05-17 18:09:37'),
(5413, '2022-05-17 00:00:00', '162.158.91.193', '2022-05-17 18:09:40', '2022-05-17 18:09:40'),
(5414, '2022-05-17 00:00:00', '162.158.90.176', '2022-05-17 18:09:40', '2022-05-17 18:09:40'),
(5415, '2022-05-17 00:00:00', '172.69.34.45', '2022-05-17 18:33:36', '2022-05-17 18:33:36'),
(5416, '2022-05-17 00:00:00', '172.70.174.194', '2022-05-17 19:40:18', '2022-05-17 19:40:18'),
(5417, '2022-05-17 00:00:00', '172.69.34.47', '2022-05-17 20:17:27', '2022-05-17 20:17:27');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(5418, '2022-05-17 00:00:00', '172.70.210.30', '2022-05-17 21:20:58', '2022-05-17 21:20:58'),
(5419, '2022-05-17 00:00:00', '172.70.131.193', '2022-05-17 21:57:12', '2022-05-17 21:57:12'),
(5420, '2022-05-17 00:00:00', '172.68.133.62', '2022-05-17 22:32:32', '2022-05-17 22:32:32'),
(5421, '2022-05-17 00:00:00', '172.69.134.94', '2022-05-17 22:32:33', '2022-05-17 22:32:33'),
(5422, '2022-05-17 00:00:00', '108.162.245.28', '2022-05-17 22:32:33', '2022-05-17 22:32:33'),
(5423, '2022-05-17 00:00:00', '162.158.106.206', '2022-05-17 22:32:33', '2022-05-17 22:32:33'),
(5424, '2022-05-17 00:00:00', '172.70.211.63', '2022-05-17 22:32:34', '2022-05-17 22:32:34'),
(5425, '2022-05-17 00:00:00', '172.70.211.115', '2022-05-17 22:32:34', '2022-05-17 22:32:34'),
(5426, '2022-05-17 00:00:00', '108.162.245.222', '2022-05-17 22:32:34', '2022-05-17 22:32:34'),
(5427, '2022-05-17 00:00:00', '162.158.106.242', '2022-05-17 22:32:34', '2022-05-17 22:32:34'),
(5428, '2022-05-17 00:00:00', '172.69.34.25', '2022-05-17 22:32:35', '2022-05-17 22:32:35'),
(5429, '2022-05-17 00:00:00', '108.162.245.252', '2022-05-17 22:32:35', '2022-05-17 22:32:35'),
(5430, '2022-05-17 00:00:00', '172.68.143.236', '2022-05-17 22:32:36', '2022-05-17 22:32:36'),
(5431, '2022-05-17 00:00:00', '108.162.245.214', '2022-05-17 22:32:37', '2022-05-17 22:32:37'),
(5432, '2022-05-17 00:00:00', '162.158.107.165', '2022-05-17 22:32:37', '2022-05-17 22:32:37'),
(5433, '2022-05-17 00:00:00', '172.70.206.154', '2022-05-17 22:32:37', '2022-05-17 22:32:37'),
(5434, '2022-05-17 00:00:00', '172.69.134.22', '2022-05-17 22:32:38', '2022-05-17 22:32:38'),
(5435, '2022-05-17 00:00:00', '162.158.107.191', '2022-05-17 22:32:39', '2022-05-17 22:32:39'),
(5436, '2022-05-17 00:00:00', '108.162.245.116', '2022-05-17 22:32:39', '2022-05-17 22:32:39'),
(5437, '2022-05-17 00:00:00', '108.162.245.8', '2022-05-17 22:32:39', '2022-05-17 22:32:39'),
(5438, '2022-05-17 00:00:00', '162.158.107.189', '2022-05-17 22:32:40', '2022-05-17 22:32:40'),
(5439, '2022-05-17 00:00:00', '162.158.107.95', '2022-05-17 22:32:40', '2022-05-17 22:32:40'),
(5440, '2022-05-17 00:00:00', '162.158.107.39', '2022-05-17 22:32:40', '2022-05-17 22:32:40'),
(5441, '2022-05-17 00:00:00', '162.158.107.35', '2022-05-17 22:32:41', '2022-05-17 22:32:41'),
(5442, '2022-05-17 00:00:00', '162.158.106.68', '2022-05-17 22:32:42', '2022-05-17 22:32:42'),
(5443, '2022-05-17 00:00:00', '172.70.210.196', '2022-05-17 22:32:43', '2022-05-17 22:32:43'),
(5444, '2022-05-17 00:00:00', '162.158.166.176', '2022-05-17 22:32:43', '2022-05-17 22:32:43'),
(5445, '2022-05-17 00:00:00', '162.158.107.237', '2022-05-17 22:32:43', '2022-05-17 22:32:43'),
(5446, '2022-05-17 00:00:00', '162.158.166.158', '2022-05-17 22:32:43', '2022-05-17 22:32:43'),
(5447, '2022-05-17 00:00:00', '162.158.166.230', '2022-05-17 22:32:44', '2022-05-17 22:32:44'),
(5448, '2022-05-17 00:00:00', '172.70.214.32', '2022-05-17 22:32:44', '2022-05-17 22:32:44'),
(5449, '2022-05-17 00:00:00', '172.70.206.170', '2022-05-17 22:32:44', '2022-05-17 22:32:44'),
(5450, '2022-05-17 00:00:00', '172.69.33.16', '2022-05-17 22:32:44', '2022-05-17 22:32:44'),
(5451, '2022-05-17 00:00:00', '108.162.245.128', '2022-05-17 22:32:45', '2022-05-17 22:32:45'),
(5452, '2022-05-17 00:00:00', '172.70.211.147', '2022-05-17 22:32:45', '2022-05-17 22:32:45'),
(5453, '2022-05-17 00:00:00', '108.162.245.202', '2022-05-17 22:32:47', '2022-05-17 22:32:47'),
(5454, '2022-05-17 00:00:00', '162.158.106.16', '2022-05-17 22:32:47', '2022-05-17 22:32:47'),
(5455, '2022-05-17 00:00:00', '108.162.246.219', '2022-05-17 22:32:47', '2022-05-17 22:32:47'),
(5456, '2022-05-17 00:00:00', '108.162.246.83', '2022-05-17 22:32:50', '2022-05-17 22:32:50'),
(5457, '2022-05-18 00:00:00', '162.158.222.148', '2022-05-18 00:36:53', '2022-05-18 00:36:53'),
(5458, '2022-05-18 00:00:00', '162.158.163.157', '2022-05-18 02:31:47', '2022-05-18 02:31:47'),
(5459, '2022-05-18 00:00:00', '172.70.189.137', '2022-05-18 02:42:51', '2022-05-18 02:42:51'),
(5460, '2022-05-18 00:00:00', '172.70.147.48', '2022-05-18 03:05:27', '2022-05-18 03:05:27'),
(5461, '2022-05-18 00:00:00', '162.158.170.246', '2022-05-18 03:11:03', '2022-05-18 03:11:03'),
(5462, '2022-05-18 00:00:00', '162.158.162.236', '2022-05-18 03:16:51', '2022-05-18 03:16:51'),
(5463, '2022-05-18 00:00:00', '172.70.251.125', '2022-05-18 03:18:28', '2022-05-18 03:18:28'),
(5464, '2022-05-18 00:00:00', '172.70.188.90', '2022-05-18 03:22:30', '2022-05-18 03:22:30'),
(5465, '2022-05-18 00:00:00', '162.158.170.246', '2022-05-18 03:34:05', '2022-05-18 03:34:05'),
(5466, '2022-05-18 00:00:00', '162.158.91.223', '2022-05-18 04:10:56', '2022-05-18 04:10:56'),
(5467, '2022-05-18 00:00:00', '172.70.131.121', '2022-05-18 04:42:10', '2022-05-18 04:42:10'),
(5468, '2022-05-18 00:00:00', '172.70.130.176', '2022-05-18 06:12:10', '2022-05-18 06:12:10'),
(5469, '2022-05-18 00:00:00', '141.101.84.220', '2022-05-18 07:07:59', '2022-05-18 07:07:59'),
(5470, '2022-05-18 00:00:00', '172.70.178.74', '2022-05-18 07:42:10', '2022-05-18 07:42:10'),
(5471, '2022-05-18 00:00:00', '172.70.147.32', '2022-05-18 08:19:53', '2022-05-18 08:19:53'),
(5472, '2022-05-18 00:00:00', '172.70.242.100', '2022-05-18 08:28:53', '2022-05-18 08:28:53'),
(5473, '2022-05-18 00:00:00', '172.70.114.92', '2022-05-18 08:29:40', '2022-05-18 08:29:40'),
(5474, '2022-05-18 00:00:00', '172.70.189.117', '2022-05-18 08:35:14', '2022-05-18 08:35:14'),
(5475, '2022-05-18 00:00:00', '172.70.218.198', '2022-05-18 08:55:11', '2022-05-18 08:55:11'),
(5476, '2022-05-18 00:00:00', '162.158.90.24', '2022-05-18 09:20:21', '2022-05-18 09:20:21'),
(5477, '2022-05-18 00:00:00', '162.158.170.82', '2022-05-18 09:28:59', '2022-05-18 09:28:59'),
(5478, '2022-05-18 00:00:00', '172.70.188.192', '2022-05-18 09:36:20', '2022-05-18 09:36:20'),
(5479, '2022-05-18 00:00:00', '162.158.170.184', '2022-05-18 09:44:39', '2022-05-18 09:44:39'),
(5480, '2022-05-18 00:00:00', '162.158.170.236', '2022-05-18 09:50:39', '2022-05-18 09:50:39'),
(5481, '2022-05-18 00:00:00', '162.158.170.184', '2022-05-18 10:12:03', '2022-05-18 10:12:03'),
(5482, '2022-05-18 00:00:00', '172.70.142.48', '2022-05-18 10:19:47', '2022-05-18 10:19:47'),
(5483, '2022-05-18 00:00:00', '172.70.218.198', '2022-05-18 11:41:50', '2022-05-18 11:41:50'),
(5484, '2022-05-18 00:00:00', '162.158.78.182', '2022-05-18 11:46:12', '2022-05-18 11:46:12'),
(5485, '2022-05-18 00:00:00', '172.70.218.28', '2022-05-18 11:48:44', '2022-05-18 11:48:44'),
(5486, '2022-05-18 00:00:00', '172.70.218.198', '2022-05-18 11:56:23', '2022-05-18 11:56:23'),
(5487, '2022-05-18 00:00:00', '162.158.48.62', '2022-05-18 12:02:13', '2022-05-18 12:02:13'),
(5488, '2022-05-18 00:00:00', '162.158.227.238', '2022-05-18 12:23:19', '2022-05-18 12:23:19'),
(5489, '2022-05-18 00:00:00', '172.70.218.28', '2022-05-18 12:24:13', '2022-05-18 12:24:13'),
(5490, '2022-05-18 00:00:00', '162.158.48.128', '2022-05-18 12:24:49', '2022-05-18 12:24:49'),
(5491, '2022-05-18 00:00:00', '172.70.218.236', '2022-05-18 12:25:43', '2022-05-18 12:25:43'),
(5492, '2022-05-18 00:00:00', '162.158.227.224', '2022-05-18 12:29:37', '2022-05-18 12:29:37'),
(5493, '2022-05-18 00:00:00', '172.70.135.63', '2022-05-18 12:31:13', '2022-05-18 12:31:13'),
(5494, '2022-05-18 00:00:00', '162.158.227.234', '2022-05-18 12:33:00', '2022-05-18 12:33:00'),
(5495, '2022-05-18 00:00:00', '162.158.235.43', '2022-05-18 12:35:23', '2022-05-18 12:35:23'),
(5496, '2022-05-18 00:00:00', '172.70.218.28', '2022-05-18 12:36:44', '2022-05-18 12:36:44'),
(5497, '2022-05-18 00:00:00', '162.158.235.161', '2022-05-18 12:37:47', '2022-05-18 12:37:47'),
(5498, '2022-05-18 00:00:00', '172.70.218.174', '2022-05-18 12:40:14', '2022-05-18 12:40:14'),
(5499, '2022-05-18 00:00:00', '162.158.227.238', '2022-05-18 12:45:38', '2022-05-18 12:45:38'),
(5500, '2022-05-18 00:00:00', '172.70.218.236', '2022-05-18 12:49:05', '2022-05-18 12:49:05'),
(5501, '2022-05-18 00:00:00', '172.70.218.28', '2022-05-18 12:50:18', '2022-05-18 12:50:18'),
(5502, '2022-05-18 00:00:00', '162.158.227.240', '2022-05-18 12:51:45', '2022-05-18 12:51:45'),
(5503, '2022-05-18 00:00:00', '162.158.235.81', '2022-05-18 12:59:16', '2022-05-18 12:59:16'),
(5504, '2022-05-18 00:00:00', '172.70.218.174', '2022-05-18 13:01:28', '2022-05-18 13:01:28'),
(5505, '2022-05-18 00:00:00', '172.70.218.236', '2022-05-18 13:03:03', '2022-05-18 13:03:03'),
(5506, '2022-05-18 00:00:00', '172.70.218.28', '2022-05-18 13:04:52', '2022-05-18 13:04:52'),
(5507, '2022-05-18 00:00:00', '172.70.218.198', '2022-05-18 13:08:08', '2022-05-18 13:08:08'),
(5508, '2022-05-18 00:00:00', '162.158.48.128', '2022-05-18 13:10:09', '2022-05-18 13:10:09'),
(5509, '2022-05-18 00:00:00', '162.158.235.43', '2022-05-18 13:11:27', '2022-05-18 13:11:27'),
(5510, '2022-05-18 00:00:00', '172.70.174.78', '2022-05-18 13:16:12', '2022-05-18 13:16:12'),
(5511, '2022-05-18 00:00:00', '162.158.235.81', '2022-05-18 13:23:38', '2022-05-18 13:23:38'),
(5512, '2022-05-18 00:00:00', '172.70.218.28', '2022-05-18 13:25:03', '2022-05-18 13:25:03'),
(5513, '2022-05-18 00:00:00', '162.158.235.181', '2022-05-18 13:31:35', '2022-05-18 13:31:35'),
(5514, '2022-05-18 00:00:00', '172.70.34.202', '2022-05-18 13:31:42', '2022-05-18 13:31:42'),
(5515, '2022-05-18 00:00:00', '162.158.78.118', '2022-05-18 13:32:58', '2022-05-18 13:32:58'),
(5516, '2022-05-18 00:00:00', '172.70.147.42', '2022-05-18 13:44:13', '2022-05-18 13:44:13'),
(5517, '2022-05-18 00:00:00', '162.158.170.184', '2022-05-18 13:50:34', '2022-05-18 13:50:34'),
(5518, '2022-05-18 00:00:00', '172.70.134.192', '2022-05-18 14:01:12', '2022-05-18 14:01:12'),
(5519, '2022-05-18 00:00:00', '162.158.222.118', '2022-05-18 14:02:48', '2022-05-18 14:02:48'),
(5520, '2022-05-18 00:00:00', '172.70.174.14', '2022-05-18 14:49:42', '2022-05-18 14:49:42'),
(5521, '2022-05-18 00:00:00', '172.70.189.117', '2022-05-18 14:54:31', '2022-05-18 14:54:31'),
(5522, '2022-05-18 00:00:00', '172.70.142.228', '2022-05-18 15:09:45', '2022-05-18 15:09:45'),
(5523, '2022-05-18 00:00:00', '172.70.38.140', '2022-05-18 15:12:10', '2022-05-18 15:12:10'),
(5524, '2022-05-18 00:00:00', '141.101.77.200', '2022-05-18 15:17:15', '2022-05-18 15:17:15'),
(5525, '2022-05-18 00:00:00', '172.70.142.42', '2022-05-18 15:25:31', '2022-05-18 15:25:31'),
(5526, '2022-05-18 00:00:00', '172.70.142.224', '2022-05-18 15:38:54', '2022-05-18 15:38:54'),
(5527, '2022-05-18 00:00:00', '162.158.162.90', '2022-05-18 15:45:54', '2022-05-18 15:45:54'),
(5528, '2022-05-18 00:00:00', '172.70.147.48', '2022-05-18 15:54:40', '2022-05-18 15:54:40'),
(5529, '2022-05-18 00:00:00', '172.70.142.42', '2022-05-18 17:12:08', '2022-05-18 17:12:08'),
(5530, '2022-05-18 00:00:00', '162.158.163.9', '2022-05-18 17:24:10', '2022-05-18 17:24:10'),
(5531, '2022-05-18 00:00:00', '162.158.163.155', '2022-05-18 17:48:22', '2022-05-18 17:48:22'),
(5532, '2022-05-18 00:00:00', '162.158.170.128', '2022-05-18 17:54:20', '2022-05-18 17:54:20'),
(5533, '2022-05-18 00:00:00', '108.162.246.151', '2022-05-18 17:59:15', '2022-05-18 17:59:15'),
(5534, '2022-05-18 00:00:00', '141.101.76.36', '2022-05-18 18:11:18', '2022-05-18 18:11:18'),
(5535, '2022-05-18 00:00:00', '162.158.170.170', '2022-05-18 18:18:07', '2022-05-18 18:18:07'),
(5536, '2022-05-18 00:00:00', '162.158.107.65', '2022-05-18 18:23:57', '2022-05-18 18:23:57'),
(5537, '2022-05-18 00:00:00', '172.70.114.90', '2022-05-18 20:42:26', '2022-05-18 20:42:26'),
(5538, '2022-05-18 00:00:00', '172.70.114.92', '2022-05-18 20:42:27', '2022-05-18 20:42:27'),
(5539, '2022-05-18 00:00:00', '162.158.170.82', '2022-05-18 22:53:06', '2022-05-18 22:53:06'),
(5540, '2022-05-18 00:00:00', '172.70.92.156', '2022-05-18 23:03:12', '2022-05-18 23:03:12'),
(5541, '2022-05-18 00:00:00', '172.70.92.160', '2022-05-18 23:13:17', '2022-05-18 23:13:17'),
(5542, '2022-05-18 00:00:00', '172.70.92.156', '2022-05-18 23:23:12', '2022-05-18 23:23:12'),
(5543, '2022-05-18 00:00:00', '172.69.34.43', '2022-05-18 23:31:51', '2022-05-18 23:31:51'),
(5544, '2022-05-18 00:00:00', '172.69.33.226', '2022-05-18 23:32:08', '2022-05-18 23:32:08'),
(5545, '2022-05-18 00:00:00', '172.70.142.48', '2022-05-18 23:54:42', '2022-05-18 23:54:42'),
(5546, '2022-05-19 00:00:00', '172.70.251.51', '2022-05-19 00:14:12', '2022-05-19 00:14:12'),
(5547, '2022-05-19 00:00:00', '172.70.189.95', '2022-05-19 00:34:50', '2022-05-19 00:34:50'),
(5548, '2022-05-19 00:00:00', '172.70.142.42', '2022-05-19 00:50:27', '2022-05-19 00:50:27'),
(5549, '2022-05-19 00:00:00', '172.70.130.82', '2022-05-19 03:37:22', '2022-05-19 03:37:22'),
(5550, '2022-05-19 00:00:00', '172.70.130.156', '2022-05-19 03:37:23', '2022-05-19 03:37:23'),
(5551, '2022-05-19 00:00:00', '172.70.189.95', '2022-05-19 04:14:27', '2022-05-19 04:14:27'),
(5552, '2022-05-19 00:00:00', '162.158.227.234', '2022-05-19 04:18:11', '2022-05-19 04:18:11'),
(5553, '2022-05-19 00:00:00', '172.70.174.236', '2022-05-19 04:19:12', '2022-05-19 04:19:12'),
(5554, '2022-05-19 00:00:00', '172.70.188.36', '2022-05-19 04:20:14', '2022-05-19 04:20:14'),
(5555, '2022-05-19 00:00:00', '172.70.218.198', '2022-05-19 04:26:17', '2022-05-19 04:26:17'),
(5556, '2022-05-19 00:00:00', '162.158.48.128', '2022-05-19 04:27:04', '2022-05-19 04:27:04'),
(5557, '2022-05-19 00:00:00', '172.70.142.224', '2022-05-19 04:27:44', '2022-05-19 04:27:44'),
(5558, '2022-05-19 00:00:00', '172.70.218.174', '2022-05-19 04:30:00', '2022-05-19 04:30:00'),
(5559, '2022-05-19 00:00:00', '162.158.227.236', '2022-05-19 04:30:55', '2022-05-19 04:30:55'),
(5560, '2022-05-19 00:00:00', '162.158.235.43', '2022-05-19 04:33:13', '2022-05-19 04:33:13'),
(5561, '2022-05-19 00:00:00', '172.70.218.198', '2022-05-19 04:44:07', '2022-05-19 04:44:07'),
(5562, '2022-05-19 00:00:00', '162.158.78.164', '2022-05-19 04:47:05', '2022-05-19 04:47:05'),
(5563, '2022-05-19 00:00:00', '172.70.218.174', '2022-05-19 04:47:42', '2022-05-19 04:47:42'),
(5564, '2022-05-19 00:00:00', '162.158.235.181', '2022-05-19 05:27:27', '2022-05-19 05:27:27'),
(5565, '2022-05-19 00:00:00', '172.70.92.156', '2022-05-19 05:38:09', '2022-05-19 05:38:09'),
(5566, '2022-05-19 00:00:00', '172.70.142.42', '2022-05-19 05:44:48', '2022-05-19 05:44:48'),
(5567, '2022-05-19 00:00:00', '172.70.92.156', '2022-05-19 06:06:38', '2022-05-19 06:06:38'),
(5568, '2022-05-19 00:00:00', '172.70.142.32', '2022-05-19 06:06:39', '2022-05-19 06:06:39'),
(5569, '2022-05-19 00:00:00', '162.158.227.224', '2022-05-19 06:10:24', '2022-05-19 06:10:24'),
(5570, '2022-05-19 00:00:00', '162.158.170.184', '2022-05-19 06:13:44', '2022-05-19 06:13:44'),
(5571, '2022-05-19 00:00:00', '172.70.135.45', '2022-05-19 06:21:01', '2022-05-19 06:21:01'),
(5572, '2022-05-19 00:00:00', '172.70.142.224', '2022-05-19 07:12:07', '2022-05-19 07:12:07'),
(5573, '2022-05-19 00:00:00', '108.162.241.110', '2022-05-19 07:35:37', '2022-05-19 07:35:37'),
(5574, '2022-05-19 00:00:00', '172.70.175.233', '2022-05-19 07:47:05', '2022-05-19 07:47:05'),
(5575, '2022-05-19 00:00:00', '172.70.142.34', '2022-05-19 07:48:55', '2022-05-19 07:48:55'),
(5576, '2022-05-19 00:00:00', '172.70.218.198', '2022-05-19 07:58:41', '2022-05-19 07:58:41'),
(5577, '2022-05-19 00:00:00', '172.68.244.64', '2022-05-19 08:07:58', '2022-05-19 08:07:58'),
(5578, '2022-05-19 00:00:00', '172.70.92.160', '2022-05-19 08:12:37', '2022-05-19 08:12:37'),
(5579, '2022-05-19 00:00:00', '162.158.78.158', '2022-05-19 08:47:05', '2022-05-19 08:47:05'),
(5580, '2022-05-19 00:00:00', '172.70.218.174', '2022-05-19 10:20:40', '2022-05-19 10:20:40'),
(5581, '2022-05-19 00:00:00', '172.70.218.28', '2022-05-19 11:14:01', '2022-05-19 11:14:01'),
(5582, '2022-05-19 00:00:00', '172.70.174.14', '2022-05-19 11:42:24', '2022-05-19 11:42:24'),
(5583, '2022-05-19 00:00:00', '172.70.218.198', '2022-05-19 11:55:29', '2022-05-19 11:55:29'),
(5584, '2022-05-19 00:00:00', '172.70.218.236', '2022-05-19 11:57:14', '2022-05-19 11:57:14'),
(5585, '2022-05-19 00:00:00', '172.70.218.174', '2022-05-19 12:01:18', '2022-05-19 12:01:18'),
(5586, '2022-05-19 00:00:00', '162.158.227.240', '2022-05-19 12:03:56', '2022-05-19 12:03:56'),
(5587, '2022-05-19 00:00:00', '172.70.162.188', '2022-05-19 12:03:59', '2022-05-19 12:03:59'),
(5588, '2022-05-19 00:00:00', '172.70.218.236', '2022-05-19 12:14:17', '2022-05-19 12:14:17'),
(5589, '2022-05-19 00:00:00', '172.70.218.174', '2022-05-19 12:22:13', '2022-05-19 12:22:13'),
(5590, '2022-05-19 00:00:00', '172.70.218.198', '2022-05-19 12:26:31', '2022-05-19 12:26:31'),
(5591, '2022-05-19 00:00:00', '172.70.218.236', '2022-05-19 12:27:29', '2022-05-19 12:27:29'),
(5592, '2022-05-19 00:00:00', '162.158.235.161', '2022-05-19 12:35:36', '2022-05-19 12:35:36'),
(5593, '2022-05-19 00:00:00', '172.70.218.174', '2022-05-19 12:36:04', '2022-05-19 12:36:04'),
(5594, '2022-05-19 00:00:00', '172.70.218.198', '2022-05-19 12:38:35', '2022-05-19 12:38:35'),
(5595, '2022-05-19 00:00:00', '162.158.162.168', '2022-05-19 12:46:12', '2022-05-19 12:46:12'),
(5596, '2022-05-19 00:00:00', '172.70.135.63', '2022-05-19 12:50:27', '2022-05-19 12:50:27'),
(5597, '2022-05-19 00:00:00', '162.158.162.156', '2022-05-19 13:04:04', '2022-05-19 13:04:04'),
(5598, '2022-05-19 00:00:00', '172.70.174.86', '2022-05-19 13:29:43', '2022-05-19 13:29:43'),
(5599, '2022-05-19 00:00:00', '172.70.34.202', '2022-05-19 13:29:49', '2022-05-19 13:29:49'),
(5600, '2022-05-19 00:00:00', '162.158.170.246', '2022-05-19 13:51:58', '2022-05-19 13:51:58'),
(5601, '2022-05-19 00:00:00', '172.70.135.59', '2022-05-19 14:53:29', '2022-05-19 14:53:29'),
(5602, '2022-05-19 00:00:00', '172.70.142.34', '2022-05-19 15:08:43', '2022-05-19 15:08:43'),
(5603, '2022-05-19 00:00:00', '172.70.242.210', '2022-05-19 15:33:58', '2022-05-19 15:33:58'),
(5604, '2022-05-19 00:00:00', '172.70.242.58', '2022-05-19 15:33:58', '2022-05-19 15:33:58'),
(5605, '2022-05-19 00:00:00', '172.68.118.124', '2022-05-19 21:14:36', '2022-05-19 21:14:36'),
(5606, '2022-05-19 00:00:00', '172.70.222.140', '2022-05-19 21:14:40', '2022-05-19 21:14:40'),
(5607, '2022-05-19 00:00:00', '162.158.118.124', '2022-05-19 21:14:44', '2022-05-19 21:14:44'),
(5608, '2022-05-19 00:00:00', '172.68.118.246', '2022-05-19 21:14:47', '2022-05-19 21:14:47'),
(5609, '2022-05-19 00:00:00', '172.70.147.48', '2022-05-19 23:32:49', '2022-05-19 23:32:49'),
(5610, '2022-05-19 00:00:00', '162.158.163.217', '2022-05-19 23:38:20', '2022-05-19 23:38:20'),
(5611, '2022-05-19 00:00:00', '141.101.77.200', '2022-05-19 23:44:03', '2022-05-19 23:44:03'),
(5612, '2022-05-20 00:00:00', '172.70.142.224', '2022-05-20 00:00:26', '2022-05-20 00:00:26'),
(5613, '2022-05-20 00:00:00', '162.158.227.224', '2022-05-20 04:27:16', '2022-05-20 04:27:16'),
(5614, '2022-05-20 00:00:00', '172.70.142.224', '2022-05-20 05:45:39', '2022-05-20 05:45:39'),
(5615, '2022-05-20 00:00:00', '141.101.76.36', '2022-05-20 05:50:56', '2022-05-20 05:50:56'),
(5616, '2022-05-20 00:00:00', '141.101.104.71', '2022-05-20 05:50:58', '2022-05-20 05:50:58'),
(5617, '2022-05-20 00:00:00', '172.70.188.150', '2022-05-20 06:11:56', '2022-05-20 06:11:56'),
(5618, '2022-05-20 00:00:00', '162.158.163.199', '2022-05-20 06:34:27', '2022-05-20 06:34:27'),
(5619, '2022-05-20 00:00:00', '172.70.92.160', '2022-05-20 06:46:50', '2022-05-20 06:46:50'),
(5620, '2022-05-20 00:00:00', '172.70.147.34', '2022-05-20 07:07:55', '2022-05-20 07:07:55'),
(5621, '2022-05-20 00:00:00', '162.158.162.90', '2022-05-20 08:11:31', '2022-05-20 08:11:31'),
(5622, '2022-05-20 00:00:00', '141.101.104.163', '2022-05-20 08:22:56', '2022-05-20 08:22:56'),
(5623, '2022-05-20 00:00:00', '141.101.76.36', '2022-05-20 09:29:22', '2022-05-20 09:29:22'),
(5624, '2022-05-20 00:00:00', '172.70.142.48', '2022-05-20 10:26:46', '2022-05-20 10:26:46'),
(5625, '2022-05-20 00:00:00', '172.70.85.154', '2022-05-20 11:01:07', '2022-05-20 11:01:07'),
(5626, '2022-05-20 00:00:00', '172.70.188.192', '2022-05-20 12:09:56', '2022-05-20 12:09:56'),
(5627, '2022-05-20 00:00:00', '172.70.131.143', '2022-05-20 12:55:16', '2022-05-20 12:55:16'),
(5628, '2022-05-20 00:00:00', '172.70.131.67', '2022-05-20 12:55:17', '2022-05-20 12:55:17'),
(5629, '2022-05-20 00:00:00', '172.70.142.224', '2022-05-20 12:58:26', '2022-05-20 12:58:26'),
(5630, '2022-05-20 00:00:00', '172.70.92.156', '2022-05-20 13:19:37', '2022-05-20 13:19:37'),
(5631, '2022-05-20 00:00:00', '172.70.134.210', '2022-05-20 13:23:35', '2022-05-20 13:23:35'),
(5632, '2022-05-20 00:00:00', '172.70.134.68', '2022-05-20 13:24:03', '2022-05-20 13:24:03'),
(5633, '2022-05-20 00:00:00', '172.70.38.82', '2022-05-20 13:24:03', '2022-05-20 13:24:03'),
(5634, '2022-05-20 00:00:00', '162.158.170.246', '2022-05-20 13:30:15', '2022-05-20 13:30:15'),
(5635, '2022-05-20 00:00:00', '172.70.189.59', '2022-05-20 14:50:24', '2022-05-20 14:50:24'),
(5636, '2022-05-20 00:00:00', '172.70.39.141', '2022-05-20 15:33:40', '2022-05-20 15:33:40'),
(5637, '2022-05-20 00:00:00', '172.70.174.60', '2022-05-20 17:03:39', '2022-05-20 17:03:39'),
(5638, '2022-05-20 00:00:00', '172.70.92.156', '2022-05-20 17:38:57', '2022-05-20 17:38:57'),
(5639, '2022-05-20 00:00:00', '172.70.188.192', '2022-05-20 17:56:49', '2022-05-20 17:56:49'),
(5640, '2022-05-20 00:00:00', '108.162.229.70', '2022-05-20 17:58:01', '2022-05-20 17:58:01'),
(5641, '2022-05-20 00:00:00', '172.70.134.94', '2022-05-20 18:13:38', '2022-05-20 18:13:38'),
(5642, '2022-05-20 00:00:00', '172.70.142.34', '2022-05-20 18:50:42', '2022-05-20 18:50:42'),
(5643, '2022-05-20 00:00:00', '172.70.142.224', '2022-05-20 19:17:19', '2022-05-20 19:17:19'),
(5644, '2022-05-20 00:00:00', '172.70.34.142', '2022-05-20 19:44:01', '2022-05-20 19:44:01'),
(5645, '2022-05-20 00:00:00', '172.70.135.149', '2022-05-20 20:06:30', '2022-05-20 20:06:30'),
(5646, '2022-05-20 00:00:00', '172.70.35.31', '2022-05-20 20:51:30', '2022-05-20 20:51:30'),
(5647, '2022-05-20 00:00:00', '162.158.222.118', '2022-05-20 23:33:10', '2022-05-20 23:33:10'),
(5648, '2022-05-21 00:00:00', '172.68.244.76', '2022-05-21 00:29:53', '2022-05-21 00:29:53'),
(5649, '2022-05-21 00:00:00', '172.70.147.34', '2022-05-21 00:36:13', '2022-05-21 00:36:13'),
(5650, '2022-05-21 00:00:00', '172.70.85.154', '2022-05-21 01:18:59', '2022-05-21 01:18:59'),
(5651, '2022-05-21 00:00:00', '172.70.142.224', '2022-05-21 01:47:12', '2022-05-21 01:47:12'),
(5652, '2022-05-21 00:00:00', '172.70.142.32', '2022-05-21 05:49:27', '2022-05-21 05:49:27'),
(5653, '2022-05-21 00:00:00', '162.158.38.52', '2022-05-21 06:43:06', '2022-05-21 06:43:06'),
(5654, '2022-05-21 00:00:00', '172.70.134.194', '2022-05-21 07:41:46', '2022-05-21 07:41:46'),
(5655, '2022-05-21 00:00:00', '172.70.38.98', '2022-05-21 08:11:46', '2022-05-21 08:11:46'),
(5656, '2022-05-21 00:00:00', '172.70.134.176', '2022-05-21 08:26:46', '2022-05-21 08:26:46'),
(5657, '2022-05-21 00:00:00', '172.70.92.150', '2022-05-21 08:27:14', '2022-05-21 08:27:14'),
(5658, '2022-05-21 00:00:00', '172.70.175.221', '2022-05-21 08:41:46', '2022-05-21 08:41:46'),
(5659, '2022-05-21 00:00:00', '162.158.162.220', '2022-05-21 08:49:35', '2022-05-21 08:49:35'),
(5660, '2022-05-21 00:00:00', '172.70.188.90', '2022-05-21 09:10:02', '2022-05-21 09:10:02'),
(5661, '2022-05-21 00:00:00', '172.70.142.34', '2022-05-21 09:29:59', '2022-05-21 09:29:59'),
(5662, '2022-05-21 00:00:00', '172.70.174.16', '2022-05-21 09:41:46', '2022-05-21 09:41:46'),
(5663, '2022-05-21 00:00:00', '172.70.188.150', '2022-05-21 11:07:06', '2022-05-21 11:07:06'),
(5664, '2022-05-21 00:00:00', '172.70.134.210', '2022-05-21 11:43:55', '2022-05-21 11:43:55'),
(5665, '2022-05-21 00:00:00', '172.70.38.246', '2022-05-21 11:43:56', '2022-05-21 11:43:56'),
(5666, '2022-05-21 00:00:00', '162.158.170.184', '2022-05-21 11:46:42', '2022-05-21 11:46:42'),
(5667, '2022-05-21 00:00:00', '172.70.147.48', '2022-05-21 11:46:43', '2022-05-21 11:46:43'),
(5668, '2022-05-21 00:00:00', '141.101.69.69', '2022-05-21 12:34:01', '2022-05-21 12:34:01'),
(5669, '2022-05-21 00:00:00', '141.101.77.139', '2022-05-21 12:40:02', '2022-05-21 12:40:02'),
(5670, '2022-05-21 00:00:00', '172.70.174.18', '2022-05-21 12:41:46', '2022-05-21 12:41:46'),
(5671, '2022-05-21 00:00:00', '172.68.245.185', '2022-05-21 13:30:00', '2022-05-21 13:30:00'),
(5672, '2022-05-21 00:00:00', '172.68.10.124', '2022-05-21 13:30:02', '2022-05-21 13:30:02'),
(5673, '2022-05-21 00:00:00', '172.68.11.7', '2022-05-21 13:30:03', '2022-05-21 13:30:03'),
(5674, '2022-05-21 00:00:00', '172.68.10.48', '2022-05-21 13:30:03', '2022-05-21 13:30:03'),
(5675, '2022-05-21 00:00:00', '172.70.130.102', '2022-05-21 13:48:26', '2022-05-21 13:48:26'),
(5676, '2022-05-21 00:00:00', '172.70.142.218', '2022-05-21 14:04:54', '2022-05-21 14:04:54'),
(5677, '2022-05-21 00:00:00', '172.70.147.42', '2022-05-21 14:40:02', '2022-05-21 14:40:02'),
(5678, '2022-05-21 00:00:00', '162.158.163.157', '2022-05-21 14:50:43', '2022-05-21 14:50:43'),
(5679, '2022-05-21 00:00:00', '141.101.76.12', '2022-05-21 15:51:16', '2022-05-21 15:51:16'),
(5680, '2022-05-21 00:00:00', '162.158.170.82', '2022-05-21 17:20:14', '2022-05-21 17:20:14'),
(5681, '2022-05-21 00:00:00', '162.158.38.164', '2022-05-21 17:25:21', '2022-05-21 17:25:21'),
(5682, '2022-05-21 00:00:00', '172.70.142.34', '2022-05-21 17:26:40', '2022-05-21 17:26:40'),
(5683, '2022-05-21 00:00:00', '162.158.170.246', '2022-05-21 17:45:25', '2022-05-21 17:45:25'),
(5684, '2022-05-21 00:00:00', '162.158.170.246', '2022-05-21 17:57:49', '2022-05-21 17:57:49'),
(5685, '2022-05-21 00:00:00', '172.70.92.150', '2022-05-21 18:09:38', '2022-05-21 18:09:38'),
(5686, '2022-05-21 00:00:00', '172.70.142.218', '2022-05-21 18:15:58', '2022-05-21 18:15:58'),
(5687, '2022-05-21 00:00:00', '172.70.230.136', '2022-05-21 19:24:32', '2022-05-21 19:24:32'),
(5688, '2022-05-21 00:00:00', '172.70.130.102', '2022-05-21 20:46:01', '2022-05-21 20:46:01'),
(5689, '2022-05-21 00:00:00', '172.70.175.3', '2022-05-21 21:50:29', '2022-05-21 21:50:29'),
(5690, '2022-05-21 00:00:00', '172.70.134.186', '2022-05-21 22:55:13', '2022-05-21 22:55:13'),
(5691, '2022-05-21 00:00:00', '172.70.189.137', '2022-05-21 23:38:05', '2022-05-21 23:38:05'),
(5692, '2022-05-21 00:00:00', '172.70.134.94', '2022-05-21 23:58:53', '2022-05-21 23:58:53'),
(5693, '2022-05-22 00:00:00', '172.70.188.36', '2022-05-22 00:04:40', '2022-05-22 00:04:40'),
(5694, '2022-05-22 00:00:00', '172.70.189.137', '2022-05-22 00:27:53', '2022-05-22 00:27:53'),
(5695, '2022-05-22 00:00:00', '172.70.142.34', '2022-05-22 00:49:38', '2022-05-22 00:49:38'),
(5696, '2022-05-22 00:00:00', '172.70.142.224', '2022-05-22 01:11:44', '2022-05-22 01:11:44'),
(5697, '2022-05-22 00:00:00', '162.158.170.246', '2022-05-22 01:20:56', '2022-05-22 01:20:56'),
(5698, '2022-05-22 00:00:00', '172.70.142.218', '2022-05-22 01:30:16', '2022-05-22 01:30:16'),
(5699, '2022-05-22 00:00:00', '172.68.11.95', '2022-05-22 02:36:59', '2022-05-22 02:36:59'),
(5700, '2022-05-22 00:00:00', '172.70.92.160', '2022-05-22 03:01:36', '2022-05-22 03:01:36'),
(5701, '2022-05-22 00:00:00', '172.70.147.42', '2022-05-22 04:02:32', '2022-05-22 04:02:32'),
(5702, '2022-05-22 00:00:00', '172.70.134.68', '2022-05-22 04:21:23', '2022-05-22 04:21:23'),
(5703, '2022-05-22 00:00:00', '162.158.78.250', '2022-05-22 04:21:31', '2022-05-22 04:21:31'),
(5704, '2022-05-22 00:00:00', '172.70.189.137', '2022-05-22 05:18:20', '2022-05-22 05:18:20'),
(5705, '2022-05-22 00:00:00', '172.68.10.148', '2022-05-22 05:25:23', '2022-05-22 05:25:23'),
(5706, '2022-05-22 00:00:00', '172.68.11.115', '2022-05-22 05:25:53', '2022-05-22 05:25:53'),
(5707, '2022-05-22 00:00:00', '172.70.147.32', '2022-05-22 06:06:58', '2022-05-22 06:06:58'),
(5708, '2022-05-22 00:00:00', '172.70.38.218', '2022-05-22 06:25:18', '2022-05-22 06:25:18'),
(5709, '2022-05-22 00:00:00', '162.158.78.112', '2022-05-22 07:10:18', '2022-05-22 07:10:18'),
(5710, '2022-05-22 00:00:00', '141.101.104.7', '2022-05-22 07:42:56', '2022-05-22 07:42:56'),
(5711, '2022-05-22 00:00:00', '141.101.76.194', '2022-05-22 07:43:20', '2022-05-22 07:43:20'),
(5712, '2022-05-22 00:00:00', '162.158.78.64', '2022-05-22 07:55:18', '2022-05-22 07:55:18'),
(5713, '2022-05-22 00:00:00', '172.70.142.228', '2022-05-22 07:55:36', '2022-05-22 07:55:36'),
(5714, '2022-05-22 00:00:00', '172.70.188.150', '2022-05-22 09:29:21', '2022-05-22 09:29:21'),
(5715, '2022-05-22 00:00:00', '172.70.92.150', '2022-05-22 09:36:16', '2022-05-22 09:36:16'),
(5716, '2022-05-22 00:00:00', '172.70.135.57', '2022-05-22 10:02:34', '2022-05-22 10:02:34'),
(5717, '2022-05-22 00:00:00', '172.70.142.218', '2022-05-22 10:51:18', '2022-05-22 10:51:18'),
(5718, '2022-05-22 00:00:00', '172.70.251.51', '2022-05-22 11:04:13', '2022-05-22 11:04:13'),
(5719, '2022-05-22 00:00:00', '162.158.79.11', '2022-05-22 13:22:04', '2022-05-22 13:22:04'),
(5720, '2022-05-22 00:00:00', '141.101.77.44', '2022-05-22 13:25:11', '2022-05-22 13:25:11'),
(5721, '2022-05-22 00:00:00', '172.70.142.34', '2022-05-22 14:39:11', '2022-05-22 14:39:11'),
(5722, '2022-05-22 00:00:00', '172.70.135.67', '2022-05-22 15:25:39', '2022-05-22 15:25:39'),
(5723, '2022-05-22 00:00:00', '172.70.188.36', '2022-05-22 15:37:46', '2022-05-22 15:37:46'),
(5724, '2022-05-22 00:00:00', '141.101.77.44', '2022-05-22 15:47:58', '2022-05-22 15:47:58'),
(5725, '2022-05-22 00:00:00', '162.158.170.170', '2022-05-22 16:07:01', '2022-05-22 16:07:01'),
(5726, '2022-05-22 00:00:00', '172.70.135.167', '2022-05-22 16:10:39', '2022-05-22 16:10:39'),
(5727, '2022-05-22 00:00:00', '162.158.38.96', '2022-05-22 16:25:28', '2022-05-22 16:25:28'),
(5728, '2022-05-22 00:00:00', '172.70.135.55', '2022-05-22 16:55:39', '2022-05-22 16:55:39'),
(5729, '2022-05-22 00:00:00', '172.70.188.150', '2022-05-22 17:22:01', '2022-05-22 17:22:01'),
(5730, '2022-05-22 00:00:00', '172.70.142.224', '2022-05-22 17:27:23', '2022-05-22 17:27:23'),
(5731, '2022-05-22 00:00:00', '172.70.188.90', '2022-05-22 17:32:43', '2022-05-22 17:32:43'),
(5732, '2022-05-22 00:00:00', '172.70.189.95', '2022-05-22 17:38:16', '2022-05-22 17:38:16'),
(5733, '2022-05-22 00:00:00', '172.70.189.137', '2022-05-22 17:48:41', '2022-05-22 17:48:41'),
(5734, '2022-05-22 00:00:00', '172.70.142.224', '2022-05-22 17:54:02', '2022-05-22 17:54:02'),
(5735, '2022-05-22 00:00:00', '162.158.38.160', '2022-05-22 18:43:54', '2022-05-22 18:43:54'),
(5736, '2022-05-22 00:00:00', '162.158.170.170', '2022-05-22 20:59:55', '2022-05-22 20:59:55'),
(5737, '2022-05-22 00:00:00', '172.70.242.54', '2022-05-22 22:36:44', '2022-05-22 22:36:44'),
(5738, '2022-05-22 00:00:00', '172.70.189.117', '2022-05-22 22:49:40', '2022-05-22 22:49:40'),
(5739, '2022-05-22 00:00:00', '162.158.163.191', '2022-05-22 23:00:47', '2022-05-22 23:00:47'),
(5740, '2022-05-22 00:00:00', '172.70.188.90', '2022-05-22 23:27:23', '2022-05-22 23:27:23'),
(5741, '2022-05-23 00:00:00', '162.158.163.155', '2022-05-23 00:05:50', '2022-05-23 00:05:50'),
(5742, '2022-05-23 00:00:00', '162.158.170.184', '2022-05-23 00:05:50', '2022-05-23 00:05:50'),
(5743, '2022-05-23 00:00:00', '162.158.163.203', '2022-05-23 00:05:53', '2022-05-23 00:05:53'),
(5744, '2022-05-23 00:00:00', '172.70.92.150', '2022-05-23 00:05:54', '2022-05-23 00:05:54'),
(5745, '2022-05-23 00:00:00', '172.70.142.34', '2022-05-23 00:12:49', '2022-05-23 00:12:49'),
(5746, '2022-05-23 00:00:00', '172.70.38.82', '2022-05-23 00:42:52', '2022-05-23 00:42:52'),
(5747, '2022-05-23 00:00:00', '162.158.163.181', '2022-05-23 02:06:08', '2022-05-23 02:06:08'),
(5748, '2022-05-23 00:00:00', '172.70.189.117', '2022-05-23 02:13:10', '2022-05-23 02:13:10'),
(5749, '2022-05-23 00:00:00', '172.70.92.156', '2022-05-23 04:54:13', '2022-05-23 04:54:13'),
(5750, '2022-05-23 00:00:00', '172.70.188.36', '2022-05-23 05:01:01', '2022-05-23 05:01:01'),
(5751, '2022-05-23 00:00:00', '172.70.211.63', '2022-05-23 06:44:36', '2022-05-23 06:44:36'),
(5752, '2022-05-23 00:00:00', '172.70.142.34', '2022-05-23 07:51:10', '2022-05-23 07:51:10'),
(5753, '2022-05-23 00:00:00', '172.70.142.228', '2022-05-23 08:09:40', '2022-05-23 08:09:40'),
(5754, '2022-05-23 00:00:00', '172.70.175.221', '2022-05-23 08:11:10', '2022-05-23 08:11:10'),
(5755, '2022-05-23 00:00:00', '172.70.92.156', '2022-05-23 08:18:53', '2022-05-23 08:18:53'),
(5756, '2022-05-23 00:00:00', '172.70.147.34', '2022-05-23 08:34:54', '2022-05-23 08:34:54'),
(5757, '2022-05-23 00:00:00', '172.70.92.160', '2022-05-23 08:42:10', '2022-05-23 08:42:10'),
(5758, '2022-05-23 00:00:00', '172.70.135.131', '2022-05-23 09:04:45', '2022-05-23 09:04:45'),
(5759, '2022-05-23 00:00:00', '162.158.162.168', '2022-05-23 09:13:51', '2022-05-23 09:13:51'),
(5760, '2022-05-23 00:00:00', '172.70.92.150', '2022-05-23 09:25:13', '2022-05-23 09:25:13'),
(5761, '2022-05-23 00:00:00', '172.70.92.150', '2022-05-23 09:44:59', '2022-05-23 09:44:59'),
(5762, '2022-05-23 00:00:00', '172.70.142.48', '2022-05-23 10:07:57', '2022-05-23 10:07:57'),
(5763, '2022-05-23 00:00:00', '172.71.10.6', '2022-05-23 10:30:53', '2022-05-23 10:30:53'),
(5764, '2022-05-23 00:00:00', '172.70.134.194', '2022-05-23 11:18:41', '2022-05-23 11:18:41'),
(5765, '2022-05-23 00:00:00', '172.70.218.198', '2022-05-23 13:12:36', '2022-05-23 13:12:36'),
(5766, '2022-05-23 00:00:00', '172.70.174.190', '2022-05-23 13:42:15', '2022-05-23 13:42:15'),
(5767, '2022-05-23 00:00:00', '172.70.122.210', '2022-05-23 13:57:00', '2022-05-23 13:57:00'),
(5768, '2022-05-23 00:00:00', '162.158.107.163', '2022-05-23 14:37:19', '2022-05-23 14:37:19'),
(5769, '2022-05-23 00:00:00', '162.158.170.232', '2022-05-23 15:23:18', '2022-05-23 15:23:18'),
(5770, '2022-05-23 00:00:00', '162.158.163.157', '2022-05-23 17:20:29', '2022-05-23 17:20:29'),
(5771, '2022-05-23 00:00:00', '172.70.142.224', '2022-05-23 17:25:03', '2022-05-23 17:25:03'),
(5772, '2022-05-23 00:00:00', '172.70.147.34', '2022-05-23 17:31:20', '2022-05-23 17:31:20'),
(5773, '2022-05-23 00:00:00', '172.70.142.224', '2022-05-23 17:47:55', '2022-05-23 17:47:55'),
(5774, '2022-05-23 00:00:00', '172.70.147.32', '2022-05-23 17:53:27', '2022-05-23 17:53:27'),
(5775, '2022-05-23 00:00:00', '162.158.170.232', '2022-05-23 17:59:07', '2022-05-23 17:59:07'),
(5776, '2022-05-23 00:00:00', '162.158.170.236', '2022-05-23 18:04:42', '2022-05-23 18:04:42'),
(5777, '2022-05-23 00:00:00', '162.158.162.252', '2022-05-23 18:10:18', '2022-05-23 18:10:18'),
(5778, '2022-05-23 00:00:00', '172.70.142.42', '2022-05-23 18:21:28', '2022-05-23 18:21:28'),
(5779, '2022-05-23 00:00:00', '172.68.254.130', '2022-05-23 23:32:18', '2022-05-23 23:32:18'),
(5780, '2022-05-23 00:00:00', '172.70.147.32', '2022-05-23 23:48:14', '2022-05-23 23:48:14'),
(5781, '2022-05-24 00:00:00', '162.158.162.240', '2022-05-24 00:17:44', '2022-05-24 00:17:44'),
(5782, '2022-05-24 00:00:00', '172.70.189.117', '2022-05-24 01:36:15', '2022-05-24 01:36:15'),
(5783, '2022-05-24 00:00:00', '162.158.162.164', '2022-05-24 02:01:51', '2022-05-24 02:01:51'),
(5784, '2022-05-24 00:00:00', '162.158.170.184', '2022-05-24 03:36:16', '2022-05-24 03:36:16'),
(5785, '2022-05-24 00:00:00', '172.70.147.34', '2022-05-24 03:49:54', '2022-05-24 03:49:54'),
(5786, '2022-05-24 00:00:00', '172.70.34.36', '2022-05-24 04:55:45', '2022-05-24 04:55:45'),
(5787, '2022-05-24 00:00:00', '172.70.142.224', '2022-05-24 08:45:01', '2022-05-24 08:45:01'),
(5788, '2022-05-24 00:00:00', '172.70.188.36', '2022-05-24 08:59:39', '2022-05-24 08:59:39'),
(5789, '2022-05-24 00:00:00', '172.70.188.90', '2022-05-24 09:09:06', '2022-05-24 09:09:06'),
(5790, '2022-05-24 00:00:00', '172.70.92.150', '2022-05-24 09:18:04', '2022-05-24 09:18:04'),
(5791, '2022-05-24 00:00:00', '172.70.92.150', '2022-05-24 09:55:36', '2022-05-24 09:55:36'),
(5792, '2022-05-24 00:00:00', '162.158.162.70', '2022-05-24 10:19:20', '2022-05-24 10:19:20'),
(5793, '2022-05-24 00:00:00', '172.70.142.224', '2022-05-24 10:25:47', '2022-05-24 10:25:47'),
(5794, '2022-05-24 00:00:00', '172.70.147.34', '2022-05-24 11:14:10', '2022-05-24 11:14:10'),
(5795, '2022-05-24 00:00:00', '172.70.210.132', '2022-05-24 11:19:44', '2022-05-24 11:19:44'),
(5796, '2022-05-24 00:00:00', '141.101.105.52', '2022-05-24 11:20:34', '2022-05-24 11:20:34'),
(5797, '2022-05-24 00:00:00', '141.101.77.254', '2022-05-24 12:50:18', '2022-05-24 12:50:18'),
(5798, '2022-05-24 00:00:00', '172.70.174.96', '2022-05-24 13:24:47', '2022-05-24 13:24:47'),
(5799, '2022-05-24 00:00:00', '172.70.188.36', '2022-05-24 13:32:33', '2022-05-24 13:32:33'),
(5800, '2022-05-24 00:00:00', '172.70.189.95', '2022-05-24 13:56:05', '2022-05-24 13:56:05'),
(5801, '2022-05-24 00:00:00', '172.70.142.48', '2022-05-24 14:50:09', '2022-05-24 14:50:09'),
(5802, '2022-05-24 00:00:00', '162.158.107.163', '2022-05-24 15:04:34', '2022-05-24 15:04:34'),
(5803, '2022-05-24 00:00:00', '108.162.246.105', '2022-05-24 15:05:05', '2022-05-24 15:05:05'),
(5804, '2022-05-24 00:00:00', '162.158.107.105', '2022-05-24 15:05:05', '2022-05-24 15:05:05'),
(5805, '2022-05-24 00:00:00', '108.162.246.7', '2022-05-24 15:05:06', '2022-05-24 15:05:06'),
(5806, '2022-05-24 00:00:00', '108.162.245.126', '2022-05-24 15:05:06', '2022-05-24 15:05:06'),
(5807, '2022-05-24 00:00:00', '108.162.245.204', '2022-05-24 15:05:06', '2022-05-24 15:05:06'),
(5808, '2022-05-24 00:00:00', '162.158.106.212', '2022-05-24 15:05:07', '2022-05-24 15:05:07'),
(5809, '2022-05-24 00:00:00', '162.158.107.209', '2022-05-24 15:05:07', '2022-05-24 15:05:07'),
(5810, '2022-05-24 00:00:00', '162.158.107.21', '2022-05-24 15:05:09', '2022-05-24 15:05:09'),
(5811, '2022-05-24 00:00:00', '162.158.107.127', '2022-05-24 15:05:10', '2022-05-24 15:05:10'),
(5812, '2022-05-24 00:00:00', '172.70.142.224', '2022-05-24 15:23:13', '2022-05-24 15:23:13'),
(5813, '2022-05-24 00:00:00', '162.158.170.82', '2022-05-24 15:35:57', '2022-05-24 15:35:57'),
(5814, '2022-05-24 00:00:00', '162.158.163.21', '2022-05-24 16:07:54', '2022-05-24 16:07:54'),
(5815, '2022-05-24 00:00:00', '162.158.170.232', '2022-05-24 17:12:22', '2022-05-24 17:12:22'),
(5816, '2022-05-24 00:00:00', '172.70.92.150', '2022-05-24 17:21:31', '2022-05-24 17:21:31'),
(5817, '2022-05-24 00:00:00', '172.70.189.95', '2022-05-24 17:26:57', '2022-05-24 17:26:57'),
(5818, '2022-05-24 00:00:00', '172.70.147.32', '2022-05-24 17:32:25', '2022-05-24 17:32:25'),
(5819, '2022-05-24 00:00:00', '172.70.92.150', '2022-05-24 17:43:25', '2022-05-24 17:43:25'),
(5820, '2022-05-24 00:00:00', '172.70.142.224', '2022-05-24 17:59:59', '2022-05-24 17:59:59'),
(5821, '2022-05-24 00:00:00', '172.70.85.34', '2022-05-24 19:19:22', '2022-05-24 19:19:22'),
(5822, '2022-05-24 00:00:00', '172.70.34.202', '2022-05-24 20:09:56', '2022-05-24 20:09:56'),
(5823, '2022-05-24 00:00:00', '172.70.175.31', '2022-05-24 20:09:57', '2022-05-24 20:09:57'),
(5824, '2022-05-24 00:00:00', '172.70.38.26', '2022-05-24 20:09:57', '2022-05-24 20:09:57'),
(5825, '2022-05-24 00:00:00', '162.158.78.58', '2022-05-24 20:09:58', '2022-05-24 20:09:58'),
(5826, '2022-05-24 00:00:00', '172.70.174.46', '2022-05-24 20:09:58', '2022-05-24 20:09:58'),
(5827, '2022-05-24 00:00:00', '172.70.38.72', '2022-05-24 20:09:58', '2022-05-24 20:09:58'),
(5828, '2022-05-24 00:00:00', '172.70.126.190', '2022-05-24 20:58:07', '2022-05-24 20:58:07'),
(5829, '2022-05-24 00:00:00', '172.70.242.128', '2022-05-24 21:22:02', '2022-05-24 21:22:02'),
(5830, '2022-05-24 00:00:00', '172.70.246.52', '2022-05-24 21:22:02', '2022-05-24 21:22:02'),
(5831, '2022-05-24 00:00:00', '172.70.211.63', '2022-05-24 22:57:39', '2022-05-24 22:57:39'),
(5832, '2022-05-25 00:00:00', '172.70.189.95', '2022-05-25 00:07:10', '2022-05-25 00:07:10'),
(5833, '2022-05-25 00:00:00', '162.158.134.56', '2022-05-25 00:12:40', '2022-05-25 00:12:40'),
(5834, '2022-05-25 00:00:00', '172.68.146.18', '2022-05-25 00:17:49', '2022-05-25 00:17:49'),
(5835, '2022-05-25 00:00:00', '162.158.170.184', '2022-05-25 00:59:49', '2022-05-25 00:59:49'),
(5836, '2022-05-25 00:00:00', '141.101.77.248', '2022-05-25 01:15:38', '2022-05-25 01:15:38'),
(5837, '2022-05-25 00:00:00', '172.70.189.59', '2022-05-25 01:40:37', '2022-05-25 01:40:37'),
(5838, '2022-05-25 00:00:00', '162.158.170.246', '2022-05-25 01:54:24', '2022-05-25 01:54:24'),
(5839, '2022-05-25 00:00:00', '162.158.163.155', '2022-05-25 01:59:20', '2022-05-25 01:59:20'),
(5840, '2022-05-25 00:00:00', '172.70.92.150', '2022-05-25 03:08:53', '2022-05-25 03:08:53'),
(5841, '2022-05-25 00:00:00', '172.70.134.68', '2022-05-25 04:14:19', '2022-05-25 04:14:19'),
(5842, '2022-05-25 00:00:00', '172.70.174.104', '2022-05-25 04:14:19', '2022-05-25 04:14:19'),
(5843, '2022-05-25 00:00:00', '162.158.170.232', '2022-05-25 04:39:41', '2022-05-25 04:39:41'),
(5844, '2022-05-25 00:00:00', '172.70.142.32', '2022-05-25 07:26:22', '2022-05-25 07:26:22'),
(5845, '2022-05-25 00:00:00', '172.70.142.34', '2022-05-25 08:31:34', '2022-05-25 08:31:34'),
(5846, '2022-05-25 00:00:00', '141.101.76.84', '2022-05-25 09:54:29', '2022-05-25 09:54:29'),
(5847, '2022-05-25 00:00:00', '172.70.246.142', '2022-05-25 12:09:29', '2022-05-25 12:09:29'),
(5848, '2022-05-25 00:00:00', '172.70.92.156', '2022-05-25 13:15:43', '2022-05-25 13:15:43'),
(5849, '2022-05-25 00:00:00', '172.70.210.146', '2022-05-25 13:21:50', '2022-05-25 13:21:50'),
(5850, '2022-05-25 00:00:00', '172.70.134.210', '2022-05-25 13:25:58', '2022-05-25 13:25:58'),
(5851, '2022-05-25 00:00:00', '172.70.175.97', '2022-05-25 13:26:16', '2022-05-25 13:26:16'),
(5852, '2022-05-25 00:00:00', '172.70.134.90', '2022-05-25 13:26:16', '2022-05-25 13:26:16'),
(5853, '2022-05-25 00:00:00', '108.162.246.105', '2022-05-25 14:20:50', '2022-05-25 14:20:50'),
(5854, '2022-05-25 00:00:00', '141.101.77.143', '2022-05-25 14:29:20', '2022-05-25 14:29:20'),
(5855, '2022-05-25 00:00:00', '172.70.147.42', '2022-05-25 14:32:02', '2022-05-25 14:32:02'),
(5856, '2022-05-25 00:00:00', '172.70.189.117', '2022-05-25 17:16:04', '2022-05-25 17:16:04'),
(5857, '2022-05-25 00:00:00', '162.158.170.232', '2022-05-25 17:28:14', '2022-05-25 17:28:14'),
(5858, '2022-05-25 00:00:00', '172.70.147.34', '2022-05-25 17:41:01', '2022-05-25 17:41:01'),
(5859, '2022-05-25 00:00:00', '172.70.142.32', '2022-05-25 17:59:42', '2022-05-25 17:59:42'),
(5860, '2022-05-25 00:00:00', '162.158.107.163', '2022-05-25 18:35:00', '2022-05-25 18:35:00'),
(5861, '2022-05-25 00:00:00', '162.158.90.212', '2022-05-25 18:52:16', '2022-05-25 18:52:16'),
(5862, '2022-05-25 00:00:00', '108.162.242.61', '2022-05-25 20:10:54', '2022-05-25 20:10:54'),
(5863, '2022-05-25 00:00:00', '108.162.241.60', '2022-05-25 20:11:51', '2022-05-25 20:11:51'),
(5864, '2022-05-26 00:00:00', '162.158.163.201', '2022-05-26 00:05:35', '2022-05-26 00:05:35'),
(5865, '2022-05-26 00:00:00', '162.158.90.50', '2022-05-26 00:25:55', '2022-05-26 00:25:55'),
(5866, '2022-05-26 00:00:00', '172.70.251.51', '2022-05-26 00:26:07', '2022-05-26 00:26:07'),
(5867, '2022-05-26 00:00:00', '172.70.147.42', '2022-05-26 01:01:11', '2022-05-26 01:01:11'),
(5868, '2022-05-26 00:00:00', '172.70.147.32', '2022-05-26 01:09:48', '2022-05-26 01:09:48'),
(5869, '2022-05-26 00:00:00', '172.70.92.150', '2022-05-26 03:55:07', '2022-05-26 03:55:07'),
(5870, '2022-05-26 00:00:00', '172.70.189.59', '2022-05-26 05:14:40', '2022-05-26 05:14:40'),
(5871, '2022-05-26 00:00:00', '162.158.129.74', '2022-05-26 06:34:00', '2022-05-26 06:34:00'),
(5872, '2022-05-26 00:00:00', '162.158.129.96', '2022-05-26 06:34:01', '2022-05-26 06:34:01'),
(5873, '2022-05-26 00:00:00', '172.70.134.90', '2022-05-26 07:16:43', '2022-05-26 07:16:43'),
(5874, '2022-05-26 00:00:00', '172.70.175.97', '2022-05-26 07:16:43', '2022-05-26 07:16:43'),
(5875, '2022-05-26 00:00:00', '172.70.142.42', '2022-05-26 07:20:12', '2022-05-26 07:20:12'),
(5876, '2022-05-26 00:00:00', '172.70.92.156', '2022-05-26 07:34:54', '2022-05-26 07:34:54'),
(5877, '2022-05-26 00:00:00', '172.70.147.32', '2022-05-26 07:42:09', '2022-05-26 07:42:09'),
(5878, '2022-05-26 00:00:00', '172.70.38.72', '2022-05-26 07:46:10', '2022-05-26 07:46:10'),
(5879, '2022-05-26 00:00:00', '172.70.142.218', '2022-05-26 08:15:48', '2022-05-26 08:15:48'),
(5880, '2022-05-26 00:00:00', '172.70.251.153', '2022-05-26 09:11:30', '2022-05-26 09:11:30'),
(5881, '2022-05-26 00:00:00', '162.158.170.232', '2022-05-26 09:15:20', '2022-05-26 09:15:20'),
(5882, '2022-05-26 00:00:00', '172.70.242.128', '2022-05-26 10:15:53', '2022-05-26 10:15:53'),
(5883, '2022-05-26 00:00:00', '162.158.90.212', '2022-05-26 10:21:22', '2022-05-26 10:21:22'),
(5884, '2022-05-26 00:00:00', '172.70.142.224', '2022-05-26 12:41:52', '2022-05-26 12:41:52'),
(5885, '2022-05-26 00:00:00', '141.101.77.221', '2022-05-26 13:01:20', '2022-05-26 13:01:20'),
(5886, '2022-05-26 00:00:00', '172.70.38.26', '2022-05-26 13:28:39', '2022-05-26 13:28:39'),
(5887, '2022-05-26 00:00:00', '172.70.174.46', '2022-05-26 13:28:59', '2022-05-26 13:28:59'),
(5888, '2022-05-26 00:00:00', '172.70.175.97', '2022-05-26 13:28:59', '2022-05-26 13:28:59'),
(5889, '2022-05-26 00:00:00', '162.158.170.236', '2022-05-26 14:41:11', '2022-05-26 14:41:11'),
(5890, '2022-05-26 00:00:00', '172.70.189.137', '2022-05-26 15:01:20', '2022-05-26 15:01:20'),
(5891, '2022-05-26 00:00:00', '172.70.188.36', '2022-05-26 15:07:32', '2022-05-26 15:07:32'),
(5892, '2022-05-26 00:00:00', '172.70.142.224', '2022-05-26 15:21:06', '2022-05-26 15:21:06'),
(5893, '2022-05-26 00:00:00', '172.70.142.42', '2022-05-26 15:47:13', '2022-05-26 15:47:13'),
(5894, '2022-05-26 00:00:00', '162.158.163.7', '2022-05-26 16:00:46', '2022-05-26 16:00:46'),
(5895, '2022-05-26 00:00:00', '172.70.142.42', '2022-05-26 17:10:53', '2022-05-26 17:10:53'),
(5896, '2022-05-26 00:00:00', '172.70.218.174', '2022-05-26 17:18:59', '2022-05-26 17:18:59'),
(5897, '2022-05-26 00:00:00', '172.70.92.156', '2022-05-26 17:31:19', '2022-05-26 17:31:19'),
(5898, '2022-05-26 00:00:00', '162.158.162.250', '2022-05-26 17:36:30', '2022-05-26 17:36:30'),
(5899, '2022-05-26 00:00:00', '172.70.142.224', '2022-05-26 17:46:57', '2022-05-26 17:46:57'),
(5900, '2022-05-26 00:00:00', '162.158.162.206', '2022-05-26 17:57:26', '2022-05-26 17:57:26'),
(5901, '2022-05-26 00:00:00', '172.70.142.42', '2022-05-26 18:02:43', '2022-05-26 18:02:43'),
(5902, '2022-05-26 00:00:00', '141.101.77.139', '2022-05-26 18:03:15', '2022-05-26 18:03:15'),
(5903, '2022-05-26 00:00:00', '172.70.189.137', '2022-05-26 18:13:18', '2022-05-26 18:13:18'),
(5904, '2022-05-26 00:00:00', '108.162.246.177', '2022-05-26 18:16:52', '2022-05-26 18:16:52'),
(5905, '2022-05-26 00:00:00', '172.71.22.74', '2022-05-26 20:53:19', '2022-05-26 20:53:19'),
(5906, '2022-05-26 00:00:00', '162.158.163.227', '2022-05-26 22:42:22', '2022-05-26 22:42:22'),
(5907, '2022-05-26 00:00:00', '172.70.142.42', '2022-05-26 23:29:56', '2022-05-26 23:29:56'),
(5908, '2022-05-26 00:00:00', '141.101.77.200', '2022-05-26 23:32:20', '2022-05-26 23:32:20'),
(5909, '2022-05-27 00:00:00', '162.158.170.82', '2022-05-27 00:00:56', '2022-05-27 00:00:56'),
(5910, '2022-05-27 00:00:00', '108.162.245.174', '2022-05-27 01:01:11', '2022-05-27 01:01:11'),
(5911, '2022-05-27 00:00:00', '172.70.142.34', '2022-05-27 01:02:30', '2022-05-27 01:02:30'),
(5912, '2022-05-27 00:00:00', '188.114.102.52', '2022-05-27 02:46:31', '2022-05-27 02:46:31'),
(5913, '2022-05-27 00:00:00', '162.158.162.220', '2022-05-27 06:42:22', '2022-05-27 06:42:22'),
(5914, '2022-05-27 00:00:00', '172.70.92.150', '2022-05-27 07:20:02', '2022-05-27 07:20:02'),
(5915, '2022-05-27 00:00:00', '162.158.111.11', '2022-05-27 08:30:12', '2022-05-27 08:30:12'),
(5916, '2022-05-27 00:00:00', '172.70.178.158', '2022-05-27 08:39:03', '2022-05-27 08:39:03'),
(5917, '2022-05-27 00:00:00', '172.70.174.46', '2022-05-27 11:12:35', '2022-05-27 11:12:35'),
(5918, '2022-05-27 00:00:00', '172.70.175.97', '2022-05-27 11:12:36', '2022-05-27 11:12:36'),
(5919, '2022-05-27 00:00:00', '172.70.147.48', '2022-05-27 12:04:29', '2022-05-27 12:04:29'),
(5920, '2022-05-27 00:00:00', '162.158.163.207', '2022-05-27 12:16:24', '2022-05-27 12:16:24'),
(5921, '2022-05-27 00:00:00', '172.70.142.42', '2022-05-27 12:51:57', '2022-05-27 12:51:57'),
(5922, '2022-05-27 00:00:00', '172.70.142.218', '2022-05-27 13:11:58', '2022-05-27 13:11:58'),
(5923, '2022-05-27 00:00:00', '172.70.189.137', '2022-05-27 13:33:35', '2022-05-27 13:33:35'),
(5924, '2022-05-27 00:00:00', '172.70.147.32', '2022-05-27 13:53:24', '2022-05-27 13:53:24'),
(5925, '2022-05-27 00:00:00', '172.70.147.42', '2022-05-27 14:35:03', '2022-05-27 14:35:03'),
(5926, '2022-05-27 00:00:00', '141.101.104.43', '2022-05-27 15:32:29', '2022-05-27 15:32:29'),
(5927, '2022-05-27 00:00:00', '172.70.142.48', '2022-05-27 17:22:37', '2022-05-27 17:22:37'),
(5928, '2022-05-27 00:00:00', '108.162.242.61', '2022-05-27 17:53:39', '2022-05-27 17:53:39'),
(5929, '2022-05-27 00:00:00', '162.158.170.170', '2022-05-27 17:55:43', '2022-05-27 17:55:43'),
(5930, '2022-05-27 00:00:00', '108.162.242.61', '2022-05-27 18:05:55', '2022-05-27 18:05:55'),
(5931, '2022-05-27 00:00:00', '162.158.162.94', '2022-05-27 18:08:45', '2022-05-27 18:08:45'),
(5932, '2022-05-27 00:00:00', '172.70.147.28', '2022-05-27 18:15:09', '2022-05-27 18:15:09'),
(5933, '2022-05-27 00:00:00', '108.162.241.16', '2022-05-27 18:23:47', '2022-05-27 18:23:47'),
(5934, '2022-05-27 00:00:00', '141.101.105.98', '2022-05-27 18:30:40', '2022-05-27 18:30:40'),
(5935, '2022-05-27 00:00:00', '108.162.242.61', '2022-05-27 18:54:20', '2022-05-27 18:54:20'),
(5936, '2022-05-27 00:00:00', '172.69.70.88', '2022-05-27 19:27:37', '2022-05-27 19:27:37'),
(5937, '2022-05-27 00:00:00', '172.69.69.169', '2022-05-27 19:27:38', '2022-05-27 19:27:38'),
(5938, '2022-05-27 00:00:00', '172.70.147.32', '2022-05-27 23:24:50', '2022-05-27 23:24:50'),
(5939, '2022-05-27 00:00:00', '172.70.162.12', '2022-05-27 23:42:46', '2022-05-27 23:42:46'),
(5940, '2022-05-28 00:00:00', '172.70.142.42', '2022-05-28 00:14:05', '2022-05-28 00:14:05'),
(5941, '2022-05-28 00:00:00', '172.70.147.48', '2022-05-28 00:30:21', '2022-05-28 00:30:21'),
(5942, '2022-05-28 00:00:00', '172.70.142.34', '2022-05-28 00:40:43', '2022-05-28 00:40:43'),
(5943, '2022-05-28 00:00:00', '172.70.189.95', '2022-05-28 01:26:37', '2022-05-28 01:26:37'),
(5944, '2022-05-28 00:00:00', '172.68.11.31', '2022-05-28 01:34:46', '2022-05-28 01:34:46'),
(5945, '2022-05-28 00:00:00', '172.70.126.238', '2022-05-28 02:41:39', '2022-05-28 02:41:39'),
(5946, '2022-05-28 00:00:00', '172.70.135.191', '2022-05-28 03:11:10', '2022-05-28 03:11:10'),
(5947, '2022-05-28 00:00:00', '172.70.175.97', '2022-05-28 03:11:10', '2022-05-28 03:11:10'),
(5948, '2022-05-28 00:00:00', '172.70.34.248', '2022-05-28 03:42:23', '2022-05-28 03:42:23'),
(5949, '2022-05-28 00:00:00', '172.70.142.224', '2022-05-28 05:35:30', '2022-05-28 05:35:30'),
(5950, '2022-05-28 00:00:00', '172.70.188.192', '2022-05-28 06:16:13', '2022-05-28 06:16:13'),
(5951, '2022-05-28 00:00:00', '162.158.162.90', '2022-05-28 06:33:04', '2022-05-28 06:33:04'),
(5952, '2022-05-28 00:00:00', '108.162.242.11', '2022-05-28 06:51:42', '2022-05-28 06:51:42'),
(5953, '2022-05-28 00:00:00', '172.70.189.95', '2022-05-28 06:56:24', '2022-05-28 06:56:24'),
(5954, '2022-05-28 00:00:00', '141.101.104.71', '2022-05-28 07:16:34', '2022-05-28 07:16:34'),
(5955, '2022-05-28 00:00:00', '141.101.77.245', '2022-05-28 07:16:41', '2022-05-28 07:16:41');
INSERT INTO `visitor` (`id`, `date`, `ip`, `updated_at`, `created_at`) VALUES
(5956, '2022-05-28 00:00:00', '172.70.189.95', '2022-05-28 07:29:42', '2022-05-28 07:29:42'),
(5957, '2022-05-28 00:00:00', '172.70.147.32', '2022-05-28 07:39:47', '2022-05-28 07:39:47'),
(5958, '2022-05-28 00:00:00', '162.158.163.9', '2022-05-28 07:50:05', '2022-05-28 07:50:05'),
(5959, '2022-05-28 00:00:00', '172.70.142.224', '2022-05-28 08:54:18', '2022-05-28 08:54:18'),
(5960, '2022-05-28 00:00:00', '172.70.92.156', '2022-05-28 09:04:22', '2022-05-28 09:04:22'),
(5961, '2022-05-28 00:00:00', '172.68.10.102', '2022-05-28 10:07:00', '2022-05-28 10:07:00'),
(5962, '2022-05-28 00:00:00', '162.158.163.199', '2022-05-28 10:53:34', '2022-05-28 10:53:34'),
(5963, '2022-05-28 00:00:00', '172.70.142.48', '2022-05-28 11:58:07', '2022-05-28 11:58:07'),
(5964, '2022-05-28 00:00:00', '172.70.246.74', '2022-05-28 12:03:00', '2022-05-28 12:03:00'),
(5965, '2022-05-28 00:00:00', '162.158.170.82', '2022-05-28 12:36:00', '2022-05-28 12:36:00'),
(5966, '2022-05-28 00:00:00', '172.70.92.150', '2022-05-28 12:57:12', '2022-05-28 12:57:12'),
(5967, '2022-05-28 00:00:00', '172.70.131.143', '2022-05-28 13:12:51', '2022-05-28 13:12:51'),
(5968, '2022-05-28 00:00:00', '162.158.170.246', '2022-05-28 13:48:34', '2022-05-28 13:48:34'),
(5969, '2022-05-28 00:00:00', '172.70.142.224', '2022-05-28 14:09:58', '2022-05-28 14:09:58'),
(5970, '2022-05-28 00:00:00', '172.70.142.34', '2022-05-28 14:30:51', '2022-05-28 14:30:51'),
(5971, '2022-05-28 00:00:00', '172.70.142.218', '2022-05-28 14:59:39', '2022-05-28 14:59:39'),
(5972, '2022-05-28 00:00:00', '172.70.246.180', '2022-05-28 15:03:36', '2022-05-28 15:03:36'),
(5973, '2022-05-28 00:00:00', '172.70.92.156', '2022-05-28 15:23:39', '2022-05-28 15:23:39'),
(5974, '2022-05-28 00:00:00', '172.70.175.31', '2022-05-28 15:52:46', '2022-05-28 15:52:46'),
(5975, '2022-05-28 00:00:00', '172.70.135.225', '2022-05-28 15:52:49', '2022-05-28 15:52:49'),
(5976, '2022-05-28 00:00:00', '172.70.34.2', '2022-05-28 15:52:49', '2022-05-28 15:52:49'),
(5977, '2022-05-28 00:00:00', '172.70.142.34', '2022-05-28 16:11:46', '2022-05-28 16:11:46'),
(5978, '2022-05-28 00:00:00', '108.162.246.49', '2022-05-28 17:04:03', '2022-05-28 17:04:03'),
(5979, '2022-05-28 00:00:00', '172.68.133.60', '2022-05-28 17:04:03', '2022-05-28 17:04:03'),
(5980, '2022-05-28 00:00:00', '172.69.134.94', '2022-05-28 17:04:03', '2022-05-28 17:04:03'),
(5981, '2022-05-28 00:00:00', '162.158.107.21', '2022-05-28 17:04:04', '2022-05-28 17:04:04'),
(5982, '2022-05-28 00:00:00', '162.158.107.151', '2022-05-28 17:04:04', '2022-05-28 17:04:04'),
(5983, '2022-05-28 00:00:00', '172.68.143.230', '2022-05-28 17:04:04', '2022-05-28 17:04:04'),
(5984, '2022-05-28 00:00:00', '108.162.245.92', '2022-05-28 17:04:05', '2022-05-28 17:04:05'),
(5985, '2022-05-28 00:00:00', '162.158.107.69', '2022-05-28 17:04:06', '2022-05-28 17:04:06'),
(5986, '2022-05-28 00:00:00', '108.162.245.204', '2022-05-28 17:04:06', '2022-05-28 17:04:06'),
(5987, '2022-05-28 00:00:00', '162.158.166.230', '2022-05-28 17:04:06', '2022-05-28 17:04:06'),
(5988, '2022-05-28 00:00:00', '162.158.107.167', '2022-05-28 17:04:06', '2022-05-28 17:04:06'),
(5989, '2022-05-28 00:00:00', '108.162.246.105', '2022-05-28 17:04:07', '2022-05-28 17:04:07'),
(5990, '2022-05-28 00:00:00', '162.158.107.133', '2022-05-28 17:04:07', '2022-05-28 17:04:07'),
(5991, '2022-05-28 00:00:00', '108.162.245.126', '2022-05-28 17:04:07', '2022-05-28 17:04:07'),
(5992, '2022-05-28 00:00:00', '162.158.106.212', '2022-05-28 17:04:08', '2022-05-28 17:04:08'),
(5993, '2022-05-28 00:00:00', '172.68.132.127', '2022-05-28 17:04:08', '2022-05-28 17:04:08'),
(5994, '2022-05-28 00:00:00', '172.68.133.88', '2022-05-28 17:04:09', '2022-05-28 17:04:09'),
(5995, '2022-05-28 00:00:00', '108.162.246.45', '2022-05-28 17:04:09', '2022-05-28 17:04:09'),
(5996, '2022-05-28 00:00:00', '108.162.245.36', '2022-05-28 17:04:10', '2022-05-28 17:04:10'),
(5997, '2022-05-28 00:00:00', '172.69.34.17', '2022-05-28 17:04:10', '2022-05-28 17:04:10'),
(5998, '2022-05-28 00:00:00', '162.158.106.146', '2022-05-28 17:04:11', '2022-05-28 17:04:11'),
(5999, '2022-05-28 00:00:00', '162.158.107.105', '2022-05-28 17:04:12', '2022-05-28 17:04:12'),
(6000, '2022-05-28 00:00:00', '162.158.107.209', '2022-05-28 17:04:13', '2022-05-28 17:04:13'),
(6001, '2022-05-28 00:00:00', '162.158.106.124', '2022-05-28 17:04:14', '2022-05-28 17:04:14'),
(6002, '2022-05-28 00:00:00', '108.162.245.214', '2022-05-28 17:04:16', '2022-05-28 17:04:16'),
(6003, '2022-05-28 00:00:00', '172.68.133.128', '2022-05-28 17:04:18', '2022-05-28 17:04:18'),
(6004, '2022-05-28 00:00:00', '172.70.206.172', '2022-05-28 17:04:20', '2022-05-28 17:04:20'),
(6005, '2022-05-28 00:00:00', '108.162.246.117', '2022-05-28 17:04:20', '2022-05-28 17:04:20'),
(6006, '2022-05-28 00:00:00', '162.158.166.216', '2022-05-28 17:04:23', '2022-05-28 17:04:23'),
(6007, '2022-05-28 00:00:00', '172.70.207.19', '2022-05-28 17:04:26', '2022-05-28 17:04:26'),
(6008, '2022-05-28 00:00:00', '172.70.188.150', '2022-05-28 17:17:51', '2022-05-28 17:17:51'),
(6009, '2022-05-28 00:00:00', '172.70.92.150', '2022-05-28 17:36:31', '2022-05-28 17:36:31'),
(6010, '2022-05-28 00:00:00', '172.70.92.156', '2022-05-28 18:01:42', '2022-05-28 18:01:42'),
(6011, '2022-05-28 00:00:00', '172.70.142.42', '2022-05-28 18:07:55', '2022-05-28 18:07:55'),
(6012, '2022-05-28 00:00:00', '172.70.174.190', '2022-05-28 23:03:14', '2022-05-28 23:03:14'),
(6013, '2022-05-28 00:00:00', '162.158.170.184', '2022-05-28 23:49:21', '2022-05-28 23:49:21'),
(6014, '2022-05-29 00:00:00', '141.101.69.47', '2022-05-29 00:00:46', '2022-05-29 00:00:46'),
(6015, '2022-05-29 00:00:00', '141.101.77.200', '2022-05-29 00:13:25', '2022-05-29 00:13:25'),
(6016, '2022-05-29 00:00:00', '162.158.170.232', '2022-05-29 00:36:59', '2022-05-29 00:36:59'),
(6017, '2022-05-29 00:00:00', '172.70.35.11', '2022-05-29 02:57:47', '2022-05-29 02:57:47'),
(6018, '2022-05-29 00:00:00', '162.158.222.160', '2022-05-29 03:39:56', '2022-05-29 03:39:56'),
(6019, '2022-05-29 00:00:00', '172.70.134.220', '2022-05-29 04:02:15', '2022-05-29 04:02:15'),
(6020, '2022-05-29 00:00:00', '172.70.174.190', '2022-05-29 05:32:14', '2022-05-29 05:32:14'),
(6021, '2022-05-29 00:00:00', '172.70.174.122', '2022-05-29 05:32:14', '2022-05-29 05:32:14'),
(6022, '2022-05-29 00:00:00', '172.69.70.140', '2022-05-29 07:14:10', '2022-05-29 07:14:10'),
(6023, '2022-05-29 00:00:00', '162.158.162.240', '2022-05-29 07:31:59', '2022-05-29 07:31:59'),
(6024, '2022-05-29 00:00:00', '172.70.92.156', '2022-05-29 07:51:45', '2022-05-29 07:51:45'),
(6025, '2022-05-29 00:00:00', '172.70.147.42', '2022-05-29 08:11:57', '2022-05-29 08:11:57'),
(6026, '2022-05-29 00:00:00', '162.158.90.176', '2022-05-29 08:28:10', '2022-05-29 08:28:10'),
(6027, '2022-05-29 00:00:00', '162.158.106.184', '2022-05-29 11:01:19', '2022-05-29 11:01:19'),
(6028, '2022-05-29 00:00:00', '162.158.107.63', '2022-05-29 11:46:19', '2022-05-29 11:46:19'),
(6029, '2022-05-29 00:00:00', '172.70.100.17', '2022-05-29 12:58:45', '2022-05-29 12:58:45'),
(6030, '2022-05-29 00:00:00', '162.158.106.206', '2022-05-29 13:02:14', '2022-05-29 13:02:14'),
(6031, '2022-05-29 00:00:00', '172.70.142.218', '2022-05-29 13:42:21', '2022-05-29 13:42:21'),
(6032, '2022-05-29 00:00:00', '172.70.147.34', '2022-05-29 14:14:18', '2022-05-29 14:14:18'),
(6033, '2022-05-29 00:00:00', '108.162.246.241', '2022-05-29 14:32:14', '2022-05-29 14:32:14'),
(6034, '2022-05-29 00:00:00', '172.70.142.218', '2022-05-29 14:34:34', '2022-05-29 14:34:34'),
(6035, '2022-05-29 00:00:00', '172.70.250.206', '2022-05-29 14:39:13', '2022-05-29 14:39:13'),
(6036, '2022-05-29 00:00:00', '172.70.142.32', '2022-05-29 14:55:30', '2022-05-29 14:55:30'),
(6037, '2022-05-29 00:00:00', '172.70.147.48', '2022-05-29 15:55:58', '2022-05-29 15:55:58'),
(6038, '2022-05-29 00:00:00', '172.70.189.117', '2022-05-29 17:14:18', '2022-05-29 17:14:18'),
(6039, '2022-05-29 00:00:00', '162.158.163.43', '2022-05-29 17:19:52', '2022-05-29 17:19:52'),
(6040, '2022-05-29 00:00:00', '172.70.142.228', '2022-05-29 17:30:58', '2022-05-29 17:30:58'),
(6041, '2022-05-29 00:00:00', '162.158.106.206', '2022-05-29 17:32:14', '2022-05-29 17:32:14'),
(6042, '2022-05-29 00:00:00', '172.70.147.32', '2022-05-29 17:59:34', '2022-05-29 17:59:34'),
(6043, '2022-05-29 00:00:00', '172.70.142.34', '2022-05-29 18:05:17', '2022-05-29 18:05:17'),
(6044, '2022-05-29 00:00:00', '172.70.142.228', '2022-05-29 18:16:26', '2022-05-29 18:16:26'),
(6045, '2022-05-29 00:00:00', '172.70.189.95', '2022-05-29 18:27:51', '2022-05-29 18:27:51'),
(6046, '2022-05-29 00:00:00', '172.70.188.150', '2022-05-29 18:33:46', '2022-05-29 18:33:46'),
(6047, '2022-05-29 00:00:00', '172.70.147.42', '2022-05-29 18:57:10', '2022-05-29 18:57:10'),
(6048, '2022-05-29 00:00:00', '172.70.188.90', '2022-05-29 19:03:06', '2022-05-29 19:03:06'),
(6049, '2022-05-29 00:00:00', '172.70.142.224', '2022-05-29 19:09:05', '2022-05-29 19:09:05'),
(6050, '2022-05-29 00:00:00', '172.70.142.34', '2022-05-29 23:01:23', '2022-05-29 23:01:23'),
(6051, '2022-05-29 00:00:00', '162.158.170.170', '2022-05-29 23:12:03', '2022-05-29 23:12:03'),
(6052, '2022-05-29 00:00:00', '172.70.92.150', '2022-05-29 23:34:59', '2022-05-29 23:34:59'),
(6053, '2022-05-30 00:00:00', '162.158.170.82', '2022-05-30 00:07:28', '2022-05-30 00:07:28'),
(6054, '2022-05-30 00:00:00', '172.70.142.48', '2022-05-30 00:17:36', '2022-05-30 00:17:36'),
(6055, '2022-05-30 00:00:00', '172.70.142.48', '2022-05-30 00:58:56', '2022-05-30 00:58:56'),
(6056, '2022-05-30 00:00:00', '172.70.189.117', '2022-05-30 01:09:27', '2022-05-30 01:09:27'),
(6057, '2022-05-30 00:00:00', '162.158.163.7', '2022-05-30 01:19:44', '2022-05-30 01:19:44'),
(6058, '2022-05-30 00:00:00', '162.158.170.170', '2022-05-30 01:40:35', '2022-05-30 01:40:35'),
(6059, '2022-05-30 00:00:00', '172.70.142.228', '2022-05-30 01:51:05', '2022-05-30 01:51:05'),
(6060, '2022-05-30 00:00:00', '172.70.92.160', '2022-05-30 02:01:30', '2022-05-30 02:01:30'),
(6061, '2022-05-30 00:00:00', '172.70.142.224', '2022-05-30 02:22:20', '2022-05-30 02:22:20'),
(6062, '2022-05-30 00:00:00', '172.70.147.48', '2022-05-30 05:33:47', '2022-05-30 05:33:47'),
(6063, '2022-05-30 00:00:00', '172.70.189.117', '2022-05-30 05:46:45', '2022-05-30 05:46:45'),
(6064, '2022-05-30 00:00:00', '172.70.189.137', '2022-05-30 05:57:09', '2022-05-30 05:57:09'),
(6065, '2022-05-30 00:00:00', '141.101.84.170', '2022-05-30 06:27:16', '2022-05-30 06:27:16'),
(6066, '2022-05-30 00:00:00', '172.70.142.224', '2022-05-30 06:36:09', '2022-05-30 06:36:09'),
(6067, '2022-05-30 00:00:00', '108.162.229.64', '2022-05-30 06:41:36', '2022-05-30 06:41:36'),
(6068, '2022-05-30 00:00:00', '162.158.170.236', '2022-05-30 07:16:25', '2022-05-30 07:16:25'),
(6069, '2022-05-30 00:00:00', '172.70.147.32', '2022-05-30 07:32:44', '2022-05-30 07:32:44'),
(6070, '2022-05-30 00:00:00', '172.70.92.156', '2022-05-30 07:43:22', '2022-05-30 07:43:22'),
(6071, '2022-05-30 00:00:00', '172.70.135.191', '2022-05-30 08:44:31', '2022-05-30 08:44:31'),
(6072, '2022-05-30 00:00:00', '172.70.35.53', '2022-05-30 09:09:07', '2022-05-30 09:09:07'),
(6073, '2022-05-30 00:00:00', '162.158.170.184', '2022-05-30 11:30:18', '2022-05-30 11:30:18'),
(6074, '2022-05-30 00:00:00', '172.70.92.156', '2022-05-30 12:43:05', '2022-05-30 12:43:05'),
(6075, '2022-05-30 00:00:00', '172.69.68.26', '2022-05-30 13:46:45', '2022-05-30 13:46:45'),
(6076, '2022-05-30 00:00:00', '172.69.69.17', '2022-05-30 16:01:45', '2022-05-30 16:01:45'),
(6077, '2022-05-30 00:00:00', '172.70.142.218', '2022-05-30 16:14:07', '2022-05-30 16:14:07'),
(6078, '2022-05-30 00:00:00', '172.70.142.228', '2022-05-30 16:21:29', '2022-05-30 16:21:29'),
(6079, '2022-05-30 00:00:00', '172.70.185.158', '2022-05-30 16:46:45', '2022-05-30 16:46:45'),
(6080, '2022-05-30 00:00:00', '172.70.189.95', '2022-05-30 17:19:58', '2022-05-30 17:19:58'),
(6081, '2022-05-30 00:00:00', '172.70.189.117', '2022-05-30 17:26:29', '2022-05-30 17:26:29'),
(6082, '2022-05-30 00:00:00', '162.158.163.41', '2022-05-30 17:33:02', '2022-05-30 17:33:02'),
(6083, '2022-05-30 00:00:00', '172.70.147.42', '2022-05-30 17:53:01', '2022-05-30 17:53:01'),
(6084, '2022-05-30 00:00:00', '172.70.142.224', '2022-05-30 17:59:16', '2022-05-30 17:59:16'),
(6085, '2022-05-30 00:00:00', '172.70.147.34', '2022-05-30 18:11:43', '2022-05-30 18:11:43'),
(6086, '2022-05-30 00:00:00', '172.70.142.224', '2022-05-30 18:44:06', '2022-05-30 18:44:06'),
(6087, '2022-05-30 00:00:00', '162.158.50.73', '2022-05-30 19:13:46', '2022-05-30 19:13:46'),
(6088, '2022-05-30 00:00:00', '172.69.33.198', '2022-05-30 19:37:22', '2022-05-30 19:37:22'),
(6089, '2022-05-30 00:00:00', '172.70.185.152', '2022-05-30 21:57:58', '2022-05-30 21:57:58'),
(6090, '2022-05-30 00:00:00', '172.69.71.154', '2022-05-30 22:20:28', '2022-05-30 22:20:28'),
(6091, '2022-05-30 00:00:00', '172.68.238.148', '2022-05-30 22:57:13', '2022-05-30 22:57:13'),
(6092, '2022-05-30 00:00:00', '172.68.238.42', '2022-05-30 22:57:14', '2022-05-30 22:57:14'),
(6093, '2022-05-30 00:00:00', '172.68.238.14', '2022-05-30 22:57:14', '2022-05-30 22:57:14'),
(6094, '2022-05-30 00:00:00', '172.69.70.230', '2022-05-30 22:58:42', '2022-05-30 22:58:42'),
(6095, '2022-05-30 00:00:00', '172.70.185.162', '2022-05-30 23:43:42', '2022-05-30 23:43:42'),
(6096, '2022-05-31 00:00:00', '162.158.162.30', '2022-05-31 01:07:27', '2022-05-31 01:07:27'),
(6097, '2022-05-31 00:00:00', '162.158.162.208', '2022-05-31 01:37:28', '2022-05-31 01:37:28'),
(6098, '2022-05-31 00:00:00', '108.162.241.16', '2022-05-31 01:59:18', '2022-05-31 01:59:18'),
(6099, '2022-05-31 00:00:00', '172.70.142.228', '2022-05-31 03:18:54', '2022-05-31 03:18:54'),
(6100, '2022-05-31 00:00:00', '172.70.189.95', '2022-05-31 04:10:03', '2022-05-31 04:10:03'),
(6101, '2022-05-31 00:00:00', '108.162.245.32', '2022-05-31 04:26:58', '2022-05-31 04:26:58'),
(6102, '2022-05-31 00:00:00', '172.70.85.40', '2022-05-31 04:32:53', '2022-05-31 04:32:53'),
(6103, '2022-05-31 00:00:00', '172.70.162.188', '2022-05-31 04:35:28', '2022-05-31 04:35:28'),
(6104, '2022-05-31 00:00:00', '172.70.85.154', '2022-05-31 04:37:44', '2022-05-31 04:37:44'),
(6105, '2022-05-31 00:00:00', '172.70.162.22', '2022-05-31 04:42:10', '2022-05-31 04:42:10'),
(6106, '2022-05-31 00:00:00', '172.70.85.34', '2022-05-31 04:48:23', '2022-05-31 04:48:23'),
(6107, '2022-05-31 00:00:00', '172.70.162.188', '2022-05-31 04:49:14', '2022-05-31 04:49:14'),
(6108, '2022-05-31 00:00:00', '172.70.90.118', '2022-05-31 04:50:03', '2022-05-31 04:50:03'),
(6109, '2022-05-31 00:00:00', '141.101.99.85', '2022-05-31 04:50:54', '2022-05-31 04:50:54'),
(6110, '2022-05-31 00:00:00', '172.70.91.17', '2022-05-31 04:52:02', '2022-05-31 04:52:02'),
(6111, '2022-05-31 00:00:00', '141.101.99.11', '2022-05-31 04:53:23', '2022-05-31 04:53:23'),
(6112, '2022-05-31 00:00:00', '172.70.85.40', '2022-05-31 04:54:06', '2022-05-31 04:54:06'),
(6113, '2022-05-31 00:00:00', '172.70.162.12', '2022-05-31 04:54:13', '2022-05-31 04:54:13'),
(6114, '2022-05-31 00:00:00', '162.158.159.82', '2022-05-31 04:54:14', '2022-05-31 04:54:14'),
(6115, '2022-05-31 00:00:00', '172.70.162.22', '2022-05-31 05:03:12', '2022-05-31 05:03:12'),
(6116, '2022-05-31 00:00:00', '172.70.85.34', '2022-05-31 05:06:14', '2022-05-31 05:06:14'),
(6117, '2022-05-31 00:00:00', '172.70.162.188', '2022-05-31 05:06:22', '2022-05-31 05:06:22'),
(6118, '2022-05-31 00:00:00', '172.70.85.40', '2022-05-31 05:06:23', '2022-05-31 05:06:23'),
(6119, '2022-05-31 00:00:00', '141.101.99.11', '2022-05-31 05:08:55', '2022-05-31 05:08:55'),
(6120, '2022-05-31 00:00:00', '172.70.91.17', '2022-05-31 05:11:22', '2022-05-31 05:11:22'),
(6121, '2022-05-31 00:00:00', '172.70.91.83', '2022-05-31 05:12:49', '2022-05-31 05:12:49'),
(6122, '2022-05-31 00:00:00', '172.70.91.103', '2022-05-31 05:14:26', '2022-05-31 05:14:26'),
(6123, '2022-05-31 00:00:00', '172.70.92.150', '2022-05-31 05:25:22', '2022-05-31 05:25:22'),
(6124, '2022-05-31 00:00:00', '172.70.85.34', '2022-05-31 05:30:58', '2022-05-31 05:30:58'),
(6125, '2022-05-31 00:00:00', '162.158.170.170', '2022-05-31 05:38:24', '2022-05-31 05:38:24'),
(6126, '2022-05-31 00:00:00', '141.101.107.82', '2022-05-31 05:38:31', '2022-05-31 05:38:31'),
(6127, '2022-05-31 00:00:00', '172.70.85.150', '2022-05-31 05:41:20', '2022-05-31 05:41:20'),
(6128, '2022-05-31 00:00:00', '172.70.85.40', '2022-05-31 05:54:37', '2022-05-31 05:54:37'),
(6129, '2022-05-31 00:00:00', '172.70.85.34', '2022-05-31 05:58:11', '2022-05-31 05:58:11'),
(6130, '2022-05-31 00:00:00', '172.70.85.154', '2022-05-31 06:02:13', '2022-05-31 06:02:13'),
(6131, '2022-05-31 00:00:00', '162.158.162.220', '2022-05-31 06:06:26', '2022-05-31 06:06:26'),
(6132, '2022-05-31 00:00:00', '172.70.91.17', '2022-05-31 06:08:52', '2022-05-31 06:08:52'),
(6133, '2022-05-31 00:00:00', '172.70.85.150', '2022-05-31 06:11:01', '2022-05-31 06:11:01'),
(6134, '2022-05-31 00:00:00', '172.70.85.40', '2022-05-31 06:11:50', '2022-05-31 06:11:50'),
(6135, '2022-05-31 00:00:00', '172.70.162.22', '2022-05-31 06:15:10', '2022-05-31 06:15:10'),
(6136, '2022-05-31 00:00:00', '172.70.85.34', '2022-05-31 06:16:13', '2022-05-31 06:16:13'),
(6137, '2022-05-31 00:00:00', '172.70.142.48', '2022-05-31 06:17:38', '2022-05-31 06:17:38'),
(6138, '2022-05-31 00:00:00', '172.70.85.150', '2022-05-31 06:22:07', '2022-05-31 06:22:07'),
(6139, '2022-05-31 00:00:00', '172.70.85.154', '2022-05-31 06:23:19', '2022-05-31 06:23:19'),
(6140, '2022-05-31 00:00:00', '172.70.90.118', '2022-05-31 06:25:47', '2022-05-31 06:25:47'),
(6141, '2022-05-31 00:00:00', '172.70.162.188', '2022-05-31 06:26:37', '2022-05-31 06:26:37'),
(6142, '2022-05-31 00:00:00', '172.70.147.42', '2022-05-31 06:28:44', '2022-05-31 06:28:44'),
(6143, '2022-05-31 00:00:00', '172.70.90.118', '2022-05-31 06:38:17', '2022-05-31 06:38:17'),
(6144, '2022-05-31 00:00:00', '172.70.90.118', '2022-05-31 06:38:17', '2022-05-31 06:38:17'),
(6145, '2022-05-31 00:00:00', '172.70.85.150', '2022-05-31 06:40:33', '2022-05-31 06:40:33'),
(6146, '2022-05-31 00:00:00', '172.70.162.22', '2022-05-31 06:43:33', '2022-05-31 06:43:33'),
(6147, '2022-05-31 00:00:00', '172.70.91.17', '2022-05-31 06:43:51', '2022-05-31 06:43:51'),
(6148, '2022-05-31 00:00:00', '172.70.162.188', '2022-05-31 06:45:04', '2022-05-31 06:45:04'),
(6149, '2022-05-31 00:00:00', '172.70.85.34', '2022-05-31 06:45:27', '2022-05-31 06:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `volunteer_detail`
--

CREATE TABLE `volunteer_detail` (
  `id` int NOT NULL,
  `volunteer_heading` varchar(255) NOT NULL,
  `volunteer_title` varchar(255) NOT NULL,
  `volunteer_description` longtext NOT NULL,
  `volunter_image_one` varchar(255) NOT NULL,
  `volunter_image_two` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `volunteer_detail`
--

INSERT INTO `volunteer_detail` (`id`, `volunteer_heading`, `volunteer_title`, `volunteer_description`, `volunter_image_one`, `volunter_image_two`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 'How to Connect?', 'And make a difference!', 'Volunteers  are the heart of a community. Our volunteers are a valuable resource for our fast-growing, fast-paced city.Our city relies on our volunteers for everything from staffing special event, such as Freedom Fest and Merry Main Street, to assisting departments with daily activities, such as shelving library books, filing records or using GIS equipment.', '/volunter_image/dresden-3681378_1920-edit.jpg', '/volunter_image/bodyworn-794111_1920.jpg', '2021-06-04 13:06:47', '2021-07-09 10:38:43', '0'),
(2, 'gnvnv', 'nvnvnvn', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/volunter_image/black_buck.jpg', '/volunter_image/elephant.jpg', '2021-06-21 11:36:44', '2021-06-21 11:41:54', '1'),
(3, 'Heading Volunteer', 'Title Volunteer', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home.', '/volunter_image/chhatbir.jpg', '/volunter_image/cat.jpg', '2021-08-03 12:10:59', '2021-08-03 12:11:05', '1'),
(4, 'Heading Volunteer', 'Title Volunteer', 'Beautiful neighborhoods, extraordinary schools, great restaurants and a rich cultural history make our city an ideal place to call home.', '/volunter_image/chhatbir.jpg', '/volunter_image/cat.jpg', '2021-08-03 12:24:44', '2021-08-03 12:58:37', '1'),
(5, 'testing', 'testing', 'testing', '/volunter_image/3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', '/volunter_image/pexels-photo-247431.jpeg', '2022-01-12 07:15:00', '2022-01-12 07:15:48', '1'),
(6, 'dfgdfgdfgd', 'And make a difference! gdd', 'rfggggggggggggggggggghhg', '/volunter_image/cat.jpg', '/volunter_image/chhatbir.jpg', '2022-01-31 10:23:25', '2022-01-31 10:23:36', '1'),
(7, 'test', 'test', 'testing', '/volunter_image/3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', '/volunter_image/photo-1575550959106-5a7defe28b56.JPG', '2022-02-02 09:04:22', '2022-02-02 09:04:44', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wetland_authority`
--

CREATE TABLE `wetland_authority` (
  `id` int NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wetland_authority`
--

INSERT INTO `wetland_authority` (`id`, `image`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '/wetland_authority/slide1.png', 'Wetland Authority', '1.	Punjab State Wetland Authority	The Ministry of Environment, Forest &amp; Climate Change (MoEFCC), Government of India, New Delhi notified Wetlands (Conservation &amp; Management) Rules, 2017 vide notification No. GSR 1203(E) dated 26.9.2017. As per the provision contained under Rule 5(I) of the said rules, the Government of Punjab has constituted Punjab State Wetlands Authority with Hon\'ble Minister Incharge Wildlife Preservation Department as Chairperson and Additional Chief Secretary (Forests) as Vice-chairperson. The State Wetlands Authority is an Apex Body to bring about an integrated management of Wetlands in the State. Earlier, Department of Science, Technology and Environment was the Nodal Agency for the enforcement of Wetlands (Conservation &amp; Management) Rules, 2017 and implementation of National Action Plan for conservation of aquatic ecosystem (wetlands) in the State with financial support of MoEFCC, Government of India. Now the mandate of implementation of Wetlands (Conservation &amp; Management) Rules, 2017 and implementation of Wetland Action Plan has been transferred to Chief Wildlife Warden, Punjab, Department of Forests and Wildlife Preservation vide Special Secretary, Government of Punjab, Science, Technology and Environment letter No.&nbsp; 172 dated 17.1.2018. The Chief Wildlife Warden, Punjab has also been designated as Nodal Department to implement wetland programmes in the State and its office will be Secretariat to State Wetlands Authority. The functions of the Authority include identification of new wetlands and their notification, digital inventorisation of all wetlands in the State, implementation of wetlands conservation and management rules, intersectoral and interdepartmental coordination for integrated action for the conservation and management of wetlands in the State, preparation, evaluation and implementation of Integrated Management Action Plans of all notified wetlands etc.Objective:	The main objective of this scheme is conservation and management of all wetlands situated in Punjab State.Target:1.	To maintain and improve the hydrological regime of the site both qualitatively and quantitatively for sustaining the aquatic biodiversity of the wetland.2	To improve and maintain wetland habitat for sustaining the various life forms that it supports.3	To build capacity and promote eco-tourism to position as a model for wetland conservation.2.		District Level Wetland Management Committee :One of the functions of the Punjab State Wetlands Authority is to prepare a list of wetlands to be notified, within six months from the date of publication of these rules; taking into cognizance any existing list of wetlands prepared/notified under other relevant State Acts.	And whereas as part of function of Punjab State Wetland Authority, to ensure collaboration and co-operation of various Departments for the successful management of the Wetlands at the District Level, District Level Wetland Management Committees has been constituted with District Level Officials vide Notification No. 34/13/2019-FT-5/1458134/1 dated 8.4.2019.	Mandate of CommitteeThe District Level Wetland Management Committee shall be responsible for management and conservation of Wetland in the District, and will be accountable to the Punjab State Wetland Authority.The said committee may recommend any activity related to development and management of any Wetland within its Jurisdiction to Punjab State Wetland Authority for approval.The said committee may co-opt experts and specialists in Technical and Scientific fields to undertake study, work as may be required from time to time.&nbsp;The District Level Wetland Management Committee shall be responsible for preparation of Brief Document on prioritised Wetlands as well as delineating zone of influence.The said committee shall formulate District Wetland Inventory Team for collecting data as well the delineation of boundary and zone of influence of enlisted Wetlands to enable field validation. The team will involve assessment based on the scientific sampling as well data derived from stakeholder consultations and indigenous traditional knowledge. After detailed assessment, to ensure standardization, the team will prepare the brief report on prioritized Wetlands.The District Level Wetland Management Committee will be entrusted with the responsibility of managing district level database.The District Level Wetland Management Committee will also oversee and monitor the water sources available in the respective Districts, Eco-system monitoring service, facts adversely affecting the Wetlands, analysis of Wetland Management needs, etc.,Enforcement of Wetland (Conservation &amp; Management) Rules 2017 in the respective Districts.The said committee shall interact with the line departments viz., Revenue, Forest, Agricultural Engineering, Public Works, Water Supply and Drainage Board and Local bodies.The District Level Wetland Management Committee will function as an advisory body for a smooth functioning of the State Wetland Authority.3.		Technical Committee and Grievance Committee	The Ministry of Environment, Forest &amp; Climate Change (MoEFCC), Government of India, New Delhi notified Wetlands (Conservation &amp; Management) Rules, 2017 vide notification No. GSR 1203(E) dated 26.9.2017. As per the provision contained under Rule 5(I) of the said rules, the Government of Punjab constituted Punjab State Wetlands Authority vide Notification No. 34/2/2018-FT-5/1192195/1 dated 21.3.2018. The State Wetlands Authority is an Apex Body to bring about an integrated management of Wetlands in the State. As one of the provisions contained in Para (III) of Punjab State Wetlands Authority, State Government vide its Notification No. 34/14/2019-FT-5/1458065/1 dated 8.4.2019 constituted Technical Committee as well as Grievance Committee of Punjab State Wetlands Authority.Mandate of CommitteeThe mandate of the Technical Committee will be as follows:1.	The Technical Committee will review brief documents and Management Plans of wetlands.2.	The Technical Committee will also review all the existing wetlands as well as make efforts to identify new wetlands.3.	The Technical Committee will also act as Grievance Committee.4.	The Technical Committee will also hear any other technical matter which will be referred by Punjab State Wetlands Authority.5.	The Technical Committee will meet once in every three months and will present its progress in the subsequent meeting of Punjab State Wetlands Authority.6.	The Technical Committee will be competent to co-opt any other expert in its composition.', '2022-01-24 12:21:44', '2022-01-28 11:11:35', '2022-01-28 11:11:35'),
(2, '/wetland_authority/slide1.png', 'Wetland Authority', '1.	Punjab State Wetland Authority	The Ministry of Environment, Forest &amp; Climate Change (MoEFCC), Government of India, New Delhi notified Wetlands (Conservation &amp; Management) Rules, 2017 vide notification No. GSR 1203(E) dated 26.9.2017. As per the provision contained under Rule 5(I) of the said rules, the Government of Punjab has constituted Punjab State Wetlands Authority with Hon\'ble Minister Incharge Wildlife Preservation Department as Chairperson and Additional Chief Secretary (Forests) as Vice-chairperson. The State Wetlands Authority is an Apex Body to bring about an integrated management of Wetlands in the State. Earlier, Department of Science, Technology and Environment was the Nodal Agency for the enforcement of Wetlands (Conservation &amp; Management) Rules, 2017 and implementation of National Action Plan for conservation of aquatic ecosystem (wetlands) in the State with financial support of MoEFCC, Government of India. Now the mandate of implementation of Wetlands (Conservation &amp; Management) Rules, 2017 and implementation of Wetland Action Plan has been transferred to Chief Wildlife Warden, Punjab, Department of Forests and Wildlife Preservation vide Special Secretary, Government of Punjab, Science, Technology and Environment letter No.&nbsp; 172 dated 17.1.2018. The Chief Wildlife Warden, Punjab has also been designated as Nodal Department to implement wetland programmes in the State and its office will be Secretariat to State Wetlands Authority. The functions of the Authority include identification of new wetlands and their notification, digital inventorisation of all wetlands in the State, implementation of wetlands conservation and management rules, intersectoral and interdepartmental coordination for integrated action for the conservation and management of wetlands in the State, preparation, evaluation and implementation of Integrated Management Action Plans of all notified wetlands etc.Objective:	The main objective of this scheme is conservation and management of all wetlands situated in Punjab State.Target:1.	To maintain and improve the hydrological regime of the site both qualitatively and quantitatively for sustaining the aquatic biodiversity of the wetland.2	To improve and maintain wetland habitat for sustaining the various life forms that it supports.3	To build capacity and promote eco-tourism to position as a model for wetland conservation.2.		District Level Wetland Management Committee :One of the functions of the Punjab State Wetlands Authority is to prepare a list of wetlands to be notified, within six months from the date of publication of these rules; taking into cognizance any existing list of wetlands prepared/notified under other relevant State Acts.	And whereas as part of function of Punjab State Wetland Authority, to ensure collaboration and co-operation of various Departments for the successful management of the Wetlands at the District Level, District Level Wetland Management Committees has been constituted with District Level Officials vide Notification No. 34/13/2019-FT-5/1458134/1 dated 8.4.2019.	Mandate of CommitteeThe District Level Wetland Management Committee shall be responsible for management and conservation of Wetland in the District, and will be accountable to the Punjab State Wetland Authority.The said committee may recommend any activity related to development and management of any Wetland within its Jurisdiction to Punjab State Wetland Authority for approval.The said committee may co-opt experts and specialists in Technical and Scientific fields to undertake study, work as may be required from time to time.&nbsp;The District Level Wetland Management Committee shall be responsible for preparation of Brief Document on prioritised Wetlands as well as delineating zone of influence.The said committee shall formulate District Wetland Inventory Team for collecting data as well the delineation of boundary and zone of influence of enlisted Wetlands to enable field validation. The team will involve assessment based on the scientific sampling as well data derived from stakeholder consultations and indigenous traditional knowledge. After detailed assessment, to ensure standardization, the team will prepare the brief report on prioritized Wetlands.The District Level Wetland Management Committee will be entrusted with the responsibility of managing district level database.The District Level Wetland Management Committee will also oversee and monitor the water sources available in the respective Districts, Eco-system monitoring service, facts adversely affecting the Wetlands, analysis of Wetland Management needs, etc.,Enforcement of Wetland (Conservation &amp; Management) Rules 2017 in the respective Districts.The said committee shall interact with the line departments viz., Revenue, Forest, Agricultural Engineering, Public Works, Water Supply and Drainage Board and Local bodies.The District Level Wetland Management Committee will function as an advisory body for a smooth functioning of the State Wetland Authority.3.		Technical Committee and Grievance Committee	The Ministry of Environment, Forest &amp; Climate Change (MoEFCC), Government of India, New Delhi notified Wetlands (Conservation &amp; Management) Rules, 2017 vide notification No. GSR 1203(E) dated 26.9.2017. As per the provision contained under Rule 5(I) of the said rules, the Government of Punjab constituted Punjab State Wetlands Authority vide Notification No. 34/2/2018-FT-5/1192195/1 dated 21.3.2018. The State Wetlands Authority is an Apex Body to bring about an integrated management of Wetlands in the State. As one of the provisions contained in Para (III) of Punjab State Wetlands Authority, State Government vide its Notification No. 34/14/2019-FT-5/1458065/1 dated 8.4.2019 constituted Technical Committee as well as Grievance Committee of Punjab State Wetlands Authority.Mandate of CommitteeThe mandate of the Technical Committee will be as follows:1.	The Technical Committee will review brief documents and Management Plans of wetlands.2.	The Technical Committee will also review all the existing wetlands as well as make efforts to identify new wetlands.3.	The Technical Committee will also act as Grievance Committee.4.	The Technical Committee will also hear any other technical matter which will be referred by Punjab State Wetlands Authority.5.	The Technical Committee will meet once in every three months and will present its progress in the subsequent meeting of Punjab State Wetlands Authority.6.	The Technical Committee will be competent to co-opt any other expert in its composition.', '2022-01-28 11:14:29', '2022-01-28 11:14:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wetland_notification`
--

CREATE TABLE `wetland_notification` (
  `id` int NOT NULL,
  `wetland_id` int NOT NULL,
  `notification_text` varchar(255) NOT NULL,
  `notification_pdf` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wetland_notification`
--

INSERT INTO `wetland_notification` (`id`, `wetland_id`, `notification_text`, `notification_pdf`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Notification District Level Committee', '/wetland_authority/1643367861.Notification Distt Level Committee.pdf', '2022-01-24 12:21:44', '2022-01-28 11:11:35', '2022-01-28 11:11:35'),
(2, 1, 'Notification PSWA', '/wetland_authority/1643367880.Notification PSWA.pdf', '2022-01-24 12:21:44', '2022-01-28 11:11:35', '2022-01-28 11:11:35'),
(3, 1, 'Notification Technical Committee', '/wetland_authority/1643367935.Notification Technical Committee.pdf', '2022-01-24 12:21:44', '2022-01-28 11:11:35', '2022-01-28 11:11:35'),
(4, 1, 'Notification District Level Committeejjj', '/wetland_authority/Notification Technical Committee.pdf', '2022-01-24 13:34:36', '2022-01-24 13:39:28', '2022-01-24 13:39:28'),
(5, 1, 'Notification District Level Committeejjj', '/wetland_authority/1643367840.Notification Distt Level Committee.pdf', '2022-01-28 11:04:00', '2022-01-28 11:04:09', '2022-01-28 11:04:09'),
(6, 2, 'Notification District Level Committee', '/wetland/1643368469.Notification Distt Level Committee.pdf', '2022-01-28 11:14:29', '2022-01-28 11:14:29', NULL),
(7, 2, 'Notification PSWA', '/wetland/1643368469.Notification PSWA.pdf', '2022-01-28 11:14:29', '2022-01-28 11:14:29', NULL),
(8, 2, 'Notification Technical Committee', '/wetland/1643368469.Notification Technical Committee.pdf', '2022-01-28 11:14:29', '2022-01-28 11:14:29', NULL),
(9, 2, 'Notification District Level Committeejjj', '/wetland_authority/1643637786.bathinda of e-rickshaw 2021-2022 (2).pdf', '2022-01-31 14:03:06', '2022-01-31 14:03:19', '2022-01-31 14:03:19');

-- --------------------------------------------------------

--
-- Table structure for table `wetland_rules`
--

CREATE TABLE `wetland_rules` (
  `id` int NOT NULL,
  `wetland_id` int NOT NULL,
  `rule_heading` varchar(255) NOT NULL,
  `rule_pdf` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wetland_rules`
--

INSERT INTO `wetland_rules` (`id`, `wetland_id`, `rule_heading`, `rule_pdf`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Wetland Rules', '/wetland_authority/1643367974.Wetlands Rules 2017.pdf', '2022-01-24 12:21:44', '2022-01-28 11:11:35', '2022-01-28 11:11:35'),
(2, 1, 'Wetland Rules hhh', '/wetland_authority/Wetlands Rules 2017.pdf', '2022-01-24 14:07:10', '2022-01-24 14:10:35', '2022-01-24 14:10:35'),
(3, 2, 'Wetland Rules', '/wetland_authority/1643368469.Wetlands Rules 2017.pdf', '2022-01-28 11:14:29', '2022-01-28 11:14:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `whatsnew_detail`
--

CREATE TABLE `whatsnew_detail` (
  `id` int NOT NULL,
  `text` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `whatsnew_detail`
--

INSERT INTO `whatsnew_detail` (`id`, `text`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'text testing', '2022-05-31', '2022-05-30 06:09:59', '2022-05-30 06:22:30', '2022-05-30 06:22:30'),
(2, 'Regarding Vacant Post of Forest Guard, Ranger & Forester in the Department', '2022-05-23', '2022-05-30 06:22:39', '2022-05-31 04:49:58', NULL),
(3, 'text testing 234', '2022-06-01', '2022-05-31 05:14:21', '2022-05-31 05:58:49', '2022-05-31 05:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `wildlife_symbol`
--

CREATE TABLE `wildlife_symbol` (
  `id` int NOT NULL,
  `banner_image` varchar(255) DEFAULT NULL,
  `banner_title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->non_deleted,1->deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wildlife_symbol`
--

INSERT INTO `wildlife_symbol` (`id`, `banner_image`, `banner_title`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/wildlifesymbol_image/slide1.png', 'Wildlife Symbols of Punjab', '2021-04-27 09:36:26', '2022-02-02 09:14:46', '0'),
(2, '/wildlifesymbol_image/slide1.png', 'WildLife Symbol', '2021-06-22 06:37:06', '2021-06-22 10:23:50', '1'),
(3, '/wildlifesymbol_image/d3.jpg', 'Wildlife Symbol of Punjab', '2021-08-04 05:57:47', '2021-08-04 06:00:33', '1'),
(4, '/wildlifesymbol_image/photo-1575550959106-5a7defe28b56.JPG', 'w', '2022-01-12 07:20:24', '2022-01-12 07:29:49', '1'),
(5, '/wildlifesymbol_image/photo-1575550959106-5a7defe28b56.JPG', 'w', '2022-01-12 07:21:13', '2022-01-12 07:29:44', '1'),
(6, '/wildlifesymbol_image/photo-1575550959106-5a7defe28b56.JPG', 'w', '2022-01-12 07:23:10', '2022-01-12 07:29:54', '1'),
(7, '/wildlifesymbol_image/3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', 'testing', '2022-01-12 07:24:17', '2022-01-12 07:29:23', '1'),
(8, '/wildlifesymbol_image/chhatbir.jpg', 'dfgdfg', '2022-01-31 11:20:13', '2022-01-31 12:03:40', '1'),
(9, '/wildlifesymbol_image/photo-1575550959106-5a7defe28b56.JPG', 'testing', '2022-02-02 09:10:13', '2022-02-02 09:14:39', '1'),
(10, '/wildlifesymbol_image/bodyworn-794111_1920.jpg', 'Wildlife Symbols of Punjab.', '2022-02-02 09:12:56', '2022-02-02 09:14:18', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wildlife_symbol_title_image`
--

CREATE TABLE `wildlife_symbol_title_image` (
  `id` int NOT NULL,
  `wildlife_symbol_id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wildlife_symbol_title_image`
--

INSERT INTO `wildlife_symbol_title_image` (`id`, `wildlife_symbol_id`, `title`, `description`, `image`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'State Animal', 'Black Buck (Antelope cervicapra)', '/wildlifesymbol_image/1619517246.black_buck.jpg', '2021-04-27 09:36:26', '2021-04-27 09:54:06', '0'),
(2, 1, 'State Bird', 'Baaz (Accipiter gentilis)', '/wildlifesymbol_image/1619516186.state_bird.png', '2021-04-27 09:36:26', '2021-04-27 09:36:26', '0'),
(3, 1, 'State Tree', 'Shisham (Dalbergia sissoo)', '/wildlifesymbol_image/1619516186.state_tree.png', '2021-04-27 09:36:26', '2021-04-27 09:36:26', '0'),
(4, 1, 'State Aquatic Animal', 'Indus River Dolphin (Platanista gangetica minor)', '/wildlifesymbol_image/1619516186.state_aquatic.png', '2021-04-27 09:36:26', '2021-04-27 09:36:26', '0'),
(5, 2, 'State Bird', 'Black Buck (Antelope cervicapra)cxvxc', '/wildlifesymbol_image/1624342179.duck.jpg', '2021-06-22 06:09:39', '2021-06-22 06:09:39', '0'),
(6, 2, 'loreum 1', 'd1', '/wildlifesymbol_image/1624342179.img16.jpg', '2021-06-22 06:09:39', '2021-06-22 06:09:39', '0'),
(7, 2, 'Flight of Coots at Harike Wildlife Sanctuary', 'Black Buck (Antelope cervicapra)cxvxc', '/wildlifesymbol_image/1624342179.img15.jpg', '2021-06-22 06:09:39', '2021-06-22 06:09:39', '0'),
(8, 2, 'State Animal', 'Black Buck (Antelope cervicapra)', '/wildlifesymbol_image/1624342179.img13.jpg', '2021-06-22 06:09:39', '2021-06-22 06:09:39', '0'),
(9, 3, 'loreum', 'Black Buck (Antelope cervicapra)', '/wildlifesymbol_image/1628056667.elephant.jpg', '2021-08-04 05:57:47', '2021-08-04 05:57:47', '0'),
(10, 3, 'State Bird', 'Black Buck (Antelope cervicapra)cxvxc', '/wildlifesymbol_image/1628056667.img3.jpg', '2021-08-04 05:57:47', '2021-08-04 05:57:47', '0'),
(11, 3, 'State Animal', 'Black Buck (Antelope cervicapra)', '/wildlifesymbol_image/1628056667.img4.jpg', '2021-08-04 05:57:47', '2021-08-04 05:57:47', '0'),
(12, 3, 'Flight of Coots at Harike Wildlife Sanctuary', 'Black Buck (Antelope cervicapra)cxvxc', '/wildlifesymbol_image/1628056667.img5.jpg', '2021-08-04 05:57:47', '2021-08-04 05:57:47', '0'),
(13, 4, 'w', 'w', '/wildlifesymbol_image/1641972024.3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', '2022-01-12 07:20:24', '2022-01-12 07:20:24', '0'),
(14, 4, '', 'w', '/wildlifesymbol_image/1641972024.pexels-photo-247431.jpeg', '2022-01-12 07:20:24', '2022-01-12 07:20:24', '0'),
(15, 5, 'w', 'w', '/wildlifesymbol_image/1641972073.3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', '2022-01-12 07:21:13', '2022-01-12 07:21:13', '0'),
(16, 5, 'w', 'w', '/wildlifesymbol_image/1641972073.pexels-photo-247431.jpeg', '2022-01-12 07:21:13', '2022-01-12 07:21:13', '0'),
(17, 6, 'w', 'w', '/wildlifesymbol_image/1641972190.3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', '2022-01-12 07:23:10', '2022-01-12 07:23:10', '0'),
(18, 6, 'w', 'w', '/wildlifesymbol_image/1641972190.pexels-photo-247431.jpeg', '2022-01-12 07:23:10', '2022-01-12 07:23:10', '0'),
(19, 7, 't', 't', '/wildlifesymbol_image/1641972257.pexels-photo-247431.jpeg', '2022-01-12 07:24:17', '2022-01-12 07:24:17', '0'),
(20, 7, 't', 'tt', '/wildlifesymbol_image/1641972257.3bdb4c988d514f3384e53641c140832aae5b146382b6efb113f4b643a48296df._RI_.png', '2022-01-12 07:24:17', '2022-01-12 07:24:17', '0'),
(21, 7, '', 't', '/wildlifesymbol_image/1641972257.photo-1575550959106-5a7defe28b56.JPG', '2022-01-12 07:24:17', '2022-01-12 07:24:17', '0'),
(22, 7, 't', 'tt', '/wildlifesymbol_image/1641972257.pexels-photo-247431.jpeg', '2022-01-12 07:24:17', '2022-01-12 07:24:17', '0'),
(23, 8, 'not', 'jkjjjjjjjjjjjjjjjjjjjjjj', '/wildlifesymbol_image/1643628013.barheaded.png', '2022-01-31 11:20:13', '2022-01-31 11:21:16', '0'),
(24, 8, 'll', 'pp', '/wildlifesymbol_image/chhatbir.jpg', '2022-01-31 11:20:13', '2022-01-31 11:21:40', '0'),
(25, 8, 'gdddddddd', 'hfghfgh', '/wildlifesymbol_image/1643628013.harike.png', '2022-01-31 11:20:13', '2022-01-31 11:20:13', '0'),
(26, 8, 'gdddddddd', 'gddddddd', '/wildlifesymbol_image/1643628013.chhatbir.jpg', '2022-01-31 11:20:13', '2022-01-31 11:20:13', '0'),
(27, 9, 't', 't', '/wildlifesymbol_image/1643793013.pexels-photo-247431.jpeg', '2022-02-02 09:10:13', '2022-02-02 09:10:13', '0'),
(28, 10, 'hjh', 'jhggggggg', '/wildlifesymbol_image/1643793176.cat.jpg', '2022-02-02 09:12:56', '2022-02-02 09:12:56', '0');

-- --------------------------------------------------------

--
-- Stand-in structure for view `word_day_view`
-- (See below for the actual view)
--
CREATE TABLE `word_day_view` (
`id` int
,`type_name` varchar(255)
,`ryear` varchar(12)
,`event_date` varchar(255)
,`month_name` varchar(255)
,`image` varchar(255)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `world_day`
--

CREATE TABLE `world_day` (
  `id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `date_of_day` varchar(255) NOT NULL,
  `event_date` varchar(255) NOT NULL,
  `month_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `world_day`
--

INSERT INTO `world_day` (`id`, `type_name`, `date_of_day`, `event_date`, `month_name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Forest day', '2022-03-31', '31', 'March', '/world_day/wetland.png', '2021-06-03 11:49:52', '2022-02-09 11:57:13', NULL),
(2, 'World Wetland', '2022-02-02', '2', 'February', '/world_day/bird.png', '2021-06-03 11:49:58', '2022-02-09 11:57:37', NULL),
(3, 'World Migratory Bird Day', '2022-05-14', '14', 'May', '/world_day/bird.png', '2021-06-03 11:50:09', '2022-02-09 11:56:34', NULL),
(4, 'World atmosphere Day', '2022-06-05', '5', 'June', '/world_day/atmosphere.png', '2021-06-03 11:50:16', '2022-02-09 11:58:44', NULL),
(5, 'World Wildlife Day', '2022-03-03', '3', 'March', '/world_day/world-wildlife.png', '2021-06-04 16:57:26', '2022-02-18 07:23:05', NULL),
(6, 'World Sparrow Day', '2022-03-20', '20', 'March', '/world_day/bird.png', '2021-06-11 17:23:40', '2022-02-09 11:59:48', NULL),
(7, 'World Water Day', '2022-03-22', '22', 'March', '/world_day/water-day.png', '2021-06-11 17:23:46', '2022-02-09 12:00:11', NULL),
(8, 'World Environment Day', '2022-06-05', '5', 'June', '/world_day/World-Environment.png', '2021-06-11 17:23:51', '2022-02-09 12:00:39', NULL),
(9, 'World Nature Conservation Day', '2022-07-28', '28', 'July', '/world_day/nature-conservation.png', '2021-06-11 17:23:56', '2022-02-09 12:01:17', NULL),
(10, 'International Tiger Day', '2022-07-29', '29', 'July', '/world_day/tiger-day.png', '2021-06-11 17:24:01', '2022-02-09 12:01:45', NULL),
(11, 'World Ozone Day', '2022-09-16', '16', 'September', '/world_day/world-ozone-day.png', '2021-06-11 17:28:11', '2022-02-09 12:02:17', NULL),
(12, 'World Lion Day', '2022-08-10', '10', 'August', '/world_day/lion_day.png', '2021-06-11 17:31:09', '2022-02-09 12:02:46', NULL),
(13, 'World Elephant Day', '2022-08-12', '12', 'August', '/world_day/elephant.png', '2021-06-11 17:31:22', '2022-02-09 12:03:19', NULL),
(14, 'Wildlife Week', '2022-10-02', '2', 'October', '/world_day/wildlife-week.png', '2021-06-11 17:56:30', '2022-02-18 07:23:56', NULL),
(15, 'World Animal Welfare Day', '2022-10-04', '4', 'October', '/world_day/world animal day.png', '2021-06-11 18:17:26', '2022-02-09 12:04:31', NULL),
(16, 'International Day For Natural Disaster Reduction', '2022-10-13', '13', 'October', '/world_day/natural-disaster-reduction.png', '2021-06-11 18:20:46', '2022-02-09 12:05:07', NULL),
(17, 'Endangered Species Day', '2022-05-20', '20', 'May', '/world_day/Endangered-Species.png', '2021-06-11 18:25:45', '2022-02-09 12:05:44', NULL),
(18, 'International Day for Biological Diversity', '2022-05-22', '22', 'May', '/world_day/Biological-Diversity.png', '2021-06-11 18:25:52', '2022-02-09 12:06:19', NULL),
(19, 'Internationl Day of Action for Rivers', '2022-03-14', '14', 'March', '/world_day/international-day-of-action-for-rivers.png', '2022-02-05 15:39:45', '2022-02-09 12:06:50', NULL),
(20, 'World Animal Day', '2022-10-04', '4', 'October', '/world_day/world animal day.png', '2022-02-05 15:41:30', '2022-02-09 12:07:33', NULL),
(21, 'International Day of Forests', '2022-03-21', '21', 'March', '/world_day/forest.png', '2022-02-05 15:42:11', '2022-02-09 12:08:09', NULL),
(22, 'Earth Hour', '2022-03-26', '26', 'March', '/world_day/earth-hour.png', '2022-02-05 15:42:30', '2022-02-09 12:09:09', NULL),
(23, 'Earth Day', '2022-04-22', '22', 'April', '/world_day/earth-day.png', '2022-02-05 15:43:51', '2022-02-09 12:09:00', NULL),
(24, 'World Fisheries Day', '2022-11-21', '21', 'November', '/world_day/world-fisheries-day.png', '2022-02-05 15:50:07', '2022-02-09 12:09:40', NULL),
(25, 'World Oceans Day', '2022-06-08', '8', 'June', '/world_day/world-ocean-day.png', '2022-02-05 15:50:38', '2022-02-09 12:10:50', NULL),
(26, 'Van Mahotsava Fortnight', '2022-07-01', '1', 'July', '/world_day/van-mohatsava.png', '2022-02-05 15:51:32', '2022-02-09 12:11:34', NULL),
(27, 'World Plastic Bag Free Day', '2022-07-03', '3', 'July', '/world_day/world-plastic-bag.png', '2022-02-05 15:52:08', '2022-02-09 12:12:07', NULL),
(28, 'Save the Himalayas Day', '2022-09-09', '9', 'September', '/world_day/save-himalaya.png', '2022-02-05 15:52:22', '2022-02-09 12:17:47', NULL),
(29, 'World Nature Day', '2022-07-28', '28', 'July', '/world_day/wold-nature-day.png', '2022-02-05 15:53:23', '2022-02-09 12:18:26', NULL),
(30, 'World Habitat Day', '2022-10-03', '3', 'October', '/world_day/world-habitat-day.png', '2022-02-05 15:53:48', '2022-02-09 12:44:37', NULL),
(31, 'World Ecology Day', '2022-06-05', '5', 'June', '/world_day/world-ecology-day.png', '2022-02-05 15:54:29', '2022-02-09 12:45:09', NULL),
(32, 'International Animal Rights Day', '2022-12-10', '10', 'December', '/world_day/international-animal-right-day.png', '2022-02-05 15:54:44', '2022-02-09 12:46:17', NULL),
(33, 'International Mountain Day', '2022-12-11', '11', 'December', '/world_day/mountain-day.png', '2022-02-05 15:56:03', '2022-02-09 12:46:55', NULL),
(34, 'hgfhgu', '2022-02-19', '19', 'February', '/world_day/World-Environment.png', '2022-02-09 11:20:12', '2022-02-09 12:47:25', '2022-02-09 12:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `world_day_detail`
--

CREATE TABLE `world_day_detail` (
  `id` int NOT NULL,
  `world_day_type_id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_of_event` date NOT NULL,
  `event_date` varchar(255) NOT NULL,
  `month_name` varchar(255) NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `world_day_detail`
--

INSERT INTO `world_day_detail` (`id`, `world_day_type_id`, `type_name`, `image`, `date_of_event`, `event_date`, `month_name`, `description`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Forest day', '/world_day/wetland.png', '2022-03-31', '31', 'March', 'Forest Day', '2021-06-11 11:43:13', '2022-02-02 14:13:32', '0'),
(2, 5, 'World Wildlife Day', '/world_day/world-wildlife.png', '2021-03-03', '3', 'March', 'World Wildlife Day.', '2021-06-11 11:45:17', '2022-01-31 10:22:14', '0'),
(3, 2, 'World Wetlands Day', '/world_day/bird.png', '2021-02-02', '2', 'February', 'World Wetlands Day', '2021-06-11 11:46:41', '2022-01-14 07:01:14', '0'),
(5, 7, 'World Water Day', '/world_day/water-day.png', '2021-03-22', '22', 'March', 'World Water Day', '2021-06-11 11:49:22', '2021-06-16 05:28:14', '0'),
(6, 8, 'World Environment Day', '/world_day/World-Environment.png', '2021-06-05', '5', 'June', 'World Environment Day', '2021-06-11 11:50:43', '2021-06-16 05:28:38', '0'),
(7, 9, 'World Nature Conservation \r\nDay', '/world_day/nature-conservation.png', '2021-07-28', '28', 'July', 'World Nature Conservation Day', '2021-06-11 11:55:07', '2022-01-14 12:35:01', '0'),
(8, 10, 'International Tiger Day', '/world_day/tiger-day.png', '2021-07-29', '29', 'July', 'International Tiger Day', '2021-06-11 11:56:09', '2022-01-14 10:54:48', '0'),
(9, 11, 'World Ozone Day', '/world_day/world-ozone-day.png', '2021-09-16', '16', 'September', 'World Ozone Day', '2021-06-11 12:00:18', '2022-01-14 10:55:07', '0'),
(10, 12, 'World Lion Day', '/world_day/lion_day.png', '2021-08-10', '10', 'August', 'World Lion Day', '2021-06-11 12:02:33', '2021-09-27 11:19:11', '0'),
(11, 13, 'World Elephant Day', '/world_day/elephant.png', '2021-08-12', '12', 'August', 'World Elephant Day', '2021-06-11 12:04:05', '2021-09-27 11:20:12', '0'),
(17, 14, 'Wildlife Week', '/world_day/world-wildlife.png', '2021-10-07', '7', 'October', 'Wildlife Week', '2021-06-11 12:30:40', '2022-01-14 11:03:12', '0'),
(19, 15, 'World Animal Day', '/world_day/world animal day.png', '2021-10-04', '4', 'October', 'World Animal Welfare Day', '2021-06-11 12:49:00', '2022-01-14 10:59:08', '0'),
(20, 16, 'International Day For Natural Disaster Reduction', '/world_day/natural-disaster-reduction.png', '2021-10-13', '13', 'October', 'International Day for Disaster Reduction', '2021-06-11 12:51:56', '2022-01-14 12:36:07', '0'),
(21, 17, 'Endangered Species Day', '/world_day/Endangered-Species.png', '2021-05-21', '21', 'May', 'Endangered Species Day...', '2021-06-11 12:54:43', '2022-02-02 07:30:11', '0'),
(26, 19, 'Internationl Day of Action for Rivers', '/world_day/international-day-of-action-for-rivers.png', '2022-03-14', '14', 'March', 'International Day of Actin for Rivers', '2022-01-14 12:38:08', '2022-01-14 12:38:08', '0'),
(27, 21, 'International Day of Forests', '/world_day/forest.png', '2022-09-21', '21', 'September', 'Internatinal Day of Forests', '2022-01-14 12:39:30', '2022-01-14 12:39:30', '0'),
(28, 22, 'Earth Hour', '/world_day/earth-hour.png', '2022-03-30', '30', 'March', 'Earth Hour', '2022-01-14 12:40:02', '2022-01-14 12:40:02', '0'),
(29, 23, 'Earth Day', '/world_day/earth-day.png', '2022-04-22', '22', 'April', 'Earth Day', '2022-01-14 12:40:32', '2022-01-14 12:40:32', '0'),
(30, 25, 'World Oceans Day', '/world_day/world-ocean-day.png', '2022-06-08', '8', 'June', 'World Oceans Day', '2022-01-14 12:41:16', '2022-01-14 12:41:16', '0'),
(31, 26, 'Van Mahotsava Fortnight', '/world_day/van-mohatsava.png', '2022-07-14', '14', 'July', 'Van Mahotsava Fortnight', '2022-01-14 12:41:59', '2022-01-14 12:41:59', '0'),
(32, 27, 'World Plastic Bag Free Day', '/world_day/world-plastic-bag.png', '2022-07-03', '3', 'July', 'World Plastic Bag Free Day', '2022-01-14 12:42:40', '2022-01-14 12:42:40', '0'),
(33, 28, 'Save the Himalayas Day', '/world_day/save-himalaya.png', '2022-09-09', '9', 'September', 'Save the Himalayas Day', '2022-01-14 12:43:12', '2022-01-14 12:43:12', '0'),
(34, 29, 'World Nature Day', '/world_day/wold-nature-day.png', '2022-10-03', '3', 'October', 'World Nature Day', '2022-01-14 12:44:03', '2022-01-14 12:44:03', '0'),
(35, 30, 'World Habitat Day', '/world_day/world-habitat-day.png', '2022-10-07', '7', 'October', 'World Habitat Day', '2022-01-14 12:44:36', '2022-01-14 12:44:36', '0'),
(36, 31, 'World Ecology Day', '/world_day/world-ecology-day.png', '2022-11-01', '1', 'November', 'World Ecology Day', '2022-01-14 12:45:03', '2022-01-14 12:45:03', '0'),
(37, 33, 'World Fisheries Day', '/world_day/world-fisheries-day.png', '2022-11-21', '21', 'November', 'World Fisheries Day', '2022-01-14 12:45:34', '2022-01-14 12:45:34', '0'),
(38, 34, 'International Animal Rights Day', '/world_day/international-animal-right-day.png', '2022-12-10', '10', 'December', 'International Animal Rights Day', '2022-01-14 12:46:07', '2022-01-14 12:46:07', '0'),
(39, 35, 'International Mountain Day', '/world_day/mountain-day.png', '2022-12-11', '11', 'December', 'International Mountain Day', '2022-01-14 12:46:39', '2022-01-14 12:46:39', '0'),
(43, 2, 'World Wetland', '/world_day/campaforest.png', '2022-02-12', '12', 'February', NULL, '2022-02-05 10:31:20', '2022-02-05 10:31:26', '1');

-- --------------------------------------------------------

--
-- Table structure for table `world_day_details`
--

CREATE TABLE `world_day_details` (
  `id` int NOT NULL,
  `worldday_type_id` int NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `worldday_type_name` varchar(255) DEFAULT NULL,
  `worldday_date` varchar(255) DEFAULT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `world_day_details`
--

INSERT INTO `world_day_details` (`id`, `worldday_type_id`, `banner_image`, `worldday_type_name`, `worldday_date`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '/forestDay/slide2.png', 'Forest day', '31 March', '<p>ffffffffffffffffffffffg</p>', '2022-02-04 07:23:02', '2022-02-09 13:18:59', '2022-02-09 13:18:59'),
(2, 6, '/forestDay/slide1.png', 'World Sparrow Day', '20   March', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>', '2022-02-04 07:27:21', '2022-05-10 10:33:11', NULL),
(3, 8, '/forestDay/black_buck.jpg', NULL, ' 5 June', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2022-02-04 08:03:10', '2022-02-04 08:09:29', '2022-02-04 08:09:29'),
(4, 2, '/forestDay/slide1.png', 'World Wetland', '2   February', 'ffffffffffffffffff', '2022-02-09 11:35:51', '2022-02-09 13:18:55', '2022-02-09 13:18:55'),
(5, 2, '/forestDay/slide1.png', 'World Wetland', '2   February', 'ffffffffffffffffff', '2022-02-09 11:40:42', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(6, 2, '/forestDay/slide1.png', 'World Wetland', '2   February', 'ccccccccccccc', '2022-02-09 11:42:19', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(7, 2, '/forestDay/PHOTO-2021-06-15-12-49-39_2.jpg', 'World Wetland', '2   February', '<p>gjhjhjhjhjhjhjhjh</p>', '2022-05-12 08:30:31', '2022-05-12 08:30:31', NULL),
(8, 11, '/forestDay/punjabwilldifelogo.png', 'World Ozone Day', '16   September', '<p>dvvvvvvvvvvvvvvvvv</p>', '2022-05-18 12:09:57', '2022-05-18 12:09:57', NULL),
(9, 22, '/forestDay/punjabwilldifelogo.png', 'Earth Hour', '26   March', '<p>sffffffffffffffffffff</p>', '2022-05-18 12:11:41', '2022-05-18 12:11:41', NULL),
(10, 1, '/forestDay/punjabwilldifelogo.png', 'Forest day', '31   March', '<p>hhhhhhhhhhhhhhhhhfg</p>', '2022-05-18 12:26:45', '2022-05-18 12:26:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `world_day_images_detail`
--

CREATE TABLE `world_day_images_detail` (
  `id` int NOT NULL,
  `world_day_detail_id` int NOT NULL,
  `world_day_id` int NOT NULL,
  `images` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `world_day_images_detail`
--

INSERT INTO `world_day_images_detail` (`id`, `world_day_detail_id`, `world_day_id`, `images`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 1, '/forestDay/1622722256.img1.jpg', '2021-06-03 12:10:56', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(2, 5, 1, '/forestDay/1622722256.img2.jpg', '2021-06-03 12:10:56', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(3, 5, 1, '/forestDay/1622722256.img3.jpg', '2021-06-03 12:10:56', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(4, 5, 1, '/forestDay/1622722256.img4.jpg', '2021-06-03 12:10:56', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(5, 5, 1, '/forestDay/1622722256.img1.jpg', '2021-06-03 17:45:19', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(6, 5, 2, '/forestDay/1622722256.img1.jpg', '2021-06-03 17:45:27', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(7, 5, 2, '/forestDay/1622722256.img1.jpg', '2021-06-03 17:45:54', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(8, 5, 2, '/forestDay/1622722256.img1.jpg', '2021-06-03 17:45:59', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(9, 6, 2, '/forestDay/1622722802.img1.jpg', '2021-06-03 12:20:02', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(10, 6, 2, '/forestDay/1622722802.img2.jpg', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(11, 6, 2, '/forestDay/1622722802.img3.jpg', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(12, 6, 2, '/forestDay/1622722802.img4.jpg', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(13, 7, 3, '/forestDay/1622723457.img1.jpg', '2021-06-03 12:30:57', '2021-06-03 12:30:57', NULL),
(14, 7, 3, '/forestDay/1622723457.img2.jpg', '2021-06-03 12:30:57', '2021-06-03 12:30:57', NULL),
(15, 7, 3, '/forestDay/1622723457.img3.jpg', '2021-06-03 12:30:57', '2021-06-03 12:30:57', NULL),
(16, 7, 3, '/forestDay/1622723457.img4.jpg', '2021-06-03 12:30:57', '2021-06-03 12:30:57', NULL),
(17, 1, 1, '/forestDay/d3.jpg', '2022-02-05 07:35:13', '2022-02-05 08:30:54', '2022-02-05 08:30:54'),
(18, 1, 7, '/forestDay/cat.jpg', '2022-02-05 07:40:42', '2022-02-05 07:40:56', '2022-02-05 07:40:56'),
(19, 1, 1, '/forestDay/cat.jpg', '2022-02-05 08:30:42', '2022-02-09 13:18:59', '2022-02-09 13:18:59'),
(20, 2, 6, '/forestDay/2.png', '2022-02-11 14:10:52', '2022-02-11 14:10:52', NULL),
(21, 7, 2, '/forestDay/1652344231.PHOTO-2021-06-15-12-49-39_2.jpg', '2022-05-12 08:30:31', '2022-05-12 08:30:31', NULL),
(22, 7, 2, '/forestDay/1652344231.PHOTO-2021-06-15-12-49-39_5.jpg', '2022-05-12 08:30:31', '2022-05-12 08:30:31', NULL),
(23, 10, 1, '/forestDay/1652876805.RK Mishra.jpg', '2022-05-18 12:26:45', '2022-05-18 12:26:45', NULL),
(24, 10, 1, '/forestDay/1652876805.neeraj.jpg', '2022-05-18 12:26:45', '2022-05-18 12:26:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `world_day_images_honour`
--

CREATE TABLE `world_day_images_honour` (
  `id` int NOT NULL,
  `world_day_detail_id` int NOT NULL,
  `world_day_id` int NOT NULL,
  `guest_honour_name` varchar(255) NOT NULL,
  `guest_honour_title` varchar(255) NOT NULL,
  `guest_of_honour` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `world_day_images_honour`
--

INSERT INTO `world_day_images_honour` (`id`, `world_day_detail_id`, `world_day_id`, `guest_honour_name`, `guest_honour_title`, `guest_of_honour`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 1, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722256.team1.png', '2021-06-03 12:10:57', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(2, 5, 1, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722256.dummy.png', '2021-06-03 12:10:57', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(3, 5, 1, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722256.team1.png', '2021-06-03 12:10:57', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(4, 5, 1, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722256.dummy.png', '2021-06-03 12:10:57', '2022-02-09 13:18:51', '2022-02-09 13:18:51'),
(5, 6, 2, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722802.dummy.png', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(6, 6, 2, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722802.team1.png', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(7, 6, 2, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722802.dummy.png', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(8, 6, 2, 'Jane Doe', 'CEO & Founder', '/forestDay/1622722802.team1.png', '2021-06-03 12:20:03', '2022-02-09 13:18:47', '2022-02-09 13:18:47'),
(9, 7, 3, 'Jane Doe', 'CEO & Founder', '/forestDay/1622723457.dummy.png', '2021-06-03 12:30:57', '2021-06-03 12:30:57', NULL),
(10, 7, 3, 'Jane Doe', 'CEO & Founder', '/forestDay/1622723457.team1.png', '2021-06-03 12:30:57', '2021-06-03 12:30:57', NULL),
(11, 7, 3, 'Jane Doe', 'CEO & Founder', '/forestDay/1622723457.dummy.png', '2021-06-03 12:30:58', '2021-06-03 12:30:58', NULL),
(12, 7, 3, 'Jane Doe', 'CEO & Founder', '/forestDay/1622723457.dummy.png', '2021-06-03 12:30:58', '2021-06-03 12:30:58', NULL),
(13, 1, 1, 'ghjjjjjjjj', 'crg', '/forestDay/dummy.png', '2022-02-05 07:39:44', '2022-02-05 07:39:50', '2022-02-05 07:39:50'),
(14, 1, 2, 'ghjjjjjjjj', 'crg', '/forestDay/dummy.jpg', '2022-02-05 07:40:04', '2022-02-05 08:30:29', '2022-02-05 08:30:29'),
(15, 1, 1, 'ghjjjjjjjj', 'crg', '/forestDay/duck.jpg', '2022-02-05 08:30:24', '2022-02-05 08:30:26', '2022-02-05 08:30:26'),
(16, 2, 6, 'ttt', 'tttttt', '/forestDay/team1.png', '2022-02-11 14:11:35', '2022-02-11 14:11:45', '2022-02-11 14:11:45'),
(17, 7, 2, 'jgjk,gj', 'hghjgj', '/forestDay/1652344231.PHOTO-2021-06-15-12-49-39_4.jpg', '2022-05-12 08:30:31', '2022-05-12 08:30:31', NULL),
(18, 10, 1, 'jgjk,gj', 'hghjgj', '/forestDay/1652876805.RK Mishra.jpg', '2022-05-18 12:26:45', '2022-05-18 12:26:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zoo_detail`
--

CREATE TABLE `zoo_detail` (
  `id` int NOT NULL,
  `zoo_type_id` int NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `zoo_image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `zoo_detail`
--

INSERT INTO `zoo_detail` (`id`, `zoo_type_id`, `type_name`, `zoo_image`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, 1, 'Chhatbir Zoo', '/zoo_image/chhatbir.jpg', '2021-06-01 14:51:47', '2022-01-31 10:16:48', '0'),
(2, 2, 'Ludhiana Zoo', '/zoo_image/ludhiana.jpg', '2021-06-01 14:54:33', '2021-06-01 14:54:33', '0'),
(3, 3, 'Mini Zoo Patiala', '/zoo_image/black_buck.jpg', '2021-06-01 14:55:00', '2021-06-01 14:55:00', '0'),
(4, 4, 'Mini Zoo Bathinda', '/zoo_image/cat.jpg', '2021-06-01 14:55:13', '2021-06-01 14:55:13', '0'),
(5, 5, 'Deer Park Neelon', '/zoo_image/duck.jpg', '2021-06-01 14:55:57', '2021-06-01 14:55:57', '0'),
(6, 7, 'Harike Wildlife Sanctuary', '/zoo_image/Folder-hari-k-patan-1.jpg', '2021-06-01 14:56:23', '2022-02-09 13:36:24', '1'),
(7, 6, 'Chhatbir', '/zoo_image/pexels-photo-247431.jpeg', '2022-01-12 06:47:47', '2022-01-31 09:53:03', '1'),
(8, 1, 'Chhatbir Zoo', '/zoo_image/3.png', '2022-01-31 09:53:12', '2022-01-31 10:08:31', '1'),
(9, 8, 'Nangal Wildlife Sanctuary', '/zoo_image/pexels-photo-247431.jpeg', '2022-02-02 08:48:38', '2022-02-09 13:36:18', '1'),
(10, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644413793.jpg', '2022-02-09 13:36:33', '2022-05-10 10:33:59', '0'),
(11, 12, 'test', '/zoo_gallery/1650453002.jpg', '2022-04-20 11:10:02', '2022-04-20 11:15:49', '1'),
(12, 12, 'test', '/zoo_gallery/1650453386.jpg', '2022-04-20 11:16:26', '2022-04-20 11:16:26', '0');

-- --------------------------------------------------------

--
-- Table structure for table `zoo_gallery`
--

CREATE TABLE `zoo_gallery` (
  `id` int NOT NULL,
  `zoo_type_id` int NOT NULL,
  `zoo_type_name` varchar(255) NOT NULL,
  `zoo_image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `zoo_gallery`
--

INSERT INTO `zoo_gallery` (`id`, `zoo_type_id`, `zoo_type_name`, `zoo_image`, `created_at`, `updated_at`, `deleted_status`) VALUES
(2, 1, 'Chhatbir Zoo', '/zoo_gallery/1650886813.jpg', '2021-06-02 08:59:00', '2022-05-03 08:21:10', '1'),
(3, 3, 'Mini Zoo Patiala', '/zoo_gallery/img3.jpg', '2021-06-02 08:59:16', '2021-06-02 08:59:16', '0'),
(4, 4, 'Mini Zoo Bathinda', '/zoo_gallery/img5.jpg', '2021-06-02 08:59:34', '2021-06-02 08:59:34', '0'),
(7, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/img13.jpg', '2021-06-02 09:00:09', '2021-06-02 09:00:09', '0'),
(8, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/img14.jpg', '2021-06-02 09:00:19', '2022-02-07 06:28:24', '1'),
(9, 9, 'Keshopur Community Reserve, Gurdaspur', '/zoo_gallery/img11.jpg', '2021-06-02 09:00:35', '2021-06-02 09:00:35', '0'),
(10, 1, 'Chhatbir Zoo', '/zoo_gallery/img13.jpg', '2021-06-10 12:42:23', '2022-05-03 08:21:50', '1'),
(12, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911339.jpg', '2021-06-17 06:28:59', '2021-06-17 06:28:59', '0'),
(13, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911356.jpg', '2021-06-17 06:29:16', '2021-06-17 06:29:16', '0'),
(14, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911363.jpg', '2021-06-17 06:29:23', '2021-06-17 06:29:23', '0'),
(15, 2, 'Ludhiana Zoo', '/zoo_gallery/1650882025.jpg', '2021-06-17 06:29:31', '2022-04-25 10:20:25', '0'),
(16, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911379.jpg', '2021-06-17 06:29:39', '2021-06-17 06:29:39', '0'),
(17, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911385.jpg', '2021-06-17 06:29:45', '2021-06-17 06:29:45', '0'),
(18, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911393.jpg', '2021-06-17 06:29:53', '2021-06-17 06:29:53', '0'),
(19, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911400.jpg', '2021-06-17 06:30:00', '2021-06-17 06:30:00', '0'),
(20, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911406.jpg', '2021-06-17 06:30:06', '2021-06-17 06:30:06', '0'),
(21, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911413.jpg', '2021-06-17 06:30:13', '2021-06-17 06:30:13', '0'),
(22, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911419.jpg', '2021-06-17 06:30:19', '2021-06-17 06:30:19', '0'),
(23, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911426.jpg', '2021-06-17 06:30:26', '2021-06-17 06:30:26', '0'),
(24, 2, 'Ludhiana Zoo', '/zoo_gallery/1623911432.jpg', '2021-06-17 06:30:32', '2021-06-17 06:30:32', '0'),
(25, 5, 'Deer Park Neelon', '/zoo_gallery/1623911536.jpg', '2021-06-17 06:32:16', '2021-06-17 06:32:16', '0'),
(26, 5, 'Deer Park Neelon', '/zoo_gallery/1623911544.jpg', '2021-06-17 06:32:24', '2021-06-17 06:32:24', '0'),
(27, 5, 'Deer Park Neelon', '/zoo_gallery/1623911551.jpg', '2021-06-17 06:32:31', '2021-06-17 06:32:31', '0'),
(28, 5, 'Deer Park Neelon', '/zoo_gallery/1623911560.jpg', '2021-06-17 06:32:40', '2021-06-17 06:32:40', '0'),
(29, 5, 'Deer Park Neelon', '/zoo_gallery/1623911567.jpg', '2021-06-17 06:32:47', '2021-06-17 06:32:47', '0'),
(30, 5, 'Deer Park Neelon', '/zoo_gallery/1623911573.jpg', '2021-06-17 06:32:53', '2021-06-17 06:32:53', '0'),
(31, 1, 'Chhatbir Zoo', '/zoo_gallery/01.jpg', '2021-12-27 10:59:57', '2022-05-03 08:21:56', '1'),
(32, 6, 'Chhatbir', '/zoo_gallery/1641970796.JPG', '2022-01-12 06:59:57', '2022-01-12 06:59:57', '0'),
(33, 6, 'Chhatbir', '/zoo_gallery/1641970905.jpeg', '2022-01-12 07:01:45', '2022-01-12 07:01:45', '0'),
(34, 1, 'Chhatbir Zoo', '/zoo_gallery/1643624425.jpg', '2022-01-31 10:20:25', '2022-05-03 08:22:00', '1'),
(35, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1643791901.jpeg', '2022-02-02 08:51:41', '2022-02-02 08:56:15', '1'),
(36, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/photo-1575550959106-5a7defe28b56.JPG', '2022-02-02 08:52:38', '2022-02-02 08:56:13', '1'),
(38, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1643804312.JPG', '2022-02-02 12:18:32', '2022-02-07 06:28:21', '1'),
(39, 1, 'Chhatbir Zoo', '/zoo_gallery/1643880804.jpeg', '2022-02-03 09:33:24', '2022-05-03 08:22:22', '1'),
(40, 1, 'Chhatbir Zoo', '/zoo_gallery/1644052568.jpg', '2022-02-05 09:16:08', '2022-02-07 03:44:47', '1'),
(41, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644204951.jpg', '2022-02-07 03:35:51', '2022-02-07 03:35:51', '0'),
(42, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644204967.jpg', '2022-02-07 03:36:07', '2022-02-07 03:36:07', '0'),
(43, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644204984.jpg', '2022-02-07 03:36:24', '2022-02-07 03:36:24', '0'),
(44, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644205009.jpg', '2022-02-07 03:36:50', '2022-02-07 03:36:50', '0'),
(45, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644205025.jpg', '2022-02-07 03:37:05', '2022-02-07 03:37:05', '0'),
(46, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644205049.jpg', '2022-02-07 03:37:29', '2022-02-07 03:37:29', '0'),
(47, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644205062.jpg', '2022-02-07 03:37:42', '2022-02-07 03:37:42', '0'),
(48, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644205121.jpg', '2022-02-07 03:38:41', '2022-02-07 03:38:41', '0'),
(49, 11, 'Abohar Wildlife Sanctuary', '/zoo_gallery/1644205182.jpg', '2022-02-07 03:39:42', '2022-02-07 03:39:42', '0'),
(50, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205275.jpg', '2022-02-07 03:41:15', '2022-02-07 03:41:15', '0'),
(51, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205283.jpg', '2022-02-07 03:41:23', '2022-02-07 03:41:23', '0'),
(52, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205292.jpg', '2022-02-07 03:41:32', '2022-02-07 03:41:32', '0'),
(53, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205302.jpg', '2022-02-07 03:41:42', '2022-02-07 03:43:30', '1'),
(54, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205310.jpg', '2022-02-07 03:41:50', '2022-02-07 03:41:50', '0'),
(55, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205320.jpg', '2022-02-07 03:42:00', '2022-02-07 03:42:00', '0'),
(56, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205332.jpg', '2022-02-07 03:42:12', '2022-02-07 03:42:12', '0'),
(57, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205349.jpg', '2022-02-07 03:42:29', '2022-02-07 03:42:29', '0'),
(58, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205358.jpg', '2022-02-07 03:42:38', '2022-02-07 03:42:38', '0'),
(59, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205366.jpg', '2022-02-07 03:42:46', '2022-02-07 03:42:46', '0'),
(60, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205375.jpg', '2022-02-07 03:42:55', '2022-02-07 03:42:55', '0'),
(61, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644205383.jpg', '2022-02-07 03:43:03', '2022-02-07 03:43:03', '0'),
(62, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644209942.jpg', '2022-02-07 04:59:02', '2022-02-07 04:59:02', '0'),
(63, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644209951.jpg', '2022-02-07 04:59:11', '2022-02-07 04:59:11', '0'),
(64, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644209989.jpg', '2022-02-07 04:59:49', '2022-02-07 04:59:49', '0'),
(65, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210006.jpg', '2022-02-07 05:00:06', '2022-02-07 05:00:06', '0'),
(66, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210006.jpg', '2022-02-07 05:00:07', '2022-02-07 05:00:07', '0'),
(67, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210020.jpg', '2022-02-07 05:00:20', '2022-02-07 05:00:20', '0'),
(68, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210029.jpg', '2022-02-07 05:00:29', '2022-02-07 05:00:29', '0'),
(69, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210041.jpg', '2022-02-07 05:00:41', '2022-02-07 05:00:41', '0'),
(70, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210051.jpg', '2022-02-07 05:00:51', '2022-02-07 05:00:51', '0'),
(71, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210069.jpg', '2022-02-07 05:01:09', '2022-02-07 05:01:09', '0'),
(72, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210083.jpg', '2022-02-07 05:01:23', '2022-02-07 05:01:23', '0'),
(73, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210093.jpg', '2022-02-07 05:01:33', '2022-02-07 05:01:33', '0'),
(74, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210182.jpg', '2022-02-07 05:03:02', '2022-02-07 05:03:02', '0'),
(75, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210199.jpg', '2022-02-07 05:03:19', '2022-02-07 05:03:19', '0'),
(76, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210226.jpg', '2022-02-07 05:03:46', '2022-02-07 05:03:46', '0'),
(77, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210240.jpg', '2022-02-07 05:04:00', '2022-02-07 05:04:00', '0'),
(78, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210263.jpg', '2022-02-07 05:04:23', '2022-02-07 05:04:23', '0'),
(79, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210281.jpg', '2022-02-07 05:04:41', '2022-02-07 05:04:41', '0'),
(80, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210301.jpg', '2022-02-07 05:05:01', '2022-02-07 05:05:01', '0'),
(81, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210320.jpg', '2022-02-07 05:05:20', '2022-02-07 05:05:20', '0'),
(82, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210337.jpg', '2022-02-07 05:05:37', '2022-02-07 05:05:37', '0'),
(83, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210352.jpg', '2022-02-07 05:05:52', '2022-02-07 05:05:52', '0'),
(84, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210374.jpg', '2022-02-07 05:06:14', '2022-02-07 05:06:14', '0'),
(85, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210392.jpg', '2022-02-07 05:06:32', '2022-02-07 05:06:32', '0'),
(86, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644210491.jpg', '2022-02-07 05:08:11', '2022-02-07 05:08:11', '0'),
(87, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215045.jpg', '2022-02-07 06:24:06', '2022-02-07 06:24:06', '0'),
(88, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215058.jpg', '2022-02-07 06:24:18', '2022-02-07 06:24:18', '0'),
(89, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215077.jpg', '2022-02-07 06:24:38', '2022-02-07 06:24:38', '0'),
(90, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215088.jpg', '2022-02-07 06:24:49', '2022-02-07 06:24:49', '0'),
(91, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215101.jpg', '2022-02-07 06:25:02', '2022-02-07 06:25:02', '0'),
(92, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215113.jpg', '2022-02-07 06:25:13', '2022-02-07 06:25:13', '0'),
(93, 7, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644215124.jpg', '2022-02-07 06:25:24', '2022-02-07 06:25:24', '0'),
(94, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215318.jpeg', '2022-02-07 06:28:38', '2022-02-07 06:28:38', '0'),
(95, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215348.jpeg', '2022-02-07 06:29:08', '2022-02-07 06:29:08', '0'),
(96, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215661.jpeg', '2022-02-07 06:34:21', '2022-02-07 06:34:21', '0'),
(97, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215681.jpg', '2022-02-07 06:34:42', '2022-02-07 06:34:42', '0'),
(98, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215692.jpg', '2022-02-07 06:34:53', '2022-02-07 06:34:53', '0'),
(99, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215701.jpg', '2022-02-07 06:35:02', '2022-02-07 06:35:02', '0'),
(100, 8, 'Nangal Wildlife Sanctuary', '/zoo_gallery/1644215714.jpeg', '2022-02-07 06:35:14', '2022-02-07 06:35:14', '0'),
(101, 1, 'Chhatbir Zoo', '/zoo_gallery/1644403643.jpg', '2022-02-09 10:47:23', '2022-05-03 08:22:25', '1'),
(102, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644413891.jpg', '2022-02-09 13:38:11', '2022-02-09 13:38:11', '0'),
(103, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465021.jpg', '2022-02-10 03:50:21', '2022-02-10 03:50:21', '0'),
(104, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465035.jpg', '2022-02-10 03:50:35', '2022-02-10 03:50:35', '0'),
(105, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465049.jpg', '2022-02-10 03:50:49', '2022-02-10 03:50:49', '0'),
(106, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465119.jpg', '2022-02-10 03:51:59', '2022-02-10 03:51:59', '0'),
(107, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465129.jpg', '2022-02-10 03:52:09', '2022-02-10 03:52:09', '0'),
(108, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465139.jpg', '2022-02-10 03:52:19', '2022-02-10 03:52:19', '0'),
(109, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465158.jpg', '2022-02-10 03:52:38', '2022-02-10 03:52:38', '0'),
(110, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465169.jpg', '2022-02-10 03:52:49', '2022-02-10 03:52:49', '0'),
(111, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465182.jpg', '2022-02-10 03:53:02', '2022-02-10 03:53:02', '0'),
(112, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465221.jpg', '2022-02-10 03:53:42', '2022-02-10 03:53:42', '0'),
(113, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465302.jpg', '2022-02-10 03:55:02', '2022-02-10 03:55:02', '0'),
(114, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644465919.jpg', '2022-02-10 04:05:19', '2022-02-10 04:06:16', '1'),
(115, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644466040.jpg', '2022-02-10 04:07:22', '2022-02-10 04:07:22', '0'),
(116, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1644466055.jpg', '2022-02-10 04:07:38', '2022-02-10 04:07:38', '0'),
(117, 12, 'test', '/zoo_gallery/1650452939.jpg', '2022-04-20 11:08:59', '2022-04-20 11:08:59', '0'),
(118, 1, 'Chhatbir Zoo', '/zoo_gallery/1650872193.jpg', '2022-04-25 07:36:33', '2022-04-25 10:47:00', '1'),
(119, 1, 'Chhatbir Zoo', '/zoo_gallery/1650872287.jpg', '2022-04-25 07:38:07', '2022-04-25 10:46:55', '1'),
(120, 1, 'Chhatbir Zoo', '/zoo_gallery/1650872365.jpg', '2022-04-25 07:39:25', '2022-04-25 07:40:52', '1'),
(121, 1, 'Chhatbir Zoo', '/zoo_gallery/1650872464.jpg', '2022-04-25 07:41:04', '2022-04-25 10:46:50', '1'),
(122, 1, 'Chhatbir Zoo', '/zoo_gallery/1650880073.jpg', '2022-04-25 07:41:24', '2022-04-25 09:50:23', '1'),
(123, 1, 'Chhatbir Zoo', '/zoo_gallery/1650876071.jpg', '2022-04-25 08:41:11', '2022-04-25 10:45:45', '1'),
(124, 1, 'Chhatbir Zoo', '/zoo_gallery/1650876088.jpg', '2022-04-25 08:41:28', '2022-04-25 10:46:01', '1'),
(125, 1, 'Chhatbir Zoo', '/zoo_gallery/1650876230.jpg', '2022-04-25 08:43:50', '2022-04-25 09:52:53', '1'),
(126, 1, 'Chhatbir Zoo', '/zoo_gallery/1650876242.jpg', '2022-04-25 08:44:02', '2022-04-25 10:46:07', '1'),
(127, 1, 'Chhatbir Zoo', '/zoo_gallery/1650876268.jpg', '2022-04-25 08:44:28', '2022-04-25 09:53:01', '1'),
(128, 1, 'Chhatbir Zoo', '/zoo_gallery/1650879423.jpg', '2022-04-25 09:37:04', '2022-04-25 10:46:13', '1'),
(129, 1, 'Chhatbir Zoo', '/zoo_gallery/1650880485.jpg', '2022-04-25 09:38:20', '2022-04-25 10:46:18', '1'),
(130, 1, 'Chhatbir Zoo', '/zoo_gallery/1650879536.jpg', '2022-04-25 09:38:56', '2022-04-25 09:54:33', '1'),
(131, 1, 'Chhatbir Zoo', '/zoo_gallery/1650879736.jpg', '2022-04-25 09:42:16', '2022-04-25 10:46:23', '1'),
(132, 1, 'Chhatbir Zoo', '/zoo_gallery/1650879779.jpg', '2022-04-25 09:42:59', '2022-04-25 10:46:28', '1'),
(133, 1, 'Chhatbir Zoo', '/zoo_gallery/1650880086.jpg', '2022-04-25 09:48:06', '2022-04-25 09:49:27', '1'),
(134, 1, 'Chhatbir Zoo', '/zoo_gallery/1650880135.jpg', '2022-04-25 09:48:55', '2022-04-25 09:49:22', '1'),
(135, 1, 'Chhatbir Zoo', '/zoo_gallery/1650881918.jpg', '2022-04-25 10:18:38', '2022-04-25 10:46:44', '1'),
(136, 1, 'Chhatbir Zoo', '/zoo_gallery/1650881956.jpg', '2022-04-25 10:19:16', '2022-04-25 10:46:35', '1'),
(137, 1, 'Chhatbir Zoo', '/zoo_gallery/1650883635.jpg', '2022-04-25 10:47:16', '2022-05-03 08:22:59', '1'),
(138, 2, 'Ludhiana Zoo', '/zoo_gallery/1650883801.jpg', '2022-04-25 10:50:01', '2022-04-25 10:50:01', '0'),
(139, 3, 'Mini Zoo Patiala', '/zoo_gallery/1650883817.jpg', '2022-04-25 10:50:17', '2022-04-25 10:50:17', '0'),
(140, 4, 'Mini Zoo Bathinda', '/zoo_gallery/1650883831.jpg', '2022-04-25 10:50:31', '2022-04-25 10:50:31', '0'),
(141, 5, 'Deer Park Neelon', '/zoo_gallery/1650883843.jpg', '2022-04-25 10:50:43', '2022-04-25 10:50:43', '0'),
(142, 10, 'Harike Wildlife Sanctuary', '/zoo_gallery/1650883852.jpg', '2022-04-25 10:50:52', '2022-04-25 10:50:52', '0'),
(143, 1, 'Chhatbir Zoo', '/zoo_gallery/1650886490.jpg', '2022-04-25 11:34:50', '2022-05-03 08:23:05', '1'),
(144, 1, 'Chhatbir Zoo', '/zoo_gallery/1650886545.jpg', '2022-04-25 11:35:45', '2022-05-03 08:23:33', '1'),
(145, 1, 'Chhatbir Zoo', '/zoo_gallery/1650886602.jpg', '2022-04-25 11:36:43', '2022-05-03 08:23:36', '1'),
(146, 1, 'Chhatbir Zoo', '/zoo_gallery/1650886881.jpg', '2022-04-25 11:37:17', '2022-05-03 08:23:45', '1'),
(147, 1, 'Chhatbir Zoo', '/zoo_gallery/1651498184.jpg', '2022-05-02 13:29:44', '2022-05-03 08:23:22', '1'),
(148, 80001, 'Wildlife Santuary', '/zoo_gallery/1651498700.jpg', '2022-05-02 13:38:20', '2022-05-02 13:38:20', '0'),
(149, 80001, 'Wildlife Santuary', '/zoo_gallery/1651498973.jpg', '2022-05-02 13:42:53', '2022-05-02 13:42:53', '0'),
(150, 1, 'Chhatbir Zoo', '/zoo_gallery/1651566302.jpg', '2022-05-03 08:24:46', '2022-05-03 08:30:24', '1'),
(151, 1, 'Chhatbir Zoo', '/zoo_gallery/1651566804.jpg', '2022-05-03 08:32:44', '2022-05-03 08:33:45', '1'),
(152, 1, 'Chhatbir Zoo', '/zoo_gallery/PHOTO-2021-06-15-12-49-39_3.jpg', '2022-05-11 12:25:12', '2022-05-11 12:25:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `zoo_gallery_banner`
--

CREATE TABLE `zoo_gallery_banner` (
  `id` int NOT NULL,
  `zoo_gallery_banner` varchar(255) NOT NULL,
  `zoo_gallery_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `zoo_gallery_banner`
--

INSERT INTO `zoo_gallery_banner` (`id`, `zoo_gallery_banner`, `zoo_gallery_heading`, `created_at`, `updated_at`, `deleted_status`) VALUES
(1, '/zoo_gallery/slide1.png', 'Gallery', '2021-06-02 06:35:26', '2021-12-27 12:51:38', '0'),
(2, '/zoo_gallery/forestday-event.jpg', 'Gallery', '2021-08-03 11:10:47', '2021-08-03 11:10:51', '1'),
(3, '/zoo_gallery/photo-1575550959106-5a7defe28b56.jfif', 'testing', '2022-01-12 06:52:44', '2022-01-12 06:54:00', '1'),
(4, '/zoo_gallery/barheaded.png', 'test gallery', '2022-01-31 10:18:16', '2022-01-31 10:18:24', '1'),
(5, '/zoo_gallery/pexels-photo-247431.jpeg', 'test', '2022-02-02 08:47:42', '2022-02-02 08:47:59', '1');

-- --------------------------------------------------------

--
-- Table structure for table `zoo_inventory`
--

CREATE TABLE `zoo_inventory` (
  `id` int NOT NULL,
  `zoo_table_id` int NOT NULL DEFAULT '0',
  `zoo_type_id` int NOT NULL,
  `animal_name` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `zoo_inventory`
--

INSERT INTO `zoo_inventory` (`id`, `zoo_table_id`, `zoo_type_id`, `animal_name`, `quantity`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 2, 'Lion', '1', '2022-05-30', '2022-02-10 14:20:34', '2022-02-10 14:43:57', NULL),
(2, 0, 2, 'Tiger', '2', '2022-05-30', '2022-02-10 14:22:41', '2022-02-10 14:22:41', NULL),
(3, 0, 1, 'Lion', '1', '2022-05-30', '2022-02-10 14:33:18', '2022-02-10 14:33:18', NULL),
(4, 0, 1, 'f', '1', '2022-05-30', '2022-02-15 05:45:35', '2022-02-15 05:46:02', '2022-02-15 05:46:02'),
(5, 9, 1, 'rtert', '1', '2022-05-30', '2022-05-09 10:48:12', '2022-05-09 10:48:44', '2022-05-09 10:48:44'),
(6, 9, 1, 'rtert', '5', '2022-05-30', '2022-05-09 10:48:12', '2022-05-09 10:48:44', '2022-05-09 10:48:44'),
(7, 10, 1, 'rtert', '1', '2022-05-30', '2022-05-10 10:40:53', '2022-05-10 10:48:26', '2022-05-10 10:48:26'),
(8, 0, 2, 'lion', '2 g', '2022-05-30', '2022-05-30 06:33:12', '2022-05-30 06:33:15', '2022-05-30 06:33:15'),
(9, 0, 2, 'lion', '52', '2022-05-30', '2022-05-31 04:28:57', '2022-05-31 04:28:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zoo_type`
--

CREATE TABLE `zoo_type` (
  `id` int NOT NULL,
  `zoo_type_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `zoo_type`
--

INSERT INTO `zoo_type` (`id`, `zoo_type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Chhatbir Zoo', '2021-06-01 20:05:07', '2021-06-01 20:05:07', NULL),
(2, 'Ludhiana Zoo', '2021-06-01 20:05:07', '2021-06-01 20:05:07', NULL),
(3, 'Mini Zoo Patiala', '2021-06-01 20:05:55', '2021-06-01 20:05:55', NULL),
(4, 'Mini Zoo Bathinda', '2021-06-01 20:05:55', '2021-06-01 20:05:55', NULL),
(5, 'Deer Park Neelon', '2021-06-01 20:06:34', '2022-05-09 11:08:03', NULL),
(10, 'Harike Wildlife Sanctuary', '2022-02-05 07:53:41', '2022-02-09 13:37:09', NULL),
(12, 'test', '2022-04-20 11:08:08', '2022-05-03 05:06:34', '2022-05-03 05:06:34');

-- --------------------------------------------------------

--
-- Structure for view `word_day_view`
--
DROP TABLE IF EXISTS `word_day_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `word_day_view`  AS SELECT `world_day`.`id` AS `id`, `world_day`.`type_name` AS `type_name`, concat((year(curdate()) - 1),date_format(`world_day`.`date_of_day`,'-%m-%d')) AS `ryear`, `world_day`.`event_date` AS `event_date`, `world_day`.`month_name` AS `month_name`, `world_day`.`image` AS `image`, `world_day`.`created_at` AS `created_at`, `world_day`.`updated_at` AS `updated_at`, `world_day`.`deleted_at` AS `deleted_at` FROM `world_day` WHERE (`world_day`.`deleted_at` is null) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `act_rule_banner`
--
ALTER TABLE `act_rule_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_headings`
--
ALTER TABLE `act_rule_headings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_management_detail`
--
ALTER TABLE `act_rule_management_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_management_heading`
--
ALTER TABLE `act_rule_management_heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_pdf_detail`
--
ALTER TABLE `act_rule_pdf_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_pdf_three`
--
ALTER TABLE `act_rule_pdf_three`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_pdf_two`
--
ALTER TABLE `act_rule_pdf_two`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `act_rule_subheading`
--
ALTER TABLE `act_rule_subheading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_address`
--
ALTER TABLE `contact_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us_banner_detail`
--
ALTER TABLE `contact_us_banner_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us_detail`
--
ALTER TABLE `contact_us_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us_message`
--
ALTER TABLE `contact_us_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_bg_images`
--
ALTER TABLE `event_bg_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_master`
--
ALTER TABLE `event_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_master_gallery_images`
--
ALTER TABLE `event_master_gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `footer_about`
--
ALTER TABLE `footer_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_about_detail`
--
ALTER TABLE `footer_about_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_policy_pdf`
--
ALTER TABLE `footer_policy_pdf`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_scheme`
--
ALTER TABLE `footer_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_scheme_name`
--
ALTER TABLE `footer_scheme_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forest_detail`
--
ALTER TABLE `forest_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_protectedarea_notification`
--
ALTER TABLE `frontend_protectedarea_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_protectedwetland_notification`
--
ALTER TABLE `frontend_protectedwetland_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_protected_area`
--
ALTER TABLE `frontend_protected_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_protected_area_images`
--
ALTER TABLE `frontend_protected_area_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_protected_wetland_detail`
--
ALTER TABLE `frontend_protected_wetland_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_wetland_images`
--
ALTER TABLE `frontend_wetland_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_zoo_detail`
--
ALTER TABLE `frontend_zoo_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_all_images`
--
ALTER TABLE `gallery_all_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_geographic_zone`
--
ALTER TABLE `home_geographic_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_detail`
--
ALTER TABLE `home_page_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `how_apply_type`
--
ALTER TABLE `how_apply_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `how_to_apply_detail`
--
ALTER TABLE `how_to_apply_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imp_link_detail`
--
ALTER TABLE `imp_link_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imp_link_links`
--
ALTER TABLE `imp_link_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_banner_images_text`
--
ALTER TABLE `landing_banner_images_text`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `management_nangal`
--
ALTER TABLE `management_nangal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `management_nangal_pdf`
--
ALTER TABLE `management_nangal_pdf`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `minister_detail`
--
ALTER TABLE `minister_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_banner_detail`
--
ALTER TABLE `notification_banner_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_detail`
--
ALTER TABLE `notification_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_pdf_detail`
--
ALTER TABLE `notification_pdf_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_banner_detail`
--
ALTER TABLE `organisation_banner_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_table_detail`
--
ALTER TABLE `organisation_table_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_tabs`
--
ALTER TABLE `organisation_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `protectedarea_category`
--
ALTER TABLE `protectedarea_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `protected_area_subcategory`
--
ALTER TABLE `protected_area_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `protected_wetland`
--
ALTER TABLE `protected_wetland`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `protected_wetland_images`
--
ALTER TABLE `protected_wetland_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `protected_wetland_noti_images`
--
ALTER TABLE `protected_wetland_noti_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_detail`
--
ALTER TABLE `services_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_how_to_apply`
--
ALTER TABLE `services_how_to_apply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_banner_detail`
--
ALTER TABLE `tender_banner_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_details`
--
ALTER TABLE `tender_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_types`
--
ALTER TABLE `tender_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourism_banner_detail`
--
ALTER TABLE `tourism_banner_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourism_data`
--
ALTER TABLE `tourism_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourism_link_detail`
--
ALTER TABLE `tourism_link_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourism_link_images`
--
ALTER TABLE `tourism_link_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourism_types`
--
ALTER TABLE `tourism_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upcoming_event_detail`
--
ALTER TABLE `upcoming_event_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `volunteer_detail`
--
ALTER TABLE `volunteer_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wetland_authority`
--
ALTER TABLE `wetland_authority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wetland_notification`
--
ALTER TABLE `wetland_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wetland_rules`
--
ALTER TABLE `wetland_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whatsnew_detail`
--
ALTER TABLE `whatsnew_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wildlife_symbol`
--
ALTER TABLE `wildlife_symbol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wildlife_symbol_title_image`
--
ALTER TABLE `wildlife_symbol_title_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `world_day`
--
ALTER TABLE `world_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `world_day_detail`
--
ALTER TABLE `world_day_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `world_day_details`
--
ALTER TABLE `world_day_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `world_day_images_detail`
--
ALTER TABLE `world_day_images_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `world_day_images_honour`
--
ALTER TABLE `world_day_images_honour`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zoo_detail`
--
ALTER TABLE `zoo_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zoo_gallery`
--
ALTER TABLE `zoo_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zoo_gallery_banner`
--
ALTER TABLE `zoo_gallery_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zoo_inventory`
--
ALTER TABLE `zoo_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zoo_type`
--
ALTER TABLE `zoo_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `act_rule_banner`
--
ALTER TABLE `act_rule_banner`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `act_rule_headings`
--
ALTER TABLE `act_rule_headings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `act_rule_management_detail`
--
ALTER TABLE `act_rule_management_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `act_rule_management_heading`
--
ALTER TABLE `act_rule_management_heading`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `act_rule_pdf_detail`
--
ALTER TABLE `act_rule_pdf_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `act_rule_pdf_three`
--
ALTER TABLE `act_rule_pdf_three`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `act_rule_pdf_two`
--
ALTER TABLE `act_rule_pdf_two`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `act_rule_subheading`
--
ALTER TABLE `act_rule_subheading`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `contact_address`
--
ALTER TABLE `contact_address`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_us_banner_detail`
--
ALTER TABLE `contact_us_banner_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us_detail`
--
ALTER TABLE `contact_us_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_us_message`
--
ALTER TABLE `contact_us_message`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `event_bg_images`
--
ALTER TABLE `event_bg_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event_master`
--
ALTER TABLE `event_master`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80003;

--
-- AUTO_INCREMENT for table `event_master_gallery_images`
--
ALTER TABLE `event_master_gallery_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer_about`
--
ALTER TABLE `footer_about`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `footer_about_detail`
--
ALTER TABLE `footer_about_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `footer_policy_pdf`
--
ALTER TABLE `footer_policy_pdf`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `footer_scheme`
--
ALTER TABLE `footer_scheme`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `footer_scheme_name`
--
ALTER TABLE `footer_scheme_name`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `forest_detail`
--
ALTER TABLE `forest_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frontend_protectedarea_notification`
--
ALTER TABLE `frontend_protectedarea_notification`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `frontend_protectedwetland_notification`
--
ALTER TABLE `frontend_protectedwetland_notification`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `frontend_protected_area`
--
ALTER TABLE `frontend_protected_area`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `frontend_protected_area_images`
--
ALTER TABLE `frontend_protected_area_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `frontend_protected_wetland_detail`
--
ALTER TABLE `frontend_protected_wetland_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `frontend_wetland_images`
--
ALTER TABLE `frontend_wetland_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `frontend_zoo_detail`
--
ALTER TABLE `frontend_zoo_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gallery_all_images`
--
ALTER TABLE `gallery_all_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `home_geographic_zone`
--
ALTER TABLE `home_geographic_zone`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `home_page_detail`
--
ALTER TABLE `home_page_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `how_apply_type`
--
ALTER TABLE `how_apply_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `how_to_apply_detail`
--
ALTER TABLE `how_to_apply_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `imp_link_detail`
--
ALTER TABLE `imp_link_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `imp_link_links`
--
ALTER TABLE `imp_link_links`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `landing_banner_images_text`
--
ALTER TABLE `landing_banner_images_text`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `management_nangal`
--
ALTER TABLE `management_nangal`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `management_nangal_pdf`
--
ALTER TABLE `management_nangal_pdf`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `minister_detail`
--
ALTER TABLE `minister_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notification_banner_detail`
--
ALTER TABLE `notification_banner_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notification_detail`
--
ALTER TABLE `notification_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `notification_pdf_detail`
--
ALTER TABLE `notification_pdf_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organisation_banner_detail`
--
ALTER TABLE `organisation_banner_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `organisation_table_detail`
--
ALTER TABLE `organisation_table_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `organisation_tabs`
--
ALTER TABLE `organisation_tabs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `protectedarea_category`
--
ALTER TABLE `protectedarea_category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50011;

--
-- AUTO_INCREMENT for table `protected_area_subcategory`
--
ALTER TABLE `protected_area_subcategory`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10024;

--
-- AUTO_INCREMENT for table `protected_wetland`
--
ALTER TABLE `protected_wetland`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150006;

--
-- AUTO_INCREMENT for table `protected_wetland_images`
--
ALTER TABLE `protected_wetland_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `protected_wetland_noti_images`
--
ALTER TABLE `protected_wetland_noti_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services_detail`
--
ALTER TABLE `services_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services_how_to_apply`
--
ALTER TABLE `services_how_to_apply`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tender_banner_detail`
--
ALTER TABLE `tender_banner_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tender_details`
--
ALTER TABLE `tender_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tender_types`
--
ALTER TABLE `tender_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tourism_banner_detail`
--
ALTER TABLE `tourism_banner_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tourism_data`
--
ALTER TABLE `tourism_data`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tourism_link_detail`
--
ALTER TABLE `tourism_link_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tourism_link_images`
--
ALTER TABLE `tourism_link_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tourism_types`
--
ALTER TABLE `tourism_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `upcoming_event_detail`
--
ALTER TABLE `upcoming_event_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6150;

--
-- AUTO_INCREMENT for table `volunteer_detail`
--
ALTER TABLE `volunteer_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wetland_authority`
--
ALTER TABLE `wetland_authority`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wetland_notification`
--
ALTER TABLE `wetland_notification`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wetland_rules`
--
ALTER TABLE `wetland_rules`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `whatsnew_detail`
--
ALTER TABLE `whatsnew_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wildlife_symbol`
--
ALTER TABLE `wildlife_symbol`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wildlife_symbol_title_image`
--
ALTER TABLE `wildlife_symbol_title_image`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `world_day`
--
ALTER TABLE `world_day`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `world_day_detail`
--
ALTER TABLE `world_day_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `world_day_details`
--
ALTER TABLE `world_day_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `world_day_images_detail`
--
ALTER TABLE `world_day_images_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `world_day_images_honour`
--
ALTER TABLE `world_day_images_honour`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `zoo_detail`
--
ALTER TABLE `zoo_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `zoo_gallery`
--
ALTER TABLE `zoo_gallery`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `zoo_gallery_banner`
--
ALTER TABLE `zoo_gallery_banner`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `zoo_inventory`
--
ALTER TABLE `zoo_inventory`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `zoo_type`
--
ALTER TABLE `zoo_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
