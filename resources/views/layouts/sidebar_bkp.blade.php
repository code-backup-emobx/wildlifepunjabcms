 <div style="padding-top: 2%">
 <div class="page-sidebar-wrapper">
 <!-- BEGIN SIDEBAR -->
                   
            <div class="page-sidebar navbar-collapse collapse">
                
                <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                    <li class="sidebar-toggler-wrapper hide">
                        <div class="sidebar-toggler">
                        </div>
                    </li>
                    <div class="clearfix" style="padding-top: 10%;"></div>
                    <li class="nav-item start active open">
                        <a href="/home" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">Dashboard</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>
                    <li class="heading">
                        <h3 class="uppercase">Features</h3>
                    </li>
                   
                    <li class="nav-item">
                        <a href="/banners_list" class="nav-link nav-toggle">
                            <i class="fa fa-list"></i>
                            <span class="title">Banners List</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">World Day</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/world_day_list" class="nav-link nav-toggle">
                                    <!-- <i class="fas fa-archway"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/worldDayDetail" class="nav-link nav-toggle">
                                   <!--  <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">World Day Detail</span>
                                </a>
                            </li>
<!-- 
                            <li class="nav-item">
                                <a href="/worldDayDetail" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Forest Day Detail</span>
                                </a>
                            </li> -->

                        </ul>
                    </li>

                    <!-- <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Minister Data</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/list_minister" class="nav-link nav-toggle">
                                   <i class="fa fa-tree" aria-hidden="true"></i>  -->
                                    <!-- <span class="title">List</span>
                                </a>
                            </li>

                        </ul>
                    </li>  -->

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Zoo Detail</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">

                            <li class="nav-item">
                                <a href="/zoo-master-list" class="nav-link nav-toggle">
                                   <!--  <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Master List</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="/zoo_list" class="nav-link nav-toggle">
                                   <!--  <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/gallery_banner" class="nav-link nav-toggle">
                                   <!--  <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Gallery Banner</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/gallery_list" class="nav-link nav-toggle">
                                   <!--  <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Gallery List</span>
                                </a>
                            </li>
                         
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Upcoming Events</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                           <!--  <li class="nav-item">
                                <a href="/event_list" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Event List</span>
                                </a>
                            </li> -->

                            <li class="nav-item">
                                <a href="/event_image" class="nav-link nav-toggle">
                               <!--  <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>
                        </ul>
                    </li> 

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Volunteer</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                           <!--   <li class="nav-item">
                                <a href="/v_image" class="nav-link nav-toggle">
                                    <i class="fa fa-universal-access"></i>
                                    <span class="title">Volunteer Image</span>
                                </a>
                            </li> -->

                            <li class="nav-item">
                                <a href="/volunteerdetail" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Volunteer Detail</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="/home_detail" class="nav-link nav-toggle">
                            <i class="fa fa-home"></i>
                            <span class="title">Home</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/symbol_of_punjab" class="nav-link nav-toggle">
                            <i class=""></i>
                            <span class="title">Wildlife Symbol Of punjab</span>
                        </a>
                    </li>

                     <li class="nav-item">
                        <a href="/videos_list" class="nav-link nav-toggle">
                            <i class="fa fa-video-camera"></i>
                            <span class="title">Videos List</span>
                        </a>
                    </li>

                    <li class="heading">
                        <h3 class="uppercase">Protected Area</h3>
                    </li>
                     <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Wildlife Santuaries</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="/bir_motibagh_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Moti Bagh List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/bir_gurdiaplur_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Gurdiaplur List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/bir_bhunerheri_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Bhunerheri List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/bir_mehas_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Mehas List</span>
                                </a>
                            </li>
                     
                            <li class="nav-item">
                                <a href="/bir_dosanjh_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Dosanjh List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/bir_bhadson_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Bhadson List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/bir_aishwan_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Aishwan List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/bir_abohar_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Bir Abohar List</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="/bir_harike_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Harike List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/takhni_rehmpur_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Takhni Rehmapur List</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="/jhajjarbachauli_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title"> Jhajjar-Bachauli Wildlife Sanctuary List</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="/kathlaur_kushlian_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Kathlaur Kushlian List</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="/nangal_wildlife_pa" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Nangal Wildlife List</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                  <!--   <li class="heading">
                        <h3 class="uppercase">Community Reserve</h3>
                    </li> -->
                     <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Community Reserve</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/panniwalalist" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Panniwala List </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/lalwanlist" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Lalwan Community List </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/keshpurmianilist" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Keshopur Miani Community List </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/siswanlist" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Siswan Community List </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                   
                     <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Conservation Reserve</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/rakh_sarai" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Rakh Sarai List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/cr_roparlist" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Ropar Wetland List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/cr_ranjitsagar" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Rangit Sagar Dam List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/cr_beasriver" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Beas River List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/cr_kalibean" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Kali Bein List</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Protected Wetland</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">

                            <li class="nav-item">
                                <a href="/wetland-master-list" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Master List</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/category-list" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>
                            
                          <!--   <li class="nav-item">
                                <a href="/harike_wildlife" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Harike Wildlife</span>
                                </a>
                            </li> -->

                          <!--   <li class="nav-item">
                                <a href="/nangal_wildlife_santuaries" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Nangal Wildlife Santuaries</span>
                                </a>
                            </li> -->

                           <!--  <li class="nav-item">
                                <a href="/keshopur_santuaries" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Keshopur Community Reserve</span>
                                </a>
                            </li> -->

                        </ul>
                    </li>

                     <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Tourism</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/tourism_detail" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Tourism Detail</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Zoo</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">

                             <li class="nav-item">
                                <a href="/zoo-list" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>
                            
                            <!-- <li class="nav-item">
                                <a href="/chattbir_zoo_list" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Chhatbir Zoo</span>
                                </a>
                            </li> -->

                          <!--   <li class="nav-item">
                                <a href="/ludhiana_zoo_list" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Ludhiana Zoo</span>
                                </a>
                            </li> -->

                           <!--  <li class="nav-item">
                                <a href="/patiala_zoo_list" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Mini Zoo,Patiala</span>
                                </a>
                            </li> -->

                          <!--   <li class="nav-item">
                                <a href="/bathinda_zoo_list" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Mini Zoo,Bathinda</span>
                                </a>
                            </li> -->

                          <!--   <li class="nav-item">
                                <a href="/deerpark_zoo_list" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Deer Park,Neelon</span>
                                </a>
                            </li> -->

                        </ul>
                    </li>

                     <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Quick Links</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/act_and_rule" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Act&Rules</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/notification_detail" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Notification</span>
                                </a>
                            </li>

                            
                           <!--  <li class="nav-item">
                                <a href="/contact_us" class="nav-link nav-toggle">
                                    <i class="fa fa-tree" aria-hidden="true"></i>
                                    <span class="title">Contact Us</span>
                                </a>
                            </li> -->

                        </ul>
                    </li>

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Usefull Links</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/scheme_detail" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Scheme</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/about_us_detail" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">About-Us</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/policy_guideline" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Policy Guideline</span>
                                </a>
                            </li>

                        </ul>
                    </li>
               
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Living Here</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/tender_detail" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Tender</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/citizen_character" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">Citizen charter</span>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">Organisation Chart</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/organisation_master" class="nav-link nav-toggle">
                                    <i class="fa fa-list" aria-hidden="true"></i>
                                    <span class="title">Organisation Master</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="/organisation_list" class="nav-link nav-toggle">
                                    <i class="fa fa-list" aria-hidden="true"></i>
                                    <span class="title">Organisation List</span>
                                </a>
                            </li>


                        </ul>
                    </li>

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">IMP LINK</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/imp_list" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Services</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/services_list" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Wetland Authority Matter</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/wetland_list" class="nav-link nav-toggle">
                                    <!-- <i class="fa fa-tree" aria-hidden="true"></i> -->
                                    <span class="title">List</span>
                                </a>
                            </li>

                        </ul>
                    </li>


                </ul>
                <!-- END SIDEBAR MENU -->
                <!-- END SIDEBAR MENU -->
            </div>
</div>
       