@extends('layouts.master')
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                            <span class="caption-subject">Update Event Forest Day</span>
                        </div>
                        <div class="actions">
                          	<a href="{{ url('/event_forestday') }}">
                          		<button type="button" class="btn btn-light-primary btn-sm"> Event Forest Day List</button>
                			
            				</a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div>

                        @if(Session::has('success'))

                        <div class="alert alert-success">

                            {{ Session::get('success') }}

                            @php

                            Session::forget('success');

                            @endphp

                        </div>

                        @endif
                        <form action="{{ url('/update_event/'.$editforestday->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                            <div class="form-body" style="padding-left: 5%">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Choose World Day: </label>
                                    <div class="col-lg-6">
                                      <select class="form-control" name="world_id">
                                          
                                          <option value="">--Select--</option>
                                          @foreach($worldDay_list as $list)
                                            <option value="{{ $list->id }}">{{ $list->world_day_type }}</option>
                                          @endforeach  
                                      </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body" style="padding-left: 5%">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Upload Event Image: </label>
                                    <div class="col-lg-6">
                                        <input type="file" class="form-control" name="image" >
                                        <div>
                                            <img src="{{ url($editforestday->image)}}" style="width: 19%;height: 50%;margin-top: 4%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                         	<div class="form-body" style="padding-left: 5%">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Event Heading: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="Enter Event  Heading" name="heading" value="{{ $editforestday->heading }}">
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-body" style="padding-left: 5%">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Event Date: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="Enter Event Date" name="date" value="{{ $editforestday->date }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                     <div class="col-lg-6">
                                        <input type="submit" class="btn btn-success" value="Submit">
                                        <a onclick="history.go(-1)" class="btn">Cancel</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
           
@endsection

