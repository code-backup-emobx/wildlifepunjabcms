@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>World Day</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/worldDayDetail') }}">World Days Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/honour-list/'.$d_id.'/'.$type_id) }}">World Day Honour List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update-honour-detail/'.$id.'/'.$d_id.'/'.$type_id) }}">Update Honour</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Honour Detail </span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/honour-list/'.$d_id.'/'.$type_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> World Day Honour List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="{{ url('/update-honour-detail/'.$edit_honour->id.'/'.$d_id.'/'.$type_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}


                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Honour Name: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="guest_honour_name" value="{{ $edit_honour->guest_honour_name }}" >
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Honour Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="guest_honour_title" value="{{ $edit_honour->guest_honour_title }}" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Honour Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="guest_of_honour" value="{{ old('	guest_of_honour') }}" accept="image/*">
                                            <img src="{{ $edit_honour->	guest_of_honour}}" style="width:20%;">
                                        </div>
                                    </div>
                                </div>

                               
                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
	
@endpush
