@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>World Day</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/world_day_list') }}">World Master List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update_worldday/'.$id) }}">Update World Day</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update World Detail </span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/world_day_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> World Detail List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                          
                            <form action="{{ url('/update_worldday/'.$edit_world->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Name: </label>
                                        <div class="col-lg-6">
                                           <input tpe="text" name="type_name" class="form-control" value="{{ $edit_world->type_name }}" placeholder="Enter World Day Name">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="image" value="">
                                            <div>
                                            	<img src="{{ $edit_world->image }}" style="width:89px;height:65px;background-color:#e6e6e6;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Event Date: </label>
                                        <div class="col-lg-6">
                                            <input class="form-control" type="date" value="{{ $edit_world->date_of_day }}" id="example-date-input" name="event_date"/>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Description: </label>
                                        <div class="col-lg-6">
                                          <textarea class="form-control" name="description">{{ $edit_world->description }}</textarea>
                                        </div>
                                    </div>
                                </div> -->
                            	

                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

