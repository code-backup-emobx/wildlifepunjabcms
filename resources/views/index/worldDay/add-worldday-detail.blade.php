@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
               <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>World Day</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/worldDayDetail') }}">World Days Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add-worldDay-detail') }}">Add World Day Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add World Day Detail </span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/worldDayDetail') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> World Detail List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="{{ url('/add-worldDay-detail') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Choose World Day: </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="world_day_type_id">
                                           	  <option value="">--Select--</option>
                                           	  @foreach($worldDay_list as $list)
                                           	  <option value="{{ $list->id }}">{{ $list->type_name	 }}</option>
                                           	  @endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="{{ old('banner_image') }}" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
									<div class="col-lg-12">
										<label>Description:</label>
										<textarea name="description" id="editor1">{{ old('description') }}</textarea>
									</div>
								</div>
                                </div>

                                <div style="background-color: #E5EFF1;padding:1%;">
                                    <h3>Forest Day Images</h3>
	                                <div class="card-body" id="images" >
										<div class="form-group row">
											<div class="col-lg-6">
												<label>Image:</label>
												<input type="file" class="form-control" name="image[]"/ accept="image/*">
											</div>
											<div class="col-lg-6">
												<label>Image:</label>
												<input type="file" class="form-control" name="image[]"/ accept="image/*">
											</div>
										</div>
									</div>

									<div id="show_div">
									</div>
								</div>	

                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" value="Add More" style="float:right;">Add More Images</button>

                                </div>
                                <hr>

                                 <div style="background-color:#eafaea;padding:1%;">
                                    <h3>Guest of Honour</h3>
                                    <div class="card-body" id="images_guest" >

                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <label>Name of Guest Honour:</label>
                                                <input type="text" class="form-control" placeholder="Enter Guest Honour" name="guest_honour_name[]"/>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Honour Title:</label>
                                                <input type="text" class="form-control" placeholder="Enter Honour Title" name="guest_honour_title[]"/>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Guest of Honour Image:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                    <input type="file" class="form-control" name="honour_image[]" accept="image/*"/>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div id="show_div_images">
                                    </div>
                                </div>  
								
								
                            	
                            	<div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more_guest" value="Add More" style="float:right;">Add More Guest Images</button>

                                </div>

                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
	<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor1');
</script>

<script>

   $( document ).ready(function() {
    $("#add_more").click(function(){
    		// alert('hello');
    		// $("#images").show();
      var html = $( "#images" ).html();
       $("#show_div").append(html);
  });
});
</script>

<script>
   $( document ).ready(function() {
    $("#add_more_guest").click(function(){
             // alert('hello');
            // $("#images").show();
      var html = $( "#images_guest" ).html();
       $("#show_div_images").append(html);
  });
});
</script>
@endpush
