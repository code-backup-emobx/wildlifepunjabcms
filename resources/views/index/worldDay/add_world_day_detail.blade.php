@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add World Day Detail </span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/world_day_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> World Detail List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="{{ url('/worldDayDetail') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Choose World Day: </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="world_day_type_id">
                                           	  <option value="">--Select--</option>
                                           	  @foreach($worldDay_list as $list)
                                           	  <option value="{{ $list->id }}">{{ $list->world_day_type	 }}</option>
                                           	  @endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="" required placeholder="Enter The Heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Date: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_date" value="" required placeholder="Enter The Date">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">World Day Description: </label>
                                        <div class="col-lg-6">
                                          <textarea class="form-control" name="description"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div style="background-color: #E5EFF1;padding:1%;">
                                    <h3>Images</h3>
	                                <div class="card-body" id="images" >
										<div class="form-group row">
											<div class="col-lg-6">
												<label>Image:</label>
												<input type="file" class="form-control" name="image[]"/>
											</div>
											<div class="col-lg-6">
												<label>Image:</label>
												<input type="file" class="form-control" name="image[]"/>
											</div>
										</div>
									</div>

									<div id="show_div">
									</div>
								</div>	

                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" value="Add More" style="float:right;">Add More Images</button>

                                </div>
                                <hr>

                                 <div style="background-color:#eafaea;padding:1%;">
                                    <h3>Guest of Honour</h3>
                                    <div class="card-body" id="images_guest" >

                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <label>Name of Guest Honour:</label>
                                                <input type="text" class="form-control" placeholder="Enter Guest Honour" name="guest_honour_name[]"/>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Honour Title:</label>
                                                <input type="text" class="form-control" placeholder="Enter Honour Title" name="guest_honour_title[]"/>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Guest of Honour Image:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                    <input type="file" class="form-control" 
                                                        name="guest_of_honour[]"/>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div id="show_div_images">
                                    </div>
                                </div>  
								
								
                            	
                            	<div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more_guest" value="Add More" style="float:right;">Add More Guest Images</button>

                                </div>

                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
    		// alert('hello');
    		// $("#images").show();
      var html = $( "#images" ).html();
       $("#show_div").append(html);
  });
});
</script>

<script>
   $( document ).ready(function() {
    $("#add_more_guest").click(function(){
             // alert('hello');
            // $("#images").show();
      var html = $( "#images_guest" ).html();
       $("#show_div_images").append(html);
  });
});
</script>
@endpush
