@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>World Day</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/worldDayDetail') }}">World Days Detail List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">World Days Detail List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-worldDay-detail') }}">
                                    <button type="button" class="btn btn-sm">Add World Day Detail
                                    <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Id </th>
                                                        <th>World Day Name </th>
                                                        <th>Banner Image </th>
                                                        <th>Description </th>
                                                        <th>World Day Date</th>
                                                        <th>Total Images </th>
                                                        <th>Guest Of Honour </th>
                                                        <th colspan="2"> Action </th>
                                                    </tr>
                                                </thead>
                                               
                                                <tbody>
                                                  @foreach($world_day_list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->  worldday_type_name }} </td>
                                                       <td> <img src="{{ $listing->banner_image }}" style="width:89px;height:65px;background-color:#e6e6e6;" /> </td>
                                                        <td> {!! $listing->description !!} </td>
                                                     
                                                        <td> {{ $listing->worldday_date}}</td>

                                                        <td> <a href="{{ url('images-list/'.$listing->id.'/'.$listing-> worldday_type_id)}}" class="btn btn-inline btn-success font-weight-bold"> {{ $listing->images_count }} </a> </td>
                                                        <td> <a href="{{ url('honour-list/'.$listing->id.'/'.$listing-> worldday_type_id)}}" class="btn btn-inline btn-success font-weight-bold"> {{ $listing->honour_count }} </a> </td>
                                                        <td>
                                                            <a href="{{ url('edit-worldday-detail/'.$listing->id) }}" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></a>
                                                        </td><td>
                                                            <a href="{{ url('delete-worldday-detail/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                   <!--  -->
                     {{ $world_day_list->links('vendor.pagination.custom') }}
                 
                </div>
            </div>
       </div>
   </div>
           
@endsection

