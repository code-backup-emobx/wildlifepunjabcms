@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>World Day</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/worldDayDetail') }}">World Days Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/honour-list/'.$id.'/'.$type_id) }}">World Day Honour List</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">World Day Honour List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-more-honour/'.$id.'/'.$type_id) }}">
                                    <button type="button" class="btn btn-sm">Add More Honour Detail
                                    <i class="fa fa-plus"></i></button>
                                </a>
                                <a href="{{ url('/worldDayDetail') }}">
                                    <button type="button" class="btn btn-sm">World Days List
                                    <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                          
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th>id </th>
                                                        <th>Honour Name </th>
                                                        <th>Honour Title </th>
                                                        <th>Honour Image </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                   @foreach($honour_list as $key => $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->guest_honour_name }} </td>
                                                        <td> {{ $listing->guest_honour_title }} </td>
                                                        
                                                       <td> <img src="{{ $listing->	guest_of_honour }}" style="width:89px;height:65px;background-color:#e6e6e6;" /> </td>
                                                       
                                                        <td>
                                                            <a href="{{ url('update-honour-detail/'.$listing->id.'/'.$id.'/'.$type_id )}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete-honour-detail/'.$listing->id.'/'.$id ) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                   @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                   <!--  -->
                    
                    {{ $honour_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

