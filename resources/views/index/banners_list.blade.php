  
  @extends('layouts.master')
  @section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Banners</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/banners_list') }}">Landing Screen Banner Images</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Landing Screen Banner Images</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/add_banner') }}">
                              		<button type="button" class="btn btn-sm">Add Banner Images<i class="fa fa-plus"></i></button>
                				</a>

                                <!-- <a href="{{ url('/add_banner_two') }}">
                                    <button type="button" class="btn btn-sm">Add Banner Slide 2<i class="fa fa-plus"></i></button>
                                </a> -->
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          
                               
                            	<form action="save_text_banner" class="form-horizontal" method="post">
                                {{ csrf_field() }}
                            	<div>
                                <textarea rows="4" cols="4" name="title" class="form-control">@if(!empty($get_data->title)){{ $get_data->title }} @else @endif</textarea>&nbsp;&nbsp;
                                <textarea rows="4" cols="4" name="sub_title" class="form-control">@if(!empty($get_data->subtitle)) {{ $get_data->subtitle }} @else @endif</textarea>
                                <input type="hidden" value="@if(!empty($get_data->title)){{ $get_data->id}}@else @endif" name="check_id">
                            	</div>&nbsp;&nbsp;
                               <button type="submit" class="btn btn-sm" style="background-color:#1BC5BD;color:white;margin-top:16px;">Save</button>
                            	</form>
                            
                                <div class="form-body">
										<div class="table-responsive">
										    <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
										    	<thead>
                                                    <tr>
                                                        <th>  Id </th>
                                                        <th> Image</th>
<!--                                                         <th> Banner Heading</th> -->
<!--                                                         <th> Banner Description</th> -->
<!--                                                         <th> Banner Slide</th> -->
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                	$count = 1;
                                                @endphp
                                                <tbody>
                                                	@foreach($banner_list as $banner_listing)
                                                	<tr>
                                                		<td> {{ $count++ }} </td>
                                                		<td> <img src="{{ $banner_listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                		
<!--                                                         <td>{{ $banner_listing->banner_heading }} </td> -->
                                                        
<!--                                                     <td> {{ $banner_listing->banner_description }} </td> -->
<!--                                                     <td> {{ $banner_listing->banner_slide }} </td> -->
                                                        <td>
                                                            <a href="{{ url('update_banner/'.$banner_listing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_banner/'.$banner_listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                	</tr>
                                                	@endforeach	
                                                </tbody>
										    </table>
										</div>
                                </div>
                           
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $banner_list->links('vendor.pagination.custom') }}
                   
                </div>
            </div>
       </div>
   </div>
           
@endsection

