  
  @extends('layouts.master')
  @section('content')

        <div class="page-content-wrapper">
            <div class="page-content">

                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Banners</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/banners_list') }}">Banner List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_banner') }}">Add Banner Slide</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5"> 
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Banner Slide</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/banners_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Banners List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="{{ url('/add_banner') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="{{ old('banner_image') }}" accept="image/*">
                                        </div>
                                    </div>
                                </div>

<!--                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Choose Banner Slide: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control banner_slide" name="banner_slide">
                                                <option value="">--Select--</option>
                                                <option value="slide_1">Slide 1</option>
                                                <option value="slide_2">Slide 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->

<!--                                 <div class="form-body b_heading" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" id="banner_heading" value="{{ old('banner_heading') }}">
                                        </div>
                                    </div>
                                </div> -->

<!--                                 <div class="form-body b_image" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Description: </label>
                                        <div class="col-lg-6">
                                            <textarea class="form-control" name="banner_description" id="banner_description">{{ old('banner_description') }}</textarea>
                                        </div>
                                    </div>
                                </div> -->
                             
                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
    $(".banner_slide").change(function () {
            var selectedValue = $(this).val();
            // alert(selectedValue);
            if(selectedValue == "slide_1"){
                $(".b_heading").show();
                $(".b_image").show();
            }
            else{
                $(".b_heading").hide();
                $(".b_image").hide();
                $('#banner_heading').val(''); 
                $('#banner_description').val(''); 
            }
        });
</script>
@endpush


