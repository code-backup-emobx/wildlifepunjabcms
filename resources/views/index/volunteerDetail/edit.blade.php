@extends('layouts.master')
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

              <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Volunteer</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/volunteerdetail') }}">List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/update_volunteerdetail/'.$id) }}">Update Volunteer Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

            <div class="row" style="margin-top:2%;">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                    <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                        <div class="caption">
                            <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                            <span class="caption-subject">Update Event Background Image</span>
                        </div>
                        <div class="actions">
                          	<a href="{{ url('/volunteerdetail') }}">
                          		<button type="button" class="btn btn-light-primary btn-sm"> Event Background Image List<i class="fa fa-list"></i></button>
                			
            				</a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div>

                        @if(Session::has('success'))

                        <div class="alert alert-success">

                            {{ Session::get('success') }}

                            @php

                            Session::forget('success');

                            @endphp

                        </div>

                        @endif
                        <form action="{{ url('/update_volunteerdetail/'.$editvolunteerdetail->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                            
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Volunteer Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Volunteer Heading" name="volunteer_heading" value="{{ $editvolunteerdetail->volunteer_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Volunteer Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Volunteer Title" name="volunteer_title" value="{{  $editvolunteerdetail->volunteer_title }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Volunteer Description: </label>
                                        <div class="col-lg-6">
                                             <textarea class="form-control" 
                                             name="volunteer_description" rows="7" >{{ $editvolunteerdetail->volunteer_description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Volunteer Image One: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="volunter_image_one" value="">
                                       
                                        <div>
                                            <img src="{{ $editvolunteerdetail->volunter_image_one }}" style="width:20%;height:30%;">
                                        </div>
                                    </div>
                                     </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Volunteer Image Two: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" 
                                            name="volunter_image_two" value="">
                                        
                                        <div>
                                            <img src="{{ $editvolunteerdetail->volunter_image_two }}" style="width:20%;height:30%;">
                                        </div>
                                        </div>
                                    </div>
                                </div>
                       
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                     <div class="col-lg-6">
                                        <input type="submit" class="btn btn-success" value="Submit">
                                        <a onclick="history.go(-1)" class="btn">Cancel</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
           
@endsection

