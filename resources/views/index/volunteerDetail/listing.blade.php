@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Volunteer</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/volunteerdetail') }}">List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Volunteer Detail List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_volunteerdetail') }}">
                                    <button type="button" class="btn btn-sm">Add Volunteer Detail
                                    <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Id </th>
                                                        <th>Volunteer Detail Heading </th>
                                                        <th>Volunteer Detail Title </th>
                                                        <th>Volunteer Detail Description </th>
                                                        <th>Volunteer Image One</th>
                                                        <th>Volunteer Image Two</th>
                                                        <th colspan="2"> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                    @foreach($volunteer_detail as $volunteerdetailListing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> {{ $volunteerdetailListing->	volunteer_heading }} </td>
                                                        <td> {{ $volunteerdetailListing->		volunteer_title }} </td>
                                                        <td> {{ $volunteerdetailListing->			volunteer_description }} </td>
                                                        <td><img src="{{ $volunteerdetailListing->volunter_image_one }}" style="width:84%;"></td>
                                                         <td><img src="{{ $volunteerdetailListing->volunter_image_two }}" style="width:84%;"></td>
                                                        <td>
                                                            <a href="{{ url('update_volunteerdetail/'.$volunteerdetailListing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a></td><td>
                                                            <a href="{{ url('delete_volunteerdetail/'.$volunteerdetailListing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach 
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $volunteer_detail->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

