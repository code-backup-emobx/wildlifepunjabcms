@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Minister Detail </span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/list_minister') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Minister List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="{{ url('/add_ministerdetail') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Minister Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control"  name="minister_image" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Minister Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Minister Heading" name="minister_heading" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Minister Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Minister Title" name="minister_title" value="" required>
                                        </div>
                                    </div>
                                </div>
                            	
                                <div class="form-group row">
									<div class="col-lg-12">
                                        <label class="col-md-3 col-form-label">About Minister Description:(optional)</label>
                                        <textarea class="col-lg-6" name="description" class="form-control" rows="7"></textarea>
                                    </div>
								</div>

                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
    CKEDITOR.replace('editor1' );
</script>

@endpush