@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Minister Detail List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_ministerdetail') }}">
                                    <button type="button" class="btn btn-sm">Add Minister Detail
                                    <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Minister Id </th>
                                                        <th>Minister Image </th>
                                                        <th>Minister Heading </th>
                                                        <th>Minister Title </th>
                                                        <th>Minister Description </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                    @foreach($minister_detail as $listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $listing->minister_image }}" style="width:84px;height:50px;"> </td>
                                                        <td> {{ $listing->minister_heading }} </td>
                                                        <td> {{ $listing->minister_title }} </td>
                                                        <td> {!! $listing->description !!} </td>
                                                         <td>
                                                            <a href="{{ url('update_minister/'.$listing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit" style="background-color:#1BC5BD;color:white;"></i></button></a>
                                                            <a href="{{ url('delete_minister/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                   @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $minister_detail->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

