@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Zoo Management</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/gallery_list') }}">Zoo Gallery List</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                                <span class="caption-subject">Zoo Gallery List</span>
                                <div class="actions">
                                <a href="{{ url('/add_zooGallery') }}">
                                    <button type="button" class="btn btn-sm">Add Zoo Gallery
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                           
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                               
                              <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Sr.No </th>
                                                        <th> Zoo Name</th>
                                                        <th> Total Images</th>
                                                        <!-- <th> Action </th> -->
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                  @foreach($zoo_type as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td>{{ $listing->zoo_type_name }}</td>
                                                      
                                                        <td><a href="{{ url('images_detail/'.$listing->id) }}" class="btn btn-inline btn-success font-weight-bold">{{ $listing->total_count }} </a></td>
                                                       
                                                      <!--   <td>
                                                            <a href="{{ url('update_gallery_banner/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_gallery_banner/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td> -->
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            <!-- *********** -->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

