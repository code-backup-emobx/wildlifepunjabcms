@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Zoo</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/zoo-list') }}">Zoo Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit-zoo-detail/'.$id) }}">Update Zoo Detail</a>
                           
                        </li>
                    </ul>
                </div>
                
                @if(Session::has('success'))

                    <div class="alert alert-success">

                        {{ Session::get('success') }}

                        @php

                        Session::forget('success');

                        @endphp

                    </div>

                @endif

                  
                <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Zoo Detail </span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/zoo-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm"> Zoo Detail List<i class="fa fa-list"></i></button>
                                
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                            <form action="{{ url('/edit-zoo-detail/'.$edit_zoo->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Select Zoo: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="zoo_type">   
                                                <option>--Select--</option>
                                                @foreach($zoo_list as $list)
                                                    <option value="{{ $list->id }}" @if($edit_zoo->type_id == $list->id) selected @endif>{{ $list->zoo_type_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Zoo Banner: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="{{ old('banner_image') }}" accept="image/*">
                                            <img src="{{ $edit_zoo->banner_image}}" style="width: 20%;margin-top: 2%;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Zoo Website Link: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="website_link" value="{{ $edit_zoo->website_link  }}" placeholder="Enter Website Link eg:http://chhatbirzoo.gov.in/" data-toggle="tooltip" data-placement="top" title="Website Link eg:http://chhatbirzoo.gov.in/">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label>Description: </label>
                                           <textarea class="form-control" name="description" id="editor">{{ $edit_zoo->description  }}</textarea>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ $edit_zoo->location  }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full Access" name="access" value="{{ $edit_zoo->access  }}"/>
                                    </div>
                                 
                                </div> 

                                <div class="form-group row">

                                    <div class="col-lg-5">
                                        <label>Visiting Days:</label>
                                        <input type="text" class="form-control" placeholder="Enter Visiting Days" name="visiting_days" value="{{ $edit_zoo->visiting_days }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Zoo Hours:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full Zoo Hours" name="zoo_hours" value="{{ $edit_zoo->zoo_hours }}"/>
                                    </div>
                                 
                                </div>

                                  <div class="form-group">

                                    <div class="col-lg-5">
                                        <label>Zoo Holidays:</label>
                                        <input type="text" class="form-control" placeholder="Enter Zoo Holidays" name="zoo_holidays" value="{{ $edit_zoo->zoo_holidays }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Special Attractions:</label>
                                        <input type="text" class="form-control" placeholder="Enter Special Attractions" name="special_attraction" value="{{ $edit_zoo->special_attractions }}"/>
                                    </div>
                                 
                                </div> 

                                <div class="form-group">

                                    <div class="col-lg-5">
                                        <label>Zoo Map Latitute:</label>
                                        <input type="text" class="form-control" placeholder="Enter Map Latitute" name=" latitude" value="{{ $edit_zoo->latitude }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Zoo Map Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Map Longitude" name="longitude" value="{{ $edit_zoo->longitude }}"/>
                                    </div>
                                 
                                </div>   
                                
                             <div class="form-group">
                                    <div class="col-lg-5">
                                        <label>Map Zoom Size:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ $edit_zoo->zoom_level }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>
                                    </div>
                                  
                                </div>
                                
                               <div class="form-body">
                                    <div class="form-group row">
                                        <label>Zoo Residents: </label>
                                           <textarea class="form-control" name="zoo_residence" id="editor1">{{ $edit_zoo->zoo_residence }}</textarea>
                                       
                                    </div>
                                </div>

                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
</script>

@endpush
