@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                

                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Zoo</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/zoo-list') }}">Zoo List</a>
                             <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/add-zoo-detail') }}">Add Zoo</a>
                        </li>
                    </ul>
                </div>
                 @if(Session::has('success'))

                    <div class="alert alert-success">

                        {{ Session::get('success') }}

                        @php

                        Session::forget('success');

                        @endphp

                    </div>

                @endif
                <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Zoo Detail </span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/zoo-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm"> Zoo List<i class="fa fa-list"></i></button>
                                
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="{{ url('/add-zoo-detail') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Select Zoo: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="zoo_type">   
                                                <option>--Select--</option>
                                                @foreach($zoo_list as $list)
                                                    <option value="{{ $list->id }}">{{ $list->zoo_type_name }}</option>
                                                @endforeach
                                              
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Zoo Banner: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="{{ old('banner_image') }}" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Zoo Website Link: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="website_link" value="{{ old('website_link') }}" placeholder="Enter Website Link eg:http://chhatbirzoo.gov.in/" data-toggle="tooltip" data-placement="top" title="Website Link eg:http://chhatbirzoo.gov.in/">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label>Description: </label>
                                           <textarea class="form-control" name="description" id="editor">{{ old('description') }}</textarea>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ old('location') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full Access" name="access" value="{{ old('access') }}"/>
                                    </div>
                                 
                                </div> 

                                <div class="form-group row">

                                    <div class="col-lg-5">
                                        <label>Visiting Days:</label>
                                        <input type="text" class="form-control" placeholder="Enter Visiting Days" name="visiting_days" value="{{ old('access') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Zoo Hours:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full Zoo Hours eg:9.00 AM to 5.00PM" name="zoo_hours" value="{{ old('zoo_hours') }}"/>
                                    </div>
                                 
                                </div>

                                  <div class="form-group">

                                    <div class="col-lg-5">
                                        <label>Zoo Holidays:</label>
                                        <input type="text" class="form-control" placeholder="Enter Zoo Holidays" name="zoo_holidays" value="{{ old('zoo_holidays') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Special Attractions:</label>
                                        <input type="text" class="form-control" placeholder="Enter Special Attractions" name="special_attraction" value="{{ old('special_attraction') }}"/>
                                    </div>
                                 
                                </div> 

                                <div class="form-group">

                                    <div class="col-lg-5">
                                        <label>Zoo Map Latitute:</label>
                                        <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name=" latitude" value="{{ old('latitude') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Zoo Map Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Map Longitude eg: 75.8204768" name="longitude" value="{{ old('longitude') }}"
                                      data-toggle="tooltip" data-placement="top" title="longitude for eg: /@30.9590687,75.8204768,16z/...just enter the after , number 75.8204768"/>
                                    </div>
                                 
                                </div>  
                            
                            	 <div class="form-group">
                                    <div class="col-lg-5">
                                        <label>Map Zoom Size:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ old('zoom_level') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>
                                    </div>
                                  
                                </div>
                                
                                
                               <div class="form-body">
                                    <div class="form-group row">
                                        <label>Zoo Residents: </label>
                                           <textarea class="form-control" name="zoo_residence" id="editor1">{{ old('zoo_residence') }}</textarea>
                                       
                                    </div>
                                </div>


                                 <div class="sub_heading_management" style="background-color: #E5EFF1; margin-bottom:20px;">
                                    <div><p style="padding:1%;color:black;">Add 
                                    Inventory</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="sub_heading_management">
                                        <div class="form-group row" >
                                            <label class="col-md-2 col-form-label">Name Of Animal: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter name of animal" name="animal_name[]" value="{{old('animal_name[]')}}">
                                            </div>

                                            <label class="col-md-1 col-form-label">Quantity: </label>
                                            <div class="col-lg-4">
                                                <input type="number" class="form-control"  name="quantity[]" value="{{old('quantity[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_sub_heading_management" style="padding-left: 2%;margin-bottom:5%;">
                                    </div>
                                </div>  

                                <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more" value="Add More" style="margin-left:86%;"><b>+</b> Add Inventory</button>
                                </div>&nbsp;
                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
</script>
<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>

@endpush
