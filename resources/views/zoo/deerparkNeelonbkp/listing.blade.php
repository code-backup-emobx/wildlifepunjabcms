@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">DeerParkNeelon Zoo Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_deerparkneelonzoo') }}">
                                    <button type="button" class="btn btn-sm">Add DeerParkNeelon Zoo
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> DeerPark Neelon Id </th>
                                                        <th> DeerPark Neelon Banner Image</th>
                                                        <th> DeerPark Neelon Banner Heading </th>
                                                        <th> DeerPark Neelon Heading </th>
                                                        <th> DeerPark Neelon Description</th>
                                                        <th> DeerPark Neelon Location</th>
                                                        <th> DeerPark Neelon Access</th>
                                                        <th> DeerPark Neelon Visting Day</th>
                                                        <th> DeerPark Neelon Zoo Hours</th>
                                                        <th> DeerPark Neelon Zoo Holidays </th>
                                                        <th> DeerPark Neelon Special Attraction </th>
                                                        <th> DeerPark Neelon Zoo Residence </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                   	@foreach($deerpark_zoo_list as $listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $listing->banner_heading }} </td>
                                                        <td> {{ $listing->heading }} </td>
                                                        <td> {{ $listing->description }} </td>
                                                        <td> {{ $listing->location }} </td>
                                                        <td> {{ $listing->access }} </td>
                                                        <td> {{ $listing->visiting_days }} </td>
                                                        <td> {{ $listing->zoo_hours }} </td>
                                                        <td> {{ $listing->zoo_holidays }} </td>
                                                        <td> {{ $listing->special_attraction }} </td>
                                                        <td> {{ $listing->zoo_residence }} </td>
                                                      
                                                        <td>
                                                            <a href="{{ url('update_deerpark_zoo/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>

                                                            <a href="{{ url('delete_deerpark_zoo/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                 	@endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $deerpark_zoo_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

