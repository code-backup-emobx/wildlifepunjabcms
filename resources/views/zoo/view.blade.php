@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
              <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Zoo</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/zoo-list') }}">Zoo Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-zoo-detail/'.$id) }}">View Zoo Detail</a>
                           
                        </li>
                    </ul>
              </div>


               @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">View Zoo Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/zoo-list') }}">
                                    <button type="button" class="btn btn-sm">Zoo Detail List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                           
                                <div class="container row">
                                    <div class="col-md-12">
                                      <div><h5><b>Zoo Name:</b></h5>{{ $view->get_zoo_name->zoo_type_name }}</div>


                                      <h5><b>Banner image:</b></h5><p><img src="{{ $view->banner_image }}" style="width:20%;"></p>
                                      <h5><b>Description:</b></h5><div>{!! $view->description !!}</div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>website_link:</b></h4><p>{{ $view->website_link }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Visiting Days:</b></h4><p>{{ $view->visiting_days }}</p>
                                        </div>
                                      </div>
                                    </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Location:</b></h4><p>{{ $view->location }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Access:</b></h4><p>{{ $view->access }}</p>
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Zoo Hours:</b></h4><p>{{ $view->zoo_hours }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Zoo Holidays:</b></h4><p>{{ $view->zoo_holidays }}</p>
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Special Attractions:</b></h4><p>{{ $view->special_attractions  }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Zoo Residence:</b></h4><p>{{ $view->zoo_residence }}</p>
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Latitude:</b></h4><p>{{ $view->latitude }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Longitude:</b></h4><p>{{ $view->longitude }}</p>
                                        </div>
                                      </div>
                                     
                                     

                                      
                                </div>
                          
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

