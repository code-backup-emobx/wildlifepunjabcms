@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Zoo Management</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/zoo-master-list') }}">Zoo Master List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Zoo Master List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-new-zoo') }}">
                                    <button type="button" class="btn btn-sm">Add Zoo
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th>Zoo Name </th>
                                                        <th> Action </th>
                                                       
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                 
                                                 	@foreach($zoolist as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->zoo_type_name }} </td>
                                                        <td>
                                                           
                                                            <a href="{{url('update-zoo/'.$listing->id )}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
                                                            <a href="{{url('delete-zoo/'.$listing->id)}}" onclick="return confirm('If you delete the zoo master then their child data automatically deleted . Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    {{ $zoolist->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

