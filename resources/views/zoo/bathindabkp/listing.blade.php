@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Bathinda Zoo Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_bathindazoo') }}">
                                    <button type="button" class="btn btn-sm">Add Bathinda Zoo
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Bathinda Id </th>
                                                        <th> Bathinda Banner Image</th>
                                                        <th> Bathinda Banner Heading </th>
                                                        <th> Bathinda Heading </th>
                                                        <th> Bathinda Description</th>
                                                        <th> Bathinda Location</th>
                                                        <th> Bathinda Access</th>
                                                        <th> Bathinda Visting Day</th>
                                                        <th> Bathinda Zoo Hours</th>
                                                        <th> Bathinda Zoo Holidays </th>
                                                        <th> Bathinda Special Attraction </th>
                                                        <th> Bathinda Zoo Residence </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                   	@foreach($bathinda_zoo_list as $listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $listing->banner_heading }} </td>
                                                        <td> {{ $listing->heading }} </td>
                                                        <td> {{ $listing->description }} </td>
                                                        <td> {{ $listing->location }} </td>
                                                        <td> {{ $listing->access }} </td>
                                                        <td> {{ $listing->visiting_days }} </td>
                                                        <td> {{ $listing->zoo_hours }} </td>
                                                        <td> {{ $listing->zoo_holidays }} </td>
                                                        <td> {{ $listing->special_attraction }} </td>
                                                        <td> {{ $listing->zoo_residence }} </td>
                                                      
                                                        <td>
                                                            <a href="{{ url('update_bathinda_zoo/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>

                                                            <a href="{{ url('delete_bathinda_zoo/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                 	@endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        {{ $bathinda_zoo_list->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

