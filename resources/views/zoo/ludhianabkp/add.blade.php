<?php error_reporting(0); ?>
@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Ludhiana Zoo Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/ludhiana_zoo_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Ludhiana Zoo List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="{{ url('/add_ludhianazoo') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Ludhiana Zoo Banner: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" placeholder="Enter WildLife Title Image" name="banner_image" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Banner Title" name="banner_heading" value="{{ old('banner_heading') }}">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Banner Heading" name="heading"  value="{{ old('heading') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Description: </label>
                                        <div class="col-lg-6">
                                           <!--  <input type="text" class="form-control" placeholder="Enter Banner Title" name="description" value=""> -->
                                           <textarea class="form-control" name="description" style="height:100px;" rows="10">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ old('location') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full Access" name="access" value="{{ old('access') }}"/>
                                    </div>
                                 
                                </div> 

                                <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Visiting Days:</label>
                                        <input type="text" class="form-control" placeholder="Enter Visiting Days" name="visiting_days" value="{{ old('visiting_days') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Zoo Hours:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full Zoo Hours" name="zoo_hours" value="{{ old('zoo_hours') }}"/>
                                    </div>
                                 
                                </div>

                                <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Zoo Holidays:</label>
                                        <input type="text" class="form-control" placeholder="Enter Zoo Holidays" name="zoo_holidays" value="{{ old('zoo_holidays') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Special Attractions:</label>
                                        <input type="text" class="form-control" placeholder="Enter Special Attractions" name="special_attraction" value="{{ old('special_attraction') }}"/>
                                    </div>
                                 
                                </div>  

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Zoo Residents: </label>
                                        <div class="col-lg-6">
                                           <textarea class="form-control" name="zoo_residence" style="height:100px;" rows="5">{{ old('zoo_residence') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

