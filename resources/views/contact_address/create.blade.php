  
  @extends('layouts.master')
  @section('content')

        <div class="page-content-wrapper">
            <div class="page-content">

                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Minister & Officials </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/contact-address') }}">Minister & Officials List</a>
                            
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5"> 
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Create Contact Adress</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/contact-address') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Minister & Officials List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="{{ url('/add_contact_detail') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                            
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Image:<span style="color:red;">*</span> </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="image" value="{{ old('image') }}" accept="image/png, image/gif, image/jpeg , image/jpg">
                                        </div>
                                    </div>
                                </div>

                            

                                <div class="form-body b_heading" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Name: <span style="color:red;">*</span></label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" placeholder="Enter contact name">
                                        </div>
                                    </div>
                                </div>
                            
                            	 <div class="form-body b_heading" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Designation: <span style="color:red;">*</span></label>
                                        <div class="col-lg-6">
<!--                                         <input name="designation" class="form-control" style="height: 100px;" value="{{ old('designation')}}"> -->
                                        <textarea name="designation" class="form-control" rows="6" cols="4">{{ old('designation') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="form-body b_heading" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Contact Number: <span>(Optional)</span></label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="contact" id="contact" value="{{ old('contact') }}" placeholder="Enter contact number">
                                        </div>
                                    </div>
                                </div>

                               
                             
                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

@endpush


