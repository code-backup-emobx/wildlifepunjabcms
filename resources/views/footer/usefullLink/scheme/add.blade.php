@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Usefull Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Scheme</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/scheme_detail') }}">Footer Scheme List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_scheme') }}">Add Scheme</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Scheme</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/scheme_detail') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Footer Scheme List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           
                            <form action="{{ url('/add_scheme ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Scheme Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Scheme Background Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="" placeholder="Enter the Background Heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="add_more_scheme">
	                                <div class="form-body" style="padding-left: 5%">
	                                    <div class="form-group row">
	                                        <label class="col-md-3 col-form-label">Name of Scheme: </label>
	                                        <div class="col-lg-6">
	                                            <input type="text" class="form-control" name="name_of_scheme[]" value="" placeholder="Enter the name of scheme">
	                                        </div>
	                                    </div>
	                                </div>

	                                <div id="add_more_scheme_name" style="margin-bottom:5%;">
                                	</div>


	                            </div>    

                               

                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" value="Add More" style="float:right;">Add More</button>

                                </div>    

                                    
                       
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
    	// alert('hello');
      var html = $( ".add_more_scheme" ).html();
       $("#add_more_scheme_name").append(html);
  });
});
</script>
@endpush