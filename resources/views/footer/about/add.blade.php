@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Usefull Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>About-Us</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/about_us_detail') }}">Footer About List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_about') }}">Add About</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add About</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/about_us_detail') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">About List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          
                            <form action="{{ url('/add_about ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload About Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Scheme Background Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{ old('banner_heading') }}" placeholder="Enter the Banner Heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="add_more_scheme">
	                                <div class="form-body">
	                                    <div class="form-group row">
	                                        <label class="col-md-3 col-form-label">Title: </label>
	                                        <div class="col-lg-6">
	                                            <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Enter the name of Title">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div> 

	                           <div class="form-group row">
									<div class="col-lg-12">
										<label>About Description:</label>
										<textarea name="description" id="editor1">{{ old('description') }}</textarea>
									</div>
								</div>
	                              
	                         <!--    <div class="add_more_scheme">
	                                <div class="form-body">
	                                    <div class="form-group row">
	                                        <label class="col-md-3 col-form-label">Protected Wetland Title: </label>
	                                        <div class="col-lg-6">
	                                            <input type="text" class="form-control" name="protected_area_heading" value="{{ old('description') }}" placeholder="Enter the name of Title">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>  
 -->
	                            <!-- <div class="form-group row">
									<div class="col-lg-12">
										<label>Protected Wetland Description:</label>
										<textarea name="protected_area_description" id="editor2"></textarea>
									</div>
								</div> -->

                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
    CKEDITOR.replace('editor1' );
</script>
<script>
    CKEDITOR.replace('editor2' );
</script>
@endpush