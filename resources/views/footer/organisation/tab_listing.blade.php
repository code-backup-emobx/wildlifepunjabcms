@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Organisation Chart</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/organisation_list') }}">Organisation List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_tabdetail/'.$id) }}">Organisation Tab Listing</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Organisation Tab Listing</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_org_detail/'.$id) }}">
                                    <button type="button" class="btn btn-sm">Add More Tab Detail<i class="fa fa-plus"></i></button>
                                </a>&nbsp;
                                 <a href="{{ url('/organisation_list') }}">
                                    <button type="button" class="btn btn-sm">Organisation List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Chart Heading  </th>
                                                        <th>Designation Of The Officer</th>
                                                        <th>Name Of The Officer</th>
                                                        <th>Range</th>
                                                        <th>Beat</th>
                                                        <th>Block</th>
                                                        <th>Territorial Range Beat Block</th>
                                                        <th>Officer Mobile No</th>
                                                        <th>Image</th>
                                                        <th> Action  </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    
                                                 	@foreach($tab_details as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->chart_heading }} </td>
                                                      	<td> {{ $listing->designation_of_the_officer }} </td>
                                                      	<td> {{ $listing->name_of_the_officer }} </td>
                                                      	<td> {{ $listing->range }} </td>
                                                      	<td> {{ $listing->beat }} </td>
                                                      	<td> {{ $listing->block }} </td>
                                                      	<td> {{ $listing->territorial_range_beat_block }} </td>
                                                      	<td> {{ $listing->officer_mobile_no }} </td>
                                                      	<td> @if($listing->image)<img src="{{ $listing->image }}" style="height:50%;width:100%;"/> @else -- @endif</td>
                                                   		
                                                        <td>
                                                            <a href="{{ url('/edit_tab_detail/'.$listing->id.'/'.$id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>

                                                            <a href="{{ url('/delete_tab_detail/'.$listing->id ) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                      {{ $tab_details->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

