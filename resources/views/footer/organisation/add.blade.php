@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Organisation Chart</span>
                            <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/organisation_list') }}">Organisation List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_organisation') }}">Add Organisation Detail</a>
                        </li>
                      
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Organisation Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/organisation_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Organisation List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          

                            <form action="{{ url('/add_organisation ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="" placeholder="Enter the banner heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Organisation: </label>
                                        <div class="col-lg-6">
                                          <select class="form-control" name="tab_id">
                                            <option value="">--Select--</option>
                                          @foreach($organisation_tab_detail as $list)
                                          <option value="{{ $list->id }}">{{ $list->tab_name }}</option>
                                          @endforeach
                                          </select>
                                        </div>
                                    </div>
                                </div>

                               
                               <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"> Chart Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="chart_heading" value="" placeholder="Enter the chart heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Designation of the Officer/ Official: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="designation_of_the_officer" value="" placeholder="Enter the designation of the officer">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Name of the Officer/ Official: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="name_of_the_officer" value="" placeholder="Enter the designation of the officer/official">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Range: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="range" value="" placeholder="Enter the range">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Block: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="block" value="" placeholder="Enter the block">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Beat: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="beat" value="" placeholder="Enter the beat">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Territorial Range/Block/Beat: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="territorial_range_beat_block" value="" placeholder="Enter the territorial Range/Block/Beat">
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Mobile No. of the Officer/Official: </label>
                                        <div class="col-lg-6">
                                            <input type="number" class="form-control" 
                                            name="officer_mobile_no" value="" cplaceholder="Enter the mobile No. of the Officer/Official">
                                        </div>
                                    </div>
                                </div>

                               
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
