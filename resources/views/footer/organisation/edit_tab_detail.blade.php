@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Organisation Chart</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/organisation_list') }}">Organisation List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_tabdetail/'.$id) }}">Organisation Tab Listing</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit_tab_detail/'.$id.'/'.$o_id) }}">Edit Organisation Detail</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Organisation Tab Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view_tabdetail/'.$o_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Organisation Tab List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          

                            <form action="{{ url('/edit_tab_detail/'.$edit_tab_detail->id.'/'.$o_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                
                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tab Heading: </label>
                                        <div class="col-lg-6">
                                            <select name="tab_id" class="form-control">
                                            	<option value="">Select</option>
                                           	@foreach($tab_name as $list)
                                           		<option value="{{ $list->id }}" @if($list->id == $edit_tab_detail->tab_id) selected @endif>{{ $list->tab_name }}</option>
                                           	@endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            
                            @if($edit_tab_detail->type_of_table == "table_data")

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Designation Of The Officer: </label>
                                        <div class="col-lg-6">
                                           <input type="text" value="{{ $edit_tab_detail->designation_of_the_officer }}" name="designation_of_the_officer" class="form-control">
                                        </div>
                                    </div>
                                </div>


                                 <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Name Of The Officer: </label>
                                        <div class="col-lg-6">
                                           <input type="text" value="{{ $edit_tab_detail->name_of_the_officer }}" name="name_of_the_officer" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Range: </label>
                                        <div class="col-lg-6">
                                           <input type="text" value="{{ $edit_tab_detail->range }}" name="range" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Beat: </label>
                                        <div class="col-lg-6">
                                           <input type="text" value="{{ $edit_tab_detail->beat }}" name="beat" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Block: </label>
                                        <div class="col-lg-6">
                                           <input type="text" value="{{ $edit_tab_detail->block }}" name="block" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Territorial Range Beat Block: </label>
                                        <div class="col-lg-6">
                                           <input type="text" value="{{ $edit_tab_detail->territorial_range_beat_block }}" name="territorial_range_beat_block" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                

                               <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Officer Mobile No: </label>
                                        <div class="col-lg-6">
                                           <input type="number" value="{{ $edit_tab_detail->officer_mobile_no }}" name="officer_mobile_no" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                            @if($edit_tab_detail->type_of_table == "image")
                            
                            	<div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Image: </label>
                                        <div class="col-lg-6">
                                           <input type="file" value="" name="image" class="form-control">
                                        </div>
                                    </div>
                                <div><img src="{{ $edit_tab_detail->image }}" style="height:20%;width:15%;margin-left:26%;"></div>
                                </div>
                            @endif

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
