@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Contact About</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/contact_us') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Contact Us Banner List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('/add_contactDetail ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

								<div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Contact Type: </label>
                                        <div class="col-lg-6">
                                            <select name="content_type_id" class="form-control">
                                            	<option value="">--Select--</option>
                                            	@foreach($contactus_detail as $list)
                                            	<option value="{{ $list->id }}">{{ $list->category_name }}</option>
                                            	@endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Icon: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="image_icon" value="" required>
                                        </div>
                                    </div>
                                </div>

                              <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Description: </label>
                                        <div class="col-lg-6">
                                           <textarea name="description" class="form-control">
                                           	
                                           </textarea>
                                        </div>
                                    </div>
                                </div>

                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

