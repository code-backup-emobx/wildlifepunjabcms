@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Contact List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_contactbanner') }}">
                                    <button type="button" class="btn btn-sm">Add Contact Banner
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>

                            <div class="actions">
                                <a href="{{ url('/add_contactDetail') }}">
                                    <button type="button" class="btn btn-sm">Add Contact Detail
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Banner Image </th>
                                                        <th> Banner Heading</th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php 
                                                    $count = 1;
                                                    @endphp
                                                 	@foreach($contact_us_banner_detail as $listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <image src="{{ $listing->banner_image }}" style="width:100px;"> </td>
                                                        <td> {{ $listing->banner_heading }} </td>
                                                       
                                                        <td>
                                                            <a href=""><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                   	@endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

