@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Living Here</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tender_detail') }}">Tender List</a>
                        </li>
                       
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Tender List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_tender_detail') }}">
                                    <button type="button" class="btn btn-sm">Add Tender Detail
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Banner Image </th>
                                                        <th> Banner Title </th>
                                                        <th> Total Tender</th>
                                                        <th> Action </th>
                                                    </tr>
                                                  
                                                </thead>
                                              
                                                <tbody>
                                                   
                                                 	@foreach($tender_list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> <img src="{{ $listing->banner_image }}" style="height:100px;width:100px;"> </td>
                                                     	<td>{{ $listing->banner_heading }}</td>

                                                       
                                                        <td>
                                                            <a href="{{ url('tender-list/'.$listing->id) }}"><span class="btn btn-light btn-primary">{{ $listing->t_count }}</span></a>
                                                           
                                                        </td>
                                                      
                                                        <td>
                                                        	<!-- <a href="{{ url('add_tenderDetail/'.$listing->id ) }}" ><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Add More Tender<i class="fa fa-plus"></i></button></a>&nbsp;&nbsp;&nbsp; -->
                                                            <a href="{{ url('update_tender/'.$listing->id ) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>&nbsp;&nbsp;&nbsp;
                                                            <a href="{{ url('delete_tender/'.$listing->id ) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                   @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    {{ $tender_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

