@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                   <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Notification</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/notification_detail') }}">Notification List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/noti_pdf_name_list/'.$id) }}">Notification Pdf Name Files List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update_detail/'.$id) }}">Edit Notification Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Notification Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/noti_pdf_name_list/'.$id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Notification List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            

                            <form action="{{url('update_detail/'.$edit_detail->id) }}" class="form" method="post" enctype="multipart/form-data">
                            	   {{ csrf_field() }}
								<div class="card-body">
									
									<div class="mb-15">
										<div class="form-group row">
											<label class="col-lg-3 col-form-label ">Pdf Name:</label>
											<div class="col-lg-6">
												<input type="text" class="form-control" name="name" value="{{ $edit_detail->name }}"/>
												
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label ">Pdf File:</label>
											<div class="col-lg-6">
												<input type="file" class="form-control" name="pdf_url"/>
												
											</div>

											<div class="col-lg-6" style="margin-left: 25%;margin-top: 1%;">
												<iframe src="{{ $edit_detail->pdf_url }}"></iframe>
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<div class="row">
										<div class="col-lg-3"></div>
										<div class="col-lg-6">
											<input type="Submit" class="btn btn-success mr-2" value="Submit">
											<button type="reset" class="btn btn-secondary">Cancel</button>
										</div>
									</div>
								</div>
							</form>
                           
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

