@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Policy Guideline</span>
                            <i class="fa fa-circle"></i>
                        </li> 
                        <li>
                            <a href="{{ url('/policy_guideline') }}">Footer Policy Guideline List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_policy_guideline') }}">Add Policy Guideline PDF</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5"> 
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Policy Guideline PDF</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/policy_guideline') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Policy Guideline List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           
                            <form action="{{ url('/add_policy_guideline ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Policy Guideline PDF: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" 
                                            name="policy_guideline_pdf" value="" accept="application/pdf">
                                        </div>
                                    </div>
                                </div>


                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

