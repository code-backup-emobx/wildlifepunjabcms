@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/act_and_rule') }}">Acts & Rules List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Acts & Rules List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/addactrule') }}">
                                    <button type="button" class="btn btn-sm">Add Acts & Rules
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Background Image </th>
                                                        <th> Banner Title </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php 
                                                    $count = 1;
                                                    @endphp
                                                 	@foreach($act_rule_list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> <img src="{{ $listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $listing->banner_heading }} </td>
                                                        <td>
                                                            <a href="{{ url('/update_actrulebanner/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('/delete_actrulebanner/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>

                                                            <a href="{{ url('/basic-actrulepdf/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">View<i class="fa fa-eye"></i></button></a>

                                                            <a href="{{ url('/view-wildlife/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Wildlife<i class="fa fa-eye"></i></button></a>

                                                             <a href="{{ url('/view_management/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Management plan<i class="fa fa-eye"></i></button></a>

                                                        </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $act_rule_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

