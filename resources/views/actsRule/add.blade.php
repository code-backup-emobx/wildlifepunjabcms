@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/act_and_rule') }}">Acts & Rules List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/addactrule') }}">Add Acts And Rules</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Acts And Rules</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/act_and_rule') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Acts And Rules List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           
                            	<form action="{{ url('/addactrule ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="card-body">
									<div class="form-group row">
										<div class="col-lg-6">
											<label>Banner Text:</label>
											<input type="text" class="form-control" name="banner_heading" placeholder="Enter the banner heading"/>
										</div>
										<div class="col-lg-6">
											<label>Banner Image:</label>
											<input type="file" class="form-control" name="banner_image" accept="image/*" />
										</div>
									</div>
								</div>	

                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                           
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection


@push('page-script')

<!-- <script>
   $( document ).ready(function() {
    $("#btnModal").click(function(){
    // alert('hi');
    $('#modal').modal('show');
  CKEDITOR.replace('editor1');
  });
});
</script> -->
<script>
    CKEDITOR.replace( 'editor1' );
</script>
@endpush

