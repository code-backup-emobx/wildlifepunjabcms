@extends('layouts.master')
@section('content')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Acts And Rules Wildlife Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view-wildlife/'.$id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Acts&Rules Wildlife List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            	<form action="{{ url('/add-more-wildlife/'.$id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}


								<div class="card-body">
									<div class="form-group row">
										<div class="col-lg-12">
											<label>Wildlife Heading Text:</label>
											<input type="text" class="form-control" name="wildlife_heading" value="{{ old('wildlife_heading') }}"/>
										</div>
										
									</div>
								</div>

							    <div class="card-body">

									<div class="form-group row">
										<div class="col-lg-12">
											<label>Wildlife Description:</label>
											<textarea name="description" id="editor1">{{ old('description') }}</textarea>
										</div>
									</div>

								</div>
								
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                           
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection


@push('page-script')

<!-- <script>
   $( document ).ready(function() {
    $("#btnModal").click(function(){
    // alert('hi');
    $('#modal').modal('show');
  CKEDITOR.replace('editor1');
  });
});
</script> -->
<script>
    CKEDITOR.replace( 'editor1' );
</script>
@endpush

