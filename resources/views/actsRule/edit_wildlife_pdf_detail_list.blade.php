@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                     <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/act_and_rule') }}">Acts & Rules List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/wildlife-pdf-detail-list/'.$pdf_id.'/'.$ab_id) }}">Acts & Rules Wildlife List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/update_subheadingpdf/'.$id.'/'.$pdf_id.'/'.$ab_id) }}">Update Act&Rule Pdf Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Act&Rule Pdf Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/wildlife-pdf-detail-list/'.$pdf_id.'/'.$ab_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Act&Rule Wildlife Pdf List<i class="fa fa-list"></i></button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          

                            <form action="{{ url('/update_subheadingpdf/'.$edit->id.'/'.$pdf_id.'/'.$ab_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Name: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="name_of_pdf" value="{{ $edit->name_of_pdf }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Pdf File: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="pdf_link" value="{{ $edit->pdf_link }}" accept=".pdf">
                                            <iframe src="{{ $edit->pdf_link}}"></iframe>
                                        </div>
                                    </div>
                                </div>

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>
@endpush
