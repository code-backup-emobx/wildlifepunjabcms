@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/act_and_rule') }}">Acts & Rules List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_management/'.$id) }}">Management List</a>
                             <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/add_new_management/'.$id) }}">Add Management Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Management Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view_management/'.$id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Management List<i class="fa fa-list"></i></button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           

                            <form action="{{ url('/add_new_management/'.$id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Management Plan Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="management_plan_heading" value="{{old('management_plan_heading')}}" placeholder="Enter Management Plan Heading">
                                        </div>
                                    </div>
                                </div>

                               <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more" value="Add More" style="margin-left:72%;">+</button>
                                </div>&nbsp;


                                <div class="sub_heading_management" style="background-color: #E5EFF1; margin-bottom:20px;">
                                    <div><p style="padding:1%;color:black;">Add Subheading Management Plan</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="sub_heading_management">
                                        <div class="form-group row" >
                                            <label class="col-md-1 col-form-label">Sub Heading: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter Sub Heading" name="subheading[]" value="{{old('subheading[]')}}">
                                            </div>

                                            <label class="col-md-1 col-form-label">Pdf File: </label>
                                            <div class="col-lg-4">
                                                <input type="file" class="form-control"  name="pdf_file[]" value="{{old('pdf_file[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_sub_heading_management" style="padding-left: 2%;margin-bottom:5%;">
                                    </div>
                                </div>  
                               
                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>
@endpush
