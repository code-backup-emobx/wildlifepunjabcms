@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Management Plan PDF List</span>
                            </div>
                           
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th>Subheading PDF</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    
                                                 	@foreach($listpdf as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> <iframe src="{{ $listing->pdf }}"></iframe> </td>
                                                        <td>
                                                            <a href="{{ url('/update_subheadingpdf/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                           
                                                           
                                                        </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $listpdf->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

