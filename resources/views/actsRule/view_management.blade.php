@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/act_and_rule') }}">Acts & Rules List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_management/'.$id) }}">Management Listt</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Management List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/act_and_rule') }}">
                                    <button type="button" class="btn btn-sm">Act & Rule List </button>
                                </a>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_new_management/'.$id) }}">
                                    <button type="button" class="btn btn-sm">Add New Management Heading </button>&nbsp;
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Management Heading </th>
                                                        <th> Total Pdf </th>
                                                        <!-- <th> Pdf File </th> -->
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                   @foreach($list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->managemt_heading  }} </td>
                                                       
                                                        <td> 
                                                          <a href="{{ url('list_mana_heading/'.$listing->id.'/'.$id) }}">  <span class="btn btn-light btn-primary">{{ $listing->total_subheading }}</span></a>
                                                            <!-- @php
                                                            $count = 1;
                                                            @endphp
                                                             @foreach($listing->getsubheading as $subheadinglisting)
                                                            {{ $count++ }}.{{ $subheadinglisting->subheading }}</br>
                                                            @endforeach -->
                                                        </td>

                                                        <!-- <td> 
                                                            @php
                                                            $count = 1;
                                                            @endphp
                                                             @foreach($listing->getsubheading as $subheadinglisting)
                                                            {{ $count++ }}.<iframe src="{{ $subheadinglisting->pdf }}"></iframe></br>
                                                            @endforeach
                                                             <a href="{{ url('list_pdf/'.$listing->id) }}">  <span class="btn btn-light btn-primary"> {{ $listing->total_pdf }}</span></a>
                                                        </td> -->
                                                     
                                                        <td>
                                                             
                                                            <a href="{{ url('/edit_management/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>

                                                            <a href="{{ url('/delete_management/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                          

                                                            
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

