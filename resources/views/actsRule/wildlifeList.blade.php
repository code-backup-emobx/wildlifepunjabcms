@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Quick Links</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/act_and_rule') }}">Acts & Rules List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-wildlife/'.$ab_id) }}">Acts & Rules Wildlife List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Acts & Rules Wildlife List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-more-wildlife/'.$ab_id) }}">
                                    <button type="button" class="btn btn-sm add_more_detail">Add Acts & Rules Wildlife
                                        <i class="fa fa-plus"></i></button>
                                </a>
                                <a href="{{ url('/act_and_rule') }}">
                                    <button type="button" class="btn btn-sm">Acts & Rules List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Heading </th>
                                                        <th> Description </th>
                                                        <th> Total Files </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                  
                                                 	@foreach($w_list as $listing)
                                                    <tr>
                                                    	<input type="hidden" value="1" id="check_value">
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->wildlife_heading }} </td>
                                                        <td> {!! $listing->description !!} </td>
                                                        <td> <a href="{{ url('wildlife-pdf-detail-list/'.$listing->id.'/'.$ab_id) }}">  <span class="btn btn-light btn-primary"> {{ $listing->w_count }}</span></a> </td>
                                                        
                                                        <td>
                                                            <a href="{{ url('/update-actrule-wildlife-detail/'.$listing->id.'/'.$ab_id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('/delete-actrule-wildlife-detail/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>

                                                        </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $w_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection
@push('page-script')
<script>
$( document ).ready(function() {
   // alert('working');
   var check_value = $("#check_value").val();
   // alert(check_value);
   if(check_value == 1){
    $(".add_more_detail").hide();
   }
   else{
    $(".add_more_detail").show();

   }
 
});
</script>
@endpush

