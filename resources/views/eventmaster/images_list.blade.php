@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Event Folder Master</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/event-gallery-images-list') }}">Event Gallery List</a>
                        </li>
                      
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                          
                                <span class="caption-subject">Event Gallery List</span>
                           
                            <div class="actions">
                                <a href="{{ url('/add-event-gallery-image/'.$id) }}">
                                    <button type="button" class="btn btn-sm"> Add Image
                                        <i class="fa fa-plus"></i></button>
                                </a>
                                <a href="{{ url('/event-gallery-images-list') }}">
                                    <button type="button" class="btn btn-sm"> Event Gallery List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Sr.No </th>
                                                        <th> Event Name</th>
                                                        <th> Images</th>
                                                        <th colspan="2"> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                  @foreach($list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td>{{ $listing->event_master_name }}</td>
                                                      
                                                        <td><img src="{{ $listing->image }}" style="width: 24%;"></td>
                                                       
                                                        <td>
                                                            <a href="{{ url('edit_event_gallery_detail/'.$listing->id.'/'.$id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a></td><td>
                                                            <a href="{{ url('delete_event_gallery_detail/'.$listing->id.'/'.$id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            <!-- *********** -->
                             {{ $list->links('vendor.pagination.custom') }}
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

