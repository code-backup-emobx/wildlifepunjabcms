 
 <div id="footer">
               <div class="container_alt container_vis">
                  <div class="foocol first">
                     <div class="footer-logo">
                        <a class="logo" href="{{ url('/index')}}">
                          
                           <img class="lazyload tranz" src="{{ asset('frontend/uploads/2018/11/logo.png') }}" data-src="{{ asset('frontend/uploads/2018/11/logo.png') }}" alt="CityGov"/> 
                     </a>
                     </div>
                     <div class="clearfix"></div>
                     <div class="textwidget">
                        <h4>Forest Complex</h4>
                        <p>Tower 2, Second floor, Sector 68, Sahibzada Ajit Singh Nagar, Punjab 160062<br /> Phone: <a href="tel:0172 229 8010">(0172) 229-8010</a> • Monday – Friday, 9:00 am &#8211; 5:00 pm</p>
                     </div>
<!--                      <form role="search" class="searchform" method="get" action=""> <label for="search-form-60504cacbe426"> <span class="screen-reader-text">Search for:</span> </label> <input id="search-form-60504cacbe426"  type="text" name="s" class="s p-border" size="30" value="I am looking for..." onfocus="if (this.value = '') {this.value = '';}" onblur="if (this.value == '') {this.value = 'I am looking for...';}" /> <button class='searchSubmit ribbon' >Search</button></form> -->
                  </div>
                 <div class="foocol sec">
                     <h2 class="widget dekoline dekoline_small">Informative Links</h2>
                     <div class="textwidget">
                       <ul>
                           <li><a href="{{ url('/tenderDetail') }}">Tender</a></li>
<!--                            <li><a href="{{ url('/demo_page') }}">Citizen charter</a></li> -->
                       <li><a href="{{ url('wetland-authority') }}">Wetland Authority</a></li>
<!--
                           <li><a href="#">Culture &#038; Recreation</a></li>
                           <li><a href="#">Business</a></li>
                           <li><a href="#">Documents</a></li>
-->
                        </ul>
                     </div>
                  </div>
                  <div class="foocol">
                     <h2 class="widget dekoline dekoline_small">Useful Links</h2>
                     <div class="textwidget">
                        <ul>
                           
                          <li><a href="{{ url('/schemeDetail') }}">Scheme</a></li>
                             <li><a href="{{ url('/aboutus_detail') }}">About us</a></li>
                             <li><a href="{{ url('/contact_us') }}">Contact us</a></li>
                          

                        @php 

                        $policyguideline = App\Models\FooterPolicyGuideline::where('deleted_status','0')->latest()->first(); 

                        @endphp

                           <li><a href="{{ $policyguideline->policy_guideline }}" target="_blank">Policy Guidelines</a></li>
                           <li><a href="{{ url('/coffee-table-book') }}" target="_blank">Coffee Table Book</a></li>
                        </ul>
                     </div>
                  </div>
                  <div id="foo-spec" class="foocol last">
                     <h2 class="widget dekoline dekoline_small">Quick Links</h2>
                     <div class="textwidget">
                        <ul>
                           <li><a href="{{ url('/acts_rule_detail') }}">Act &amp; Rules</a></li>
                            <li><a href="{{ url('/organisation_chart_detail') }}">Organization Chart</a></li>
                           <li><a href="{{ url('/demo_page') }}">Calendar</a></li>
                           <li><a href="{{ url('/notification_details') }}">Notification</a></li>
                           <li><a href="{{ url('/tourism_details') }}">Tourism</a></li>
                           <li><a href="{{ url('/imp_link') }}">Important Link</a></li>
<!--                            <li><a href="{{ url('/login') }}">Login</a></li> -->
                        </ul>
                     </div>
                  </div>
                     
               
               
               </div>
               <div class="clearfix"></div>
               <div class="container_vis">
                  <div id="footop" class="footop populated">
                     <div class="footop-right">
                        <ul class="social-menu tranz">
                           <li class="sprite-facebook"><a class="mk-social-facebook" href="https://www.facebook.com/Department-of-Wildlife-Preservation-105583598822053" target="_blank"><img src="{{ asset('assets/img/facebook_footer.png')}}" /><span>Facebook</span></a></li>
                           <li class="sprite-twitter"><a class="mk-social-twitter-alt" href="https://twitter.com/punjab_forest" target="_blank"><img src="{{ asset('assets/img/twitter_footer.png')}}" /><span>Twitter</span></a></li>
                           <li class="sprite-instagram"><a class="mk-social-photobucket" href="https://www.youtube.com/channel/UChWnVVdus0NrTZObHWlaMVg" target="_blank"><img src="{{ asset('assets/img/youtube_white.png')}}" /><span>YouTube</span></a></li>
<!--                           <li class="sprite-foursquare"><a href="index.html#Foursquare"><i class="fab fa-foursquare"></i><span>Foursquare</span></a></li>-->
<li class="sprite-facebook"><a class="" href="https://play.google.com/store/apps/details?id=com.app.wildlifedepartment" target="_blank"><img src="{{ asset('frontend/img/play.png') }}" style="width:36%;"/></a></li>
                           <li class="search-item"> <a class="searchOpen" href="" aria-label="Open Search Window"><i class="fas fa-search"></i><span class="screen-reader-text">Open Search Window</span></a></li>
                        </ul>
                     </div>
                     <h2 class="footer_text">Department of Wildlife Preservation, Punjab</h2>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="footer-menu">
                  <div class="container">
<!--                      <ul id="menu-footer-menu" class="bottom-menu">
                        <li id="menu-item-5479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-5479"><a href="index.html" aria-current="page">Home</a></li>
                        <li id="menu-item-5523" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5523"><a href="citygov/documents.html">Documents</a></li>
                        <li id="menu-item-5477" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5477"><a href="index.html">Government Old</a></li>
                        <li id="menu-item-5478" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5478"><a href="citygov/news.html">Announcements</a></li>
                     </ul> -->
                     @php
                     $date = date('Y');
                     @endphp
                      <div class="footer_credits"><a href="#">Copyright © {{$date}} - Department of Wildlife Preservation, Punjab | </a> <a href="https://www.emobx.com/" target="_blank">Developed and Maintained by eMobx</a> </div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div id="curtain" class="tranz">
<!--                <form role="search" class="searchform" method="get" action="index.html"> <label for="search-form-60504cacc0198"> <span class="screen-reader-text">Search for:</span> </label> <input id="search-form-60504cacc0198"  type="text" name="s" class="s p-border" size="30" value="I am looking for..." onfocus="if (this.value = '') {this.value = '';}" onblur="if (this.value == '') {this.value = 'I am looking for...';}" /> <button class='searchSubmit ribbon' >Search</button></form> -->
               <a class='curtainclose' href="" ><i class="fa fa-times"></i><span class="screen-reader-text">Close Search Window</span></a>
            </div>
            <div class="scrollTo_top ribbon"> <a title="Scroll to top" class="rad" href="">&uarr;</a></div>
         </div>
      </div>
    
      <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
      <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
      <noscript>
         <style>.lazyload{display:none;}</style>
      </noscript>
      <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
            jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
               bubbles: true,
               cancelable: true
            } );
         } catch ( e ) {
            jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
            jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script>
<!-- <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script> -->
<script defer src="{{ asset('frontend/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js') }}"></script>
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src="{{ asset('frontend/plugins/autoptimize/classes/external/js/lazysizes.min.js') }}"></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length -1}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
 <script type="text/javascript">
    $('#zoo_gallery').change(function(){
      // alert('here');
      var gallery_id = $(this).val();
       // alert(gallery_id);
      var token = '{{ csrf_token() }}';

        if(gallery_id){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('getzoo_image')}}?zoo_gallery="+gallery_id,
           success:function(data){
           if(data){
             // console.log("success");              
            // console.log(data);
             $("#zoo_gallery_images").empty();
              $("#myModalContent").empty();
              $.each(data,function(key,value){
                   var d = key+1;
                   // console.log(value);
                    $("#zoo_gallery_images").append('<div class="col-md-3 m-top1"><img src="'+value+'" style="width:100%;margin-bottom: 20px;height:100%;"  onclick="openModal('+d+');currentSlide('+d+')" class="hover-shadow cursor"></div>');

                    $('#myModalContent').append('<div class="mySlides"><div class="numbertext"></div><img src="'+value+'" style="width:100%;"><a class="prev" onclick="plusSlides(-1)">&#10094;</a><a class="next" onclick="plusSlides(1)">&#10095;</a></div>');

              

                   
                    
                });

           }else{
               $("#zoo_gallery_images").html('No record Found');
            }
            
           }
        });
    }


   });
</script>
   </body>
   </script>
        @stack('page-script')
    </body>
</html>