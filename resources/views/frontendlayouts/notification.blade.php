@if ($errors->any())
<!-- <div class="clerifix">&nbsp;</div> -->
    <div class="col-lg-12 alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (\Illuminate\Support\Facades\Session::has('success'))
<div class="clerifix">&nbsp;</div>
    <div class="col-lg-12 alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif
@if (\Illuminate\Support\Facades\Session::has('fail'))
<div class="clerifix">&nbsp;</div>
    <div class="alert alert-danger">
        <ul>
            <li>{{ Session::get('fail') }}</li>
           
        </ul>
    </div>
@endif
@if (\Illuminate\Support\Facades\Session::has('warning'))
<div class="clerifix">&nbsp;</div>
    <div class="alert alert-warning">
        <ul>
            
                <li>{{ Session::get('warning') }}</li>
           
        </ul>
    </div>
@endif