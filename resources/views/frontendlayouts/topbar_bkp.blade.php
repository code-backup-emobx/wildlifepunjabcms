      <div class="upper tmnf_width_normal tmnf-sidebar-active header_default">
         <div id="header" class="tranz" itemscope itemtype="http://schema.org/WPHeader">
            <div class="container_head">
               <a class="screen-reader-text ribbon skip-link" href="#content_start">Skip to content</a>
               <div class="clearfix"></div>
               <div id="titles" class="tranz2"> <a class="logo" href="{{ url('/index') }}"> <img class="tranz" src="{{ asset('frontend/uploads/2018/11/logo_1.png') }}" alt="CityGov"/> </a></div>
               <div class="header-right for-menu">
                  <input type="checkbox" id="showmenu" aria-label="Open Menu"> <label for="showmenu" class="show-menu ribbon" tabindex="0"><i class="fas fa-bars"></i> <span>Menu</span></label>
                  <nav id="navigation" class="rad tranz" itemscope itemtype="http://schema.org/SiteNavigationElement" role="navigation" aria-label="Main Menu">
                     <ul id="main-nav" class="nav" role="menubar">
                        
                         
                         <li id="menu-item-5514" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5514"><a href="{{ url('/home_page') }}">Home<span class="menu-item-description">Introduction</span></a></li>
                         <li id="menu-item-5514" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5514"><a href="{{ url('/wildlife_symbol') }}">State Wildlife Symbol<span class="menu-item-description">Current update</span></a></li>
                       <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Protected Area<span class="menu-item-description">Protected area list</span></a>
                           <ul class="sub-menu">
                                <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Wildlife Santuaries</a>
                            <ul class="sub-menu">
                                  <div class="ex1">  
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_motibagh') }}">Bir Moti Bagh Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_gurdialpur') }}">Bir Gurdialpura Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_bhunerheri') }}">Bir Bhunerheri Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_mehas_detail') }}">Bir Mehas Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_dosanjh_detail')}}">Bir Bir Dosanjh Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_bhadson_detail') }}">Bir Bhadson Wildlife Sanctuary </a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_aishwan_detail') }}"> Bir Aishwan Wildlife Sanctuary </a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_abohar_detail') }}"> Abohar Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_harike_detail') }}"> Harike Wildlife Sanctuary </a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_takhni_rehmapur') }}"> Takhni Rehmapur Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_jhajjar_bachauli') }}"> Jhajjar-Bachauli Wildlife Sanctuary</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_kathlaurkushlian_detail') }}"> Kathlaur-Kushlian Wildlife Sanctuary </a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/bir_nangal_wildlife_detail') }}">Nangal Wildlife Sanctuary</a></li>
                             </div>
                              </ul> 
                           </li>
                       
                           <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Community  Reserves</a>
                             <ul class="sub-menu">
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/panni-wala') }}">Panniwala-Gumjal-Haripura-Diwankhera Community Reserve</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/lalwan_detail') }}">Lalwan Community Reserve</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/keshopurdetail') }}">Keshopur-Miani Community Reserve</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/siswan_detail') }}">Siswan Community Reserve</a></li>
                                 
                            </ul> 
                           </li>
                              
                           <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Conservation Reserves</a>
                             <ul class="sub-menu">
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/rakh_sarai-detail') }}">Rakh Sarai Amanat Khan Conservation Reserve</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/ropar_wetland_detail')}}">Ropar Wetland Conservation Reserve</a></li>
                                 
                            <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/ranjit_sagar_dam_detail') }}">Ranjit Sagar Dam Conservation Reserve</a></li>
                                 
                           <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/beas_river_dam_detail') }}">Beas River Conservation Reserve</a></li>
                                 
                           <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/kali_bein_detail') }}">Kali Bein Conservation Reserve</a></li>  
                        </ul> 
                     </li>  
                  </ul>
               </li>
                         
                          <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Protected Wetland<span class="menu-item-description">Wetland Symbols</span></a>
                           <ul class="sub-menu">
                             <!--  <li id="menu-item-6206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6206"><a href="{{ url('/harika_santuary') }}">Harike Wildlife Sanctuary</a></li>
                              <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/nangal_santuary') }}">Nangal Wildlife Sanctuary</a></li>
                              <li id="menu-item-5519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5519"><a href="{{ url('/keshopur_detail') }}">Keshopur Community Reserve, Gurdaspur</a></li> -->
                              @php
                              $list = App\Models\ProtectedWetland::get();
                              @endphp
                              @foreach($list as $p_list)
                              <li id="menu-item-5519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5519"><a href="{{ url('/protected-wetland-detail/'.$p_list->id) }}">{{ $p_list->name }}</a></li>
                              @endforeach
                           </ul>
                        </li>
                         
                            <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Zoos<span class="menu-item-description">zoos features</span></a>
                           <ul class="sub-menu">
                              <!-- <li id="menu-item-6206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6206"><a href="{{ url('/chhhatbir_zoo_detail') }}">Chhatbir Zoo</a></li> -->
                              <!-- <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/ludhiana_zoo_detail') }}">Ludhiana Zoo</a></li> -->
                              <!-- <li id="menu-item-5519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5519"><a href="{{ url('patiala_zoo_detail') }}">Mini zoo, Patiala</a></li> -->
                               <!-- <li id="menu-item-5519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5519"><a href="{{ url('bathinda_zoo_detail') }}">Mini zoo, Bathinda</a></li> -->
                               <!-- <li id="menu-item-5519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5519"><a href="{{ url('deerpark_zoo_detail') }}">Deer Park, Neelon</a></li> -->

                               @php
                                $zoo_list = App\Models\ZooType::get();
                               @endphp
                               @foreach($zoo_list as $listing)

                                <li id="menu-item-5519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5519"><a href="{{ url('zoo-detail-list/'.$listing->id  ) }}">{{ $listing->zoo_type_name }}</a></li>

                               @endforeach
                             
                           </ul>
                        </li>
                         
                         <li id="menu-item-5515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5515" aria-haspopup="true" role="menuitem" aria-expanded="false" tabindex="0">
                           <a href="#">Services<span class="menu-item-description">online services</span></a>
                           <ul class="sub-menu">
                             <!-- <li id="menu-item-6206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6206"><a href="{{ url('/service_detail') }}">Issuance of Hunting Permit for crop damages wild animals like Blue Bull and Wild Boar.<span class="online"> Offline</span></a></li> -->
                              <li id="menu-item-6206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6206"><a href="https://play.google.com/store/apps/details?id=com.app.wildlifedepartment" target="_blank">Issuance of Hunting Permit for crop damages wild animals like Blue Bull and Wild Boar.<span class="online"> <p style="color:#66cc66;"> Online</p></span></a></li>
                              <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="https://play.google.com/store/apps/details?id=com.app.wildlifedepartment" target="_blank">Issuance of Permission/NOC for taking Arms License. <span class="online"> <p style="color:#66cc66;"> Online</p></span></a></li>
                               <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/demo_page') }}">Research, Study, Survey <span class="online"></span></a></li>
                               <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="{{ url('/demo_page') }}">Filmshooting, Flash photography, Flash videography <span class="online"> Offline</span></a></li>
                               <li id="menu-item-5518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5518"><a href="http://parivesh.nic.in/" target="_blank">Developmental <span class="online"> Online</span></a></li>
                            
                           </ul>
                        </li>

                    </ul>
                  </nav>
               </div>
               <div class="clearfix"></div>
               <div id="bottombar" class="bottomnav tranz" role="navigation" aria-label="Quick Links">
                  <p class="menu_label">Quick Links:</p>
                  <div class="header-right">
                    <ul id="add-nav" class="nav">
                         <li id="menu-item-5541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5541"><a href="{{ url('/acts_rule_detail') }}">Acts &amp; Rules</a></li>
                          <li id="menu-item-6208" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6208"><a href="{{ url('organisation_chart_detail') }}">Organization Chart</a></li>
                        <li id="menu-item-5542" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5542"><a href="{{ url('calendar') }}">Calendar</a></li>
                        <li id="menu-item-5544" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5544"><a href="{{ url('notification_details') }}">Notification</a></li>
                          <li id="menu-item-5544" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5544"><a href="{{ url('/tourism_details') }}">Tourism</a></li>
                           <li id="menu-item-5544" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5544"><a href="{{ url('/imp_link') }}">Wetland Authority</a></li>
                     </ul>
                      <ul class="social-menu tranz">
                        <li class="sprite-facebook"><a class="mk-social-facebook" href="https://www.facebook.com/Department-of-Forests-Wildlife-Preservation-Punjab-164167300318218" target="_blank"><img src="{{ asset('assets/img/facebook.png')}}" /><span>Facebook</span></a></li>
                        <li class="sprite-twitter"><a class="mk-social-twitter-alt" href="https://twitter.com/punjab_forest" target="_blank"><img src="{{ asset('assets/img/twitter.png')}}" /><span>Twitter</span></a></li>
                        <li class="sprite-instagram"><a class="mk-social-photobucket" href="{{url('/demo_page')}}"><img src="{{ asset('assets/img/insta.png')}}" /><span>Instagram</span></a></li>

<!--                        <li class="sprite-foursquare"><a href="index.html#Foursquare"><i class="fab fa-foursquare"></i><span>Foursquare</span></a></li>-->
<!--                        <li class="search-item"> <a class="searchOpen" href="" aria-label="Open Search Window"><i class="fas fa-search"></i><span class="screen-reader-text">Open Search Window</span></a></li>-->
                     </ul>

                     
                        @php 

                        $visitor_count = App\Models\Visitor::count();

                        @endphp
                      <div class="visitor-count">
                         <p style="color: white;">Visitor Count</p>
                          <div class="center-counter">
                           @if($visitor_count <= 9)
                              <span class="counter">0</span>
                             <span class="counter">0</span>
                            <span class="counter">0</span>
                            <span class="counter">{{ $visitor_count }}</span>
                           @elseif($visitor_count <= 99)
                             
                            <span class="counter">0</span>
                            <span class="counter">0</span>
                            <span class="counter">{{ $visitor_count }}</span> 
                           @elseif($visitor_count <= 999)
                             
                            <span class="counter">0</span>
                            <span class="counter">{{ $visitor_count }}</span>
                           @else
                  <span class="counter">{{ $visitor_count }}</span>
                          @endif
                            </div>
                      </div>

                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
    
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
           
         
           
     
     