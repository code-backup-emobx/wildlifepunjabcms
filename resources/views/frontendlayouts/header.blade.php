<?php 
   header('X-XSS-Protection: 1; mode=block'); 
   header("Strict-Transport-Security:max-age=63072000");
    header("X-Frame-Options: SAMEORIGIN");
    header("X-Content-Type-Options: nosniff");
?>
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" http-equiv="Content-Security-Policy" content="width=device-width, initial-scale=1" />
     
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <link rel="pingback" href="http://dannci.wpmasters.org/citygov/xmlrpc.php" /> -->

<!--       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
   
<!--    latest change start-->
 
<!--     latest change end-->
   
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
<!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
   <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<!-- {{asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}} -->
      <!-- <link media="all" href="citygov/wp-content/cache/autoptimize/1/css/autoptimize_95d1c346e5a31a679b70522517e62a7e.css" rel="stylesheet" /> -->

      <link media="all" href="{{ asset('frontend/css/autoptimize_95d1c346e5a31a679b70522517e62a7e.css')}}" rel="stylesheet" />

       <link href="{{ asset('frontend/css/extra.css') }}" rel="stylesheet" />

      <link media="only screen and (max-width: 768px)" href="{{ asset('frontend/css/autoptimize_2e61cf55767583ee8f53abbd2039ead1.css') }}" rel="stylesheet" />

      <title>Wildlife Punjab</title>
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel='preconnect' href='https://fonts.gstatic.com' crossorigin />
      <link rel="alternate" type="application/rss+xml" title="CityGov &raquo; Feed" href="http://dannci.wpmasters.org/citygov/feed/" />
      <link rel="alternate" type="application/rss+xml" title="CityGov &raquo; Comments Feed" href="http://dannci.wpmasters.org/citygov/comments/feed/" />
    
      <link rel='stylesheet' id='opensans-googlefont-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A400&#038;ver=5.6.2' type='text/css' media='all' />

      <!-- <link rel='stylesheet' id='elementor-post-5974-css'  href='citygov/wp-content/cache/autoptimize/1/css/autoptimize_single_2e7e41547cc1ce5f93c6c68fe319d2dd.css' type='text/css' media='all' /> -->
<!-- {{ asset('frontend/css/autoptimize_95d1c346e5a31a679b70522517e62a7e.css')}} -->
      <link rel='stylesheet' id='elementor-post-5974-css'  href="{{ asset('frontend/css/autoptimize_single_2e7e41547cc1ce5f93c6c68fe319d2dd.css')}}" type='text/css' media='all' />

      <link rel='stylesheet' id='elementor-post-20-css'  href="{{ asset('frontend/css/autoptimize_single_15a6cb28ab8893266eeec7fb05672f1d.css') }}" type='text/css' media='all' />

      <link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Nunito%20Sans:200,300,400,600,700,800,900,200italic,300italic,400italic,600italic,700italic,800italic,900italic%7CPoppins:600,400,700,500&#038;subset=latin&#038;display=swap&#038;ver=1604670524" />

      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito%20Sans:200,300,400,600,700,800,900,200italic,300italic,400italic,600italic,700italic,800italic,900italic%7CPoppins:600,400,700,500&#038;subset=latin&#038;display=swap&#038;ver=1604670524" media="print" onload="this.media='all'">
   
      <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.6.2' type='text/css' media='all' />
	
   
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

   
      <script type='text/javascript' src="{{ asset('frontend/wp-includes/js/jquery/jquery.min.js') }}" id='jquery-core-js'></script> <script type='text/javascript' id='cookie-law-info-js-extra'>var Cli_Data = {"nn_cookie_ids":[],"cookielist":[],"non_necessary_cookies":{"necessary":[],"non-necessary":[]},"ccpaEnabled":"","ccpaRegionBased":"","ccpaBarEnabled":"","ccpaType":"gdpr","js_blocking":"","custom_integration":"","triggerDomRefresh":""};
         var cli_cookiebar_settings = {"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":"","button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":"1","button_1_new_win":"","button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":"","button_2_hidebar":"","button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":"1","button_3_new_win":"","button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#62a329","button_4_as_button":"","font_family":"inherit","header_fix":"","notify_animate_hide":"1","notify_animate_show":"","notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":"","scroll_close_reload":"","accept_close_reload":"","reject_close_reload":"","showagain_tab":"","showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":"","show_once":"10000","logging_on":"","as_popup":"","popup_overlay":"1","bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"};
         var log_object = {"ajax_url":"index.html"};
      </script> <script type='text/javascript' id='frontjs-js-extra'>var wpdm_url = {"home":"http:\/\/dannci.wpmasters.org\/citygov\/","site":"http:\/\/dannci.wpmasters.org\/citygov\/","ajax":"index.html"};
         var wpdm_asset = {"spinner":"<i class=\"fas fa-sun fa-spin\"><\/i>"};
      </script> <script type='text/javascript' id='wp-polyfill-js-after'>( 'fetch' in window ) || document.write( '<script src="http://dannci.wpmasters.org/citygov/wp-includes/js/dist/vendor/wp-polyfill-fetch.min.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="http://dannci.wpmasters.org/citygov/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="http://dannci.wpmasters.org/citygov/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="http://dannci.wpmasters.org/citygov/wp-includes/js/dist/vendor/wp-polyfill-url.min.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="http://dannci.wpmasters.org/citygov/wp-includes/js/dist/vendor/wp-polyfill-formdata.min.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="citygov/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min.js"></scr' + 'ipt>' );
         ( "objectFit" in document.documentElement.style ) || document.write( '<script src="{{ asset('frontend/plugins/gutenberg/vendor/object-fit-polyfill.min.f68d8b8b.js')}}"></scr' + 'ipt>' );
      </script> <script type='text/javascript' id='wp-i18n-js-after'>wp.i18n.setLocaleData( { 'text direction\u0004ltr': [ 'ltr' ] }, 'default' );</script> <script type='text/javascript' id='jquery-ui-datepicker-js-after'>jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});</script> <script type='text/javascript' id='wp-dom-ready-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-a11y-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='jquery-ui-autocomplete-js-extra'>var uiAutocompleteL10n = {"noResults":"No results found.","oneResult":"1 result found. Use up and down arrow keys to navigate.","manyResults":"%d results found. Use up and down arrow keys to navigate.","itemSelected":"Item selected."};</script> <script type='text/javascript' id='events-manager-js-extra'>var EM = {"ajaxurl":"index.html","locationajaxurl":"http:\/\/dannci.wpmasters.org\/citygov\/wp-admin\/admin-ajax.php?action=locations_search","firstDay":"1","locale":"en","dateFormat":"dd\/mm\/yy","ui_css":"citygov/wp-content/plugins/events-manager/includes/css/jquery-ui.min.css","show24hours":"0","is_ssl":"","bookingInProgress":"Please wait while the booking is being submitted.","tickets_save":"Save Ticket","bookingajaxurl":"index.html","bookings_export_save":"Export Bookings","bookings_settings_save":"Save Settings","booking_delete":"Are you sure you want to delete?","booking_offset":"30","bb_full":"Sold Out","bb_book":"Book Now","bb_booking":"Booking...","bb_booked":"Booking Submitted","bb_error":"Booking Error. Try again?","bb_cancel":"Cancel","bb_canceling":"Canceling...","bb_cancelled":"Cancelled","bb_cancel_error":"Cancellation Error. Try again?","txt_search":"Search","txt_searching":"Searching...","txt_loading":"Loading...","event_detach_warning":"Are you sure you want to detach this event? By doing so, this event will be independent of the recurring set of events.","delete_recurrence_warning":"Are you sure you want to delete all recurrences of this event? All events will be moved to trash.","disable_bookings_warning":"Are you sure you want to disable bookings? If you do this and save, you will lose all previous bookings. If you wish to prevent further bookings, reduce the number of spaces available to the amount of bookings you currently have","booking_warning_cancel":"Are you sure you want to cancel your booking?"};</script> 
      <link rel="https://api.w.org/" href="http://dannci.wpmasters.org/citygov/wp-json/" />
      <link rel="alternate" type="application/json" href="http://dannci.wpmasters.org/citygov/wp-json/wp/v2/pages/20" />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://dannci.wpmasters.org/citygov/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://dannci.wpmasters.org/citygov/wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 5.6.2" />
      <meta name="generator" content="WooCommerce 4.9.0" />
      <link rel="canonical" href="index.html" />
      <link rel='shortlink' href='index.html' />
      <link rel="alternate" type="application/json+oembed" href="http://dannci.wpmasters.org/citygov/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdannci.wpmasters.org%2Fcitygov%2F" />
      <link rel="alternate" type="text/xml+oembed" href="http://dannci.wpmasters.org/citygov/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdannci.wpmasters.org%2Fcitygov%2F&#038;format=xml" />
      <script>var wpdm_site_url = 'http://dannci.wpmasters.org/citygov/';
         var wpdm_home_url = 'http://dannci.wpmasters.org/citygov/';
         var ajax_url = 'index.html';
         var wpdm_ajax_url = 'index.html';
         var wpdm_ajax_popup = '0';
      </script> 
      <meta name="framework" content="Redux 4.1.24" />
      <noscript>
         <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
      </noscript>
      <link rel="icon" href="{{ asset('frontend/uploads/2018/11/logo-150x150.png')}}" sizes="32x32" />
      <link rel="icon" href="{{ asset('frontend/uploads/2018/11/logo.png') }}" sizes="192x192" />
      <link rel="apple-touch-icon" href="{{ asset('frontend/uploads/2018/11/logo.png') }}" />
      <meta name="msapplication-TileImage" content="{{ asset('frontend/uploads/2018/11/logo.png') }}" />
      <meta name="generator" content="WordPress Download Manager 3.1.12" />
   </head>
   <body class="home page-template page-template-homepage page-template-homepage-php page page-id-20 theme-citygov is-eleslider woocommerce-no-js">
      <div class="upper tmnf_width_normal tmnf-sidebar-active header_default">