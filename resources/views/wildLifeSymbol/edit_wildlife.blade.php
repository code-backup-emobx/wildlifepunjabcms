<?php error_reporting(0); ?>
@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wildlife Symbol of Punjab</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/symbol_of_punjab') }}">WildLife Symbol Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/listing_image_wildife/'.$w_id) }}">WildLife Symbol Images Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update_wildlifesymbol/'.$id.'/'.$w_id) }}">Edit WildLife Symbol Image Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit WildLife Symbols Image Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/listing_image_wildife/'.$w_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> WildLife Symbols Detail List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="{{ url('/update_wildlifesymbol/'.$edit_wildlife->id.'/'.$w_id ) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wildlife Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter WildLife Title " name="title" value="{{ $edit_wildlife->title }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wildlife Description : </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter WildLife Description" name="description" value="{{ $edit_wildlife->description }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wildlife Image : </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" placeholder="Enter WildLife Image" name="image" value="{{ $edit_wildlife->image }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    	<img src="{{ $edit_wildlife->image }}" style="width:20%;height:20%;margin-left: 27%;">
                                    </div>
                                </div>
                                <input type="hidden" value="{{ $w_id }}" name="w_id">
                                <!-- ------------------------------------------ -->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

