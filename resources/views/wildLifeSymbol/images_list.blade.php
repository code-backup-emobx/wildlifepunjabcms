@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">

                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wildlife Symbol of Punjab</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/symbol_of_punjab') }}">WildLife Symbol Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/listing_image_wildife/'.$id) }}">WildLife Symbol Images Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">WildLif Symbol Images Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_more_wildlife_detail/'.$id) }}">
                                    <button type="button" class="btn btn-sm">Add More Detail
                                        <i class="fa fa-plus"></i></button>
                                </a>
                                <a href="{{ url('/symbol_of_punjab') }}">
                                    <button type="button" class="btn btn-sm">Wildlife Symbol List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Image Title </th>
                                                        <th> Image Description </th>
                                                        <th> Image</th>
                                                        <th colspan="2"> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @foreach($list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                       
                                                        <td> {{ $listing->title }} </td>
                                                        <td> {{ $listing->description }} </td>
                                                        <td> <img src="{{ $listing->image }}" style="width:20%;height:20%;"> </td>
                                                        
                                                       

                                                        <td>
                                                            <a href="{{ url('update_wildlifesymbol/'.$listing->id.'/'.$id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a></td><td>
                                                            <a href="{{ url('delete_wildlifesymbol/'.$listing->id.'/'.$id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach 
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    {{ $list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

