@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wildlife Symbol of Punjab</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/symbol_of_punjab') }}">WildLif Symbol Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">WildLife Symbol Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_wildlifesymbol') }}">
                                    <button type="button" class="btn btn-sm">Add Wildlife Sybmol
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> WildLife Symbol Banner </th>
                                                        <th> WildLife Symbol Banner Title </th>
                                                        <th> Total Images</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                    @foreach($wildlifesymbol_list as $wildlifesymbol_listing)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $wildlifesymbol_listing->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $wildlifesymbol_listing->banner_title }} </td>
                                                        
                                                        <td> <a href="{{ url('listing_image_wildife/'.$wildlifesymbol_listing->id) }}"><span class="btn btn-inline btn-primary font-weight-bold">{{ $wildlifesymbol_listing->w_count }}</span>
                                                           
                                                        </td>

                                                        <td>
                                                            <a href="{{ url('update_wildlifesymbol/'.$wildlifesymbol_listing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_wildlifesymbol/'.$wildlifesymbol_listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach 
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    {{ $wildlifesymbol_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

