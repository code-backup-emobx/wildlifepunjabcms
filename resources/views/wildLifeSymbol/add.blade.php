<?php error_reporting(0); ?>
@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add WildLife Symbols</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/symbol_of_punjab') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> WildLife Symbols List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="{{ url('/add_wildlifesymbol') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload WildLife Symbol Banner: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" placeholder="Enter WildLife Title Image" name="banner_image" value="{{ old('banner_image') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Banner Title" name="banner_title" value="{{ old('banner_title') }}">
                                        </div>
                                    </div>
                                </div>


                                <!-- ------------------------------------------ -->
                                  
                                        <div style="background-color: #E5EFF1;padding-bottom: 2%;">
                                        <div><p style="padding:1%;color:black;">Add Image And Title For Wildlife Symbols</p></div>
                                        <div class="card-body" style="margin-left:2%;" >
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Title 1:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Title 1" name="title[]" value="{{ old('title') }}"/>
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Description 1:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Description 1" name="description[]" value="{{ old('description') }}"/>
                                                  
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Image 1:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="file" class="form-control" placeholder="" name="image[]" value="{{ old('image') }}"/>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 2 -->
                                         <div class="card-body" style="margin-left:2%;" >
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Title 2:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Title 2" name="title[]" value="{{ old('title') }}"/>
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Description 2:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Description 2" name="description[]" value="{{ old('description') }}"/>
                                                  
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Image 2:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="file" class="form-control" placeholder="" name="image[]" value="{{ old('image') }}"/>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 3 -->
                                         <div class="card-body" style="margin-left:2%;" >
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Title 3:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Title 3" name="title[]" value="{{ old('title') }}"/>
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Description 3:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Description 3" name="description[]" value="{{ old('description') }}"/>
                                                  
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Image 3:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="file" class="form-control" placeholder="" name="image[]" value="{{ old('image') }}"/>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 4 -->
                                         <div class="card-body" style="margin-left:2%;" >
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Title 4:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Title 4" name="title[]" value="{{ old('title') }}"/>
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Description 4:</label>
                                                    <input type="text" class="form-control" placeholder="Enter Description 4" name="description[]" value="{{ old('description') }}"/>
                                                  
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Image 4:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="file" class="form-control" placeholder="" name="image[]" value="{{ old('image') }}"/>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                   
                                <!-- ------------------------------------------ -->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

