@extends('layouts.login_tmp')
    <!-- begin::Body -->
@section('content')
        <div class="logo">
            <a href="">
                <img src="{{asset('assets/img/campaforest.png')}}" alt="" style="width:100px;"/> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="{{ url('/login') }}" method="post" id="my_form">
                {{ csrf_field() }}
                <h3 class="form-title">Login to your account</h3>
               
                <div class="form-group">
                    <input class="form-control form-control-solid placeholder-no-fix" type="email"  placeholder="Email" name="email"/> 
                </div>
                <div class="form-group">
                   <input class="form-control form-control-solid placeholder-no-fix" type="password"  placeholder="Password" id="pass_show"  /> 
                        
                </div>
            	<input type="hidden" name="password" id="pass_get" value="">
                <div class="form-actions">
                  <!--   <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span></span>
                    </label> -->
              	     <div class="form-group mt-1 mb-3">
                <div class="captcha">
                    <span>{!! captcha_img() !!}</span>
                    <button type="button" class="btn btn-danger" class="refresh-captcha" id="refresh-captcha">
                        &#x21bb;
                    </button>
                </div>
            </div>

            <div class="form-group mb-4">
                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
            </div>
                
                   <button type="submit" name="login_submit" class="btn pulse-btn green pull-right">Login</button>
                

                   
                </div>
               
            </form>
         
        </div>
@endsection

