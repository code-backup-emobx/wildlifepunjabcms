@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                   <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>IMP LINK</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_implink_list/'.$id) }}">List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update_link/'.$id.'/'.$banner_id) }}">Edit Imp Link</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Imp Link</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view_implink_list/'.$banner_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Imp Link List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           

                            <form action="{{ url('/update_link/'.$id.'/'.$banner_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                           
								
                            	   <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Imp Link Logo: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="logo" accept="image/png,image/gif,image/jpeg">
                                        
                                        	<div>
                                    			<img src="{{ $edit_link->logo }}" style="width:100px;height:50%;margin-left:10px;margin-top:5px;">
                                    		</div>
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                            
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Imp Link URL: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter URL of Important Link i.e http://moef.gov.in/en/" name="links" value="{{ $edit_link->links}}" data-toggle="tooltip" data-placement="bottom" title="Enter the url of website for eg: http://moef.gov.in/en/">
                                        </div>
                                    </div>
                                </div>
                              <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Imp Link Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter URL of Important Link Title" name="title" value="{{ $edit_link->title}}">
                                        </div>
                                    </div>
                                </div>

                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

