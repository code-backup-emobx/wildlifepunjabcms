@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wetland Authority</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/wetland_list') }}">List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/wetland_not_listing/'.$w_id) }}">Wetlan Authority Notifications List</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Wetlan Authority Notifications List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('add_noti_authority/'.$w_id) }}">
                                    <button type="button" class="btn btn-sm">Add Notification
                                        <i class="fa fa-plus"></i></button>
                                </a>&nbsp;
                                  <a href="{{ url('wetland_list') }}">
                                    <button type="button" class="btn btn-sm">Wetland List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Notification Text </th>
                                                        <th> Notification Pdf </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                @foreach($noti_listing as $listing)
                                                    <tr>
                                                    	<td>{{ $listing->id }}</td>
                                                        <td>{{ $listing->	notification_text }} </td>
                                                        <td><iframe src="{{ $listing->notification_pdf	 }}"></iframe> </td>

                                                        <td>
                                                           
                                                            <a href="{{ url('edit_wetlandauthority_noti/'.$listing->id.'/'.$w_id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_wetlandauthority_noti/'.$listing->id.'/'.$w_id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>

                                                        </td>
                                                    </tr>
                                                @endforeach	
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    {{ $noti_listing->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

