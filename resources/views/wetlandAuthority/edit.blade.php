@extends('layouts.master')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wetland Authority</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/wetland_list') }}">List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit_wetlandauthority/'.$id) }}">Edit Wetland Authority</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Wetland Authority</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/wetland_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Wetland Authority List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          
                            <form action="{{ url('/edit_wetlandauthority/'.$edit->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" accept="image/*">
                                        </div>
                                        <div>
                                        	<img src="{{ $edit->image }}" style="width:25%;margin-top:3px;margin-left:27%;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{ $edit->title }}" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wetland Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea id="summernote" name="description">{{ $edit->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
   
                              
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </form>
                            <!-- END FORM-->



                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

	<script>
	   $(document).ready(function() {
  		$('#summernote').summernote();
		});
	</script>



	
@endpush
