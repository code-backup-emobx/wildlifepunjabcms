@extends('layouts.master')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Wetland Authority</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/wetland_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Wetland Authority List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('/add_authority ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{old('banner_heading')}}" placeholder="Enter Banner Heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wetland Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea id="summernote" name="description">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="sub_heading_management" style="background-color: #E5EFF1; margin-bottom:10px;">
                                    <div><p style="padding:1%;color:black;">Add Notification</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="sub_heading_management">
                                        <div class="form-group row" >
                                            <label class="col-md-1 col-form-label">Notification Title: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter Notification Title" name="notificationtitle[]" value="{{old('notificationtitle[]')}}">
                                            </div>

                                            <label class="col-md-1 col-form-label">Pdf File: </label>
                                            <div class="col-lg-4">
                                                <input type="file" class="form-control"  name="pdf_file[]" value="{{old('pdf_file[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_sub_heading_management" style="padding-left: 2%;margin-bottom:0%;">
                                    </div>
                                </div>  

                                <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more" value="Add More" style="margin-left:72%;"><b>+</b> Add More Notifications</button>
                                </div>&nbsp; 

                                   <div class="rule_heading" style="background-color: #E5EFF1; margin-bottom:10px;">
                                    <div><p style="padding:1%;color:black;">Add Rules</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="rule_heading">
                                        <div class="form-group row" >
                                            <label class="col-md-1 col-form-label">Heading: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter Notification Title" name="rule_heading[]" value="{{old('rule_heading[]')}}">
                                            </div>

                                            <label class="col-md-1 col-form-label">Pdf File: </label>
                                            <div class="col-lg-4">
                                                <input type="file" class="form-control"  name="rule_pdf[]" value="{{old('rule_pdf[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_rules_" style="padding-left: 2%;margin-bottom:0%;">
                                    </div>
                                </div>  

                                <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more_rules" value="Add More" style="margin-left:72%;"><b>+</b> Add More Rules</button>
                                </div>&nbsp;      
                              
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </form>
                            <!-- END FORM-->



                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

	<script>
	   $(document).ready(function() {
  		$('#summernote').summernote();
		});
	</script>

	<script>
	   $( document ).ready(function() {
	    $("#add_more").click(function(){
	      var html = $("#sub_heading_management").html();
	       $("#add_more_sub_heading_management").append(html);
	  });
	});
	</script>

	<script>
	   $( document ).ready(function() {
	    $("#add_more_rules").click(function(){
	      var html = $("#rule_heading").html();
	       $("#add_more_rules_").append(html);
	  });
	});
	</script>

@endpush
