@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
               <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wetland Authority</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/wetland_list') }}">List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_wetlandauthority/'.$id) }}">View Wetland Authority</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">View Wetlan Authority Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('wetland_list') }}">
                                    <button type="button" class="btn btn-sm">Wetlan Authority lIst
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                                <div class="form-body">
                                      <h5><b><u>Banner Title</u></b></h5><p>{{ $list->title }}</p>
                                      <h5><b><u>Banner image</u></b></h5><p><img src="{{ $list->image }}" style="width:20%;"></p>
                                      <h5><b><u>Description</u></b></h5><div>{!! $list->description !!}</div>
                                     <div class="mt-4"><h4><b><u>Wetland Notification</u></b></h4></div>
                                     @php 
                                     $count =1;
                                     @endphp
                                      @foreach($list->get_notification as $n)
                                      <div>
                                      	<h5><b>{{ $count++ }} {{ $n->notification_text }}</b></h5>
                                      	<p><iframe src="{{$n->notification_pdf }}"></iframe></p>
                                      </div>
                                      @endforeach

                                     <div class="mt-4"><h4><b><u>Wetland Rules</u></b></h4></div>
                                     @php 
                                     $count =1;
                                     @endphp
                                      @foreach($list->get_rules as $n)
                                      <div>
                                      	<h5><b>{{ $count++ }} {{ $n->rule_heading }}</b></h5>
                                      	<p><iframe src="{{$n->rule_pdf }}"></iframe></p>
                                      </div>
                                      @endforeach
                                </div>
                          
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

