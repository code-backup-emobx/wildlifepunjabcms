@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Wetland Authority</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/wetland_list') }}">List</a>
                        </li>
                       
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Wetland Authority List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('add_authority') }}">
                                    <button type="button" class="btn btn-sm">Add Wetlan Authority
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                          
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Banner Heading </th>
                                                        <th> Banner Image </th>
                                                        <th> Notifications  </th>
                                                        <th> Rules </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php
                                                    $check = 0;
                                                    @endphp
                                                @foreach($list as $listing)

                                                <input type="hidden" value="1" id="check_value">
                                                    <tr>
                                                    	<td>{{ $listing->id }}</td>
                                                        <td>{{ $listing->title }} </td>
                                                        <td><img src="{{ $listing->image }}" style="width:118px;"> </td>

                                                        <td><a href="{{ url('wetland_not_listing/'.$listing->id) }}"><span class="btn btn-inline btn-primary font-weight-bold">{{$listing->notification_count}}</span></a></td>

                                                        <td><a href="{{ url('wetland_rules_listing/'.$listing->id) }}"><span class="btn btn-inline btn-primary font-weight-bold">{{ $listing->rule_count}}</span></a></td>

                                                        <td>
                                                           
                                                            <a href="{{ url('edit_wetlandauthority/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('delete_wetlandauthority/'.$listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>

                                                            <a href="{{ url('view_wetlandauthority/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-eye"></i></button></a>

                                                        </td>
                                                    </tr>
                                                @endforeach	
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    {{ $list->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection
@push('page-script')
<script>
$( document ).ready(function() {
   // alert('working');
   var check_value = $("#check_value").val();
   // alert(check_value);
   if(check_value == 1){
    $(".actions").hide();
   }
   else{
    $(".actions").show();

   }
 
});
</script>
@endpush

