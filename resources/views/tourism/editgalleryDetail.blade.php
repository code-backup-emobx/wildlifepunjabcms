@extends('layouts.master')
@section('content')

	<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Tourism </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tourism_detail') }}">Tourism List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-gallery-master/'.$m_id) }}">Tourism Master Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/view_detail/'.$id.'/'.$b_id.'/'.$m_id) }}">Tourism Master Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_gallery/'.$id.'/'.$b_id.'/'.$m_id) }}">Edit Tourism Gallery Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Tourism Gallery Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view_detail/'.$id.'/'.$b_id.'/'.$m_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">Tourism Gallery Detail<i class="fa fa-list"></i> </button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          
                            <form action="{{ url('edit_detail/'.$tourismDetail->id.'/'.$b_id.'/'.$m_id.'/'.$type_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                            

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" 
                                            name="banner_image">
                                        
                                            <div>
                                            	
                                            	<img src="{{ $tourismDetail->banner_image }}" style="width:20%;height:20%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" 
                                            name="banner_heading" value="{{ $tourismDetail->banner_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
										<div class="col-lg-12">
											<label>Description Heading:</label>
											<input type="text" class="form-control" 
                        							name="description_heading" value="{{ $tourismDetail->description_heading }}">
										</div>
									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Description:</label>
                											<textarea name="description" id="editor1">{{ $tourismDetail->description }}</textarea>
                										</div>
                									</div>
                                </div>
                            
                             <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Map Latitude:</label>
                										 <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_lat" value="{{ $tourismDetail->map_lat }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                										</div>
                									</div>
                                </div>
                            
                            	 <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Map Longitude:</label>
                											 <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_long" value="{{ $tourismDetail->map_long }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>

                										</div>
                									</div>
                                </div>
                            	
                            	 <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Zoo Level:</label>
                										  <input type="text" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ $tourismDetail->zoom_level }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>

                										</div>
                									</div>
                                </div>
                            

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>How To Reach:</label>
                											<input type="text" class="form-control" 
                                        name="heading_how_to_reach" value="{{ $tourismDetail->heading_how_to_reach }}">
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>How To Reach Detail:</label>
                											<textarea name="detail_how_to_reach" id="editor2">{{ $tourismDetail->detail_how_to_reach }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Permission Heading:</label>
                  											<input type="text" class="form-control" 
                                          name="heading_permission_required" value="{{ $tourismDetail->heading_permission_required }}">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Permission Description:</label>
                											<textarea 
                											name="detail_required_permmision" id="editor3">{{ $tourismDetail->	detail_required_permmision }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Hire A Guide Heading:</label>
                  											<input type="text" class="form-control" 
                                          name="heading_hire_a_guide" value="{{ $tourismDetail->heading_hire_a_guide }}">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Hire A Guide Heading Description:</label>
                											<textarea name="descripton_hire_guide" id="editor4">{{ $tourismDetail->descripton_hire_guide }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                  									<div class="col-lg-12">
                  										<label>Accommodation Options Heading:</label>
                  										<input type="text" class="form-control" 
                                        name="heading_accommodation_options" value="{{ $tourismDetail->heading_accommodation_options }}">
                  									</div>
                  								</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Accommodation Options Detail:</label>
                											<textarea name="detail_accommodation_options" id="editor5">{{ $tourismDetail->detail_accommodation_options }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Do's Heading:</label>
                  											<input type="text" class="form-control" 
                                           name="heading_do" value="{{ $tourismDetail->heading_do }}">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Do's Detail:</label>
                											<textarea name="detail_do" id="editor7">{{ $tourismDetail->detail_do }}</textarea>
                										</div>
                									</div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Don'ts Heading:</label>
                  											<input type="text" class="form-control" 
                                           name="heading_dont" value="{{ $tourismDetail->heading_dont }}">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                    <div class="col-lg-12">
                                      <label>Don'ts Detail:</label>
                                      <textarea name="detail_dont" id="editor8">{{ $tourismDetail->detail_dont }}</textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                      <div class="col-lg-12">
                                        <label>Heading Best Time Visit:</label>
                                        <input type="text" class="form-control" 
                                           name="heading_best_time_visit" value="{{ $tourismDetail->heading_best_time_visit }}">
                                      </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                    <div class="col-lg-12">
                                      <label>Heading Best Time Visit Detail:</label>
                                      <textarea name="detail_best_time_visit" id="editor9">{{ $tourismDetail->detail_best_time_visit }}</textarea>
                                    </div>
                                  </div>
                                </div>

                               

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
    CKEDITOR.replace('editor1');
</script>
<script>
    CKEDITOR.replace('editor2');
</script>
<script>
    CKEDITOR.replace('editor3');
</script>
<script>
    CKEDITOR.replace('editor4');
</script>
<script>
    CKEDITOR.replace('editor5');
</script>
<script>
    CKEDITOR.replace('editor6');
</script>
<script>
	 CKEDITOR.replace('editor7');
</script>
<script>
   CKEDITOR.replace('editor8');
</script>
<script>
   CKEDITOR.replace('editor9');
</script>


@endpush
