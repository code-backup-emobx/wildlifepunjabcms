@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
              <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Tourism </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tourism_detail') }}">Tourism List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-gallery-master/'.$t_id) }}">Tourism Master Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update-master-detail/'.$id.'/'.$t_id) }}">Edit Tourism Master Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Tourism Master Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view-gallery-master/'.$t_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Tourism Master List<i class="fa fa-list"></i></button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                         

                            <form action="{{ url('/update-master-detail/'.$edit->id.'/'.$t_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                               

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Type : </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="tourism_type_id">
                                           	<option>Select</option>
                                           	@foreach($tourism_type as $list)
                                           		<option value="{{ $list->id }}" @if($edit->tourism_type_id == $list->id) selected @endif>{{ $list->type_name }}</option>
                                           	@endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Image : </label>
                                        <div class="col-lg-6">
                                           <input type="file" name="image" class="form-control" accept="image/*">
                                           <img src="{{ $edit->image }}" style="width:50%;height:50%;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Time : </label>
                                        <div class="col-lg-6">
                                           <input type="text" name="time" class="form-control" value="{{ $edit->time }}">
                                        </div>
                                    </div>
                                </div>

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
