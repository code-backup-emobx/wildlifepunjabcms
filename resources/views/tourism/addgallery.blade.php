@extends('layouts.master')
@section('content')

	<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <div class="page-content-wrapper">
            <div class="page-content">
               <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Tourism </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tourism_detail') }}">Tourism List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-gallery-master/'.$m_id) }}">Tourism Master Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                         <li>
                            <a href="{{ url('/view_detail/'.$id.'/'.$type_id.'/'.$m_id) }}">Tourism Master Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add_gallery/'.$id.'/'.$type_id.'/'.$m_id) }}">Add Tourism Gallery Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Tourism Gallery Detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/view_detail/'.$id.'/'.$type_id.'/'.$m_id) }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Tourism Detail List<i class="fa fa-list"></i></button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           

                            <form action="{{ url('add_gallery/'.$id.'/'.$type_id.'/'.$m_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tourism Type: </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="tourism_type_id">
                                           	<option>Select</option>
                                         	@foreach($tourism_type as $list)
                                         		<option value="{{ $list->id }}">{{ $list->type_name }}</option>
                                         	@endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" 
                                            name="banner_image" value="" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" 
                                            name="banner_heading" value="{{ old('banner_heading') }}" placeholder="Enter Banner Heading">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Description Heading:</label>
                											<input type="text" class="form-control" 
                                        name="description_heading" value="{{ old('description_heading') }}" placeholder="Enter Description Heading">
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Description:</label>
                											<textarea name="description" id="editor1">{{ old('description') }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>How To Reach:</label>
                											<input type="text" class="form-control" 
                                        name="heading_how_to_reach" value="{{ old('heading_how_to_reach') }}" placeholder="Enter How To Reach">
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>How To Reach Detail:</label>
                											<textarea name="detail_how_to_reach" id="editor2">{{ old('detail_how_to_reach') }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Permission Required Heading:</label>
                  											<input type="text" class="form-control" 
                                          name="heading_permission_required" value="{{ old('heading_permission_required') }}" placeholder="Enter Permission Required Heading">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Permission Required Description:</label>
                											<textarea name="detail_required_permmision" id="editor3">{{ old('detail_required_permmision') }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Hire A Guide Heading:</label>
                  											<input type="text" class="form-control" 
                                          name="heading_hire_a_guide" value="{{ old('heading_hire_a_guide') }}" placeholder="Enter Hire A Guide Heading">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Hire A Guide Heading Description:</label>
                											<textarea name="descripton_hire_guide" id="editor4">{{ old('descripton_hire_guide') }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                  									<div class="col-lg-12">
                  										<label>Accommodation Options Heading:</label>
                  										<input type="text" class="form-control" 
                                        name="heading_accommodation_options" value="{{ old('heading_accommodation_options') }}" placeholder="Enter Accommodation Options Heading">
                  									</div>
                  								</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Accommodation Options Detail:</label>
                											<textarea name="detail_accommodation_options" id="editor5">{{ old('detail_accommodation_options') }}</textarea>
                										</div>
                									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                   <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Do's Heading:</label>
                  											<input type="text" class="form-control" 
                                           name="heading_do" value="{{ old('heading_do') }}" placeholder="Enter Do's Heading">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                										<div class="col-lg-12">
                											<label>Do's Detail:</label>
                											<textarea name="detail_do" id="editor7">{{ old('detail_do') }}</textarea>
                										</div>
                									</div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                  										<div class="col-lg-12">
                  											<label>Don'ts Heading:</label>
                  											<input type="text" class="form-control" 
                                           name="heading_dont" value="{{ old('heading_dont') }}" placeholder="Enter Don'ts Heading">
                  										</div>
                  									</div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                    <div class="col-lg-12">
                                      <label>Don'ts Detail:</label>
                                      <textarea name="detail_dont" id="editor8">{{ old('detail_dont') }}</textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                      <div class="col-lg-12">
                                        <label>Heading Best Time Visit:</label>
                                        <input type="text" class="form-control" 
                                           name="heading_best_time_visit" value="{{ old('heading_best_time_visit') }}" placeholder="Enter Heading Best Time Visit">
                                      </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                    <div class="col-lg-12">
                                      <label>Heading Best Time Visit Detail:</label>
                                      <textarea name="detail_best_time_visit" id="editor9">{{ old('detail_best_time_visit') }}</textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                  <div class="form-group row">
                                      <div class="col-lg-12">
                                        <label>Heading Images:</label>
                                        <input type="text" class="form-control" 
                                           name="images_heading" value="{{ old('images_heading') }}" placeholder="Enter Heading Images">
                                      </div>
                                    </div>
                                </div>


                                <div class="form-body geographic_zone" id="add_images" style="padding-left: 5%">
                                   	<div class="card-body">
                  										<div class="form-group row">
                  											<div class="col-lg-6">
                  												<label>Add Image:</label>
                  												<input type="file" class="form-control" 
                  												name="image[]"/>
                  											</div>
                  											<div class="col-lg-6">
                  												<label>Add Image:</label>
                  												<input type="file" class="form-control" 
                  												name="image[]"/>
                  											</div>
                  										</div>
                  									</div>
                                </div>

                                 <div id="add_more_geographic_zone" style="padding-left: 5%;margin-bottom:5%;">
                                    </div>

                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" 
                                    value="Add More" style="float:right;">Add More</button>
                                </div>   

                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
    CKEDITOR.replace('editor1');
</script>
<script>
    CKEDITOR.replace('editor2');
</script>
<script>
    CKEDITOR.replace('editor3');
</script>
<script>
    CKEDITOR.replace('editor4');
</script>
<script>
    CKEDITOR.replace('editor5');
</script>
<script>
    CKEDITOR.replace('editor6');
</script>
<script>
	 CKEDITOR.replace('editor7');
</script>
<script>
   CKEDITOR.replace('editor8');
</script>
<script>
   CKEDITOR.replace('editor9');
</script>
<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
    	// alert('hello');
      var html = $( ".geographic_zone" ).html();
       $("#add_more_geographic_zone").append(html);
  });
});
</script>

@endpush
