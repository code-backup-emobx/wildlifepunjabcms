@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Tourism </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tourism_detail') }}">Tourism List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-gallery-master/'.$m_id) }}">Tourism Master Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view_detail/'.$id.'/'.$type_id.'/'.$m_id) }}"> Tourism Master Detail List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Tourism Master Detail List</span>
                            </div>
                            <div class="actions">
                                <a href="{{url('/add_gallery/'.$id.'/'.$type_id.'/'.$m_id) }}">
                                    <button type="button" class="btn btn-sm">Add More Tourism Detail
                                        <i class="fa fa-plus"></i></button>
                                </a>
                                <a href="{{ url('/view-gallery-master/'.$m_id) }}">
                                    <button type="button" class="btn btn-sm">Tourism Master List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Background Image </th>
                                                        <th> Heading </th>
                                                        <th> Description Heading </th>
                                                        <th> Description </th>
                                                        <th> Heading How To Reach </th>
                                                        <th> Detail How To Reach </th>
                                                        <th> Heading Permission Required </th>
                                                        <th> Detail Permission Required </th>
                                                        <th> Heading Hire A Guide </th>
                                                        <th> Detail Hire A Guide </th>
                                                        <th> Heading Accommodation options </th>
                                                        <th> Detail Accommodation options </th>
                                                        <th> Heading Do </th>
                                                        <th> Detail Do </th>
                                                        <th> Heading Don't </th>
                                                        <th> Detail Don't </th>
                                                        <th> Heading Best Time Visit </th>
                                                        <th> Detail Best Time Visit </th>
                                                        <th>Total Images</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                    @php 
                                                    $count = 1;
                                                    @endphp
                                                 	@if(!empty($tourismDetaillist_))
                                                    @foreach($tourismDetaillist_ as $tourismDetaillist)
                                                    <tr>
                                                        <td> {{ $count++ }} </td>
                                                        <td> <img src="{{ $tourismDetaillist->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $tourismDetaillist->banner_heading }} </td>
                                                        <td> {{ $tourismDetaillist->description_heading }} </td>
                                                        <td> {!! $tourismDetaillist->description !!} </td>
                                                         <td> {{ $tourismDetaillist->heading_how_to_reach }} </td>
                                                         <td> {!! $tourismDetaillist->detail_how_to_reach !!} </td>
                                                         <td> {{ $tourismDetaillist->heading_permission_required }} </td>

                                                         <td> {!! $tourismDetaillist->	detail_required_permmision !!} </td>

                                                         <td> {{ $tourismDetaillist->	heading_hire_a_guide }} </td>

                                                         <td> {!! $tourismDetaillist->	descripton_hire_guide !!} </td>

                                                         <td> {{ $tourismDetaillist->		heading_accommodation_options }} </td>

                                                         <td> {!! $tourismDetaillist->	detail_accommodation_options !!} </td>

                                                         <td> {{ $tourismDetaillist->heading_do }} </td>
                                                         <td> {!! $tourismDetaillist->detail_do !!} </td>
                                                         <td> {{ $tourismDetaillist->heading_dont }} </td>
                                                         <td> {!! $tourismDetaillist->detail_dont !!} </td>

                                                         <td> {{ $tourismDetaillist->heading_best_time_visit }} </td>

                                                         <td> {!! $tourismDetaillist->detail_best_time_visit !!} </td>
                                                         <td><a href="{{ url('detail-images-list/'.$tourismDetaillist->id.'/'.$id.'/'.$m_id) }}"><span class="btn btn-light btn-primary"> {{ $tourismDetaillist->image_count}}</span></a></td>
                                                      
                                                        <td>
                                                           
                                                             <a href="{{url('edit_detail/'.$tourismDetaillist->id.'/'.$id.'/'.$m_id.'/'.$type_id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white; margin-top:3%;"><i class="fa fa-edit"></i> Edit</button></a>

                                                            <a href="{{url('delete-detail/'.$tourismDetaillist->id )}}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>

                                                        </td>
                                                    </tr>
                                                    
                                                    @endforeach
                                                   @endif
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

