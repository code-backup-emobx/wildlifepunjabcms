@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Tourism </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tourism_detail') }}">Tourism List</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Tourism List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_tourism') }}">
                                    <button type="button" class="btn btn-sm">Add Tourism
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Banner Image </th>
                                                        <th> Banner Heading </th>
                                                      
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                  
                                                 	@foreach($tourism_detail as $list)
                                                    <tr>
                                                        <td> {{ $list->id }} </td>
                                                        <td> <img src="{{ $list->banner_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $list->banner_heading }} </td>
                                                      
                                                      
                                                        <td>
                                                           
                                                            <a href="{{url('update_tourism/'.$list->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
                                                            <a href="{{url('delete_tourism/'.$list->id)}}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>

                                                           <!--   <a href="{{url('add_gallery/'.$list->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Add Detail & Images</button></a> -->

                                                               <a href="{{url('view-gallery-master/'.$list->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Master Detail<i class="fa fa-eye"></i></button></a>
                                                            <!--  <a href="{{url('view_detail/'.$list->   tourism_type_id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white; margin-top:3%;"><i class="fa fa-eye"></i> Detail</button></a> -->
                                                           <!--   <a href="{{url('edit_detail/'.$list->   tourism_type_id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white; margin-top:3%;"><i class="fa fa-eye"></i> Detail</button></a> -->
                                                        </td>
                                                    </tr>
                                                	@endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $tourism_detail->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

