@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Tourism </span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/tourism_detail') }}">Tourism List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-gallery-master/'.$id) }}">Tourism Master Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Tourism Master List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-more-tourism/'.$id) }}">
                                    <button type="button" class="btn btn-sm">Add More Tourism
                                        <i class="fa fa-plus"></i></button>
                                </a>
                                 <a href="{{ url('/tourism_detail') }}">
                                    <button type="button" class="btn btn-sm">List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th>Image</th>
                                                        <th> Tourism Type Name </th>
                                                        <th> Time </th>
                                                      
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                  
                                                 	@foreach($list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> <img src="{{ $listing->image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $listing->get_tourism_type->type_name }} </td>
                                                      <td> {{ $listing->time }} </td>
                                                      
                                                        <td>
                                                           
                                                            <a href="{{url('update-master-detail/'.$listing->id.'/'.$id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
                                                            <a href="{{url('delete-master-detail/'.$listing->id)}}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>

                                                                <a href="{{url('view_detail/'.$listing->id.'/'.$listing->tourism_type_id.'/'.$id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-eye"></i> Detail</button></a>

                                                          <!--    <a href="{{url('add_gallery/'.$listing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Add Detail & Images</button></a> -->

                                                               
                                                            <!--  <a href="{{url('view_detail/'.$listing->   tourism_type_id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white; margin-top:3%;"><i class="fa fa-eye"></i> Detail</button></a> -->
                                                           <!--   <a href="{{url('edit_detail/'.$listing->   tourism_type_id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white; margin-top:3%;"><i class="fa fa-eye"></i> Detail</button></a> -->
                                                        </td>
                                                    </tr>
                                                	@endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $list->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

