@extends('frontendlayouts.master')
@section('content')

  
      <div class="upper tmnf_width_normal tmnf-sidebar-active header_default">
      
         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
              
              @if(!empty($home_detail->bg_image))
                <img class="lazyload page-header-img" src='{{ $base_url }}/{{ $home_detail->bg_image }}' data-src="{{ $base_url }}/{{ $home_detail->bg_image }}"/>
              @else
                 <img class="lazyload page-header-img" src='' data-src="" alt="Proposed Downtown District Ordinance"/>
              @endif
                @if(!empty($home_detail))
               <div class="container">
              
                  <h1 class="entry-title">{{ $home_detail->banner_heading }}</h1>
                
               </div>
            </div>
                          <div class="container mb-5">
                        
                            <h3 class="mt-5">{{ $home_detail->title }}</h3>
        					<p>
        						{{ $home_detail->intro_description }}
							</p>
        					<!-- <p>
        						India is unique in terms of biological diversity due to its unique biogeographical location, diversified climatic condition and enormous ecodiversity and
            					geodiversity. India ranks 8th in the list contributing 10% to the world’s Biological Diversity.
            				</p> -->
        
        					<h3 class="mt-3">{{ $home_detail->wild_life_title }}</h3>
        					<p>
        						{{ $home_detail->wild_description }}
        					</p>
        
        					<h5>{{ $home_detail->geographic_statement }}</h5>
                  @endif
        					<ol>
                    @if(!empty($home_detail->home_geographic_zone))
          						@foreach($home_detail->home_geographic_zone as $geographic_list )
  								  	 <li>{{ $geographic_list->geography_zone }}</li>
  							  	  @endforeach
                    @endif  
							</ol>
                         </div>
                       
                     </div>
                  </div>
               </div>
            </div>
		             <div class="clearfix"></div>
		            <div class="clearfix"></div>
		             <div class="m-top">
		    
		             </div>
		        
         </div>
      </div>
    
     
@endsection