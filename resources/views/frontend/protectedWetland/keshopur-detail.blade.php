@extends('frontendlayouts.master')
@section('content')


         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
               <img class="lazyload page-header-img" src="{{ $base_url }}/{{ $keshopur_detail->banner_image }}" data-src="{{ $base_url }}/{{ $keshopur_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $keshopur_detail->banner_heading }}</h1>
               </div>
            </div>

    <div class="container mb-5">
      
        <h3 class="mt-5">{{ $keshopur_detail->title_wildlife }}</h3>
        <p> {{ $keshopur_detail->description }} </p>
      
       <div class="card6">
           <div class="blue">
            <div class="row row1">
                <div class="col-md-4">
                    <b> Location</b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->location }}
                </div>
             </div>
           </div>
           <div class="orange">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Access </b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->access }}
                </div>
             </div>
           </div>
           <div class="blue">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Latitude</b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->latitude }}

                </div>
             </div>
           </div>
           <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Longitude   </b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->longitude }}

                </div>
             </div>
           </div>
           <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Altitude </b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->altitude }}

                </div>
             </div>
           </div>
           <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Flora </b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->flora }} 
                </div>
             </div>
           </div>
           <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Fauna  </b>
                </div>
                <div class="col-md-8">
                    : {{ $keshopur_detail->fauna }} 
                </div>
             </div>
           </div>
            
        </div>
        <h3 class="m-top1"> Reach Us</h3>
        <div class="map m-top1">
            <a href="https://www.google.com/maps/place/Keshopur,+Punjab+143521/@32.0856942,75.3933319,14.75z/data=!4m5!3m4!1s0x391b8db169c9a9dd:0x5ab5c2eb2e6a8bd8!8m2!3d32.0832891!4d75.4024228" target="_blank"> <img src="{{ asset('assets/img/keshopur.png')}}"></a>
        </div>
        
        <h3 class="mt-3">{{ $keshopur_detail->historical_heading }}</h3>
        <p>{{ $keshopur_detail->historical_description }}</p>
   
        
    @if(!empty($keshopur_detail->get_keshopur_images))
     
		<div class="m-top1">
			@foreach($keshopur_detail->get_keshopur_images as $get_images)
			  @if($loop->odd)
				<div class="row row1">
					<div class="col-md-6 padding_between">
						<div class="pattern">
							<img src="{{ asset('assets/img/pattern.png') }}">
							<div class="Wildlife-text">
								<b>{{ $get_images->title }}</b>

							</div>
						</div>
					</div>
					<div class="col-md-6 padding_between">
						<div class="Wetlands">
						<img src="{{ $get_images->image }}">
						</div>
					</div>
				</div>
				@else($loop->even)
				<div class="row row1">
					<div class="col-md-6 padding_between">
						<div class="Wetlands">
							<img src="{{ $get_images->image }}">
						</div>
					</div>
					<div class="col-md-6 padding_between">
						<div class="pattern">
							<img src="{{ asset('assets/img/pattern.png') }}">
							<div class="Wildlife-text">
								<b>{{ $get_images->title }}</b>
							</div>
						</div>
					</div>
				</div>
				@endif
			@endforeach
		<!-- 	<div class="row row1">
				<div class="col-md-6 padding_between">
					<div class="pattern">
						<img src="citygov/wp-content/uploads/2018/09/PATTERN.png">
						<div class="Wildlife-text">
							<b>Pied Kingfisher at Harike Wildlife Sanctuary</b>
						</div>
					</div>
				</div>
				<div class="col-md-6 padding_between">
					<div class="Wetlands">
						<img src="citygov/wp-content/uploads/2018/09/kingfisher.png">
					</div>
				</div>
			</div> -->
		</div>

	@endif

</div>
</div>
</div>
</div>

</div>
    
   
        <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="m-top">
            
             </div>
    </div>
    

 @endsection