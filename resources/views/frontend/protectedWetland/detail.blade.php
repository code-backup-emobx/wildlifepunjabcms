@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         @if($wetland_list)
                          <div class="page-header">
                         <img class="lazyload page-header-img" src="{{ $wetland_list->banner_image }}" data-src="{{ $wetland_list->banner_image }}" alt="Proposed Downtown District Ordinance"/>
                         <div class="container">
                            <h1 class="entry-title">{{ $wetland_list->get_type_name->name }}</h1>
                         </div>
                      </div>
                      @else
                       <div class="page-header">
                         <img class="lazyload page-header-img" src="" data-src="" alt="Proposed Downtown District Ordinance"/>
                         <div class="container">
                            <h1 class="entry-title">No Record Found.....</h1>
                         </div>
                      </div>
                      @endif
@if($wetland_list)
    <div class="container mb-5">
      
        <h3 class="mt-5">Introduction</h3>
        {!! $wetland_list->description !!} 
      
       <div class="card6">
           <div class="blue">
            <div class="row row1">
                <div class="col-md-4">
                    <b> Location</b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->location }}
                </div>
             </div>
           </div>
           <div class="orange">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Access </b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->access }}
                </div>
             </div>
           </div>
           <div class="blue">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Latitude</b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->latitude }}

                </div>
             </div>
           </div>
           <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Longitude   </b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->longitude }}

                </div>
             </div>
           </div>
           <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Altitude </b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->altitude }}

                </div>
             </div>
           </div>
       <div class="blue">
                       <div class="row row1">
                          <div class="col-md-4">
                              <b> Notification Detail </b>
                          </div>
                          
                          <div class="col-md-8">
                               
                             <p class="blue1">
                                  @php
                                $count = 1;
                                @endphp
                                @foreach($wetland_list->get_notification as $notification_listing)
                                
                                {{ $count++ }}.<a href="{{ $notification_listing->pdf_file }}" target="_blank">
                                  <u> {{ $notification_listing->title }}</u>
                                </a></br> 
                                @endforeach
                              </p>
                             
               
                           </div>
                       </div>
                       </div>
           <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Flora </b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->flora }} 
                </div>
             </div>
           </div>
           <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Fauna  </b>
                </div>
                <div class="col-md-8">
                    : {{ $wetland_list->fauna }} 
                </div>
             </div>
           </div>
            
        </div>
        <h3 class="m-top1"> Reach Us</h3>
        @if($wetland_list->zoom_level)
        <div class="map m-top1">
            <!-- <a href="https://www.google.com/maps/search/harike+wildlife+sanctuary+punjab/@31.1581492,75.0067905,12z/data=!3m1!4b1" target="_blank"> <img src="{{ asset('assets/img/bir_harike.png')}}"></a> -->
             <iframe src="https://maps.google.com/maps?q={{ $wetland_list->map_lat }},{{ $wetland_list->map_long }}&z={{ $wetland_list->zoom_level }}&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
        </div>
        @else
    	 <div class="map m-top1">
            <!-- <a href="https://www.google.com/maps/search/harike+wildlife+sanctuary+punjab/@31.1581492,75.0067905,12z/data=!3m1!4b1" target="_blank"> <img src="{{ asset('assets/img/bir_harike.png')}}"></a> -->
             <iframe src="https://maps.google.com/maps?q={{ $wetland_list->map_lat }},{{ $wetland_list->map_long }}&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
        </div>
        @endif

        
        @if($video)
           <h3 class="m-top1"> {{ $video->video_title }} </h3>
            <div class="map mt-3" >
               <iframe src="https://www.youtube.com/embed/{{ $video->video_link }}" allowfullscreen style="display: block;
        width: 100%;
        height: 550px;"></iframe>
            </div>
           
           @endif

           <div class="row">
                <div class="col-md-10"><h3 class="m-top1">Images</h3></div>
                <div class="col-md-2"><a href="{{ url('/protectedwetland_view_all_image/'.$id) }}"><h3 class="m-top1">View All Images</h3></a></div>
              </div>
                       <div class="row m-bottom1">
                        @php
                        $count= 1;
                        @endphp
                        @foreach($images as $image)
                          <div class="col-md-3 m-top1">
                            <img src="{{ $image->images }}" style="width:100%;height:90%;" onclick="openModal();currentSlide('{{ $count++ }}')" class="hover-shadow cursor" >
                          </div>
                        @endforeach  
                         
                        </div>
                  
                    <!-- images modal -->
                <div id="myModal" class="modal">
 
				  <span class="close cursor close_symbol" onclick="closeModal()">&times;</span>
				   
				  <div class="modal-content" id="myModalContent">

				    <!-- <div class="mySlides">
				      <div class="numbertext">1 / 8</div>
				      <img src="citygov/Harike-Photos/Harike-Photos/IMG_3016jk.jpg" style="width:100%">
				    </div> -->
				    @if($images)
				     @php
				     $count = 1;
				     @endphp
				    @foreach($images as $list)
<!-- 				   {{ $list->images}} -->
				      <div class="mySlides">
				        <div class="numbertext"></div>
				        <img src="{{ $list->images }}" style="width: 100%;">
				      </div>
				    @endforeach
				  	@endif   
				    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				    <a class="next" onclick="plusSlides(1)">&#10095;</a>
				  </div>
				</div>
				<!-- images modal -->
                  
        
        <h3 class="mt-3">Historical Importance</h3>
        {!! $wetland_list->history_description !!}
   
        
    @if($wetland_list->get_detail)
     
		<div class="m-top1">
			@foreach($wetland_list->get_detail as $get_images)
			  @if($loop->odd)
				<div class="row row1">
					<div class="col-md-6 padding_between">
						<div class="pattern">
							<img src="{{ asset('assets/img/pattern.png') }}">
							<div class="Wildlife-text">
								<b>{{ $get_images->title }}</b>

							</div>
						</div>
					</div>
					<div class="col-md-6 padding_between">
						<div class="Wetlands">
						<img src="{{ $get_images->images }}">
						</div>
					</div>
				</div>
				@else($loop->even)
				<div class="row row1">
					<div class="col-md-6 padding_between">
						<div class="Wetlands">
							<img src="{{ $get_images->images }}">
						</div>
					</div>
					<div class="col-md-6 padding_between">
						<div class="pattern">
							<img src="{{ asset('assets/img/pattern.png') }}">
							<div class="Wildlife-text">
								<b>{{ $get_images->title }}</b>
							</div>
						</div>
					</div>
				</div>
				@endif
			@endforeach
		<!-- 	<div class="row row1">
				<div class="col-md-6 padding_between">
					<div class="pattern">
						<img src="citygov/wp-content/uploads/2018/09/PATTERN.png">
						<div class="Wildlife-text">
							<b>Pied Kingfisher at Harike Wildlife Sanctuary</b>
						</div>
					</div>
				</div>
				<div class="col-md-6 padding_between">
					<div class="Wetlands">
						<img src="citygov/wp-content/uploads/2018/09/kingfisher.png">
					</div>
				</div>
			</div> -->
		</div>

  @endif
	@endif

</div>

</div>
</div>
</div>

</div>
    
   
        <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="m-top">
            
             </div>
    </div>
    

 @endsection

 @push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src="{{ asset('frontend/plugins/autoptimize/classes/external/js/lazysizes.min.js') }}"></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

 <script type="text/javascript">
    $('#zoo_gallery').change(function(){
      // alert('here');
      var gallery_id = $(this).val();
       // alert(gallery_id);
      var token = '{{ csrf_token() }}';

        if(gallery_id){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('getzoo_image')}}?zoo_gallery="+gallery_id,
           success:function(data){
           if(data){
             // console.log("success");              
            // console.log(data);
             $("#zoo_gallery_images").empty();
              $("#myModalContent").empty();
              $.each(data,function(key,value){
                   var d = key+1;
                   // console.log(value);
                    $("#zoo_gallery_images").append('<div class="col-md-3 m-top1"><img src="'+value+'" style="width:100%;height:100%;" onclick="openModal('+d+');currentSlide('+d+')" class="hover-shadow cursor"></div>');

                    $('#myModalContent').append('<div class="mySlides"><div class="numbertext"></div><img src="'+value+'" style="width:100%;height:70%;"><a class="prev" onclick="plusSlides(-1)">&#10094;</a><a class="next" onclick="plusSlides(1)">&#10095;</a></div>');

              

                   
                    
                });

           }else{
               $("#zoo_gallery_images").html('No record Found');
            }
            
           }
        });
    }


   });
</script>
@endpush


