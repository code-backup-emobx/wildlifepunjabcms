@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
               <img class="lazyload page-header-img" src="{{ $base_url }}/{{ $nangal_santuary->banner_image }}" data-src="{{ $base_url }}/{{ $nangal_santuary->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $nangal_santuary->banner_heading }}</h1>
               </div>
            </div>

    <div class="container mb-5">
      
        <h3 class="mt-5">{{ $nangal_santuary->title_wildlife }}</h3>
        <p> {{ $nangal_santuary->description }} </p>
      
       <div class="card6">
           <div class="blue">
            <div class="row row1">
                <div class="col-md-4">
                    <b> Location</b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->location }}
                </div>
             </div>
           </div>
           <div class="orange">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Access </b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->access }}
                </div>
             </div>
           </div>
           <div class="blue">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Latitude</b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->latitude }}

                </div>
             </div>
           </div>
           <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Longitude   </b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->longitude }}

                </div>
             </div>
           </div>
           <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Altitude </b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->altitude }}

                </div>
             </div>
           </div>
           <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Flora </b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->flora }} 
                </div>
             </div>
           </div>
           <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Fauna  </b>
                </div>
                <div class="col-md-8">
                    : {{ $nangal_santuary->fauna }} 
                </div>
             </div>
           </div>
            
        </div>
        <h3 class="m-top1"> Reach Us</h3>
        <div class="map m-top1">
            <a href="https://www.google.com/maps/place/Harike+Pattan+Bird+Sanctuary/@31.1342056,75.0332113,12z/data=!4m8!1m2!2m1!1sharike+wildlife+sanctuary!3m4!1s0x39198a5e7abe17c5:0xeee7778bc7ed3af6!8m2!3d31.1613313!4d75.0069842" target="_blank"> <img src="{{ asset('assets/img/bir_moti.png')}}"></a>
        </div>
        
        <!-- <h3 class="mt-3">{{ $nangal_santuary->historical_heading }}</h3> -->
        <!-- <p>{{ $nangal_santuary->historical_description }}</p> -->
   
        
    @if(!empty($nangal_santuary->get_nangal_images))
     
		<div class="m-top1">
			@foreach($nangal_santuary->get_nangal_images as $get_images)
			  @if($loop->odd)
				<div class="row row1">
					<div class="col-md-6 padding_between">
						<div class="pattern">
							<img src="{{ asset('assets/img/pattern.png') }}">
							<div class="Wildlife-text">
								<b>{{ $get_images->title }}</b>

							</div>
						</div>
					</div>
					<div class="col-md-6 padding_between">
						<div class="Wetlands">
						<img src="{{ $get_images->image }}">
						</div>
					</div>
				</div>
				@else($loop->even)
				<div class="row row1">
					<div class="col-md-6 padding_between">
						<div class="Wetlands">
							<img src="{{ $get_images->image }}">
						</div>
					</div>
					<div class="col-md-6 padding_between">
						<div class="pattern">
							<img src="{{ asset('assets/img/pattern.png') }}">
							<div class="Wildlife-text">
								<b>{{ $get_images->title }}</b>
							</div>
						</div>
					</div>
				</div>
				@endif
			@endforeach
		<!-- 	<div class="row row1">
				<div class="col-md-6 padding_between">
					<div class="pattern">
						<img src="citygov/wp-content/uploads/2018/09/PATTERN.png">
						<div class="Wildlife-text">
							<b>Pied Kingfisher at Harike Wildlife Sanctuary</b>
						</div>
					</div>
				</div>
				<div class="col-md-6 padding_between">
					<div class="Wetlands">
						<img src="citygov/wp-content/uploads/2018/09/kingfisher.png">
					</div>
				</div>
			</div> -->
		</div>

	@endif

</div>
</div>
</div>
</div>

</div>
    
   
        <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="m-top">
            
             </div>
    </div>
    

 @endsection