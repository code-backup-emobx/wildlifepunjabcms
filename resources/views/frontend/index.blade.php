@extends('frontendlayouts.master')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
<!-- topbar starts--------------- -->
<!----------- top bar ends-------- -->

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                        <!-- banner --------------------------------------- -->
                        <section class="elementor-section elementor-top-section elementor-element elementor-element-3bd08613 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="3bd08613" data-element_type="section">
                           <div class="elementor-container elementor-column-gap-no">
                              <div class="elementor-row">
                                 <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-325cfefc" data-id="325cfefc" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <div class="elementor-element elementor-element-8968ce8 elementor-widget elementor-widget-shortcode" data-id="8968ce8" data-element_type="widget" data-widget_type="shortcode.default">
                                             <div class="elementor-widget-container">
                                                <div class="elementor-shortcode">
                                                   <div class="wpm_eleslider_wrap">
                                                      <noscript><img src="citygov/wp-content/plugins/eleslider/assets/images/tail-spin.svg" width="40" alt=""></noscript>
                                                      <img class="lazyload" src='data:image/svg+xml,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%2040%2026.666666666667%22%3E%3C/svg%3E' data-src="{{ asset('frontend/plugins/eleslider/assets/images/tail-spin.svg ') }}" width="40" alt="">
                                                      <div class="wpm_eleslider loop owl-carousel loading dots_text_">
                                                         <div class="eleinside eleinside_Left" data-dot="Slide 2">
                                                            <a href="" title="Slide 2" >
                                                            @if(!empty($get_banner_one))
                                                               <img width="1920" height="900" src="{{ $base_url }}/{{ $get_banner_one[0]->banner_image }}" data-src="{{ $base_url }}/{{ $get_banner_one[0]->banner_image }}" class="lazyload tranz bg_image wp-post-image" alt="banner is not uploaded" /> 
                                                               @else
                                                                <img width="1920" height="900" src="" data-src="" class="lazyload tranz bg_image wp-post-image" alt="" /> 
                                                            @endif
                                                            </a>
                                                            <div class="eleslideinside tranz ">
                                                               <h1 class="dekoline dekoline_large" style="text-shadow: 2px 2px 8px #000000;">@if($landing_banner_image_text) {{ $landing_banner_image_text->title }} @else @endif</h1>
                                                               <p style="text-shadow: 2px 2px 8px #000000;">@if($landing_banner_image_text) {{ $landing_banner_image_text->subtitle }} @else @endif</p>
                                                            </div>
                                                         </div>
                                                         <div class="eleinside eleinside_Left" data-dot="Slide 1">
                                                            <a href="" title="Slide 1" >
                                                              @if(!empty($get_banner_one))
                                                               <img width="1920" height="900" src="{{ $base_url }}/{{ $get_banner_one[1]->banner_image }}" data-src="{{ $base_url }}/{{ $get_banner_one[1]->banner_image }}" class="lazyload tranz bg_image wp-post-image" alt="" /> 
                                                               @else
                                                                    <img width="1920" height="900" src="" data-src="" class="lazyload tranz bg_image wp-post-image" alt="Banner not uploaded" /> 
                                                               @endif
                                                            </a>
                                                            <div class="eleslideinside tranz ">
                                                               <h1 class="dekoline dekoline_large" style="text-shadow: 2px 2px 8px #000000;">@if($landing_banner_image_text) {{ $landing_banner_image_text->title }} @else @endif</h1>
                                                               <p style="text-shadow: 2px 2px 8px #000000;">@if($landing_banner_image_text) {{ $landing_banner_image_text->subtitle }} @else @endif</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clearfix"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- banner ends- -->
                   
                           <section class="elementor-section elementor-top-section elementor-element elementor-element-1d93501a elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1d93501a" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                           <div class="elementor-container elementor-column-gap-wider">
                              <div class="elementor-row">
                         		
                                 @if($world_list_previous)
                              	
                                 @foreach($world_list_previous as $dayslist)
                                  @if($loop->odd)
                                 <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-32f6784e" data-id="32f6784e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <div class="elementor-element elementor-element-3f5df4d2 elementor-widget elementor-widget-image" data-id="3f5df4d2" data-element_type="widget" data-widget_type="image.default">
                                             <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                 
                                                   <img width="64" height="64" src="{{ $dayslist->image }}" data-src="{{ $dayslist->image }}" class="lazyload attachment-large size-large" alt="" />
                                                </div>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-67d590e1 elementor-widget elementor-widget-heading" data-id="67d590e1" data-element_type="widget" data-widget_type="heading.default">
                                             <div class="elementor-widget-container">
                                                 <h2 class="elementor-heading-title elementor-size-default"> <a href="{{ url('/worldday_detail/'.$dayslist->id) }}">{{ $dayslist->type_name }}</a></h2>
                                                   <h3 style="color:white;">{{ $dayslist->event_date }}&nbsp;{{ $dayslist->month_name }}</h3>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-21f6829c elementor-widget elementor-widget-text-editor" data-id="21f6829c" data-element_type="widget" data-widget_type="text-editor.default">
                                             <div class="elementor-widget-container">
<!--                                                 <div class="elementor-text-editor elementor-clearfix">description2</div> -->
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                @else($loop->even)
                                 <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7a28baf1" data-id="7a28baf1" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <div class="elementor-element elementor-element-28d20957 elementor-widget elementor-widget-image" data-id="28d20957" data-element_type="widget" data-widget_type="image.default">
                                             <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                
                                                   <img width="64" height="64" src="{{ $dayslist->image }}" data-src="{{ $dayslist->image }}" class="lazyload attachment-large size-large" alt="" />
                                                </div>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-3c089bfa elementor-widget elementor-widget-heading" data-id="3c089bfa" data-element_type="widget" data-widget_type="heading.default">
                                             <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><a href="{{ url('/worldday_detail/'.$dayslist->id) }}">{{ $dayslist->type_name }}</a></h2>
                                                 <h3 style="color:white;">{{ $dayslist->event_date }}&nbsp;{{ $dayslist->month_name }}</h3>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-58aad56e elementor-widget elementor-widget-text-editor" data-id="58aad56e" data-element_type="widget" data-widget_type="text-editor.default">
                                             <div class="elementor-widget-container">
<!--                                                 <div class="elementor-text-editor elementor-clearfix">description3</div> -->
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endif
                                  @endforeach
                                 @endif
                                    <!-- --------- -->
                              </div>
                           
                           
                           </div>
                         
                        </section>
                     
					 
                     
        <!--  minister detail           -->
                           <div class="minister-background">
                         <div class="container">
                         
                          	<!-- What news section strats -->
                   
                     	<a href="#"> 
           					<div class="ticker-wrapper-h">
								<div class="heading">What's New</div>
									<ul class="news-ticker-h">
                                    @foreach($whats_new as $list)
        								<li><a href="#">{{ $list->text }}<br><i style="color: #fff" class="fa fa-calendar">&nbsp;{{ $list->date }}</i></a>
            							</li>
                                    @endforeach
									</ul>
        						</div>
          				</a>
                    
						<!-- What news section ends -->
                         
                         
                            <div class="container overflow-hidden">
  <div class="row gx-5" style="padding-bottom: 30px">
  @foreach($minister_and_officials as $list)
    <div class="col-md-4 col-sm-12 minister" style="padding-bottom: 20px">
     <div class="">
         <ul>
        <li class="list_li">
        <img src="{{ $list->image}}">
            <div class="break" style="margin-top:-7px;">
                <h5> {{ $list->name}}</h5>
                <p style="  font-size: 12px;margin-bottom: 0;color: #FFFFFF;margin-left: 22px;">{!! $list->designation !!}<p>
            @if($list->contact)
                <p style="  font-size: 12px;margin-bottom: 0;color: #FFFFFF;margin-left: 22px;">{{ $list->contact}}</p>
            @else
            @endif
               </div>
            </li>
        </ul>
        </div>
    </div>
   
     @endforeach
    
  </div>
</div> 
                             </div>
                         </div>
        <!--  minister detail      -->
                     
  
                         <div class="container">
                             <div class="m-bottom m-top1">
<!--                               <h2 class="text-center">Gallery</h2>-->
                                 <button class="tablink videos" onclick="openPage('Home', this, 'green')" id="defaultOpen">Gallery</button>
                                    <button class="tablink videos" onclick="openPage('News', this, 'green')">Video</button>


                                <div id="Home" class="tabcontent">
<!--                                   <div class="row m-bottom1">
                                         @foreach($zoo_detail as $listing)
                                          @if($loop->odd)
                                          <div class="col-md-4">
                                            <img src="{{ $listing->zoo_image }}">
                                            <p class="text-center"><b>{{ $listing->type_name }}</b></p>
                                          </div>
                                          @endif
                                          @endforeach
                                      
                                        </div> -->
<!--                                      <div class="row">
                                        @foreach($zoo_detail as $listing)
                                          @if($loop->even)
                                          <div class="col-md-4">
                                            <img src="{{ $listing->zoo_image }}">
                                            <p class="text-center"><b>{{ $listing->type_name }}</b></p>
                                          </div>
                                          @endif
                                          @endforeach
                                    </div>  -->
                                    <div class="row m-bottom1">
                           @if(!empty($gallery_images))
                           @foreach($gallery_images as $list)
                            <div class="col-md-3">
                                   <img src="{{ $list->image }}" style="height: 70%;width: 100%;">
                                            <p><b>{{ $list->type_name }}</b></p>
                            </div>
                            @endforeach
                            @endif
                                 
                                    </div>

                                    <div class="m-top1">
                                      <a href="{{ url('view_gallery') }}"><button class="image-custom"><b>View Images</b></button></a>
                                 </div>

                               </div>


                 <div id="News" class="tabcontent">
                        <div class="row">
                           @if(!empty($video_detail))
                           @foreach($video_detail as $list)
                            <div class="col-md-3">
                                    <!-- <video width="100%" poster="https://www.youtube.com/embed/{{ $list->video_link }}" controls>
                                    <source src="https://www.youtube.com/embed/{{ $list->video_link }}" type="video/mp4">
   
                                    </video> -->
                                    <iframe src="https://www.youtube.com/embed/{{ $list->video_link }}" width="100%" allowfullscreen></iframe>
                                        <p class="text-center"><b>{{ $list->video_title }}</b></p>
                            </div>
                            @endforeach
                            @endif
                                 
                                    </div>

                                    <div class="m-top1">
                                      <a href="view_all_videos"><button class="all-image"><b>View All Videos</b></button></a>
                                 </div> 
                  </div>
                             
                                </div>
                         </div>
                         
                     
                        <section class="elementor-section elementor-top-section elementor-element elementor-element-23fcac97 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="23fcac97" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" style="background-image: url('{{ $event_bg_detail->   bg_image }}')">
                           <div class="elementor-container elementor-column-gap-no">
                              <div class="elementor-row">
                                 <div class="elementor-column elementor-col-66 elementor-top-column elementor-element elementor-element-15063aac" data-id="15063aac" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <div class="elementor-element elementor-element-fdce42e elementor-widget elementor-widget-heading" data-id="fdce42e" data-element_type="widget" data-widget_type="heading.default">
                                             <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-xl">Upcoming Events</h2>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-2d39d5af dekoline elementor-widget elementor-widget-heading" data-id="2d39d5af" data-element_type="widget" data-widget_type="heading.default">
                                             <div class="elementor-widget-container">
                                                <h3 class="elementor-heading-title elementor-size-default">Join us in these events!</h3>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-bec6fbf elementor-widget elementor-widget-shortcode" data-id="bec6fbf" data-element_type="widget" data-widget_type="shortcode.default">
                                             <div class="elementor-widget-container">
                                                <div class="elementor-shortcode">
                                                   <div class='css-events-list'>
                                                      <table class="events-table" >
                                                         <tbody>
                                                            @foreach($world_list_upcoming as $event_listing)
                                                            <tr>
                                                               <td class="thumb">
                                                                 
                                                                  <img width="80" height="80" src="{{ $event_listing->image }}" data-src="{{ $event_listing->image }}" class="lazyload attachment-130x130 size-130x130 wp-post-image" alt="Along Pines Run" />
                                                               </td>
                                                               <td class="list_text">
                                                                  <h3><a href="{{ url('/worldday_detail/'.$event_listing->id) }}">{{ $event_listing->type_name }}</a></h3>
                                                                  <p class="event_meta"> <span class="ribbon">{{ $event_listing->event_date }}&nbsp;&nbsp;{{ $event_listing->month_name }}</span><strong>{{ $event_listing->type_name}}</strong></p>
                                                               </td>
                                                            </tr>
                                                            @endforeach
                                                            
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-53cb9c98" data-id="53cb9c98" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <div class="elementor-element elementor-element-ad7e60a elementor-widget elementor-widget-heading" data-id="ad7e60a" data-element_type="widget" data-widget_type="heading.default">
                                             <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-xl">{{ $event_bg_detail->image_heading }}</h2>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
        <!-- become a volunteer section ----------- -->
                       <section class="elementor-section elementor-top-section elementor-element elementor-element-60694fed elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="60694fed" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                           <div class="elementor-container elementor-column-gap-no">
                              <div class="elementor-row">
                                 <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3222d4a2" data-id="3222d4a2" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <section class="elementor-section elementor-inner-section elementor-element elementor-element-1498c132 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1498c132" data-element_type="section">
                                             <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                   <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-f7e49b4" data-id="f7e49b4" data-element_type="column">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                         <div class="elementor-widget-wrap">
                                                            <div class="elementor-element elementor-element-2c7ada81 dekoline elementor-widget elementor-widget-heading" data-id="2c7ada81" data-element_type="widget" data-widget_type="heading.default">
                                                               <div class="elementor-widget-container">
                                                                  @if(!empty($volunter_detail->volunteer_heading))
                                                                     <h2 class="elementor-heading-title elementor-size-xl">{{ $volunter_detail->volunteer_heading }}</h2>
                                                                  @else
                                                                     <h2 class="elementor-heading-title elementor-size-xl"></h2>
                                                                  @endif
                                                               </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-62bd69b9 elementor-widget elementor-widget-heading" data-id="62bd69b9" data-element_type="widget" data-widget_type="heading.default">
                                                               <div class="elementor-widget-container">
                                                                  @if(!empty($volunter_detail->volunteer_title))
                                                                     <h3 class="elementor-heading-title elementor-size-default">{{ $volunter_detail->volunteer_title }}</h3>
                                                                  @else
                                                                     <h3 class="elementor-heading-title elementor-size-default"></h3>
                                                                  @endif
                                                               </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-3b08300c elementor-widget elementor-widget-text-editor" data-id="3b08300c" data-element_type="widget" data-widget_type="text-editor.default">
                                                               <div class="elementor-widget-container">
                                                                  <div class="elementor-text-editor elementor-clearfix">
                                                                     @if(!empty($volunter_detail->volunteer_description ))
                                                                        <p>{{ $volunter_detail->volunteer_description}}</p>
                                                                     @else
                                                                        <p></p>
                                                                     @endif
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <!-- <div class="elementor-element elementor-element-7acdf5 elementor-widget elementor-widget-text-editor" data-id="7acdf5" data-element_type="widget" data-widget_type="text-editor.default">
                                                               <div class="elementor-widget-container">
                                                                  <div class="elementor-text-editor elementor-clearfix">
                                                                     <p>Our city relies on our volunteers for everything from staffing special event, such as Freedom Fest and Merry Main Street, to assisting departments with daily activities, such as shelving library books, filing records or using GIS equipment.</p>
                                                                  </div>
                                                               </div>
                                                            </div> -->
                                                              <div class="elementor-element elementor-element-2d7fbcee elementor-widget elementor-widget-button" data-id="2d7fbcee" data-element_type="widget" data-widget_type="button.default">
                                                               <div class="elementor-widget-container">
                                                                   <div class="elementor-button-wrapper"> <a href="{{ url('how-to-apply') }}" class="elementor-button-link elementor-button elementor-size-sm" role="button"> <span class="elementor-button-content-wrapper"> <span class="elementor-button-icon elementor-align-icon-right"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </span> <span class="elementor-button-text" id="myBtn">How To Apply</span> </span> </a></div>
                                                                  <div id="myModal" class="modal">

  <!-- Modal content -->
                              
                                    <!-- modal content emd  -->
    
                                                               </div>
                                                            </div>
                                                            <!-- ghghghghghghg -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </section>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-326b7547" data-id="326b7547" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                       <div class="elementor-widget-wrap">
                                          <div class="elementor-element elementor-element-42fb8369 elementor-widget elementor-widget-image" data-id="42fb8369" data-element_type="widget" data-widget_type="image.default">
                                             <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                   @if(!empty($volunter_detail->volunter_image_one))
                                                      <img class="lazyload" src="{{ $volunter_detail->volunter_image_one }}" data-src="{{ $volunter_detail->volunter_image_one }}" title="dresden-3681378_1920-edit" alt="Stairs" />
                                                   @else
                                                      <img class="lazyload" src="" data-src="" title="dresden-3681378_1920-edit" alt="Stairs" />
                                                   @endif
                                                </div>
                                             </div>
                                          </div>
                                          <div class="elementor-element elementor-element-1b768d3f elementor-widget elementor-widget-image" data-id="1b768d3f" data-element_type="widget" data-widget_type="image.default">
                                             <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                   @if(!empty($volunter_detail->volunter_image_two))
                                                      <img class="lazyload" src="{{ $volunter_detail->volunter_image_two }}" data-src="{{ $volunter_detail->volunter_image_two }}" title="bodyworn-794111_1920"/>
                                                   @else
                                                      <img class="lazyload" src="" data-src="" title="bodyworn-794111_1920" alt="Become a Volunteer!" />
                                                   @endif
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!----------- become a volunteer section ends -->
                     </div>
                  </div>
               </div>
            </div>
           

        
    
     


    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

<script>

$(document).ready(function (e) {
    $('#save_howtoapply').on('submit',(function(e) {
      // alert('hello');
        e.preventDefault();
        var formData = new FormData(this);
        // console.log(formData);
        // alert(formData);

      
        $.ajax({
            type:'POST',
            url:'/how_to_apply_detail',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log("success");
                console.log(data);

              swal("Your application sent successfully. You'll receive response from wildlife department shortly."); 
              // location.reload();
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));

  
});
</script>

<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>

@endsection