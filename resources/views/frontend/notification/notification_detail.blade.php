@extends('frontendlayouts.master')
@section('content')

    <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
        <div class="elementor-inner">
	        <div class="elementor-section-wrap">
	           <div class="page-header">
				  
				   <img class="lazyload page-header-img" src="{{ $notification_detail->banner_image }}" data-src="{{ $notification_detail->banner_image }}"  alt="Proposed Downtown District Ordinance"/>
				   <div class="container">
				     <h1 class="entry-title">{{ $notification_detail->banner_heading }}</h1>
				   </div>
				</div>
		        <div class="container mb-5">
		            <div class="accordion accordion-toggle-arrow mt-5" id="accordionExample1">
		        	@foreach($notification_detail['get_details'] as $detail_notify)
		                <div class="card">
		                    <div class="card-header">
		                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseone{{ $detail_notify->id }}">
		                          {{ $detail_notify->name }}
		                        </div>
		                    </div>
		                    <div id="collapseone{{ $detail_notify->id }}" class="collapse" data-parent="#accordionExample1">
		                        <div class="card-body">
		                            <div>
				                  		<object data="{{ $detail_notify->pdf_url }}" type="application/pdf" width="500" height="678">

				                    	<iframe src="{{ $detail_notify->pdf_url }}" width="500" height="678"
				                    	></iframe>
				                  </object>
				                	</div>
		                        </div>
		                    </div>
		                </div>
		            @endforeach
		                  
		            </div>
		        </div>     
	        </div>
        </div>
    </div>

<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="m-top">
</div>
</div>
@endsection