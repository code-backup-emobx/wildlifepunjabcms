@extends('frontendlayouts.master')
@section('content')

  
      <div class="upper tmnf_width_normal tmnf-sidebar-active header_default">
      
         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
              
             
                <img class="lazyload page-header-img" src="{{ asset('assets/img/slide1.png') }}" data-src="{{ asset('assets/img/slide1.png')}}"/>
        
             
               <div class="container">
              
                  <h1 class="entry-title"></h1>
                
               </div>
            </div>
                          <div class="container mb-5">
                        
                            <h3 class="mt-2"></h3>

        					
        				<div>
					@include('frontendlayouts.notification')
                                <h4>Select the way to connect</h4>
                                 <form class="contact_form" name="save_howtoapply" action="{{ url('how-to-apply') }}" method="post" novalidate="novalidate">
                                 @csrf

                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{ old('email') }}" pattern="\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b">
                                           
                                 </div>
                                 <div class="form-group">
                                       <label for="formGroupExampleInput">Mobile Number (Optional)</label>
                                       <input type="number" class="form-control" id="mobile_number" placeholder="Enter mobile number" name="mobile_number" value="{{ old('mobile_number') }}">
                                     </div>

                                  <select class="m-top2 form-control" id="connect" name="type_id">
                                  	<option value="">--Select--</option>
                                       @foreach($how_to_apply_type as $listing)
                                          <option value="{{ $listing->id }}">{{ $listing->type_name }}</option>
                                     
                                        @endforeach
                                         
                                   </select>
                                             <!--<p style="font-size: 16px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
												-->
                                             <div class="form-group">
                                                <label for="comment">District:</label>
                                               <select class="form-control" name="district_id">
                                               	<option value="">--Select--</option>
                                                @foreach($district_list as $listing)
                                                  <option value="{{ $listing->id }}">{{ $listing->district_name }}</option>
                                                @endforeach
                                               </select>
                                           </div>

                                        <div class="form-group">
                                          <label for="comment">Description:</label>
                                          <textarea class="form-control" rows="5" id="comment" name="description">{{ old('mobile_number') }}</textarea>
                                        </div>
                                     
                                           <div class="modal-footer">
                                             <input type="submit" class="save-button" value="Save">
                                          </div>
                                           </form>
                                      </div>
        				
        
        				
          
                         </div>
                       
                     </div>
                  </div>
               </div>
            </div>
		             <div class="clearfix"></div>
		            <div class="clearfix"></div>
		             <div class="m-top">
		    
		             </div>
		        
         </div>
      </div>
    
     
@endsection