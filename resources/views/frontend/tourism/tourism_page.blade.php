@extends('frontendlayouts.master')
  
@section('content')


<div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
<div class="elementor-inner">
<div class="elementor-section-wrap">
	<div class="page-header">
		<img class="lazyload page-header-img" src="{{ $tourism_image_detail->banner_image }}" data-src="{{ $tourism_image_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
		<div class="container">
			<h1 class="entry-title">{{ $tourism_image_detail->banner_heading }}</h1>
		</div>
	</div>
<div class="container mb-5">
<div class="row">
	<div class="col-md-8">
		<h3> {{ $tourism_image_detail->description_heading }} </h3>
		{!! $tourism_image_detail->description !!}<span>
    <a href="{{ url('/'.$redirect_url.'/'.$redirect_id) }}"><b style="color: #e8816e;">read more</b></a></span>
		<h3 class="m-top1"> Reach Us</h3>
		<div class="map m-top1">
          @if($tourism_image_detail->zoom_level)
					        	<div class="map mt-3">
					            	<!-- <a href="https://www.google.com/maps/place/Bir+Moti+Bagh,+Punjab+147001/@30.2812319,76.3741471,12.75z/data=!4m8!1m2!2m1!1sbir+moti+bagh+wildlife+sanctuary!3m4!1s0x391029711584e90d:0xf88573a581755ec1!8m2!3d30.2875736!4d76.4013471" target="_blank"> <img src="{{ asset('assets/img/bir_moti.png')}}"></a> -->
					             	<iframe src="https://maps.google.com/maps?q={{ $tourism_image_detail->map_lat }},{{ $tourism_image_detail->map_long }}&z={{ $tourism_image_detail->zoom_level}}&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
					        	</div>
                           @else
                  				 <div class="map mt-3">
					            <!-- <a href="https://www.google.com/maps/place/Bir+Moti+Bagh,+Punjab+147001/@30.2812319,76.3741471,12.75z/data=!4m8!1m2!2m1!1sbir+moti+bagh+wildlife+sanctuary!3m4!1s0x391029711584e90d:0xf88573a581755ec1!8m2!3d30.2875736!4d76.4013471" target="_blank"> <img src="{{ asset('assets/img/bir_moti.png')}}"></a> -->
					             <iframe src="https://maps.google.com/maps?q={{ $tourism_image_detail->map_lat }},{{ $tourism_image_detail->map_long }}&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
					        	</div>
                           @endif
<!-- 			<a href="https://www.google.com/maps/place/Harike+Pattan+Bird+Sanctuary/@31.1342056,75.0332113,12z/data=!4m8!1m2!2m1!1sharike+wildlife+sanctuary!3m4!1s0x39198a5e7abe17c5:0xeee7778bc7ed3af6!8m2!3d31.1613313!4d75.0069842" target="_blank"> <img src="{{ asset('assets/img/harike.png') }}"></a> -->
		</div>
		<!-- <div class="card" style="width: 100%; margin-top: 12px;">
			<div class="card-body">
				<h5 class="card-title">{{ $tourism_image_detail->heading_how_can }}</h5>
				<p class="card-text">{!! $tourism_image_detail->how_can_you_visit !!}</p>
			</div>
		</div> -->
		<div class="card" style="width: 100%; margin-top: 12px;">
          <div class="card-body" style="border: 1px solid #082c45">
            <h5 class="card-title" style="color: #db6e43">{{ $tourism_image_detail->heading_how_to_reach }}</h5>
           
              {!! $tourism_image_detail->detail_how_to_reach !!}
              <!-- 
              <p>Near Railway Station&nbsp;: <b>Firozpur Railway Station</b></p>
              <p>Near Airport&nbsp;: <b>Amritsar</b></p> -->
           
          </div>
        </div>
		<!-- <div class="card" style="width: 100%; margin-top: 12px;">
			<div class="card-body">
				<h5 class="card-title">{{ $tourism_image_detail->heading_how_hire }}</h5>
				<p class="card-text">{!! $tourism_image_detail->how_to_hire !!}</p>
			</div>
		</div> -->
		<div class="card" style="width: 100%; margin-top: 12px;">
          <div class="card-body" style="border: 1px solid #082c45">
            <h5 class="card-title" style="color: #db6e43">{{ $tourism_image_detail->heading_permission_required }}</h5>
           
             {!! $tourism_image_detail->detail_required_permmision !!}
              <!-- <p>DFO&nbsp;: <b>Firozpur</b></p>
              <p>Mobile Number&nbsp;: <b>9965321285</b></p> -->
           
          </div>
        </div>
		<!-- <div class="card" style="width: 100%; margin-top: 12px;">
			<div class="card-body">
				<h5 class="card-title">{{ $tourism_image_detail->accomodation_heading }}</h5>
				<p class="card-text">{!! $tourism_image_detail->accomodation_available !!}</p>
			</div>
		</div> -->
		<div class="card" style="width: 100%; margin-top: 12px;">
          <div class="card-body" style="border: 1px solid #082c45">
            <h5 class="card-title" style="color: #db6e43">{{ $tourism_image_detail->heading_hire_a_guide }}</h5>
           
           {!! $tourism_image_detail->descripton_hire_guide !!}
           
          </div>
        </div>
		<!-- <div class="card" style="width: 100%; margin-top: 12px;">
			<div class="card-body">
				<h5 class="card-title">{{ $tourism_image_detail->heading_does_dont }}</h5>
				<p class="card-text">{!! $tourism_image_detail->do_and_dont !!}</p>
			</div>
		</div> -->
		<div class="card" style="width: 100%; margin-top: 12px;">
          <div class="card-body" style="border: 1px solid #082c45">
            <h5 class="card-title" style="color: #db6e43">{{ $tourism_image_detail->heading_accommodation_options }}</h5>
           
            {!! $tourism_image_detail->detail_accommodation_options !!}
           
          </div>
        </div>
        <div class="card" style="width: 100%; margin-top: 12px;">
          <div class="card-body" style="border: 1px solid #082c45">
            <h5 class="card-title" style="color: #db6e43">{{ $tourism_image_detail->heading_do }}</h5>
              {!! $tourism_image_detail->detail_do !!}
              <hr>
           <h5 class="m-top1" style="color: #db6e43">{{ $tourism_image_detail->heading_dont }}</h5>
              {!! $tourism_image_detail->detail_dont !!} 
          </div>
        </div>

        <div class="card" style="width: 100%; margin-top: 12px;">
          <div class="card-body" style="border: 1px solid #082c45">
            <h5 class="card-title" style="color: #db6e43">{{ $tourism_image_detail->heading_best_time_visit }}</h5>
           
          {!! $tourism_image_detail->detail_best_time_visit !!}
           
          </div>
        </div>
		<!-- <div class="card" style="width: 100%; margin-top: 12px;">
			<div class="card-body">
				<h5 class="card-title">{{ $tourism_image_detail->heading_best_time }}</h5>
				<p class="card-text">{!! $tourism_image_detail->best_time_to_visit !!}</p>
			</div>
		</div> -->

	</div>

	<div class="col-md-4">
                                    <table class="table table-borderless m-top1">
                                          <thead>
                                            <tr>
                                             
                                              <th scope="col">Monday</th>
                                              <th scope="col">9:30am - 05:00pm</th>
                                              
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>Tuesday</td>
                                              <td>09:30am - 05:00pm</td>
                                              </tr>
                                                 <tr>
                                              <td>Wednesday</td>
                                              <td>09:30am - 05:00pm</td>
                                              </tr>
                                                <tr>
                                              <td>Thursday</td>
                                              <td>09:30am - 05:00pm</td>
                                              </tr>
                                               <tr>
                                              <td>Friday</td>
                                              <td>09:30am - 05:00pm</td>
                                              </tr>
                                               <tr>
                                              <td>Saturday</td>
                                              <td>09:30am - 05:00pm</td>
                                              </tr>
                                              <tr>
                                              <td>Sunday</td>
                                              <td>Closed</td>
                                              </tr>
                                            
                                          </tbody>
                                        </table>
                                    
                                    <button class="book-button book-button1">More info</button>
                                    
                                </div>
</div>

<h2 class="m-top1">{{ $tourism_image_detail->images_heading }}</h2>
 @php
	   $count = 1;
	   @endphp
	<div class="row m-top1">
		@if(!empty($tourism_image_detail['get_images']))
			@foreach($tourism_image_detail['get_images'] as $images)
			<div class="col-md-3">
				<img src="{{ $images->image }}" style="width:100%;margin-bottom: 20px;" onclick="openModal();currentSlide('{{ $count++ }}')" class="hover-shadow cursor">
			</div>
			@endforeach
		@endif	
	</div>
	<!-- <div class="row m-top1">
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_2900.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
		</div>
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3601.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
		</div>
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3623JK.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
		</div>
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3953jk.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
		</div>
	</div> -->
<!-- 	<div class="row m-top1">
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3481.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
		</div>
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_4096.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
		</div>
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3480.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
		</div>
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3462.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
		</div>
	</div> -->
	<!-- <div class="row m-top1">
		<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3457JK.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
		</div> -->
	<!-- 	<div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_4816.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
		</div> -->
		<!-- <div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_4370.jpgjk.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
		</div> -->
		<!-- <div class="col-md-3">
			<img src="citygov/Harike-Photos/Harike-Photos/IMG_3265.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
		</div> -->
	<!-- </div> -->


<div id="myModal" class="modal">

	<span class="close cursor close_symbol" onclick="closeModal()">&times;</span>

<div class="modal-content">
	@if(!empty($tourism_image_detail['get_images']))
	   @php
	   $count = 1;
	   @endphp
		@foreach($tourism_image_detail['get_images'] as $images)
		<div class="mySlides">
			<div class="numbertext">{{ $count++ }}./.{{ $total_image_count }}</div>
			<img src="{{ $images->image }}" style="width:100%">
		</div>
		@endforeach
	@endif		

	
<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>


</div>
</div>      
</div>
</div>

</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="m-top">
<script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src="{{ asset('frontend/plugins/autoptimize/classes/external/js/lazysizes.min.js')}}"></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script> <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
         	jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
         		bubbles: true,
         		cancelable: true
         	} );
         } catch ( e ) {
         	jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
         	jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script>
<!-- <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script>  -->
<script defer src="citygov/wp-content/cache/autoptimize/1/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js"></script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script> 
</div>

@endsection

