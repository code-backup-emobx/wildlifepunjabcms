@extends('frontendlayouts.master')
@section('content')


<div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
<div class="elementor-inner">
<div class="elementor-section-wrap">


	<div class="page-header">
		<img class="lazyload page-header-img" src="{{ $banner_detail->banner_image }}" data-src="{{ $banner_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
		<div class="container">
			<h1 class="entry-title">{{ $banner_detail->banner_heading }}</h1>
		</div>
	</div>
	<div class="container mb-5">
		<div class="row">
			@foreach($tourism_detail as $list)
			<div class="col-md-4 mt-5">
				<a href="{{ url('/tourism_page/'.$list->tourism_type_id.'/'.$list->redirect_url.'/'.$list->redirect_id) }}"> <div class="card" style="width:100%">
					<img class="card-img-top" src="{{ $list->image }}" alt="Card image" style="width:100%">
					<div class="card-body">
						<h4 class="card-title">{{ $list['get_tourism_type']->type_name }}</h4>
						<p>{{ $list->time }}</p>

					</div>
					</div>
				</a>
			</div>
			@endforeach
		<!-- 	<div class="col-md-4">
				<a href="Tourism_detail.html">
					<div class="card" style="width:100%">
						<img class="card-img-top" src="citygov/wp-content/uploads/2018/09/tourism2.jpg" alt="Card image" style="width:100%">
						<div class="card-body">
							<h4 class="card-title">Harike wetland and bird sanctuary</h4>
							<p >9:00am - 5:00pm</p>

						</div>
					</div>
				</a>
			</div> -->
		<!-- 	<div class="col-md-4">
				<a href="Tourism_detail.html">
					<div class="card" style="width:100%">
						<img class="card-img-top" src="citygov/wp-content/uploads/2018/09/tourism3.jpg" alt="Card image" style="width:100%">
						<div class="card-body">
							<h4 class="card-title">Rakh bagh park</h4>
							<p>9:00am - 5:00pm</p>

						</div>
					</div>
				</a>
			</div> -->
		</div>

		<!-- <div class="row mt-3">
			<div class="col-md-4">
				<a href="Tourism_detail.html"> 
					<div class="card" style="width:100%">
						<img class="card-img-top" src="citygov/wp-content/uploads/2018/09/tourism1.jpg" alt="Card image" style="width:100%">
						<div class="card-body">
							<h4 class="card-title">Bir Bhuner Heri</h4>
							<p>9:00am - 5:00pm</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="Tourism_detail.html"> 
					<div class="card" style="width:100%">
						<img class="card-img-top" src="citygov/wp-content/uploads/2018/09/tourism2.jpg" alt="Card image" style="width:100%">
						<div class="card-body">
							<h4 class="card-title">Harike wetland and bird sanctuary</h4>
							<p>9:00am - 5:00pm</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="Tourism_detail.html">
					<div class="card" style="width:100%">
						<img class="card-img-top" src="citygov/wp-content/uploads/2018/09/tourism3.jpg" alt="Card image" style="width:100%">
						<div class="card-body">
							<h4 class="card-title">Rakh bagh park</h4>
							<p>9:00am - 5:00pm</p>
						</div>
					</div>
				</a>
			</div>
		</div> -->

	</div>
</div>



</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="m-top">
</div></div>
@endsection