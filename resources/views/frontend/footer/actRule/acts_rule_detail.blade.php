@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       <div class="page-header">
               
               <img class="lazyload page-header-img" src="{{ $act_rule_list[0]->banner_image }}" data-src="{{ $act_rule_list[0]->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                 <h1 class="entry-title">{{ $act_rule_list[0]->banner_heading }}</h1>
                 
               </div>
            </div>
                    <div class="container mb-5">
                    	
                       <div class="card">
                       		<div class="card-body">
                       	@foreach($act_rule_list as $list)
                    	@foreach($list['get_pdf_detail_one'] as $listng_one)
                        		<div class="accordion accordion-toggle-arrow" id="accordionExample1">
                            		<div class="card">
		                                <div class="card-header">
		                                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseone{{ $listng_one->id }}">
		                                       {{ $listng_one->name }}
		                                    </div>
		                                </div>
		                                <div id="collapseone{{ $listng_one->id }}" class="collapse" data-parent="#accordionExample1">
		                                    <div class="card-body">
		                                        <div>
						                            <object
						                                data="{{ $listng_one->pdf_url }}"
						                                type="application/pdf"
						                                width="500"
						                                height="678"
						                              >
					                                <iframe
					                                  src="{{ $listng_one->pdf_url }}"
					                                  width="500"
					                                  height="678"
					                                >
					                                <p>This browser does not support PDF!</p>
					                                </iframe>

		                              				</object>
		                            			</div>
		                                    </div>
		                                </div>
                            		</div>
                                </div>
						@endforeach
						@endforeach
                            </div>
						</div>
                             
                        <div class="card m-top">
                       <div class="card-body">
                        @foreach($act_rule_list as $wildlife_title)
                          @foreach($wildlife_title['get_actRuleHeadings'] as $w)
                        <h2>{{ $w->wildlife_heading }} </h2>
                        
                       <h5>{{ $w->title }}</h5>
                        {!! $w->description !!}
                      @endforeach
                      @endforeach

                          <div class="accordion accordion-toggle-arrow mt-5" id="accordionExample1">
                                  @foreach($act_rule_list as $listing)
                                  @foreach($listing['get_pdf_two'] as $list)
                                 <div class="card">
                                <div class="card-header">
                                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTen{{ $list->id }}">
                                     
                                      {{ $list->name_of_pdf }}
                                    </div>
                                </div>
                                <div id="collapseTen{{ $list->id }}" class="collapse" data-parent="#accordionExample1">
                                    <div class="card-body">
                                        <div>
                              <object data="{{ $list->pdf_link }}" type="application/pdf" width="500" height="678">

                                <iframe src="{{ $list->pdf_link }}" width="500" height="678"
                                >
                                <p>This browser does not support PDF!</p>
                                </iframe>

                              </object>
                            </div>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                                @endforeach
                                
                        </div>
                            </div>
                        </div>
                        
                         <div class="card m-top">
                       <div class="card-body">
                         
                          <h2> Management Plan</h2>

                        @if(!empty($act_rule_list))
                        @foreach($act_rule_list as $heading)
                        @foreach($heading['get_management_headings'] as $headings)

                        <div class="accordion accordion-toggle-arrow mt-2" id="accordionExample1">
                                  
                                 <div class="card">
                                <div class="card-header">
                                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapsem{{ $headings->id }}">
                                     
                                     {{ $headings->managemt_heading }}
                                    </div>
                                </div>
                                <div id="collapsem{{ $headings->id }}" class="collapse" data-parent="#accordionExample1">
                                  <div class="card-body">
                                    <!-- 1 -->
                                      <div class="accordion accordion-toggle-arrow mt-2" id="accordionExample1">
                                  @foreach($headings->getsubheading as $subheadings)
                                 <div class="card mt-2">
                                <div class="card-header">
                                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse_m{{ $subheadings->id }}">
                                     
                                     {{ $subheadings->subheading }}
                                    </div>
                                </div>
                                <div id="collapse_m{{ $subheadings->id }}" class="collapse" data-parent="#accordionExample1">
                                  <div class="card-body">
                                    <div>
                                      <object data="{{ $subheadings->pdf }}" type="application/pdf" width="500" height="678">
                                        <iframe src="{{ $subheadings->pdf }}" width="500" height="678"
                                          >
                                          <p>This browser does not support PDF!</p>
                                          </iframe>
                                      </object>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                                    <!-- 1 -->
                                   
                                  </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endforeach
                        @endif

                         <!-- @foreach($act_rule_list as $list_three)
                         @foreach($list_three['get_pdf_three'] as $three_list)
                         <div class="accordion accordion-toggle-arrow" id="accordionExample1">
                           <div class="card">
                                <div class="card-header">
                                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseone{{ $three_list->id }}">
                                    {{ $three_list->name_management_pdf }} 
                                      
                                    </div>
                                </div>
                                <div id="collapseone{{ $three_list->id }}" class="collapse" data-parent="#accordionExample1">
                                    <div class="card-body">
                                        <div>
                              <object data="{{ $three_list->management_pdf }}" type="application/pdf" width="500"  height="678" >

                                <iframe src="{{ $three_list->management_pdf }}" height="678" >
                                <p>This browser does not support PDF!</p>
                                </iframe>

                              </object>
                            </div>
                                    </div>
                                </div>
                            </div>
                             @endforeach 
                             @endforeach 
                             </div> -->

                                 <!--  @foreach($act_rule_list as $n)
                                  @foreach($n['get_actRulenangalHeadings'] as $nangal)
                                    <p style="margin-top: 20px" > <b>{{ $nangal->management_nangal }}</b></p>
                                  @endforeach
                                  @endforeach
                                <div class="card">
                                    @foreach($act_rule_list as $list_four)
                                    @foreach($list_four['get_actRulenangalpdfHeadings'] as $four_list)
                                <div class="card-header">
                                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree{{ $four_list->id }}">
                                    {{ $four_list->nangal_text }} 
                                    
                                    </div>
                                </div>
                                <div id="collapseThree{{ $four_list->id }}" class="collapse" data-parent="#accordionExample1">
                                    <div class="card-body">
                                        <div>
                              <object
                                data="{{ $four_list->nangal_pdf }}" type="application/pdf" width="500" height="678" >

                                <iframe src="{{ $four_list->nangal_pdf }}" height="678"
                                >
                                <p>This browser does not support PDF!</p>
                                </iframe>

                              </object>
                            </div>
                                    </div>
                                </div>
                                @endforeach
                                @endforeach
                            </div> -->
                             
                             
                        </div>
                        
                             </div>
                        </div>
                        
                        
                    </div>                    
                              
                              
                              
                         </div>
                      </div>

                       
                              
                     </div>
                  </div>
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
            
             </div>
         
    
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src='citygov/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js'></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script> <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
         	jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
         		bubbles: true,
         		cancelable: true
         	} );
         } catch ( e ) {
         	jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
         	jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script> 
<!-- <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script>  -->
<script defer src="citygov/wp-content/cache/autoptimize/1/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js"></script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

      
   @endsection