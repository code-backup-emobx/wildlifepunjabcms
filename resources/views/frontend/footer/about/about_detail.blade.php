@extends('frontendlayouts.master')
@section('content')
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
             
               <img class="lazyload page-header-img" src="{{ $aboutus_detail->banner_image }}" data-src="{{ $aboutus_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $aboutus_detail->banner_heading }}</h1>
               </div>
            </div>
   
                         
                          <div class="container mb-5">
                              <h3> {{ $aboutus_detail['get_footer_about_detail']->title }}</h3>
                              {!! $aboutus_detail['get_footer_about_detail']->description !!}
                             <!--  <b>{{ $aboutus_detail['get_footer_about_detail']->protected_area_heading }}:</b>{!! $aboutus_detail['get_footer_about_detail']->protected_area_description !!} -->
        </div>
                     </div>
                  </div>
               </div>
            </div>
         <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
        	 </div>
		        
         </div>
      </div>
                
@endsection