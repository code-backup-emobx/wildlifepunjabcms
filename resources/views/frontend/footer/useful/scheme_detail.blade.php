@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
            
               <img class="lazyload page-header-img" src="{{ $list_scheme->banner_image }}" data-src="{{ $list_scheme->bannner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $list_scheme->banner_heading }}</h1>
               </div>
            </div>
    <div class="container mb-5">
      
        <table class="m-top">
            <tr>
                <th> Sr. No.</th>
                <th> Name of Scheme</th>
            </tr>
            @php
            $count = 1;
            @endphp
            @foreach($list_scheme['get_scheme_name'] as $name_ofscheme )
             	<tr>
	                <td> {{ $count++ }} </td>
	                <td> {{ $name_ofscheme->name_of_scheme }} </td>
            	</tr>
         	@endforeach
          
            
        </table>
        
    </div>
                     </div>
                  </div>
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
             	 </div>
		        
         </div>
      </div>
                
@endsection