@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
            
               <img class="lazyload page-header-img" src="{{ $tender_list_t1->banner_image }}" data-src="{{ $tender_list_t1->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $tender_list_t1->banner_heading }}</h1>
               </div>
            </div>
   
                         
                          <div class="container mb-5">
                              @foreach($tender_type as $type_list)
                                <button class="tablink" onclick="openPage('{{ $type_list->id }}', this, '#082c45')" id="defaultOpen">{{ $type_list->type_name }}</button>
                                
                              @endforeach

                                <div id="1" class="tabcontent">
                                	
                                	@foreach($tender_list_t1['get_tender_detail_t1'] as $tender_detail)
                                 
                                    <div class="row">
	                                    <div class="col-md-9">
	                                        <h5>{{ $tender_detail->text }}</h5>
	                                    </div>
	                                    <div class="col-md-3">
	                                        <a href="{{ $tender_detail->tender_pdf }}"  target="_blank"><button class="tender-button"><b>View more info of Tender</b></button></a>
	                                    </div>
                                    </div>
                              
                                   
                                    @endforeach
                                    
                                </div>
                                <div id="2" class="tabcontent">

                                    @foreach($tender_list_t2['get_tender_detail_t2'] as $tender_list)
                                      <div class="row">
                                        <div class="col-md-9">
                                            <h5>{{ $tender_list->text }}</h5>
                                        </div>
                                        <div class="col-md-3">
                                             <a href="{{ $tender_list->tender_pdf }}"  target="_blank"><button class="tender-button"><b>View more info of Tender</b></button></a>
                                        </div>
                                      </div>
                                    @endforeach
                                </div>
                  
        
        
                          </div>
                         
                     </div>
                  </div>
               </div>
            </div>
         <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
           
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src='citygov/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js'></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script> <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
         	jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
         		bubbles: true,
         		cancelable: true
         	} );
         } catch ( e ) {
         	jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
         	jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script> 
<!--              <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script> -->
             <script defer src="citygov/wp-content/cache/autoptimize/1/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js"></script>
       
       <script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
      
 
@endsection