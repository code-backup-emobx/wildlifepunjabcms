@extends('frontendlayouts.master')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">


         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
             
               <img class="lazyload page-header-img" src="{{ $service_detail->banner_image }}" data-src="{{ $service_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $service_detail->banner_heading }}</h1>
               </div>
            </div>
   
                         
                          <div class="container mb-5">
                              <h3> {{ $service_detail->title }}</h3>
                              <p> {{ $service_detail->description }}</p>
                             
                                  <div class="m-top1">
                                      <h3> How to Apply</h3>
                                       <div class="card">
                                        <div class="card-body">
                                      <form id="how_to_apply" action="{{ url('/add_services') }}" method="Post">
                                      	@csrf
                                              <div class="form-group">
                                                <label for="formGroupExampleInput">Name</label>
                                                <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="enter name">
                                              </div>
                                           <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" name="email_address" aria-describedby="emailHelp" placeholder="Enter email">
                                           
                                          </div>
                                          <div class="form-group">
                                                <label for="formGroupExampleInput">Mobile Number</label>
                                                <input type="text" class="form-control" id="formGroupExampleInput1" name="mobile_number" placeholder="enter mobile number">
                                              </div>
                                          <div class="form-group">
                                                <label for="formGroupExampleInput">aadhar card Number</label>
                                                <input type="text" class="form-control" id="formGroupExampleInput2" name="aadhar_number" placeholder="aadhar card number">
                                              </div>
                                            <div class="form-group">
                                                <label for="inputAddress">Address 1</label>
                                                <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="address_one">
                                              </div>
                                              <div class="form-group">
                                                <label for="inputAddress2">Address 2</label>
                                                <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor" 
                                                name="address_two">
                                              </div>
                                              <div class="form-row">
                                                <div class="form-group col-md-6">
                                                  <label for="inputCity">City</label>
                                                  <input type="text" class="form-control"  
                                                  name="city" id="inputCity">
                                                </div>
                                               
                                                <div class="form-group col-md-6">
                                                  <label for="inputZip">Zip</label>
                                                  <input type="text" class="form-control" name="zip" id="inputZip">
                                                </div>
                                              </div>
                                            <button type="submit" class="book-button book-button1">Save</button>

                                          <button data-href="/tasks" id="export" class="book-button book-button1" onclick="exportTasks(event.target);" disabled=""><i class="bi bi-download" ></i>Download</button>
                                       <!--  <span data-href="/tasks" id="export" class="book-button book-button1" onclick="exportTasks(event.target);" disabled=""><i class="bi bi-download" ></i>Download</span> -->
                                      </form>
                                           </div>
                                      </div>

                                      
                                  </div>
                       
        
                         </div>
                         
                         
                      
                     </div>
                  </div>
               </div>
            </div>
         <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
          
             </div>
          
           
         </div>
      </div>
    
      <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
      <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
      <noscript>
         <style>.lazyload{display:none;}</style>
      </noscript>
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src='citygov/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js'></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script> <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
         	jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
         		bubbles: true,
         		cancelable: true
         	} );
         } catch ( e ) {
         	jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
         	jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script> 
<!-- <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script>  -->
<script defer src="citygov/wp-content/cache/autoptimize/1/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js"></script>
      
   <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
$(document).ready(function (e) {
    $('#how_to_apply').on('submit',(function(e) {
      // alert('hello');
        e.preventDefault();
        var formData = new FormData(this);
        // console.log(formData);
        // alert(formData);

        $.ajax({
            type:'POST',
            url:'/services_how_to_apply',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log("success");
                console.log(data);

              swal("Successfully Added the Detail"); 
              $('#modalFade').fadeOut();
              document.getElementById("how_to_apply").reset();
              $("#export").prop('disabled', false);
            },
            error: function(data){
                console.log("error");
                console.log(data);
                 swal("Please Fill the Detail"); 
            }
        });
    }));

  
});
</script>

<script>
 
   function exportTasks(_this) {
   	// alert('working');
      let _url = $(_this).data('href');
      window.location.href = _url;
   }

</script>

@endsection