@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                          <div class="page-header">
               					<img class="lazyload page-header-img" src="{{ $base_url }}/{{ $birgurdialpur_detail->banner_image }}" data-src="{{ $base_url }}/{{ $birgurdialpur_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
			               <div class="container">
			                  <h1 class="entry-title">{{ $birgurdialpur_detail->banner_heading }}</h1>
			               </div>
           			 	</div>
					    <div class="container mb-5">
					        <p class="m-top1">{{ $birgurdialpur_detail->description }}</p>
					        <hr>
					      <h3 class="mt-3 mb-3">{{ $birgurdialpur_detail->wildlife_heading }}</h3>
					        <p class="Wildlife">{{ $birgurdialpur_detail->wildlife_title }}</p>
					         <div class="card6">
					            <div class="blue">
						            <div class="row row1">
						                <div class="col-md-4">
						                    <b> District</b>
						                </div>
						                <div class="col-md-8">
						                    : {{ $birgurdialpur_detail->district }}
						                </div>
						            </div>
					            </div>
					            <div class="orange">
						            <div class="row row1">
						                <div class="col-md-4">
						                    <b> Location</b>
						                </div>
						                <div class="col-md-8">
						                    : {{ $birgurdialpur_detail->location }}
						                </div>
						            </div>
					            </div>
					            <div class="blue">
						            <div class="row row1">
						                <div class="col-md-4">
						                    <b> Area</b>
						                </div>
						                <div class="col-md-8">
						                    : {{ $birgurdialpur_detail->area }}
						                </div>
						            </div>
					            </div>
					            <div class="orange">
					            <div class="row row1">
					                <div class="col-md-4">
					                    <b>Status of Land </b>
					                </div>
					                <div class="col-md-8">
					                    : {{ $birgurdialpur_detail->status_of_land }}

					                </div>
					            </div>
					            </div>
					             <div class="blue">
					             <div class="row row1">
					                <div class="col-md-4">
					                    <b> Notification Detail </b>
					                </div>
					                
					                <div class="col-md-8">
					                     
					                  
					                    <p class="blue1">
					                   	    @php
					                   		$count = 1;
					                   		@endphp
					                   		@foreach($birgurdialpur_detail->get_notification_detail as $notification_listing)
					                   		
						                   	{{ $count++ }}.<a href="{{ $notification_listing->pdf_url }}" target="_blank">
						                   		<u>: {{ $notification_listing->notification_title }}</u>
						                   	</a></br> 
						                   	@endforeach
					                    </p>
					                   
					                   
					     
					                 </div>
					             </div>
					             </div>
					             <div class="orange">
					             <div class="row row1">
					                <div class="col-md-4">
					                    <b>Important Fauna </b>
					                </div>
					                <div class="col-md-8">
					                    : {{ $birgurdialpur_detail->important_fauna }}
					                </div>
					             </div>
					             </div>
					             <div class="blue">
					             <div class="row row1">
					                <div class="col-md-4">
					                    <b> Important Flora  </b>
					                </div>
					                <div class="col-md-8">
					                    : {{ $birgurdialpur_detail->important_flora }}
					                </div>
					             </div>
					             </div>
					        </div>
					         <h3 class="m-top1"> Reach Us</h3>
					        <div class="map mt-3">
					            <a href="https://www.google.com/maps/place/Bir+Gurdialpura+Wildlife+Sanctuary/@30.0496632,76.187423,17z/data=!3m1!4b1!4m5!3m4!1s0x3911ceb786aa2503:0xb3745e56e474642a!8m2!3d30.0496632!4d76.1896117" target="_blank"> <img src="{{ asset('assets/img/bir_gurdialpura.png')}}"></a>
					        </div>

					        @php
                            $path = request()->path();
                            $video = App\Models\Video::where('site_link',$path)->first(); 
                            @endphp
                            @if(!empty($video))
                            <h3 class="m-top1"> {{ $video->video_title }} </h3>
                                <div class="map mt-3" >
                                   <iframe src="https://www.youtube.com/embed/{{ $video->video_link }}" allowfullscreen style="display: block;
                            width: 100%;
                            height: 550px;"></iframe>
                                </div>
                            @endif 

					    </div>
                     </div>
                  </div>
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
            
             </div>
         </div>
    
    
     @endsection
    
     