@extends('frontendlayouts.master')
@section('content')

  
      <div class="upper tmnf_width_normal tmnf-sidebar-active header_default">
      
         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
              
             
                <img class="lazyload page-header-img" src="{{ asset('assets/img/slide1.png') }}" data-src="{{ asset('assets/img/slide1.png')}}"/>
        
             
               <div class="container">
              
                  <h1 class="entry-title"></h1>
                
               </div>
            </div>
                          <div class="container mb-5">
                        
                            <h3 class="mt-5"></h3>
        					
        					<p>
                          <h4>Gallery Images</h4>
        
        				<div class="row m-bottom1">
                       
                       	@foreach($images as $image)
                          <div class="col-md-3 m-top1">
                            <img src="{{ $image->image }}" style="width:100%;height:70%;" onclick="openModal();currentSlide('')" class="hover-shadow cursor">
                          </div>
                        @endforeach  
                         
                        </div>
        
        				  <!-- images modal -->
                <div id="myModal" class="modal">
 
				  <span class="close cursor close_symbol" onclick="closeModal()">&times;</span>
				   
				  <div class="modal-content" id="myModalContent">

				    <!-- <div class="mySlides">
				      <div class="numbertext">1 / 8</div>
				      <img src="citygov/Harike-Photos/Harike-Photos/IMG_3016jk.jpg" style="width:100%">
				    </div> -->
				    @if($images)
				     @php
				     $count = 1;
				     @endphp
				    @foreach($images as $list)
				   
				      <div class="mySlides">
				        <div class="numbertext"></div>
				        <img src="{{ $list->image }}" style="width:100%;">
				      </div>
				    @endforeach
				  	@endif   
				    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				    <a class="next" onclick="plusSlides(1)">&#10095;</a>
				  </div>
				</div>
				<!-- images modal -->
          
                         </div>
                       
                     </div>
                  </div>
               </div>
            </div>
		             <div class="clearfix"></div>
		            <div class="clearfix"></div>
		             <div class="m-top">
		    
		             </div>
		        
         </div>
      </div>
    
     
@endsection

 @push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src="{{ asset('frontend/plugins/autoptimize/classes/external/js/lazysizes.min.js') }}"></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

 <script type="text/javascript">
    $('#zoo_gallery').change(function(){
      // alert('here');
      var gallery_id = $(this).val();
       // alert(gallery_id);
      var token = '{{ csrf_token() }}';

        if(gallery_id){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('getzoo_image')}}?zoo_gallery="+gallery_id,
           success:function(data){
           if(data){
             // console.log("success");              
            // console.log(data);
             $("#zoo_gallery_images").empty();
              $("#myModalContent").empty();
              $.each(data,function(key,value){
                   var d = key+1;
                   // console.log(value);
                    $("#zoo_gallery_images").append('<div class="col-md-3 m-top1"><img src="'+value+'" style="width:100%;height:100%;" onclick="openModal('+d+');currentSlide('+d+')" class="hover-shadow cursor"></div>');

                    $('#myModalContent').append('<div class="mySlides"><div class="numbertext"></div><img src="'+value+'" style="width:100%;height:70%;"><a class="prev" onclick="plusSlides(-1)">&#10094;</a><a class="next" onclick="plusSlides(1)">&#10095;</a></div>');

              

                   
                    
                });

           }else{
               $("#zoo_gallery_images").html('No record Found');
            }
            
           }
        });
    }


   });
</script>
@endpush


