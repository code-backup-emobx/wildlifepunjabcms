@extends('frontendlayouts.master')
@section('content')

         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                     	@if($detail)
                          <div class="page-header">
               					<img class="lazyload page-header-img" src="{{ $detail->banner_image }}" data-src="{{ $detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
			               <div class="container">
			                  <h1 class="entry-title">Protected Area</h1>
			               </div>
           			 	</div>
           			 	@else
           			 	<div class="page-header">
               					<img class="lazyload page-header-img" src="" data-src="" alt="Proposed Downtown District Ordinance"/>
			               <div class="container">
			                  <h1 class="entry-title">No record found....</h1>
			               </div>
           			 	</div>
           			 	@endif
           			 	@if($detail)
					    <div class="container mb-5">
					       
					      <h3 class="mt-3 mb-3">{{ $detail->get_cat->category_name }}</h3>
					        <p class="Wildlife">{{ $detail->get_subcat->name }}</p>
                         <div class="m-top1">{!! $detail->description !!}</div>
					        <hr>
					         <div class="card6">
					            <div class="blue">
						            <div class="row row1">
						                <div class="col-md-4">
						                    <b> District</b>
						                </div>
						                <div class="col-md-8">
						                    : {{ $detail->district }}
						                </div>
						            </div>
					            </div>
					            <div class="orange">
						            <div class="row row1">
						                <div class="col-md-4">
						                    <b> Location</b>
						                </div>
						                <div class="col-md-8">
						                    : {{ $detail->location }}
						                </div>
						            </div>
					            </div>
					            <div class="blue">
						            <div class="row row1">
						                <div class="col-md-4">
						                    <b> Area</b>
						                </div>
						                <div class="col-md-8">
						                    : {{ $detail->area }}
						                </div>
						            </div>
					            </div>
					            <div class="orange">
					            <div class="row row1">
					                <div class="col-md-4">
					                    <b>Status of Land </b>
					                </div>
					                <div class="col-md-8">
					                    : {{ $detail->status_of_land }}

					                </div>
					            </div>
					            </div>
					             <div class="blue">
					             <div class="row row1">
					                <div class="col-md-4">
					                    <b> Notification Detail </b>
					                </div>
					                
					                <div class="col-md-8">
					                     
					                   <p class="blue1">
					                   	    @php
					                   		$count = 1;
					                   		@endphp
					                   		@foreach($detail->get_notification as $notification_listing)
					                   		
						                   	{{ $count++ }}.<a href="{{ $notification_listing->pdf_file }}" target="_blank">
						                   		<u> {{ $notification_listing->title }}</u>
						                   	</a></br> 
						                   	@endforeach
					                    </p>
					                   
					     
					                 </div>
					             </div>
					             </div>
					             <div class="orange">
					             <div class="row row1">
					                <div class="col-md-4">
					                    <b>Important Fauna </b>
					                </div>
					                <div class="col-md-8">
					                     {!! $detail->imp_fauna !!}
					                </div>
					             </div>
					             </div>
					             <div class="blue">
					             <div class="row row1">
					                <div class="col-md-4">
					                    <b> Important Flora  </b>
					                </div>
					                <div class="col-md-8">
					                    {!! $detail->imp_flora !!}
					                </div>
					             </div>
					             </div>
					        </div>
                 
					         <h3 class="m-top1"> Reach Us</h3>
                            @if($detail->zoom_level)
					        	<div class="map mt-3">
					            	<!-- <a href="https://www.google.com/maps/place/Bir+Moti+Bagh,+Punjab+147001/@30.2812319,76.3741471,12.75z/data=!4m8!1m2!2m1!1sbir+moti+bagh+wildlife+sanctuary!3m4!1s0x391029711584e90d:0xf88573a581755ec1!8m2!3d30.2875736!4d76.4013471" target="_blank"> <img src="{{ asset('assets/img/bir_moti.png')}}"></a> -->
					             	<iframe src="https://maps.google.com/maps?q={{ $detail->map_lat }},{{ $detail->map_long }}&z={{ $detail->zoom_level}}&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
					        	</div>
                           @else
                  				 <div class="map mt-3">
					            <!-- <a href="https://www.google.com/maps/place/Bir+Moti+Bagh,+Punjab+147001/@30.2812319,76.3741471,12.75z/data=!4m8!1m2!2m1!1sbir+moti+bagh+wildlife+sanctuary!3m4!1s0x391029711584e90d:0xf88573a581755ec1!8m2!3d30.2875736!4d76.4013471" target="_blank"> <img src="{{ asset('assets/img/bir_moti.png')}}"></a> -->
					             <iframe src="https://maps.google.com/maps?q={{ $detail->map_lat }},{{ $detail->map_long }}&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
					        	</div>
                           @endif
                  

					         @if($video)
                            <h3 class="m-top1"> {{ $video->video_title }} </h3>
                                <div class="map mt-3" >
                                   <iframe src="https://www.youtube.com/embed/{{ $video->video_link }}" allowfullscreen style="display: block;
                            width: 100%;
                            height: 550px;"></iframe>
                                </div>
                               
                            @endif

                            	 <div class="row">
                <div class="col-md-10"><h3 class="m-top1">Images</h3></div>
                <div class="col-md-2"><a href="{{ url('/protectedarea_view_all_image/'.$id) }}"><h3 class="m-top1">View All Images</h3></a></div>
              </div>
                       <div class="row m-bottom1">
                       	@php
                       	$count= 1;
                       	@endphp
                       	@foreach($images as $image)
                          <div class="col-md-3 m-top1">
                            <img src="{{ $image->image }}" style="width:100%;height:70%;" onclick="openModal();currentSlide('{{ $count++ }}')" class="hover-shadow cursor">
                          </div>
                        @endforeach  
                         
                        </div>

					    </div>
                     </div>
                  </div>
               </div>
            </div>
            @endif

               <!-- images modal -->
                <div id="myModal" class="modal">
 
				  <span class="close cursor close_symbol" onclick="closeModal()">&times;</span>
				   
				  <div class="modal-content" id="myModalContent">

				    <!-- <div class="mySlides">
				      <div class="numbertext">1 / 8</div>
				      <img src="citygov/Harike-Photos/Harike-Photos/IMG_3016jk.jpg" style="width:100%">
				    </div> -->
				    @if($images)
				     @php
				     $count = 1;
				     @endphp
				    @foreach($images as $list)
				   
				      <div class="mySlides">
				        <div class="numbertext"></div>
				        <img src="{{ $list->image }}" style="width:100%;/* height:445px; *//* object-fit:cover; */">
				      </div>
				    @endforeach
				  	@endif   
				    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				    <a class="next" onclick="plusSlides(1)">&#10095;</a>
				  </div>
				</div>
				<!-- images modal -->
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
            
             </div>
         </div>
    
    
     @endsection
    
     @push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src="{{ asset('frontend/plugins/autoptimize/classes/external/js/lazysizes.min.js') }}"></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

 <script type="text/javascript">
    $('#zoo_gallery').change(function(){
      // alert('here');
      var gallery_id = $(this).val();
       // alert(gallery_id);
      var token = '{{ csrf_token() }}';

        if(gallery_id){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('getzoo_image')}}?zoo_gallery="+gallery_id,
           success:function(data){
           if(data){
             // console.log("success");              
            // console.log(data);
             $("#zoo_gallery_images").empty();
              $("#myModalContent").empty();
              $.each(data,function(key,value){
                   var d = key+1;
                   // console.log(value);
                    $("#zoo_gallery_images").append('<div class="col-md-3 m-top1"><img src="'+value+'" style="width:100%;height:100%;" onclick="openModal('+d+');currentSlide('+d+')" class="hover-shadow cursor"></div>');

                    $('#myModalContent').append('<div class="mySlides"><div class="numbertext"></div><img src="'+value+'" style="width:100%;height:70%;"><a class="prev" onclick="plusSlides(-1)">&#10094;</a><a class="next" onclick="plusSlides(1)">&#10095;</a></div>');

              
                });

           }else{
               $("#zoo_gallery_images").html('No record Found');
            }
            
           }
        });
    }


   });
</script>
@endpush


