@extends('frontendlayouts.master')
@section('content')


         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                    
                       @if($zoo_detail_list)
                        <div class="page-header">
              				<img class="lazyload page-header-img" src="{{ $zoo_detail_list->banner_image }}" data-src="{{ $zoo_detail_list->banner_image }}" alt="Proposed Downtown District Ordinance"/>
			               <div class="container">
			                  <h1 class="entry-title">Zoo</h1>
			               </div>
            			</div>
            			@else
            			<div class="page-header">
              				<img class="lazyload page-header-img" src="" data-src="" alt="Proposed Downtown District Ordinance"/>
			               <div class="container">
			                  <h1 class="entry-title">No Record Found....</h1>
			               </div>
            			</div>

            			@endif

            			@if($zoo_detail_list)

    <div class="container mb-5">
      
        <div class="row">
            <div class="col-md-9">
                <h3 class="mt-5"><b>{{ $zoo_detail_list->get_zoo_name->zoo_type_name }}</b></h3>
            </div>
            <div class="col-md-3">
                <a href="{{ $zoo_detail_list->website_link }}"  target="_blank"><button class="website"><b>Go to Website</b></button></a>
            </div>
        </div>

        <div> {!! $zoo_detail_list->description !!}</div>
        
         <div class="card6">
             <div class="blue">
            <div class="row row1">
                <div class="col-md-4">
                    <b>Location</b>
                </div>
                <div class="col-md-8">
                    : {{ $zoo_detail_list->location }}
                </div>
             </div>
             </div>
             <div class="orange">
              <div class="row row1">
                <div class="col-md-4">
                    <b> Access</b>
                </div>
                <div class="col-md-8">
                    : {{ $zoo_detail_list->access }}
                </div>
             </div>
             </div>
             <div class="blue">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Visiting Days</b>
                </div>
                <div class="col-md-8">
                    : {{ $zoo_detail_list->visiting_days }}

                </div>
             </div>
             </div>
             <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b>Zoo Hours</b> 
                </div>
                <div class="col-md-8">
                    : {{ $zoo_detail_list->zoo_hours }}

                </div>
             </div>
             </div>
             <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Zoo Holidays</b>
                </div>
                <div class="col-md-8">
                    : {{ $zoo_detail_list->zoo_holidays }}

                </div>
             </div>
             </div>
             <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Special Attractions</b>
                </div>
                <div class="col-md-8">
                    : {{ $zoo_detail_list->special_attractions }}
                </div>
             </div>
             </div>
             <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b>Zoo Residents</b>   
                </div>
                <div class="col-md-8">
                    : {!! $zoo_detail_list->zoo_residence !!}
                </div>
             </div>
             </div>
            
        </div>
         <h3 class="m-top1"> Reach Us</h3>
       @if($zoo_detail_list->zoom_level)
        <div class="map mt-3">
            <!-- <a href="https://www.google.com/maps/place/M.C.+Zoological+Park,+Chhat+Bir+Zoo,+Zirakpur/@30.603913,76.7902743,17z/data=!3m1!4b1!4m5!3m4!1s0x390feaeb4ecb5685:0x75150e39c9351b0a!8m2!3d30.603913!4d76.792463" target="_blank"> <img src="{{ asset('assets/img/chatbir_zoo.png')}}"></a> -->
            <!-- <iframe src="https://maps.google.com/maps?q=30.6906669,76.7262366&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe> -->

            <iframe src="https://maps.google.com/maps?q={{ $zoo_detail_list->latitude }},{{ $zoo_detail_list->longitude }}&z={{ $zoo_detail_list->zoom_level}}&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
        </div>
       @else 
    <div class="map mt-3">
            <!-- <a href="https://www.google.com/maps/place/M.C.+Zoological+Park,+Chhat+Bir+Zoo,+Zirakpur/@30.603913,76.7902743,17z/data=!3m1!4b1!4m5!3m4!1s0x390feaeb4ecb5685:0x75150e39c9351b0a!8m2!3d30.603913!4d76.792463" target="_blank"> <img src="{{ asset('assets/img/chatbir_zoo.png')}}"></a> -->
            <!-- <iframe src="https://maps.google.com/maps?q=30.6906669,76.7262366&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe> -->

            <iframe src="https://maps.google.com/maps?q={{ $zoo_detail_list->latitude }},{{ $zoo_detail_list->longitude }}&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" width="360" height="500" allowfullscreen></iframe>
        </div>
    
    @endif

        @if($video)
           <h3 class="m-top1"> {{ $video->video_title }} </h3>
            <div class="map mt-3" >
               <iframe src="https://www.youtube.com/embed/{{ $video->video_link }}" allowfullscreen style="display: block;
        width: 100%;
        height: 550px;"></iframe>
            </div>
           
           @endif

          			<div class="row">
                <div class="col-md-10"><h3 class="m-top1">Zoo Images</h3></div>
          			<div class="col-md-2"><a href="{{ url('/zoo_view_all_image/'.$id) }}"><h3 class="m-top1">View All Images</h3></a></div>
              </div>
                       <div class="row m-bottom1">
                       	@php
                       	$count= 1;
                       	@endphp
                       	@foreach($zoo_images as $images)
                          <div class="col-md-3 m-top1">
                            <img src="{{ $images->zoo_image }}" style="width:100%;height:90%;" onclick="openModal();currentSlide('{{ $count++}}')" class="hover-shadow cursor">
                          </div>
                        @endforeach  
                         
                        </div>
    
      <!-- images modal -->
                <div id="myModal" class="modal">
 
				  <span class="close cursor close_symbol" onclick="closeModal()">&times;</span>
				   
				  <div class="modal-content" id="myModalContent">

				    <!-- <div class="mySlides">
				      <div class="numbertext">1 / 8</div>
				      <img src="citygov/Harike-Photos/Harike-Photos/IMG_3016jk.jpg" style="width:100%">
				    </div> -->
				    @if($zoo_images)
				     @php
				     $count = 1;
				     @endphp
				    @foreach($zoo_images as $images)
<!-- 				   {{ $images->zoo_image}} -->
				      <div class="mySlides">
				        <div class="numbertext"></div>
				        <img src="{{ $images->zoo_image }}" style="width:100%;" >
				      </div>
				    @endforeach
				  	@endif   
				    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				    <a class="next" onclick="plusSlides(1)">&#10095;</a>
				  </div>
				</div>
				<!-- images modal -->

                             <h3 class="m-top1">Zoo Inventory &nbsp;@if($inventory_date){{ ( $inventory_date->date ) }}  @endif </h3>
                       <div class="row m-bottom1">
                       
                          <div class="col-md-12 m-top1">
                                
                            <div class="table-responsive">
                              <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                <thead>
                                  <tr>
                                  <th> Name Of Animal </th>
                                  <th> Quantity </th>
                                  </tr>
                                </thead>
                                <tbody >

                                @foreach($inventory_list as $listing)
                                <tr>
                                  <td> {{ $listing->animal_name }} </td>

                                  <td> {{ $listing->quantity }} </td>
                                
                                </tr>
                                @endforeach
                                </tbody>

                              </table>
                              <div class="d-flex justify-content-center">


                              </div>
                              <!--end::Table-->
                            </div>

                          </div>
                     
                         
                        </div>

        
        
                         </div>
                      </div>
                   </div>
                </div>
                @endif

                <!-- images modal -->
               
				        <!-- images modal -->

    </div>
    
       <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="m-top">
            <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
      <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
      <noscript>
         <style>.lazyload{display:none;}</style>
      </noscript>
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src='citygov/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js'></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script> <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
         	jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
         		bubbles: true,
         		cancelable: true
         	} );
         } catch ( e ) {
         	jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
         	jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script>
<!--             <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script> -->
            <script defer src="citygov/wp-content/cache/autoptimize/1/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js"></script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

      
  @endsection