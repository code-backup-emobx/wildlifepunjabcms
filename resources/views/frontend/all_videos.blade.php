@extends('frontendlayouts.master')
@section('content')

<!-- @php
echo request()->path();
@endphp -->
         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
               @if(!empty($zoogallerybanner_detail->zoo_gallery_banner))
                  <img class="lazyload page-header-img" src="{{  $zoogallerybanner_detail->zoo_gallery_banner }}" data-src="{{  $zoogallerybanner_detail->zoo_gallery_banner }}" alt=""/>
               @else
                  <img class="lazyload page-header-img" src="" data-src="" alt="Proposed Downtown District Ordinance"/>
               @endif
               <div class="container">
                @if(!empty($zoogallerybanner_detail->zoo_gallery_heading))
                  <h1 class="entry-title">{{  $zoogallerybanner_detail->zoo_gallery_heading }}</h1>
                @else
                   <h1 class="entry-title"></h1>
                @endif  
               </div>
            </div>
                          <div class="container mb-5">
                              <div class="row m-top1">
                                  <div class="col-md-6">
                                      <h2>View All Videos</h2>
                                  </div>
                                  <div class="col-md-6">
                                              
                                     
                                      <select class="pull-right" id="zoo_gallery_y" name="zoo_gallery">

                                        <option value="0">All</option>
                                        @foreach($zoo as $listing)
                                          <option value="{{ $listing->id}}">{{ $listing->zoo_type_name }}</option>
                                        @endforeach 
                                        @foreach($protected_area as $list)
                                           <option value="{{ $list->id}}">{{ $list->name }}</option>
                                        @endforeach 
                                        @foreach($protected_wetland as $wetland_list)
                                         <option value="{{ $wetland_list->id}}">{{ $wetland_list->name }}</option>
                                        @endforeach
                                      </select>
                                     
                                  </div>
                              
                              </div>
                              
                            

<!-- <div class="row m-top1" id="zoo_gallery_images">
  <div class="col-md-3">
  @foreach($gallery_list as $listing)
    <img src="{{ $listing->zoo_image }}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
 @endforeach
  </div>
 
</div> -->
<div class=".show_error" style="display:none;">
  <h4>Record Not Found....</h4>
</div>
<div class="row m-top1" id="zoo_gallery_images">
    @if(!empty($gallery_list))
    @php
    $count = 1;
    @endphp
      @foreach($gallery_list as $images)
      <div class="col-md-3">
        <iframe src="https://www.youtube.com/embed/{{ $images->video_link }}" style="width:100%;margin-bottom: 20px;" onclick="openModal();currentSlide('{{ $count++ }}')" class="hover-shadow cursor" allowfullscreen></iframe>
      </div>
      @endforeach
    @endif  
  </div>

  
    
               

<div id="myModal" class="modal">
 
  <span class="close cursor close_symbol" onclick="closeModal()">&times;</span>
   
  <div class="modal-content">

    <!-- <div class="mySlides">
      <div class="numbertext">1 / 8</div>
      <img src="citygov/Harike-Photos/Harike-Photos/IMG_3016jk.jpg" style="width:100%">
    </div> -->
    @if(!empty($gallery_list))
     @php
     $count = 1;
     @endphp
    @foreach($gallery_list as $images)
    <div class="mySlides">
      <div class="numbertext">{{ $count++ }}./.{{ $gallery_count }}</div>
      <iframe src="https://www.youtube.com/embed/{{ $images->video_link }}" style="width:100%"></iframe>
    </div>
    @endforeach
  @endif    

   
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>


  </div>
</div>

                              
 
                          </div>
                              
                              

                         </div>
                       
                       
                      
                     </div>
                  </div>
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
<div class="m-top"></div>
       
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src="{{ asset('frontend/plugins/autoptimize/classes/external/js/lazysizes.min.js') }}"></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script>
       <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("header").style.display = "none";
    document.getElementById("footer").style.display = "none";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
  document.getElementById("header").style.display = "block";
 document.getElementById("footer").style.display = "block";
    
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
 @endsection   
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script type="text/javascript">
    $('#zoo_gallery_y').change(function(){
      // alert('here');
      var gallery_id = $(this).val();
       // alert(gallery_id);
      var token = '{{ csrf_token() }}';

        if(gallery_id){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('get_all_videos')}}?zoo_gallery="+gallery_id,
           success:function(data){
           if(data){
             console.log("success");              
            console.log(data);
             $("#zoo_gallery_images").empty();
              $.each(data,function(key,value){
                    $("#zoo_gallery_images").append('<div class="col-md-3 m-top1"><iframe src="https://www.youtube.com/embed/'+value+'" style="width:100%" onclick="openModal();currentSlide('+key+')" class="hover-shadow cursor"></iframe></div>');

                });

           }else{
               $(".show_error").html('No record Found');
            }
            
           }
        });
    }


   });
</script>


@endpush

