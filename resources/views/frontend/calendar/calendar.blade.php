@extends('frontendlayouts.master')
@section('content')
   <link href="{{ asset('frontend/css/main.css')}}" rel="stylesheet" />

<style>

  
  #calendar {
    max-width: 1000px;
    margin: 0 auto;
  }

</style>


         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
                               <noscript><img class="page-header-img" src="../wp-content/uploads/2018/11/dresden-3681378_1920-edit-2.jpg" alt="Proposed Downtown District Ordinance"/></noscript>
                               <img class="lazyload page-header-img" src='data:image/svg+xml,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%20210%20140%22%3E%3C/svg%3E' data-src="citygov/wp-content/uploads/2018/09/slide1.png" alt="Proposed Downtown District Ordinance"/>
                               <div class="container">
                                  <h1 class="entry-title">Calendar</h1>
                               </div>
                         </div>
                         <div class="container col-md-10">
                       <div id='calendar'></div>    
                     </div>
                    
                         </div>
                   </div>

                </div>
              </div>
        
           </div>
                 

         
       
   
             
                       
                  
   
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
          </div>




          @endsection