@extends('frontendlayouts.master')
@section('content')


   
      <div class="upper tmnf_width_normal tmnf-sidebar-active header_default">
        
         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         <div class="page-header">
			               <img class="lazyload page-header-img" src="{{ $base_url }}/{{ $wildlife_detail->banner_image }}" data-src="{{ $base_url }}/{{ $wildlife_detail->banner_image }}" alt="Proposed Downtown District Ordinance"/>
			               <div class="container">
			                  <h1 class="entry-title">{{ $wildlife_detail->banner_title }}</h1>
			               </div>
            			</div>

				        <div class="container mb-5 mt-2">
				        	
				        		@foreach($wildlife_detail->get_wildlife_symbol_images as $listing)
				        		   @if( $loop->odd )
							        <div class="row row1  mt-0">
							           <div class="col-md-6 padding_between">
							               <div class="pattern">
							            	<img src="{{ asset('assets/img/pattern.png') }}">
							                <div class="Wildlife-text">
							                   <b>{{ $listing->title }}</b><br>{{ $listing->description }}

							                </div>
							               </div>
							           </div>
							         
							           <div class="col-md-6 padding_between">
							                <div class="Wetlands">
							                    <img src="{{ $base_url }}/{{ $listing->image }}">
							                    <!-- <img src="citygov/wp-content/uploads/2018/09/state_animal.png"> -->
							                </div>
							        	</div>
							        </div>

							        @else($loop->even)

						            <div class="row row1">
							            <div class="col-md-6 padding_between">
							             <div class="Wetlands">
							                    <img src="{{ $base_url }}/{{ $listing->image}}">
							                </div>
							            </div>
							            <div class="col-md-6 padding_between">
							                <div class="pattern">
							                    <img src="{{ asset('assets/img/pattern.png') }}">
							                    <div class="Wildlife-text">
							                   <b>{{ $listing->title }}</b><br>{{ $listing->description }}
							                </div>
							               </div>
							        	</div>
						           </div>
						           @endif
						        @endforeach
					           
				        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
        	<div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="m-top">
           
            </div>
                
@endsection