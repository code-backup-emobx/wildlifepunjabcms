@extends('frontendlayouts.master')
@section('content')


         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                          <div class="page-header">
              				<img class="lazyload page-header-img" src="{{ $base_url }}/{{ $ludhiana_zoo->banner_image }}" data-src="{{ $base_url }}/{{ $ludhiana_zoo->banner_image }}" alt="Proposed Downtown District Ordinance"/>
               <div class="container">
                  <h1 class="entry-title">{{ $ludhiana_zoo->banner_heading }}</h1>
               </div>
            </div>

    <div class="container mb-5">
      
        <div class="row">
            <div class="col-md-9">
                 <h3 class="mt-5"><b>{{ $ludhiana_zoo->heading }}</b></h3>
            </div>
            <div class="col-md-3">
                <a href="http://chhatbirzoo.gov.in/"  target="_blank"><button class="website"><b>Go to Website</b></button></a>
            </div>
        </div>

        <p>{{ $ludhiana_zoo->description }}</p>
        
         <div class="card6">
             <div class="blue">
            <div class="row row1">
                <div class="col-md-4">
                    <b>Location</b>
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->location }}
                </div>
             </div>
             </div>
             <div class="orange">
              <div class="row row1">
                <div class="col-md-4">
                    <b> Access</b>
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->access }}
                </div>
             </div>
             </div>
             <div class="blue">
              <div class="row row1">
                <div class="col-md-4">
                    <b>Visiting Days</b>
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->visiting_days }}

                </div>
             </div>
             </div>
             <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b>Zoo Hours</b> 
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->zoo_hours }}

                </div>
             </div>
             </div>
             <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Zoo Holidays</b>
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->zoo_holidays }}

                </div>
             </div>
             </div>
             <div class="orange">
             <div class="row row1">
                <div class="col-md-4">
                    <b> Special Attractions</b>
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->special_attraction }}
                </div>
             </div>
             </div>
             <div class="blue">
             <div class="row row1">
                <div class="col-md-4">
                    <b>Zoo Residents</b>   
                </div>
                <div class="col-md-8">
                    : {{ $ludhiana_zoo->zoo_residence }}
                </div>
             </div>
             </div>
            
        </div>
         <h3 class="m-top1"> Reach Us</h3>
        <div class="map mt-3">
            <a href="https://www.google.com/maps/place/Ludhiana+Zoo/@30.9590687,75.8204768,16z/data=!4m9!1m2!2m1!1sludhiana+Zoo,!3m5!1s0x0:0x703615e4e189e155!8m2!3d30.959069!4d75.8230803!15sCg1sdWRoaWFuYSBab28skgEDem9v" target="_blank"> <img src="{{ asset('assets/img/ludhiana_zoo.png')}}"></a>
        </div>
        
        
                         </div>
                      </div>
                   </div>
                </div>
                
        

    </div>
    
       <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="m-top">
            </div>
    </div>

@endsection