@extends('frontendlayouts.master')
@section('content')


         <div class="wrapper p-border">
            <div class="homebuilder builder">
               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                         
                          <div class="page-header">
               @if(!empty($zoogallerybanner_detail->zoo_gallery_banner))
                  <img class="lazyload page-header-img" src="{{  $zoogallerybanner_detail->zoo_gallery_banner }}" data-src="{{  $zoogallerybanner_detail->zoo_gallery_banner }}" alt=""/>
               @else
                  <img class="lazyload page-header-img" src="" data-src="" alt="Proposed Downtown District Ordinance"/>
               @endif
               <div class="container">
                @if(!empty($zoogallerybanner_detail->zoo_gallery_heading))
                  <h1 class="entry-title">{{  $zoogallerybanner_detail->zoo_gallery_heading }}</h1>
                @else
                   <h1 class="entry-title"></h1>
                @endif  
               </div>
            </div>
                          <div class="container mb-5">
                              <div class="row m-top1">
                                  <div class="col-md-6">
                                      <h2>Images</h2>
                                  </div>
                                  <div class="col-md-6">
                                              
                                     
                                      <select class="pull-right" id="zoo_gallery" name="zoo_gallery">

                                        <option value="0">All</option>
                                        @foreach($zoo_type_detail as $listing)
                                          <option value="{{ $listing->id}}">{{ $listing->zoo_type_name }}</option>
                                        @endforeach 
                                      @foreach($protectedAreaSubcategory as $list)
                                        <option value="{{ $list->id}}">{{ $list->name }}</option>
                                      @endforeach
                                       @foreach($protectedWetland as $list_)
                                        <option value="{{ $list_->id}}">{{ $list_->name }}</option>
                                      @endforeach
                                      </select>
                                     
                                  </div>
                              
                              </div>
                              
                            

<!-- <div class="row m-top1" id="zoo_gallery_images">
  <div class="col-md-3">
  @foreach($gallery_list as $listing)
    <img src="{{ $listing->zoo_image }}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
 @endforeach
  </div>
 
</div> -->

<div class="row m-top1" id="zoo_gallery_images">
    @if(!empty($gallery_list))
    @php
    $count = 1;
    @endphp
      @foreach($gallery_list as $images)
      <div class="col-md-3 m-top1">
        <img src="{{ $images->image }}" style="width:100%;margin-bottom: 20px;height:100%;" onclick="openModal();currentSlide('{{ $count++ }}')" class="hover-shadow cursor">
      </div>
      @endforeach
    @endif  
  </div>

  
    
               

<div id="myModal" class="modal">
 
  <span class="close cursor close_symbol" onclick="closeModal()">&times;</span>
   
  <div class="modal-content" id="myModalContent">

    <!-- <div class="mySlides">
      <div class="numbertext">1 / 8</div>
      <img src="citygov/Harike-Photos/Harike-Photos/IMG_3016jk.jpg" style="width:100%">
    </div> -->
    @if(!empty($gallery_list))
     @php
     $count = 1;
     @endphp
    @foreach($gallery_list as $images)
   
      <div class="mySlides">
        <div class="numbertext">{{ $count++ }}./.{{ $gallery_count }}</div>
        <img src="{{ $images->image }}" style="width: 100%;" >
      </div>
    
    @endforeach
  @endif    

   
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>


  </div>
</div>

                              
 
                          </div>
                              
                              

                         </div>
                       
                       
                      
                     </div>
                  </div>
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
           
      
@endsection
@push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
            


@endpush


