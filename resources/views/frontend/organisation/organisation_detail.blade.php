@extends('frontendlayouts.master')
@section('content')


               <div data-elementor-type="wp-post" data-elementor-id="20" class="elementor elementor-20" data-elementor-settings="[]">
                  <div class="elementor-inner">
                     <div class="elementor-section-wrap">
                       
                          <div class="page-header">
               					@if($data['tab_1'])
                            <img class="lazyload page-header-img" src="{{ $data['tab_1']->banner_image }}" data-src="{{ $data['tab_1']->banner_image }}" alt="Proposed Downtown District Ordinance"/>
                           <div class="container">
                              <h1 class="entry-title">{{ $data['tab_1']->banner_heading }}</h1>
                           </div>
                          @endif
                        </div>
                          <div class="container mb-5">
                             
                             <div class="tab m-top1">
                              @foreach($organisation_tab_detail as $tab_names)
                                  <button class="tablinks active" onclick="openCity(event, '{{ $tab_names->id }}')" id="defaultOpen">{{ $tab_names->tab_name}} </button>
                                @endforeach  
                             </div>

                             <div id="1" class="tabcontent">
                                  <h3>     @if(!empty($data['tab_1']['get_detail_t1'][0]->chart_heading))
                                    {{ $data['tab_1']['get_detail_t1'][0]->chart_heading }}
                                    @endif</h3>
                                 <div style="overflow-x:auto;">
                                  <table>
                                  @if($data['table_count_1'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the Officer/ Official</th>
                                        <th>Name of the Officer/ Official</th>
                                        <th>Range </th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                    
                                  </thead>
                                  @endif
                                 
                                      @foreach($data['tab_1']['get_detail_t1'] as $tab1_list)
                                      @if($tab1_list->type_of_table == 'table_data')
                                        
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab1_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab1_list->name_of_the_officer }}</td>
                                          <td>{{ $tab1_list->range }}</td>
                                          @if(!empty( $tab1_list->block ))
                                            <td>{{ $tab1_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab1_list->beat ))
                                            <td>{{ $tab1_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab1_list->territorial_range_beat_block ))
                                            <td>{{ $tab1_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab1_list->officer_mobile_no }}</td>
<!--                                         <td>@if($tab1_list->image)<img src="{{ $tab1_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                      
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab1_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab1_list->name_of_the_officer }}</td>
                                          <td>{{ $tab1_list->range }}</td>
                                          @if(!empty( $tab1_list->block ))
                                            <td>{{ $tab1_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab1_list->beat ))
                                            <td>{{ $tab1_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab1_list->territorial_range_beat_block ))
                                            <td>{{ $tab1_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab1_list->officer_mobile_no }}</td>
<!--                                        <td>@if($tab1_list->image)<img src="{{ $tab1_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                        @endif
                                      @endif
                                        @endforeach
                                     
                                </table>
                                    <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_1']['get_detail_t1'] as $tab1_list)
                                     @if($tab1_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab1_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                 </div>
                             </div>

                            
                          
                              <div id="2" class="tabcontent ">
                                  <h3> @if(!empty($data['tab_2']['get_detail_t2'][0]->chart_heading))
                                    {{ $data['tab_2']['get_detail_t2'][0]->chart_heading }}
                                    @endif</h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_2'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_2']['get_detail_t2'] as $tab2_list)

                                      @if($tab2_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab2_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab2_list->name_of_the_officer }}</td>
                                          <td>{{ $tab2_list->range }}</td>
                                          @if(!empty( $tab2_list->block ))
                                            <td>{{ $tab2_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab2_list->beat ))
                                            <td>{{ $tab2_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab2_list->territorial_range_beat_block ))
                                            <td>{{ $tab2_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab2_list->officer_mobile_no }}</td>
<!--                                         <td>@if($tab2_list->image)<img src="{{ $tab2_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                      <div><img src="{{ $tab2_list->image }}"></div>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab2_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab2_list->name_of_the_officer }}</td>
                                          <td>{{ $tab2_list->range }}</td>
                                          @if(!empty( $tab2_list->block ))
                                            <td>{{ $tab2_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab2_list->beat ))
                                            <td>{{ $tab2_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab2_list->territorial_range_beat_block ))
                                            <td>{{ $tab2_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab2_list->officer_mobile_no }}</td>
<!--                                         <td>@if($tab2_list->image)<img src="{{ $tab2_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_2']['get_detail_t2'] as $tab2_list)
                                     @if($tab2_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab2_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                             </div>
                          </div>

                             <div id="3" class="tabcontent">
                                  <h3>@if(!empty($data['tab_3']['get_detail_t3'][0]->chart_heading))
                                    {{ $data['tab_3']['get_detail_t3'][0]->chart_heading }}
                                    @endif</h3>
                                 <div style="overflow-x:auto;">
                                  <table>
                                    @if($data['table_count_3'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the Officer/Official</th>
                                        <th>Name of the Officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                  </thead>
                                  @endif
                                    
                                     @foreach($data['tab_3']['get_detail_t3'] as $tab3_list)
                                     @if($tab3_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab3_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab3_list->name_of_the_officer }}</td>
                                          <td>{{ $tab3_list->range }}</td>
                                          @if(!empty( $tab3_list->block ))
                                            <td>{{ $tab3_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab3_list->beat ))
                                            <td>{{ $tab3_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab3_list->territorial_range_beat_block ))
                                            <td>{{ $tab3_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab3_list->officer_mobile_no }}</td>
                                           
                                        <!-- <td>@if($tab3_list->image)<img src="{{ $tab3_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                  <div><img src="{{ $tab3_list->image }}"></div>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab3_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab3_list->name_of_the_officer }}</td>
                                          <td>{{ $tab3_list->range }}</td>
                                          @if(!empty( $tab3_list->block ))
                                            <td>{{ $tab3_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab3_list->beat ))
                                            <td>{{ $tab3_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab3_list->territorial_range_beat_block ))
                                            <td>{{ $tab3_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab3_list->officer_mobile_no }}</td>
                                        
                                        <!-- <td>@if($tab3_list->image)<img src="{{ $tab3_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                   <div><img src="{{ $tab3_list->image }}"></div>
                                        @endif
                                        @endif
                                        @endforeach
                                      

                                 </table>
                                  <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_3']['get_detail_t3'] as $tab3_list)
                                     @if($tab3_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab3_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                 </div>
                             </div> 
                              
                              <div id="4" class="tabcontent">
                                  <h3>@if(!empty($data['tab_4']['get_detail_t4'][0]->chart_heading))
                                    {{ $data['tab_4']['get_detail_t4'][0]->chart_heading }}
                                    @endif</h3>
                                  <div style="overflow-x:auto;">
                                  <table>
                                    @if($data['table_count_4'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the Officer/Official</th>
                                        <th>Name of the Officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                  	</thead>
                                  	@endif
                                    
                                      @foreach($data['tab_4']['get_detail_t4'] as $tab4_list)
                                       @if($tab4_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab4_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab4_list->name_of_the_officer }}</td>
                                          <td>{{ $tab4_list->range }}</td>
                                          @if(!empty( $tab4_list->block ))
                                            <td>{{ $tab4_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab4_list->beat ))
                                            <td>{{ $tab4_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab4_list->territorial_range_beat_block ))
                                            <td>{{ $tab4_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab4_list->officer_mobile_no }}</td>
                                        
<!--                                         <td>@if($tab4_list->image)<img src="{{ $tab4_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab4_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab4_list->name_of_the_officer }}</td>
                                          <td>{{ $tab4_list->range }}</td>
                                          @if(!empty( $tab4_list->block ))
                                            <td>{{ $tab4_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab4_list->beat ))
                                            <td>{{ $tab4_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab4_list->territorial_range_beat_block ))
                                            <td>{{ $tab4_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab4_list->officer_mobile_no }}</td>
                                        
<!--                                         <td>@if($tab4_list->image)<img src="{{ $tab4_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                        
                                   </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_4']['get_detail_t4'] as $tab4_list)
                                     @if($tab4_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab4_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                              </div>
                                  
                              
                              <div id="5" class="tabcontent">
                                  <h3>@if(!empty($data['tab_5']['get_detail_t5'][0]->chart_heading))
                                    {{ $data['tab_5']['get_detail_t5'][0]->chart_heading }}
                                    @endif</h3>
                                  <div style="overflow-x:auto;">
                                  <table>
                                   @if($data['table_count_5'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the Officer/Official</th>
                                        <th>Name of the Officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                  </thead>
                                  @endif
                                    
                                      @foreach($data['tab_5']['get_detail_t5'] as $tab5_list)
                                      @if($tab5_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab5_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab5_list->name_of_the_officer }}</td>
                                          <td>{{ $tab5_list->range }}</td>
                                          @if(!empty( $tab5_list->block ))
                                            <td>{{ $tab5_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab5_list->beat ))
                                            <td>{{ $tab5_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab5_list->territorial_range_beat_block ))
                                            <td>{{ $tab5_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab5_list->officer_mobile_no }}</td>
                                        
                                        <!-- <td>@if($tab5_list->image)<img src="{{ $tab5_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                  <div><img src="{{ $tab5_list->image }}"></div>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab5_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab5_list->name_of_the_officer }}</td>
                                          <td>{{ $tab5_list->range }}</td>
                                          @if(!empty( $tab5_list->block ))
                                            <td>{{ $tab5_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab5_list->beat ))
                                            <td>{{ $tab5_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab5_list->territorial_range_beat_block ))
                                            <td>{{ $tab5_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab5_list->officer_mobile_no }}</td>
                                        
                                        <!-- <td>@if($tab5_list->image)<img src="{{ $tab5_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                      
                                 </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_5']['get_detail_t5'] as $tab5_list)
                                     @if($tab5_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab5_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                              </div>
                              
                               <div id="6" class="tabcontent">
                                  <h3>@if(!empty($data['tab_6']['get_detail_t6'][0]->chart_heading))
                                    {{ $data['tab_6']['get_detail_t6'][0]->chart_heading }}
                                    @endif</h3>
                                   <div style="overflow-x:auto;">
                                  <table>
                                    @if($data['table_count_6'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the Officer/Official</th>
                                        <th>Name of the Officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                          <th>Remarks</th>
                                      </tr>
                                  </thead>
                                  @endif
                                    	
                                      @foreach($data['tab_6']['get_detail_t6'] as $tab6_list)
                                      @if($tab6_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab6_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab6_list->name_of_the_officer }}</td>
                                          <td>{{ $tab6_list->range }}</td>
                                          @if(!empty( $tab6_list->block ))
                                            <td>{{ $tab6_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab6_list->beat ))
                                            <td>{{ $tab6_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab6_list->territorial_range_beat_block ))
                                            <td>{{ $tab6_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab6_list->officer_mobile_no }}</td>
                                            <td>---</td>
                                         <!-- <td>@if($tab6_list->image)<img src="{{ $tab6_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab6_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab6_list->name_of_the_officer }}</td>
                                          <td>{{ $tab6_list->range }}</td>
                                          @if(!empty( $tab6_list->block ))
                                            <td>{{ $tab6_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab6_list->beat ))
                                            <td>{{ $tab6_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab6_list->territorial_range_beat_block ))
                                            <td>{{ $tab6_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab6_list->officer_mobile_no }}</td>
                                            <td>---</td>
                                         <!-- <td>@if($tab6_list->image)<img src="{{ $tab6_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                      
                                   </table>
                                   
                                    <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_6']['get_detail_t6'] as $tab6_list)
                                     @if($tab6_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab6_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                   
                                   </div>
                              </div>
                              
                                <div id="7" class="tabcontent">
                                  <h3>@if(!empty($data['tab_7']['get_detail_t7'][0]->chart_heading))
                                    {{ $data['tab_7']['get_detail_t7'][0]->chart_heading }}
                                    @endif</h3>
                                <div style="overflow-x:auto;">
                                  <table>
                                   @if($data['table_count_7'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the Officer/Official</th>
                                        <th>Name of the Officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                        
                                      </tr>
                                  </thead>
                                  @endif
                                    
                                       @foreach($data['tab_7']['get_detail_t7'] as $tab7_list)
                                       @if($tab7_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab7_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab7_list->name_of_the_officer }}</td>
                                          <td>{{ $tab7_list->range }}</td>
                                          @if(!empty( $tab7_list->block ))
                                            <td>{{ $tab7_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab7_list->beat ))
                                            <td>{{ $tab7_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab7_list->territorial_range_beat_block ))
                                            <td>{{ $tab7_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab7_list->officer_mobile_no }}</td>
                                             <!-- <td>@if($tab7_list->image)<img src="{{ $tab7_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab7_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab7_list->name_of_the_officer }}</td>
                                          <td>{{ $tab7_list->range }}</td>
                                          @if(!empty( $tab7_list->block ))
                                            <td>{{ $tab7_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab7_list->beat ))
                                            <td>{{ $tab7_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab7_list->territorial_range_beat_block ))
                                            <td>{{ $tab7_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab7_list->officer_mobile_no }}</td>
                                             <!-- <td>@if($tab7_list->image)<img src="{{ $tab7_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                      
                                    </table>
                                 <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_7']['get_detail_t7'] as $tab7_list)
                                     @if($tab7_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab7_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                </div>
 
                          </div>

                          <div id="8" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_8']['get_detail_t8'][0]->chart_heading))
                                    {{ $data['tab_8']['get_detail_t8'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_8'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_8']['get_detail_t8'] as $tab8_list)
                                      @if($tab8_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab8_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab8_list->name_of_the_officer }}</td>
                                          <td>{{ $tab8_list->range }}</td>
                                          @if(!empty( $tab8_list->block ))
                                            <td>{{ $tab8_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab8_list->beat ))
                                            <td>{{ $tab8_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab8_list->territorial_range_beat_block ))
                                            <td>{{ $tab8_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab8_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab8_list->image)<img src="{{ $tab8_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab8_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab8_list->name_of_the_officer }}</td>
                                          <td>{{ $tab8_list->range }}</td>
                                          @if(!empty( $tab8_list->block ))
                                            <td>{{ $tab8_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab8_list->beat ))
                                            <td>{{ $tab8_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab8_list->territorial_range_beat_block ))
                                            <td>{{ $tab8_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab8_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab8_list->image)<img src="{{ $tab8_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_8']['get_detail_t8'] as $tab8_list)
                                     @if($tab8_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab8_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                                <div id="9" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_9']['get_detail_t9'][0]->chart_heading))
                                   
                                    {{ $data['tab_9']['get_detail_t9'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                   @if($data['table_count_9'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_9']['get_detail_t9'] as $tab9_list)
                                       @if($tab9_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab9_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab9_list->name_of_the_officer }}</td>
                                          <td>{{ $tab9_list->range }}</td>
                                          @if(!empty( $tab9_list->block ))
                                            <td>{{ $tab9_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab9_list->beat ))
                                            <td>{{ $tab9_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab9_list->territorial_range_beat_block ))
                                            <td>{{ $tab9_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab9_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab9_list->image)<img src="{{ $tab9_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab9_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab9_list->name_of_the_officer }}</td>
                                          <td>{{ $tab9_list->range }}</td>
                                          @if(!empty( $tab9_list->block ))
                                            <td>{{ $tab9_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab9_list->beat ))
                                            <td>{{ $tab9_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab9_list->territorial_range_beat_block ))
                                            <td>{{ $tab9_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab9_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab9_list->image)<img src="{{ $tab9_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_9']['get_detail_t9'] as $tab9_list)
                                     @if($tab9_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab9_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                               <div id="10" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_10']['get_detail_t10'][0]->chart_heading))
                                    {{ $data['tab_10']['get_detail_t10'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                  
                                   <table>
                                   <div>
                                 
                                     @if($data['table_count'] != 0)
                                    <thead clas="check">
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                  </thead>
                            		@endif
                                   </div>
                                      @foreach($data['tab_10']['get_detail_t10'] as $tab10_list)
                                      @if($tab10_list->type_of_table == 'table_data')
                                    <input type="hidden" value="1" id="check_value">
                                       @if($loop->odd)
                                       <tbody>
                                        <tr class="tr-color">
                                          <td>{{ $tab10_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab10_list->name_of_the_officer }}</td>
                                          <td>{{ $tab10_list->range }}</td>
                                          @if(!empty( $tab10_list->block ))
                                            <td>{{ $tab10_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab10_list->beat ))
                                            <td>{{ $tab10_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab10_list->territorial_range_beat_block ))
                                            <td>{{ $tab10_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab10_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab10_list->image)<img src="{{ $tab10_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab10_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab10_list->name_of_the_officer }}</td>
                                          <td>{{ $tab10_list->range }}</td>
                                          @if(!empty( $tab10_list->block ))
                                            <td>{{ $tab10_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab10_list->beat ))
                                            <td>{{ $tab10_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab10_list->territorial_range_beat_block ))
                                            <td>{{ $tab10_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab10_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab10_list->image)<img src="{{ $tab10_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                    </tbody>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_10']['get_detail_t10'] as $tab10_list)
                                     @if($tab10_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab10_list->image}}" style="height:137%;width:106%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                                <div id="11" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_11']['get_detail_t11'][0]->chart_heading))
                                    {{ $data['tab_11']['get_detail_t11'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_11'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                    @endif
                                    
                                      @foreach($data['tab_11']['get_detail_t11'] as $tab11_list)
                                       @if($tab11_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab11_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab11_list->name_of_the_officer }}</td>
                                          <td>{{ $tab11_list->range }}</td>
                                          @if(!empty( $tab11_list->block ))
                                            <td>{{ $tab11_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab11_list->beat ))
                                            <td>{{ $tab11_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab11_list->territorial_range_beat_block ))
                                            <td>{{ $tab11_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab11_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab11_list->image)<img src="{{ $tab11_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab11_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab11_list->name_of_the_officer }}</td>
                                          <td>{{ $tab11_list->range }}</td>
                                          @if(!empty( $tab11_list->block ))
                                            <td>{{ $tab11_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab11_list->beat ))
                                            <td>{{ $tab11_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab11_list->territorial_range_beat_block ))
                                            <td>{{ $tab11_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab11_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab11_list->image)<img src="{{ $tab11_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_11']['get_detail_t11'] as $tab11_list)
                                     @if($tab11_list->type_of_table == "image")
                                        <div class="col-md-12">
                                            <img src="{{ $tab11_list->image}}" style="height:137%;width:106%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>


                                <div id="12" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_12']['get_detail_t12'][0]->chart_heading))
                                    {{ $data['tab_12']['get_detail_t12'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_12'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_12']['get_detail_t12'] as $tab12_list)
                                       @if($tab12_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab12_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab12_list->name_of_the_officer }}</td>
                                          <td>{{ $tab12_list->range }}</td>
                                          @if(!empty( $tab12_list->block ))
                                            <td>{{ $tab12_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab12_list->beat ))
                                            <td>{{ $tab12_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab12_list->territorial_range_beat_block ))
                                            <td>{{ $tab12_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab12_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab12_list->image)<img src="{{ $tab12_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab12_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab12_list->name_of_the_officer }}</td>
                                          <td>{{ $tab12_list->range }}</td>
                                          @if(!empty( $tab12_list->block ))
                                            <td>{{ $tab12_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab12_list->beat ))
                                            <td>{{ $tab12_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab12_list->territorial_range_beat_block ))
                                            <td>{{ $tab12_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab12_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab12_list->image)<img src="{{ $tab12_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_12']['get_detail_t12'] as $tab12_list)
                                     @if($tab12_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab12_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                                   <div id="13" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_13']['get_detail_t13'][0]->chart_heading))
                                    {{ $data['tab_13']['get_detail_t13'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_13'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                    @endif
                                    
                                      @foreach($data['tab_13']['get_detail_t13'] as $tab13_list)
                                       @if($tab13_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab13_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab13_list->name_of_the_officer }}</td>
                                          <td>{{ $tab13_list->range }}</td>
                                          @if(!empty( $tab13_list->block ))
                                            <td>{{ $tab13_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab13_list->beat ))
                                            <td>{{ $tab13_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab13_list->territorial_range_beat_block ))
                                            <td>{{ $tab13_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab13_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab13_list->image)<img src="{{ $tab13_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab13_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab13_list->name_of_the_officer }}</td>
                                          <td>{{ $tab13_list->range }}</td>
                                          @if(!empty( $tab13_list->block ))
                                            <td>{{ $tab13_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab13_list->beat ))
                                            <td>{{ $tab13_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab13_list->territorial_range_beat_block ))
                                            <td>{{ $tab13_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab13_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab13_list->image)<img src="{{ $tab13_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_13']['get_detail_t13'] as $tab13_list)
                                     @if($tab13_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab13_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                                 <div id="14" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_14']['get_detail_t14'][0]->chart_heading))
                                    {{ $data['tab_14']['get_detail_t14'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_14'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_14']['get_detail_t14'] as $tab14_list)
                                       @if($tab14_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab14_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab14_list->name_of_the_officer }}</td>
                                          <td>{{ $tab14_list->range }}</td>
                                          @if(!empty( $tab14_list->block ))
                                            <td>{{ $tab14_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab14_list->beat ))
                                            <td>{{ $tab14_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab14_list->territorial_range_beat_block ))
                                            <td>{{ $tab14_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab14_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab14_list->image)<img src="{{ $tab14_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab14_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab14_list->name_of_the_officer }}</td>
                                          <td>{{ $tab14_list->range }}</td>
                                          @if(!empty( $tab14_list->block ))
                                            <td>{{ $tab14_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab14_list->beat ))
                                            <td>{{ $tab14_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab14_list->territorial_range_beat_block ))
                                            <td>{{ $tab14_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab14_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab14_list->image)<img src="{{ $tab14_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_14']['get_detail_t14'] as $tab14_list)
                                     @if($tab14_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab14_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>
                              

                               <div id="15" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_15']['get_detail_t15'][0]->chart_heading))
                                    {{ $data['tab_15']['get_detail_t15'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_15'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                   
                                      @foreach($data['tab_15']['get_detail_t15'] as $tab15_list)
                                       @if($tab15_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab15_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab15_list->name_of_the_officer }}</td>
                                          <td>{{ $tab15_list->range }}</td>
                                          @if(!empty( $tab15_list->block ))
                                            <td>{{ $tab15_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab15_list->beat ))
                                            <td>{{ $tab15_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab15_list->territorial_range_beat_block ))
                                            <td>{{ $tab15_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab15_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab15_list->image)<img src="{{ $tab15_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab15_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab15_list->name_of_the_officer }}</td>
                                          <td>{{ $tab15_list->range }}</td>
                                          @if(!empty( $tab15_list->block ))
                                            <td>{{ $tab15_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab15_list->beat ))
                                            <td>{{ $tab15_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab15_list->territorial_range_beat_block ))
                                            <td>{{ $tab15_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab15_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab15_list->image)<img src="{{ $tab15_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_15']['get_detail_t15'] as $tab15_list)
                                     @if($tab15_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab15_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                               <div id="16" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_16']['get_detail_t16'][0]->chart_heading))
                                    {{ $data['tab_16']['get_detail_t16'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                   @if($data['table_count_16'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_16']['get_detail_t16'] as $tab16_list)
                                       @if($tab16_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab16_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab16_list->name_of_the_officer }}</td>
                                          <td>{{ $tab16_list->range }}</td>
                                          @if(!empty( $tab16_list->block ))
                                            <td>{{ $tab16_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab16_list->beat ))
                                            <td>{{ $tab16_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab16_list->territorial_range_beat_block ))
                                            <td>{{ $tab16_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab16_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab16_list->image)<img src="{{ $tab16_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                     
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab16_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab16_list->name_of_the_officer }}</td>
                                          <td>{{ $tab16_list->range }}</td>
                                          @if(!empty( $tab16_list->block ))
                                            <td>{{ $tab16_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab16_list->beat ))
                                            <td>{{ $tab16_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab16_list->territorial_range_beat_block ))
                                            <td>{{ $tab16_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab16_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab16_list->image)<img src="{{ $tab16_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_16']['get_detail_t16'] as $tab16_list)
                                     @if($tab16_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab16_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>

                                <div id="17" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_17']['get_detail_t17'][0]->chart_heading))
                                    {{ $data['tab_17']['get_detail_t17'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_17'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                   
                                      @foreach($data['tab_17']['get_detail_t17'] as $tab17_list)
                                       @if($tab17_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab17_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab17_list->name_of_the_officer }}</td>
                                          <td>{{ $tab17_list->range }}</td>
                                          @if(!empty( $tab17_list->block ))
                                            <td>{{ $tab17_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab17_list->beat ))
                                            <td>{{ $tab17_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab17_list->territorial_range_beat_block ))
                                            <td>{{ $tab17_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab17_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab17_list->image)<img src="{{ $tab17_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab17_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab17_list->name_of_the_officer }}</td>
                                          <td>{{ $tab17_list->range }}</td>
                                          @if(!empty( $tab17_list->block ))
                                            <td>{{ $tab17_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab17_list->beat ))
                                            <td>{{ $tab17_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab17_list->territorial_range_beat_block ))
                                            <td>{{ $tab17_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab17_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab17_list->image)<img src="{{ $tab17_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_17']['get_detail_t17'] as $tab17_list)
                                     @if($tab17_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab17_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>

                              <div id="18" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_18']['get_detail_t18'][0]->chart_heading))
                                    {{ $data['tab_18']['get_detail_t18'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_18'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                   
                                      @foreach($data['tab_18']['get_detail_t18'] as $tab18_list)
                                       @if($tab18_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab18_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab18_list->name_of_the_officer }}</td>
                                          <td>{{ $tab18_list->range }}</td>
                                          @if(!empty( $tab18_list->block ))
                                            <td>{{ $tab18_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab18_list->beat ))
                                            <td>{{ $tab18_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab18_list->territorial_range_beat_block ))
                                            <td>{{ $tab18_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab18_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab18_list->image)<img src="{{ $tab18_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab18_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab18_list->name_of_the_officer }}</td>
                                          <td>{{ $tab18_list->range }}</td>
                                          @if(!empty( $tab18_list->block ))
                                            <td>{{ $tab18_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab18_list->beat ))
                                            <td>{{ $tab18_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab18_list->territorial_range_beat_block ))
                                            <td>{{ $tab18_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab18_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab18_list->image)<img src="{{ $tab18_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_18']['get_detail_t18'] as $tab18_list)
                                     @if($tab18_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab18_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>
                              
                              <div id="19" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_19']['get_detail_t19'][0]->chart_heading))
                                    {{ $data['tab_19']['get_detail_t19'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                      @if($data['table_count_19'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                   
                                      @foreach($data['tab_19']['get_detail_t19'] as $tab19_list)
                                       @if($tab19_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab19_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab19_list->name_of_the_officer }}</td>
                                          <td>{{ $tab19_list->range }}</td>
                                          @if(!empty( $tab19_list->block ))
                                            <td>{{ $tab19_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab19_list->beat ))
                                            <td>{{ $tab19_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab19_list->territorial_range_beat_block ))
                                            <td>{{ $tab19_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab19_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab19_list->image)<img src="{{ $tab19_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab19_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab19_list->name_of_the_officer }}</td>
                                          <td>{{ $tab19_list->range }}</td>
                                          @if(!empty( $tab19_list->block ))
                                            <td>{{ $tab19_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab19_list->beat ))
                                            <td>{{ $tab19_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab19_list->territorial_range_beat_block ))
                                            <td>{{ $tab19_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab19_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab19_list->image)<img src="{{ $tab19_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_19']['get_detail_t19'] as $tab19_list)
                                     @if($tab19_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab19_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                              <div id="20" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_20']['get_detail_t20'][0]->chart_heading))
                                    {{ $data['tab_20']['get_detail_t20'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_20'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_20']['get_detail_t20'] as $tab20_list)
                                       @if($tab20_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab20_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab20_list->name_of_the_officer }}</td>
                                          <td>{{ $tab20_list->range }}</td>
                                          @if(!empty( $tab20_list->block ))
                                            <td>{{ $tab20_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab20_list->beat ))
                                            <td>{{ $tab20_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab20_list->territorial_range_beat_block ))
                                            <td>{{ $tab20_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab20_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab20_list->image)<img src="{{ $tab20_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab20_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab20_list->name_of_the_officer }}</td>
                                          <td>{{ $tab20_list->range }}</td>
                                          @if(!empty( $tab20_list->block ))
                                            <td>{{ $tab20_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab20_list->beat ))
                                            <td>{{ $tab20_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab20_list->territorial_range_beat_block ))
                                            <td>{{ $tab20_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab20_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab20_list->image)<img src="{{ $tab20_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_20']['get_detail_t20'] as $tab20_list)
                                     @if($tab20_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab20_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                             <div id="21" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_21']['get_detail_t21'][0]->chart_heading))
                                    {{ $data['tab_21']['get_detail_t21'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                      @if($data['table_count_21'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_21']['get_detail_t21'] as $tab21_list)
                                       @if($tab21_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab21_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab21_list->name_of_the_officer }}</td>
                                          <td>{{ $tab21_list->range }}</td>
                                          @if(!empty( $tab21_list->block ))
                                            <td>{{ $tab21_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab21_list->beat ))
                                            <td>{{ $tab21_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab21_list->territorial_range_beat_block ))
                                            <td>{{ $tab21_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab21_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab21_list->image)<img src="{{ $tab21_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab21_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab21_list->name_of_the_officer }}</td>
                                          <td>{{ $tab21_list->range }}</td>
                                          @if(!empty( $tab21_list->block ))
                                            <td>{{ $tab21_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab21_list->beat ))
                                            <td>{{ $tab21_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab21_list->territorial_range_beat_block ))
                                            <td>{{ $tab21_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab21_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab21_list->image)<img src="{{ $tab21_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_21']['get_detail_t21'] as $tab21_list)
                                     @if($tab21_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab21_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                              <div id="22" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_22']['get_detail_t22'][0]->chart_heading))
                                    {{ $data['tab_22']['get_detail_t22'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_22'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_22']['get_detail_t22'] as $tab22_list)
                                       @if($tab22_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab22_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab22_list->name_of_the_officer }}</td>
                                          <td>{{ $tab22_list->range }}</td>
                                          @if(!empty( $tab22_list->block ))
                                            <td>{{ $tab22_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab22_list->beat ))
                                            <td>{{ $tab22_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab22_list->territorial_range_beat_block ))
                                            <td>{{ $tab22_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab22_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab22_list->image)<img src="{{ $tab22_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab22_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab22_list->name_of_the_officer }}</td>
                                          <td>{{ $tab22_list->range }}</td>
                                          @if(!empty( $tab22_list->block ))
                                            <td>{{ $tab22_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab22_list->beat ))
                                            <td>{{ $tab22_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab22_list->territorial_range_beat_block ))
                                            <td>{{ $tab22_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab22_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab22_list->image)<img src="{{ $tab22_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_22']['get_detail_t22'] as $tab22_list)
                                     @if($tab22_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab22_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>

                              <div id="23" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_23']['get_detail_t23'][0]->chart_heading))
                                    {{ $data['tab_23']['get_detail_t23'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_23'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_23']['get_detail_t23'] as $tab23_list)
                                       @if($tab23_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab23_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab23_list->name_of_the_officer }}</td>
                                          <td>{{ $tab23_list->range }}</td>
                                          @if(!empty( $tab23_list->block ))
                                            <td>{{ $tab23_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab23_list->beat ))
                                            <td>{{ $tab23_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab23_list->territorial_range_beat_block ))
                                            <td>{{ $tab23_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab23_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab23_list->image)<img src="{{ $tab23_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab23_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab23_list->name_of_the_officer }}</td>
                                          <td>{{ $tab23_list->range }}</td>
                                          @if(!empty( $tab23_list->block ))
                                            <td>{{ $tab23_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab23_list->beat ))
                                            <td>{{ $tab23_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab23_list->territorial_range_beat_block ))
                                            <td>{{ $tab23_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab23_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab23_list->image)<img src="{{ $tab23_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_23']['get_detail_t23'] as $tab23_list)
                                     @if($tab23_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab23_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                               <div id="24" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_24']['get_detail_t24'][0]->chart_heading))
                                    {{ $data['tab_24']['get_detail_t24'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_24'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                    
                                      @foreach($data['tab_24']['get_detail_t24'] as $tab24_list)
                                       @if($tab24_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab24_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab24_list->name_of_the_officer }}</td>
                                          <td>{{ $tab24_list->range }}</td>
                                          @if(!empty( $tab24_list->block ))
                                            <td>{{ $tab24_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab24_list->beat ))
                                            <td>{{ $tab24_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab24_list->territorial_range_beat_block ))
                                            <td>{{ $tab24_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab24_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab24_list->image)<img src="{{ $tab24_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab24_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab24_list->name_of_the_officer }}</td>
                                          <td>{{ $tab24_list->range }}</td>
                                          @if(!empty( $tab24_list->block ))
                                            <td>{{ $tab24_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab24_list->beat ))
                                            <td>{{ $tab24_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab24_list->territorial_range_beat_block ))
                                            <td>{{ $tab24_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab24_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab24_list->image)<img src="{{ $tab24_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_24']['get_detail_t24'] as $tab24_list)
                                     @if($tab24_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab24_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                             <div id="25" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_25']['get_detail_t25'][0]->chart_heading))
                                    {{ $data['tab_25']['get_detail_t25'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_25'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_25']['get_detail_t25'] as $tab25_list)
                                       @if($tab25_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab25_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab25_list->name_of_the_officer }}</td>
                                          <td>{{ $tab25_list->range }}</td>
                                          @if(!empty( $tab25_list->block ))
                                            <td>{{ $tab25_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab25_list->beat ))
                                            <td>{{ $tab25_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab25_list->territorial_range_beat_block ))
                                            <td>{{ $tab25_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab25_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab25_list->image)<img src="{{ $tab25_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab25_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab25_list->name_of_the_officer }}</td>
                                          <td>{{ $tab25_list->range }}</td>
                                          @if(!empty( $tab25_list->block ))
                                            <td>{{ $tab25_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab25_list->beat ))
                                            <td>{{ $tab25_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab25_list->territorial_range_beat_block ))
                                            <td>{{ $tab25_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab25_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab25_list->image)<img src="{{ $tab25_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_25']['get_detail_t25'] as $tab25_list)
                                     @if($tab25_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab25_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                             <div id="26" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_26']['get_detail_t26'][0]->chart_heading))
                                    {{ $data['tab_26']['get_detail_t26'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_26'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_26']['get_detail_t26'] as $tab26_list)
                                       @if($tab26_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab26_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab26_list->name_of_the_officer }}</td>
                                          <td>{{ $tab26_list->range }}</td>
                                          @if(!empty( $tab26_list->block ))
                                            <td>{{ $tab26_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab26_list->beat ))
                                            <td>{{ $tab26_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab26_list->territorial_range_beat_block ))
                                            <td>{{ $tab26_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab26_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab26_list->image)<img src="{{ $tab26_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab26_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab26_list->name_of_the_officer }}</td>
                                          <td>{{ $tab26_list->range }}</td>
                                          @if(!empty( $tab26_list->block ))
                                            <td>{{ $tab26_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab26_list->beat ))
                                            <td>{{ $tab26_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab26_list->territorial_range_beat_block ))
                                            <td>{{ $tab26_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab26_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab26_list->image)<img src="{{ $tab26_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_26']['get_detail_t26'] as $tab26_list)
                                     @if($tab26_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab26_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                              <div id="27" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_27']['get_detail_t27'][0]->chart_heading))
                                    {{ $data['tab_27']['get_detail_t27'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_27'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                   
                                      @foreach($data['tab_27']['get_detail_t27'] as $tab27_list)
                                       @if($tab27_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab27_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab27_list->name_of_the_officer }}</td>
                                          <td>{{ $tab27_list->range }}</td>
                                          @if(!empty( $tab27_list->block ))
                                            <td>{{ $tab27_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab27_list->beat ))
                                            <td>{{ $tab27_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab27_list->territorial_range_beat_block ))
                                            <td>{{ $tab27_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab27_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab27_list->image)<img src="{{ $tab27_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab27_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab27_list->name_of_the_officer }}</td>
                                          <td>{{ $tab27_list->range }}</td>
                                          @if(!empty( $tab27_list->block ))
                                            <td>{{ $tab27_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab27_list->beat ))
                                            <td>{{ $tab27_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab27_list->territorial_range_beat_block ))
                                            <td>{{ $tab27_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab27_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab27_list->image)<img src="{{ $tab27_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_27']['get_detail_t27'] as $tab27_list)
                                     @if($tab27_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab27_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                                <div id="28" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_28']['get_detail_t28'][0]->chart_heading))
                                    {{ $data['tab_28']['get_detail_t28'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_28'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_28']['get_detail_t28'] as $tab28_list)
                                       @if($tab28_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab28_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab28_list->name_of_the_officer }}</td>
                                          <td>{{ $tab28_list->range }}</td>
                                          @if(!empty( $tab28_list->block ))
                                            <td>{{ $tab28_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab28_list->beat ))
                                            <td>{{ $tab28_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab28_list->territorial_range_beat_block ))
                                            <td>{{ $tab28_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab28_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab28_list->image)<img src="{{ $tab28_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab28_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab28_list->name_of_the_officer }}</td>
                                          <td>{{ $tab28_list->range }}</td>
                                          @if(!empty( $tab28_list->block ))
                                            <td>{{ $tab28_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab28_list->beat ))
                                            <td>{{ $tab28_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab28_list->territorial_range_beat_block ))
                                            <td>{{ $tab28_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab28_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab28_list->image)<img src="{{ $tab28_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_28']['get_detail_t28'] as $tab28_list)
                                     @if($tab28_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab28_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                             <div id="29" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_29']['get_detail_t29'][0]->chart_heading))
                                    {{ $data['tab_29']['get_detail_t29'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_29'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_29']['get_detail_t29'] as $tab29_list)
                                       @if($tab29_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab29_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab29_list->name_of_the_officer }}</td>
                                          <td>{{ $tab29_list->range }}</td>
                                          @if(!empty( $tab29_list->block ))
                                            <td>{{ $tab29_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab29_list->beat ))
                                            <td>{{ $tab29_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab29_list->territorial_range_beat_block ))
                                            <td>{{ $tab29_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab29_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab29_list->image)<img src="{{ $tab29_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab29_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab29_list->name_of_the_officer }}</td>
                                          <td>{{ $tab29_list->range }}</td>
                                          @if(!empty( $tab29_list->block ))
                                            <td>{{ $tab29_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab29_list->beat ))
                                            <td>{{ $tab29_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab29_list->territorial_range_beat_block ))
                                            <td>{{ $tab29_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab29_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab29_list->image)<img src="{{ $tab29_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_29']['get_detail_t29'] as $tab29_list)
                                     @if($tab29_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab29_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                             <div id="30" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_30']['get_detail_t30'][0]->chart_heading))
                                    {{ $data['tab_30']['get_detail_t30'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_30'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_30']['get_detail_t30'] as $tab30_list)
                                       @if($tab30_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab30_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab30_list->name_of_the_officer }}</td>
                                          <td>{{ $tab30_list->range }}</td>
                                          @if(!empty( $tab30_list->block ))
                                            <td>{{ $tab30_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab30_list->beat ))
                                            <td>{{ $tab30_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab30_list->territorial_range_beat_block ))
                                            <td>{{ $tab30_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab30_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab30_list->image)<img src="{{ $tab30_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab30_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab30_list->name_of_the_officer }}</td>
                                          <td>{{ $tab30_list->range }}</td>
                                          @if(!empty( $tab30_list->block ))
                                            <td>{{ $tab30_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab30_list->beat ))
                                            <td>{{ $tab30_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab30_list->territorial_range_beat_block ))
                                            <td>{{ $tab30_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab30_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab30_list->image)<img src="{{ $tab30_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_30']['get_detail_t30'] as $tab30_list)
                                     @if($tab30_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab30_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>


                             <div id="31" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_31']['get_detail_t31'][0]->chart_heading))
                                    {{ $data['tab_31']['get_detail_t31'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                     @if($data['table_count_31'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_31']['get_detail_t31'] as $tab31_list)
                                       @if($tab9_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab31_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab31_list->name_of_the_officer }}</td>
                                          <td>{{ $tab31_list->range }}</td>
                                          @if(!empty( $tab31_list->block ))
                                            <td>{{ $tab31_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab31_list->beat ))
                                            <td>{{ $tab31_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab31_list->territorial_range_beat_block ))
                                            <td>{{ $tab31_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab31_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab31_list->image)<img src="{{ $tab31_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab31_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab31_list->name_of_the_officer }}</td>
                                          <td>{{ $tab31_list->range }}</td>
                                          @if(!empty( $tab31_list->block ))
                                            <td>{{ $tab31_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab31_list->beat ))
                                            <td>{{ $tab31_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab31_list->territorial_range_beat_block ))
                                            <td>{{ $tab31_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab31_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab31_list->image)<img src="{{ $tab31_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_31']['get_detail_t31'] as $tab31_list)
                                     @if($tab31_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab31_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>


                             <div id="32" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_32']['get_detail_t32'][0]->chart_heading))
                                    {{ $data['tab_32']['get_detail_t32'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_32'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_32']['get_detail_t32'] as $tab32_list)
                                       @if($tab32_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab32_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab32_list->name_of_the_officer }}</td>
                                          <td>{{ $tab32_list->range }}</td>
                                          @if(!empty( $tab32_list->block ))
                                            <td>{{ $tab32_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab32_list->beat ))
                                            <td>{{ $tab32_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab32_list->territorial_range_beat_block ))
                                            <td>{{ $tab32_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab32_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab32_list->image)<img src="{{ $tab32_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab32_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab32_list->name_of_the_officer }}</td>
                                          <td>{{ $tab32_list->range }}</td>
                                          @if(!empty( $tab32_list->block ))
                                            <td>{{ $tab32_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab32_list->beat ))
                                            <td>{{ $tab32_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab32_list->territorial_range_beat_block ))
                                            <td>{{ $tab32_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab32_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab32_list->image)<img src="{{ $tab32_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_32']['get_detail_t32'] as $tab32_list)
                                     @if($tab32_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab32_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>

                               <div id="33" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_33']['get_detail_t33'][0]->chart_heading))
                                    {{ $data['tab_33']['get_detail_t33'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                      @if($data['table_count_33'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_33']['get_detail_t33'] as $tab33_list)
                                       @if($tab33_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab33_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab33_list->name_of_the_officer }}</td>
                                          <td>{{ $tab33_list->range }}</td>
                                          @if(!empty( $tab33_list->block ))
                                            <td>{{ $tab33_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab33_list->beat ))
                                            <td>{{ $tab33_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab33_list->territorial_range_beat_block ))
                                            <td>{{ $tab33_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab33_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab33_list->image)<img src="{{ $tab33_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab33_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab33_list->name_of_the_officer }}</td>
                                          <td>{{ $tab33_list->range }}</td>
                                          @if(!empty( $tab33_list->block ))
                                            <td>{{ $tab33_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab33_list->beat ))
                                            <td>{{ $tab33_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab33_list->territorial_range_beat_block ))
                                            <td>{{ $tab33_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab33_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab33_list->image)<img src="{{ $tab33_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_33']['get_detail_t33'] as $tab33_list)
                                     @if($tab33_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab33_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>

                              <div id="34" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_34']['get_detail_t34'][0]->chart_heading))
                                    {{ $data['tab_34']['get_detail_t34'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                      @if($data['table_count_34'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_34']['get_detail_t34'] as $tab34_list)
                                       @if($tab34_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab34_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab34_list->name_of_the_officer }}</td>
                                          <td>{{ $tab34_list->range }}</td>
                                          @if(!empty( $tab34_list->block ))
                                            <td>{{ $tab34_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab34_list->beat ))
                                            <td>{{ $tab34_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab34_list->territorial_range_beat_block ))
                                            <td>{{ $tab34_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab34_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab34_list->image)<img src="{{ $tab34_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab34_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab34_list->name_of_the_officer }}</td>
                                          <td>{{ $tab34_list->range }}</td>
                                          @if(!empty( $tab34_list->block ))
                                            <td>{{ $tab34_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab34_list->beat ))
                                            <td>{{ $tab34_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab34_list->territorial_range_beat_block ))
                                            <td>{{ $tab34_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab34_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab34_list->image)<img src="{{ $tab34_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                  
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_34']['get_detail_t34'] as $tab34_list)
                                     @if($tab34_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab34_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  
                                  </div>
                             </div>

                               <div id="35" class="tabcontent ">
                                  <h3>
                                    @if(!empty($data['tab_35']['get_detail_t35'][0]->chart_heading))
                                    {{ $data['tab_35']['get_detail_t35'][0]->chart_heading }}
                                    @endif
                                  </h3>
                                  <div style="overflow-x:auto;">
                                   <table>
                                    @if($data['table_count_35'] != 0)
                                    <thead>
                                      <tr>
                                        <th>Designation of the officer/Official</th>
                                        <th>Name of the officer/Official</th>
                                        <th>Range</th>
                                        <th>Block</th>
                                        <th>Beat</th>
                                        <th>Territorial Range/Block/Beat</th>
                                          <th>Mobile No. of the Officer/Official</th>
                                      </tr>
                                   </thead>
                                   @endif
                                      @foreach($data['tab_35']['get_detail_t35'] as $tab35_list)
                                       @if($tab35_list->type_of_table == 'table_data')
                                       @if($loop->odd)
                                        <tr class="tr-color">
                                          <td>{{ $tab35_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab35_list->name_of_the_officer }}</td>
                                          <td>{{ $tab35_list->range }}</td>
                                          @if(!empty( $tab35_list->block ))
                                            <td>{{ $tab35_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab35_list->beat ))
                                            <td>{{ $tab35_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab35_list->territorial_range_beat_block ))
                                            <td>{{ $tab35_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab35_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab35_list->image)<img src="{{ $tab35_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @else($loop->even)
                                        <tr>
                                          <td>{{ $tab35_list->designation_of_the_officer }}</td>
                                          <td>{{ $tab35_list->name_of_the_officer }}</td>
                                          <td>{{ $tab35_list->range }}</td>
                                          @if(!empty( $tab35_list->block ))
                                            <td>{{ $tab35_list->block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab35_list->beat ))
                                            <td>{{ $tab35_list->beat }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                           @if(!empty( $tab35_list->territorial_range_beat_block ))
                                            <td>{{ $tab35_list->territorial_range_beat_block }}</td>
                                            @else
                                            <td>---</td>
                                            @endif
                                            <td>{{ $tab35_list->officer_mobile_no }}</td>
                                         <!-- <td>@if($tab35_list->image)<img src="{{ $tab35_list->image }}" style="height:50%;width:80%;"/>@else -- @endif</td> -->
                                        
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                    
                                  </table>
                                   <div class="container">
                                      <div class="row">
                                  @foreach($data['tab_35']['get_detail_t35'] as $tab35_list)
                                     @if($tab35_list->type_of_table == "image")
                                        <div class="col-md-4">
                                            <img src="{{ $tab35_list->image}}" style="height:83%;width:67%;">
                                          </div>
                                    @endif
                                   @endforeach
                                      </div>
                                    </div>
                                  </div>
                             </div>
                              

                         </div>
                       
                       
                      
                     </div>
                  </div>
               </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
             <div class="m-top">
           
      <script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async data-noptimize="1" src='citygov/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js'></script> <script type="text/javascript">(function () {
         var c = document.body.className;
         c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
         document.body.className = c;
         })()
      </script> <script type='text/javascript' id='lodash-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='lodash-js-after'>window.lodash = _.noConflict();</script> <script type='text/javascript' id='wp-url-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-translations'>( function( domain, translations ) {
         var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
         localeData[""].domain = domain;
         wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script> <script type='text/javascript' id='wp-api-fetch-js-after'>wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://dannci.wpmasters.org/citygov/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "36240d6985" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "index.html";
      </script> <script type='text/javascript' id='contact-form-7-js-extra'>var wpcf7 = [];</script> <script type='text/javascript' id='wc-add-to-cart-js-extra'>var wc_add_to_cart_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dannci.wpmasters.org\/citygov\/shop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};</script> <script type='text/javascript' id='woocommerce-js-extra'>var woocommerce_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%"};</script> <script type='text/javascript' id='wc-cart-fragments-js-extra'>var wc_cart_fragments_params = {"ajax_url":"index.html","wc_ajax_url":"\/citygov\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7e9f8ba3100c6419bde79170db537c82","fragment_name":"wc_fragments_7e9f8ba3100c6419bde79170db537c82","request_timeout":"5000"};</script> <script type='text/javascript' id='wc-cart-fragments-js-after'>jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
         var jetpackLazyImagesLoadEvent;
         try {
          jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
            bubbles: true,
            cancelable: true
          } );
         } catch ( e ) {
          jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
          jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
         }
         jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
         } );
      </script>
<!--              <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/dannci.wpmasters.org\/citygov\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":20,"title":"CityGov%20%E2%80%93%20Just%20another%20WordPress%20site","excerpt":"","featuredImage":false}};</script>  -->
             <script defer src="citygov/wp-content/cache/autoptimize/1/js/autoptimize_a94b36bc5315f68bdcb2ad7e8702ca91.js"></script>
      <script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
          document.getElementById("defaultOpen").click();
</script>
      
  

@endsection

@push('page-script')




<script type="text/javascript">
    $('#defaultOpen').click(function(){
      // alert('hello');
    var tab_id = $(this).val(); 
    // alert(tab_id);
     var token = '{{ csrf_token() }}';    
    if(categoryID){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('breedgetpet')}}?pet_category="+categoryID,
           success:function(res){               
            if(res){
                $("#pet_breed").empty();
                $("#pet_breed").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#pet_breed").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#pet_breed").empty();
            }
           }
        });
    }
    else{
        $("#pet_breed").empty();
        
    }      
   });



  
</script>

@endpush