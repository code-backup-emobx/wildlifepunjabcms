@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
              <div class="page-bar mt-5">
                     <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Wetland</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/protected-wetland-list') }}">Protected Wetland List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-protected-wetland-detail/'.$id) }}">View  Protected Wetland Detail</a>
                        </li>
                    </ul>
              </div>


               @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">View Protected Wetland Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/protected-wetland-list') }}">
                                    <button type="button" class="btn btn-sm">Protected Wetland Detail
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                           
                                <div class="container row">
                                    <div class="col-md-12">
                                      <div><h5><b>Protected Wetland Name:</b></h5>{{ $view_list->get_type_name->name }}</div>


                                      <h5><b>Banner image:</b></h5><p><img src="{{ $view_list->banner_image }}" style="width:20%;"></p>
                                      <h5><b>Description:</b></h5><div>{!! $view_list->description !!}</div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Location:</b></h4><p>{{ $view_list->location }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Access:</b></h4><p>{{ $view_list->access }}</p>
                                        </div>
                                      </div>
                                    </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Latitude:</b></h4><p>{{ $view_list-> latitude }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Longitude:</b></h4><p>{{ $view_list->longitude }}</p>
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Altitude:</b></h4><p>{{ $view_list->altitude }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Flora:</b></h4><p>{{ $view_list->flora }}</p>
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Fauna:</b></h4><p>{{ $view_list->fauna }}</p>
                                        </div>
                                       
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Map Latitude:</b></h4><p>{{ $view_list->map_lat }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Map Longitude:</b></h4><p>{{ $view_list->map_long }}</p>
                                        </div>
                                      </div>

                                      <div><h5><b>Protected Wetland Historical Description:</b></h5>{!! $view_list->history_description !!}</div>
                                     
                                      @php 
                                     $count =1;
                                     @endphp
                                      @foreach($view_list->get_detail as $d)
                                      <div class="col-md-6 mt-4 row">
                                        <div class="">
                                        <h5><b>{{ $count++ }} {{ $d->title }}</b></h5>
                                        <p><img src="{{$d->images }}" style="width:20%;"></p>
                                      </div>
                                      </div>
                                      @endforeach

                                      
                                </div>
                          
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

