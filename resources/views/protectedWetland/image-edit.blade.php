@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Wetland</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/protected-wetland-list') }}">Protected Wetland List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit-protected-wetland-image/'.$id.'/'.$p_id) }}">Update Protected Wetland Historical Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Protected Wetland Historical Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/pro-wet-images-detail/'.$p_id) }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Wetland Historical Images List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('/edit-protected-wetland-image/'.$edit_image->id.'/'.$p_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                  <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Historical Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="title" value="{{ $edit_image->title }}">
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Historical Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="images" value="{{ $edit_image->image }}" accept="image/*">
                                            <img src="{{ $edit_image->images }}" style="width:30%;height:20%;">
                                        </div>
                                    </div>
                                </div>

                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div></div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');

    $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $( "#historical_desc" ).html();
       $("#add_more_history_de").append(html);
  });
});
</script>

@endpush
