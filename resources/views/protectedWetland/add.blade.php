@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Wetland</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/protected-wetland-list') }}">Protected Wetland List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/add-protected-wetland') }}">Add Protected Wetland Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Protected Wetland Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/protected-wetland-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Wetland Detail List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                      

                            <form action="{{ url('/add-protected-wetland') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Select Protected Wetland: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="type_id">
                                                <option>--Select--</option>
                                                @foreach($type_list as $t_l)
                                                    <option value="{{ $t_l->id }}">{{$t_l->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Description: </label>
                                        <div>
                                            <textarea class="form-control" name="description" id="editor">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Altitude: </label>
                                        <div class="col-md-6">
                                             <input type="text" class="form-control" placeholder="Enter Altitude" name="altitude" value="{{ old('altitude') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ old('location') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Access" name="access" value="{{ old('access') }}"/>
                                    </div>
                                 
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Latitute:</label>
                                        <input type="text" class="form-control" placeholder="Enter Latitute" name="latitude" value="{{ old('latitude') }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Longitude" name="longitude" value="{{ old('longitude') }}"/>
                                    </div>
                                </div>

                               

                                <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Fauna:</label>
                                        <input type="text" class="form-control" placeholder="Enter Fauna" name="fauna" value="{{ old('fauna') }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Flora:</label>
                                        <input type="text" class="form-control" placeholder="Enter Flora" name="flora" value="{{ old('flora') }}"/>
                                    </div>
                                 
                                </div>

                                <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Map Latitude:</label>
                                       <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_lat" value="{{ old('map_lat') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Map Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_long" value="{{ old('map_long') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>
                                 
                                </div>

                                 <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Map Zoom Size:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ old('zoom_level') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>
                                    </div>
                                  
                                </div>

                              

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Historical Description: </label>
                                        <div>
                                            <textarea class="form-control" name="history_description" id="editor1">{{ old('history_description') }}</textarea>
                                        </div>
                                    </div>
                                </div>     
                                <!-- -------------------- -->

                               <div style="background-color: #E5EFF1;">
                                    <div><p style="padding:1%;color:black;">Historical Data</p></div>
                                    <div class="form-body" style="padding-left: 5%;" id="historical_desc">
                                        <div class="form-group row" >
                                            <div class="col-lg-5">
                                            <label>Title:</label>
                                            <input type="text" class="form-control" placeholder="Enter the Title of image" name="title[]"/>
                                        </div>
                                        <div class="col-lg-5">
                                            <label>Image:</label>
                                            <input type="file" class="form-control" placeholder="Enter Status Of Land" name="images[]" accept="image/*"/>
                                        </div>
                                        </div>
                                       
                                    </div>
                                    <div id="add_more_history_de" style="padding-left: 5%;">
                                    </div>
                                </div>
                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" value="Add More" style="float:right;">Add More</button>

                                </div>  


                                 <div class="sub_heading_management" style="background-color: #E5EFF1; margin-bottom:20px;">
                                    <div><p style="padding:1%;color:black;margin-top:10%;">Add Notification Detail</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="sub_heading_management">
                                        <div class="form-group row" >
                                            <label class="col-md-1 col-form-label">Notification Title: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter Notification Title" name="title_noti[]" value="{{old('title_noti[]')}}">
                                            </div>

                                            <label class="col-md-1 col-form-label">Pdf File: </label>
                                            <div class="col-lg-4">
                                                <input type="file" class="form-control"  name="pdf_file[]" value="{{old('pdf_file[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_sub_heading_management" style="padding-left: 2%;margin-bottom:5%;">
                                    </div>
                                </div>  

                                <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more_" value="Add More" style="margin-left:72%;"><b>+</b> Add New Notification</button>
                                </div>&nbsp;  

                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');

    $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $( "#historical_desc" ).html();
       $("#add_more_history_de").append(html);
  });
});
</script>

<script>
   $( document ).ready(function() {
    $("#add_more_").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>

@endpush
