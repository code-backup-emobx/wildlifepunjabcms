@extends('layouts.master')
@section('content')



        <div class="page-content-wrapper">

            <div class="page-content">

                <div class="page-bar mt-5">
		            <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Wetland</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/protected-wetland-list') }}">Protected Wetland List</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/protected-wetland-list/'.$p_id) }}">Protected Wetland Historical Detail</a>
		                </li>
		            </ul>
        		</div>

        		@if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
            	
         			<div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>
                <div>
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Protected Wetalnd Historical Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-protected-wetland-image/'.$p_id) }}">
                                    <button type="button" class="btn btn-sm">Add Historical Detail
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
						<div class="card-body pt-0 pb-3">
							<div class="tab-content">
								<!--begin::Table-->
								<div class="table-responsive">
									<table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
										<thead>
											<tr>
											<th> Id </th>
											<th> Image </th>
											<th> Title </th>
											<th> Action </th>
											</tr>
										</thead>
										<tbody >

										@foreach($images_list as $listing)
										<tr>
											<td> {{ $listing->id }} </td>

											
											<td> <img src="{{ $listing->images}}" style="width:150px;"> </td>
											<td> {{ $listing->title }} </td>
											
										
											<td> 
												<a href="{{url('edit-protected-wetland-image/'.$listing->id.'/'.$p_id )}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
											
                                                <a href="{{url('delete-protected-wetland-image/'.$listing->id.'/'.$p_id )}}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
											</td>
										</tr>
										@endforeach
										</tbody>

									</table>
									<div class="d-flex justify-content-center">


									</div>
									<!--end::Table-->
								</div>
							</div>
						</div>
                                        
                    </div>
                    {{ $images_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
   </div>
   </div>
           
@endsection

