@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                  <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Wetland</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/protected-wetland-list') }}">Protected Wetland List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit-protected-wetland/'.$id) }}">Update Protected Wetland Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Protected Wetland Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/protected-wetland-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Wetland Detail List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                           <form action="{{ url('/edit-protected-wetland/'.$edit_wet_prot->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Select Protected Wetland: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="type_id">
                                                <option>--Select--</option>
                                                @foreach($type_list as $t_l)
                                                    <option value="{{ $t_l->id }}" @if($edit_wet_prot->type_id == $t_l->id) selected @endif>{{$t_l->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" accept="image/*">
                                            <img src="{{ $edit_wet_prot->banner_image }}" style="width:40%;height:40%;margin-top:2%;">
                                        </div>
                                    </div>
                                </div>

                               <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Description: </label>
                                        <div>
                                            <textarea class="form-control" name="description" id="editor">{{ $edit_wet_prot->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                               <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Altitude: </label>
                                        <div class="col-md-6">
                                             <input type="text" class="form-control" placeholder="Enter Altitude" name="altitude" value="{{ $edit_wet_prot->altitude }}"/>
                                        </div>
                                    </div>
                                </div>


                             <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ $edit_wet_prot->location }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Access" name="access" value="{{ $edit_wet_prot->access }}"/>
                                    </div>
                                 
                                </div>
                             <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Latitute:</label>
                                        <input type="text" class="form-control" placeholder="Enter Latitute" name="latitude" value="{{ $edit_wet_prot->latitude }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Longitude" name="longitude" value="{{ $edit_wet_prot->longitude }}"/>
                                    </div>
                                </div>

                               

                             <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Fauna:</label>
                                        <input type="text" class="form-control" placeholder="Enter Fauna" name="fauna" value="{{ $edit_wet_prot->fauna }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Flora:</label>
                                        <input type="text" class="form-control" placeholder="Enter Flora" name="flora" value="{{ $edit_wet_prot->flora }}"/>
                                    </div>
                                 
                                </div>

                                  <div class="form-group row" style="padding-left:5%;">

                                    <div class="col-lg-5">
                                        <label>Map Latitude:</label>
                                       <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_lat" value="{{ $edit_wet_prot->map_lat }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Map Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_long" value="{{ $edit_wet_prot->map_long }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>
                                 
                                </div>
                           
                            <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Map Zoom Size:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ $edit_wet_prot->zoom_level }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>
                                    </div>
                                  
                                </div>


                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Historical Description: </label>
                                        <div>
                                            <textarea class="form-control" name="history_description" id="editor1">{{ $edit_wet_prot->history_description }}</textarea>
                                        </div>
                                    </div>
                                </div>     
                               
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');

</script>

@endpush
