@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
		            <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Wetland</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/wetland-images-list') }}">Images List</a>
		                </li>
		            </ul>
        		</div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Protected Wetland Images List</span>
                            </div>
                             <div class="actions">
                                <a href="{{ url('/add-wetland-images') }}">
                                    <button type="button" class="btn btn-sm">Add Images
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
													<tr>
													<th> Id </th>
													<th> Wetland Name </th>
													<th> Image</th>
													
													<th> Action </th>
													</tr>
												</thead>
                                              
                                              	<tbody >

										@foreach($list as $listing)
										<tr>
											<td> {{ $listing->id }} </td>
											<td>{{ $listing->wetland_name->name}}</td>
												<td> <img src="{{ $listing->images }}" style="width:150px;"> </td>
										
											
											<td> 
												<a href="{{url('edit-wetland-images/'.$listing->id )}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
											
                                                <a href="{{url('delete-wetland-images/'.$listing->id.'/'.$listing->wetland_id )}}" onclick="return confirm('If you delete the master then child data automatically deleted . Are you sure you want to delete the master?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
											</td>
										</tr>
										@endforeach
										</tbody>

									</table>
									<div class="d-flex justify-content-center">


									</div>
									<!--end::Table-->
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $list->links('vendor.pagination.custom') }} 
                </div>
            </div>
       </div>
   </div>
           
@endsection

