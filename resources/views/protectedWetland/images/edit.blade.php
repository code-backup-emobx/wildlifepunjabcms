@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Wetland</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/wetland-images-list') }}">Images List</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/edit-wetland-images/'.$id) }}">Edit Image</a>
		                </li>
		            </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Protected Wetland Image</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/wetland-images-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Wetalnd Images List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                      

                            <form action="{{ url('/edit-wetland-images/'.$edit->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                              

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wetland Name: </label>
                                        <div class="col-lg-6">
                                           <select class="form-control" name="wetland_id">
                                             <option value="">--Select--</option>
                                                @foreach($list as $list)
                                                <option value="{{$list->id}}" @if($edit->wetland_id == $list->id ) selected @endif>{{ $list->name}}</option>
                                                @endforeach
                                           </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Image: </label>
                                        <div class="col-lg-6">
                                           <input type="file" name="image" class="form-control" value="{{ $edit->images }}" accept="image/*">
                                           <img src="{{ $edit->images }}" style="width:30%;margin-top:3%;">
                                        </div>
                                    </div>
                                </div>

                             
                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

