@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">

                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Wetland</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/protected-wetland-list') }}">Protected Wetland Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/pro-wet-pdf-detail/'.$p_id.'/'.$type_id) }}">Protected Wetland Notification List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit-prot-wet/'.$id.'/'.$p_id.'/'.$type_id) }}">Edit New Notitification</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Protected Wetland Notification</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('pro-wet-pdf-detail/'.$p_id.'/'.$type_id) }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Wetland Notifications List<i class="fa fa-list"></i></button>
                                
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          
                            <form action="{{ url('edit-prot-wet/'.$edit_noti->id.'/'.$p_id.'/'.$type_id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                             
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Notification Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="title" value="{{ $edit_noti->title }}" placeholder="Enter the notification text">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Notification Pdf: </label>
                                        <div class="col-lg-6">
                                            <input type="file" name="pdf_file" class="form-control" value="{{ old('pdf_file') }}" accept=".pdf">
                                            <iframe src="{{ $edit_noti->pdf_file }}"></iframe>
                                        </div>
                                    </div>
                                </div>

                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
  
</div>
           
@endsection
@push('page-script')

@endpush