@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                
                  <div class="page-bar mt-5">
                     <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Wetland</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/wetland-master-list') }}">Masters List</a>
                            <i class="fa fa-circle"></i>
		                </li>
                        <li>
                            <a href="{{ url('/add-new-master') }}">Add New</a>
                        </li>
		            </ul>
                </div>
                
                <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">

                <div class="row">
                    <div class="col-md-12">

                         @if(Session::has('success'))

                    <div class="alert alert-success">

                        {{ Session::get('success') }}

                        @php

                        Session::forget('success');

                        @endphp

                    </div>

                @endif
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Protected Wetland Master </span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/wetland-master-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm"> Protected Wetland Masters List<i class="fa fa-list"></i></button>
                                
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="{{ url('/add-new-master') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                               
                                
                               <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-md-3">Protecte Wetland Master Name: </label>
                                        <div class="col-md-6">
                                           <input type="text" name="name" class="form-control" value="{{ old('name')}}" placeholder="Enter The Wetland Master Name">
                                        </div>
                                    </div>
                                </div>

                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
 
@endpush
