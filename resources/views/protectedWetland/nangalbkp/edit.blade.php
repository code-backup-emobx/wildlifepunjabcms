<?php error_reporting(0); ?>
@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Nangal Wildlife Sanctuary</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/Nangal_wildlife') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Nangal Wildlife Sanctuary List</button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('update_nangal_detail/'.$editnangal->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Nangal Wildlife Sanctuary Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="">
                                            <div>
                                            	<img src="{{ $editnangal->banner_image }}" style="width: 104px;height: 78px;margin-top:2%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Nangal Wildlife Sanctuary Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{ $editnangal->banner_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Nangal Wildlife Title : </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="title_wildlife" value="{{ $editnangal->title_wildlife }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Nangal Wildlife Sanctuary Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea class="form-control" name="description" style=" height: 118px;" rows="9">{{ $editnangal->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">

                                	<div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ $editnangal->location }}"/>
                                    </div>

                                    <div class="col-lg-5">
                                        <label>Access:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full District" name="access" value="{{ $editnangal->location }}"/>
                                    </div>
                                 
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Latitute:</label>
                                        <input type="text" class="form-control" placeholder="Enter the Area" name="latitude" value="{{ $editnangal->latitude }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Longitude:</label>
                                        <input type="text" class="form-control" placeholder="Enter Status Of Land" name="longitude" value="{{ $editnangal->latitude }}"/>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Altitude: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="altitude" style=" height: 57px;" rows="5">{{ $editnangal->altitude }}</textarea>
                                        </div>
                                    </div>
                                </div>  

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Flora: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="flora" style=" height: 118px;" rows="5">{{ $editnangal->flora }}</textarea>
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Fauna: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="fauna" style=" height: 118px;" rows="5">{{ $editnangal->fauna }}</textarea>
                                        </div>
                                    </div>
                                </div>   

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Historical Heading: </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" 
                                            name="historical_heading" value="{{ $editnangal->historical_heading }}">
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Historical Description: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="historical_description" style="height:110px;" rows="5">{{  $editnangal->historical_description }}</textarea>
                                        </div>
                                    </div>
                                </div>     
                                <!-- -------------------- -->
                              <!--   <div style="background-color: #E5EFF1;padding-bottom: 2%;">
                                        <div><p style="padding:1%;color:black;">Add Image And Title For Historical Importance</p></div>
                               		<div class="form-group row" style="padding-left:5%;">
	                                    <div class="col-lg-5">
	                                        <label>Title 1:</label>
	                                        <input type="text" class="form-control" placeholder="Enter the Area" name="title[]" value="{{  $editnangal['get_nangal_images'][0]->title }}"/>
	                                    </div>
	                                    <div class="col-lg-5">
	                                        <label>Image 1:</label>
	                                        <input type="file" class="form-control" placeholder="Enter Status Of Land" name="image[]"/>
	                                        <div>
	                                        	<img src="{{  $editnangal['get_nangal_images'][0]->image }}" style="width: 104px;height: 78px;margin-top:2%;">
	                                        </div>
	                                    </div>
                                	</div>

                                	<div class="form-group row" style="padding-left:5%;">
	                                    <div class="col-lg-5">
	                                        <label>Title 2:</label>
	                                        <input type="text" class="form-control" placeholder="Enter the Area" name="title[]" value="{{  $editnangal['get_nangal_images'][1]->title }}"/>
	                                    </div>
	                                    <div class="col-lg-5">
	                                        <label>Image 2:</label>
	                                        <input type="file" class="form-control" placeholder="Enter Status Of Land" name="image[]"/>
	                                        <div>
	                                        	<img src="{{  $editnangal['get_nangal_images'][1]->image }}" style="width: 104px;height: 78px;margin-top:2%;">
	                                        </div>
	                                    </div>
                                	</div>

                                	<div class="form-group row" style="padding-left:5%;">
	                                    <div class="col-lg-5">
	                                        <label>Title 3:</label>
	                                        <input type="text" class="form-control" placeholder="Enter the Area" name="title[]" value="{{  $editnangal['get_nangal_images'][2]->title }}"/>
	                                    </div>
	                                    <div class="col-lg-5">
	                                        <label>Image 3:</label>
	                                        <input type="file" class="form-control" placeholder="Enter Status Of Land" name="image[]"/>
	                                        <div>
	                                        	<img src="{{  $editnangal['get_nangal_images'][2]->image }}" style="width: 104px;height: 78px;margin-top:2%;">
	                                        </div>
	                                    </div>
                                	</div>

                               </div>
                                -->
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
