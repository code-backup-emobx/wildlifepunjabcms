@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">

                <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>What's New</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/whats_new_list') }}">What's New List</a>
                        </li>
                        
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit What's New</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/whats_new_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm">What's New List<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          
                            <form action="{{ url('edit-whatnewdetail/'.$edit->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                             
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Whats New Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="text" value="{{ $edit->text }}" placeholder="Enter the text">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Date: </label>
                                        <div class="col-lg-6">
                                            <input type="date" name="date" class="form-control" value="{{ $edit->date }}">
                                            
                                        </div>
                                    </div>
                                </div>

                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
  
</div>
           
@endsection
@push('page-script')

@endpush