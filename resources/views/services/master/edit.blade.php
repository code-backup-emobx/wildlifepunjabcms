@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Services</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/services-master-list') }}">Services List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit-services/'.$id) }}">Edit Services</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Services</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/services-master-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Services List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                      

                            <form action="{{ url('/edit-services/'.$edit->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                              

                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Services Name: </label>
                                        <div class="col-lg-6">
                                            <input type="text" name="name" class="form-control" value="{{ $edit->name }}" placeholder="Enter the services name">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Status: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="status">
                                                <option value="">--Select--</option>
                                                
                                                <option value="online" @if($edit->status == 'online') selected @endif>Online</option>
                                                <option value="offline" @if($edit->status == 'offline') selected @endif>Offline</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Link: </label>
                                        <div class="col-lg-6">
                                            <input type="text" name="website_link" class="form-control" value="{{ $edit->website_link }}" placeholder="Enter the url" data-toggle="tooltip" data-placement="top" title="Enter the url you have to link this services">
                                        </div>
                                    </div>
                                </div>
                             
                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
  

@endpush
