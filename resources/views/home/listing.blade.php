@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">

                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Home</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/home_detail') }}">Home Page Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Home Page Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add_homepagedetail') }}">
                                    <button type="button" class="btn btn-sm">Add Home Detail
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Id </th>
                                                        <th> Home Banner Heading </th>
                                                        <th> Home Background Image </th>
                                                        <th> Home Title</th>
                                                        <th> Home Introduction Description </th>
                                                        <th> Home Wildlife Title </th>
                                                        <th> Home Wild Description </th>
                                                        <th> Home Geographic Heading </th>
                                                        <th> Home Geographic Zone</th>
                                                        <th colspan="2"> Action </th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                <tbody>
                                                    @foreach($homedetail_list as $homedetail_listing)
                                                    <tr>
                                                        <td> {{ $homedetail_listing->id }} </td>
                                                        <td> <img src="{{ $homedetail_listing->bg_image }}" style="width:89px;height:65px;"> </td>
                                                        <td> {{ $homedetail_listing->banner_heading }} </td>
                                                        <td> {{ $homedetail_listing->title }} </td>
                                                        <td> {{ $homedetail_listing->intro_description }} </td>
                                                        <td> {{ $homedetail_listing->wild_life_title }} </td>
                                                        <td> {{ $homedetail_listing->wild_description }} </td>
                                                        <td>{{ $homedetail_listing->geographic_statement}}</td>
                                                        <td> 
                                                            <a href="{{ url('geographic_listing/'.$homedetail_listing->id) }}"><span class="btn btn-inline btn-primary font-weight-bold">{{ $homedetail_listing->g_count }}</span></a>
                                                            
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('update_homedetail/'.$homedetail_listing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a></td><td>
                                                            <a href="{{ url('delete_homedetail/'.$homedetail_listing->id) }}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach 
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     {{ $homedetail_list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
           
@endsection

