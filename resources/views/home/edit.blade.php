@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                   <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Home</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/home_detail') }}">Home Page Detail</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/update_homedetail/'.$id) }}">Edit Page Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Home Page detail</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/home_detail') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Home Page Detail<i class="fa fa-list"></i></button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <form action="{{ url('/update_homedetail/'.$edithomedetail->id ) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Home Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" placeholder="Enter Zoo Image" name="bg_image" value="">
                                            <div>
                                            	<img src="{{ $edithomedetail->bg_image }}" style="    width: 89px;height: 65px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Banner Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Banner Heading" name="banner_heading" value="{{ $edithomedetail->banner_heading }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Title" name="title" value="{{ $edithomedetail->title }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Introduction Description: </label>
                                        <div class="col-lg-6">
                                           <!--  <input type="text" class="form-control" placeholder="Enter Introduction Description" name="intro_description" value="{{ $edithomedetail->intro_description }}"> -->
                                           <textarea class="form-control" name="intro_description" rows="9">{{ $edithomedetail->intro_description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wild Life Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Wild Life Title" name="wild_life_title" value="{{ $edithomedetail->wild_life_title }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Wild Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" placeholder="Enter Wild Description" name="wild_description" value="{{ $edithomedetail->wild_description }}" rows="7"> -->
                                            <textarea class="form-control" name="wild_description" rows="9">{{ $edithomedetail->wild_description }}</textarea>
                                        </div>
                                    </div>
                                </div>

<!-- 
                                <div class="btn-side">
                                    <button type="button" class="btn btn-danger" id="add_more" value="Add More" style="float:right;">Add More</button>

                                </div>   -->
                            
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $( "#geographic_zone" ).html();
       $("#add_more_geographic_zone").append(html);
  });
});
</script>
@endpush
