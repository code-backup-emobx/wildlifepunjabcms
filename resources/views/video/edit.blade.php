@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                  <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Videos List</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/videos_list') }}">Videos List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit_video/'.$id) }}">Update Video</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Update Video</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/videos_list') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Video List<i class="fa fa-list"></i></button>
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                          

                            <form action="{{ url('/edit_video/'.$edit_video->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Video Title: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="video_title" value="{{ $edit_video->video_title }}" placeholder="Enter Video Title">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Video Link: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="video_link" value="https://www.youtube.com/watch?v={{ $edit_video->video_link }}" placeholder="Enter Video Link" data-toggle="tooltip" data-placement="top" title="You Tube Video link eg: https://www.youtube.com/watch?v=yWeasduvdgc">
                                        </div>
                                        <div class="col-lg-6" style="margin-left:25%;margin-top:2%;"> <iframe src="https://www.youtube.com/embed/{{ $edit_video->video_link }}" allowfullscreen></iframe></div>
                                       
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Type: </label>
                                        <div class="col-lg-6">
                                            <select name="type" class="form-control">
                                            	@foreach($zoo_list as $z_list)
                                                    <option value="{{ $z_list->id }}" @if($z_list->id == $edit_video->type) selected @endif>{{ $z_list->zoo_type_name }}</option>
                                                @endforeach
                                                @foreach($protected_area as $list)
                                                    <option value="{{ $list->id }}" @if($list->id == $edit_video->type) selected @endif>{{ $list->name }}</option>
                                                @endforeach
                                                @foreach($protect_wetland as $w_list)
                                                    <option value="{{ $w_list->id }}" @if($w_list->id == $edit_video->type) selected @endif>{{ $w_list->name }}</option>
                                                @endforeach
                                              @foreach($eventmaster as $e_list)
                                                    <option value="{{ $e_list->id }}" @if($e_list->id == $edit_video->type) selected @endif>{{ $e_list->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            
                               </div>
                               
                                <!--  ---------------------->
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
