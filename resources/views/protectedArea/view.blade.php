@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
              <div class="page-bar mt-5">
                     <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Area</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/prot-area-list') }}">Protected Area Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/view-prot-area/'.$id) }}">View Protected Area Detail</a>
                        </li>
                    </ul>
              </div>


               @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif

                <div class="row" style="margin-top:2%;">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">View Protected Area Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/prot-area-list') }}">
                                    <button type="button" class="btn btn-sm">Protected Area Detail
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                           
                           
                                <div class="container row">
                                    <div class="col-md-12">
                                      <div><h5><b>Protected Area Category Name:</b></h5>{{ $view->get_cat->category_name }}</div>

                                       <div><h5><b>Protected Area Sub Category Name:</b></h5>{{ $view->get_subcat->name }}</div>

                                      <h5><b>Banner image:</b></h5><p><img src="{{ $view->banner_image }}" style="width:20%;"></p>

                                      <h5><b>Description:</b></h5><div>{!! $view->description !!}</div>

                                    </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Latitude:</b></h4><p>{{ $view->map_lat }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Longitude:</b></h4><p>{{ $view->map_long }}</p>
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                         <div class="col-md-6">
                                          <h4><b>Location:</b></h4><p>{{ $view->location }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Flora:</b></h4>{{ $view->imp_flora }}
                                        </div>
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Fauna:</b></h4>{{ $view->imp_fauna }}
                                        </div>
                                       
                                      </div>

                                      <div class="col-md-12 mt-4">
                                        <div class="col-md-6">
                                          <h4><b>Map Latitude:</b></h4><p>{{ $view->map_lat }}</p>
                                        </div>
                                        <div class="col-md-6">
                                          <h4><b>Map Longitude:</b></h4><p>{{ $view->map_long }}</p>
                                        </div>
                                      </div>

                                  
                                     
                                      @php 
                                     $count =1;
                                     @endphp
                                      @foreach($view->get_notification as $d)
                                      <div class="col-md-6 mt-4 row">
                                        <div class="">
                                        <h5><b>{{ $count++ }} {{ $d->title }}</b></h5>
                                        <p><iframe src="{{$d->pdf_file }}" style="width:20%;"></iframe> </p>
                                      </div>
                                      </div>
                                      @endforeach

                                      
                                </div>
                          
                            <!-- END FORM-->
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
   </div>
           
@endsection

