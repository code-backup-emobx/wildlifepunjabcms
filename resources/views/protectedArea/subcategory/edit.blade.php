@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Area</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/prot-area-subcategory-list') }}">Sub Category List</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/edit-prot-area-subcategory/'.$id) }}">Edit Sub Category</a>
		                </li>
		            </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Protected Area Sub Category</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/prot-area-subcategory-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Area Sub Category List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                      

                            <form action="{{ url('/edit-prot-area-subcategory/'.$edit_subcat->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                              

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Category Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="category_id">
                                                <option value="">--Select--</option>
                                                @foreach($category_list as $list)
                                                <option value="{{$list->id}}" @if($edit_subcat->category_id == $list->id ) selected @endif>{{ $list->category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Sub Category Name: </label>
                                        <div class="col-lg-6">
                                            <input type="text" name="subcat_name" class="form-control" value="{{ $edit_subcat->name }}">
                                        </div>
                                    </div>
                                </div>

                             
                             
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
 

@endpush
