@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Area</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/prot-area-list') }}">Protected Area Detail List</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/add-prot-area') }}">Add Protected Detail</a>
		                </li>
		            </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Protected Area Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/prot-area-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Area Detail List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                      

                            <form action="{{ url('/add-prot-area') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                              
                               <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="{{ old('banner_image') }}" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Category Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="category_id" id="category_id" value="{{ old('category_id') }}">
                                                <option value="">--Select--</option>
                                                @foreach($category_list as $list)
                                                <option value="{{$list->id}}">{{ $list->category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Sub Category Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="sub_category_id" id="sub_category_id" value="{{ old('sub_category_id') }}">
                                                <option value="">--Select--</option>
                                               
                                                <option value="'+value+'"></option>
                                              
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>District:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full District" name="district" value="{{old('district')}}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{old('location')}}" />
                                    </div>
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Area:</label>
                                        <input type="text" class="form-control" placeholder="Enter the Area" name="area" value="{{old('area')}}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Status Of Land:</label>
                                        <input type="text" class="form-control" placeholder="Enter Status Of Land" name="status_of_land" value="{{old('status_of_land')}}" />
                                    </div>
                                </div>
                                  <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Map Latitute:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_lat" value="{{ old('map_lat') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Map Longitude:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_long" value="{{ old('map_long') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>
                                </div>
                             <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Map Zoom Size:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ old('zoom_level') }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>
                                    </div>
                                  
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Description: </label>
                                        <div class="">
                                            <textarea class="form-control" name="description" id=
                                            "editor1">{{old('description')}}</textarea>
                                        </div>
                                    </div>
                                </div>
 
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Important Fauna: </label>
                                        <div>
                                            <textarea class="form-control" name="important_fauna" id="editor">{{old('important_fauna')}}</textarea>
                                        </div>
                                    </div>
                                </div>   
                                    
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Important Fauna: </label>
                                        <div>
                                            <textarea class="form-control" name="important_flora" id="editor2">{{old('important_flora')}}</textarea>
                                        </div>
                                    </div>
                                </div>   

                                 <div class="sub_heading_management" style="background-color: #E5EFF1; margin-bottom:20px;">
                                    <div><p style="padding:1%;color:black;">Add Notification Detail</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="sub_heading_management">
                                        <div class="form-group row" >
                                            <label class="col-md-1 col-form-label">Notification Title: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter Notification Title" name="title[]" value="{{old('title[]')}}">
                                            </div>

                                            <label class="col-md-1 col-form-label">Pdf File: </label>
                                            <div class="col-lg-4">
                                                <input type="file" class="form-control"  name="pdf_file[]" value="{{old('pdf_file[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_sub_heading_management" style="padding-left: 2%;margin-bottom:5%;">
                                    </div>
                                </div>  

                                <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more" value="Add More" style="margin-left:72%;"><b>+</b> Add New Notification</button>
                                </div>&nbsp;  
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
@push('page-script')
 <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>

<script type="text/javascript">
    $('#category_id').change(function(){
    var categoryID = $(this).val();
    var token = '{{ csrf_token() }}';    
    if(categoryID){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('getsubcategory')}}?category_id="+categoryID,
           success:function(res){               
            if(res){
                console.log(res);
                $("#sub_category_id").empty();
                $("#sub_category_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#sub_category_id").append('<option value="'+value+'">'+key+'</option>');
                });
           
            }else{
               $("#sub_category_id").empty();
            }
           }
        });
    }
    else{
        $("#sub_category_id").empty();
        
    }      
   });
</script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
</script>
@endpush


