@extends('layouts.master')
@section('content')



        <div class="page-content-wrapper">

            <div class="page-content">

                <div class="page-bar mt-5">
		            <ul class="page-breadcrumb">
		                <li>
		                	<a href="{{ url('/home') }}">Dashboard</a>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<span>Protected Area</span>
		                	<i class="fa fa-circle"></i>
		                </li>
		                <li>
		                	<a href="{{ url('/prot-area-subcategory-list') }}">Protected Detail List</a>
		                </li>
		            </ul>
        		</div>

        		@if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
            	
         				<div>
                <div class="row" style="margin-top:2%;">
                <div>
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title" style="border-bottom: 0px solid #eef1f5">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Protected Area Detail List</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/add-prot-area') }}">
                                    <button type="button" class="btn btn-sm">Add 
                                        <i class="fa fa-plus"></i></button>
                                </a>
                            </div>
                        </div>
						<div class="card-body pt-0 pb-3">
							<div class="tab-content">
								<!--begin::Table-->
								<div class="table-responsive">
									<table class="table table-bordered table-head-custom table-head-bg table-vertical-center table-checkable" id="kt_datatable">
										<thead>
											<tr>
											<th> Id </th>
											<th> Category Name </th>
											<th> SubCategory Name </th>
											<th> Banner Image </th>
											<th> District </th>
											<th> Area </th>
											<th> Total Notification </th>
											<th colspan="3"> Action </th>
											</tr>
										</thead>
										<tbody >

										@foreach($list as $listing)
										<tr>
											<td>{{ $listing->id }} </td>

											<td> {{ $listing->get_cat->category_name }} </td>
											<td> {{ $listing->get_subcat->name }} </td>
											<td> <img src="{{ $listing->banner_image }}" style="width:150px;"> </td>
										
											<td> {{ $listing->district }} </td>
											<td> {{ $listing->area }} </td>
										<td><a href="{{ url('prot-area-noti-list/'.$listing->id.'/'.$listing->subcategory_id) }}"><span class="btn btn-inline btn-primary font-weight-bold">{{ $listing->pdf_count }}</span></a></td>
											<td> 
												<a href="{{ url('view-prot-area/'.$listing->id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-eye"></i></button></a>
											</td>
											<td> 
												<a href="{{ url('edit-prot-area/'.$listing->id) }}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;"><i class="fa fa-edit"></i></button></a>
											</td><td>
                                                <a href="{{ url('delete-prot-area/'.$listing->id) }}" onclick="return confirm('If you delete the detail then their chils data automatically deleted . Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a>
											</td>
										</tr>
										@endforeach
										</tbody>

									</table>
									<div class="d-flex justify-content-center">


									</div>
									<!--end::Table-->
								</div>
							</div>
						</div>
                                        
                    </div>
                      {{ $list->links('vendor.pagination.custom') }}
                </div>
            </div>
       </div>
   </div>
   </div>
   </div>
           
@endsection

