@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                 <div class="page-bar mt-5">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Protected Area</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/prot-area-list') }}">Protected Area Detail List</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{ url('/edit-prot-area/'.$id) }}">Edit Protected Detail</a>
                        </li>
                    </ul>
                </div>

                @if(Session::has('success'))

                <div class="alert alert-success">

                    {{ Session::get('success') }}

                    @php

                    Session::forget('success');

                    @endphp

                </div>

                @endif
                
                    <div class="d-flex flex-column-fluid mt-10" style="margin-top:2%;">
                <div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Edit Protected Area Detail</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('/prot-area-list') }}">
                                    <button type="button" class="btn btn-light-primary btn-sm">Protected Area Detail List<i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                      

                            <form action="{{ url('/edit-prot-area/'.$edit_protectedareadetail->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                              
                               <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Banner Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="{{ old('banner_image') }}" accept="image/*">
                                            <img src="{{ $edit_protectedareadetail->banner_image }}" style="width:30%;margin-top:3%;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Category Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="category_id" id="category_id" value="{{ old('category_id') }}">
                                                <option value="">--Select--</option>
                                                @foreach($category as $list)
                                                <option value="{{$list->id}}" @if($list->id == $edit_protectedareadetail->category_id) selected @endif>{{ $list->category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Sub Category Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="sub_category_id" id="sub_category_id" value="{{ old('sub_category_id') }}">
                                                @if(!empty($edit_protectedareadetail->get_subcat->id ))
                                                <option value="{{ $edit_protectedareadetail->get_subcat->id }}">{{ $edit_protectedareadetail->get_subcat->name }} </option>
                                               @else
                                                <option value="'+value+'"></option>
                                                @endif
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>District:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full District" name="district" value="{{ $edit_protectedareadetail->district }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{ $edit_protectedareadetail->location }}" />
                                    </div>
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Area:</label>
                                        <input type="text" class="form-control" placeholder="Enter the Area" name="area" value="{{ $edit_protectedareadetail->area }}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Status Of Land:</label>
                                        <input type="text" class="form-control" placeholder="Enter Status Of Land" name="status_of_land" value="{{ $edit_protectedareadetail->status_of_land }}" />
                                    </div>
                                </div>
                                  <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Map Latitute:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_lat" value="{{  $edit_protectedareadetail->map_lat }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Map Longitude:</label>
                                         <input type="text" class="form-control" placeholder="Enter Map Latitute eg:30.9590687" name="map_long" value="{{ $edit_protectedareadetail->map_long }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the after @ number 30.9590687"/>
                                    </div>
                                </div>
                            
                             <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Map Zoom Size:</label>
                                         <input type="number" class="form-control" placeholder="Enter Map Zoom Size eg:1-25  any integer number" name="zoom_level" value="{{ $edit_protectedareadetail->zoom_level }}"  data-toggle="tooltip" data-placement="top" title="latitute for eg: /@30.9590687,75.8204768,16z/...just enter the any interger number before z number 1-25"/>
                                    </div>
                                  
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Description: </label>
                                        <div class="">
                                            <textarea class="form-control" name="description" id=
                                            "editor1">{!! $edit_protectedareadetail->description !!}</textarea>
                                        </div>
                                    </div>
                                </div>
 
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Important Fauna: </label>
                                        <div>
                                            <textarea class="form-control" name="important_fauna" id="editor">{{ $edit_protectedareadetail->imp_fauna }}</textarea>
                                        </div>
                                    </div>
                                </div>   
                                    
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-form-label">Important Fauna: </label>
                                        <div>
                                            <textarea class="form-control" name="important_flora" id="editor2">{{ $edit_protectedareadetail->imp_flora }}</textarea>
                                        </div>
                                    </div>
                                </div>   

                               
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection
@push('page-script')
 <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>

<script type="text/javascript">
    $('#category_id').change(function(){
    var categoryID = $(this).val();
    var token = '{{ csrf_token() }}';    
    if(categoryID){
        $.ajax({
             headers: {
                    'X-CSRF-TOKEN': token
                },
           type:"GET",
           url:"{{url('getsubcategory')}}?category_id="+categoryID,
           success:function(res){               
            if(res){
                console.log(res);
                $("#sub_category_id").empty();
                $("#sub_category_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#sub_category_id").append('<option value="'+value+'">'+key+'</option>');
                });
           
            }else{
               $("#sub_category_id").empty();
            }
           }
        });
    }
    else{
        $("#sub_category_id").empty();
        
    }      
   });
</script>
<script>
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
</script>
@endpush


