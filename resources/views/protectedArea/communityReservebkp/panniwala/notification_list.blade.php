@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Notifications Listing</span>
                            </div>
                            <div class="actions">
                                <a href="{{ url('panniwala_add_new_notification/'.$id) }}">
                                    <button type="button" class="btn btn-sm">Add New Notification
                                        <i class="fa fa-plus"></i></button>
                                </a>
                                <a href="{{ url('panniwalalist') }}">
                                    <button type="button" class="btn btn-sm">Panniwala List
                                        <i class="fa fa-list"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            @if(Session::has('success'))

                            <div class="alert alert-success">

                                {{ Session::get('success') }}

                                @php

                                Session::forget('success');

                                @endphp

                            </div>

                            @endif
                            <form action="" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Notification Text </th>
                                                        <th> Notification Pdf </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                              
                                                <tbody>
                                                   
                                                    @foreach($notification_list as $listing)
                                                    <tr>
                                                        <td> {{ $listing->id }} </td>
                                                        <td> {{ $listing->notification_title }} </td>
                                                         <td><iframe src="{{ $listing->pdf_url}}"></iframe></td>
                                                        <td>
                                                            <a href="{{ url('panniwala_edit_notification_text/'.$listing->id.'/'.$id)}}"><button type="button" class="btn btn-sm" style="background-color:#1BC5BD;color:white;">Edit<i class="fa fa-edit"></i></button></a>
                                                            <a href="{{ url('panniwala_delete_notification_text/'.$listing->id)}}" onclick="return confirm('Are you sure you want to delete this?')"><button type="button" class="btn btn-sm btn-danger">Delete<i class="fa fa-trash"></i></button></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                               
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                  
                </div>
            </div>
       </div>
   </div>
           
@endsection

