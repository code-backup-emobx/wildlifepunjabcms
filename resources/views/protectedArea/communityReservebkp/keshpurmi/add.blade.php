@extends('layouts.master')
@section('content')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Add Keshpurmi Community Reserve</span>
                            </div>
                            <div class="actions">
                              	<a href="{{ url('/keshpurmianilist') }}">
                              		<button type="button" class="btn btn-light-primary btn-sm"> Keshpurmi Community List</button>
                    			
                				</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                            @endif

                            <form action="{{ url('/add_keshpurmi ') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Keshpurmi Community Background Image: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="banner_image" value="" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Keshpurmi Community Heading: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="banner_heading" value="{{old('banner_heading')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Keshpurmi Community Description: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" name="banner_heading" value=""> -->
                                            <textarea class="form-control" name="description" rows="9">{{old('description')}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Community Heading:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Community Heading" name="wildlife_heading" value="{{old('wildlife_heading')}}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Community Title:</label>
                                        <input type="text" class="form-control" placeholder="Enter Community Title" name="wildlife_title" value="{{old('wildlife_title')}}">
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>District:</label>
                                        <input type="text" class="form-control" placeholder="Enter Full District" name="district" value="{{old('district')}}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" placeholder="Enter Location" name="location" value="{{old('location')}}"/>
                                    </div>
                                </div>
                                    
                                <div class="form-group row" style="padding-left:5%;">
                                    <div class="col-lg-5">
                                        <label>Area:</label>
                                        <input type="text" class="form-control" placeholder="Enter the Area" name="area" value="{{old('area')}}"/>
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Status Of Land:</label>
                                        <input type="text" class="form-control" placeholder="Enter Status Of Land" name="status_of_land" value="{{old('status_of_land')}}"/>
                                    </div>
                                </div>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Important Fauna: </label>
                                        <div class="col-md-6">
                                            <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="important_fauna" rows="5">{{old('important_fauna')}}</textarea>
                                        </div>
                                    </div>
                                </div>   
                                    
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Important Fauna: </label>
                                        <div class="col-lg-6">
                                             <!-- <input type="text" class="form-control" name="bg_image" value=""> -->
                                            <textarea class="form-control" name="important_flora" rows="5">{{old('important_flora')}}</textarea>
                                        </div>
                                    </div>
                                </div> 

                                <div class="sub_heading_management" style="background-color: #E5EFF1; margin-bottom:20px;">
                                    <div><p style="padding:1%;color:black;">Add Notification Detail</p></div>
                                    <div class="form-body" style="padding-left: 2%;" id="sub_heading_management">
                                        <div class="form-group row" >
                                            <label class="col-md-1 col-form-label">Notification Title: </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter Notification Title" name="notificationtitle[]" value="{{old('notificationtitle[]')}}"> 
                                            </div>

                                            <label class="col-md-1 col-form-label">Pdf File: </label>
                                            <div class="col-lg-4">
                                                <input type="file" class="form-control"  name="pdf_file[]" value="{{old('pdf_file[]')}}" accept=".pdf">
                                            </div>

                                        </div>
                                       
                                    </div>
                                    <div id="add_more_sub_heading_management" style="padding-left: 2%;margin-bottom:5%;">
                                    </div>
                                </div>  

                                <div class="btn-side">
                                    <button type="button" class="btn btn-success" id="add_more" value="Add More" style="margin-left:72%;"><b>+</b> Add New Notification</button>
                                </div>&nbsp;       
                              
                           
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')

<script>
   $( document ).ready(function() {
    $("#add_more").click(function(){
      var html = $("#sub_heading_management").html();
       $("#add_more_sub_heading_management").append(html);
  });
});
</script>
@endpush
